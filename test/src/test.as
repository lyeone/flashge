package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	
	import FlashGE.objects.AxisFrame;
	import FlashGE.objects.Entity;
	import FlashGE.render.Camera3D;
	import FlashGE.render.ViewPort;
	
	public class test extends Sprite
	{
		public var camera:Camera3D = new Camera3D(0.1, 10000);
		public var rootNode:Entity = new Entity;
		
		public function test()
		{
			this.addEventListener(Event.ADDED_TO_STAGE, onAddToStage);
		}
		
		private function onAddToStage(e:Event):void
		{
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.frameRate = 24;
			stage.stage3Ds[0].addEventListener(Event.CONTEXT3D_CREATE, onContext3DCreate);
			stage.stage3Ds[0].requestContext3D();
		}
		
		private function onContext3DCreate(e:Event):void
		{
			camera.view = new ViewPort(this.stage.stageWidth, this.stage.stageHeight, 0, 0, 4);
			camera.setRootNode(rootNode);
			camera.closeDeltaTimer = true;
			
			camera.setPosition(300, -300,300);
			camera.lookAt(0,0,0);
			
			var axis:AxisFrame = new AxisFrame;
			rootNode.addChild(axis);
			rootNode.addChild(camera);
			this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function onEnterFrame(e:Event):void
		{
			camera.render(this.stage.stage3Ds[0]);
		}
	}
}