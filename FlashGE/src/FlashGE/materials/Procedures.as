/*@author lye 123*/
package FlashGE.materials
{
	import flash.utils.Dictionary;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.materials.compiler.Procedure;
	import FlashGE.materials.compiler.VariableType;
	
	use namespace FlashGE;
	public final class Procedures
	{
		static FlashGE var procedureSet:Dictionary = new Dictionary;
		
		static FlashGE const uvProcedure:Procedure = new Procedure([
			"#v0=vUV", 
			"#a0=aUV", 
			"mov v0, a0"
		], "uvProcedure");
		
		static FlashGE const uvProcedureEx:Procedure = new Procedure([
			"#v0=vUV", 
			"#a0=aUV", 
			"#c127=cUVTrans", // v0, v1, t, rotation
			"mov t1, c127",
			"mul t1.xy, t1.xy, c127.zz",
			"add t1.xy, a0.xy, t1.xy",	
			"mov v0, t1"
		], "uvProcedure");
		
		static FlashGE const uvSimpleTransProcedure:Procedure = 
			new Procedure([
				"#v0=vUV", 
				"#a0=aUV",
				"#c0=cUVTrans",
				"mov t1, c0",
				"mul t1.xy, t1.xy, t1.zz",
				"mov t0, a0",
				"add t0.xy, t0.xy, t1.xy",
				"mov v0, t0",
				"mov v0.zw, a0.xy"
			], "uvSimpleTransProcedure");
		
		static FlashGE const uvTransProcedure:Procedure = 
			new Procedure([
				"#v0=vUV", 
				"#v1=vUVOrg",
				"#a0=aUV", 
				"#c125=cUVTrans2",
				"#c126=cUVScale", // uscale,vscale,uoffset,voffset
				"#c127=cUVTrans", // v0, v1, t, rotation
				"mul t0.x, a0.x, c126.x",
				"mul t0.y, a0.y, c126.y",
				"mov t1, c127",
				"sin t2.x, t1.w", // sin
				"cos t2.y, t1.w", // cos
				
				"mul t0.z, t0.x, t2.y", // x*cos
				"mul t0.w, t0.y, t2.x", // y*sin
				"sub t2.z, t0.z, t0.w", // X
				
				"mul t0.z, t0.x, t2.x", // x*sin
				"mul t0.w, t0.y, t2.y", // y*cos
				"add t2.w, t0.z, t0.w", // Y
				
				"mov t1.xy, c127.xy",
				"mul t1.xy, t1.xy, c127.zz",
				"add t2.xy, t2.zw, c126.zw",
				"add t2.xy, t2.xy, t1.xy",	
				"mov v0, t2",
				"mov t0, a0",
				"add t0.xy, t0.xy, c125.zw",
				"mov v1, t0"
			], "uvTransProcedure");
		
		static FlashGE const uvFrameProcedure:Procedure = 
			new Procedure([
				"#v0=vUV", 
				"#v1=vUVOrg",
				"#a0=aUV", 
				"#c125=cUVTrans2",
				"#c126=cUVScale", // uscale,vscale,uoffset,voffset
				"#c127=cUVTrans", // 0, 0, 0, rotation
				"mul t0.x, a0.x, c126.x",
				"mul t0.y, a0.y, c126.y",
				"mov t1, c127",
				"sin t2.x, t1.w", // sin
				"cos t2.y, t1.w", // cos
				
				"mul t0.z, t0.x, t2.y", // x*cos
				"mul t0.w, t0.y, t2.x", // y*sin
				"sub t2.z, t0.z, t0.w", // X
				
				"mul t0.z, t0.x, t2.x", // x*sin
				"mul t0.w, t0.y, t2.y", // y*cos
				"add t2.w, t0.z, t0.w", // Y
				
				"add t2.xy, t2.zw, c126.zw",
				"mov v0, t2",
				"mov t0, a0",
				"add t0.xy, t0.xy, c125.zw",
				"mov v1, t0"
			], "uvFrameProcedure");		
		
		static FlashGE const vertDiffuseProcedure:Procedure = new Procedure(["mov v6, i0"], "vertDiffuseProcedure");
		static FlashGE function getModelVertexProcedure(decodeNormal:Boolean, recieveShadow:Boolean):Procedure
		{
			/*
			half3 decode (half4 enc, float3 view)
			{
			half4 nn = enc*half4(2,2,0,0) + half4(-1,-1,1,-1);
			half l = dot(nn.xyz,-nn.xyw);
			nn.z = l;
			nn.xy *= sqrt(l);
			return nn.xyz * 2 + half3(0,0,-1);
			}			
			array[j++] = "mov t1.x, i0.x";
			array[j++] = "sge t1.x, t1.x, t1.x"; // x  1
			array[j++] = "add t1.y, t1.x, t1.x"; // y  2
			array[j++] = "slt t1.z, t1.x, t1.x"; // z  0
			array[j++] = "neg t1.w, t1.x"; 		 // w -1
			*/
			
			
			var shader:Array = [];
			var i:int = 0;
			shader[i++] = "#v1=vNormal";
			//shader[i++] = "#v2=vViewDir";
			//shader[i++] = "#c124=cCameraPos";
			
			shader[i++] = "m44 o0, i0, c0";
			
			if (decodeNormal) {
				shader[i++] = "mov t1.x, i0.x";
				shader[i++] = "sge t1.x, t1.x, t1.x"; // x  1
				shader[i++] = "add t1.y, t1.x, t1.x"; // y  2
				shader[i++] = "slt t1.z, t1.x, t1.x"; // z  0
				shader[i++] = "neg t1.w, t1.x"; 		 // w -1
				shader[i++] = "mov t2.xyzw, t1.wwxw";
				shader[i++] = "mov t3.xyzw, t1.yyzz"; // 2200
				shader[i++] = "mul t3, i1, t3";
				shader[i++] = "add t3, t3, t2"; // nn
				shader[i++] = "neg t2, t3"; 
				shader[i++] = "dp3 t0.z, t3.xyz, t2.xyw"; //t0.z = l
				shader[i++] = "sqt t0.w, t0.z";
				shader[i++] = "mul t0.xy, t3.xy, t0.w";
				shader[i++] = "mul t0.xyz, t0.xyz, t1.yyy";
				shader[i++] = "sub t0.z, t0.z, t1.x";
				
				shader[i++] = "sge t0.w, t0.w, t0.w";					
				shader[i++] = "mov v1, t0";
			} else {
				//shader[i++] = "mov t2, i1";
				//shader[i++] = "sge t2.w, t2.w, t2.w";					
				shader[i++] = "mov v1, i1";
			}
			//shader[i++] = "sub t1, c124, i0";
			//shader[i++] = "mov v2, t1";
			
			if (recieveShadow) {
				shader[i++] = "#v5=vShadowUV";
				shader[i++] = "#c125=cShadowMapUVProjection";
				shader[i++] = "m34 v5.xyz, i0, c125";
				shader[i++] = "mov v5.w, i0.w";
			}
			
			var res:Procedure = new Procedure(shader, "SimpleProject");
			res.assignVariableName(VariableType.CONSTANT, 0, "cProjMatrix", 4);
			return res;
		}
		
		static FlashGE function getEffectVertexProcedure(openNormalAlpha:Boolean):Procedure 
		{
			var i:int = 0;
			var array:Array = [];
			
			if (openNormalAlpha) {
				array[i++] = "#v2=vNormal";
				array[i++] = "#v3=vViewVector";
				array[i++] = "#c125=cCameraPos";
				
				array[i++] = "m44 o0, i0, c0";
				
				array[i++] = "sub t1, c125, i0";
				array[i++] = "mov t1.w, c125.w";
				array[i++] = "mov v3, t1";
				
				array[i++] = "mov t2, i1";
				array[i++] = "sge t2.w, t2.w, t2.w";
				
				array[i++] = "mov v2, t2";
			} else {
				array[i++] = "m44 o0, i0, c0";
			}
			
			var res:Procedure = new Procedure(array, "effectVertexProcedure");
			res.assignVariableName(VariableType.CONSTANT, 0, "cProjMatrix", 4);
			return res;
		}
		
		static FlashGE function getModelFramentProcedure(mat:Material, 
														 texNum:int, 
														 texOp:int, 
														 filterIndex:int, 
														 repeatIndex:int, 
														 mipIndex:int, 
														 texFormatID0:uint, 
														 texFormatID1:uint, 
														 isCube0:Boolean,  
														 isCube1:Boolean, 
														 recieveShadow:Boolean, 
														 openAlphaTest:Boolean):Procedure
		{
			var filter:String = Defines.FILTERS[filterIndex];
			var repeat:String = Defines.REPEATS[repeatIndex];
			var texFormat0:String = Defines.TEX_FORMATS[texFormatID0];
			var texFormat1:String = Defines.TEX_FORMATS[texFormatID1];
			var texFlag0:String = isCube0 ? "cube" : "2d"; 
			var texFlag1:String = isCube1 ? "cube" : "2d"; 
			var mip:String = Defines.MIPS[mipIndex];
			var curIndex:uint = 0;
			var ret:Procedure;
			var findProcedure:Vector.<Procedure> = procedureSet[mat.programCachedID] as Vector.<Procedure>;
			if (findProcedure!=null) {
				ret = findProcedure[curIndex];
				if (ret != null)
					return ret;
			} else {
				findProcedure = new Vector.<Procedure>;
				findProcedure.length = 2;
				procedureSet[mat.programCachedID] = findProcedure;
			}
			
			var shader:Array = [];
			var i:int = 0;
			shader[i++] ="#v0=vUV";
			if (texNum > 0) {
				shader[i++] ="#s0=sTexture0";
				if (texNum == 2) {
					shader[i++] ="#s1=sTexture1";
					shader[i++] ="tex t0, v0.xy, s0 <"+ texFlag0 + ", "+filter+","+repeat+", "+mip+","+texFormat0+">";
					shader[i++] ="tex t1, v0.xy, s1 <"+ texFlag1 + ", "+filter+","+repeat+", "+mip+","+texFormat1+">";
					
					if (texOp == Defines.TEX_OP_MUL) {
						shader[i++] = "mul t0, t0, t1";
					} else if(texOp == Defines.TEX_OP_ADD) {
						shader[i++] = "add t0, t0, t1";
					} else if(texOp == Defines.TEX_OP_SUB) {
						shader[i++] = "sub t0, t0, t1";
					} else if(texOp == Defines.TEX_OP_ALPHA) {
						shader[i++] ="mov t2.x, t0.x";
						shader[i++] ="sub t3, t0, t1";
						shader[i++] ="mul t3, t3, t2.x";
						shader[i++] ="add t0, t3, t1";
					}
				} else {
					shader[i++] ="tex t0, v0.xy, s0 <"+ texFlag0 + ", "+filter+","+repeat+", "+mip+","+texFormat0+">";
				}
				
				shader[i++] = "mul t0.w, t0.w, c1.w";
				if (openAlphaTest) {
					shader[i++] = "sub t2.x, t0.w, c0.w";
					shader[i++] = "kil t2.x";
				}
			}
			
			/*
			float4 ps_main( PS_INPUT Input ) : COLOR0
			{
				float2 TexSize 		= float2(512.0, 512.0);
				float2  upLeftUV 	= float2(Input.Texcoord.x - 1.0/TexSize.x, Input.Texcoord.y - 1.0/TexSize.y);
				float4  bkColor 	= float4(0.5, 0.5, 0.5, 1.0);
				float4  curColor    = tex2D(baseMap, Input.Texcoord);
				float4  upLeftColor = tex2D(baseMap, upLeftUV );
				float4  delColor 	= curColor - upLeftColor;
				float  h 			= 0.3 * delColor.x + 0.59 * delColor.y + 0.11* delColor.z;
				float4  outColor 	= float4(h, h, h, 0.0)+ bkColor;
				return  outColor;
			}
			*/
			/*
			float4 ps_main( PS_INPUT Input ) : COLOR0
			{      
				float3 fvLightDirection = normalize( Input.LightDirection );
				float3 fvNormal         = normalize( Input.Normal );
				float  fNDotL           = dot( fvNormal, fvLightDirection ); 
				
				float4 fvBaseColor      = tex2D( baseMap, Input.Texcoord );
				float4 fvTotalAmbient   = fvAmbient * fvBaseColor; 
				float4 fvTotalDiffuse   = fvDiffuse * fNDotL * fvBaseColor; 
				
				return( saturate( fvTotalAmbient + fvTotalDiffuse + fvTotalSpecular ) );
				
			} 
			*/
			shader[i++] = "#v1=vNormal";	// (x, y, z, 1)
			shader[i++] = "#c0=cLightDir";	// (x, y, z, 0)
			shader[i++] = "#c1=cDiffuse";	// (r, g, b, 0)
			shader[i++] = "#c2=cAmbient";	// (r, g, b, 0)

			shader[i++] = "nrm t2.xyz, v1.xyz"; // vNormal
			//shader[i++] = "mov t3.xyz, c0.xyz"; // cLightDir
			//shader[i++] = "nrm t3.xyz, t3.xyz"; // cLightDir
			shader[i++] = "dp3 t4.w, t2.xyz, c0.xyz"; // fNDotL
						
			if (texNum > 0) {
				shader[i++] = "mul t1.xyz, t0.xyz, c2.xyz"; // totalAmbient
				shader[i++] = "mul t2.xyz, t0.xyz, c1.xyz";
				shader[i++] = "mul t2.xyz, t2.xyz, t4.www";
			} else {
				shader[i++] = "mov t1.xyz, c2.xyz"; // totalAmbient
				shader[i++] = "mov t2.xyz, c1.xyz";
			}
			
			//shader[i++] = "add t0.xyz, t1.xyz, t2.xyz";
			//shader[i++] = "sat t0.xyz, t0.xyz";
			
			//if (texNum == 0) {
			//	shader[i++] = "sge t0.w, t1.x, t1.x";
			//}
			
			if (recieveShadow) {
				
				shader[i++] = "#v5=vShadowUV";
				shader[i++] = "#c5=cShadowConstants";
				shader[i++] = "#c6=cShadowDist";
				if (texNum==2) { 
					shader[i++] = "#s2=sShadowMap";
					shader[i++] = "tex t1.xy, v5, s2 <2d, clamp, near, nomip>";
				} else {
					shader[i++] = "#s1=sShadowMap";
					shader[i++] = "tex t1.xy, v5, s1 <2d, clamp, near, nomip>";
				}
				
				shader[i++] = "mov t1.zw, v5.zz";
				shader[i++] = "dp3 t1.x, t1.xyz, c5.xyz";
				shader[i++] = "sub t1.y, c6.x, t1.z";
				shader[i++] = "mul t1.y, t1.y, c6.y";
				shader[i++] = "sat t1.xy, t1.xy";
				shader[i++] = "mul t1.x, t1.x, t1.y";
				shader[i++] = "sub t1.x, c6.z, t1.x";
				shader[i++] = "max t1.x, t1.x, c6.w";
				shader[i++] = "mul o0, t0, t1.x";
			} else {
				shader[i++] = "add t0.xyz, t1.xyz, t2.xyz";
				shader[i++] = "mov o0, t0";
			}
			
			ret = new Procedure(shader);
			findProcedure[curIndex] = ret;
			return ret;
		}
		
		static FlashGE function getEffectFramentProcedure(mat:Material,
														  texNum:int, texOp:int, 
														  filterIndex:int, 
														  repeatIndex:int, 
														  mipIndex:int, 
														  openNormalAlpha:Boolean,
														  doubleAlpha:Boolean, 
														  useVertDiffuse:Boolean, 
														  texFormatID0:uint, 
														  texFormatID1:uint, 
														  isCube0:Boolean, 
														  isCube1:Boolean, 
														  isGrayColor:Boolean, 
														  replaceAlpha:int):Procedure
		{
			var filter:String = Defines.FILTERS[filterIndex];
			var repeat:String = Defines.REPEATS[repeatIndex];
			var texFormat0:String = Defines.TEX_FORMATS[texFormatID0];
			var texFormat1:String = Defines.TEX_FORMATS[texFormatID1];
			var texFlag0:String = isCube0 ? "cube" : "2d"; 
			var texFlag1:String = isCube1 ? "cube" : "2d"; 
			var mip:String = Defines.MIPS[mipIndex];
			var curIndex:uint = doubleAlpha ? 1 : 0;
			var ret:Procedure;
			var findProcedure:Vector.<Procedure> = procedureSet[mat.programCachedID] as Vector.<Procedure>;
			if (findProcedure!=null) {
				ret = findProcedure[curIndex];
				if (ret != null)
					return ret;
			} else {
				findProcedure = new Vector.<Procedure>;
				findProcedure.length = 2;
				procedureSet[mat.programCachedID] = findProcedure;
			}
			
			var shader:Array = [];
			var i:int = 0;
			
			if (texNum == 2) {
				shader[i++] ="#v0=vUV";
				shader[i++] ="#v1=vUVOrg";
				shader[i++] ="#s0=sTexture0";
				shader[i++] ="#s1=sTexture1";
				shader[i++] ="#c0=cColor";
				shader[i++] ="#c1=cFactors"; 
				shader[i++] ="tex t0, v0, s0 <"+ texFlag0 + ", "+filter+","+repeat+", "+mip+","+texFormat0+">";
				shader[i++] ="tex t1, v1, s1 <"+ texFlag1 + ", "+filter+","+repeat+", "+mip+","+texFormat1+">";
				if (texOp == Defines.TEX_OP_MUL) {
					shader[i++] = "mul t0, t0, t1";
				} else if(texOp == Defines.TEX_OP_ADD) {
					shader[i++] = "add t0, t0, t1";
				} else if(texOp == Defines.TEX_OP_SUB) {
					shader[i++] = "sub t0, t0, t1";
				} else if(texOp == Defines.TEX_OP_ALPHA) {
					shader[i++] ="mov t2.x, t0.w";
					shader[i++] ="sub t3, t0, t1";
					shader[i++] ="mul t3, t3, t2.x";
					shader[i++] ="add t0, t3, t1";
				}
				
			} else {
				shader[i++] ="#v0=vUV";
				//shader[i++] ="#v1=vUVOrg";
				shader[i++] ="#s0=sTexture0";
				shader[i++] ="#c0=cColor";
				shader[i++] ="#c1=cFactors";
				shader[i++] ="tex t0, v0, s0 <"+ texFlag0 + ", "+filter+","+repeat+", "+mip+","+texFormat0+">";
			}
			
			if (replaceAlpha != Defines.REPLACE_ALPHA_DEFAULT) {
				shader[i++] ="mov t5.xyz, t0.xyz";
			}
			
			if (isGrayColor) {
				shader[i++] ="mul t0, t0, c0";
				shader[i++] ="add t1.xyz, t0.xxx, t0.yyy";
				shader[i++] ="add t0.xyz, t1.xyz, t0.zzz";
			} else {
				shader[i++] ="mul t0, t0, c0";
			}
			
			if (replaceAlpha != Defines.REPLACE_ALPHA_DEFAULT) {
				shader[i++] ="mul t5.xyz, t5.xyz, c0.www";
			}	
			
			if (useVertDiffuse) {
				shader[i++] ="mul t0, t0, v6";
				
				if (replaceAlpha != Defines.REPLACE_ALPHA_DEFAULT) {
					shader[i++] ="mul t5.xyz, t5.xyz, v6.www";
				}
			}
			
			if (openNormalAlpha) {
				//	
				shader[i++] ="#v3=vViewVector";
				shader[i++] ="nrm t2.xyz, v3.xyz";
				shader[i++] ="nrm t3.xyz, v2.xyz";
				
				shader[i++] ="mov t2.w, v3.w";
				shader[i++] ="mov t3.w, v2.w";
				
				shader[i++] ="dp3 t4.w, t2.xyz, t3.xyz";
				shader[i++] ="add t4.w, t4.w, t2.w";
				shader[i++] ="abs t4.w, t4.w";
				shader[i++] ="pow t4.w, t4.w, c1.x";
				shader[i++] ="mul t4.w, t4.w, c1.y";
				shader[i++] ="sat t4.w, t4.w";
				shader[i++] ="mul t0.w, t4.w, t0.w";
			}
			
			if (doubleAlpha) {
				shader[i++] ="slt t2.x, t0.w, c1.w";
				shader[i++] ="mul t0.w, t0.w, t2.x";
			} else {
				shader[i++] ="sub t2.x, t0.w, c1.w";
				shader[i++] ="kil t2.x";
			}
			
			if (replaceAlpha == Defines.REPLACE_ALPHA_R) {
				shader[i++] = "mov t0.w, t5.x";
			} else if (replaceAlpha == Defines.REPLACE_ALPHA_G) {
				shader[i++] = "mov t0.w, t5.y";
			} else if (replaceAlpha == Defines.REPLACE_ALPHA_B) {
				shader[i++] = "mov t0.w, t5.z";
			}
			
			shader[i++] = "mov o0, t0";
			ret = new Procedure(shader);
			findProcedure[curIndex] = ret;
			return ret;
		}
		
		static FlashGE function getPixelShaderHSL(shadow:Boolean, texFormatID:int):Procedure
		{
			
			var texFormat:String = Defines.TEX_FORMATS[texFormatID];
			var array:Array = [			
				"#v0=vUV",
				"#s0=sTexture0",
				"#c0=cHSLC0",
				"#c1=cHSLC1",
				"#c2=cHSLC2",
				"#c3=cHSLC3",
				"tex t0, v0, s0 <2d, linear,clamp, miplinear,"+texFormat+">",
				"mul t0, t0, c3",
				"min t4.x, t0.x, t0.y",
				"min t4.x, t4.x, t0.z",
				"max t4.y, t0.x, t0.y",
				"max t4.y, t4.y, t0.z",
				"sub t4.z, t4.y, t4.x",
				"add t5.z, t4.x, t4.y",
				"div t5.z, t5.z, c0.y",
				"add t2.x, t4.y, t4.x",
				"div t2.x, t4.z, t2.x",
				"sub t2.y, c0.y, t4.y",
				"sub t2.y, t2.y, t4.x",
				"div t2.y, t4.z, t2.y",
				"slt t5.y, t5.z, c0.z",
				"sge t3, t5.z, c0.z",
				"mul t5.y, t5.y, t2.x",
				"mul t3, t3, t2.y",
				"add t5.y, t5.y, t3",
				"seq t2.xyz, t0.xyz, t4.y",
				"sne t2.w, t2.x, t2.y",
				"min t2.y, t2.y, t2.w",
				"sne t2.w, t2.x, t2.z",
				"min t2.z, t2.z, t2.w",
				"sne t2.w, t2.y, t2.z",
				"min t2.z, t2.z, t2.w",
				"sub t3.xyz, t0.yzx, t0.zxy",
				"div t3.xyz, t3.xyz, t4.z",
				"mov t3.w, c0.x",
				"add t3.y, t3.y, c0.y",
				"add t3.z, t3.z, c0.y",
				"add t3.z, t3.z, c0.y",
				"mov t3.w, c0.y",
				"add t3.w, t3.w, c0.y",
				"add t3.w, t3.w, c0.y",
				"div t3.xyz, t3.xyz, t3.w",
				"mul t2.xyz, t2.xyz, t3.xyz",
				"add t5.x, t2.x, t2.y",
				"add t5.x, t5.x, t2.z",
				"slt t2.x, t5.x, c0.x",
				"add t5.x, t5.x, t2.x",
				"mov t5.w, t0.w",
				
				"mul t5.xyz, t5.xyz, c2.xyz",
				//----------
				"add t1.x, c0.w, t5.y",
				"mul t1.x, t1.x, t5.z",
				"mul t1.y, t5.z, t5.y",
				"sub t1.y, t5.y, t1.y",
				"add t1.y, t1.y, t5.z",
				"slt t2.y, t5.z, c0.z",
				"mul t1.x, t1.x, t2.y",
				"sge t2.y, t5.z, c0.z",
				"mul t1.y, t1.y, t2.y",
				"add t2.y, t1.x, t1.y",
				"mul t2.x, c0.y, t5.z",
				"sub t2.x, t2.x, t2.y",
				"sub t2.z, t2.y, t2.x",
				"mov t3.y, t5.x",
				"sub t3.x, t5.x, c1.z",
				"slt t3.w, t3.x, c0.x",
				"add t3.x, t3.x, t3.w",
				"sub t3.z, t5.x, c1.x",
				"max t3.z, t3.z, c0.x",
				"sge t0.xyz, t3.xyz, c0.z",
				"slt t1.xyz, t3.xyz, c1.z",
				"min t0.xyz, t0.xyz, t1.xyz",
				"sub t1.xyz, c1.z, t3.xyz",
				"mul t1.xyz, t1.xyz, t2.z",
				"div t1.xyz, t1.xyz, c1.y",
				"add t1.xyz, t1.xyz, t2.x",
				"mul t0.xyz, t0.xyz, t1.xyz",
				"mul t1.xyz, t2.z, t3.xyz",
				"div t1.xyz, t1.xyz, c1.y",
				"add t1.xyz, t1.xyz, t2.x",
				"slt t4.xyz, t3.xyz, c1.y",
				"mul t1.xyz, t1.xyz, t4.xyz",
				"add t0.xyz, t0.xyz, t1.xyz",
				"sge t4.xyz, t3.xyz, c1.y",
				"slt t1.xyz, t3.xyz, c0.z",
				"min t1.xyz, t4.xyz, t1.xyz",
				"mul t1.xyz, t1.xyz, t2.y",
				"add t0.xyz, t0.xyz, t1.xyz",
				"sge t1.xyz, t3.xyz, c1.z",
				"mul t1.xyz, t1.xyz, t2.x",
				"add t0.xyz, t0.xyz, t1.xyz"
			];
			var i:int = array.length;
			if (shadow) {
				array[i++] = "#v1=vShadowUV";
				array[i++] = "#c4=cShadowConstants";
				array[i++] = "#c5=cShadowDist";
				array[i++] = "#s1=sShadowMap";
				
				array[i++] = "mov t1.zw, v1.zz";
				array[i++] = "tex t1.xy, v1, s1 <2d, clamp, near, nomip>";
				array[i++] = "dp3 t1.x, t1.xyz, c4.xyz";
				array[i++] = "sub t1.y, c5.x, t1.z";
				array[i++] = "mul t1.y, t1.y, c5.y";
				array[i++] = "sat t1.xy, t1.xy";
				array[i++] = "mul t1.x, t1.x, t1.y";
				array[i++] = "sub t1.x, c5.z, t1.x";
				array[i++] = "max t1.x, t1.x, c5.w";
				
				array[i++] = "mul o0, t0, t1.x";
			} else {
				
				array[i++] = "mov o0, t0";
			}
			
			return new Procedure(array);
		}
		
		static FlashGE function getPixelShaderNoneHSL(texFormatID:int):Procedure
		{
			var texFormat:String = Defines.TEX_FORMATS[texFormatID];
			var array:Array = [						
				"#v0=vUV",
				"#s0=sTexture0",
				"tex o0, v0.xy, s0 <2d, linear,clamp, miplinear, "+texFormat+">"
			];
			return new Procedure(array);
		}
		
		static FlashGE const vertexShaderHSL:Procedure =
			new Procedure([
				"#v0=vUV", 
				"#a0=aUV", 
				"#c0=cUVScale",
				"#c1=cProjMatrix",
				"m44 o0, i0, c1",
				"mul t0.xy, a0.xy, c0.xy",
				"add t0.xyzw, t0.xyxy, c0.zwzw",
				"mov v0, t0"
			]);
		
		static FlashGE const vertexShaderHSLGrid:Procedure =
			new Procedure([
				"#v0=vUV",
				"#a0=aUV",
				"#c0=cUVScale",
				"#c1=cDecode01", // gridX - cameraX, gridY + cameraY
				"#c2=cDecode02", // screenHalfWidth, screenHalfHeight,0.5,1
				"add t0, i0, c1",
				"div o0, t0, c2",
				"mul t0.xy, a0.xy, c0.xy",
				"add v0.xyzw, t0.xyxy, c0.zwzw"
			]);
		
		static FlashGE const vertexShaderHSLGridSimple:Procedure =
			new Procedure([
				"#v0=vUV",
				"#a0=aUV",
				"#c1=cDecode01", // gridX - cameraX, gridY + cameraY
				"#c2=cDecode02", // screenHalfWidth, screenHalfHeight,0.5,1
				"add t0, i0, c1",
				"div o0, t0, c2",
				"mov v0, a0.xyxy"
			]);
		
		static FlashGE const vertextShaderSkyBox:Procedure = 
			new Procedure([
				"#v0=vUV", 
				"#a0=aUV", 
				"#c0=cProjMatrix",
				"m44 o0, i0, c0",
				"mov t0, a0",
				"mov v0, t0"
			]);
		
		static FlashGE const pixelShadowShaderSkyBox:Procedure =
			new Procedure([
				"#v0=vUV",
				"#s0=sTexture0",
				"tex t0, v0, s0 <2d, linear, repeat, miplinear, dxt1>",
				"mov o0, t0",
			]);
		
		static FlashGE const vertexShaderColor:Procedure = 
			new Procedure([
				"#c0=cProjMatrix",
				"m44 o0, i0, c0"
			]);
		
		static FlashGE const pixelShaderColor:Procedure = 
			new Procedure([
				"#c0=cColor",
				"mov o0, c0"
			]);
		
		static FlashGE const vertexShadowShaderHSL:Procedure =
			new Procedure([
				"#v0=vUV", 
				"#a0=aUV", 
				"#c0=cUVScale",
				"#c1=cProjMatrix",
				"#c2=cProjMatrix1",
				"#c3=cProjMatrix2",
				"#c4=cProjMatrix3",
				"#c5=cShadowMapUVProjection",
				"dp4 o0.x, i0, c1",
				"dp4 o0.y, i0, c2",
				"dp4 o0.z, i0, c3",
				"dp4 o0.w, i0, c4",
				"mul t0.x, a0.x, c0.x",
				"mul t0.y, a0.y, c0.y",
				"mov t0.zw, c0.zw",
				"mov v0, t0",
				
				"#v1=vShadowUV",
				"m34 v1.xyz, i0, c5",
				"mov v1.w, i0.w"
			]);
		
		static FlashGE function getPixelShadowShaderNoneHSL(texFormatID:int):Procedure
		{
			var texFormat:String = Defines.TEX_FORMATS[texFormatID];
			var array:Array = [						
				"#v0=vUV",				
				"#v1=vShadowUV",
				"#c0=cHSLC3",
				"#c1=cShadowConstants",
				"#c2=cShadowDist",
				"#s1=sShadowMap",
				"#s0=sTexture0",
				"tex t1, v0, s0 <2d, linear, clamp, miplinear, "+texFormat+">",
				"mul t1, t1, c0",
				
				"mov t0.zw, v1.zz",
				"tex t0.xy, v1, s1 <2d, clamp, near, nomip>",
				"dp3 t0.x, t0.xyz, c1.xyz",
				"sub t0.y, c2.x, t0.z",
				"mul t0.y, t0.y, c2.y",
				"sat t0.xy, t0.xy",
				"mul t0.x, t0.x, t0.y",
				"sub t0.x, c2.z, t0.x",
				"max t0.x, t0.x, c2.w",
				"mul o0, t1, t0.x",
			];
			return new Procedure(array);
		}
		
		static FlashGE function getShadowMapVShader():Procedure {
			var shader:Procedure = Procedure.compileFromArray([
				"#v0=vShadowUV",
				"m34 v0.xyz, i0, c0",
				"mov v0.w, i0.w"
			], "ShadowMapVertex");
			shader.assignVariableName(VariableType.CONSTANT, 0, "cShadowMapUVProjection", 3);
			return shader;
		}
		
		static FlashGE function getShadowMapFShader():Procedure {
			
			return Procedure.compileFromArray([
				"#v0=vShadowUV",
				"#c0=cShadowConstants",
				"#c1=cShadowDist",
				"#s0=sShadowMap",
				
				"mov t0.zw, v0.zz",
				"tex t0.xy, v0, s0 <2d,clamp,near,nomip>",
				"dp3 t0.x, t0.xyz, c0.xyz",
				"sub t0.y, c1.x, t0.z",
				"mul t0.y, t0.y, c1.y",
				"sat t0.xy, t0.xy",
				"mul t0.x, t0.x, t0.y",
				"sub t0.x, c1.z, t0.x",
				"mul o0, i0, t0.x"
			], "ShadowMapFragment");
		}
		
		static FlashGE function getBasicVShader(shadow:Boolean):Procedure
		{
			var shader:Procedure = Procedure.compileFromArray([				
				"m44 o0, i0, c0",
				"mov t0, i1",
			], "BasicVShader");
			
			shader.assignVariableName(VariableType.CONSTANT, 0, "cProjMatrix", 4);
			
			return shader;
		}
		
		
		static FlashGE function getWingUVShader(useDistortion:Boolean, fixAlpha:Boolean):Procedure
		{
			var shader:Array = [];
			var i:int = 0;
			
			shader[i++] = "#c0=cUVTrans"; // v0, v1, t, 0
			if (useDistortion) {			
				shader[i++] = "#v0=vUV"; 
				shader[i++] = "#a0=aUV";
				
				shader[i++] = "mov t1, c0";
				shader[i++] = "mul t1.xy, t1.xy, t1.zz";
				shader[i++] = "add t0.xy, t1.xy, a0.xy";
				shader[i++] = "mov t0.zw, a0.zw";
				shader[i++] = "mov v0, t0";
				
				//shader[i++] = "mov v0, a0";
				
				shader[i++] = "#c1=cUVTrans2";// x1y1, x2y2
				shader[i++] = "#v2=vUV2"; 
				shader[i++] = "#v3=vUV3"; 
				shader[i++] = "mov t1, c0";
				shader[i++] = "mul t1, t1.zzzz, c1";
				shader[i++] = "add v2.xy, t1.xy, t0.xy";
				shader[i++] = "mov v2.zw, t0.zw";
				
				shader[i++] = "add v3.xy, t1.zw, t0.xy";
				shader[i++] = "mov v3.zw, t0.zw";
				if (true == fixAlpha) {
					shader[i++] = "#v4=vUV4"; 
					shader[i++] = "mov v4, a0";
				}
			} else {
				shader[i++] = "#v0=vUV"; 
				shader[i++] = "#a0=aUV";
				shader[i++] = "mov t1, c0";
				shader[i++] = "mul t1.xy, t1.xy, t1.zz";
				shader[i++] = "add v0.xy, t1.xy, a0.xy";
				shader[i++] = "mov v0.zw, a0.zw";
				if (true == fixAlpha) {
					shader[i++] = "#v2=vUV2"; 
					shader[i++] = "mov v2, a0";
				}
			}
			var ret:Procedure = new Procedure(shader, "wingUVShader");
			return ret;
		}
		
		static FlashGE function getWingVShader():Procedure
		{
			var shader:Procedure = Procedure.compileFromArray([
				"#v5=vAlpha",
				"mov t0, i0",
				"dp3 t2.w, t0.xyz, t0.xyz",
				"sqt t2.w, t2.w",
				
				"div t1.y, t2.w, c4.z",
				"mov t1.w, t2.w",
				"div t1.w, t1.w, c4.y",
				"add t1.w, t1.w, c5.w",
				"sin t1.w, t1.w",
				"mul t1.w, t1.w, c4.x",
				"mul t1.w, t1.w, t1.y",
				"add t0.x, t0.x, t1.w",
				
				"div t1.y, t2.w, c5.z",
				"mov t1.w, t2.w",
				"div t1.w, t1.w, c5.y",
				"add t1.w, t1.w, c5.w",
				"sin t1.w, t1.w",
				"mul t1.w, t1.w, c5.x",
				"mul t1.w, t1.w, t1.y",
				"add t0.y, t0.y, t1.w",
				
				"div t1.y, t2.w, c6.z",
				"mov t1.w, t2.w",
				"div t1.w, t1.w, c6.y",
				"add t1.w, t1.w, c5.w",
				"sin t1.w, t1.w",
				"mul t1.w, t1.w, c6.x",
				"mul t1.w, t1.w, t1.y",
				"add t0.z, t0.z, t1.w",
				"sge t0.w, t0.w, t0.w",
				
				"mov t1.w, c4.w",
				"div t1.w, t1.w, t2.w",
				"sge t1.x, t1.w, t1.w",
				"min t1.x, t1.x, t1.w",
				"sge t1.w, t1.w, t1.w",
				"mov v5, t1.xxxx",
				"m44 o0, t0, c0"
			], "WingVShader");
			
			shader.assignVariableName(VariableType.CONSTANT, 0, "cProjMatrix", 4);
			shader.assignVariableName(VariableType.CONSTANT, 4, "cWing0", 1); // x
			shader.assignVariableName(VariableType.CONSTANT, 5, "cWing1", 1); // y
			shader.assignVariableName(VariableType.CONSTANT, 6, "cWing2", 1); // z
			return shader;
		}	
		
		static FlashGE function getWingFShader(fixAlpha:Boolean, 
											   texFormat0:int, 
											   texFormat1:int, 
											   useDistortion:Boolean, repeatIndex:int):Procedure 
		{
			var i:int = 0;
			var shader:Array = [];
			var repeat:String = Defines.REPEATS[repeatIndex];
			shader[i++] = "#v0=vUV";
			shader[i++] = "#v5=vAlpha";
			shader[i++] = "#s0=sTexture0";				
			shader[i++] = "#c0=cColor";
			if (useDistortion) {
				
				shader[i++] = "#s1=sTexture1";
				shader[i++] = "#c1=cDistortion";
				shader[i++] = "#v2=vUV2";
				shader[i++] = "#v3=vUV3";
				
				shader[i++] = "mov t1.x, c1.x";
				shader[i++] = "sge t1.x, t1.x, t1.x";
				
				shader[i++] = "tex t2, v2, s1 <2d,repeat,linear, miplinear," + Defines.TEX_FORMATS[texFormat1] + ">";
				shader[i++] = "add t2.x, t2.x, t2.x";
				shader[i++] = "sub t1.y, t2.x, t1.x";
				
				shader[i++] = "tex t2, v3, s1 <2d,repeat,linear, miplinear," + Defines.TEX_FORMATS[texFormat1] + ">";
				shader[i++] = "add t2.x, t2.x, t2.x";
				shader[i++] = "sub t1.z, t2.x, t1.x";
				
				shader[i++] = "mul t1.xy, t1.yz, c1.xy";
				shader[i++] = "add t1.x, t1.x, t1.y";
				
				shader[i++] = "mul t2.x, v0.y, c1.z";
				shader[i++] = "add t2.x, t2.x, c1.w";
				shader[i++] = "mul t1.x, t1.x, t2.x";
				shader[i++] = "add t2.xy, t1.xx, v0.xy";
				
				//shader[i++] = "tex t1, t2.xy, s0 <2d, clamp, linear, miplinear," + Defines.TEX_FORMATS[texFormat0] + ">";
				shader[i++] = "tex t0, t2.xy, s0 <2d, "+repeat+", linear, miplinear," + Defines.TEX_FORMATS[texFormat0] + ">";
				
				shader[i++] = "mul t0.xyz, t0.xyz, t0.www";
				if (fixAlpha) {
					shader[i++] = "#v4=vUV4";
					shader[i++] = "tex t1, v4, s0 <2d,"+repeat+",linear, miplinear," + Defines.TEX_FORMATS[texFormat0] + ">";
					shader[i++] = "mov t0.w, t1.w";
				}
				shader[i++] = "mul t0, t0, c0";
				shader[i++] = "mul t0.w, t0.w, v5.x";
				
			} else {
				shader[i++] = "tex t0, v0.xy, s0 <2d,"+repeat+",linear, miplinear," + Defines.TEX_FORMATS[texFormat0] + ">";
				shader[i++] = "mul t0, t0, c0";
				
				if (fixAlpha) {
					shader[i++] = "#v2=vUV2";
					shader[i++] = "tex t1, v2, s0 <2d,"+repeat+",linear, miplinear," + Defines.TEX_FORMATS[texFormat0] + ">";
					shader[i++] = "mul t0.w, t1.w, v5.x";
				} else {
					shader[i++] = "mul t0.w, t0.w, v5.x";
				}
			}
			
			shader[i++] = "mov o0, t0";
			
			var ret:Procedure = new Procedure(shader, "WingFShader");
			return ret;
		}
		
		static FlashGE function getBasicFShader(shadow:Boolean, texFormat:int):Procedure {
			var shader:Procedure = Procedure.compileFromArray([
				"#v0=vUV",
				"#s0=sTexture0",
				"#c0=cColor",
				"tex t0, v0, s0 <2d,clamp,linear, miplinear," + Defines.TEX_FORMATS[texFormat] + ">",
				"mul t0, t0, c0",
				"mov o0, t0"
			], "BasicFShader");
			
			return shader;
		}
		
	}
}