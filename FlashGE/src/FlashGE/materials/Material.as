/*@author lye 123*/
package FlashGE.materials 
{
	import flash.display3D.Context3D;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Context3DTriangleFace;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.Program3D;
	import flash.display3D.VertexBuffer3D;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Colour;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.effects.EffectUnitBase;
	import FlashGE.effects.SpriteUnit;
	import FlashGE.materials.compiler.Linker;
	import FlashGE.materials.compiler.Procedure;
	import FlashGE.materials.compiler.VariableType;
	import FlashGE.objects.Entity;
	import FlashGE.objects.Skin;
	import FlashGE.objects.Surface;
	import FlashGE.render.Camera3D;
	import FlashGE.render.RenderUnit;
	import FlashGE.render.Renderer;
	import FlashGE.resources.Geometry;
	import FlashGE.resources.LoaderMgr3D;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;
	
	public class Material
	{
		static public var totalProgramCatchedList:Dictionary = new Dictionary;
		
		FlashGE var textureFiles:Vector.<TextureFile>;
		FlashGE var textureFilesCount:int = 0;
		FlashGE var refCount:int = 0;
		
		FlashGE var programCachedID:String;
		
		FlashGE var openDoubleAlpha:Boolean = false;
		FlashGE var alpha:Number = 1;
		private var red:Number = 1;
		private var green:Number = 1;		
		private var blue:Number = 1;
		private var culling:String = Context3DTriangleFace.FRONT;
		private var useLight:Boolean = false;
		
		private var uvUOffset:Number = 0.5;
		private var uvVOffset:Number = 0.5;
		private var uv2UOffset:Number = 0.5;
		private var uv2VOffset:Number = 0.5;
		private var uvUScale:Number = 1;
		private var uvVScale:Number = 1;
		
		private var uvV0:Number = 0;
		private var uvV1:Number = 0;
		private var uvAngle:Number = 0;
		//private var curTime:Number = 0;
		
		private var renderPriority:int = Defines.PRIORITY_OPAQUE;
		private var useVertDiffuse:Boolean = false;
		private var closeUVTrans:Boolean = false;
		private var useUVType:int = Defines.UV_TYPE_TRANS;
		
		private var srcBlendValue:int = Defines.BLEND_ONE;
		private var dstBlendValue:int = Defines.BLEND_ZERO;
		private var replaceAlpha:int = Defines.REPLACE_ALPHA_DEFAULT;
		
		private var texOpIndex:int = 0;
		private var repeatIndex:int = 0;
		private var mipIndex:int = 0;
		private var filterIndex:int = 0;		
		private var alphaTest:Number = 0;		
		private var alphaIntensity:Number = 0;
		private var alphaSharpness:Number = 0;
		
		private var hue:Number = 1;
		private var saturation:Number = 1;
		private var lightness:Number = 1;
		FlashGE var type:int = Defines.MATERIAL_MODEL;
		FlashGE var typeChanged:Boolean = false;
		private var borderPower:Number = 0;
		private var borderColor:Colour = new Colour;
		
		private var recieveShadow:Boolean = false;
		private var isGrayColor:Boolean = false;
		private var waveContext:Vector.<Colour>;
		private var fixWaveAlpha:Boolean = false;
		private var tempTrans:Transform3D;
		private var usedContext3D:Context3D;
		private var getGlobalTimer:Function;
		private var startPlayTimer:int;
		private var curUsedProgramA:ShaderProgram;
		private var curUsedProgramB:ShaderProgram;
		private var loaderMgr:LoaderMgr3D;
		
		public function setStartPlayTimer(v:int):void
		{
			this.startPlayTimer = v;
		}
		
		public function Material()
		{
			this.loaderMgr = LoaderMgr3D.getInstance();
		}
		
		public function setWaveContext(i:int, amplitude:Number, waveLength:Number, attenuation:Number, alphaFactor:Number = 0):void
		{
			waveContext = waveContext || new Vector.<Colour>(5);
			waveContext[0] = waveContext[0] || new Colour;
			waveContext[1] = waveContext[1] || new Colour;
			waveContext[2] = waveContext[2] || new Colour;
			waveContext[3] = waveContext[3] || new Colour;
			waveContext[4] = waveContext[4] || new Colour;
			waveContext[i].r = amplitude;
			waveContext[i].g = waveLength;
			waveContext[i].b = attenuation;
			waveContext[i].a = alphaFactor;
		}
		
		public function setFixWaveAlpha(fix:Boolean):void
		{
			if (this.fixWaveAlpha != fix) {
				this.markProgramDirty();
				this.fixWaveAlpha = fix;
			}
		}
		
		public function setBorderPower(v:Number):void
		{
			this.borderPower = v;
		}
		
		public function setBorderColor(r:Number, g:Number, b:Number):void
		{
			this.borderColor.r = r;
			this.borderColor.g = g;
			this.borderColor.b = b;
		}
		
		private var diffuseR:Number = 0.508235319;
		private var diffuseG:Number = 0.508235319;
		private var diffuseB:Number = 0.508235319;
		
		public function setDiffuseColor(r:Number, g:Number, b:Number):void
		{
			//this.diffuse.setRGB(r, g, b);
			this.diffuseR = r;
			this.diffuseG = g;
			this.diffuseB = b;
		}
		
		//public function setGlossiness(glossiness:Number):void
		//{
		//	this.glossiness = glossiness;
		//}
		
		public function setType(type:int):void
		{
			if (this.type != type) {
				this.typeChanged = true;
				this.type = type;
				this.markProgramDirty();
			}
		}
		
		public function enableUseLight(enable:Boolean):void
		{
			this.useLight = enable;
		}
		
		public function getHUE():Number
		{
			return this.hue;
		}
		
		public function setHUE(hue:Number):void
		{
			this.hue = hue;
		}
		
		public function getSaturation():Number
		{
			return this.saturation;
		}
		
		public function setSaturation(saturation:Number):void
		{
			this.saturation = saturation;
		}		
		
		public function getLightness():Number
		{
			return this.lightness;
		}
		
		public function setLightness(lightness:Number):void
		{
			this.lightness = lightness;
		}
		
		public function setHSL(h:Number, s:Number, l:Number):void
		{
			if ((this.hue == 1 && this.saturation == 1 && this.lightness == 1) || (h != 1 || s != 1 || l != 1)) {
				this.markProgramDirty();
			} else if ((h == 1 && s == 1 && l == 1) || (this.hue != 1 || this.saturation != 1 || this.lightness != 1)) {
				this.markProgramDirty();
			}
			this.hue = h;
			this.saturation = s;
			this.lightness = l;
		}
		
		public function getCulling():String
		{
			return this.culling;
		}
		
		public function setCulling(culling:String):void
		{
			this.culling = culling;
		}
		
		public function getOpenDoubleAlpha():Boolean
		{
			return this.openDoubleAlpha;
		}
		
		public function setOpenDoubleAlpha(open:Boolean):void
		{
			this.openDoubleAlpha = open;
		}
		
		public function setAlpha(alpha:Number):void
		{
			this.alpha = alpha;
		}
		
		public function getAlpha():Number
		{
			return this.alpha;
		}
		
		public function setColor(r:Number, g:Number, b:Number, a:Number):void
		{
			this.red = r;
			this.green = g;
			this.blue = b;
			this.alpha = a;
		}
		
		public function setRed(red:Number):void
		{
			this.red = red;
		}
		
		public function getRed():Number
		{
			return this.red;
		}
		
		public function setGreen(green:Number):void
		{
			this.green = green;
		}
		
		public function getGreen():Number
		{
			return this.green;
		}
		
		public function setBlue(blue:Number):void
		{
			this.blue = blue;
		}
		
		public function getBlue():Number
		{
			return this.blue;
		}
		
		public function setUVContext(uOffset:Number, vOffset:Number, 
									 uScale:Number, vScale:Number, 
									 v0:Number, v1:Number):void
		{
			this.uvUOffset = uOffset;
			this.uvVOffset = vOffset;
			this.uvUScale = uScale;
			this.uvVScale = vScale;
			this.uvV0 = v0;
			this.uvV1 = v1;
		}
		
		public function getUVUOffset():Number
		{
			return this.uvUOffset;	
		}
		
		public function setUVUOffset(u:Number):void
		{
			this.uvUOffset = u;
		}
		
		public function getUVVOffset():Number
		{
			return this.uvVOffset;	
		}
		
		public function setUVVOffset(v:Number):void
		{
			this.uvVOffset = v;
		}
		
		public function getUV2UOffset():Number
		{
			return this.uv2UOffset;	
		}
		
		public function setUV2UOffset(u:Number):void
		{
			this.uv2UOffset = u;
		}
		
		public function getUV2VOffset():Number
		{
			return this.uvVOffset;	
		}
		
		public function setUV2VOffset(v:Number):void
		{
			this.uv2VOffset = v;
		}
		
		public function getUVUScale():Number
		{
			return this.uvUScale;	
		}
		
		public function setUVUScale(u:Number):void
		{
			this.uvUScale = u;
		}
		
		public function getUVVScale():Number
		{
			return this.uvVScale;	
		}
		
		public function setUVVScale(v:Number):void
		{
			this.uvVScale = v;
		}
		
		public function getUVV0():Number
		{
			return this.uvV0;
		}
		
		public function setUVV0(v:Number):void
		{
			this.uvV0 = v;
		}
		
		public function getUVV1():Number
		{
			return this.uvV1;
		}
		
		public function setUVV1(v:Number):void
		{
			this.uvV1 = v;
		}
		
		//public function getCurTime():Number
		//{
		//	return this.curTime;	
		//}
		
		//public function setCurTime(curTime:Number):void
		//{
		//	this.curTime = curTime;	
		//}
		
		public function getUVAngle():Number
		{
			return this.uvAngle;	
		}
		
		public function setUVAngle(angle:Number):void
		{
			this.uvAngle = angle;	
		}
		
		public function getAlphaIntensity():Number
		{
			return this.alphaIntensity;
		}
		
		public function setAlphaIntensity(v:Number):void
		{
			if (v != this.alphaIntensity) {
				this.alphaIntensity = v;
				this.markProgramDirty();
			}
		}
		
		public function getReplaceAlpha(camera:Camera3D):Number
		{
			if (camera.context3DProperties.isOpenGL) {
				return Defines.REPLACE_ALPHA_DEFAULT;
			}
			return this.replaceAlpha;
		}
		
		public function setReplaceAlpha(v:int):void
		{
			if (v != this.replaceAlpha) {
				this.replaceAlpha = v;
				this.markProgramDirty();
			}
		}
		
		public function getAlphaSharpness():Number
		{
			return this.alphaSharpness;
		}
		
		public function setAlphaSharpness(v:Number):void
		{
			if (v!=this.alphaSharpness) {
				this.alphaSharpness = v;
				this.markProgramDirty();
			}
		}
		
		public function getAlphaTest():Number
		{
			return this.alphaTest;
		}
		
		public function setAlphaTest(v:Number):void
		{
			this.alphaTest = v;
		}
		
		public function getRenderPriority():int
		{
			return this.renderPriority;
		}
		
		public function setRenderPriority(v:int):void
		{
			this.renderPriority = v;
		}	
		
		public function getSrcBlend():int
		{
			return this.srcBlendValue;
		}
		
		public function setSrcBlend(v:int):void
		{
			this.srcBlendValue = v;
		}		
		
		public function getDstBlend():int
		{
			return this.dstBlendValue;
		}
		
		public function setDstBlend(v:int):void
		{
			this.dstBlendValue = v;
		}	
		
		public function setBlend(src:int, dst:int):void
		{
			this.srcBlendValue = src;
			this.dstBlendValue = dst;
		}
		
		public function getTexOpIndex():int
		{
			return this.texOpIndex;
		}
		
		public function setTexOpIndex(v:int):void
		{
			if (v != this.texOpIndex) {
				this.texOpIndex = v;
				this.markProgramDirty();
			}
		}
		
		public function getRepeatIndex():int
		{
			return this.repeatIndex;
		}
		
		public function setRepeatIndex(v:int):void
		{
			if (v != this.repeatIndex) {
				this.repeatIndex = v;
				this.markProgramDirty();
			}
		}
		
		public function getMipIndex():int
		{
			return this.mipIndex;
		}
		
		public function setMipIndex(v:int):void
		{
			if (v != this.mipIndex) {
				this.mipIndex = v;
				this.markProgramDirty();
			}
		}
		
		public function getFilterIndex():int
		{
			return this.filterIndex;
		}
		
		public function setFilterIndex(v:int):void
		{
			if (v != this.filterIndex) {
				this.filterIndex = v;
				this.markProgramDirty();
			}
		}
		
		public function getCloseUVTrans():Boolean
		{
			return this.closeUVTrans;
		}
		
		public function setCloseUVTrans(v:Boolean):void
		{
			if (v != this.closeUVTrans) {
				this.closeUVTrans = v;
				this.markProgramDirty();
			}
		}
		
		public function setUseVertDiffuse(v:Boolean):void
		{
			if (v != this.useVertDiffuse) {
				this.useVertDiffuse = v;
				this.markProgramDirty();
			}
		}
		
		public function getUseVertDiffuse():Boolean
		{
			return this.useVertDiffuse;
		}	
		
		public function getUseUVType():int
		{
			return this.useUVType;
		}
		
		public function setUseUVType(v:int):void
		{
			if (v != this.useUVType) {
				this.useUVType = v;
				this.markProgramDirty();
			}
		}
		
		public function setGrayColor(isGray:Boolean):void
		{
			if (isGray != this.isGrayColor) {
				this.isGrayColor = isGray;
				this.markProgramDirty();
			}
		}
		
		public function setRecieveShadow(recieveShadow:Boolean):void
		{
			if (recieveShadow != this.recieveShadow) {
				this.recieveShadow = recieveShadow;
				this.markProgramDirty();
			}
		}
		
		FlashGE function getEffectShaderProgram(surface:Surface, camera:Camera3D, doubleAlpha:Boolean, texNum:int):ShaderProgram
		{
			var curIndex:uint = doubleAlpha ? 1 : 0;
			var programKey:String = surface.materialKey + "_" + curIndex;
			
			var curProgram:ShaderProgram = totalProgramCatchedList[programKey];
			if (curProgram != null) {
				return curProgram;
			}
			
			curProgram = this.getShaderProgram(programKey);
			if (curProgram != null) {
				return curProgram;
			}
			
			var openNormalAlpha:Boolean = isOpenNormalAlpha(camera);
			var isEditor:Boolean = CONFIG::EDITOR;
			if (!camera.context3DProperties.isOpenGL && (surface.object is SpriteUnit) && !isEditor) {
				curProgram = collectSpriteUnitShader(camera, surface.object as EffectUnitBase, doubleAlpha, openNormalAlpha);
			} else {
				
				var vertexLinker:Linker = new Linker(Context3DProgramType.VERTEX);
				var positionVar:String = "aPosition";
				vertexLinker.declareVariable(positionVar, VariableType.ATTRIBUTE);
				
				if (surface.object.positionProcedure != null) {
					surface.object.appendPositionTransformProcedure(vertexLinker);
					positionVar = "tTransformedPosition";
				}
				
				var proc:Procedure = Procedures.getEffectVertexProcedure(openNormalAlpha);
				vertexLinker.addProcedure(proc);
				vertexLinker.setInputParams(proc, positionVar);
				
				if (closeUVTrans) {
					vertexLinker.addProcedure(Procedures.uvProcedure);
				} else {
					
					if(useUVType == Defines.UV_TYPE_FRAME) {
						vertexLinker.addProcedure(Procedures.uvFrameProcedure);
					} else {
						vertexLinker.addProcedure(Procedures.uvTransProcedure);
					}
				}
				
				if (this.useVertDiffuse) {
					vertexLinker.declareVariable("aVertDiffuse", VariableType.ATTRIBUTE);
					vertexLinker.addProcedure(Procedures.vertDiffuseProcedure, "aVertDiffuse");
				}
				
				var texFormatID0:int = texNum > 0 ? this.textureFiles[0].getFormatID() : 0;
				var texFormatID1:int = texNum > 1 ? this.textureFiles[1].getFormatID() : 0;
				var isCube0:Boolean = texNum > 0 ? this.textureFiles[0].isCube() : false;
				var isCube1:Boolean = texNum > 1 ? this.textureFiles[1].isCube() : false;
				
				var fragmentLinker:Linker = new Linker(Context3DProgramType.FRAGMENT);
				proc = Procedures.getEffectFramentProcedure(this, 
					this.textureFilesCount,this.texOpIndex,
					this.filterIndex, this.repeatIndex, 
					this.mipIndex, openNormalAlpha, 
					doubleAlpha, this.useVertDiffuse, 
					texFormatID0, texFormatID1,
					isCube0, isCube1,
					this.isGrayColor,
					this.getReplaceAlpha(camera));
				
				fragmentLinker.addProcedure(proc);	
				fragmentLinker.varyings = vertexLinker.varyings;
				
				curProgram = new ShaderProgram(vertexLinker, fragmentLinker);		
			}
			totalProgramCatchedList[programKey] = curProgram;
			
			return curProgram;
		}
		
		public function markProgramDirty():void
		{
			this.programCachedID = null
			this.curUsedProgramA = null;
			this.curUsedProgramB = null;
		}
		
		static public function createMaterial(vv:String = ""):Material
		{
			var ret:Material = Pool.getInstance().popMaterial(vv);
			ret.contructor();
			ret.addRef();
			return ret;
		}
		
		public function setTexture(index:int, curTexture:TextureFile):void
		{			
			if (curTexture != null) {
				curTexture.addRef();
				if (index >= this.textureFiles.length) {
					this.textureFiles.push(curTexture);
					textureFilesCount++;
					this.markProgramDirty();
				} else {
					var preTexture:TextureFile = this.textureFiles[index];
					if (preTexture != null) {
						preTexture.release();
					}
					this.textureFiles[index] = curTexture;
				}
			} else {
				if (index < this.textureFiles.length) {
					this.textureFiles[index].release();
					this.textureFiles.splice(index, 1);
					textureFilesCount--;
					this.markProgramDirty();
				}
			}
		}
		
		public function setTextureByFileName(index:int, fileName:String):void
		{
			if (fileName.length > 0) {
				var tex:TextureFile = TextureFile.createFromFile(fileName, true);
				this.setTexture(index, tex);
				tex.release();
			}
		}
		
		public function getTexture(index:int):TextureFile
		{
			return this.textureFiles[index];
		}
		
		public function getTextures():Vector.<TextureFile>
		{
			return this.textureFiles;
		}
		
		FlashGE function collectWater(camera:Camera3D, surface:Surface, geometry:Geometry):int 
		{
				return 0;
			}
			
		FlashGE function collectColor(camera:Camera3D, surface:Surface, geometry:Geometry):int 
		{
			var context3D:Context3D = camera.context3D;
			var object:Entity = surface.object;
			var positionBuffer:VertexBuffer3D = geometry.getVertexBuffer(Defines.POSITION, context3D);
			var uvBuffer:VertexBuffer3D = geometry.getVertexBuffer(Defines.TEXCOORDS[0], context3D);
			
			if (positionBuffer == null || uvBuffer == null) {				
				return 0;
			}
			
			var texFormatID:int = textureFiles[0].getFormatID();
			var programIndex:int = texFormatID;
			if (this.programCachedID == null || surface.materialKey == null) {
				calcSurfaceKey(camera, surface, String(programIndex));
			}
			
			if (this.curUsedProgramA == null) {
				this.curUsedProgramA = totalProgramCatchedList[surface.materialKey];
			}
				
			if (curUsedProgramA == null) {
					
					var positionVar:String = "aPosition";
					var vertexLinker:Linker = new Linker(Context3DProgramType.VERTEX);
					
					vertexLinker.declareVariable(positionVar, VariableType.ATTRIBUTE);
					
					if (surface.object.positionProcedure != null) {
						surface.object.appendPositionTransformProcedure(vertexLinker);
						positionVar = "tTransformedPosition";
					}
					
					var procV:Procedure = Procedures.vertexShaderColor;
					var procF:Procedure = Procedures.pixelShaderColor;
					
					vertexLinker.addProcedure(procV);
					vertexLinker.setInputParams(procV, positionVar);
					
					var fragmentLinker:Linker = new Linker(Context3DProgramType.FRAGMENT);
					fragmentLinker.addProcedure(procF);	
					fragmentLinker.varyings = vertexLinker.varyings;
				curUsedProgramA = new ShaderProgram(vertexLinker, fragmentLinker);
				totalProgramCatchedList[surface.materialKey] = curUsedProgramA;
				}
			
			var renderUnit:RenderUnit = camera.pool.popRenderUnit();				
			renderUnit.object = object;
			renderUnit.program = curUsedProgramA.getProgram3D(context3D);
			renderUnit.indexBuffer = geometry.getIndexBuffer(context3D);
			renderUnit.firstIndex = surface.indexBegin;
			renderUnit.numTriangles = surface.trianglesNum;
			
			object.setTransformConstants(renderUnit, surface, curUsedProgramA, camera, 0x2);
			renderUnit.setProjectionConstants(camera, curUsedProgramA.cProjMatrix, object.localToCameraTransform);
			renderUnit.culling = this.culling;
			
			object.setTransformConstants(renderUnit, surface, curUsedProgramA, camera, 0x1);
			renderUnit.setVertexBufferAt(curUsedProgramA.aPosition, positionBuffer, 
				geometry.attrOffsetsList[Defines.POSITION], 
				Defines.FORMATS[Defines.POSITION]);
			
			renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cColor, 
				this.red * camera.mulColorFactor + camera.addColorFactor, 
				this.green * camera.mulColorFactor + camera.addColorFactor, 
				this.blue * camera.mulColorFactor + camera.addColorFactor, 
				this.alpha);
			
			renderUnit.srcBlend =  Defines.BLEND_FACTORS[this.srcBlendValue];
			renderUnit.dstBlend = Defines.BLEND_FACTORS[this.dstBlendValue];
			renderUnit.sortID = curUsedProgramA.id * 0xffffff + geometry.id;
			camera.renderer.addRenderUnit(renderUnit, this.renderPriority);
			renderUnit.release();
			return 1;
		}
		
		FlashGE function collectDrawsHSL(camera:Camera3D, surface:Surface, geometry:Geometry):int 
		{
			var context3D:Context3D = camera.context3D;
			var object:Entity = surface.object;
			var positionBuffer:VertexBuffer3D = geometry.getVertexBuffer(Defines.POSITION, context3D);
			var uvBuffer:VertexBuffer3D = geometry.getVertexBuffer(Defines.TEXCOORDS[0], context3D);
			
			if (positionBuffer == null || uvBuffer == null || textureFilesCount == 0) {
				return 0;
			}
			
			var curTexture:TextureFile = textureFiles[0];
			if ( curTexture !=null && curTexture.getTexture(context3D) == null) {
				return 0;
			}
			
			var hasShadowMap:Boolean = this.recieveShadow && camera.shadowCalculor != null && camera.shadowCalculor.shadowMapFile != null;
			var curIndex:int = 0;
			if (this.hue==1 &&this.saturation==1&& this.lightness==1) {
				curIndex = 1;
			}
			
			if (hasShadowMap) {
				curIndex = curIndex + 2;
			}			
			
			var texFormatID:int = textureFiles[0].getFormatID();
			var programIndex:int = texFormatID * 4 + curIndex;
			var resetUnit:Boolean = false;
			
			if (this.typeChanged) {
				surface.clearRenderUnits();
				this.typeChanged = false;
			}
			
			if (this.programCachedID == null || surface.materialKey == null) {
				resetUnit = true;
				calcSurfaceKey(camera, surface, String(programIndex));
			}
			
			if (this.curUsedProgramA == null) {
				this.curUsedProgramA = totalProgramCatchedList[surface.materialKey]; 
				if (curUsedProgramA == null) {
					curUsedProgramA = this.getShaderProgram(surface.materialKey);
				}
			}
				
			if (curUsedProgramA == null) {
					
					var positionVar:String = "aPosition";
					var vertexLinker:Linker = new Linker(Context3DProgramType.VERTEX);
					
					vertexLinker.declareVariable(positionVar, VariableType.ATTRIBUTE);
					
					if (surface.object.transformProcedure != null) {
						surface.object.appendPositionTransformProcedure(vertexLinker);
						positionVar = "tTransformedPosition";
					}
					
					var procV:Procedure;
					var procF:Procedure;
					if (curIndex == 0) {
						procV = Procedures.vertexShaderHSL;
						procF = Procedures.getPixelShaderHSL(false, texFormatID);
					} else if (curIndex == 1) {
						procV = Procedures.vertexShaderHSL;
						procF = Procedures.getPixelShaderNoneHSL(texFormatID);
					} else if (curIndex == 2) {
						
						procV = Procedures.vertexShadowShaderHSL;
						procF = Procedures.getPixelShaderHSL(true, texFormatID);
					} else {
						
						procV = Procedures.vertexShadowShaderHSL;
						procF = Procedures.getPixelShadowShaderNoneHSL(texFormatID);
					}
					
					vertexLinker.addProcedure(procV);
					vertexLinker.setInputParams(procV, positionVar);
					
					var fragmentLinker:Linker = new Linker(Context3DProgramType.FRAGMENT);
					fragmentLinker.addProcedure(procF);	
					fragmentLinker.varyings = vertexLinker.varyings;
				curUsedProgramA = new ShaderProgram(vertexLinker, fragmentLinker);
				totalProgramCatchedList[surface.materialKey] = curUsedProgramA;
				}

			var program3D:Program3D;
			program3D = curUsedProgramA.getProgram3D(context3D);
			var renderUnit:RenderUnit = surface.renderUnit0;
			var indexBuffer3D:IndexBuffer3D = geometry.getIndexBuffer(context3D);
			
			if (surface.renderUnit0 == null) {
				renderUnit = camera.pool.popRenderUnit();
				renderUnit.object = object;
				renderUnit.program = program3D;
				renderUnit.indexBuffer = indexBuffer3D;
				renderUnit.firstIndex = surface.indexBegin;
				renderUnit.numTriangles = surface.trianglesNum;
				surface.renderUnit0 = renderUnit;
				resetUnit = true;
			}
			
			if (resetUnit==false && this.usedContext3D != context3D) {
				resetUnit = true;
			}

			if (resetUnit) {
				renderUnit.numTriangles = surface.trianglesNum;
				renderUnit.program = program3D;
				renderUnit.indexBuffer = indexBuffer3D;
				renderUnit.fragmentConstantsRegistersCount = 0;
				renderUnit.vertexConstantsRegistersCount = 0;
				renderUnit.vertexBuffersLength = 0;
				if (renderUnit.culling != this.culling) {
					renderUnit.culling = this.culling;
				}
				object.setTransformConstants(renderUnit, surface, curUsedProgramA, camera, 0x3);
				
				renderUnit.setVertexBufferAt(curUsedProgramA.aPosition, positionBuffer, 
					geometry.attrOffsetsList[Defines.POSITION], 
					Defines.FORMATS[Defines.POSITION]);
				
				renderUnit.setVertexBufferAt(curUsedProgramA.aUV, uvBuffer, 
					geometry.attrOffsetsList[Defines.TEXCOORDS[0]], 
					Defines.FORMATS[Defines.TEXCOORDS[0]]);			
				if (curUsedProgramA.cHSLC0>=0) {
					renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cHSLC0, 0,2,0.5,1);
				}
				
				if (curUsedProgramA.cHSLC1>=0) {
					renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cHSLC1, 1/3.0,1/6.0,2/3.0,0);
				}		
				
				if (curUsedProgramA.cFactors >=0 ) {
					renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cFactors, -1, 0,0,0);
				}
				
				if (curUsedProgramA.cHSLC2>=0) {
					renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cHSLC2, this.hue, this.saturation, this.lightness, 0);
				}
				
				if (hasShadowMap) {
					if (this.tempTrans == null) {
						this.tempTrans = camera.pool.popTransform3D();
					}
					
					this.tempTrans.combine(camera.shadowCalculor.cameraToShadowMapUVProjection, object.localToCameraTransform);
					renderUnit.setVertexConstantsFromTransform(curUsedProgramA.cShadowMapUVProjection, tempTrans);
					
					renderUnit.setTextureAt(curUsedProgramA.sShadowMap, camera.shadowCalculor.shadowMapFile);
					renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cShadowConstants, -255*Defines.DIFFERENCE_MULTIPLIER, 
						-Defines.DIFFERENCE_MULTIPLIER, Defines.BIASD_MULTIPLIER*255*Defines.DIFFERENCE_MULTIPLIER, 1/16);							
					renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cShadowDist, 0.9999, Defines.DIFFERENCE_MULTIPLIER, 1, Defines.SHADER_OPAQUE);
				}
				
				renderUnit.srcBlend = Defines.BLEND_FACTORS[this.srcBlendValue];
				renderUnit.dstBlend = Defines.BLEND_FACTORS[this.dstBlendValue];
			}
			
			if (curUsedProgramA.cUVScale>=0) {
				renderUnit.setVertexConstantsFromNumbers(curUsedProgramA.cUVScale, 
					this.uvUScale,  this.uvVScale, 
					this.uvUOffset, this.uvVOffset);
			}
			
			renderUnit.releaseAllTextures();
			for (var i:int = 0; i < textureFilesCount; i++ ) {
				renderUnit.setTextureAt(curUsedProgramA.sTextures[i], textureFiles[i]);
			}
			if (curUsedProgramA.cHSLC3>=0) {
				renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cHSLC3, 
					this.red * camera.mulColorFactor *camera.sunColorR+ camera.addColorFactor, 
					this.green * camera.mulColorFactor*camera.sunColorG + camera.addColorFactor, 
					this.blue * camera.mulColorFactor*camera.sunColorB + camera.addColorFactor, 
					this.alpha);
			}
			renderUnit.setProjectionConstants(camera, curUsedProgramA.cProjMatrix, object.localToCameraTransform);
			renderUnit.sortID = (curUsedProgramA.id + curTexture.id) * 0xffffff + geometry.id;
			camera.renderer.addRenderUnit(renderUnit, this.renderPriority);
			//renderUnit.release();
			return 1;
		}
		
		FlashGE function collectDraws(camera:Camera3D, surface:Surface, geometry:Geometry):int 
		{
			if (camera.closeCollectRenderUnits) {
				return 0;
			}
			
			if (this.textureFilesCount == 1) {
				this.texOpIndex = 0;
			}
									
			var renderCount:int = 0;
			if (usedContext3D == null) {
				usedContext3D = camera.context3D;
			}
			
			if (this.type == Defines.MATERIAL_HSL) {
				renderCount = collectDrawsHSL(camera, surface, geometry);
			} else if (this.type == Defines.MATERIAL_EFFECT) {
				renderCount = collectEffectDraws(camera, surface, geometry);
			} else if (this.type == Defines.MATERIAL_FOG) {
				renderCount = collectFog(camera, surface, geometry);
			} else if (this.type == Defines.MATERIAL_BASIC) {
				renderCount = collectBasic(camera, surface, geometry);
			} else if (this.type == Defines.MATERIAL_WING) {
				renderCount = collectWing(camera, surface, geometry);
			} else if (this.type == Defines.MATERIAL_COLOR) {
				renderCount = collectColor(camera, surface, geometry);
			} else if (this.type == Defines.MATERIAL_WATER) {
				renderCount = collectWater(camera, surface, geometry);
			} else {
				renderCount = collectModelDraws(camera, surface, geometry);
			} 
			
			if (this.usedContext3D != camera.context3D) {
				this.usedContext3D = camera.context3D;
			}
			
			return renderCount;
		}
		
		public function getShaderProgram(programKey:String):ShaderProgram
		{
			var curAllFxList:Dictionary = loaderMgr.getAllFxList();
			if (curAllFxList != null) {
				var item:Object = curAllFxList[programKey];
				var shader:ShaderProgram;
				if (item != null) {
					shader = totalProgramCatchedList[programKey];
					if (shader == null) {
						var allFxRawData:ByteArray = loaderMgr.getAllFxRawData();
						allFxRawData.position = item.offset;
						shader = new ShaderProgram(null, null);
						shader.read(allFxRawData);
						totalProgramCatchedList[programKey] = shader;
					}
					return shader;
				}
			}
			return null;
		}
		
		FlashGE function collectWing(camera:Camera3D, surface:Surface, geometry:Geometry):int
		{
			var i:int = 0, n:int = 0;
			var object:Entity = surface.object;
			var context3D:Context3D = camera.context3D;
			if (textureFilesCount == 0) {
				return 0;
			}
			
			for (i = 0; i < textureFilesCount; i++) {
				var tex:TextureFile = this.textureFiles[i]
				if (tex == null) {
					return 0;
				}
				if (tex.getTexture(context3D) == null) {
					return 0;
				}
			}
			
			if (this.programCachedID == null || surface.materialKey == null) {
				calcSurfaceKey(camera, surface);
			}
			
			if (curUsedProgramA == null) {
				curUsedProgramA = totalProgramCatchedList[surface.materialKey];
				if (curUsedProgramA == null) {
					curUsedProgramA = this.getShaderProgram(surface.materialKey);
			}
			}
			
			var texFormat0:uint = textureFilesCount > 0 ? this.textureFiles[0].getFormatID() : 0;
			var texFormat1:uint = textureFilesCount > 1 ? this.textureFiles[1].getFormatID() : 0;
			var useDistortion:Boolean = textureFilesCount == 2;
			if (curUsedProgramA == null) {
				
				var vertexLinker:Linker = new Linker(Context3DProgramType.VERTEX);
				var positionVar:String = "aPosition";
				var uvVar:String = "aUV";
				
				vertexLinker.declareVariable(positionVar, VariableType.ATTRIBUTE);
				vertexLinker.declareVariable(uvVar, VariableType.ATTRIBUTE);
				
				var tmpProcedure:Procedure;
				if (object.positionProcedure != null) {
					positionVar = "tTransformedPosition";
					tmpProcedure = object.positionProcedure;
					vertexLinker.declareVariable(positionVar);
					vertexLinker.addProcedure(tmpProcedure);
					vertexLinker.setInputParams(tmpProcedure, "aPosition");
					vertexLinker.setOutputParams(tmpProcedure, positionVar);
				}
				
				tmpProcedure = Procedures.getWingVShader();
				vertexLinker.addProcedure(tmpProcedure);
				vertexLinker.setInputParams(tmpProcedure, positionVar);				
				vertexLinker.addProcedure(Procedures.getWingUVShader(useDistortion, fixWaveAlpha));
				
				var fragmentLinker:Linker = new Linker(Context3DProgramType.FRAGMENT);
				fragmentLinker.addProcedure(Procedures.getWingFShader(fixWaveAlpha, 
					texFormat0, texFormat1, useDistortion, repeatIndex));	
				fragmentLinker.varyings = vertexLinker.varyings;
				totalProgramCatchedList[surface.materialKey] = curUsedProgramA 
					= new ShaderProgram(vertexLinker, fragmentLinker);
			}
			
			var renderer:Renderer = camera.renderer;
			
			var renderUnit:RenderUnit = camera.pool.popRenderUnit();
			renderUnit.object = object;
			renderUnit.program = curUsedProgramA.getProgram3D(camera.context3D);
			renderUnit.indexBuffer = geometry.getIndexBuffer(camera.context3D);
			renderUnit.firstIndex = surface.indexBegin;
			renderUnit.numTriangles = surface.trianglesNum;
			
			renderUnit.setProjectionConstants(camera, curUsedProgramA.cProjMatrix, object.localToCameraTransform);
			
			var positionBuffer:VertexBuffer3D = geometry.getVertexBuffer(Defines.POSITION, camera.context3D);
			var uvBuffer:VertexBuffer3D = geometry.getVertexBuffer(Defines.TEXCOORDS[0], camera.context3D);
			
			renderUnit.setVertexBufferAt(curUsedProgramA.aPosition, positionBuffer, 
				geometry.attrOffsetsList[Defines.POSITION], 
				Defines.FORMATS[Defines.POSITION]);
			renderUnit.setVertexBufferAt(curUsedProgramA.aUV, uvBuffer, 
				geometry.attrOffsetsList[Defines.TEXCOORDS[0]], 
				Defines.FORMATS[Defines.TEXCOORDS[0]]);
			
			object.setTransformConstants(renderUnit, surface, curUsedProgramA, camera, 0x3);
			renderUnit.texturesLength = 0;
			renderUnit.setTextureAt(0, this.textureFiles[0]);
			if (textureFilesCount > 1) {
				renderUnit.setTextureAt(1, this.textureFiles[1]);
			}
			// x 振幅
			// y 时间
			// z 波长
			// w 衰减强度
			var time:Number;
			if (this.startPlayTimer != -1) {
				time = (getTimer() - this.startPlayTimer)/1000.0;
			} else {
				time = getTimer() / 1000.0;
			}
			if (this.waveContext != null) {
				renderUnit.setVertexConstantsFromNumbers(curUsedProgramA.cWing0, 
					waveContext[0].r, waveContext[0].g, waveContext[0].b, waveContext[0].a);
				
				renderUnit.setVertexConstantsFromNumbers(curUsedProgramA.cWing1, 
					waveContext[1].r, waveContext[1].g, waveContext[1].b, time);
				
				renderUnit.setVertexConstantsFromNumbers(curUsedProgramA.cWing2,
					waveContext[2].r, waveContext[2].g, waveContext[2].b);
			} else {
				renderUnit.setVertexConstantsFromNumbers(curUsedProgramA.cWing0, 0, 0, 10, 200);
				renderUnit.setVertexConstantsFromNumbers(curUsedProgramA.cWing1, 0, 0, 10, 200);
				renderUnit.setVertexConstantsFromNumbers(curUsedProgramA.cWing2, 0, 0, 10, 200);
			}
			
			if (curUsedProgramA.cUVTrans2 >= 0) {
				renderUnit.setVertexConstantsFromNumbers(curUsedProgramA.cUVTrans2, 
					waveContext[3].r, waveContext[3].g, waveContext[3].b, waveContext[3].a);
			}
			
			if (curUsedProgramA.cDistortion >= 0) {
				renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cDistortion, 
					waveContext[4].r, waveContext[4].g, waveContext[4].b, waveContext[4].a);
			}
			
			renderUnit.setVertexConstantsFromNumbers(curUsedProgramA.cUVTrans, this.uvV0, this.uvV1, time, 0);
			renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cColor, this.red, this.green, this.blue, this.alpha);
			
			renderUnit.culling = getCulling();
			renderUnit.srcBlend = Defines.BLEND_FACTORS[getSrcBlend()];
			renderUnit.dstBlend = Defines.BLEND_FACTORS[getDstBlend()];
			renderUnit.sortID = (curUsedProgramA.id + this.textureFiles[0].id) * 0xffffffff + geometry.id;
			renderer.addRenderUnit(renderUnit, this.renderPriority);
			renderUnit.release();
			return 1;
		}
		
		FlashGE function collectBasic(camera:Camera3D, surface:Surface, geometry:Geometry):int
		{
			var object:Entity = surface.object;
			if (textureFilesCount == 0) {
				return 0;
			}
			
			var curTexture:TextureFile = this.textureFiles[0];
			if (curTexture == null) {
				return 0;
			}
			
			if (curTexture.isLoaded() == false || curTexture.getTexture(camera.context3D) == null) {
				return 0;
			}
			
			if (this.programCachedID == null || surface.materialKey == null) {
				calcSurfaceKey(camera, surface);
			}
			
			if (this.curUsedProgramA == null) {
				this.curUsedProgramA = totalProgramCatchedList[surface.materialKey]; 
				if (curUsedProgramA == null) {
					curUsedProgramA = this.getShaderProgram(surface.materialKey);
				}
			}
			
			if (curUsedProgramA == null) {
				var vertexLinker:Linker = new Linker(Context3DProgramType.VERTEX);
				var positionVar:String = "aPosition";
				var normalVar:String = "aNormal";
				var uvVar:String = "aUV";
				var texFormat:int = this.textureFiles[0].getFormatID();
				
				vertexLinker.declareVariable(positionVar, VariableType.ATTRIBUTE);
				vertexLinker.declareVariable(normalVar, VariableType.ATTRIBUTE);
				vertexLinker.declareVariable(uvVar, VariableType.ATTRIBUTE);
				
				var tmpProcedure:Procedure;
				if(object.transformProcedure!=null) {
					positionVar = "tTransformedPosition";
					normalVar = "tTransformedNormal";
					tmpProcedure = object.transformProcedure;
					vertexLinker.declareVariable(positionVar);
					vertexLinker.declareVariable(normalVar);
					vertexLinker.addProcedure(tmpProcedure);
					vertexLinker.setInputParams(tmpProcedure, "aPosition", "aNormal");
					vertexLinker.setOutputParams(tmpProcedure, positionVar, normalVar);
				}
				tmpProcedure = Procedures.getBasicVShader(false);
				vertexLinker.addProcedure(tmpProcedure);
				vertexLinker.setInputParams(tmpProcedure, positionVar, normalVar);				
				vertexLinker.addProcedure(Procedures.uvProcedure);
				
				var fragmentLinker:Linker = new Linker(Context3DProgramType.FRAGMENT);
				fragmentLinker.addProcedure(Procedures.getBasicFShader(false, texFormat));	
				fragmentLinker.varyings = vertexLinker.varyings;
				curUsedProgramA = new ShaderProgram(vertexLinker, fragmentLinker);
				totalProgramCatchedList[surface.materialKey] = curUsedProgramA;
			}
			
			var renderer:Renderer = camera.renderer;
			
			var renderUnit:RenderUnit = camera.pool.popRenderUnit();
			renderUnit.object = object;
			renderUnit.program = curUsedProgramA.getProgram3D(camera.context3D);
			renderUnit.indexBuffer = geometry.getIndexBuffer(camera.context3D);
			renderUnit.firstIndex = surface.indexBegin;
			renderUnit.numTriangles = surface.trianglesNum; 
			renderUnit.setProjectionConstants(camera, curUsedProgramA.cProjMatrix, object.localToCameraTransform);
			
			var positionBuffer:VertexBuffer3D = geometry.getVertexBuffer(Defines.POSITION, camera.context3D);
			var uvBuffer:VertexBuffer3D = geometry.getVertexBuffer(Defines.TEXCOORDS[0], camera.context3D);
			var normalBuffer:VertexBuffer3D = geometry.getVertexBuffer(Defines.NORMAL, camera.context3D);
			
			renderUnit.setVertexBufferAt(curUsedProgramA.aPosition, positionBuffer, geometry.attrOffsetsList[Defines.POSITION], 
				Defines.FORMATS[Defines.POSITION]);
			renderUnit.setVertexBufferAt(curUsedProgramA.aUV, uvBuffer, geometry.attrOffsetsList[Defines.TEXCOORDS[0]], 
				Defines.FORMATS[Defines.TEXCOORDS[0]]);
			if (normalBuffer == null) {
				
				renderUnit.setVertexBufferAt(curUsedProgramA.aNormal, positionBuffer, geometry.attrOffsetsList[Defines.NORMAL], 
					Defines.FORMATS[Defines.NORMAL]);
			} else {
				renderUnit.setVertexBufferAt(curUsedProgramA.aNormal, normalBuffer, geometry.attrOffsetsList[Defines.NORMAL], 
					Defines.FORMATS[Defines.NORMAL]);
			}
			
			object.setTransformConstants(renderUnit, surface, curUsedProgramA, camera, 0x3);
			renderUnit.setTextureAt(0, curTexture);
			renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cColor, this.red, this.green, this.blue, this.alpha);
			
			renderUnit.culling = getCulling();
			renderUnit.srcBlend = Defines.BLEND_FACTORS[getSrcBlend()];
			renderUnit.dstBlend = Defines.BLEND_FACTORS[getDstBlend()];
			renderUnit.sortID = (curUsedProgramA.id + curTexture.id) * 0xffffffff + geometry.id;
			renderer.addRenderUnit(renderUnit, getRenderPriority());
			renderUnit.release();
			return 1;
		}
		
		FlashGE function collectFog(camera:Camera3D, surface:Surface, geometry:Geometry):int
		{
			return 0;
		}
		
		FlashGE function collectModelDraws(camera:Camera3D, surface:Surface, geometry:Geometry):int
		{
			var context3D:Context3D = camera.context3D;
			var object:Entity = surface.object;
						
			var positionBuffer:VertexBuffer3D = geometry.getVertexBuffer(Defines.POSITION, context3D);
			var uvBuffer:VertexBuffer3D = geometry.getVertexBuffer(Defines.TEXCOORDS[0], context3D);
			if (positionBuffer == null || uvBuffer == null || textureFilesCount == 0) {
				return 0;
			}
			
			var i:int = 0;
			
			for (i = 0; i < textureFilesCount; i++) {
				if (this.textureFiles[i] != null && this.textureFiles[i].getTexture(context3D) == null) {
					return 0;
				}
			}
			
			var hasShadowMap:Boolean = camera.shadowCalculor != null && camera.shadowCalculor.shadowMapFile != null;
			if (this.recieveShadow && hasShadowMap==false) {
				return 0;
			}
			
			if (this.typeChanged) {
				surface.clearRenderUnits();
				this.typeChanged = false;
			}
			
			var resetRenderUnit:Boolean = false;
			if (this.programCachedID == null || surface.materialKey == null) {
				calcSurfaceKey(camera, surface);
				resetRenderUnit = true;
			}
			
			var transformProcedure:Procedure;
			if (this.recieveShadow) {
				transformProcedure = object.transformReciveShadowProcedure;
				if (transformProcedure == null && object is Skin) {
					(object as Skin).skinData.calculateSurfacesProceduresReciveShadow();
					return 0;
				}
			}
			
			if (this.curUsedProgramA == null) {
				this.curUsedProgramA = totalProgramCatchedList[surface.materialKey];
				if (curUsedProgramA == null) {
					curUsedProgramA = this.getShaderProgram(surface.materialKey);
				}
			}
			
			if (curUsedProgramA == null ) {
				
				var vertexLinker:Linker = new Linker(Context3DProgramType.VERTEX);
				var positionVar:String = "aPosition";
				var normalVar:String = "aNormal";
				vertexLinker.declareVariable(positionVar, VariableType.ATTRIBUTE);
				vertexLinker.declareVariable(normalVar, VariableType.ATTRIBUTE);
				
				if (this.recieveShadow) {
					transformProcedure = object.transformReciveShadowProcedure;
				} else {
					transformProcedure = object.transformProcedure;
				}
				var tmpProcedure:Procedure;
				if (transformProcedure!=null) {
					positionVar = "tTransformedPosition";
					normalVar = "tTransformedNormal";
					tmpProcedure = transformProcedure;
					vertexLinker.declareVariable(positionVar);
					vertexLinker.declareVariable(normalVar);
					vertexLinker.addProcedure(tmpProcedure);
					vertexLinker.setInputParams(tmpProcedure, "aPosition", "aNormal");
					vertexLinker.setOutputParams(tmpProcedure, positionVar, normalVar);
					tmpProcedure = Procedures.getModelVertexProcedure(false, recieveShadow);
				} else {
					tmpProcedure = Procedures.getModelVertexProcedure(true, this.recieveShadow);
				}
				vertexLinker.addProcedure(tmpProcedure);
				vertexLinker.setInputParams(tmpProcedure, positionVar, normalVar);
				
				vertexLinker.addProcedure(Procedures.uvProcedureEx);
				
				var texFormatID0:uint = textureFilesCount > 0 ? this.textureFiles[0].getFormatID() : 0;
				var texFormatID1:uint = textureFilesCount > 1 ? this.textureFiles[1].getFormatID() : 0;
				var isCube0:Boolean = textureFilesCount > 0 ? this.textureFiles[0].isCube() : false;
				var isCube1:Boolean = textureFilesCount > 1 ? this.textureFiles[1].isCube() : false;
				var fragmentLinker:Linker = new Linker(Context3DProgramType.FRAGMENT);
				
				tmpProcedure = Procedures.getModelFramentProcedure(this,
					textureFilesCount, this.texOpIndex,
					this.filterIndex, this.repeatIndex, 
					this.mipIndex, texFormatID0, texFormatID1, 
					isCube0, isCube1,
					this.recieveShadow, this.alphaTest > 0);
				
				fragmentLinker.addProcedure(tmpProcedure);
				fragmentLinker.varyings = vertexLinker.varyings;
				
				curUsedProgramA = new ShaderProgram(vertexLinker, fragmentLinker);		
				totalProgramCatchedList[surface.materialKey] = curUsedProgramA;
				resetRenderUnit = true;			
			}
			
			var program3D:Program3D;
			program3D = curUsedProgramA.getProgram3D(context3D);
			var indexBuffer3D:IndexBuffer3D = geometry.getIndexBuffer(context3D);
			var renderUnit:RenderUnit = surface.renderUnit0; 
			if (surface.renderUnit0 == null) {
				renderUnit = camera.pool.popRenderUnit();
				surface.renderUnit0 = renderUnit;
				resetRenderUnit = true;
			}

			if (resetRenderUnit == false) {
				if (this.usedContext3D != context3D ||
					renderUnit.indexBuffer != indexBuffer3D ||
					renderUnit.program != program3D) {
					resetRenderUnit = true;
				}
			}
			if (resetRenderUnit == true) {
				renderUnit.object = object;
				renderUnit.firstIndex = surface.indexBegin;
				renderUnit.numTriangles = surface.trianglesNum;
				renderUnit.indexBuffer = indexBuffer3D;
				renderUnit.program = program3D;
				renderUnit.culling = "front";
				renderUnit.vertexBuffersLength = 0;
				renderUnit.fragmentConstantsRegistersCount = 0;
				renderUnit.vertexConstantsRegistersCount = 0;
				
				object.setTransformConstants(renderUnit, surface, curUsedProgramA, camera, 0x1);
				renderUnit.setVertexBufferAt(curUsedProgramA.aPosition, positionBuffer, geometry.attrOffsetsList[Defines.POSITION], Defines.FORMATS[Defines.POSITION]);
				renderUnit.setVertexBufferAt(curUsedProgramA.aUV, uvBuffer, geometry.attrOffsetsList[Defines.TEXCOORDS[0]], Defines.FORMATS[Defines.TEXCOORDS[0]]);
				
				if (curUsedProgramA.aNormal >= 0) {
					var normalBuffer:VertexBuffer3D = geometry.getVertexBuffer(Defines.NORMAL, context3D);
					if (normalBuffer != null) { 
						renderUnit.setVertexBufferAt(curUsedProgramA.aNormal, normalBuffer, geometry.attrOffsetsList[Defines.NORMAL], Defines.FORMATS[Defines.NORMAL]);
					} else {
						renderUnit.setVertexBufferAt(curUsedProgramA.aNormal, positionBuffer, geometry.attrOffsetsList[Defines.NORMAL],	Defines.FORMATS[Defines.NORMAL]);
					}
				}
			}
			
			renderUnit.releaseAllTextures();
			for (i =0; i < textureFilesCount; i++ ) {
				renderUnit.setTextureAt(curUsedProgramA.sTextures[i], textureFiles[i]);
			}
			
			if (curUsedProgramA.cUVTrans >= 0) {
				if (getGlobalTimer == null) {
					getGlobalTimer = getTimer;
				}
				var t:Number = getGlobalTimer()/1000;
				renderUnit.setVertexConstantsFromNumbers(curUsedProgramA.cUVTrans, this.uvV0, this.uvV1, t, 0);
			}
			
			if (this.recieveShadow == true) {
				
				if (hasShadowMap == true) {
					if (this.tempTrans == null) {
						this.tempTrans = camera.pool.popTransform3D();
					}
					
					tempTrans.combine(camera.shadowCalculor.cameraToShadowMapUVProjection, object.localToCameraTransform);
					renderUnit.setVertexConstantsFromTransform(curUsedProgramA.cShadowMapUVProjection, tempTrans);
					renderUnit.setTextureAt(curUsedProgramA.sShadowMap, camera.shadowCalculor.shadowMapFile);
					renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cShadowConstants, -255*Defines.DIFFERENCE_MULTIPLIER, 
						-Defines.DIFFERENCE_MULTIPLIER, Defines.BIASD_MULTIPLIER*255*Defines.DIFFERENCE_MULTIPLIER, 1/16);
					renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cShadowDist, 0.9999, Defines.DIFFERENCE_MULTIPLIER, 1, 0.75);
				}
			}

			if (this.useLight == true) {
				renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cDiffuse, 
					this.diffuseR*camera.mulColorFactor*camera.sunColorR + camera.addColorFactor, 
					this.diffuseG*camera.mulColorFactor*camera.sunColorG + camera.addColorFactor,
					this.diffuseB*camera.mulColorFactor*camera.sunColorB + camera.addColorFactor, 
					this.alpha);
				renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cAmbient, 
					0.688235319, 0.688235319, 0.688235319, 
					0);
			} else {
				renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cDiffuse, 0, 0, 0, this.alpha);
				renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cAmbient, 
					this.red*camera.mulColorFactor + camera.addColorFactor, 
					this.green*camera.mulColorFactor + camera.addColorFactor, 
					this.blue*camera.mulColorFactor + camera.addColorFactor, 0);
			}
			
			renderUnit.setProjectionConstants(camera, curUsedProgramA.cProjMatrix, object.localToCameraTransform);			
			
			if (this.tempTrans == null) {
				this.tempTrans = camera.pool.popTransform3D();
			}
			
			this.tempTrans.combine(object.cameraToLocalTransform, camera.getRootNode().localToCameraTransform);
			var _x:Number = -camera.lightDir.x;
			var _y:Number = -camera.lightDir.y;
			var _z:Number = -camera.lightDir.z;
			var localX:Number = _x*tempTrans.a + _y*tempTrans.b + _z*tempTrans.c;
			var localY:Number = _x*tempTrans.e + _y*tempTrans.f + _z*tempTrans.g;
			var localZ:Number = _x*tempTrans.i + _y*tempTrans.j + _z*tempTrans.k;
			var len:Number = 1.0 / Math.sqrt(localX*localX + localY*localY + localZ*localZ);
			renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cLightDir, localX*len, localY*len, localZ*len, 0.01);
			object.setTransformConstants(renderUnit, surface, curUsedProgramA, camera, 0x2);
			
			var srcBlend:String = Defines.BLEND_FACTORS[this.srcBlendValue];
			if (srcBlend != renderUnit.srcBlend) {
				renderUnit.srcBlend = srcBlend; 
			}
			
			var dstBlend:String= Defines.BLEND_FACTORS[this.dstBlendValue];
			if (dstBlend != renderUnit.dstBlend) {
				renderUnit.dstBlend = dstBlend;
			}
			renderUnit.sortID = (curUsedProgramA.id + this.textureFiles[0].id) * 0xffffffff + geometry.id;
			camera.renderer.addRenderUnit(renderUnit, this.renderPriority);
			//renderUnit.release();
			return 1;
		}
		
		public function isOpenNormalAlpha(camera:Camera3D):Boolean
		{
			if (camera.context3DProperties.isOpenGL) {
				return false;
			}
			return this.alphaIntensity != 0;
		}
		
		public function calcProgramCachedID(camera:Camera3D):void
		{
			var texLen:int = this.textureFiles.length;
			var texFormatID0:uint = (texLen > 0 ? this.textureFiles[0].getFormatID() : uint.MAX_VALUE);
			var texFormatID1:uint = (texLen > 1 ? this.textureFiles[1].getFormatID() : uint.MAX_VALUE);
			var isCube0:Boolean = (texLen > 0 ? this.textureFiles[0].isCube() : false);
			var isCube1:Boolean = (texLen > 1 ? this.textureFiles[1].isCube() : false);
			var openNormalAlpha:Boolean = isOpenNormalAlpha(camera);

			var curUseUVType:int = this.closeUVTrans ? 0 : (this.useUVType == Defines.UV_TYPE_NONE ? Defines.UV_TYPE_TRANS : Defines.UV_TYPE_FRAME);
			this.programCachedID = this.closeUVTrans + "_" +
				isCube0 + "_" +
				texFormatID0 + "_" +
				isCube1 + "_" +
				texFormatID1 + "_" +
				curUseUVType + "_" +
				this.useVertDiffuse + "_" +
				this.textureFilesCount + "_" +
				this.texOpIndex + "_" +
				this.filterIndex + "_" +
				this.repeatIndex + "_" +
				this.mipIndex + "_" +
				this.type + "_" +
				this.recieveShadow + "_" +
				this.alphaTest + "_" + 
				this.isGrayColor + "_" +
				this.getReplaceAlpha(camera) + "_" +
				this.fixWaveAlpha + "_" +
				openNormalAlpha;
		}
			
		private function calcSurfaceKey(camera:Camera3D, surface:Surface, tag:String = null):void
		{
			if (this.programCachedID == null) {
				this.calcProgramCachedID(camera);
			}
			
			var object:Entity = surface.object;
			if (object.programKey == null) {
				object.calcProgramKey(camera);
			}
			if ((this.type == Defines.MATERIAL_EFFECT && (object is Skin)) || this.recieveShadow) {
				surface.materialKey = surface.materialKeyBase + object.programKey + "_" + this.programCachedID;
			} else {
				surface.materialKey = object.programKey + "_" + this.programCachedID;
			}
			if (tag != null) {
				surface.materialKey = surface.materialKey + "_" + tag;
			}
		}
		
		FlashGE function collectSpriteUnitShader(camera:Camera3D, unit:EffectUnitBase, 
												 doubleAlpha:Boolean, openNormalAlpha:Boolean):ShaderProgram
		{
			var vertexShaderArray:Array = [];
			var arrayIndex:int = 0;
			var outValue:Object = new Object;
			outValue.constIndex = 0;
			arrayIndex = unit.getShaderArray(vertexShaderArray, arrayIndex, outValue);
			var vertexIndex:int = 0;
			var constIndex:int = outValue.constIndex;
			
			var vNormal:String;
			var vViewVector:String;
			var cCameraPos:String;
			
			var aIndex:int = 1;
			var aPosition:String = "a0";
			var aNormal:String;
			var aUV:String;
			var cProjMatrixConstIndex:int = constIndex;
			var cProjMatrix:String = "c" + constIndex;
			vertexShaderArray[arrayIndex++] = "#a0=aPosition"; 
			vertexShaderArray[arrayIndex++] = "#"+cProjMatrix+"=cProjMatrix"; 
			vertexShaderArray[arrayIndex++] = "m44 o0, t0,"+cProjMatrix;
			constIndex+=4;
			if (openNormalAlpha) {
				aNormal = "a" + aIndex;
				aIndex++;
				vNormal = "v"+vertexIndex;
				vViewVector = "v" +vertexIndex;
				vertexIndex+=2;
				cCameraPos = "c"+constIndex;
				constIndex++;
				
				vertexShaderArray[arrayIndex++] = "#"+vNormal+"=vNormal"; 
				vertexShaderArray[arrayIndex++] = "#"+vViewVector+"=vViewVector";
				vertexShaderArray[arrayIndex++] = "#"+cCameraPos+"=cCameraPos";
				
				vertexShaderArray[arrayIndex++] = "sub t1, "+cCameraPos+", " + aPosition;
				vertexShaderArray[arrayIndex++] = "mov t1.w, "+cCameraPos+".w";
				vertexShaderArray[arrayIndex++] = "mov "+vViewVector+", t1";
				
				vertexShaderArray[arrayIndex++] = "mov t2, " + aNormal;
				vertexShaderArray[arrayIndex++] = "sge t2.w, t2.w, t2.w";
				
				vertexShaderArray[arrayIndex++] = "mov "+vNormal+", t2";
			}
			
			var vUV:String = "v" + vertexIndex;
			vertexIndex++;
			var vUVOrg:String = "";
			if (textureFilesCount == 2) {
				vUVOrg = "v" + vertexIndex;
				vertexIndex++;
			}
			
			aUV = "a" + aIndex;
			aIndex++;
			
			var cUVTrans2:String = "c"+constIndex;
			constIndex++;
			var cUVScale:String = "c"+constIndex;
			constIndex++;
			var cUVTrans:String = "c"+constIndex;
			constIndex++;
			
			vertexShaderArray[arrayIndex++] = "#"+aUV+"=aUV";
			vertexShaderArray[arrayIndex++] = "#"+vUV+"=vUV";
			
			if (closeUVTrans) {
				vertexShaderArray[arrayIndex++] = "mov "+vUV+"," + aUV;
			} else {
				
				vertexShaderArray[arrayIndex++] = "#"+cUVScale+"=cUVScale"; // uscale,vscale,uoffset,voffset
				vertexShaderArray[arrayIndex++] = "#"+cUVTrans+"=cUVTrans"; // 0, 0, 0, rotation
				vertexShaderArray[arrayIndex++] = "mul t0.xy, "+aUV+".xy, "+cUVScale+".xy";
				vertexShaderArray[arrayIndex++] = "mov t1," + cUVTrans;
				vertexShaderArray[arrayIndex++] = "sin t2.x, t1.w"; // sin
				vertexShaderArray[arrayIndex++] = "cos t2.y, t1.w"; // cos
				
				vertexShaderArray[arrayIndex++] = "mul t0.z, t0.x, t2.y"; // x*cos
				vertexShaderArray[arrayIndex++] = "mul t0.w, t0.y, t2.x"; // y*sin				
				//vertexShaderArray[arrayIndex++] = "mul t0.zw, t0.xy, t2.yx"; // x*cos, y*sin
				vertexShaderArray[arrayIndex++] = "sub t2.z, t0.z, t0.w"; // X
				
				vertexShaderArray[arrayIndex++] = "mul t0.z, t0.x, t2.x"; // x*sin
				vertexShaderArray[arrayIndex++] = "mul t0.w, t0.y, t2.y"; // y*cos
				//vertexShaderArray[arrayIndex++] = "mul t0.zw, t0.xy, t2.xy"; // x*sin,y*cos				
				vertexShaderArray[arrayIndex++] = "add t2.w, t0.z, t0.w"; // Y
				
				vertexShaderArray[arrayIndex++] = "add t2.xy, t2.zw, "+cUVScale+".zw";
				if(useUVType != Defines.UV_TYPE_FRAME) {
					
					vertexShaderArray[arrayIndex++] = "mov t1.xy, "+cUVTrans+".xy";
					vertexShaderArray[arrayIndex++] = "mul t1.xy, t1.xy, "+cUVTrans+".zz";
					vertexShaderArray[arrayIndex++] = "add t2.xy, t2.xy, t1.xy";
				} 
				vertexShaderArray[arrayIndex++] = "mov "+vUV+", t2";
				if (textureFilesCount == 2) {
					vertexShaderArray[arrayIndex++] = "#"+cUVTrans2+"=cUVTrans2";
					vertexShaderArray[arrayIndex++] = "mov t0, "+ aUV;
					vertexShaderArray[arrayIndex++] = "#"+vUVOrg+"=vUVOrg";
					vertexShaderArray[arrayIndex++] = "add t0.xy, t0.xy, "+cUVTrans2+".zw";
					vertexShaderArray[arrayIndex++] = "mov "+vUVOrg+", t0";
				}
			}
			
			var aVertDiffuse:String;
			var cVertDiffuse:String;
			
			if (this.useVertDiffuse) {
				
				aVertDiffuse = "a" + aIndex;
				aIndex++;
				
				cVertDiffuse = "c"+constIndex;
				constIndex++;
				
				vertexShaderArray[arrayIndex++] = "mov "+cVertDiffuse+", "+aVertDiffuse;
			}
			
			var texFormatID0:int = this.textureFilesCount > 0 ? this.textureFiles[0].getFormatID() : 0;
			var texFormatID1:int = this.textureFilesCount > 1 ? this.textureFiles[1].getFormatID() : 0;
			var isCube0:Boolean = this.textureFilesCount > 0 ? this.textureFiles[0].isCube() : false;
			var isCube1:Boolean = this.textureFilesCount > 1 ? this.textureFiles[1].isCube() : false;
			
			var filter:String = Defines.FILTERS[filterIndex];
			var repeat:String = Defines.REPEATS[repeatIndex];
			var texFormat0:String = Defines.TEX_FORMATS[texFormatID0];
			var texFormat1:String = Defines.TEX_FORMATS[texFormatID1];
			var texFlag0:String = isCube0 ? "cube" : "2d"; 
			var texFlag1:String = isCube1 ? "cube" : "2d"; 
			var mip:String = Defines.MIPS[mipIndex];
			var curIndex:uint = doubleAlpha ? 1 : 0;
			
			var fragConstIndex:int = 0;
			var cColor:String = "c" + fragConstIndex; fragConstIndex++;
			var cFactors:String = "c" + fragConstIndex;fragConstIndex++;
			
			
			var fragShaderArray:Array = [];
			arrayIndex = 0;
			
			fragShaderArray[arrayIndex++] ="#"+vUV+"=vUV";
			fragShaderArray[arrayIndex++] ="#"+cColor+"=cColor";
			fragShaderArray[arrayIndex++] ="#"+cFactors+"=cFactors";
			
			if (textureFilesCount == 2) {
				
				fragShaderArray[arrayIndex++] ="#"+vUVOrg+"=vUVOrg";
				fragShaderArray[arrayIndex++] ="#s0=sTexture0";
				fragShaderArray[arrayIndex++] ="#s1=sTexture1";
				fragShaderArray[arrayIndex++] ="tex t0, "+vUV+", s[0] <"+ texFlag0 + ", "+filter+","+repeat+", "+texFormat0+","+mip+">";
				fragShaderArray[arrayIndex++] ="tex t1, "+vUVOrg+", s[1] <"+ texFlag1 + ", "+filter+","+repeat+", "+texFormat1+","+mip+">";
				if (texOpIndex == Defines.TEX_OP_MUL) {
					fragShaderArray[arrayIndex++] = "mul t0, t0, t1";
				} else if(texOpIndex == Defines.TEX_OP_ADD) {
					fragShaderArray[arrayIndex++] = "add t0, t0, t1";
				} else if(texOpIndex == Defines.TEX_OP_SUB) {
					fragShaderArray[arrayIndex++] = "sub t0, t0, t1";
				} else if(texOpIndex == Defines.TEX_OP_ALPHA) {
					fragShaderArray[arrayIndex++] ="mov t2.x, t0.w";
					fragShaderArray[arrayIndex++] ="sub t3, t0, t1";
					fragShaderArray[arrayIndex++] ="mul t3, t3, t2.x";
					fragShaderArray[arrayIndex++] ="add t0, t3, t1";
				}
				
			} else {
				fragShaderArray[arrayIndex++] ="#s0=sTexture0";
				fragShaderArray[arrayIndex++] ="tex t0, "+vUV+", s[0] <"+ texFlag0 + ", "+filter+","+repeat+", "+texFormat0+","+mip+">";
			}
			
			var curReplaceAlpha:Number = this.getReplaceAlpha(camera);
			if (curReplaceAlpha != Defines.REPLACE_ALPHA_DEFAULT) {
				fragShaderArray[arrayIndex++] ="mov t5.xyz, t0.xyz";
			}
			
			if (isGrayColor) {
				fragShaderArray[arrayIndex++] ="mul t0, t0, "+cColor;
				fragShaderArray[arrayIndex++] ="add t1.xyz, t0.xxx, t0.yyy";
				fragShaderArray[arrayIndex++] ="add t0.xyz, t1.xyz, t0.zzz";
			} else {
				fragShaderArray[arrayIndex++] ="mul t0, t0, "+cColor;
			}
			
			if (curReplaceAlpha != Defines.REPLACE_ALPHA_DEFAULT) {
				fragShaderArray[arrayIndex++] ="mul t5.xyz, t5.xyz, "+cColor+".www";
			}	
			
			if (useVertDiffuse) {
				fragShaderArray[arrayIndex++] ="mul t0, t0, "+cVertDiffuse;
				
				if (curReplaceAlpha != Defines.REPLACE_ALPHA_DEFAULT) {
					fragShaderArray[arrayIndex++] ="mul t5.xyz, t5.xyz, "+cVertDiffuse+".www";
				}
			}
			
			if (openNormalAlpha) {
				//	
				fragShaderArray[arrayIndex++] ="#"+vViewVector+"=vViewVector";
				fragShaderArray[arrayIndex++] ="nrm t2.xyz, "+vViewVector+".xyz";
				fragShaderArray[arrayIndex++] ="nrm t3.xyz, "+vNormal+".xyz";
				
				fragShaderArray[arrayIndex++] ="mov t2.w, "+vViewVector+".w";
				fragShaderArray[arrayIndex++] ="mov t3.w, "+vNormal+".w";
				
				fragShaderArray[arrayIndex++] ="dp3 t4.w, t2.xyz, t3.xyz";
				fragShaderArray[arrayIndex++] ="add t4.w, t4.w, t2.w";
				fragShaderArray[arrayIndex++] ="abs t4.w, t4.w";
				fragShaderArray[arrayIndex++] ="pow t4.w, t4.w, "+cFactors+".x";
				fragShaderArray[arrayIndex++] ="mul t4.w, t4.w, "+cFactors+".y";
				fragShaderArray[arrayIndex++] ="sat t4.w, t4.w";
				fragShaderArray[arrayIndex++] ="mul t0.w, t4.w, t0.w";
			}
			
			if (doubleAlpha) {
				fragShaderArray[arrayIndex++] ="slt t2.x, t0.w, "+cFactors+".w";
				fragShaderArray[arrayIndex++] ="mul t0.w, t0.w, t2.x";
			} else {
				fragShaderArray[arrayIndex++] ="sub t2.x, t0.w, "+cFactors+".w";
				fragShaderArray[arrayIndex++] ="kil t2.x";
			}
			
			if (curReplaceAlpha == Defines.REPLACE_ALPHA_R) {
				fragShaderArray[arrayIndex++] = "mov t0.w, t5.x";
			} else if (curReplaceAlpha == Defines.REPLACE_ALPHA_G) {
				fragShaderArray[arrayIndex++] = "mov t0.w, t5.y";
			} else if (curReplaceAlpha == Defines.REPLACE_ALPHA_B) {
				fragShaderArray[arrayIndex++] = "mov t0.w, t5.z";
			}
			
			fragShaderArray[arrayIndex++] = "mov o0, t0";		
			
			var vertProcedure:Procedure = new Procedure();
			vertProcedure.compileFromArray(vertexShaderArray);
			
			var fragProcedure:Procedure = new Procedure();
			fragProcedure.compileFromArray(fragShaderArray);
			
			var vertexLinker:Linker = new Linker(Context3DProgramType.VERTEX);
			
			vertexLinker.declareVariable("aPosition", VariableType.ATTRIBUTE);
			vertexLinker.declareVariable("aUV", VariableType.ATTRIBUTE);
			
			vertProcedure.assignVariableName(VariableType.CONSTANT, cProjMatrixConstIndex, "cProjMatrix", 4);
			vertexLinker.setInputParams(vertProcedure, "aPosition", "aUV");
			vertexLinker.addProcedure(vertProcedure);	
			
			var fragmentLinker:Linker = new Linker(Context3DProgramType.FRAGMENT);
			fragmentLinker.addProcedure(fragProcedure);	
			fragmentLinker.varyings = vertexLinker.varyings;
			var curProgram:ShaderProgram = new ShaderProgram(vertexLinker, fragmentLinker);		
			return curProgram;
		}
		
		FlashGE function collectEffectDraws(camera:Camera3D, surface:Surface, geometry:Geometry):int
		{
			var context3D:Context3D = camera.context3D;
			var object:Entity = surface.object;
			var positionBuffer:VertexBuffer3D = geometry.getVertexBuffer(Defines.POSITION, context3D);
			var uvBuffer:VertexBuffer3D = geometry.getVertexBuffer(Defines.TEXCOORDS[0], context3D);
			
			if (positionBuffer == null || uvBuffer == null || textureFilesCount == 0) {
				return 0;
			}
			
			for (var i:int=0; i < textureFilesCount; i++) {
				if (this.textureFiles[i] != null && this.textureFiles[i].getTexture(context3D) == null) {
					return 0;
				}
			}

			var needReset:Boolean = false;
			
			if (this.typeChanged) {
				surface.clearRenderUnits();
				this.typeChanged = false;
			}
			
			if (this.programCachedID == null || surface.materialKey == null) {
				calcSurfaceKey(camera, surface);
				needReset = true;
			}
			
			if (this.openDoubleAlpha && this.alphaTest != 1 && !camera.context3DProperties.isOpenGL) {
				this.alphaTest = 1;
			}
			
			if (this.curUsedProgramA == null) {
				this.curUsedProgramA = getEffectShaderProgram(surface, camera, false, textureFilesCount);
			}
			
			var indexBuffer3D:IndexBuffer3D = geometry.getIndexBuffer(context3D);
			var program3D:Program3D;
			program3D = curUsedProgramA.getProgram3D(context3D);
			var renderUnit:RenderUnit = surface.renderUnit0;
			if (renderUnit == null) {
				renderUnit = camera.pool.popRenderUnit();
				surface.renderUnit0 = renderUnit;
				needReset = true;
			}
			
			if (needReset ==false) {
				if (this.usedContext3D != context3D ||
					renderUnit.indexBuffer != indexBuffer3D ||
					renderUnit.program != program3D) {
					needReset = true;
				}
			}

			if (needReset || surface.forceResetUnit) {
				renderUnit.object = object;
				renderUnit.firstIndex = surface.indexBegin;
				renderUnit.numTriangles = surface.trianglesNum;
				renderUnit.program = program3D;
				renderUnit.indexBuffer = indexBuffer3D;
				renderUnit.fragmentConstantsRegistersCount = 0;
				renderUnit.vertexConstantsRegistersCount = 0;
				renderUnit.vertexBuffersLength = 0;
				renderUnit.releaseAllTextures();
				for (i = 0; i < textureFilesCount; i++ ) {
					renderUnit.setTextureAt(curUsedProgramA.sTextures[i], textureFiles[i]);
				}
				
				renderUnit.setVertexBufferAt(curUsedProgramA.aPosition, positionBuffer, geometry.attrOffsetsList[Defines.POSITION], Defines.FORMATS[Defines.POSITION]);
				
				if (curUsedProgramA.aNormal >= 0) {
					var normalBuffer:VertexBuffer3D = geometry.getVertexBuffer(Defines.NORMAL, context3D);
					if (normalBuffer!=null) { 
						renderUnit.setVertexBufferAt(curUsedProgramA.aNormal, normalBuffer, geometry.attrOffsetsList[Defines.NORMAL], Defines.FORMATS[Defines.NORMAL]);
					} else {
						renderUnit.setVertexBufferAt(curUsedProgramA.aNormal, positionBuffer, geometry.attrOffsetsList[Defines.NORMAL],	Defines.FORMATS[Defines.NORMAL]);
					}
				}
				
				renderUnit.setVertexBufferAt(curUsedProgramA.aUV, uvBuffer, geometry.attrOffsetsList[Defines.TEXCOORDS[0]], Defines.FORMATS[Defines.TEXCOORDS[0]]);
				
				if (this.useVertDiffuse) {
					var vertDiffuse:VertexBuffer3D = geometry.getVertexBuffer(Defines.DIFFUSE, context3D);
					renderUnit.setVertexBufferAt(curUsedProgramA.aVertDiffuse, vertDiffuse, geometry.attrOffsetsList[Defines.DIFFUSE], Defines.FORMATS[Defines.DIFFUSE]);
				}
				object.setTransformConstants(renderUnit, surface, curUsedProgramA, camera, 0x1);
				if (camera.context3DProperties.isOpenGL && this.replaceAlpha != Defines.REPLACE_ALPHA_DEFAULT) {
					renderUnit.srcBlend = Defines.BLEND_FACTORS[Defines.BLEND_ONE];
					renderUnit.dstBlend = Defines.BLEND_FACTORS[Defines.BLEND_ONE];
				} else {
					renderUnit.srcBlend = Defines.BLEND_FACTORS[this.srcBlendValue];
					renderUnit.dstBlend = Defines.BLEND_FACTORS[this.dstBlendValue];
				}
			}
			var isEditor:Boolean = CONFIG::EDITOR;
			if (!camera.context3DProperties.isOpenGL && (object is SpriteUnit) && !isEditor) {
				renderUnit.setProjectionConstants(camera, curUsedProgramA.cProjMatrix, null);
			} else {
				renderUnit.setProjectionConstants(camera, curUsedProgramA.cProjMatrix, object.localToCameraTransform);
			}
			
			if (curUsedProgramA.cEyePos >= 0) {
				var camTransform:Transform3D = object.cameraToLocalTransform;
				renderUnit.setVertexConstantsFromNumbers(curUsedProgramA.cEyePos, camTransform.d, camTransform.h, camTransform.l, -1);
			}
			
			if (curUsedProgramA.cUVScale >= 0) {
				renderUnit.setVertexConstantsFromNumbers(curUsedProgramA.cUVScale, uvUScale, uvVScale, uvUOffset, uvVOffset);
			}
			
			if (curUsedProgramA.cUVTrans2 >= 0) {
				renderUnit.setVertexConstantsFromNumbers(curUsedProgramA.cUVTrans2, 0, 0, uv2UOffset, uv2VOffset);
			}
			
			if (curUsedProgramA.cUVTrans >= 0 && !this.closeUVTrans) {
				if (getGlobalTimer == null) {
					getGlobalTimer = getTimer;
				}
				var t:Number = getGlobalTimer()/1000;
				renderUnit.setVertexConstantsFromNumbers(curUsedProgramA.cUVTrans, uvV0, uvV1, t, this.uvAngle);
			}
			
			//Constants
			if (curUsedProgramA.cFactors >= 0) {
				renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cFactors, this.alphaSharpness, this.alphaIntensity, 0, this.alphaTest);
			}
			
			if (curUsedProgramA.cColor >= 0) {
				if (this.isGrayColor) {
					// 0.3+G*0.59+B*0.11
					renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cColor, 0.3, 0.59, 0.11, alpha);
				} else {
					renderUnit.setFragmentConstantsFromNumbers(curUsedProgramA.cColor, 
						red*camera.mulColorFactor + camera.addColorFactor, 
						green*camera.mulColorFactor + camera.addColorFactor, 
						blue*camera.mulColorFactor + camera.addColorFactor, alpha);
				}
			}
			
			if (renderUnit.culling != this.culling) {
				renderUnit.culling = this.culling;
			}
			object.setTransformConstants(renderUnit, surface, curUsedProgramA, camera, 0x2);
			if (this.openDoubleAlpha && !camera.context3DProperties.isOpenGL) {
				
				renderUnit.sortID = (curUsedProgramA.id + this.textureFiles[0].id) * 0xffffffff + geometry.id;
				camera.renderer.addRenderUnit(renderUnit, Defines.PRIORITY_OPAQUE);
				if (curUsedProgramB == null) {
					curUsedProgramB = getEffectShaderProgram(surface, camera, true, textureFilesCount);
				}
				if (surface.renderUnit1 == null) {
					surface.renderUnit1 = camera.pool.popRenderUnit();
					surface.renderUnit1.object = object;
				}
				if (surface.renderUnit1.ref != null) {
					surface.renderUnit1.ref.release();
				}
				surface.renderUnit1.ref = renderUnit;
				renderUnit.addRef();
				surface.renderUnit1.program = curUsedProgramB.getProgram3D(context3D);
				renderUnit.sortID = (curUsedProgramB.id + this.textureFiles[0].id) * 0xffffffff + geometry.id;
				camera.renderer.addRenderUnit(surface.renderUnit1, Defines.PRIORITY_TRANSPARENT_SORT);
				
			} else {
				
				renderUnit.sortID = (curUsedProgramA.id + this.textureFiles[0].id)* 0xffffffff + geometry.id;
				if (isOpenNormalAlpha(camera)) {
					camera.renderer.addRenderUnit(renderUnit, Defines.PRIORITY_OPAQUE);
				} else {
					camera.renderer.addRenderUnit(renderUnit, renderPriority);
				}
			}
			// no release for cache, renderUnit.release();
			return 1;
		}
		
		public function clone():Material 
		{
			var res:Material = Material.createMaterial();
			res.clonePropertiesFrom(this);
			return res;
		}
		
		private function contructor():void
		{
			this.startPlayTimer = -1;
			this.curUsedProgramA = null;
			this.curUsedProgramB = null;
			this.textureFiles = new Vector.<TextureFile>;
			this.textureFilesCount = 0;
			this.openDoubleAlpha= false;
			this.alpha = 1;
			this.red = 1;
			this.green = 1;		
			this.blue = 1;
			this.culling = "none";
			this.useLight = false;
			
			this.uvUOffset = 0.5;
			this.uvVOffset = 0.5;
			this.uv2UOffset = 0.5;
			this.uv2VOffset = 0.5;
			this.uvUScale = 1;
			this.uvVScale = 1;
			
			this.uvV0 = 0;
			this.uvV1 = 0;
			this.uvAngle = 0;
			//this.curTime = 0;
			
			this.renderPriority = Defines.PRIORITY_OPAQUE;
			this.useVertDiffuse = false;
			this.closeUVTrans = false;
			this.useUVType = Defines.UV_TYPE_TRANS;
			
			this.srcBlendValue = Defines.BLEND_ONE;
			this.dstBlendValue = Defines.BLEND_ZERO;
			this.replaceAlpha = Defines.REPLACE_ALPHA_DEFAULT;
			
			this.texOpIndex = 0;
			this.repeatIndex = 0;
			this.mipIndex = 0;
			this.filterIndex = 0;
			this.alphaTest = 0;
			this.alphaIntensity = 0;
			this.alphaSharpness = 0;
			
			this.hue = 1;
			this.saturation = 1;
			this.lightness = 1;
			this.type = Defines.MATERIAL_MODEL;		
			this.borderPower = 0;
			this.borderColor.setRGBA(0,0,0,0);
			this.recieveShadow = false;
			
			this.isGrayColor = false;
			this.waveContext = null;
			
			for (var i:int = 0, n:int = this.textureFiles.length; i < n; i++) {
				if (this.textureFiles[i] != null) {
					this.textureFiles[i].release();
					this.textureFiles[i] = null;
				}
			}
			
			this.textureFilesCount = 0;
			this.refCount = 0;
			
			this.programCachedID = null;
						
			if (this.tempTrans != null) {
				Pool.getInstance().pushTransform3D(this.tempTrans);
				this.tempTrans = null;
			}
			this.usedContext3D = null;
			diffuseR = 0.508235319;
			diffuseG = 0.508235319;
			diffuseB = 0.508235319;
			this.typeChanged = false;
		}
		
		protected function clonePropertiesFrom(source:Material):void 
		{
			var mat:Material = source;
			if (mat.textureFilesCount > 0) {
				this.textureFiles.length = mat.textureFilesCount;
				for (var i:int =0; i < textureFilesCount; i++) {
					this.textureFiles[i] = mat.textureFiles[i];
					this.textureFiles[i].addRef();
				}
			}
			this.textureFilesCount = mat.textureFilesCount;
			this.openDoubleAlpha = mat.openDoubleAlpha;
			this.alpha = mat.alpha;
			this.red = mat.red;
			this.green = mat.green;
			this.blue = mat.blue;
			this.culling = mat.culling;
			this.useLight = mat.useLight;
			
			this.uvUOffset = mat.uvUOffset;
			this.uvVOffset = mat.uvVOffset;
			this.uv2UOffset = mat.uv2UOffset;
			this.uv2VOffset = mat.uv2VOffset;
			this.uvUScale = mat.uvUScale;
			this.uvVScale = mat.uvVScale;
			
			this.uvV0 = mat.uvV0;
			this.uvV1 = mat.uvV1;
			this.uvAngle = mat.uvAngle;
			//this.curTime = mat.curTime;
			
			this.renderPriority = mat.renderPriority;
			this.useVertDiffuse = mat.useVertDiffuse;
			this.closeUVTrans = mat.closeUVTrans;
			this.useUVType = mat.useUVType;
			
			this.srcBlendValue = mat.srcBlendValue;
			this.dstBlendValue = mat.dstBlendValue;
			this.replaceAlpha = mat.replaceAlpha;
			
			this.texOpIndex = mat.texOpIndex;
			this.repeatIndex = mat.repeatIndex;
			this.mipIndex = mat.mipIndex;
			this.filterIndex = mat.filterIndex;
			this.alphaTest = mat.alphaTest;
			this.alphaIntensity = mat.alphaIntensity;
			this.alphaSharpness = mat.alphaSharpness;
			
			this.hue = mat.hue;
			this.saturation = mat.saturation;
			this.lightness = mat.lightness;
			this.type = mat.type;
			this.borderPower = mat.borderPower;
			this.borderColor.copy(mat.borderColor);
			
			this.recieveShadow = mat.recieveShadow;
			
			this.isGrayColor = mat.isGrayColor;
			
			if (mat.waveContext != null) {
				this.waveContext = new Vector.<Colour>(mat.waveContext.length);
				
				for (var j:int = 0; j < mat.waveContext.length; j++) {
					var item:Colour = mat.waveContext[j];
					this.waveContext[j] = new Colour;
					this.waveContext[j].copy(mat.waveContext[j]);
				}
			}
			this.startPlayTimer = -1;
			
			diffuseR = mat.diffuseR;
			diffuseG = mat.diffuseG;
			diffuseB = mat.diffuseB;
			this.markProgramDirty();
		}
		
		private function destructor():void
		{
			this.curUsedProgramA = null;
			this.curUsedProgramB = null;
			this.programCachedID = null;
			var i:int;
			var n:int;
			
			for (i = 0; i < textureFilesCount; i++) {
				this.textureFiles[i].release();
				this.textureFiles[i] = null;
			}
			this.textureFiles.length = 0;
			this.textureFilesCount = 0;
			if (this.tempTrans != null) {
				Pool.getInstance().pushTransform3D(this.tempTrans);
				this.tempTrans = null;
			}
		}
		
		public function addRef():void
		{
			this.refCount++;
		}
		
		public function release():void
		{
			this.refCount--;
			if (this.refCount == 0) {
				this.destructor();
				Pool.getInstance().pushMaterial(this);
			}
		}
		
	}
}
