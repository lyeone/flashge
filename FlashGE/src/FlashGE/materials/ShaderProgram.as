// 2014-3-10 lye
package FlashGE.materials
{
	import flash.display3D.Context3D;
	import flash.display3D.Program3D;
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Global;
	import FlashGE.materials.compiler.Linker;
	
	use namespace FlashGE;
	public class ShaderProgram
	{		
		private var refCount:int = 0;
		private var context3D:Context3D;
		private var program3D:Program3D;
		private var invalidShader:Boolean;
		
		FlashGE var vertexLinker:Linker;
		FlashGE var fragmentLinker:Linker;
		
		FlashGE var aPosition:int = -1;
		FlashGE var aNormal:int = -1;
		FlashGE var aUV:int = -1;
		FlashGE var cProjMatrix:int = -1;
		
		FlashGE var cColor:int = -1;
		FlashGE var sTextures:Vector.<int> = new <int>[-1,-1,-1];
		FlashGE var aVertDiffuse:int = -1;
		FlashGE var cUVScale:int = -1;
		FlashGE var cUVTrans:int = -1;
		FlashGE var cUVTrans2:int = -1;
		FlashGE var cDistortion:int = -1;
		FlashGE var cFactors:int = -1;
		FlashGE var cDecode01:int = -1;
		FlashGE var cDecode02:int = -1;
		FlashGE var cHSLC0:int = -1;
		FlashGE var cHSLC1:int = -1;
		FlashGE var cHSLC2:int = -1;
		FlashGE var cHSLC3:int = -1;
		FlashGE var cJoint0:int = -1;
		FlashGE var cJoint1:int = -1;
		FlashGE var cJoint2:int = -1;
		FlashGE var cJoint3:int = -1;
		FlashGE var cWing0:int = -1;
		FlashGE var cWing1:int = -1;
		FlashGE var cWing2:int = -1;
		
		FlashGE var cAmbient:int = -1; // (r, g, b, 0)
		FlashGE var cDiffuse:int = -1; // (r, g, b, borderIntensity)
		FlashGE var cSpecular:int = -1;// (r, g, b, power)
		FlashGE var cLightDir:int = -1;// (x, y, z, 0)
		FlashGE var cEyePos:int = -1;  // (x, y, z, 0)
		FlashGE var cBorderColor:int = - 1; //(r, g, b, borderPower)
		FlashGE var cShadowScale:int = -1;
		FlashGE var cShadowConstants:int = -1;
		FlashGE var cShadowMapUVProjection:int = -1;
		FlashGE var sShadowMap:int = -1;
		FlashGE var cShadowDist:int = -1;
		
		private var vertexBytes:ByteArray;
		private var fragmentBytes:ByteArray;
		
		public var id:Number;
		private static var idRander:int = 0;
		
		public function ShaderProgram(vertexLinker:Linker, fragmentLinker:Linker)
		{
			id = (idRander++) * 0xffff;
			this.vertexLinker = vertexLinker;
			this.fragmentLinker = fragmentLinker;
			this.invalidShader = false;
		}
		
		public function write(rawData:ByteArray):void
		{
			rawData.writeByte(aPosition);
			rawData.writeByte(aNormal);
			rawData.writeByte(aUV);
			rawData.writeByte(cProjMatrix);
			
			rawData.writeByte(cColor);
			rawData.writeByte(sTextures[0]);
			rawData.writeByte(sTextures[1]);
			rawData.writeByte(sTextures[2]);
			rawData.writeByte(aVertDiffuse);
			rawData.writeByte(cUVScale);
			rawData.writeByte(cUVTrans);
			rawData.writeByte(cUVTrans2);
			rawData.writeByte(cDistortion);
			rawData.writeByte(cFactors);
			rawData.writeByte(cDecode01);
			rawData.writeByte(cDecode02);
			rawData.writeByte(cHSLC0);
			rawData.writeByte(cHSLC1);
			rawData.writeByte(cHSLC2);
			rawData.writeByte(cHSLC3);
			rawData.writeByte(cJoint0);
			rawData.writeByte(cJoint1);
			rawData.writeByte(cJoint2);
			rawData.writeByte(cJoint3);
			rawData.writeByte(cWing0);
			rawData.writeByte(cWing1);
			rawData.writeByte(cWing2);
			
			rawData.writeByte(cAmbient);
			rawData.writeByte(cDiffuse);
			rawData.writeByte(cSpecular);
			rawData.writeByte(cLightDir);
			rawData.writeByte(cEyePos);
			rawData.writeByte(cBorderColor);
			rawData.writeByte(cShadowScale);
			rawData.writeByte(cShadowConstants);
			rawData.writeByte(cShadowMapUVProjection);
			rawData.writeByte(sShadowMap);
			rawData.writeByte(cShadowDist);
			var dataLen:int;
			if (this.vertexBytes == null) {
				dataLen = this.vertexLinker.data.length;
				rawData.writeShort(dataLen);
				rawData.writeBytes(this.vertexLinker.data);
			} else {
				dataLen = this.vertexBytes.length;
				rawData.writeShort(dataLen);
				rawData.writeBytes(this.vertexBytes);
			}
			
			if (this.fragmentBytes == null) {
				dataLen = this.fragmentLinker.data.length;
				rawData.writeShort(dataLen);
				rawData.writeBytes(this.fragmentLinker.data);
			} else {
				dataLen = this.fragmentBytes.length;
				rawData.writeShort(dataLen);
				rawData.writeBytes(this.fragmentBytes);
			}
		}
		
		public function read(rawData:ByteArray):void
		{
			aPosition = rawData.readByte();
			aNormal = rawData.readByte();
			aUV = rawData.readByte();
			cProjMatrix = rawData.readByte();
			
			cColor = rawData.readByte();
			sTextures[0] = rawData.readByte();
			sTextures[1] = rawData.readByte();
			sTextures[2] = rawData.readByte();
			aVertDiffuse = rawData.readByte();
			cUVScale = rawData.readByte();
			cUVTrans = rawData.readByte();
			cUVTrans2 = rawData.readByte();
			cDistortion = rawData.readByte();
			cFactors = rawData.readByte();
			cDecode01 = rawData.readByte();
			cDecode02 = rawData.readByte();
			cHSLC0 = rawData.readByte();
			cHSLC1 = rawData.readByte();
			cHSLC2 = rawData.readByte();
			cHSLC3 = rawData.readByte();
			cJoint0 = rawData.readByte();
			cJoint1 = rawData.readByte();
			cJoint2 = rawData.readByte();
			cJoint3 = rawData.readByte();
			cWing0 = rawData.readByte();
			cWing1 = rawData.readByte();
			cWing2 = rawData.readByte();
			
			cAmbient = rawData.readByte();
			cDiffuse = rawData.readByte();
			cSpecular = rawData.readByte();
			cLightDir = rawData.readByte();
			cEyePos = rawData.readByte();
			cBorderColor = rawData.readByte();
			cShadowScale = rawData.readByte();
			cShadowConstants = rawData.readByte();
			cShadowMapUVProjection = rawData.readByte();
			sShadowMap = rawData.readByte();
			cShadowDist = rawData.readByte();
			
			var vertexLen:int = rawData.readShort();
			this.vertexBytes = new ByteArray;
			this.vertexBytes.endian = Endian.LITTLE_ENDIAN;
			rawData.readBytes(this.vertexBytes, 0, vertexLen);
			this.vertexBytes.position = 0;
			
			var fragmentLen:int = rawData.readShort();
			this.fragmentBytes = new ByteArray;
			this.fragmentBytes.endian = Endian.LITTLE_ENDIAN;
			rawData.readBytes(this.fragmentBytes, 0, fragmentLen);
			this.fragmentBytes.position = 0;
		}
		
		
		public function addRef():void
		{
			this.refCount++;
		}
		
		public function release():void
		{
			this.refCount--;
			if (this.refCount==0) {
				if (this.program3D!=null) {
					this.program3D.dispose();
					this.program3D = null;
				}
				context3D = null;
			}
		}
		
		public function getProgram3D(context3D:Context3D):Program3D
		{
			if (invalidShader) {
				return null;
			}
			
			if (context3D != null && (this.program3D==null || this.context3D != context3D)) {

				if (this.vertexBytes == null) {
					this.upload(context3D);
				} else {
					this.uploadRawData(context3D);
				}
			}
			return this.program3D;
		}
		
		private function uploadRawData(context3D:Context3D):void
		{
			if (program3D != null && this.context3D != context3D) {
				program3D.dispose();
				program3D = null;
			}
			
			try {
				
				program3D = context3D.createProgram();
				program3D.upload(this.vertexBytes, this.fragmentBytes);
				this.context3D = context3D;
			} catch (e:Error) {
				invalidShader = true;
				program3D = null;
				CONFIG::DEBUG {
					Global.throwError("ShaderProgram::uploadRawData traceback=" + (new Error).getStackTrace());
				}
			}
		}
		
		private function upload(context3D:Context3D):void 
		{
			var restore:Boolean = false;
			if (program3D != null && this.context3D != context3D) {
				program3D.dispose();
				program3D = null;
				restore = true;
			}
			
			if (vertexLinker != null && fragmentLinker != null) {
				
				if (vertexLinker.data == null) {
					vertexLinker.link(1);
				}
				
				if (fragmentLinker.data == null) {
					fragmentLinker.link(1);
				}
				
				try {
					program3D = context3D.createProgram();
					program3D.upload(vertexLinker.data, fragmentLinker.data);
				} catch (e:Error) {
					invalidShader = true;
					program3D = null;
					var s:String = (A3DUtils.disassemble(vertexLinker.data)) +
						vertexLinker.describeLinkageInfo() +
						A3DUtils.disassemble(fragmentLinker.data) +
						fragmentLinker.describeLinkageInfo() + (new Error).getStackTrace();
					
					CONFIG::DEBUG {
						Global.throwError(s);
					}
					return;
				}

				if (restore == false) {
					aPosition = vertexLinker.findVariable("aPosition");
					aNormal = vertexLinker.findVariable("aNormal");
					aUV = vertexLinker.findVariable("aUV");
					cEyePos = vertexLinker.findVariable("cCameraPos");
					cProjMatrix = vertexLinker.findVariable("cProjMatrix");
					aVertDiffuse = vertexLinker.findVariable("aVertDiffuse");
					cDecode01 = vertexLinker.findVariable("cDecode01");
					cDecode02 = vertexLinker.findVariable("cDecode02");
					cUVScale = vertexLinker.findVariable("cUVScale");
					cUVTrans = vertexLinker.findVariable("cUVTrans");
					cUVTrans2 = vertexLinker.findVariable("cUVTrans2");
					cShadowScale = vertexLinker.findVariable("cShadowScale");
					cShadowMapUVProjection = vertexLinker.findVariable("cShadowMapUVProjection");
					cJoint0 = vertexLinker.findVariable("joint0");
					cJoint1 = vertexLinker.findVariable("joint1");
					cJoint2 = vertexLinker.findVariable("joint2");
					cJoint3 = vertexLinker.findVariable("joint3");
					cWing0 = vertexLinker.findVariable("cWing0");
					cWing1 = vertexLinker.findVariable("cWing1");
					cWing2 = vertexLinker.findVariable("cWing2");
					
					cLightDir = fragmentLinker.findVariable("cLightDir");
					cDiffuse = fragmentLinker.findVariable("cDiffuse");
					cAmbient = fragmentLinker.findVariable("cAmbient");
					cSpecular = fragmentLinker.findVariable("cSpecular");
					cBorderColor = fragmentLinker.findVariable("cBorder");
					cHSLC0 = fragmentLinker.findVariable("cHSLC0");
					cHSLC1 = fragmentLinker.findVariable("cHSLC1");
					cHSLC2 = fragmentLinker.findVariable("cHSLC2");
					cHSLC3 = fragmentLinker.findVariable("cHSLC3");
					cColor = fragmentLinker.findVariable("cColor");
					cFactors = fragmentLinker.findVariable("cFactors");
					cShadowConstants = fragmentLinker.findVariable("cShadowConstants");
					sShadowMap = fragmentLinker.findVariable("sShadowMap");
					cShadowDist = fragmentLinker.findVariable("cShadowDist");
					cDistortion = fragmentLinker.findVariable("cDistortion");
					
					for (var i:int = 0; i < 3; i++) {
						sTextures[i] = fragmentLinker.findVariable("sTexture"+ String(i));
					}
				}
				this.context3D = context3D;
			}

		}
	}
}