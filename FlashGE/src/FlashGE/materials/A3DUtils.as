/**
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * If it is not possible or desirable to put the notice in a particular file, then You may include the notice in a location (such as a LICENSE file in a relevant directory) where a recipient would be likely to look for such a notice.
 * You may add additional accurate notices of copyright ownership.
 *
 * It is desirable to notify that Covered Software was "Powered by AlternativaPlatform" with link to http://www.alternativaplatform.com/ 
 * */

package FlashGE.materials {

	import FlashGE.FlashGE;
	import FlashGE.materials.compiler.CommandType;
	import FlashGE.materials.compiler.VariableType;

	import avmplus.getQualifiedSuperclassName;

	import flash.display3D.Context3D;
	import flash.display3D.Context3DTextureFormat;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.VertexBuffer3D;
	import flash.display3D.textures.Texture;
	import flash.geom.Point;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import flash.utils.Endian;
	import flash.utils.getDefinitionByName;

	/**
	 * @private
	 */
	public class A3DUtils {
		

		private static function getData(source:Vector.<int>):ByteArray {
			var result:ByteArray = new ByteArray();
			for (var i:int = 0, length:int = source.length; i < length; i++) {
				result.writeByte(source[i]);
			}
			return result;
		}

		public static function vectorNumberToByteArray(vector:Vector.<Number>):ByteArray {
			var result:ByteArray = new ByteArray();
			result.endian = Endian.LITTLE_ENDIAN;
			for (var i:int = 0; i < vector.length; i++) {
				result.writeFloat(vector[i]);
			}
			result.position = 0;
			return result;
		}

		public static function byteArrayToVectorUint(byteArray:ByteArray):Vector.<uint> {
			var result:Vector.<uint> = new Vector.<uint>();
			var length:uint = 0;
			byteArray.position = 0;
			byteArray.endian = Endian.LITTLE_ENDIAN;
			while (byteArray.bytesAvailable > 0) {
				result[length++] = byteArray.readUnsignedShort();
			}
			return result;
		}

		// Disassembler
		private static var programType:Vector.<String> = Vector.<String>(["VERTEX", "FRAGMENT"]);
		private static var samplerDimension:Vector.<String> = Vector.<String>(["2D", "cube", "3D"]);
		private static var samplerWraping:Vector.<String> = Vector.<String>(["clamp", "repeat"]);
		private static var samplerMipmap:Vector.<String> = Vector.<String>(["mipnone", "mipnearest", "miplinear"]);
		private static var samplerFilter:Vector.<String> = Vector.<String>(["nearest", "linear"]);
		private static var swizzleType:Vector.<String> = Vector.<String>(["x", "y", "z", "w"]);
		private static var twoOperandsCommands:Dictionary;

		// TODO: option to turn off auto-prefixes
		public static function disassemble(byteCode:ByteArray):String {
			if (!twoOperandsCommands) {
				twoOperandsCommands = new Dictionary();
				twoOperandsCommands[0x1] = true;
				twoOperandsCommands[0x2] = true;
				twoOperandsCommands[0x3] = true;
				twoOperandsCommands[0x4] = true;
				twoOperandsCommands[0x6] = true;
				twoOperandsCommands[0xb] = true;
				twoOperandsCommands[0x11] = true;
				twoOperandsCommands[0x12] = true;
				twoOperandsCommands[0x13] = true;
				twoOperandsCommands[0x17] = true;
				twoOperandsCommands[0x18] = true;
				twoOperandsCommands[0x19] = true;
				twoOperandsCommands[0x26] = true;
				twoOperandsCommands[0x28] = true;
				twoOperandsCommands[0x29] = true;
				twoOperandsCommands[0x2a] = true;
				twoOperandsCommands[0x2c] = true;
				twoOperandsCommands[0x2d] = true;
			}
			var res:String = "";
			byteCode.position = 0;
			if (byteCode.bytesAvailable < 7) {
				return "error in byteCode header";
			}

			res += "magic = " + byteCode.readUnsignedByte().toString(16);
			res += "\nversion = " + byteCode.readInt().toString(10);
			res += "\nshadertypeid = " + byteCode.readUnsignedByte().toString(16);
			var pType:String = programType[byteCode.readByte()];
			res += "\nshadertype = " + pType;
			res += "\nsource\n";
			pType = pType.substring(0, 1).toLowerCase();
			var lineNumber:uint = 1;
			while (byteCode.bytesAvailable - 24 >= 0) {
				res += (lineNumber++).toString() + ": " + getCommand(byteCode, pType) + "\n";
			}
			if (byteCode.bytesAvailable > 0) {
				res += "\nunexpected byteCode length. extra bytes:" + byteCode.bytesAvailable;
			}
			return res;
		}

		private static function getCommand(byteCode:ByteArray, programType:String):String {
			var cmd:uint = byteCode.readUnsignedInt();
			var command:String = CommandType.COMMAND_NAMES[cmd];
			var result:String;
			var destNumber:uint = byteCode.readUnsignedShort();
			var swizzle:uint = byteCode.readByte();
			var s:String = "";
			var destSwizzle:uint = 4;
			if (swizzle < 15) {
				s += ".";
				s += ((swizzle & 0x1) > 0) ? "x" : "";
				s += ((swizzle & 0x2) > 0) ? "y" : "";
				s += ((swizzle & 0x4) > 0) ? "z" : "";
				s += ((swizzle & 0x8) > 0) ? "w" : "";
				destSwizzle = s.length - 1;
			}

			var sourceSwizzleLimit:int = destSwizzle;
			if (cmd == CommandType.TEX) {
				sourceSwizzleLimit = 2;
			} else if (cmd == CommandType.DP3) {
				sourceSwizzleLimit = 3;
			} else if (cmd == CommandType.DP4) {
				sourceSwizzleLimit = 4;
			}

			var destType:String = VariableType.TYPE_NAMES[byteCode.readUnsignedByte()].charAt(0);
			result = command + " " + attachProgramPrefix(destType, programType) + destNumber.toString() + s + ", ";
			result += attachProgramPrefix(getSourceVariable(byteCode, sourceSwizzleLimit), programType);

			if (twoOperandsCommands[cmd]) {
				if (cmd == CommandType.TEX || cmd == CommandType.TED) {
					result += ", " + attachProgramPrefix(getSamplerVariable(byteCode), programType);
				} else {
					result += ", " + attachProgramPrefix(getSourceVariable(byteCode, sourceSwizzleLimit), programType);
				}
			} else {
				byteCode.readDouble();
			}
			
			if (cmd == CommandType.ELS || cmd == CommandType.EIF) {
				result = " " + command;
			}
			return result;
		}

		private static function attachProgramPrefix(variable:String, programType:String):String {
			var char:uint = variable.charCodeAt(0);
			if (char == "o".charCodeAt(0)) {
				return variable + (programType == "f" ? "c" : "p");
			} else if (char == "d".charCodeAt(0)) {
				return "o"+variable;
			} else if (char != "v".charCodeAt(0)) {
				return programType + variable;
			}
			return variable;
		}

		private static function getSamplerVariable(byteCode:ByteArray):String {
			var number:uint = byteCode.readUnsignedInt();
			byteCode.readByte();
			var dim:uint = byteCode.readByte() >> 4;
			var wraping:uint = byteCode.readByte() >> 4;
			var n:uint = byteCode.readByte();
			return "s" + number.toString() + " <" + samplerDimension[dim] + ", " + samplerWraping[wraping]
					+ ", " + samplerFilter[(n >> 4) & 0xf] + ", " + samplerMipmap[n & 0xf] + ">";
		}

		private static function getSourceVariable(byteCode:ByteArray, swizzleLimit:uint):String {
			var s1Number:uint = byteCode.readUnsignedShort();
			var offset:uint = byteCode.readUnsignedByte();
			var s:String = getSourceSwizzle(byteCode.readUnsignedByte(), swizzleLimit);

			var s1Type:String = VariableType.TYPE_NAMES[byteCode.readUnsignedByte()].charAt(0);
			var indexType:String = VariableType.TYPE_NAMES[byteCode.readUnsignedByte()].charAt(0);
			var comp:String = swizzleType[byteCode.readUnsignedByte()];

			if (byteCode.readUnsignedByte() > 0) {
				return	 s1Type + "[" + indexType + s1Number.toString() + "." + comp + ((offset > 0) ? ("+" + offset.toString()) : "") + "]" + s;
			}
			return s1Type + s1Number.toString() + s;
		}

		private static function getSourceSwizzle(swizzle:uint, swizzleLimit:uint = 4):String {
			var s:String = "";
			if (swizzle != 0xe4) {
				s += ".";
				s += swizzleType[(swizzle & 0x3)];
				s += swizzleType[(swizzle >> 2) & 0x3];
				s += swizzleType[(swizzle >> 4) & 0x3];
				s += swizzleType[(swizzle >> 6) & 0x3];
				s = swizzleLimit < 4 ? s.substring(0, swizzleLimit + 1) : s;
			}
			return s;
		}

		FlashGE static function checkParent(child:Class, parent:Class):Boolean {
			var current:Class = child;
			if (parent == null) return true;
			while (true) {
				if (current == parent) return true;
				var className:String = getQualifiedSuperclassName(current);
				if (className != null) {
					current = getDefinitionByName(className) as Class;
				} else return false;
			}
			return false;
		}

	}
}
