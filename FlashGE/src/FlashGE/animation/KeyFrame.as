// 2014-3-10 lye

package FlashGE.animation
{
	import FlashGE.FlashGE;
	import FlashGE.common.Transform3D;

	use namespace FlashGE;	
	
	public class KeyFrame 
	{
		FlashGE var transform:Transform3D;		
		
		FlashGE var axisX:Number;
		FlashGE var axisY:Number;
		FlashGE var axisZ:Number;
		FlashGE var cos_angle:Number;
		
		FlashGE var x:Number;
		FlashGE var y:Number;
		FlashGE var z:Number;
		FlashGE var sin_angle:Number;		
	}
}