// 2014-3-10 lye

package FlashGE.animation
{
	import FlashGE.FlashGE;
	import FlashGE.objects.Bone;
	
	use namespace FlashGE;
	
	public class Track
	{
		FlashGE var boneName:String;
		FlashGE var boneInstance:Bone;
		FlashGE var keyFrameList:Vector.<KeyFrame>;
		
		public function Track(boneName:String) 
		{
			this.boneName = boneName;
		}
		
		public function dispose():void
		{
			this.boneName = null;
			this.boneInstance = null;
			var kf:KeyFrame;
			if (this.keyFrameList != null) {
				for (var i:int = 0, n:int = this.keyFrameList.length; i < n; i++) {
					kf = this.keyFrameList[i];
					kf.transform = null;
				}
				this.keyFrameList = null;
			}
		}
	}
}
