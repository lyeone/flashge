// 2014-3-10 lye

package FlashGE.animation 
{
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.geom.Vector3D;
	import flash.utils.ByteArray;
	import flash.utils.CompressionAlgorithm;
	import flash.utils.Dictionary;
	import flash.utils.Endian;
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.loaders.RawDataReader;
	import FlashGE.objects.Skeleton;
	import FlashGE.resources.LoaderMgr3D;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;
	
	public class SkeletonAni
	{
		static FlashGE var skeletonAnis:Dictionary = new Dictionary;
		static private var recycledFiles:Dictionary = new Dictionary;
		
		FlashGE var loadState:int = Defines.LOAD_NULL;
		FlashGE var fileName:String;
		FlashGE var skeleton:Skeleton;
		FlashGE var maxFrameCount:int = 0;
		FlashGE	var trackList:Vector.<Track> = new Vector.<Track>;
		
		FlashGE var loadListeners:Vector.<Function> = null;
		FlashGE var rootBoneOffset:Number = 0;
		FlashGE var name:String;
		FlashGE var needLoadOK:Boolean = false;
		FlashGE var getCurTimer:Function = getTimer;
		FlashGE var refCount:int = 0;
		private var usedFrame:int = 0;
		FlashGE var hasDisposed:Boolean = false;
		private var pool:Pool;
		public var interval:Number;
		public function addRef():void
		{
			var ret:SkeletonAni = SkeletonAni.recycledFiles[fileName];
			if (ret != null) {
				SkeletonAni.skeletonAnis[fileName] = ret;
				delete SkeletonAni.recycledFiles[fileName];
			}
			
			this.usedFrame = Global.frameCount;
			this.refCount++;
		}
		
		public function release():void
		{
			if (this.refCount > 0) {
				this.refCount--;
			} else {
				CONFIG::DEBUG {
					var e:Error = new Error;
					Global.log("SkeletonAni::release invalid refCount" + e.getStackTrace());
				}
			}
			if (this.refCount == 0) {
				if (Global.useCacheData) {
					delete SkeletonAni.skeletonAnis[fileName];
					SkeletonAni.recycledFiles[fileName] = this;
				} else {
					this.dispose();
				}
			}
		}
		
		private function dispose():void
		{
			RawDataReader.getInstance().removeListener(parseRawData);
			this.fileName = null;
			this.hasDisposed = true;
			this.skeleton = null;
			
			if (this.trackList != null) {
				var track:Track;
				for (var i:int = 0, n:int = this.trackList.length; i < n; i++) {
					track = this.trackList[i];
					if (track != null) {
						track.dispose();
					}
				}
				this.trackList = null;
			}
			
			if (loadListeners != null) {
				for (i = 0, n = this.loadListeners.length; i < n; i++) {
					if (this.loadListeners[i] != null) {
						this.loadListeners[i] = null;
					}
				}
				loadListeners = null;
			}
		}
		
		static public function checkMemory():void
		{
			var tmpAni:SkeletonAni = null;
			var hasDisposed:Boolean = false;
			for each(tmpAni in SkeletonAni.recycledFiles) {
				if (hasDisposed==false) {
					hasDisposed = true;
				}
				tmpAni.dispose();
			}
			if (hasDisposed) {
				SkeletonAni.recycledFiles = new Dictionary;
			}
		}
		
		public function SkeletonAni(ske:Skeleton)
		{			
			this.skeleton = ske;
		}		
		
		public function toString():String
		{
			return "ani(refCount=" + this.refCount + ",fileName=" + this.fileName + ",loadState="+loadState +",maxFrameCount="+maxFrameCount + ")";
		}
		
		static public function printAll():String
		{
			var ani:SkeletonAni = null;
			var ret:String = "SkeletonAni::printAll cache:\n";
			for each(ani in SkeletonAni.skeletonAnis) {
				ret += ani.toString() + "\n";
			}
			
			ret += "SkeletonAni::printAll recycled:\n";
			for each(ani in SkeletonAni.recycledFiles) {
				ret += ani.toString() + "\n";
			}
			
			ret += "SkeletonAni::printAll end\n";
			return ret;
		}
		
		static public function create(skeleton:Skeleton, 
									  fileName:String, 
									  loadListener:Function = null, 
									  insertFirst:Boolean = false,
									  failedIfFirstLoad:Boolean = false, 
									  idleLoader:Boolean = false, moveToHighPriority:Boolean = false):SkeletonAni
		{
			var ret:SkeletonAni;//
			if (Global.useCacheData) {
				ret = SkeletonAni.recycledFiles[fileName];
				if (ret == null) {
					ret = SkeletonAni.skeletonAnis[fileName];
				} else {
					SkeletonAni.skeletonAnis[fileName] = ret;
					delete SkeletonAni.recycledFiles[fileName];
				}
			}
			
			if (ret == null) {
				ret = new SkeletonAni(skeleton);
				if (Global.useCacheData) {
					skeletonAnis[fileName] = ret;
				}
				ret.addRef();
				ret.loadState = Defines.LOAD_ING;
				ret.load(fileName, insertFirst, idleLoader, moveToHighPriority);
				if (failedIfFirstLoad) {
					if (loadListener != null) {
						loadListener(ret, false);
					}
				} else {
					ret.addListener(loadListener);
				}
			} else {
				ret.skeleton = skeleton;
				ret.addRef();
				if (idleLoader == false) {
					LoaderMgr3D.getInstance().checkIdle(fileName, insertFirst, moveToHighPriority);
				}
				
				if (failedIfFirstLoad) {
					if (ret.loadState != Defines.LOAD_OK && ret.loadState != Defines.LOAD_FAILD) {
						if (loadListener != null) {
							loadListener(ret, false);
						}
					} else {
						ret.addListener(loadListener);
					}
				} else {
					ret.addListener(loadListener);
				}
			}
			return ret;
		}		
		
		public function isNeedLoadOK():Boolean
		{
			return this.needLoadOK
		}
		
		private var lockAddListener:Boolean = false;
		private function addListener(listener:Function):void
		{
			if (listener==null) return;
			if (lockAddListener) {
				CONFIG::DEBUG {
					Global.warning("skeletonAni::addListener locked, ani = " + this.getFileName());
				}
				return;
			}
			
			if (this.loadState == Defines.LOAD_FAILD ||
				this.loadState == Defines.LOAD_OK) {
				listener(this, this.loadState == Defines.LOAD_OK);
			} else {
				
				if (loadListeners == null) {
					loadListeners = new Vector.<Function>;
				}
				
				for (var i:int = 0, n:int = this.loadListeners.length; i < n; i++) {
					if (this.loadListeners[i] == null) {
						this.loadListeners[i] = listener;
						return;
					}
				}
				this.loadListeners.push(listener);
			}
		}
		
		public function removeLoadListener(listener:Function):void
		{
			if (listener == null) return;
			if (loadListeners == null) {
				loadListeners = new Vector.<Function>;
			}
			
			for (var i:int = 0, n:int = this.loadListeners.length; i < n; i++) {
				if (this.loadListeners[i] == listener) {
					this.loadListeners[i] = null;
					return;
				}
			}
		}
		
		private function postListeners(isOK:Boolean):void
		{
			if (this.loadListeners == null) {
				return;
			}
			lockAddListener = true;
			var listeners:Vector.<Function> = loadListeners;
			this.loadListeners = null;
			for (var i:int = 0, n:int = listeners.length; i < n; i++) {
				if (listeners[i] != null) {
					listeners[i](this, isOK);
				}
			}
			lockAddListener = false;
		}
		
		public function getMaxFrame():int
		{
			return this.maxFrameCount;
		}
		
		public function getFileName():String
		{
			return this.fileName;
		}
		
		public function getName():String
		{
			return this.name;	
		}
		
		public function getLoadState():int
		{
			return this.loadState;	
		}
		
		public function isLoaded():Boolean
		{
			return this.loadState == Defines.LOAD_OK || this.loadState == Defines.LOAD_FAILD;
		}
		
		private function load(fileName:String, insertFirst:Boolean, idleLoader:Boolean = false, moveToHighPriority:Boolean = false):void
		{
			this.fileName = fileName;
			this.name = fileName.split("/").pop();
			this.name = this.name.substr(0, this.name.length - 4);
			if (this.fileName.indexOf("female/run.ani") >= 0 ||
				this.fileName.indexOf("male/run.ani") >= 0) {
				this.interval = 100.0/1000.0; 
			} else {
				this.interval = 24.0/1000.0;
			}
			needLoadOK = (this.name.indexOf("stand") >= 0);
			if (fileName.indexOf(".ani") >= 0) {
				if (moveToHighPriority) {
					LoaderMgr3D.getInstance().pushHighPriorityLoader(fileName, onLoadComplete, true, true, insertFirst);
				} else if (idleLoader) {
					LoaderMgr3D.getInstance().pushIdleLoader(fileName, onLoadComplete, true, true, insertFirst);
				} else {
					LoaderMgr3D.getInstance().pushLoader(fileName, onLoadComplete, true, true, insertFirst);
				}
			} else {
				onLoadComplete(null);
			}
		}
		
		FlashGE var dataContext:RawDataContext;
		
		private function onLoadComplete(rawData:ByteArray, isHighPriority:Boolean = false):void
		{
			this.usedFrame = Global.frameCount;
			if (hasDisposed) {
				this.loadState = Defines.LOAD_FAILD;
				postListeners(false);
				return;
			}
			
			if (rawData == null) {
				postListeners(false);
				this.loadState = Defines.LOAD_FAILD;
				// todo:lye
				//Global.throwError("Not Exist File:"+this.fileName);
				return;
			}
			
			dataContext = new RawDataContext;
			dataContext.raw = rawData;
			dataContext.raw.endian = Endian.LITTLE_ENDIAN;
			dataContext.raw.position = 0;
			RawDataReader.getInstance().addListener(parseRawData, isHighPriority);
		}
		
		private function parseRawData(timer:int):int
		{
			if (pool == null) {
				pool = Pool.getInstance();
			}
			try {
				var curTimer:int = getCurTimer();
				if (dataContext.version == -1) {
					var tag:String = dataContext.raw.readUTFBytes(7);
					if (tag == "deflate") {
						var temp:ByteArray = new ByteArray;
						temp.endian = Endian.LITTLE_ENDIAN;
						temp.position = 0;
						dataContext.raw.readBytes(temp, 0, dataContext.raw.length - 7);
						temp.uncompress(CompressionAlgorithm.ZLIB);
						dataContext.raw = temp;
					}
					dataContext.raw.position = 0;				
					dataContext.hasSignature = false;
					
					dataContext.version = dataContext.raw.readUnsignedShort();				
					dataContext.boneCount = dataContext.raw.readByte();
					if (dataContext.boneCount < 0) {
						dataContext.raw = null;
						dataContext.trackNameList = null;
						dataContext = null;
						RawDataReader.getInstance().removeListener(parseRawData);
						this.loadState = Defines.LOAD_FAILD;
						postListeners(false);
						return timer - (getCurTimer() - curTimer); 
					} else {
						maxFrameCount = dataContext.raw.readUnsignedShort();
					}
					
					if (getCurTimer() - curTimer >= timer) {
						return 0;
					}
				}
				
				var len:int = 0;
				var i:int = 0;
				var name:String;
				if (dataContext.trackNameList == null) {
					dataContext.trackNameList = new Vector.<String>;
					dataContext.trackNameList.length = dataContext.boneCount;
					for (i=0; i <dataContext.boneCount; i++) {
						len = dataContext.raw.readUnsignedByte();
						name = dataContext.raw.readUTFBytes(len);
						dataContext.trackNameList[i] = name;
						if (name =="Bip001") {
							dataContext.rootBoneIndex = i;
						}
					}
					
					trackList.length = dataContext.boneCount;
					if (getCurTimer() - curTimer >= timer) {
						return 0;
					}
				}
				
				var index:int = 0;
				//var keyNum:int = 0;
				//var track:Track;
				var data:ByteArray = dataContext.raw;
				while (dataContext.boneIndex < dataContext.boneCount) {
					if (dataContext.stepTrack == null) {
						index = data.readUnsignedByte();
						dataContext.stepTrack = new Track(dataContext.trackNameList[index]);
						dataContext.stepTrack.keyFrameList = new Vector.<KeyFrame>(maxFrameCount)
						this.trackList[dataContext.boneIndex] = dataContext.stepTrack;
						dataContext.stepJ = 0;
					}
					
					while (dataContext.stepJ < maxFrameCount) {
						var keyFrame:KeyFrame = new KeyFrame;
						var time:Number = data.readFloat();
						keyFrame.axisX= data.readFloat();
						keyFrame.axisY= data.readFloat();
						keyFrame.axisZ= data.readFloat();
						keyFrame.cos_angle= data.readFloat();
						keyFrame.x = data.readFloat();
						keyFrame.y = data.readFloat();
						keyFrame.z = data.readFloat();
						keyFrame.sin_angle= data.readFloat();
						
						var axisX:Number = data.readFloat();
						var axisY:Number = data.readFloat();
						var axisZ:Number = data.readFloat();
						var angle:Number = data.readFloat();
						
						var x0:Number = data.readFloat();
						var y0:Number = data.readFloat();
						var z0:Number = data.readFloat();
						keyFrame.transform = new Transform3D;
						keyFrame.transform.makeRotation(axisX, axisY, axisZ, angle);
						keyFrame.transform.d = x0;
						keyFrame.transform.h = y0;
						keyFrame.transform.l = z0;
						
						dataContext.stepTrack.keyFrameList[dataContext.stepJ] = keyFrame;
						dataContext.stepJ++;
						if (getCurTimer() - curTimer >= timer) {
							return 0;
						}
					}
					
					if (dataContext.rootBoneIndex == dataContext.boneIndex) {
						var from:KeyFrame = dataContext.stepTrack.keyFrameList[0];
						var to:KeyFrame = dataContext.stepTrack.keyFrameList[maxFrameCount-1];
						var diff:Vector3D = pool.popVector3D();
						diff.setTo(from.transform.d - to.transform.d, 
							from.transform.h - to.transform.h, 
							from.transform.l - to.transform.l);
						rootBoneOffset = diff.length; 
						pool.pushVector3D(diff);
					}
					dataContext.stepTrack = null;
					dataContext.boneIndex++;
					if (getCurTimer() - curTimer >= timer) {
						return 0;
					}
				}
				
				dataContext.stepTrack = null;
				dataContext.raw.length = 0;
				dataContext.raw = null;
				dataContext.trackNameList = null;
				dataContext = null;
				RawDataReader.getInstance().removeListener(parseRawData);
				this.loadState = Defines.LOAD_OK;
				postListeners(true);
			} catch(e:Error) {
				dataContext.raw = null;
				dataContext.trackNameList = null;
				dataContext = null;
				RawDataReader.getInstance().removeListener(parseRawData);
				this.loadState = Defines.LOAD_FAILD;
				postListeners(false);
			}
			return timer - (getCurTimer() - curTimer);
		}
		
		public function updateAni(frameIndex:int):void
		{
			usedFrame = Global.frameCount;
			if (this.loadState != Defines.LOAD_OK) {
				return;
			}
			
			if (this.hasDisposed) {
				return;
			}
			
			if (this.skeleton == null || this.skeleton.allBonesCount == 0) {
				return;
			}
			
			var tracksNum:int = this.trackList.length;
			var track:Track;
			var boneName:String;
			for (var i:int = 0; i < tracksNum; i++) {
				track = this.trackList[i];
				boneName = track.boneName;
				
				if (track.boneInstance == null) {
					track.boneInstance = this.skeleton.getBoneByName(boneName);
				}
				
				if (track.boneInstance==null) {
					continue;
				}
				
				var keyframe:KeyFrame = track.keyFrameList[frameIndex];
				
				//track.boneInstance.jointTransform = (keyframe.boneTransform);
				//track.boneInstance.boneTransform.copy(keyframe.boneTransform);
				if (track.boneInstance.curKeyFrame != keyframe) {
					track.boneInstance.curKeyFrame = keyframe;
				}
			}
			
			//this.skeleton.collectJointsTransform();
		}	
		
		public function getRootBoneOffset():Number
		{
			return rootBoneOffset;
		}
	}
}
import flash.utils.ByteArray;

import FlashGE.animation.Track;

internal class RawDataContext
{
	public var raw:ByteArray;
	
	public var hasSignature:Boolean = true;
	public var version:int = -1;	
	public var boneCount:int = 0;
	public var trackNameList:Vector.<String>;
	public var rootBoneIndex:int = 0; 
	public var boneIndex:int = 0;
	
	public var stepTrack:Track;
	public var stepJ:int;
}
