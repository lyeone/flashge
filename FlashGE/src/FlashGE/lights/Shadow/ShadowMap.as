package FlashGE.lights.Shadow
{
	import flash.display3D.Context3D;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Context3DTextureFormat;
	import flash.display3D.VertexBuffer3D;
	import flash.display3D.textures.Texture;
	import flash.geom.Rectangle;
	import flash.utils.Dictionary;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.materials.ShaderProgram;
	import FlashGE.materials.compiler.Linker;
	import FlashGE.materials.compiler.Procedure;
	import FlashGE.materials.compiler.VariableType;
	import FlashGE.objects.Entity;
	import FlashGE.objects.Mesh;
	import FlashGE.objects.Model;
	import FlashGE.objects.RenderObject;
	import FlashGE.objects.Skin;
	import FlashGE.objects.Surface;
	import FlashGE.render.Camera3D;
	import FlashGE.render.RenderUnit;
	import FlashGE.render.Renderer;
	import FlashGE.resources.Geometry;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;
	public class ShadowMap
	{
		static private var programsCached:Dictionary = new Dictionary;
		private var castersLen:int = 0;
		private var preCastersLen:int = 1;
		private var casters:Vector.<RenderObject> = new Vector.<RenderObject>;
		private var castersTrans:Vector.<Transform3D> = new Vector.<Transform3D>;
		FlashGE var shadowMapFile:TextureFile;
		private var renderer:Renderer = new Renderer("shadowmap");
		private var needSetup:Boolean = true;
		private var globalToLightTransform:Transform3D = new Transform3D;
		FlashGE var cameraToShadowMapContextProjection:Transform3D = new Transform3D;
		FlashGE var cameraToShadowMapUVProjection:Transform3D = new Transform3D;
		FlashGE var objectToShadowMapTransform:Transform3D = new Transform3D;
		private var usedContext3D:Context3D;
		private var centerX:Number = 0;
		private var centerY:Number = 0;
		private var centerZ:Number = 0;
		private var width:Number = 0;
		private var height:Number = 0;
		private var mapSize:Number = 1;
		private var nearPos:Number = 0;
		private var farPos:Number = 0;
		private var programs:Dictionary = new Dictionary;
		private var rect:Rectangle = new Rectangle;
		private var addCasterTempTransform:Transform3D = new Transform3D;
		private var meshTempTransform:Transform3D = new Transform3D;
		private var pool:Pool;
		public function ShadowMap(width:Number, height:Number, nearPos:Number, farPos:Number, mapSize:int = 512, pcfOffset:Number = 0)
		{
			this.width = width;
			this.height = height;
			this.nearPos = nearPos;
			this.farPos = farPos;
			this.mapSize = mapSize;
			this.programs = ShadowMap.programsCached;
			this.pool = Pool.getInstance();
		}
		
		private function checkAndCreateTexture(context3D:Context3D):void
		{
			if (shadowMapFile == null) {
				var shadowMap:Texture = context3D.createTexture(mapSize, mapSize, Context3DTextureFormat.BGRA, true);
				shadowMapFile = TextureFile.createFrom3D(shadowMap, mapSize, mapSize);
			}
		}
		
		public function addCaster(caster:RenderObject, parentTransform:Transform3D):void
		{
			//if (caster.parent==null) return;
			this.casters[castersLen] = caster;
			var t:Transform3D;
			if (castersLen <= this.castersTrans.length) {
				this.castersTrans[castersLen] = t = new Transform3D;
			} else {
				t = this.castersTrans[castersLen];	
			}

			var cury:Number = caster.y;
			var curz:Number = caster.z;
			addCasterTempTransform.compose(caster.x, cury, curz, 
				caster.rotationX, caster.rotationY, caster.rotationZ, 
				caster.scaleX, caster.scaleY, caster.scaleZ);
			t.combine(parentTransform, addCasterTempTransform);
			castersLen++;
		}
		
		public function addCasterByPos(caster:RenderObject, parentTransform:Transform3D, 
									   shadowX:Number, shadowY:Number, shadowZ:Number):void
		{
			this.casters[castersLen] = caster;
			var t:Transform3D;
			if (castersLen <= this.castersTrans.length) {
				this.castersTrans[castersLen] = t = new Transform3D;
			} else {
				t = this.castersTrans[castersLen];	
			}
			addCasterTempTransform.compose(shadowX, shadowY, shadowZ, 
				caster.rotationX, caster.rotationY, caster.rotationZ, 
				caster.scaleX, caster.scaleY, caster.scaleZ);
			t.combine(parentTransform, addCasterTempTransform);
			castersLen++;
		}
		
		public function setCenterPos(x:Number, y:Number, z:Number):void
		{
			centerX = x;
			centerY = y;
			centerZ = z;
		}
		
		public function setup(camera:Camera3D, lightCameraToLocalTransform:Transform3D):void
		{
			
			if (camera.context3D != usedContext3D) {
				if (shadowMapFile != null) {
					shadowMapFile.release();
					shadowMapFile = null;
				}
				usedContext3D = camera.context3D;
			}
			
			var i:int = 0;
			var frustumMinX:Number;
			var frustumMaxX:Number;
			var frustumMinY:Number;
			var frustumMaxY:Number;
			var frustumMinZ:Number;
			var frustumMaxZ:Number;
			
			globalToLightTransform.combine(lightCameraToLocalTransform, camera.globalToLocalTransform);
			
			var cx:Number = centerX*globalToLightTransform.a + centerY*globalToLightTransform.b + centerZ*globalToLightTransform.c + globalToLightTransform.d;
			var cy:Number = centerX*globalToLightTransform.e + centerY*globalToLightTransform.f + centerZ*globalToLightTransform.g + globalToLightTransform.h;
			var cz:Number = centerX*globalToLightTransform.i + centerY*globalToLightTransform.j + centerZ*globalToLightTransform.k + globalToLightTransform.l;
			
			var wPSize:Number = width/mapSize;
			var hPSize:Number = height/mapSize;
			cx = Math.round(cx/wPSize)*wPSize;
			cy = Math.round(cy/hPSize)*hPSize;
			
			frustumMinX = cx - width*0.5;
			frustumMaxX = cx + width*0.5;
			frustumMinY = cy - height*0.5;
			frustumMaxY = cy + height*0.5;
			
			frustumMinZ = cz + nearPos;
			frustumMaxZ = cz + farPos;
			
			var correction:Number = (mapSize - 2)/mapSize;
			cameraToShadowMapContextProjection.a = 2/(frustumMaxX - frustumMinX)*correction;
			cameraToShadowMapContextProjection.b = 0;
			cameraToShadowMapContextProjection.c = 0;
			cameraToShadowMapContextProjection.e = 0;
			cameraToShadowMapContextProjection.f = -2/(frustumMaxY - frustumMinY)*correction;
			cameraToShadowMapContextProjection.g = 0;
			cameraToShadowMapContextProjection.h = 0;
			cameraToShadowMapContextProjection.i = 0;
			cameraToShadowMapContextProjection.j = 0;
			cameraToShadowMapContextProjection.k = 1 / (frustumMaxZ - frustumMinZ);
			cameraToShadowMapContextProjection.d = (-0.5 * (frustumMaxX + frustumMinX) * cameraToShadowMapContextProjection.a);
			cameraToShadowMapContextProjection.h = (-0.5 * (frustumMaxY + frustumMinY) * cameraToShadowMapContextProjection.f);
			cameraToShadowMapContextProjection.l = -frustumMinZ / (frustumMaxZ - frustumMinZ);
			
			cameraToShadowMapUVProjection.copy(cameraToShadowMapContextProjection);
			cameraToShadowMapUVProjection.a = 1 / ((frustumMaxX - frustumMinX)) * correction;
			cameraToShadowMapUVProjection.f = 1 / ((frustumMaxY - frustumMinY)) * correction;
			cameraToShadowMapUVProjection.d = 0.5 - (0.5 * (frustumMaxX + frustumMinX) * cameraToShadowMapUVProjection.a);
			cameraToShadowMapUVProjection.h = 0.5 - (0.5 * (frustumMaxY + frustumMinY) * cameraToShadowMapUVProjection.f);
			
			cameraToShadowMapContextProjection.prepend(lightCameraToLocalTransform);
			cameraToShadowMapUVProjection.prepend(lightCameraToLocalTransform); 
			
			this.checkAndCreateTexture(camera.context3D);
		}
		
		private function collectRenderUnits(camera:Camera3D, caster:RenderObject, localToCameraTransform:Transform3D):void
		{
			var child:Entity;
			var i:int;
			var j:int;
			var n:int;
			var m:int;
			var skin:Skin;
			var program:ShaderProgram;
			
			if (caster.model == null || caster.hasDisposed == true) return;
			
			if (caster.skeletonAni != null) {
				caster.skeletonAni.updateAni(caster.aniCurFrame);
			}
			
			var model:Model = caster.model;
			objectToShadowMapTransform.combine(cameraToShadowMapContextProjection, localToCameraTransform);
			for each(var subMesh:Entity in model.subMeshList) {
				
				if (subMesh is Skin) {
					collectSkin(camera, subMesh as Skin);
				} else if (subMesh is Mesh) {
					collectMesh(camera, subMesh as Mesh);
				}
			}
		}
		
		private function collectMesh(camera:Camera3D, mesh:Mesh):void
		{
			if (mesh.hasDisposed) {
				return;
			}
			
			var program:ShaderProgram;
			
			for (var i:int = 0, n:int = mesh.surfacesLength; i < n; i++) {
				
				var surface:Surface = mesh.surfaces[i];
				var geometry:Geometry = mesh.geometry;
				
				program = programs[0];
				if (program == null) {
					program = programs[0] = getProgram(null);
				}
				
				var renderUnit:RenderUnit = camera.pool.popRenderUnit();
				renderUnit.object = mesh;
				renderUnit.program = program.getProgram3D(usedContext3D);
				renderUnit.indexBuffer = geometry.getIndexBuffer(usedContext3D);
				renderUnit.firstIndex = surface.indexBegin;
				renderUnit.numTriangles = surface.trianglesNum;
				
				mesh.setTransformConstants(renderUnit, surface, program, camera);
				renderUnit.setVertexBufferAt(program.aPosition, 
					geometry.getVertexBuffer(Defines.POSITION, usedContext3D), 
					geometry.attrOffsetsList[Defines.POSITION], 
					Defines.FORMATS[Defines.POSITION]);
				renderUnit.setVertexConstantsFromTransform(program.cProjMatrix, objectToShadowMapTransform);
				renderUnit.setVertexConstantsFromNumbers(program.cShadowScale, 255, 0, 0, 1);
				renderUnit.setFragmentConstantsFromNumbers(program.cShadowConstants, 1 / 255, 0, 0, 1);
				
				renderer.addRenderUnit(renderUnit, Defines.PRIORITY_OPAQUE);
				renderUnit.release();
			}
			
		}
		
		private function collectSkin(camera:Camera3D, skin:Skin):void
		{
			if (skin.skinData == null || skin.skeleton == null || 
				skin.skeleton.loadState != Defines.LOAD_OK ||
				skin.hasDisposed == true) {
				return;
			}
			
			var program:ShaderProgram;
			
			for (var i:int = 0, n:int = skin.surfacesLength; i < n; i++) {
				
				var surface:Surface = skin.surfaces[i];
				var geometry:Geometry = skin.skinData.geometry;
				var uvBuffer:VertexBuffer3D;					
				
				skin.transformProcedure = skin.skinData.surfaceShadowTransformProcedures[i];
				program = programs[skin.transformProcedure];
				if (program == null) {
					programs[skin.transformProcedure] = program = getProgram(skin.transformProcedure);
				}
				
				var renderUnit:RenderUnit = pool.popRenderUnit();
				renderUnit.object = skin;
				renderUnit.program = program.getProgram3D(usedContext3D);
				renderUnit.indexBuffer = geometry.getIndexBuffer(usedContext3D);
				renderUnit.firstIndex = surface.indexBegin;
				renderUnit.numTriangles = surface.trianglesNum;
				
				skin.setTransformConstants(renderUnit, surface, program, camera);
				renderUnit.setVertexBufferAt(program.aPosition, geometry.getVertexBuffer(Defines.POSITION, usedContext3D), geometry.attrOffsetsList[Defines.POSITION], Defines.FORMATS[Defines.POSITION]);
				renderUnit.setVertexConstantsFromTransform(program.cProjMatrix, objectToShadowMapTransform);
				renderUnit.setVertexConstantsFromNumbers(program.cShadowScale, 255, 0, 0, 1);
				renderUnit.setFragmentConstantsFromNumbers(program.cShadowConstants, 1 / 255, 0, 0, 1);
				
				renderer.addRenderUnit(renderUnit, Defines.PRIORITY_OPAQUE);
				renderUnit.release();
			}
		}
		
		public function calculateShadowMap(camera:Camera3D, lightCameraToLocalTransform:Transform3D):void
		{
			if (this.castersLen == 0 && this.preCastersLen ==0) {
				return;
			}
			
			if (camera.closeCollectRenderUnits) {
				return;
			}
			
			this.setup(camera, lightCameraToLocalTransform);
			
			for (var i:int = 0; i < castersLen; i++) {
				if (this.casters[i].isVisible()) {
					this.collectRenderUnits(camera, this.casters[i], this.castersTrans[i]);
				}
			}
			var context3D:Context3D = camera.context3D;
			context3D.setRenderToTexture(this.shadowMapFile.getTexture(context3D), true);
			context3D.clear(1, 0, 0, 0.3);
			
			renderer.camera = camera;
			
			rect.x = 1;
			rect.y = 1;
			rect.width = mapSize - 2;
			rect.height = mapSize - 2;
			camera.context3D.setScissorRectangle(rect);
			
			renderer.render(camera.context3D, false);
			
			camera.context3D.setScissorRectangle(null);
			
			camera.context3D.setRenderToBackBuffer();
			this.preCastersLen = this.castersLen;
			this.castersLen = 0;
		}
		
		private function getProgram(transformProcedure:Procedure):ShaderProgram
		{
			var program:ShaderProgram;			
			
			var vLinker:Linker = new Linker(Context3DProgramType.VERTEX);
			var fLinker:Linker = new Linker(Context3DProgramType.FRAGMENT);
			
			var positionVar:String = "aPosition";
			vLinker.declareVariable(positionVar, VariableType.ATTRIBUTE);				
			
			if (transformProcedure != null) {
				var newPosVar:String = "tTransformedPosition";
				vLinker.declareVariable(newPosVar);
				vLinker.addProcedure(transformProcedure, positionVar);
				vLinker.setOutputParams(transformProcedure, newPosVar);
				positionVar = newPosVar;
			}
			
			
			var proc:Procedure = Procedure.compileFromArray([
				"#c3=cShadowScale",
				"#v0=vDistance",
				"m34 t0.xyz, i0, c0",
				"mov t0.w, c3.w",
				"mul v0, t0, c3.x",
				"mov o0, t0"
			]);
			
			proc.assignVariableName(VariableType.CONSTANT, 0, "cProjMatrix", 3);
			vLinker.addProcedure(proc, positionVar);
			
			fLinker.addProcedure(Procedure.compileFromArray([
				"#v0=vDistance",
				"#c0=cShadowConstants",
				"frc t0.y, v0.z",
				"sub t0.x, v0.z, t0.y",
				"mul t0.x, t0.x, c0.x",
				"mov t0.zw, c0.zw",
				"mov o0, t0"
			]));
			program = new ShaderProgram(vLinker, fLinker);
			fLinker.varyings = vLinker.varyings;
			
			return program;
		}
		
		public function dispose():void
		{
			if (shadowMapFile != null) {
				shadowMapFile.release();
				shadowMapFile = null;
			}
		}
	}
}