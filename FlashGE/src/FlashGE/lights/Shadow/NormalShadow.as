package FlashGE.lights.Shadow
{
	import flash.display3D.Context3D;
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Context3DVertexBufferFormat;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.Program3D;
	import flash.display3D.VertexBuffer3D;
	import flash.display3D.textures.TextureBase;
	import flash.utils.ByteArray;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.materials.compiler.Procedure;
	import FlashGE.objects.Entity;
	import FlashGE.render.Camera3D;
	import FlashGE.render.RenderUnit;
	import FlashGE.resources.TextureFile;

	CONFIG::DEBUG {
	import FlashGE.common.Global;
	}
	
	use namespace FlashGE;
	public class NormalShadow extends Entity
	{
		private var baseTransform:Transform3D = new Transform3D;
		private var renderUnit:RenderUnit;
		private var curIndexBuffer:IndexBuffer3D;
		private var curVertexBuffer:VertexBuffer3D;
		private var curProgram3D:Program3D;
		private var context3DUsed:Context3D;
		private var textureFile:TextureFile;
		private var curShadowCount:int = 0;
		private var maxShadowCount:int = 64;
		private var vertexBytesCode:ByteArray;
		private var fragmentBytesCode:ByteArray;
		
		public function NormalShadow()
		{
			this.renderUnit = Pool.getInstance().popRenderUnit();
			this.renderUnit.srcBlend = Context3DBlendFactor.SOURCE_ALPHA;
			this.renderUnit.dstBlend = Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA;
			this.renderUnit.object = this;
		}
		
		public function setBaseTransform(transform:Transform3D):void
		{
			this.baseTransform.copy(transform);	
		}
		
		public function addItem(x:Number, y:Number, z:Number):void
		{
			if (curShadowCount < 124) {
				renderUnit.setVertexConstantsFromNumbers(4 + curShadowCount, x, y, z, 1);
				curShadowCount++;
				renderUnit.numTriangles = curShadowCount * 2;
				renderUnit.vertexConstantsRegistersCount = 4 + curShadowCount;
			}
		}
		
		public function create3DBuffers(context3D:Context3D, texFormatID:int):void
		{
			if (curIndexBuffer != null) {
				curIndexBuffer.dispose();
				curIndexBuffer = null;
			}
			
			if (curVertexBuffer != null) {
				curVertexBuffer.dispose();
				curVertexBuffer = null;
			}
			
			if (curProgram3D != null) {
				curProgram3D.dispose();
				curProgram3D = null;
			}
			
			var i:int, n:int;
			var vertices:Vector.<Number> = new Vector.<Number>();
			var indices:Vector.<uint> = new Vector.<uint>();
			for (i = 0; i < maxShadowCount; i++) {
				vertices.push(
					-100.5,-100.5, 0, 0, 0, 4+i,
					-100.5, 100.5, 0, 0, 1, 4+i,
					 100.5, 100.5, 0, 1, 1, 4+i,
					 100.5,-100.5, 0, 1, 0, 4+i);
				
				indices.push(i*4 + 3, i*4 + 1, i*4, 
					i*4 + 1, i*4 + 3, i*4 + 2);
			}
			
			curIndexBuffer = context3D.createIndexBuffer(maxShadowCount*6);
			CONFIG::DEBUG {
				Global.indexBuffersCount++;
			}
			curIndexBuffer.uploadFromVector(indices, 0, maxShadowCount*6);
			
			curVertexBuffer = context3D.createVertexBuffer(maxShadowCount * 4, 6);
			CONFIG::DEBUG {
				Global.vertexBuffersCount++;
			}
			curVertexBuffer.uploadFromVector(vertices, 0, maxShadowCount*4);
			
			if (this.vertexBytesCode == null || this.fragmentBytesCode == null) {
				var shaderIndex:int = 0;
				var vertShaderArray:Array = [];
				vertShaderArray[shaderIndex++] = "mov t4, c[a1.z]";
				
				vertShaderArray[shaderIndex++] = "mov t0, c0";
				vertShaderArray[shaderIndex++] = "dp4 t0.w, t0, t4";
				
				vertShaderArray[shaderIndex++] = "mov t1, c1";
				vertShaderArray[shaderIndex++] = "dp4 t1.w, t1, t4";
				
				vertShaderArray[shaderIndex++] = "mov t2, c2";
				vertShaderArray[shaderIndex++] = "dp4 t2.w, t2, t4";
				
				vertShaderArray[shaderIndex++] = "mov t3, c3";
				vertShaderArray[shaderIndex++] = "dp4 t3.w, t3, t4";
				
				vertShaderArray[shaderIndex++] = "m44 o0, a0, t0";
				vertShaderArray[shaderIndex++] = "mov v0, a1";
				
				shaderIndex = 0;
				var fragShaderArray:Array = [];
				fragShaderArray[shaderIndex++] = "tex t0, v0.xy, s0 <2d, linear, repeat, miplinear, "+Defines.TEX_FORMATS[texFormatID]+">";
				fragShaderArray[shaderIndex++] = "slt t1, t0, t0";
				fragShaderArray[shaderIndex++] = "mov t1.w, t0.x";
				fragShaderArray[shaderIndex++] = "mov o0, t1";
				
				var vertProcedure:Procedure = new Procedure(vertShaderArray);
				var fragProcedure:Procedure = new Procedure(fragShaderArray);
				this.vertexBytesCode = vertProcedure.getByteCode(Context3DProgramType.VERTEX);
				this.fragmentBytesCode = fragProcedure.getByteCode(Context3DProgramType.FRAGMENT);
			}
			curProgram3D = context3D.createProgram();
			curProgram3D.upload(this.vertexBytesCode, this.fragmentBytesCode);
			context3DUsed = context3D;
			
			renderUnit.vertexBuffersLength = 0
			renderUnit.setVertexBufferAt(0, curVertexBuffer, 0, Context3DVertexBufferFormat.FLOAT_3);
			renderUnit.setVertexBufferAt(1, curVertexBuffer, 3, Context3DVertexBufferFormat.FLOAT_3);
			
			renderUnit.indexBuffer = curIndexBuffer;
			renderUnit.program = curProgram3D;
		}
		
		FlashGE function collectShadows(camera:Camera3D):void 
		{
			if (renderUnit == null || this.curShadowCount == 0 || this.parent == null) {
				return;
			}
			
			if (renderUnit.vertexConstantsRegistersCount == 0) {
				return;
			}
			
			if (textureFile == null) {
				if (TextureFile.forceATF) {
					textureFile = TextureFile.createFromFile("tex/shadow.atf");
				} else {
					textureFile = TextureFile.createFromFile("tex/shadow.tga");
				}
				this.curShadowCount = 0;
				return;
			}

			var texture3D:TextureBase = textureFile.getTexture(camera.context3D);
			if (texture3D == null) {
				this.curShadowCount = 0;
				return;
			}
			
			if (context3DUsed != camera.context3D || this.curShadowCount > maxShadowCount) {
				context3DUsed = camera.context3D;
				if (this.curShadowCount > maxShadowCount) {
					maxShadowCount = Math.min(this.curShadowCount * 2, 124);
				}
				this.create3DBuffers(context3DUsed, textureFile.getFormatID());
			}
			
			if (curIndexBuffer == null || curVertexBuffer == null || curProgram3D == null) {
				this.curShadowCount = 0;
				return;
			}
			
			if (renderUnit.texturesLength == 0) {
				renderUnit.setTextureAt(0, textureFile);
			}
			
			renderUnit.setProjectionConstants(camera, 0, this.parent.localToCameraTransform);
			camera.renderer.addRenderUnit(renderUnit, Defines.PRIORITY_EFFECT);
			curShadowCount = 0;
		}
		
	}
}