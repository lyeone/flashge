package FlashGE.resources
{
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.utils.ByteArray;
	import flash.utils.CompressionAlgorithm;
	import flash.utils.Dictionary;
	import flash.utils.Endian;
	
	import FlashGE.common.Defines;
	
	CONFIG::DEBUG {
	import FlashGE.common.Global;
	}
		
	public class LoaderMgr3D
	{
		static private var instance:LoaderMgr3D;
		
		public var isHighPriority:Boolean = false;
		private var loaderHolders:Vector.<LoaderHolder>;
		private var loaderHoldersCount:int;
		
		private var header:LoaderItem;
		private var tail:LoaderItem;
		private var curLoaderHoldersCount:int = 0;
		private var totalLoadedBytes:int = 0;
		private var filesList:Dictionary;
		private var filesListByPath:Dictionary = new Dictionary;
		private var idleMgr:LoaderMgr3D;
		private var highMgr:LoaderMgr3D;
		private var hostMgr:LoaderMgr3D;
		private var isBusy:Boolean = false;
		private var allFxList:Dictionary;
		private var allShadersKey:Vector.<String>;
		private var allParticleShadersKey:Vector.<String>;
		private var allFxRawData:ByteArray;
		
		static public function getInstance():LoaderMgr3D
		{
			if (LoaderMgr3D.instance == null) {
				var ret:LoaderMgr3D = new LoaderMgr3D;
				ret.initLoader(2);
				LoaderMgr3D.instance = ret;
			}
			
			return LoaderMgr3D.instance;
		}
		
		private function initLoader(count:int):void
		{
			loaderHoldersCount = count;
			loaderHolders = new Vector.<LoaderHolder>(count);
			var item:LoaderHolder;
			for (var i:int = 0; i < count; i++) {
				item = new LoaderHolder;
				item.isIdle = true;
				loaderHolders[i] = item;
			}
		}
		
		public function markBusy(busy:Boolean):void
		{
			this.isBusy = busy;
		}
		
		public function getTotalLoadedBytes():int
		{
			return this.totalLoadedBytes;	
		}
		
		public function getFilesList():Dictionary
		{
			var curFilesList:Dictionary;
			if (this.hostMgr != null) {
				curFilesList = this.hostMgr.filesList;
			} else {
				curFilesList = this.filesList;
			}
			return curFilesList;
		}
		
		public function getAllShadersKey():Vector.<String>
		{
			return this.allShadersKey;
		}
		
		public function getAllParticleShadersKey():Vector.<String>
		{
			return this.allParticleShadersKey;
		}
		
		public function getAllFxList():Dictionary
		{
			var curFilesList:Dictionary;
			if (this.hostMgr != null) {
				curFilesList = this.hostMgr.allFxList;
			} else {
				curFilesList = this.allFxList;
			}
			return curFilesList;	
		}
		
		public function getAllFxRawData():ByteArray
		{
			var curFilesList:ByteArray;
			if (this.hostMgr != null) {
				curFilesList = this.hostMgr.allFxRawData;
			} else {
				curFilesList = this.allFxRawData;
			}
			return curFilesList;	
		}
		
		public function hasFile(url:String):Boolean
		{
			var curFilesList:Dictionary = this.getFilesList();
			
			if (curFilesList == null) {
				return true;
			}
			
			if (curFilesList[url] == null) {
				return false;
			}
			
			return true;
		}
		
		public function moveToIdleList():void
		{
			if (this.header == null) {
				return;
			}
			if (idleMgr == null) {
				idleMgr = new LoaderMgr3D;
				idleMgr.hostMgr = this;
				idleMgr.initLoader(1);
			}
			
			this.tail.next = idleMgr.header;
			idleMgr.header = this.header;
			if (idleMgr.tail == null) {
				idleMgr.tail = this.tail;
			}
			this.header = this.tail = null;
		}
			
		public function getAllFilesByPath(path:String):Dictionary
		{
			var curFilesList:Dictionary = this.getFilesList();
			if (curFilesList == null || path == null) {
				return null;
			}
			
			var cachesList:Dictionary = filesListByPath[path];
			if (cachesList == null) {
				filesListByPath[path] = cachesList = new Dictionary;
				for (var fileName:* in curFilesList) {
					var fileNameString:String = fileName;
					if (fileNameString.indexOf(path) >= 0) {
						cachesList[fileName] = true;
					}
				}
			}
			return cachesList;
		}
		
		public function pushHighPriorityLoader(url:String, onLoadedListener:Function, isUrlLoader:Boolean, 
									   checkFilesList:Boolean, insertFirst:Boolean):void
		{
			if (highMgr == null) {
				highMgr = new LoaderMgr3D;
				highMgr.isHighPriority = true;
				highMgr.hostMgr = this;
				highMgr.initLoader(2);
			}
			highMgr.pushLoader(url, onLoadedListener, isUrlLoader, checkFilesList, insertFirst);
		}
		
		public function pushLoaderItem(item:LoaderItem, insertFirst:Boolean):void
		{
			if (insertFirst) {
				if (header == null){
					tail = header = item;
				} else {
					item.next = header;
					header = item;
				}
			} else {
				if (header == null){
					tail = header = item;
				} else {
					tail.next = item;
					tail = item;
				}
			}
		}
		
		public function pushIdleLoader(url:String, onLoadedListener:Function, isUrlLoader:Boolean, 
								   checkFilesList:Boolean, insertFirst:Boolean):void
		{
			if (idleMgr == null) {
				idleMgr = new LoaderMgr3D;
				idleMgr.hostMgr = this;
				idleMgr.initLoader(1);
			}
			var item:LoaderItem = new LoaderItem;
			
			item.forbidLoad = false;
			var curFilesList:Dictionary = this.getFilesList();
			var realURL:String;
			if (curFilesList != null) {
				realURL = curFilesList[url]; 
				if (realURL == null) {
					item.forbidLoad = true;
				}
			}
			
			if (realURL == null) {
				realURL = url; 
			}
			item.url = realURL;
			item.isUrlLoader = isUrlLoader;
			item.onLoadedListener = onLoadedListener;
			item.next = null;
			if (insertFirst) {
				if (this.idleMgr.header == null){
					this.idleMgr.tail = this.idleMgr.header = item;
				} else {
					item.next = this.idleMgr.header;
					this.idleMgr.header = item;
				}
				
			} else {
				if (this.idleMgr.header == null){
					this.idleMgr.tail = this.idleMgr.header = item;
				} else {
					if (this.idleMgr.tail != null) {
						this.idleMgr.tail.next = item;
					}
					this.idleMgr.tail = item;
				}
			}
		}
		
		public function checkNormal(url:String, insertFirst:Boolean):void
		{
			if (this.header == null) {
				return;
			}
			
			var item:LoaderItem = this.header, 
				findItem:LoaderItem = null, 
				preItem:LoaderItem = null;
			while(item != null) {
				
				if (item.url == url) {
					findItem = item;
					break;
				}
				
				preItem = item;
				item = item.next;
			}
			
			if (findItem == null) {
				return;
			}
			
			if (findItem == this.tail &&
				findItem == this.header) {
				this.tail = this.header = null;
			} else if (findItem == this.tail) {
				this.tail = preItem;
				if (preItem != null) {
					preItem.next = null;
				}
			} else if(findItem == this.header) {
				this.header = findItem.next;
				findItem.next = null
			} else {
				preItem.next = findItem.next;
				findItem.next = null;
			}
			
			item = findItem;
			item.next = null;
			
			if (highMgr == null) {
				highMgr = new LoaderMgr3D;
				highMgr.isHighPriority = true;
				highMgr.hostMgr = this;
				highMgr.initLoader(2);
			}
			highMgr.pushLoaderItem(item, insertFirst);
			
		}
		
		public function checkIdle(url:String, insertFirst:Boolean, moveToHighPriority:Boolean):void
		{
			if (idleMgr == null) {
				return;
			}
			
			var item:LoaderItem = this.idleMgr.header, 
				findItem:LoaderItem = null, 
				preItem:LoaderItem = null;
			while(item != null) {
				
				if (item.url == url) {
					findItem = item;
					break;
				}
				
				preItem = item;
				item = item.next;
			}
			
			if (findItem == null) {
				
				if (moveToHighPriority)　{
					checkNormal(url, insertFirst);
				}
					
				return;
			}
			
			if (findItem == this.idleMgr.tail &&
				findItem == this.idleMgr.header) {
				this.idleMgr.tail = this.idleMgr.header = null;
			} else if (findItem == this.idleMgr.tail) {
				this.idleMgr.tail = preItem;
				if (preItem != null) {
					preItem.next = null;
				}
			} else if(findItem == this.idleMgr.header) {
				this.idleMgr.header = findItem.next;
				findItem.next = null
			} else {
				preItem.next = findItem.next;
				findItem.next = null;
			}
			
			item = findItem;
			item.next = null;
			
			if (moveToHighPriority) {
				if (highMgr == null) {
					highMgr = new LoaderMgr3D;
					highMgr.isHighPriority = true;
					highMgr.hostMgr = this;
					highMgr.initLoader(2);
				}
				highMgr.pushLoaderItem(item, insertFirst);
			} else {
				pushLoaderItem(item, insertFirst);
			}
		}
		
		public function pushLoader(url:String, onLoadedListener:Function, isUrlLoader:Boolean, 
								   checkFilesList:Boolean, insertFirst:Boolean):void
		{
			var item:LoaderItem = null, 
				findItem:LoaderItem = null, 
				preItem:LoaderItem = null;
			
			if (idleMgr != null) {
				item = this.idleMgr.header
				while(item != null) {
					
					if (item.url == url) {
						findItem = item;
						break;
					}
					
					preItem = item;
					item = item.next;
				}
			}
			
			if (findItem != null) {
				
				if (findItem == this.idleMgr.tail &&
					findItem == this.idleMgr.header) {
					this.idleMgr.tail = this.idleMgr.header = null;
				} else if (findItem == this.idleMgr.tail) {
					this.idleMgr.tail = preItem;
					if (preItem != null) {
						preItem.next = null;
					}
				} else if(findItem == this.idleMgr.header) {
					this.idleMgr.header = findItem.next;
					findItem.next = null
				} else {
					preItem.next = findItem.next;
					findItem.next = null;
				}
				item = findItem;
			} else {		
				item = new LoaderItem;
			}
			
			item.forbidLoad = false;
			
			var realURL:String;
			var curFilesList:Dictionary = this.getFilesList();
			if (curFilesList != null) {
				realURL = curFilesList[url];
				if (realURL == null) {
					item.forbidLoad = true;
				}
			}
			if (realURL == null) {
				realURL = url;
			}
			item.url = realURL;
			item.isUrlLoader = isUrlLoader;
			item.onLoadedListener = onLoadedListener;
			item.next = null;
			if (insertFirst) {
				if (header == null){
					tail = header = item;
				} else {
					item.next = header;
					header = item;
				}
				
			} else {
				if (header == null){
					tail = header = item;
				} else {
					tail.next = item;
					tail = item;
				}
			}
		}
		
		public function onEnterFrame():void
		{
			if (highMgr != null && highMgr.header != null) {
				highMgr.onEnterFrame();
				return;
			}
			
			if (isBusy) {
				return;
			}
			
			if (curLoaderHoldersCount >= loaderHoldersCount) {
				return;
			}
			
			var holder:LoaderHolder;
			for (var i:int = 0; i < this.loaderHoldersCount; i++) {
				if (this.loaderHolders[i].isIdle) {
					holder = this.loaderHolders[i];
					break;
				}
			}
			
			if (holder == null) {
				return;
			}
			
			if (header != null) {
			
				curLoaderHoldersCount++;
				var item:LoaderItem = this.header;
				this.header = this.header.next;
				
				item.mgr = this;
				item.next = null
				item.doLoad(holder);

				if (header == null) {
					this.tail = null;
				}
			}

			if (curLoaderHoldersCount == 0 && idleMgr != null && header == null) {
				this.idleMgr.onEnterFrame();
			}
		}
		
		public function decLoadCount(loadedBytes):void
		{
			this.totalLoadedBytes += loadedBytes;
			curLoaderHoldersCount--;
		}
		
		public function parseAllFxs(rawData:ByteArray):void
		{
			if (rawData == null) {
				return;
			}
			
			try {
				this.allFxList = new Dictionary;
				this.allShadersKey = new Vector.<String>;
				this.allParticleShadersKey = new Vector.<String>;
				allFxRawData = rawData;
				allFxRawData.endian = Endian.LITTLE_ENDIAN;
				allFxRawData.position = 0;
				allFxRawData.uncompress(CompressionAlgorithm.ZLIB);
				allFxRawData.position = 0;
				var filesCount:int = allFxRawData.readUnsignedShort();
				var bytesLength:int;
				var url:String;
				var offset:int;
				
				var item:Object;
				var preItem:Object;
				for (var i:int = 0; i < filesCount; i++) {
					
					bytesLength = allFxRawData.readUnsignedShort();
					url = allFxRawData.readUTFBytes(bytesLength);
					item = new Object;
					item.offset = allFxRawData.readUnsignedInt(); 
					if (preItem != null) {
						preItem.fileSize = item.offset - preItem.offset;
					}
					preItem = item;
					this.allFxList[url] = item;
					if (url.indexOf("/") < 0) {
						if (url.indexOf("particle_") >=0) {
							this.allParticleShadersKey.push(url);
						} else {
							this.allShadersKey.push(url);
						}
					}
				}
				if (item != null) {
					item.fileSize = allFxRawData.length - item.offset;
				}
				
			} catch (e:Error) {
				allFxList = null;
				allFxRawData = null;
				CONFIG::DEBUG {
					Global.log("LoaderMgr::onLoaded not exist files.list");
				}
			}
		}
		
		public function parseAllFilesList(rawData:ByteArray):void
		{
			if (rawData == null) {
				return;
			}
			
			try {
				var raw:ByteArray = rawData;
				raw.endian = Endian.LITTLE_ENDIAN;
				raw.position = 0;
				raw.uncompress(CompressionAlgorithm.ZLIB);
				raw.position = 0;
				var buffer:String = raw.readUTFBytes(raw.length);
				var bufferArray:Array = buffer.split("\n");
				filesList = new Dictionary;
				var md5Name:String;
				var key:String;
				for (var i:int = 0, n:int = bufferArray.length; i < n; i++) {
					md5Name = bufferArray[i];
					key = md5Name.slice(0, md5Name.indexOf("?"));
					filesList[key] = md5Name;
				}
			} catch (e:Error) {
				filesList = null;
				CONFIG::DEBUG {
					Global.log("LoaderMgr::onLoaded not exist files.list");
				}
			}
		}
		
	}
}
import flash.display.Loader;
import flash.events.ErrorEvent;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLLoader;
import flash.net.URLLoaderDataFormat;
import flash.utils.ByteArray;
import flash.utils.Dictionary;
import flash.utils.Endian;

import FlashGE.common.Defines;
import FlashGE.common.Global;
import FlashGE.resources.LoaderMgr3D;

internal class LoaderItem
{
	public var isUrlLoader:Boolean;
	public var url:String;
	public var onLoadedListener:Function;
	public var next:LoaderItem;
	public var mgr:LoaderMgr3D;
	public var forbidLoad:Boolean;
	public var tryCount:int = 0;
	public var loaderHolder:LoaderHolder;
	
	public function doLoad(holder:LoaderHolder):void
	{
		if (this.forbidLoad) {
			onLoaded(null);
		} else {
			
			this.loaderHolder = holder;
			this.loaderHolder.isIdle = false;
			var needLoad:Boolean = true;
			if (this.isUrlLoader) {
				var curAllFxList:Dictionary = this.mgr.getAllFxList();
				if (curAllFxList != null) {
					var item:Object = curAllFxList[this.url];
					if (item != null) {
						needLoad = false;
						var itemData:ByteArray = new ByteArray;
						itemData.endian = Endian.LITTLE_ENDIAN;
						itemData.position = 0;
						
						var allFxRawData:ByteArray = this.mgr.getAllFxRawData();
						allFxRawData.position = item.offset;
						allFxRawData.readBytes(itemData, 0, item.fileSize);
						
						releaseLoaderHolder();
						
						var listener:Function = onLoadedListener;
						onLoadedListener = null;
						if (listener != null) {
							listener(itemData, mgr.isHighPriority);
						}
						
						mgr.decLoadCount(item.fileSize);
						
						this.url = null;
						this.mgr = null;
					}
				}
			}
			
			if (needLoad) {
				if (this.isUrlLoader) {
					var urlLoader:URLLoader = holder.urlLoader;
					if (urlLoader == null) {
						urlLoader = new URLLoader;
						urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
						holder.urlLoader = urlLoader;
					}
					
					urlLoader.addEventListener(Event.COMPLETE, onLoaded);
					urlLoader.addEventListener(IOErrorEvent.DISK_ERROR, onLoaded);
					urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onLoaded);
					urlLoader.addEventListener(IOErrorEvent.NETWORK_ERROR, onLoaded);
					urlLoader.addEventListener(IOErrorEvent.VERIFY_ERROR, onLoaded);
					urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onLoaded);
					urlLoader.load(Defines.createRequest(this.url, false, true));
				} else {
					var loader:Loader = holder.loader;
					if (loader == null) {
						loader = new Loader;
						holder.loader = loader;
					}
					loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoaded);
					loader.contentLoaderInfo.addEventListener(IOErrorEvent.DISK_ERROR, onLoaded);
					loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onLoaded);
					loader.contentLoaderInfo.addEventListener(IOErrorEvent.NETWORK_ERROR, onLoaded);
					loader.contentLoaderInfo.addEventListener(IOErrorEvent.VERIFY_ERROR, onLoaded);
					loader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onLoaded);
					loader.load(Defines.createRequest(this.url, false, true));
				}
			}
		}
	}
	
	private function onLoaded(e:Event):void
	{
		releaseLoaderHolder();
		if (e is ErrorEvent) {
			tryCount++;
			if (e is IOErrorEvent) {
				if ((e as IOErrorEvent).errorID == 2032) {
					tryCount = 10;
				}
			}
			if (tryCount < 10) {
				
				loaderHolder = null;
				var usedMgr:LoaderMgr3D = this.mgr;
				this.mgr = null;
				usedMgr.decLoadCount(0);
				usedMgr.pushLoaderItem(this, false);
				return;
			}
		}
		
		var listener:Function = onLoadedListener;
		onLoadedListener = null;
		if (listener != null) {
			if (this.isUrlLoader) {
				
				if (e == null || (e is ErrorEvent)) {
					listener(null, mgr.isHighPriority);
				} else {
					listener(e.target.data, mgr.isHighPriority);
				}
				
			} else {
				try {
					listener(e, mgr.isHighPriority);
				} catch (e1:Error) {
					CONFIG::DEBUG {
						Global.throwError("LoaderMgr::doLoad url=" + this.url);
					}
					listener(null, mgr.isHighPriority);
				}
			}
		}
		
		if (e==null ||(e is ErrorEvent)) {
			mgr.decLoadCount(0);
		} else {
			mgr.decLoadCount(e.target.bytesLoaded);
		}
		loaderHolder = null;
		this.url = null;
		this.mgr = null;
	}
	
	private function releaseLoaderHolder():void
	{
		if (loaderHolder != null) {
			if (this.isUrlLoader) {
				var urlLoader:URLLoader = loaderHolder.urlLoader;
				if (urlLoader != null) {
					urlLoader.removeEventListener(Event.COMPLETE, onLoaded);
					urlLoader.removeEventListener(IOErrorEvent.DISK_ERROR, onLoaded);
					urlLoader.removeEventListener(IOErrorEvent.IO_ERROR, onLoaded);
					urlLoader.removeEventListener(IOErrorEvent.NETWORK_ERROR, onLoaded);
					urlLoader.removeEventListener(IOErrorEvent.VERIFY_ERROR, onLoaded);
					urlLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onLoaded);
				}
			} else {
				var loader:Loader = loaderHolder.loader;
				if (loader != null) {
					loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onLoaded);
					loader.contentLoaderInfo.removeEventListener(IOErrorEvent.DISK_ERROR, onLoaded);
					loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onLoaded);
					loader.contentLoaderInfo.removeEventListener(IOErrorEvent.NETWORK_ERROR, onLoaded);
					loader.contentLoaderInfo.removeEventListener(IOErrorEvent.VERIFY_ERROR, onLoaded);
					loader.contentLoaderInfo.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onLoaded);
				}
			}
			
			this.loaderHolder.isIdle = true;
		}
	}
}

internal class LoaderHolder
{
	public var urlLoader:URLLoader;
	public var loader:Loader;
	public var isIdle:Boolean;
}