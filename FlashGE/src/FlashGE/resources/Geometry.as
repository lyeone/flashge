/*@author lye 123*/

package FlashGE.resources
{
	import flash.display3D.Context3D;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.VertexBuffer3D;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import flash.utils.Endian;
	
	import FlashGE.FlashGE;
	import FlashGE.common.AxisAlignedBox;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.common.Transform3D;
	import FlashGE.objects.VertexBuffer;
	import FlashGE.render.RenderUnit;

	CONFIG::DEBUG {
	import FlashGE.common.Global;
	}

	use namespace FlashGE;

	public class Geometry
	{
		static private var ctor:private_ctor;
		
		FlashGE var context3D:Context3D;
		FlashGE var vertexStreamsList:Vector.<VertexBuffer> = new Vector.<VertexBuffer>();
		FlashGE var indexBuffer3D:IndexBuffer3D;
		FlashGE var verticesCount:int;
		FlashGE var indicesBuffer:Vector.<uint> = new Vector.<uint>();
		FlashGE var attrStreamsList:Vector.<VertexBuffer> = new Vector.<VertexBuffer>();
		FlashGE var attrStreamsCount:uint = 0;
		FlashGE var attrOffsetsList:Vector.<int> = new Vector.<int>();
		FlashGE var attrStridesList:Vector.<int> = new Vector.<int>();
		FlashGE var renderUnit:RenderUnit;
		FlashGE var refCount:int = 1;
		
		CONFIG::DEBUG {
			static public var geometrysList:Dictionary = new Dictionary;
			
			public static function print():void
			{
				trace("GeometrysList::printf begin")
				for (var k:Geometry in geometrysList) {
					trace(k.refCount, k.verticesCount, k.indicesBuffer.length)
				}
				trace("GeometrysList::printf end")
			}
		}
		private static var idRander:int = 0;
		FlashGE var id:Number;
		public function Geometry(ctor:private_ctor, verticesCount:int) 
		{
			id = idRander++;
			this.verticesCount = verticesCount;
			CONFIG::DEBUG {
				Global.geometrysCount++;
			}
		}
		
		FlashGE function addRef():void
		{
			this.refCount++;
		}
		
		FlashGE function release(forceDispose3D:Boolean = false):void
		{
			this.refCount--;
			if (this.refCount <= 0){
				this.dispose();
			}
		}
		
		FlashGE function dispose():void
		{
			CONFIG::DEBUG {
				if (this.refCount == 0) {
					Global.geometrysCount--;
					delete geometrysList[this];
				}
			}
				
			this.dispose3D();
			var i:int, n:int;
			var vb:VertexBuffer;
			for (i = 0, n = vertexStreamsList.length; i < n; i++) {
				vb = vertexStreamsList[i];
				if (vb != null) {
					vb.attributes.length = 0;
					vb.data.length = 0;
				}
			}
			vertexStreamsList.length = 0;
			
			for (i = 0, n = attrStreamsList.length; i < n; i++) {
				vb = attrStreamsList[i];
				if (vb != null) {
					vb.attributes.length = 0;
					vb.data.length = 0;
				}
			}
			attrStreamsList.length = 0;
		}
		
		FlashGE function dispose3D():void
		{
			if (indexBuffer3D != null) {
				CONFIG::DEBUG {
					Global.indexBuffersCount--;
				}
				indexBuffer3D.dispose();
				indexBuffer3D = null;
			}
			
			var numBuffers:int = vertexStreamsList.length;
			for (var i:int = 0; i < numBuffers; i++) {
				var vBuffer:VertexBuffer = vertexStreamsList[i];
				if (vBuffer.vertBuffer3D != null) {
					CONFIG::DEBUG {
						Global.vertexBuffersCount--;
					}
					vBuffer.vertBuffer3D.dispose();
					vBuffer.vertBuffer3D = null;
				}
			}
			
			this.context3D = null;
		}
		
		static public function create(numVertices:int = 0):Geometry
		{
			var ret:Geometry = new Geometry(Geometry.ctor, numVertices);
			return ret;
		}
		
		public function getNumTriangles():int 
		{
			return indicesBuffer.length/3;
		}

		public function setVerticesCount(value:int):void 
		{
			if (verticesCount != value) {
				for each (var vBuffer:VertexBuffer in vertexStreamsList) {
					var numMappings:int = vBuffer.attributes.length;
					vBuffer.data.length = 4*numMappings*value;
				}
				verticesCount = value;
			}
		}
		
		public function addVertexStream(attributes:Array):int 
		{
			var numMappings:int = attributes.length;
			if (numMappings < 1 ) {
				Global.throwError("Must be at least one attribute ​​to create the buffer. numMappings=" + numMappings);
				return 0;
			}
			
			var vBuffer:VertexBuffer = new VertexBuffer();
			var newBufferIndex:int = vertexStreamsList.length;
			var attribute:uint = attributes[0];
			var stride:int = 1;
			for (var i:int = 1; i <= numMappings; i++) {
				var next:uint = (i < numMappings) ? attributes[i] : 0;
				if (next != attribute) {
					if (attribute != 0) {
						if (attribute < attrStreamsList.length && attrStreamsList[attribute] != null) {
							Global.throwError("Attribute " + attribute + " already used in this geometry.");
						}
						var numStandartFloats:int = Defines.getAttributeStride(attribute);
						if (numStandartFloats != 0 && numStandartFloats != stride) {
							Global.throwError("Standard attributes must be predefined size.");
						}
						if (attrStreamsList.length < attribute) {
							attrStreamsList.length = attribute + 1;
							attrOffsetsList.length = attribute + 1;
							attrStridesList.length = attribute + 1;
							attrStreamsCount = attrStreamsList.length;
						}
						
						var startIndex:int = i - stride;
						attrStreamsList[attribute] = vBuffer;
						attrOffsetsList[attribute] = startIndex;
						attrStridesList[attribute] = stride;
					}
					stride = 1;
				} else {
					stride++;
				}
				attribute = next;
			}
			vBuffer.attributes = attributes.slice();
			vBuffer.data = new ByteArray();
			vBuffer.data.endian = Endian.LITTLE_ENDIAN;
			vBuffer.data.length = 4*numMappings*verticesCount;
			vertexStreamsList[newBufferIndex] = vBuffer;
			return newBufferIndex;
		}

		public function getVertexStreamAttributes(index:int):Array 
		{
			return vertexStreamsList[index].attributes.slice();
		}

		public function hasAttribute(attribute:uint):Boolean
		{
			return attribute < attrStreamsList.length && attrStreamsList[attribute] != null;
		}

		public function findVertexStreamByAttribute(attribute:uint):int
		{
			var vBuffer:VertexBuffer = (attribute < attrStreamsList.length) ? attrStreamsList[attribute] : null;
			if (vBuffer != null) {
				for (var i:int = 0; i < vertexStreamsList.length; i++) {
					if (vertexStreamsList[i] == vBuffer) {
						return i;
					}
				}
			}
			return -1;
		}
		
		public function getAttributeOffset(attribute:uint):int
		{
			var vBuffer:VertexBuffer = (attribute < attrStreamsList.length) ? attrStreamsList[attribute] : null;
			if (vBuffer == null) {
				Global.throwError("Attribute not found.");
			}
			return attrOffsetsList[attribute];
		}

		public function setAttributeValues(attribute:uint, values:Vector.<Number>):void 
		{
			var vBuffer:VertexBuffer = (attribute < attrStreamsList.length) ? attrStreamsList[attribute] : null;
			if (vBuffer == null) {
				Global.throwError("Attribute not found.");
			}
			var stride:int = attrStridesList[attribute];
			if (values == null || values.length != stride*verticesCount) {
				Global.throwError("Values count must be the same.");
			}
			var numMappings:int = vBuffer.attributes.length;
			var data:ByteArray = vBuffer.data;
			var offset:int = attrOffsetsList[attribute];
			// Copy values
			for (var i:int = 0; i < verticesCount; i++) {
				var srcIndex:int = stride*i;
				data.position = 4*(numMappings*i + offset);
				for (var j:int = 0; j < stride; j++) {
					data.writeFloat(values[int(srcIndex + j)]);
				}
			}
		}

		public function getAttributeValues(attribute:uint):Vector.<Number>
		{
			var vBuffer:VertexBuffer = (attribute < attrStreamsList.length) ? attrStreamsList[attribute] : null;
			if (vBuffer == null) {
				Global.throwError("Attribute not found.");
			}
			var data:ByteArray = vBuffer.data;
			var stride:int = attrStridesList[attribute];
			var result:Vector.<Number> = new Vector.<Number>(stride*verticesCount);
			var numMappings:int = vBuffer.attributes.length;
			var offset:int = attrOffsetsList[attribute];
			// Copy values
			for (var i:int = 0; i < verticesCount; i++) {
				data.position = 4*(numMappings*i + offset);
				var dstIndex:int = stride*i;
				for (var j:int = 0; j < stride; j++) {
					result[int(dstIndex + j)] = data.readFloat();
				}
			}
			return result;
		}

		private function upload(context3D:Context3D):void
		{
			var vBuffer:VertexBuffer;
			var i:int;
			var numBuffers:int = vertexStreamsList.length;
			if (indexBuffer3D != null) {
				CONFIG::DEBUG {
					Global.indexBuffersCount--;
				}
				indexBuffer3D.dispose();
				indexBuffer3D = null;
			}
			
			for (i = 0; i < numBuffers; i++) {
				vBuffer = vertexStreamsList[i];
				if (vBuffer.vertBuffer3D != null) {
					CONFIG::DEBUG {
						Global.vertexBuffersCount--;
					}
					vBuffer.vertBuffer3D.dispose();
					vBuffer.vertBuffer3D = null;
				}
			}
			
			if (indicesBuffer.length <= 0 || verticesCount <= 0) {
				return;
			}

			try {
				for (i = 0; i < numBuffers; i++) {
					vBuffer = vertexStreamsList[i];
					var numMappings:int = vBuffer.attributes.length;
					var data:ByteArray = vBuffer.data;
					if (data == null) {
						Global.throwError("Cannot upload without vertex buffer data.");
					}
					vBuffer.vertBuffer3D = context3D.createVertexBuffer(verticesCount, numMappings);
					CONFIG::DEBUG {
						Global.vertexBuffersCount++;
					}
					vBuffer.vertBuffer3D.uploadFromByteArray(data, 0, 0, verticesCount);
				}
				var numIndices:int = indicesBuffer.length;
				indexBuffer3D = context3D.createIndexBuffer(numIndices);
				CONFIG::DEBUG {
					Global.indexBuffersCount++;
				}
				indexBuffer3D.uploadFromVector(indicesBuffer, 0, numIndices);
			} catch(e:Error) {
				
				this.dispose3D();
				
				CONFIG::DEBUG {
					Global.warning("Geometry::upload" + e.errorID + "," + e.message);
				}
					
				if (e.errorID == 3672) {
					Global.needDisposeContext3d = true;
				}
			}
			
			this.context3D = context3D;
		}

		public function updateIndexBufferInContextFromVector(data:Vector.<uint>, startOffset:int, count:int):void 
		{
			if (indexBuffer3D == null) {
				Global.throwError("Geometry must be uploaded.");
			}
			indexBuffer3D.uploadFromVector(data, startOffset, count);
		}

		public function updateIndexBufferInContextFromByteArray(data:ByteArray, byteArrayOffset:int, startOffset:int, count:int):void
		{
			if (indexBuffer3D == null) {
				Global.throwError("Geometry must be uploaded.");
			}
			indexBuffer3D.uploadFromByteArray(data, byteArrayOffset, startOffset, count);
		}

		public function updateVertexBufferInContextFromVector(index:int, data:Vector.<Number>, startVertex:int, numVertices:int):void 
		{
			if (indexBuffer3D == null) {
				Global.throwError("Geometry must be uploaded.");
			}
			vertexStreamsList[index].vertBuffer3D.uploadFromVector(data, startVertex, numVertices);
		}

		public function updateVertexBufferInContextFromByteArray(index:int, data:ByteArray, byteArrayOffset:int, startVertex:int, numVertices:int):void
		{
			if (indexBuffer3D == null) {
				Global.throwError("Geometry must be uploaded.");
			}
			vertexStreamsList[index].vertBuffer3D.uploadFromByteArray(data, byteArrayOffset, startVertex, numVertices);
		}

		public function getVertexBuffer(attribute:int, context3D:Context3D):VertexBuffer3D 
		{
			if (this.context3D != context3D) {
				this.upload(context3D);
			}
			
			if (attribute < attrStreamsList.length) {
				var stream:VertexBuffer = attrStreamsList[attribute];
				return stream != null ? stream.vertBuffer3D : null;
			} else {
				return null;
			}
		}
		
		public function getIndexBuffer(context3D:Context3D):IndexBuffer3D
		{
			if (this.context3D != context3D || this.indexBuffer3D == null) {
				this.upload(context3D);
			}
			return this.indexBuffer3D;
		}
		
		FlashGE function updateBoundBox(boundBox:AxisAlignedBox, transform:Transform3D = null):void
		{
			var vBuffer:VertexBuffer = (Defines.POSITION < attrStreamsList.length) ? attrStreamsList[Defines.POSITION] : null;
			if (vBuffer == null) {
				Global.throwError("Cannot calculate BoundBox without data.");
			}
			var offset:int = attrOffsetsList[Defines.POSITION];
			var numMappings:int = vBuffer.attributes.length;
			var data:ByteArray = vBuffer.data;

			for (var i:int = 0; i < verticesCount; i++) {
				data.position = 4*(numMappings*i + offset);
				var vx:Number = data.readFloat();
				var vy:Number = data.readFloat();
				var vz:Number = data.readFloat();
				var x:Number, y:Number, z:Number;
				if (transform != null) {
					x = transform.a*vx + transform.b*vy + transform.c*vz + transform.d;
					y = transform.e*vx + transform.f*vy + transform.g*vz + transform.h;
					z = transform.i*vx + transform.j*vy + transform.k*vz + transform.l;
				} else {
					x = vx;
					y = vy;
					z = vz;
				}
				if (x < boundBox.minX) boundBox.minX = x;
				if (x > boundBox.maxX) boundBox.maxX = x;
				if (y < boundBox.minY) boundBox.minY = y;
				if (y > boundBox.maxY) boundBox.maxY = y;
				if (z < boundBox.minZ) boundBox.minZ = z;
				if (z > boundBox.maxZ) boundBox.maxZ = z;
			}
		}
	}
}

internal class private_ctor{}
