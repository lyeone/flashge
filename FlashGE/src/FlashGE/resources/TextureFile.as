// .lye 2014-2-27
package FlashGE.resources
{
	import flash.display.BitmapData;
	import flash.display.LoaderInfo;
	import flash.display3D.Context3D;
	import flash.display3D.Context3DTextureFormat;
	import flash.display3D.textures.CubeTexture;
	import flash.display3D.textures.Texture;
	import flash.display3D.textures.TextureBase;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.filters.ConvolutionFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import flash.utils.Endian;
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.common.BitmapByteArray;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.loaders.RawDataReader;
	import FlashGE.loaders.TGADecoder;
	
	use namespace FlashGE;
	public class TextureFile
	{
		static public var forceATF:Boolean = false;
		/* refCount == 0, this.data is still valid!!! */
		static private var usingFiles:Dictionary = new Dictionary;
		static private var recycledFiles:Dictionary = new Dictionary;
		static private var filter:ConvolutionFilter = new ConvolutionFilter(2, 2, [1, 1, 1, 1], 4, 0, false, true);
		
		private var dataBytes:BitmapByteArray;
		private var bitmapData:BitmapData;
		private var loadState:int = Defines.LOAD_NULL;
		private var resizeAlign:Boolean = false;
		
		private var lr:LoaderInfo;
		private var refCount:int = 0;
		private var context3D:Context3D;
		private var texture3D:TextureBase;
		private var fileName:String = null;
		private var needUpdate:Boolean = true;
		private var realWidth:int = 0;
		private var realHeight:int= 0;
		private var alignWidth:int= 0;
		private var alignHeight:int= 0;
		
		private var isATF:Boolean = false;
		private var atfFormat:String;
		private var atfBytes:ByteArray;
		private var atfUploadReady:Boolean = false;
		private var pure3DTexture:Boolean = false;
		private var isCubeTexture:Boolean = false;
		private var usedFrame:int = 0;
		FlashGE var id:Number;
		
		CONFIG::DEBUG {
		private var videoMemory:int = 0;
		}
		FlashGE var loadListeners:Vector.<Function> = new Vector.<Function>;
		private var hasDisposed:Boolean = false;
		private var useLocalStorage:Boolean = false;
		
		static public function printAll():String
		{
			var tex:TextureFile = null;
			var ret:String = "TextureFile::printAll cache:\n";
			for each(tex in TextureFile.usingFiles) {
				ret += tex.toString() + "\n";
			}
			
			ret += "TextureFile::printAll recycled:\n";
			for each(tex in TextureFile.recycledFiles) {
				ret += tex.toString() + "\n";
			}
			
			ret += "MeshLoader::printAll end\n";
			return ret;
		}
		
		private static var idRander:int = 0;
		public function TextureFile()
		{
			id = idRander++;
		}
		
		public function getFileName():String
		{
			return this.fileName;
		}
		
		public function toString():String
		{
			return "tex:" + this.refCount + "," + this.fileName + "," + this.hasDisposed + "," + this.alignWidth + "x" + this.alignHeight;	
		}
		
		public function addListener(callback:Function):void
		{
			if (this.loadState == Defines.LOAD_OK || this.loadState == Defines.LOAD_FAILD) {
				callback(this, this.loadState == Defines.LOAD_OK);
			} else {
				this.loadListeners.push(callback);
			}		
		}
		
		private function postListeners(isOK:Boolean):void
		{
			for (var i:int = 0, len:int = this.loadListeners.length; i < len; i++) {
				var callback:Function = this.loadListeners[i];
				callback(this, isOK);
			}
			this.loadListeners.length = 0;
		}
		
		public function isLoaded():Boolean
		{
			return this.loadState == Defines.LOAD_OK || this.loadState == Defines.LOAD_FAILD;	
		}
		
		public function getLoadState():int
		{
			return this.loadState;
		}
		
		public function getRealWidth():int
		{
			return this.realWidth;
		}
		
		public function getRealHeight():int
		{
			return this.realHeight;
		}
		
		public function getAlignWidth():int
		{
			return this.alignWidth;
		}
		
		public function getAlignHeight():int
		{
			return this.alignHeight;	
		}
		
		public function addRef():void
		{
			this.usedFrame = Global.frameCount;
			this.refCount++;
		}
		
		public function release():void
		{
			this.refCount--;
			if (this.refCount == 0) {
				if (Global.useCacheData && this.fileName != null) {
					delete TextureFile.usingFiles[this.fileName];
					recycledFiles[this.fileName] = this;
				} else {
					this.dispose();
				}
			}
		}
		
		public function dispose():void
		{
			dataContext = null;
			hasDisposed = true;
			CONFIG::DEBUG {
				Global.texturesCount--;
			}
				
			if (bitmapData != null) {
				bitmapData.dispose();
				bitmapData = null;
			}
			
			if (this.atfBytes != null) {
				this.atfBytes.length = 0;
				this.atfBytes = null;
			}
			
			if (this.dataBytes != null) {
				this.dataBytes.dispose();
				this.dataBytes = null;
			}
			
			this.atfUploadReady = false;
			this.context3D = null;
			if (this.texture3D != null) {
				
				CONFIG::DEBUG {
					Global.videoMemoryCount -= this.videoMemory;
				}
					
				this.texture3D.removeEventListener(Event.TEXTURE_READY, onTextureUploadReady);
				this.texture3D.dispose();
				this.texture3D = null;
			}
		}
		
		static public function checkMemory():void
		{
			var tex:TextureFile = null;
			
			for each(tex in TextureFile.recycledFiles) {
				
				if (Global.useCacheData && tex.fileName != null && tex.fileName.length > 0) {
					delete TextureFile.usingFiles[tex.fileName];
				}
				tex.dispose();
			}
			
			TextureFile.recycledFiles = new Dictionary;
		}
				
		static public function hasFile(url:String):TextureFile
		{
			var useCache:Boolean = Global.useCacheData;
			url = Defines.baseResPath + url;
			var ret:TextureFile = (useCache ? TextureFile.recycledFiles[url] : null);
			if (ret != null) {
				TextureFile.usingFiles[url] = ret;
				delete TextureFile.recycledFiles[url];
			} else {
				ret = (useCache ? TextureFile.usingFiles[url] : null);
			}
			
			return ret;
		}
		
		static public function createFromFile(url:String, 
											  delayLoad:Boolean = false, 
											  resizeAlign:Boolean = false, 
											  listener:Function = null, 
											  insertFirst:Boolean = false,
											  idleLoader:Boolean = false,
											  moveToHighPriority:Boolean = false, 
											  checkFileList:Boolean = true, 
											  useLocalStorage:Boolean = false):TextureFile
		{
			var useCache:Boolean = Global.useCacheData;
			url = Defines.baseResPath + url;
			var ret:TextureFile = (useCache ? TextureFile.recycledFiles[url] : null);
			if (ret != null) {
				TextureFile.usingFiles[url] = ret;
				delete TextureFile.recycledFiles[url];
			} else {
				ret = (useCache ? TextureFile.usingFiles[url] : null);
			}
			
			
			if (ret == null) {
				ret = new TextureFile();
				ret.fileName = url;
				ret.addRef();
				ret.useLocalStorage = useLocalStorage;
				if (useCache) {
					TextureFile.usingFiles[url] = ret;
				}
				if (listener != null) {
					ret.addListener(listener);
				}
				ret.resizeAlign = resizeAlign;
				if (delayLoad==true) {
					ret.fileName = url;
				} else {
					ret.loadFromFile(url, insertFirst, idleLoader, moveToHighPriority, checkFileList);
				}
				
			} else {
				ret.addRef();
				if (idleLoader == false) {
					LoaderMgr3D.getInstance().checkIdle(url, insertFirst, moveToHighPriority);
				}
				if (listener != null) {
					ret.addListener(listener);
				}
			}
			
			return ret;
		}
		
		static public function createFrom3D(texture3D:Texture, alignWidth:int, alignHeight:int):TextureFile
		{
			var ret:TextureFile = new TextureFile();
			ret.addRef();
			ret.setTexture3D(texture3D);
			ret.alignWidth = alignWidth;
			ret.alignHeight = alignHeight;
			return ret;
		}
		
		private function setTexture3D(t:Texture):void
		{
			if (this.texture3D != null) {
				this.texture3D.removeEventListener(Event.TEXTURE_READY, onTextureUploadReady);
				this.texture3D.dispose();
				this.texture3D = null;
			}
			this.texture3D = t;
			this.loadState = Defines.LOAD_OK;
			this.pure3DTexture = true;
		}
		
		public function uploadFromByteArray(bytes:ByteArray, width:int, height:int, closeMipMap:Boolean = false):void
		{
			this.dataBytes = new BitmapByteArray;
			var wLog2Num:Number = Math.log(width)/Math.LN2;
			var hLog2Num:Number = Math.log(height)/Math.LN2;
			var wLog2:int = Math.ceil(wLog2Num);
			var hLog2:int = Math.ceil(hLog2Num);
			
			if (wLog2 != wLog2Num || hLog2 != hLog2Num || wLog2 > 11 || hLog2 > 11) {
				wLog2 = (wLog2 > 11) ? 11 : wLog2;
				hLog2 = (hLog2 > 11) ? 11 : hLog2;
				var dstWidth:int = 1 << wLog2;
				var dstHeight:int = 1 << hLog2;
				
				var dstBytes:ByteArray = new ByteArray;
				dstBytes.endian = Endian.LITTLE_ENDIAN;
				
				for (var y:int = 0; y < height; ++y) {
					dstBytes.position = dstWidth * 4 * y;
					for (var x:int = 0; x < width; ++x) {
						dstBytes.writeInt(bytes.readUnsignedInt());
					}
				}
				
				dstBytes.length = dstWidth * dstHeight * 4;
				this.dataBytes.init(dstBytes, dstWidth, dstHeight, closeMipMap);
				this.alignWidth = dstWidth;
				this.alignHeight = dstHeight;
			} else {
				this.dataBytes.init(bytes, width, height, closeMipMap);
				this.alignWidth = width;
				this.alignHeight = height;
			}
			
			if (this.texture3D != null) {
				this.texture3D.removeEventListener(Event.TEXTURE_READY, onTextureUploadReady);
				this.texture3D.dispose();
				this.texture3D = null;
			}
			
			this.atfUploadReady = false;
			this.needUpdate = true;
			this.realWidth = width;
			this.realHeight = height;
			this.loadState = Defines.LOAD_OK;
			CONFIG::DEBUG {
				this.videoMemory = (this.realWidth * this.realHeight * 4) * 1.25;
			}
		}
		
		static public function createFromByteArray(bytes:ByteArray, width:int, height:int):TextureFile
		{
			var ret:TextureFile = new TextureFile();
			ret.addRef();
			ret.fileName = null;
			ret.uploadFromByteArray(bytes, width, height);
			return ret;
		}
		
		private function loadFromFile(url:String, insertFirst:Boolean, idleLoader:Boolean = false, moveToHighPriority:Boolean = false, checkFileList:Boolean = true):void
		{
			this.pure3DTexture = false;
			this.fileName = url;
			this.loadState = Defines.LOAD_ING;
			var hasLoaded:Boolean = false;
			if (this.useLocalStorage) {
				var shareObj:SharedObject = SharedObject.getLocal("zszs");
				if (shareObj.data[url] != null) {
					CONFIG::DEBUG {
						Global.log("TextureFile::loadFromFile shareObj read", this.fileName);
					}
					this.atfBytes = shareObj.data[url]; 
					onLoaded(shareObj.data[url], moveToHighPriority);
					hasLoaded = true;
				}
			}
			
			if (!hasLoaded) {
				if (url.indexOf(".tga") == -1 && url.indexOf(".atf") == -1) {
					if (moveToHighPriority) {
						LoaderMgr3D.getInstance().pushHighPriorityLoader(url, onOtherLoaded, false, checkFileList, insertFirst);
					} else if (idleLoader) {
						LoaderMgr3D.getInstance().pushIdleLoader(url, onOtherLoaded, false, checkFileList, insertFirst);
					} else {
						LoaderMgr3D.getInstance().pushLoader(url, onOtherLoaded, false, checkFileList, insertFirst);
					}
				} else {
					if (moveToHighPriority) {
						LoaderMgr3D.getInstance().pushHighPriorityLoader(url, onLoaded, true, checkFileList, insertFirst);
					} else if (idleLoader) {
						LoaderMgr3D.getInstance().pushIdleLoader(url, onLoaded, true, checkFileList, insertFirst);
					} else {
						LoaderMgr3D.getInstance().pushLoader(url, onLoaded, true, checkFileList, insertFirst);
					}
				}
			}
		}
		
		public function getTexture(context3D:Context3D, create:Boolean = true):TextureBase
		{
			if (this.loadState == Defines.LOAD_FAILD || this.hasDisposed) {
				return null;
			}

			usedFrame = Global.frameCount;
			if (create == false) {
				if (this.isATF) {
					if (this.atfUploadReady) {
						return this.texture3D;
					}
					return null;
				}
				return this.texture3D;
			}
			
			if (this.pure3DTexture) {
				return this.texture3D;
			}
			
			if (this.loadState != Defines.LOAD_OK && this.loadState != Defines.LOAD_FAILD) {
				
				if (this.loadState == Defines.LOAD_NULL && this.fileName != null) {
					this.loadFromFile(this.fileName, false);
				}
				return null;
			}
			
			if (this.context3D != context3D && this.texture3D!=null) {
				this.texture3D.removeEventListener(Event.TEXTURE_READY, onTextureUploadReady);
				this.texture3D.dispose();
				this.texture3D = null;
				this.atfUploadReady = false;
				CONFIG::DEBUG {
					Global.videoMemoryCount -= this.videoMemory;
					Global.texturesCount--;
				}
			}
			
			if (this.context3D != context3D) {
				this.context3D = context3D;
			}
			var texture:Texture;
			
			if (this.isATF) {
				
				if (this.texture3D!=null) {
					if (this.atfUploadReady) {
						return this.texture3D;
					}
					return null;
				}
				
				if (this.atfBytes != null) {

					try {
						if (this.texture3D == null) {
							if (this.isCubeTexture) {
								this.texture3D = context3D.createCubeTexture(this.alignWidth, this.atfFormat, false);
							} else {
								this.texture3D = context3D.createTexture(this.alignWidth, this.alignHeight, this.atfFormat, false);
							}
							CONFIG::DEBUG {
								
								Global.texturesCount++;
								Global.videoMemoryCount += (this.videoMemory);
							}
						}
						
						this.texture3D.removeEventListener(Event.TEXTURE_READY, onTextureUploadReady);
						texture3D.addEventListener(Event.TEXTURE_READY, onTextureUploadReady);
						if (this.isCubeTexture) {
							CubeTexture(texture3D).uploadCompressedTextureFromByteArray(this.atfBytes, 0, true);
						} else {
							Texture(texture3D).uploadCompressedTextureFromByteArray(this.atfBytes, 0, true);
						}
					} catch(e:Error) {
						if (this.texture3D != null) {
							this.texture3D.removeEventListener(Event.TEXTURE_READY, onTextureUploadReady);
							this.texture3D.dispose();
							this.texture3D = null;
							
							CONFIG::DEBUG {
								
								Global.texturesCount--;
								Global.videoMemoryCount -= (this.videoMemory);
							}
						}
						
						CONFIG::DEBUG {
							Global.warning("TextureFile::getTexture(atf)" + this.fileName + "," + e.errorID + "," + e.message);
						}
						return null;
					}

					return null;
				}
			} else if (this.dataBytes != null) {
				
				if (this.texture3D == null || this.needUpdate == true) {
					try {
						
						var mipsLen:int = this.dataBytes.mipsList.length;
						if(this.texture3D == null) {
							if (mipsLen == 1) {
								this.texture3D = context3D.createTexture(this.dataBytes.width, 
									this.dataBytes.height, Context3DTextureFormat.BGRA, false);
							} else {
								this.texture3D = context3D.createTexture(this.dataBytes.width, 
									this.dataBytes.height, Context3DTextureFormat.BGRA, false);
							}
							
							CONFIG::DEBUG {
								Global.texturesCount++;
								Global.videoMemoryCount += (this.videoMemory);
							}
						}

						texture = Texture(this.texture3D);
						
						
						for (var i:int = 0; i < mipsLen; i++) {
							texture.uploadFromByteArray(dataBytes.mipsList[i], 0, i);
						}
						this.atfUploadReady = true;
					} catch(e:Error) {
						if (this.texture3D != null) {
							this.texture3D.removeEventListener(Event.TEXTURE_READY, onTextureUploadReady);
							this.texture3D.dispose();
							this.texture3D = null;
							
							CONFIG::DEBUG {								
								Global.texturesCount--;
								Global.videoMemoryCount -= (this.videoMemory);
							}
						}
						
						CONFIG::DEBUG {
							Global.warning("TextureFile::getTexture(data)" + this.fileName + "," + e.errorID + "," + e.message);
						}
						return null;
					}
					
					this.needUpdate = false;
				}
			} else if (this.bitmapData != null) {
				if (this.texture3D == null || this.needUpdate == true) {
					try {
						if(this.texture3D == null) {
							this.texture3D = context3D.createTexture(this.bitmapData.width, 
								this.bitmapData.height, Context3DTextureFormat.BGRA, false);
							
							CONFIG::DEBUG {
								Global.texturesCount++;
								Global.videoMemoryCount += (this.videoMemory);
							}
						}
						
						texture = Texture(this.texture3D);
						texture.uploadFromBitmapData(this.bitmapData);
						this.createMips(texture, bitmapData);
						this.atfUploadReady = true;
					} catch (e:Error) {
						if (this.texture3D != null) {
							this.texture3D.removeEventListener(Event.TEXTURE_READY, onTextureUploadReady);
							this.texture3D.dispose();
							this.texture3D = null;
							
							CONFIG::DEBUG {								
								Global.texturesCount--;
								Global.videoMemoryCount -= (this.videoMemory);
							}
						}
						
						CONFIG::DEBUG {
							Global.warning("TextureFile::getTexture(bmp)" + this.fileName + "," + e.errorID + "," + e.message);
						}
						return null;
					}
					this.needUpdate = false;
				}
			}

			if (this.atfUploadReady) {
				return this.texture3D;
			}
			
			return null;
		}
		
		private function onTextureUploadReady(e:Event):void
		{
			this.texture3D.removeEventListener(Event.TEXTURE_READY, onTextureUploadReady);
			this.atfUploadReady = true;
		}
		
		public function createMips(texture:Texture, bitmapData:BitmapData):void
		{
			var pool:Pool = Pool.getInstance();
			var rect:Rectangle = pool.popRectangle();
			rect.width = bitmapData.width;
			rect.height = bitmapData.height;
			var matrix:Matrix = pool.popMatrix();
			matrix.a = matrix.d = 0.5;
			matrix.b = matrix.c = 0;
			var point:Point = pool.popPoint();
			
			var level:int = 1;
			var bmp:BitmapData = new BitmapData(rect.width, rect.height, bitmapData.transparent, 0);
			var current:BitmapData = bitmapData;
			while (rect.width%2 == 0 || rect.height%2 == 0) {
				bmp.applyFilter(current, rect, point, filter);
				rect.width >>= 1;
				rect.height >>= 1;
				if (rect.width == 0) rect.width = 1;
				if (rect.height == 0) rect.height = 1;
				if (current != bitmapData) 
					current.dispose();
				
				current = new BitmapData(rect.width, rect.height, bitmapData.transparent, 0);
				current.draw(bmp, matrix, null, null, null, false);
				texture.uploadFromBitmapData(current, level++);
			}
			if (current != bitmapData) current.dispose();
			bmp.dispose();
			pool.pushMatrix(matrix);
			pool.pushPoint(point);
			pool.pushRectangle(rect);
		}
		
		FlashGE var dataContext:RawDataContext;
		private function onLoaded(rawData:ByteArray, isHighPriority:Boolean = false):void
		{
			if (rawData != null) {
				dataContext = new RawDataContext;
				dataContext.raw = rawData;
				dataContext.raw.endian = Endian.LITTLE_ENDIAN;
				dataContext.raw.position = 0;
				dataContext.isOnLoaded = true;
			}
			RawDataReader.getInstance().addListener(parseRawData);	
		}
		
		private function onOtherLoaded(e:Event, isHighPriority:Boolean = false):void
		{
			if (e != null && !(e is ErrorEvent)) {

				dataContext = new RawDataContext;
				dataContext.bitmapData = e.target.content.bitmapData;
				dataContext.isOnLoaded = false;
			}
			RawDataReader.getInstance().addListener(parseRawData);
		}
		
		private function parseRawData(timer:int):int
		{
			RawDataReader.getInstance().removeListener(parseRawData);
			this.usedFrame = Global.frameCount;
			var curTimer:int = getTimer();
			if (dataContext == null) {
				loadState = Defines.LOAD_FAILD;
				this.postListeners(false);
				
				return timer - (getTimer() - curTimer);
			}
			
			if (dataContext.isOnLoaded == false) {
				return parseRawDataOther(timer);
			}
			
			if (hasDisposed) {
				loadState = Defines.LOAD_FAILD;
				this.postListeners(false);
				return timer - (getTimer() - curTimer);
			}
			
			if (dataContext.raw == null) {
				CONFIG::DEBUG {
					Global.log("TextureFile::onLoaded", fileName);
				}
				loadState = Defines.LOAD_FAILD;
				this.postListeners(false);
				return timer - (getTimer() - curTimer);
			}
			
			var wLog2Num:Number = 0;
			var hLog2Num:Number = 0;
			var wLog2:int = 0;
			var hLog2:int = 0;
			
			if(this.fileName.indexOf(".atf") >=0) {
				this.isATF = true;
				parseATFTexture(dataContext.raw);
			} else {
				var tgaDecoder:TGADecoder = new TGADecoder();
				dataBytes = tgaDecoder.decode(dataContext.raw);
				this.realWidth = tgaDecoder._width;
				this.realHeight = tgaDecoder._height;
				this.alignWidth = dataBytes.width;
				this.alignHeight = dataBytes.height;
				
				wLog2Num = Math.log(dataBytes.width)/Math.LN2;
				hLog2Num = Math.log(dataBytes.height)/Math.LN2;
				wLog2 = Math.ceil(wLog2Num);
				hLog2 = Math.ceil(hLog2Num);
				
				if (wLog2 != wLog2Num || hLog2 != hLog2Num || wLog2 > 11 || hLog2 > 11) {
					CONFIG::DEBUG {
						Global.log("TextureFile::onLoaded: Texture size not a power of two. fileName="+this.getFileName());
					}
					dataBytes = null;
					loadState = Defines.LOAD_FAILD;
					return timer - (getTimer() - curTimer);
				}
				
				CONFIG::DEBUG {
					this.videoMemory = (this.realWidth * this.realHeight * 4) * 1.25;
				
					if (this.realWidth >=256 || this.realHeight >= 256 || this.alignWidth>=256 || this.alignHeight>=256) {
						Global.log("TextureFile::onLoaded filename="+this.fileName+",size=("+this.realWidth +"," + this.realHeight+"), alignSize=(" + this.alignWidth + "," + this.alignHeight +")");
					}
				}
			} // end else
			dataContext = null;
			loadState = Defines.LOAD_OK;
			this.postListeners(true);
			return timer - (getTimer() - curTimer);
		}
		
		private function parseRawDataOther(timer:int):int
		{
			var curTimer:int = getTimer();
			if (hasDisposed) {
				loadState = Defines.LOAD_FAILD;
				this.postListeners(false);
				return getTimer() - curTimer;
			}
			
			if (dataContext == null || dataContext.bitmapData == null) {
				CONFIG::DEBUG {
					Global.log("TextureFile::onOtherLoaded", fileName);
				}
				loadState = Defines.LOAD_FAILD;
				this.postListeners(false);
				return getTimer() - curTimer;
			}
			
			var wLog2Num:Number = 0;
			var hLog2Num:Number = 0;
			var wLog2:int = 0;
			var hLog2:int = 0;
			
			bitmapData = dataContext.bitmapData;
			
			this.realWidth = bitmapData.width;
			this.realHeight = bitmapData.height;
			var source:BitmapData = bitmapData;
			wLog2Num = Math.log(bitmapData.width)/Math.LN2;
			hLog2Num = Math.log(bitmapData.height)/Math.LN2;
			wLog2 = Math.ceil(wLog2Num);
			hLog2 = Math.ceil(hLog2Num);
			if (wLog2 != wLog2Num || hLog2 != hLog2Num || wLog2 > 11 || hLog2 > 11) {
				wLog2 = (wLog2 > 11) ? 11 : wLog2;
				hLog2 = (hLog2 > 11) ? 11 : hLog2;
				if (resizeAlign) {
					var resizeMatrix:Matrix = Pool.getInstance().popMatrix();
					resizeMatrix.b = resizeMatrix.c = resizeMatrix.tx = resizeMatrix.ty = 0;
					var scaleTex:Boolean = true;
					if (scaleTex) {
						resizeMatrix.a = (1 << wLog2)/bitmapData.width;
						resizeMatrix.d = (1 << hLog2)/bitmapData.height;
					}
					source = new BitmapData(1 << wLog2, 1 << hLog2, bitmapData.transparent, 0x0);						
					source.draw(bitmapData, resizeMatrix, null, null, null, false);
					Pool.getInstance().pushMatrix(resizeMatrix);
				} else {
					source = new BitmapData(1 << wLog2, 1 << hLog2, bitmapData.transparent, 0x0);
					source.draw(bitmapData, null, null, null, null, false);
				}
				bitmapData = source;
			}// end if
			
			this.alignWidth = bitmapData.width;
			this.alignHeight = bitmapData.height;	
			
			CONFIG::DEBUG {
				this.videoMemory = (this.realWidth * this.realHeight * 4) * 1.25;
				
				if (this.realWidth >=256 || this.realHeight >= 256 || this.alignWidth>=256 || this.alignHeight>=256) {
					Global.log("TextureFile::onOtherLoaded filename="+this.fileName+",size=("+this.realWidth +"," + this.realHeight+"), alignSize=(" + this.alignWidth + "," + this.alignHeight +")");
				}
			}
				
				loadState = Defines.LOAD_OK;
			this.postListeners(true);
			return getTimer() - curTimer;
		}
		
		public function getFormatID():uint
		{
			if (this.atfFormat == Context3DTextureFormat.COMPRESSED)
				return 0;
			
			if (this.atfFormat == Context3DTextureFormat.COMPRESSED_ALPHA)
				return 1;
			
			return 2;
		}
		
		public function isCube():Boolean
		{
			return this.isCubeTexture;
		}
		
		static private var hasFlushMax:Boolean = false;
		private function parseATFTexture(value:ByteArray):void
		{	
			if (this.useLocalStorage) {
				var shareObj:SharedObject = SharedObject.getLocal("zszs");
				if (shareObj.data[this.fileName] == null) {
					shareObj.data[this.fileName] = value;
					if (!hasFlushMax) {
						hasFlushMax = true;
						var ret:String = shareObj.flush(512*1024*1024);
						CONFIG::DEBUG {
							Global.log("TextureFile::parseATFTexture shareObj flush", ret, this.fileName);
						}
					}
				}
			}
			this.atfBytes = value;
			value.endian = Endian.LITTLE_ENDIAN;
			value.position = 0;
			
			var sign:String = value.readUTFBytes(3);
			var totalBytes:int = (value.readUnsignedByte() << 16) + (value.readUnsignedByte() << 8) + value.readUnsignedByte();
			var tdata:uint = value.readUnsignedByte();
			var ttype:int = tdata >> 7;
			var tformat:int = tdata & 0x7f;
			
			this.atfFormat = Context3DTextureFormat.COMPRESSED_ALPHA;
			
			switch (tformat)
			{
				case 0:
				case 1: this.atfFormat = Context3DTextureFormat.BGRA; break;
				case 2:
				case 3: this.atfFormat = Context3DTextureFormat.COMPRESSED; break;
				case 4:
				case 5: this.atfFormat = Context3DTextureFormat.COMPRESSED_ALPHA; break;
				default: Global.throwError("TextureFile::parseATFTexture Invalid ATF format");
			}
			
			switch (ttype)
			{
				case 0: isCubeTexture = false; break;
				case 1: isCubeTexture = true; break;
				
				default: Global.throwError("Invalid ATF type");
			}
			
			this.alignWidth = (1 << value.readUnsignedByte());
			this.alignHeight = (1 << value.readUnsignedByte());
			var numTextures:int = value.readUnsignedByte();
			this.atfBytes = value;
			if (this.atfBytes.length > totalBytes + 6) {
				this.atfBytes.position = totalBytes + 6;
				this.realWidth = this.atfBytes.readUnsignedShort();
				this.realHeight = this.atfBytes.readUnsignedShort();
			} else {
				this.realWidth = this.alignWidth;
				this.realHeight = this.alignHeight;
			}
			
			CONFIG::DEBUG {
				if (this.atfFormat == Context3DTextureFormat.BGRA) {
					this.videoMemory = (this.alignWidth * this.realHeight * 4) * 1.25;	
				} else if (this.atfFormat == Context3DTextureFormat.COMPRESSED) {
					this.videoMemory = (this.alignWidth * this.realHeight / 16) * 8 * 1.25;
				} else if (this.atfFormat == Context3DTextureFormat.COMPRESSED_ALPHA) {
					this.videoMemory = (this.alignWidth * this.realHeight / 8) * 8 * 1.25;
				}
			
				if (this.alignWidth>256 || this.alignHeight>256) {
					Global.log("TextureFile::parseATFTexture filename="+this.fileName+",size=("+this.realWidth +"," + this.realHeight+"), alignSize=(" + this.alignWidth + "," + this.alignHeight +")");
				}
			}
		}
	}
}

import flash.display.BitmapData;
import flash.utils.ByteArray;

internal class RawDataContext
{
	var raw:ByteArray;
	var bitmapData:BitmapData ;
	var isOnLoaded:Boolean;
}