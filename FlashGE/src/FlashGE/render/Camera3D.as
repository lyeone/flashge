/*@author lye 123*/

package FlashGE.render 
{
	import flash.display.Stage3D;
	import flash.display3D.Context3D;
	import flash.geom.Vector3D;
	CONFIG::FPS {
	import flash.system.System;
	import flash.text.TextFieldAutoSize;
	}
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.common.FrustumPlane;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.lights.DirectionLight;
	import FlashGE.lights.Shadow.NormalShadow;
	import FlashGE.lights.Shadow.ShadowMap;
	import FlashGE.loaders.RawDataReader;
	import FlashGE.objects.DefaultSilhouette;
	import FlashGE.objects.Entity;
	import FlashGE.objects.RenderObject;
	import FlashGE.objects.RenderScene;
	import FlashGE.resources.LoaderMgr3D;
	import FlashGE.profile.ProfileCollector;

	CONFIG::DEBUG {
	import flash.text.TextField;
	}
	
	use namespace FlashGE;
	
	public class Camera3D extends Entity 
	{
		public var view:ViewPort;
		public var orthographic:Boolean = false;
		public var isDebuger:Boolean = false;
		public var profile:ProfileCollector = new ProfileCollector;
		FlashGE var fov:Number = Math.PI / 2;
		FlashGE var nearClipping:Number;
		FlashGE var farClipping:Number;
		FlashGE var focalLength:Number;
		FlashGE var m0:Number;
		FlashGE var m5:Number;
		FlashGE var m10:Number;
		FlashGE var m14:Number;
		FlashGE var correctionX:Number;
		FlashGE var correctionY:Number;		
		
		
		FlashGE var m0P:Number;
		FlashGE var m5P:Number;
		FlashGE var m10P:Number;
		FlashGE var m14P:Number;
		FlashGE var correctionXP:Number;
		FlashGE var correctionYP:Number;	
		
		FlashGE var exTransform:Transform3D = new Transform3D;
		FlashGE var frustum:FrustumPlane;
		FlashGE var origins:Vector.<Vector3D> = new Vector.<Vector3D>();
		FlashGE var directions:Vector.<Vector3D> = new Vector.<Vector3D>();
		public var context3D:Context3D;
		FlashGE var context3DProperties:Context3DProperties = new Context3DProperties();
		FlashGE var renderer:Renderer = new Renderer("main");
		
		FlashGE var rootNode:Entity; 
		
		private var offsetX:Number = 0, offsetY:Number = 0, offsetZ:Number = 0;
		private var origPosValid:Boolean = false;
		private var origX:Number = 0, origY:Number = 0, origZ:Number = 0;
		FlashGE var lookAtX:Number = 0, lookAtY:Number = 0, lookAtZ:Number = 0;
		private var origLookAtX:Number = 0, origLookAtY:Number = 0, origLookAtZ:Number = 0;
		private var lookAtOffsetX:Number = 0, lookAtOffsetY:Number = 0, lookAtOffsetZ:Number = 0;
		private var camRotX:Number = 0, camRotY:Number = 0, camRotZ:Number = 0;
		private var camType:uint = 0;
		private var observeObj:RenderObject = null;
		private var composeParent:Boolean = true;
		private var origProjectType:Boolean = false;
		
		FlashGE var lightDir:Vector3D = new Vector3D;
		FlashGE var shadowCalculor:ShadowMap;
		FlashGE var normalShadow:NormalShadow = new NormalShadow;
		FlashGE var defaultSilhouette:DefaultSilhouette = new DefaultSilhouette;
		
		FlashGE var directionalLight:DirectionLight = new DirectionLight;
		FlashGE var firstFrameTimer:Number = -1;
		FlashGE var mulColorFactor:Number = 1;
		FlashGE var addColorFactor:Number = 0;
		FlashGE var sunColorR:Number = 1;
		FlashGE var sunColorG:Number = 1;
		FlashGE var sunColorB:Number = 1;
		FlashGE var sunColorSceneR:Number = 1;
		FlashGE var sunColorSceneG:Number = 1;
		FlashGE var sunColorSceneB:Number = 1;
		FlashGE var cameraChanged:Boolean = false;
		FlashGE var getCurTimer:Function = getTimer;
		public var loaderMgr:LoaderMgr3D;
		public var closeDeltaTimer:Boolean = false;
		public var renderScene:RenderScene = new RenderScene;
		public var rawDataReader:RawDataReader;
		public var pool:Pool;
		public var closeCollectRenderUnits:Boolean = false;
		public var aniSpeedExtra:Number = 0;
		CONFIG::DEBUG {			
			FlashGE var fpsPanel:TextField;
		}
			
		public function Camera3D(nearClipping:Number, farClipping:Number)
		{
			this.rawDataReader = RawDataReader.getInstance();
			this.renderScene.camera = this;
			this.nearClipping = nearClipping;
			this.farClipping = farClipping;
			frustum = new FrustumPlane();
			frustum.next = new FrustumPlane();
			frustum.next.next = new FrustumPlane();
			frustum.next.next.next = new FrustumPlane();
			frustum.next.next.next.next = new FrustumPlane();
			frustum.next.next.next.next.next = new FrustumPlane();
			
			// 太阳光的方向，从光源发出
			this.setLightDir(-1, 2.5, -3);
			this.openShadowMap(false);
			loaderMgr = LoaderMgr3D.getInstance();
			pool = Pool.getInstance();
		}
		
		public function getContext3D():Context3D
		{
			return this.context3D;
		}
		
		public function openShadowMap(open:Boolean):ShadowMap
		{
			if (open && shadowCalculor == null) {
				shadowCalculor = new ShadowMap(1000, 1000, -500, 500, 1024, 0.2);
			} else if(!open && shadowCalculor !=null) {
				shadowCalculor.dispose();
				shadowCalculor = null;
			}
			return shadowCalculor;
		}
		
		public function getShadowCalculor():ShadowMap
		{
			return this.shadowCalculor;
		}
		
		public function getNormalShadow():NormalShadow
		{
			return normalShadow;
		}
		
		public function getDefaultSilhouette():DefaultSilhouette
		{
			return defaultSilhouette;
		}
		
		public function setRootNode(rootNode:Entity):void
		{
			this.rootNode = rootNode;
		}
		
		public function getRootNode():Entity
		{
			return this.rootNode;
		}
		
		public function SetCameraOffset(x:Number, y:Number, z:Number):void
		{
			this.offsetX = x, this.offsetY = y, this.offsetZ = z;	
		}	
		
		public function SetCameraRot(x:Number, y:Number, z:Number):void
		{
			this.camRotX = x, this.camRotY = y, this.camRotZ = z;
		}
		
		public function SetCameraLookAtOffset(x:Number, y:Number, z:Number):void
		{
			this.lookAtOffsetX = x, this.lookAtOffsetY = y, this.lookAtOffsetZ = z;	
		}
		
		public function SetCamType(t:uint):void
		{
			this.camType = t;
		}
		
		public function SetCamTarget(o:RenderObject):void
		{
			this.observeObj = o;
		}
		
		public function UpdateSceneCamera(offsetValid:Boolean):void
		{
			if (offsetValid)
			{
				if (!this.origPosValid) {
					this.origX = this.x, this.origY = this.y, this.origZ = this.z;
					this.origLookAtX = this.lookAtX, this.origLookAtY = this.lookAtY, this.origLookAtZ = this.lookAtZ;
					this.origPosValid = true;
					this.origProjectType = this.orthographic;
				}
				
				var targetX:Number, targetY:Number, targetZ:Number;
				var targetRotX:Number, targetRotY:Number, targetRotZ:Number;
				targetX = targetY = targetZ = 0;
				targetRotX = targetRotY = targetRotZ = 0;
				
				if (this.observeObj) {
					targetX = this.observeObj.x;
					targetY = this.observeObj.y;
					targetZ = this.observeObj.z;
					
					targetRotX = this.observeObj.rotationX;
					targetRotY = this.observeObj.rotationY;
					targetRotZ = this.observeObj.rotationZ;
				}
				
				var rotLocal:Transform3D = pool.popTransform3D();
				rotLocal.fromEulerAngles(camRotX, camRotY, camRotZ);
				
				var rotObj:Transform3D = pool.popTransform3D();
				rotObj.fromEulerAngles(targetRotX, targetRotY, targetRotZ);
				rotLocal.append(rotObj);
				
				this.x = this.offsetX*rotLocal.a + this.offsetY*rotLocal.b + this.offsetZ*rotLocal.c + targetX;
				this.y = this.offsetX*rotLocal.e + this.offsetY*rotLocal.f + this.offsetZ*rotLocal.g + targetY;
				this.z = this.offsetX*rotLocal.i + this.offsetY*rotLocal.j + this.offsetZ*rotLocal.k + targetZ;
				
				this.lookAtX = this.lookAtOffsetX*rotLocal.a + this.lookAtOffsetY*rotLocal.b + this.lookAtOffsetZ*rotLocal.c + targetX;
				this.lookAtY = this.lookAtOffsetX*rotLocal.e + this.lookAtOffsetY*rotLocal.f + this.lookAtOffsetZ*rotLocal.g + targetY;
				this.lookAtZ = this.lookAtOffsetX*rotLocal.i + this.lookAtOffsetY*rotLocal.j + this.lookAtOffsetZ*rotLocal.k + targetZ;
				
				this.orthographic = false;
				pool.pushTransform3D(rotLocal);
				pool.pushTransform3D(rotObj);
				//trace("UpdateSceneCamera cam: \t" +  debugx + "\t" +  debugy + "\t" + debugz+ "\t"  +  vec.x + "\t" +  vec.y + "\t" + vec.z + "\t" + this.cameraScaleRatio);
				
				this.lookAt(lookAtX, lookAtY, lookAtZ);
				this.composeParent = false;
			} else if (this.origPosValid && !offsetValid){
				this.orthographic = this.origProjectType;
				//trace("========================UpdateSceneCamera, reset camera================================");
				this.origPosValid = false;
				this.x = this.origX, this.y = this.origY, this.z = this.origZ;
				this.lookAt(this.origLookAtX, this.origLookAtY, this.origLookAtZ);
				this.SetCamType(0);
			}
		}
		
		public function UpdateShakeCamera(offsetValid:Boolean):void
		{
			if (!this.origPosValid && offsetValid)
			{
				this.origPosValid = true;
				this.origX = this.x, this.origY = this.y, this.origZ = this.z;
				this.origLookAtX = this.lookAtX, this.origLookAtY = this.lookAtY, this.origLookAtZ = this.lookAtZ;
				
				this.x = this.origX + this.offsetX;
				this.y = this.origY + this.offsetY;
				this.z = this.origZ + this.offsetZ;
				
			} else if (this.origPosValid && offsetValid) {
				this.x = this.origX + this.offsetX;
				this.y = this.origY + this.offsetY;
				this.z = this.origZ + this.offsetZ;
				
			} else if (this.origPosValid && !offsetValid){
				
				this.origPosValid = false;
				this.x = this.origX, this.y = this.origY, this.z = this.origZ;
				this.lookAt(this.origLookAtX, this.origLookAtY, this.origLookAtZ);
				this.SetCamType(0);
			}
		}
		
		public function UpdateRenderCamera():void
		{
			this.composeParent = true;
			//cam offset included,TODO:
			var offsetValid:Boolean = Math.abs(this.offsetX) + Math.abs(this.offsetY) + Math.abs(this.offsetZ) > 0 ||
				Math.abs(this.lookAtOffsetX) + Math.abs(this.lookAtOffsetY) + Math.abs(this.lookAtOffsetZ) > 0 ||
				Math.abs(this.camRotX) + Math.abs(this.camRotY) + Math.abs(this.camRotZ) > 0;
			
			if (this.camType == 1) {//global space
				UpdateSceneCamera(offsetValid);
			} else if (this.camType == 2) {//local space
				UpdateShakeCamera(offsetValid);
			} else{//do nth
			}
		}
		
		public function setLightDir(x:Number, y:Number, z:Number):void
		{
			this.lightDir.setTo(x, y, z);
			var dx:Number = x;
			var dy:Number = y;
			var dz:Number = z;
			var rx:Number = Math.atan2(dz, Math.sqrt(dx*dx + dy*dy)) - Math.PI/2;
			var ry:Number = 0;
			var rz:Number = -Math.atan2(dx, dy);
			directionalLight.setRotation(rx, ry, rz);
			directionalLight.composeTransforms();
		}
		
		private var curDeltaTimerTotal:Number = 0;
		private var curDeltaTimerCount:int = 0;
		private var preFrameTimer:Number = 0;
		public	var curDeltaTimer:Number = 0;
		public	var curFrameDeltaTimer:Number = 0;
		public  var enableEffectUseDeltaTimer:Boolean = true;
		public function render(stage3D:Stage3D):void
		{
			var curTimer:Number = getTimer();
			if (this.preFrameTimer == 0) {
				preFrameTimer = curTimer;
			}
			
			curFrameDeltaTimer = (curTimer - this.preFrameTimer);//*speed;
			curDeltaTimer = curFrameDeltaTimer;
			if (curDeltaTimer < 80) {
				curDeltaTimerTotal += curDeltaTimer;
				curDeltaTimerCount++;
			} else if (curDeltaTimerCount > 0) {
				curDeltaTimer = curDeltaTimerTotal / curDeltaTimerCount;
			}
			
			if (curDeltaTimer > 80) {
				curDeltaTimer = 80;
			}
			if (curDeltaTimerTotal > 10000) {
				curDeltaTimerTotal = 0;
				curDeltaTimerCount = 0;
			}

			preFrameTimer = curTimer;
			renderImpl(stage3D);
			loaderMgr.onEnterFrame();
			
			if (Global.needDisposeContext3d == true) {
				Global.needDisposeContext3d = false;
				Global.particlesErrorCount = 0;
				stage3D.context3D.dispose();
			}
		}
		
		FlashGE var localToGlobalTransform:Transform3D = new Transform3D();
		FlashGE var globalToLocalTransform:Transform3D = new Transform3D();
		public function renderImpl(stage3D:Stage3D):void
		{	
			if (context3D != stage3D.context3D) {
				Global.particlesErrorCount = 0;
			}
			context3D = stage3D.context3D;
			if (context3D == null)
				return;
				
			UpdateRenderCamera();
			
			Global.frameCount++;
			
			renderer.camera = this;			
			calculateProjection(view.viewWidth, view.viewHeight);
			var ret:Boolean = view.configureContext3D(context3D, this);
			cameraChanged = cameraChanged || (ret || transformChanged);
		
			var root:Entity = this;
			
			this.composeTransforms();
			this.localToGlobalTransform.copy(transform);
			this.globalToLocalTransform.copy(inverseTransform);
			this.transform.append(this.exTransform);
			
			inverseTransform.copy(this.transform);
			inverseTransform.invert();
			
			while (root.getParent() != null) {
				root = root.getParent();
				if (root.transformChanged) {
					root.composeTransforms();
					//root.localToCameraChanged = true;
					cameraChanged = true;
				}
				if (this.composeParent) {
					localToGlobalTransform.append(root.transform);
					globalToLocalTransform.prepend(root.inverseTransform); 
					//this.localToCameraChanged = this.localToCameraChanged || root.localToCameraChanged; 
				}
			}

			if (root.visible) {
				
					//if (root.localToCameraChanged || this.localToCameraChanged) {
						root.cameraToLocalTransform.combine(root.inverseTransform, localToGlobalTransform);
						root.localToCameraTransform.combine(globalToLocalTransform, root.transform);
					//}
					
					if (root.boundBox != null) {
						calculateFrustum(root.cameraToLocalTransform);
						root.culledType = root.boundBox.checkFrustumCulling(frustum, 63);
					} else {
						root.culledType = 63;
					}
					
					if (root.culledType >= 0) {
						root.calculateVisibility(this);
					}
					
					root.calculateChildrenVisibility(this);
					//root.localToCameraChanged = false;
					//this.localToCameraChanged = false;
					
				if (root.culledType >= 0) {
					root.collectRenderUnits(this, false);
				}
				
				root.collectChildrenRenderUnits(this, false);
				
				if (!this.closeCollectRenderUnits) {
					if (this.shadowCalculor != null) {
						directionalLight.cameraToLocalTransform.combine(directionalLight.inverseTransform, root.cameraToLocalTransform);
						this.shadowCalculor.calculateShadowMap(this, directionalLight.cameraToLocalTransform);
					}
				
					normalShadow.collectShadows(this);
					defaultSilhouette.collectShadows(this);
				}
			}
			
			if (!this.closeCollectRenderUnits) {
				var r:Number = ((view.backgroundColor >> 16) & 0xff)/0xff;
				var g:Number = ((view.backgroundColor >> 8) & 0xff)/0xff;
				var b:Number = ((view.backgroundColor & 0xff))/0xff;
				context3D.clear(r, g, b, 0, 1);
				renderer.render(context3D);
				context3D.present();
			}
			
			//rawDataReader.check(8);
			
			CONFIG::FPS {
				if (this.fpsPanel == null) {
					this.fpsPanel = new TextField;
					fpsPanel.y = 10;
					fpsPanel.autoSize = TextFieldAutoSize.LEFT;
					fpsPanel.textColor = 0xffffff;
					fpsPanel.multiline = true;
					fpsPanel.background =  true;
					fpsPanel.backgroundColor = 0;
					fpsPanel.alpha = 0.8;
					
					Global.mainSprite.stage.addChild(this.fpsPanel);
				}
				
				var details:String = "video:"+bytesToString(Global.videoMemoryCount)
					//+"\ngeo:" + Global.geometrysCount
					//+"\nvb:" + Global.vertexBuffersCount
					//+"\nib:" + Global.indexBuffersCount
					//+"\ntex:" + Global.texturesCount
					+"\nparticle:" + Global.particlesCount
					//+"\nmaxParticle:" + Global.maxParticlesCount
					//+"\npoolParticles:" + pool.particlesCount
					+"\ndraws:" + Global.drawsCount
					+"\nfreeMemory:" + bytesToString(System.freeMemory)
					+"\nprivateMemory:" + bytesToString(System.privateMemory)
					+"\nprocessCPUUsage:" + (System.processCPUUsage)
					+"\ntotalMemory:" + bytesToString(System.totalMemory)
					+"\ntotalMemoryNumber:" + bytesToString(System.totalMemoryNumber);
				fpsPanel.text = details;
			} // end CONFIG::DEBUG
				
				CONFIG::DEBUG {
					if (Global.maxParticlesCount < Global.particlesCount) {
						Global.maxParticlesCount = Global.particlesCount;
					}
					Global.particlesCount = 0;
					Global.drawsCount = 0;
				}
				
				
			cameraChanged = false;
		}
		
		CONFIG::FPS {
			public function showFPS(show:Boolean):void
			{
				if (fpsPanel == null) {
					return;
				}
					
				if (show) {
					if (this.fpsPanel.parent == null) {
						Global.mainSprite.stage.addChild(this.fpsPanel);
					}
				} else {
					if (this.fpsPanel.parent != null) {
						this.fpsPanel.parent.removeChild(this.fpsPanel);
					}
				}
			}
		}
		
		private function bytesToString(bytes:int):String {
			if (bytes < 1024) return bytes + "B";
			else if (bytes < 10240) return (bytes / 1024).toFixed(2) + "K";
			else if (bytes < 102400) return (bytes / 1024).toFixed(1) + "K";
			else if (bytes < 1048576) return (bytes >> 10) + "K";
			else if (bytes < 10485760) return (bytes / 1048576).toFixed(2) + "M";
			else if (bytes < 104857600) return (bytes / 1048576).toFixed(1) + "M";
			else return String(bytes >> 20) + "M";
		}
		
		public function lookAtOffset(dx:Number, dy:Number, dz:Number):void
		{
			this.lookAt(this.lookAtX + dx, this.lookAtY + dy, this.lookAtZ + dz);	
		}
		
		public function lookAt(x:Number, y:Number, z:Number):void
		{			
			this.lookAtX = x, this.lookAtY = y, this.lookAtZ = z;
			var deltaX:Number = x - this.x;
			var deltaY:Number = y - this.y;
			var deltaZ:Number = z - this.z;
			var rotX:Number = Math.atan2(deltaZ, Math.sqrt(deltaX * deltaX + deltaY * deltaY));
			rotationX = rotX - 0.5 * Math.PI;
			rotationY = 0;
			rotationZ =  -  Math.atan2(deltaX,deltaY);
		}
		
		public function setExTransform(trans:Transform3D):void
		{
			exTransform.copy(trans);
		}
		
		public function getExTransform():Transform3D
		{
			return this.exTransform;
		}
		
		FlashGE function calculateProjection(width:Number, height:Number):void
		{
			var viewSizeX:Number = width * 0.5;
			var viewSizeY:Number = height * 0.5;
			focalLength = Math.sqrt(viewSizeX * viewSizeX + viewSizeY * viewSizeY) / Math.tan(fov * 0.5);
			if (!orthographic) {
				m0 = focalLength / viewSizeX;
				m5 = -focalLength / viewSizeY;
				m10 = farClipping / (farClipping - nearClipping);
				m14 = -nearClipping * m10;
			} else {
				m0 = 1 / viewSizeX;
				m5 = -1 / viewSizeY;
				m10 = 1 / (farClipping - nearClipping);
				m14 = -nearClipping * m10;
			}
			correctionX = viewSizeX / focalLength;
			correctionY = viewSizeY / focalLength;
			
			m0P = focalLength / viewSizeX;
			m5P = -focalLength / viewSizeY;
			m10P = farClipping / (farClipping - nearClipping);
			m14P = -nearClipping * m10P;
		}
		
		public function getRayInCamera(screenX:Number, screenY:Number, pos:Vector3D, dir:Vector3D):void
		{
			var aspect:Number = view.viewWidth / view.viewHeight;
			var halfcx:Number = view.viewWidth * 0.5;
			var halfcy:Number = view.viewHeight * 0.5;

			var xPercent:Number = ((screenX - halfcx) / halfcx);
			var yPercent:Number = ((halfcy - screenY) / halfcy);
			var nearPlaneHalfSize:Number = this.nearClipping * Math.tan(fov*0.5);

			pos.x = xPercent * nearPlaneHalfSize * aspect;
			pos.y = yPercent * nearPlaneHalfSize;
			pos.z = nearClipping;
			dir = pos.clone();
			dir.normalize();
		}
		
		FlashGE function calculateFrustum(transform:Transform3D):void 
		{
			var nearPlane:FrustumPlane = frustum;
			var farPlane:FrustumPlane = nearPlane.next;
			var leftPlane:FrustumPlane = farPlane.next;
			var rightPlane:FrustumPlane = leftPlane.next;
			var topPlane:FrustumPlane = rightPlane.next;
			var bottomPlane:FrustumPlane = topPlane.next;
			if (!orthographic) {
				var fa:Number = transform.a * correctionX;
				var fe:Number = transform.e * correctionX;
				var fi:Number = transform.i * correctionX;
				var fb:Number = transform.b * correctionY;
				var ff:Number = transform.f * correctionY;
				var fj:Number = transform.j * correctionY;
				nearPlane.x = fj * fe - ff * fi;
				nearPlane.y = fb * fi - fj * fa;
				nearPlane.z = ff * fa - fb * fe;
				nearPlane.offset = (transform.d + transform.c * nearClipping) * nearPlane.x + (transform.h + transform.g * nearClipping) * nearPlane.y + (transform.l + transform.k * nearClipping) * nearPlane.z;
				
				farPlane.x = -nearPlane.x;
				farPlane.y = -nearPlane.y;
				farPlane.z = -nearPlane.z;
				farPlane.offset = (transform.d + transform.c * farClipping) * farPlane.x + (transform.h + transform.g * farClipping) * farPlane.y + (transform.l + transform.k * farClipping) * farPlane.z;
				
				var ax:Number = -fa - fb + transform.c;
				var ay:Number = -fe - ff + transform.g;
				var az:Number = -fi - fj + transform.k;
				var bx:Number = fa - fb + transform.c;
				var by:Number = fe - ff + transform.g;
				var bz:Number = fi - fj + transform.k;
				topPlane.x = bz * ay - by * az;
				topPlane.y = bx * az - bz * ax;
				topPlane.z = by * ax - bx * ay;
				topPlane.offset = transform.d * topPlane.x + transform.h * topPlane.y + transform.l * topPlane.z;
				// Right plane.
				ax = bx;
				ay = by;
				az = bz;
				bx = fa + fb + transform.c;
				by = fe + ff + transform.g;
				bz = fi + fj + transform.k;
				rightPlane.x = bz * ay - by * az;
				rightPlane.y = bx * az - bz * ax;
				rightPlane.z = by * ax - bx * ay;
				rightPlane.offset = transform.d * rightPlane.x + transform.h * rightPlane.y + transform.l * rightPlane.z;
				// Bottom plane.
				ax = bx;
				ay = by;
				az = bz;
				bx = -fa + fb + transform.c;
				by = -fe + ff + transform.g;
				bz = -fi + fj + transform.k;
				bottomPlane.x = bz*ay - by*az;
				bottomPlane.y = bx*az - bz*ax;
				bottomPlane.z = by*ax - bx*ay;
				bottomPlane.offset = transform.d*bottomPlane.x + transform.h*bottomPlane.y + transform.l*bottomPlane.z;
				// Left plane.
				ax = bx;
				ay = by;
				az = bz;
				bx = -fa - fb + transform.c;
				by = -fe - ff + transform.g;
				bz = -fi - fj + transform.k;
				leftPlane.x = bz*ay - by*az;
				leftPlane.y = bx*az - bz*ax;
				leftPlane.z = by*ax - bx*ay;
				leftPlane.offset = transform.d*leftPlane.x + transform.h*leftPlane.y + transform.l*leftPlane.z;
			} else {
				var viewSizeX:Number = view.viewWidth*0.5;
				var viewSizeY:Number = view.viewHeight*0.5;
				// Near plane.
				nearPlane.x = transform.j*transform.e - transform.f*transform.i;
				nearPlane.y = transform.b*transform.i - transform.j*transform.a;
				nearPlane.z = transform.f*transform.a - transform.b*transform.e;
				nearPlane.offset = (transform.d + transform.c*nearClipping)*nearPlane.x + (transform.h + transform.g*nearClipping)*nearPlane.y + (transform.l + transform.k*nearClipping)*nearPlane.z;
				// Far plane.
				farPlane.x = -nearPlane.x;
				farPlane.y = -nearPlane.y;
				farPlane.z = -nearPlane.z;
				farPlane.offset = (transform.d + transform.c*farClipping)*farPlane.x + (transform.h + transform.g*farClipping)*farPlane.y + (transform.l + transform.k*farClipping)*farPlane.z;
				// Top plane.
				topPlane.x = transform.i*transform.g - transform.e*transform.k;
				topPlane.y = transform.a*transform.k - transform.i*transform.c;
				topPlane.z = transform.e*transform.c - transform.a*transform.g;
				topPlane.offset = (transform.d - transform.b*viewSizeY)*topPlane.x + (transform.h - transform.f*viewSizeY)*topPlane.y + (transform.l - transform.j*viewSizeY)*topPlane.z;
				// Bottom plane.
				bottomPlane.x = -topPlane.x;
				bottomPlane.y = -topPlane.y;
				bottomPlane.z = -topPlane.z;
				bottomPlane.offset = (transform.d + transform.b*viewSizeY)*bottomPlane.x + (transform.h + transform.f*viewSizeY)*bottomPlane.y + (transform.l + transform.j*viewSizeY)*bottomPlane.z;
				// Left plane.
				leftPlane.x = transform.k*transform.f - transform.g*transform.j;
				leftPlane.y = transform.c*transform.j - transform.k*transform.b;
				leftPlane.z = transform.g*transform.b - transform.c*transform.f;
				leftPlane.offset = (transform.d - transform.a*viewSizeX)*leftPlane.x + (transform.h - transform.e*viewSizeX)*leftPlane.y + (transform.l - transform.i*viewSizeX)*leftPlane.z;
				// Right plane.
				rightPlane.x = -leftPlane.x;
				rightPlane.y = -leftPlane.y;
				rightPlane.z = -leftPlane.z;
				rightPlane.offset = (transform.d + transform.a*viewSizeX)*rightPlane.x + (transform.h + transform.e*viewSizeX)*rightPlane.y + (transform.l + transform.i*viewSizeX)*rightPlane.z;
			}
		}
		
	}
}
