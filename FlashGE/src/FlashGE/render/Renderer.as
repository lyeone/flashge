/*@author lye 123*/
package FlashGE.render
{	
	import flash.display3D.Context3D;
	import flash.display3D.Context3DCompareMode;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Program3D;
	import flash.utils.Dictionary;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.objects.Entity;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;
	
	public class Renderer 
	{
		FlashGE var name:String;
		FlashGE var camera:Camera3D;
		FlashGE var drawUnits:Vector.<RenderUnit> = new Vector.<RenderUnit>();		
		FlashGE var _contextProperties:Context3DProperties;
		FlashGE var preContext3D:Context3D;
		FlashGE var allUnits:Dictionary = new Dictionary;
		
		public function Renderer(name:String):void
		{
			this.name = name;
		}
		
		FlashGE function clearAllUnits():void
		{
			var drawUnitsLength:int = drawUnits.length;
			for (var unitIndex:int = 0; unitIndex < drawUnitsLength; unitIndex++) {
				var list:RenderUnit = drawUnits[unitIndex];
				if (list != null) {
					
					while (list != null) {
						var next:RenderUnit = list.next;
						list.next = null;
						list.release();
						list = next;
					}
				}
			}
			
			drawUnits.length = 0;
		}
		
		public var INIT_DIR:Vector.<int> = new <int>[
			Defines.PRIORITY_SKY, // 10
			Defines.PRIORITY_BACKGROUD, //13
			Defines.PRIORITY_BEFORE_OPAQUE, //16
			Defines.PRIORITY_OPAQUE, //20
			Defines.PRIORITY_DECALS,//30
			Defines.PRIORITY_TRANSPARENT_SORT,//40
			Defines.PRIORITY_NEXT_LAYER,//50
			Defines.PRIORITY_TRANSPARENT_SORT_TRUE,//60
			Defines.PRIORITY_EFFECT,//70
			Defines.PRIORITY_FONT,//75
			Defines.PRIORITY_AFTER_EFFECT,//80
			Defines.PRIORITY_SCENE//90
		];
		
		FlashGE function render(context3D:Context3D, renderGrids:Boolean = true):void
		{
			if (camera.closeCollectRenderUnits) {
				this.clearAllUnits();
				return;
			}
			
			if (this.preContext3D != context3D) {
				if (this.preContext3D == null) {
					this.preContext3D = context3D;	
				} else {
					this.clearAllUnits();
					this.preContext3D = context3D;
					return;
				}
			}
			
			updateContext3D(context3D);
			if (renderGrids) {
				camera.renderScene.renderGrids();
			}
			var drawUnitsLength:int = drawUnits.length;
			for (var index:int = 0; index < 12; index++) {
				var unitIndex:int = INIT_DIR[index];
				var list:RenderUnit = (unitIndex < drawUnitsLength ? drawUnits[unitIndex] : null);
				if (list != null) {
					switch (unitIndex) {
						case Defines.PRIORITY_SKY: // 10
							context3D.setDepthTest(false, Context3DCompareMode.ALWAYS);
							break;
						case Defines.PRIORITY_BACKGROUD: //13
							context3D.setDepthTest(true, Context3DCompareMode.LESS);
							break;
						case Defines.PRIORITY_BEFORE_OPAQUE: //16
							context3D.setDepthTest(false, Context3DCompareMode.ALWAYS);
							break;
						case Defines.PRIORITY_OPAQUE: //20
							if (list.next != null) list = sortBySortID(list);
							context3D.setDepthTest(true, Context3DCompareMode.LESS);
							break;
						case Defines.PRIORITY_DECALS://30
							context3D.setDepthTest(false, Context3DCompareMode.LESS_EQUAL);
							break;
						case Defines.PRIORITY_TRANSPARENT_SORT://40
							if (list.next != null) list = sortByAverageZ(list);
							context3D.setDepthTest(false, Context3DCompareMode.LESS);
							break;
						case Defines.PRIORITY_NEXT_LAYER://50
							context3D.setDepthTest(false, Context3DCompareMode.ALWAYS);
							break;
						case Defines.PRIORITY_TRANSPARENT_SORT_TRUE://60
							if (list.next != null) list = sortByAverageZ(list);
							context3D.setDepthTest(true, Context3DCompareMode.LESS_EQUAL);
							break;
						case Defines.PRIORITY_EFFECT://70
							if (list.next != null) list = sortByAverageZ(list);
							context3D.setDepthTest(false, Context3DCompareMode.LESS_EQUAL);
							break;
						case Defines.PRIORITY_FONT://75
							context3D.setDepthTest(false, Context3DCompareMode.LESS);
							break;
						case Defines.PRIORITY_AFTER_EFFECT://80
							if (list.next != null) list = sortByAverageZ(list);
							context3D.setDepthTest(true, Context3DCompareMode.LESS_EQUAL);
							break;
						case Defines.PRIORITY_SCENE://90
							context3D.setDepthTest(false, Context3DCompareMode.LESS);
							break;		
					}
					
					while (list != null) {
						var next:RenderUnit = list.next;
						try {
							renderUnits(list, context3D, camera);
							list.release();
						} catch (e:Error) {
							if (e.errorID == 3672) {
								Global.needDisposeContext3d = true;
							}
							
							CONFIG::DEBUG {

								if (e.errorID == 3672) {
									Global.throwError("Renderer::render" +e.message + e.getStackTrace() + list.object, e.errorID );
								} else {
									Global.log("Renderer::render"  + list.object + e.message +"," + e.errorID);
								}
							}
						}
						list.next = null;
						list = next;
					}
				}
			}
			
			freeContext3DProperties(context3D);
			drawUnits.length = 0;
			allUnits = new Dictionary;
		}
		
		private function printList(list:RenderUnit):void
		{
			//trace(Global.frameCount, "printList begin:")
			//while(list != null) {
			//	trace("\t", list.sortID, list.usedProgram.id,  list.textures[0].id, list.usedGeometry.id, "\t","\t", list.textures[0].getFileName());
			//	list = list.next;
			//}
			//trace(Global.frameCount, "printList end");
		}
		
		FlashGE function addRenderUnit(renderUnit:RenderUnit, renderPriority:int):void 
		{
			if (renderPriority >= drawUnits.length) {
				drawUnits.length = renderPriority + 1;
			}
			
			renderUnit.addRef();
			
			var list:RenderUnit = drawUnits[renderPriority];
			var pre:RenderUnit = list;
			var temp:RenderUnit;
			while(list != null) {
				if (list.id == renderUnit.id) {
					pre.next = list.next;
					temp = list;
					list = list.next;
					temp.release();
					temp.next = null;
				} else {
					pre = list;
					list = list.next;
				}
			}
			
			if (renderUnit.next != null) {
				CONFIG::DEBUG {
					Global.log(Global.frameCount,"Repeat Renderer::addRenderUnit " + renderUnit.object.name + String(renderUnit.object));
				}
					return;
			}
			if (allUnits[renderUnit.id] == true) {
				return;
			}
			allUnits[renderUnit.id] = renderUnit;
			renderUnit.next = drawUnits[renderPriority];
			drawUnits[renderPriority] = renderUnit;
		}
		
		protected function renderUnits(renderUnitIn:RenderUnit, context:Context3D, camera:Camera3D):void
		{
			var program3D:Program3D = renderUnitIn.program;
			var renderUnit:RenderUnit = renderUnitIn.ref;
			if (renderUnit == null) {
				renderUnit = renderUnitIn;
			}
			
			var object:Entity = renderUnitIn.object || renderUnit.object;
			
			if (object == null || object.isValid == false || program3D == null || renderUnit.indexBuffer == null) {
				return;
			}
			
			if (camera.context3DProperties.isOpenGL) {
				if (renderUnit.texturesLength == 1) {
					if (null == renderUnit.textures[0].getTexture(context)) {
						return;
					}
				}
			}
			
			if (_contextProperties.blendSource != renderUnit.srcBlend || 
				_contextProperties.blendDestination != renderUnit.dstBlend) {
				context.setBlendFactors(renderUnit.srcBlend, renderUnit.dstBlend);
				_contextProperties.blendSource = renderUnit.srcBlend;
				_contextProperties.blendDestination = renderUnit.dstBlend;
			}
			
			if (_contextProperties.culling != renderUnit.culling) {
				context.setCulling(renderUnit.culling);
				_contextProperties.culling = renderUnit.culling;
			}
			
			var bufferIndex:int;
			var bufferBit:int;
			var currentBuffers:int;
			var textureSampler:int;
			var textureBit:int;
			var currentTextures:int;
			for (var i:int = 0; i < renderUnit.vertexBuffersLength; i++) {
				bufferIndex = renderUnit.vertexBuffersIndexes[i];
				bufferBit = 1 << bufferIndex;
				currentBuffers |= bufferBit;
				context.setVertexBufferAt(bufferIndex, 
					renderUnit.vertexBuffers[i], 
					renderUnit.vertexBuffersOffsets[i], 
					renderUnit.vertexBuffersFormats[i]);
			}
			
			if (renderUnit.vertexConstantsRegistersCount > 0) {
				context.setProgramConstantsFromVector(Context3DProgramType.VERTEX, 0, 
					renderUnit.vertexConstants, renderUnit.vertexConstantsRegistersCount);
			}
			
			if (renderUnit.fragmentConstantsRegistersCount > 0) {
				context.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 0, 
					renderUnit.fragmentConstants, renderUnit.fragmentConstantsRegistersCount);
			}
			
			for (i = 0; i < renderUnit.texturesLength; i++) {
				textureSampler = renderUnit.texturesSamplers[i];
				textureBit = 1 << textureSampler;
				currentTextures |= textureBit;
				context.setTextureAt(textureSampler, renderUnit.textures[i].getTexture(context));
			}
			
			if (_contextProperties.program3D != program3D) {
				context.setProgram(program3D);
				_contextProperties.program3D = program3D;
			}
			
			var _usedBuffers:uint = _contextProperties.usedBuffers & ~currentBuffers;
			var _usedTextures:uint = _contextProperties.usedTextures & ~currentTextures;
			for (bufferIndex = 0; _usedBuffers > 0; bufferIndex++) {
				bufferBit = _usedBuffers & 1;
				_usedBuffers >>= 1;
				if (bufferBit) 
					context.setVertexBufferAt(bufferIndex, null);
			}
			
			for (textureSampler = 0; _usedTextures > 0; textureSampler++) {
				textureBit = _usedTextures & 1;
				_usedTextures >>= 1;
				if (textureBit) 
					context.setTextureAt(textureSampler, null);
			}
			
			CONFIG::DEBUG {
				if (context.enableErrorChecking != true) {
					context.enableErrorChecking = true;
				}
			}
			
			context.drawTriangles(renderUnit.indexBuffer, renderUnit.firstIndex, renderUnit.numTriangles);
			
			_contextProperties.usedBuffers = currentBuffers;
			_contextProperties.usedTextures = currentTextures;
			CONFIG::DEBUG {
				Global.drawsCount++;
			}
		}
		
		protected function updateContext3D(value:Context3D):void
		{
			_contextProperties = camera.context3DProperties;
		}
		
		FlashGE function freeContext3DProperties(context3D:Context3D):void
		{
			_contextProperties.culling = null;
			_contextProperties.blendSource = null;
			_contextProperties.blendDestination = null;
			_contextProperties.program3D = null;
			
			var usedBuffers:uint = _contextProperties.usedBuffers;
			var usedTextures:uint = _contextProperties.usedTextures;
			
			var bufferIndex:int;
			var bufferBit:int;
			var textureSampler:int;
			var textureBit:int;
			for (bufferIndex = 0; usedBuffers > 0; bufferIndex++) {
				bufferBit = usedBuffers & 1;
				usedBuffers >>= 1;
				if (bufferBit > 0) {
					context3D.setVertexBufferAt(bufferIndex, null);
				}
			}
			
			for (textureSampler = 0; usedTextures > 0; textureSampler++) {
				textureBit = usedTextures & 1;
				usedTextures >>= 1;
				if (textureBit > 0) {
					context3D.setTextureAt(textureSampler, null);
				}
			}
			
			_contextProperties.usedBuffers = 0;
			_contextProperties.usedTextures = 0;
		}
		
		FlashGE function sortBySortID(list:RenderUnit, direction:Boolean = true):RenderUnit
		{
			var left:RenderUnit = list;
			var right:RenderUnit = list.next;
			while (right != null && right.next != null) {
				list = list.next;
				right = right.next.next;
			}
			right = list.next;
			list.next = null;
			if (left.next != null) {
				left = sortBySortID(left, direction);
			}
			if (right.next != null) {
				right = sortBySortID(right, direction);
			}
			
			var leftL:Number = left.sortID;
			var rightL:Number= right.sortID;
			var flag:Boolean = direction ? (leftL > rightL) : (leftL < rightL);
			if (flag) {
				list = left;
				left = left.next;
			} else {
				list = right;
				right = right.next;
			}
			var last:RenderUnit = list;
			while (true) {
				if (left == null) {
					last.next = right;
					return list;
				} else if (right == null) {
					last.next = left;
					return list;
				}
				if (flag) {
					
					leftL = left.sortID;
					rightL = right.sortID;
					if (direction ? (leftL > rightL) : (leftL < rightL)) {
						last = left;
						left = left.next;
					} else {
						last.next = right;
						last = right;
						right = right.next;
						flag = false;
					}
					
				} else {
					leftL = left.sortID;
					rightL = right.sortID;
					if (direction ? (leftL < rightL) : (leftL > rightL)) {
						last = right;
						right = right.next;
					} else {
						last.next = left;
						last = left;
						left = left.next;
						flag = true;
					}
				}
			}
			return null;
		}

		FlashGE function sortByAverageZ(list:RenderUnit, direction:Boolean = true):RenderUnit 
		{
			var left:RenderUnit = list;
			var right:RenderUnit = list.next;
			while (right != null && right.next != null) {
				list = list.next;
				right = right.next.next;
			}
			right = list.next;
			list.next = null;
			if (left.next != null) {
				left = sortByAverageZ(left, direction);
			}
			if (right.next != null) {
				right = sortByAverageZ(right, direction);
			}
			
			var leftL:Number = (left.object == null ? 0 : left.object.localToCameraTransform.l);
			var rightL:Number= (right.object == null ? 0 : right.object.localToCameraTransform.l);
			var flag:Boolean = direction ? (leftL > rightL) : (leftL < rightL);
			if (flag) {
				list = left;
				left = left.next;
			} else {
				list = right;
				right = right.next;
			}
			var last:RenderUnit = list;
			while (true) {
				if (left == null) {
					last.next = right;
					return list;
				} else if (right == null) {
					last.next = left;
					return list;
				}
				if (flag) {
					
					leftL = (left.object == null ? 0 : left.object.localToCameraTransform.l);
					rightL = (right.object == null ? 0 : right.object.localToCameraTransform.l);
					if (direction ? (leftL > rightL) : (leftL < rightL)) {
						last = left;
						left = left.next;
					} else {
						last.next = right;
						last = right;
						right = right.next;
						flag = false;
					}
					
				} else {
					leftL = (left.object == null ? 0 : left.object.localToCameraTransform.l);
					rightL = (right.object == null ? 0 : right.object.localToCameraTransform.l);
					if (direction ? (leftL < rightL) : (leftL > rightL)) {
						last = right;
						right = right.next;
					} else {
						last.next = left;
						last = left;
						left = left.next;
						flag = true;
					}
				}
			}
			return null;
		}
	}
}
