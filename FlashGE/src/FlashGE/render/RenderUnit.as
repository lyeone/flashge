/*@author lye 123*/

package FlashGE.render
{
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Context3DTriangleFace;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.Program3D;
	import flash.display3D.VertexBuffer3D;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.objects.Entity;
	import FlashGE.resources.TextureFile;

	use namespace FlashGE;

	public class RenderUnit 
	{
		FlashGE var sortID:Number;
		FlashGE var next:RenderUnit;
		FlashGE var ref:RenderUnit;
		FlashGE var object:Entity;
		FlashGE var program:Program3D;
		FlashGE var indexBuffer:IndexBuffer3D;
		FlashGE var firstIndex:int;
		FlashGE var numTriangles:int;
		
		FlashGE var srcBlend:String = Context3DBlendFactor.ONE;
		FlashGE var dstBlend:String = Context3DBlendFactor.ZERO;
		FlashGE var culling:String = Context3DTriangleFace.FRONT;

		FlashGE var textures:Vector.<TextureFile> = new Vector.<TextureFile>();
		FlashGE var texturesSamplers:Vector.<int> = new Vector.<int>();
		FlashGE var texturesLength:int = 0;
		
		FlashGE var vertexBuffers:Vector.<VertexBuffer3D> = new Vector.<VertexBuffer3D>();
		FlashGE var vertexBuffersIndexes:Vector.<int> = new Vector.<int>();
		FlashGE var vertexBuffersOffsets:Vector.<int> = new Vector.<int>();
		FlashGE var vertexBuffersFormats:Vector.<String> = new Vector.<String>();
		FlashGE var vertexBuffersLength:int = 0;
		
		FlashGE var vertexConstants:Vector.<Number> = new Vector.<Number>();
		FlashGE var vertexConstantsRegistersCount:int = 0;
		FlashGE var fragmentConstants:Vector.<Number> = new Vector.<Number>();
		FlashGE var fragmentConstantsRegistersCount:int = 0;
		FlashGE var refCount:int = 0;
		FlashGE var id:int;
		static private var id_calc:int = 1;
		public function RenderUnit()
		{
			id = id_calc++;
		}
		
		public function toString():String
		{
			var ret:String;
			ret = "RenderUnit:ref=" + this.refCount + ",vert=" + vertexConstants.length + ",frag=" + fragmentConstants.length +",obj" + this.object;
			return ret;
		}
		
		public function reuse():void
		{
			this.clear()
			this.refCount = 1;
			this.object = null;
			this.program = null;
			this.indexBuffer = null;
			this.firstIndex = 0;
			this.numTriangles = 0;
			this.next = null;
			this.ref = null;
			this.vertexBuffersLength = 0;
			this.fragmentConstantsRegistersCount = 0;
			this.vertexConstantsRegistersCount = 0;
			this.texturesLength = 0;
		}
		
		public function addRef():void
		{
			this.refCount++;
		}
		
		public function release():void
		{
			this.refCount--;
			if (refCount <= 0) {
				if (this.ref != null) {
					this.ref.release();
					this.ref = null;
				}
				
				var i:int, n:int = textures.length;
				for (i = 0; i < n; i++) {
					if (textures[i] != null) {
						textures[i].release();
						textures[i] = null;
					}
				}
				texturesLength = 0;
				
				Pool.getInstance().pushRenderUnit(this);
			}
		}
		
		public function cloneTo(unit:RenderUnit):void
		{
			unit.object = this.object;
			unit.program = this.program;
			unit.indexBuffer = this.indexBuffer;
			unit.firstIndex = this.firstIndex;
			unit.numTriangles = this.numTriangles;
			
			unit.srcBlend = this.srcBlend;
			unit.dstBlend = this.dstBlend;
			unit.culling = this.culling;
			
			var i:int, n:int;
			unit.textures.length = this.textures.length;
			for (i = 0, n = this.textures.length; i < n; i++) {
				unit.textures[i] = this.textures[i];
				if (unit.textures[i] != null) {
					unit.textures[i].addRef();
				}
			}

			unit.texturesSamplers.length = this.texturesSamplers.length;
			for (i =0, n = this.texturesSamplers.length; i < n; i++) {
				unit.texturesSamplers[i] = this.texturesSamplers[i];
			}
			unit.texturesLength = this.texturesLength;
			
			unit.vertexBuffers.length = this.vertexBuffers.length;
			for (i =0; i < this.vertexBuffers.length; i++) {
				unit.vertexBuffers[i] = this.vertexBuffers[i];
			}
			
			unit.vertexBuffersIndexes.length = this.vertexBuffersIndexes.length;
			for (i =0; i < this.vertexBuffersIndexes.length; i++) {
				unit.vertexBuffersIndexes[i] = this.vertexBuffersIndexes[i];
			}
			
			unit.vertexBuffersOffsets.length = this.vertexBuffersOffsets.length;
			for (i =0; i < this.vertexBuffersOffsets.length; i++) {
				unit.vertexBuffersOffsets[i] = this.vertexBuffersOffsets[i];
			}
			
			unit.vertexBuffersFormats.length = this.vertexBuffersFormats.length;
			for (i =0; i < this.vertexBuffersFormats.length; i++) {
				unit.vertexBuffersFormats[i] = this.vertexBuffersFormats[i];
			}
			unit.vertexBuffersLength = this.vertexBuffersLength;
			
			unit.vertexConstants.length = this.vertexConstants.length;
			for (i =0; i < this.vertexConstants.length; i++) {
				unit.vertexConstants[i] = this.vertexConstants[i];
			}			
			unit.vertexConstantsRegistersCount = this.vertexConstantsRegistersCount;
			
			for (i =0; i < this.fragmentConstants.length; i++) {
				unit.fragmentConstants[i] = this.fragmentConstants[i];
			}
			unit.fragmentConstantsRegistersCount = this.fragmentConstantsRegistersCount;
		}
			
		public function clear():void
		{
			ref = null;
			object = null;
			program = null;
			indexBuffer = null;
			srcBlend = Context3DBlendFactor.ONE;
			dstBlend = Context3DBlendFactor.ZERO;
			culling = Context3DTriangleFace.FRONT;
			var i:int;
			var n:int = this.textures.length;
			for (i=0; i < n; i++) {
				if (textures[i] != null) {
					textures[i].release();
					textures[i] = null;
				}
			}
			texturesLength = 0;
			
			for (i=0; i < vertexBuffersLength; i++) { 
				vertexBuffers[i] = null;
			}
			vertexBuffersLength = 0;
			
			vertexConstantsRegistersCount = 0;
			fragmentConstantsRegistersCount = 0;
		}
		
		FlashGE function releaseAllTextures():void
		{
			for (var i:int = 0, n:int = this.textures.length; i < n; i++) {
				if (textures[i] != null) {
					textures[i].release();
					textures[i] = null;
				}
			}
			this.texturesLength = 0;
		}
		
		FlashGE function setTextureAt(sampler:int, texture:TextureFile):void
		{
			if (sampler >= 8) {
				CONFIG::DEBUG {
					Global.throwError("RenderUnit::setTextureAt,sampler="+sampler +",file="+texture.getFileName());
				}
				return;
			}
			
			texture.addRef();
			texturesSamplers[this.texturesLength] = sampler;
			var pre:TextureFile = (this.texturesLength < this.textures.length ? textures[this.texturesLength] : null);
			if (pre != null) {
				pre.release();
			}
			textures[this.texturesLength] = texture;
			this.texturesLength++;
		}
		
		FlashGE function setVertexBufferAt(index:int, buffer:VertexBuffer3D, bufferOffset:int, format:String):void
		{
			if (uint(index) > 8) Global.throwError("VertexBuffer index " + index + " is out of bounds.");
			if (buffer == null) Global.throwError("Buffer is null");
			vertexBuffersIndexes[vertexBuffersLength] = index;
			vertexBuffers[vertexBuffersLength] = buffer;
			vertexBuffersOffsets[vertexBuffersLength] = bufferOffset;
			vertexBuffersFormats[vertexBuffersLength] = format;
			vertexBuffersLength++;
		}
		
		FlashGE function setVertexConstantsFromVector(firstRegister:int, data:Vector.<Number>, numRegisters:int):void
		{
			if (uint(firstRegister) > (128 - numRegisters)) Global.throwError("Register index " + firstRegister + " is out of bounds.");
			var offset:int = firstRegister << 2;
			if (firstRegister + numRegisters > vertexConstantsRegistersCount) {
				vertexConstantsRegistersCount = firstRegister + numRegisters;
				vertexConstants.length = vertexConstantsRegistersCount << 2;
			}
			for (var i:int = 0, len:int = numRegisters << 2; i < len; i++) {
				vertexConstants[offset] = data[i];
				offset++;
			}
		}
		
		FlashGE function setVertexConstantsFromNumbers(firstRegister:int, x:Number, y:Number, z:Number, w:Number = 1):void
		{
			if (uint(firstRegister) > 127) Global.throwError("Register index " + firstRegister + " is out of bounds.");
			var offset:int = firstRegister << 2;
			if (firstRegister + 1 > vertexConstantsRegistersCount) {
				vertexConstantsRegistersCount = firstRegister + 1;
				vertexConstants.length = vertexConstantsRegistersCount << 2;
			}
			vertexConstants[offset] = x; offset++;
			vertexConstants[offset] = y; offset++;
			vertexConstants[offset] = z; offset++;
			vertexConstants[offset] = w;
		}
		
		FlashGE function setVertexConstantsFromTransform(firstRegister:int, transform:Transform3D):void 
		{
			if (uint(firstRegister) > 125) Global.throwError("Register index " + firstRegister + " is out of bounds.");
			var offset:int = firstRegister << 2;
			if (firstRegister + 3 > vertexConstantsRegistersCount) {
				vertexConstantsRegistersCount = firstRegister + 3;
				vertexConstants.length = vertexConstantsRegistersCount << 2;
			}
			vertexConstants[offset] = transform.a; offset++;
			vertexConstants[offset] = transform.b; offset++;
			vertexConstants[offset] = transform.c; offset++;
			vertexConstants[offset] = transform.d; offset++;
			
			vertexConstants[offset] = transform.e; offset++;
			vertexConstants[offset] = transform.f; offset++;
			vertexConstants[offset] = transform.g; offset++;
			vertexConstants[offset] = transform.h; offset++;
			
			vertexConstants[offset] = transform.i; offset++;
			vertexConstants[offset] = transform.j; offset++;
			vertexConstants[offset] = transform.k; offset++;
			vertexConstants[offset] = transform.l;
		}

		FlashGE function setProjectionConstants(camera:Camera3D, firstRegister:int, transform:Transform3D = null, forcePerspective:Boolean = false):void 
		{
			if (uint(firstRegister) > 124) Global.throwError("Register index is out of bounds.");
			var offset:int = firstRegister << 2;
			if (firstRegister + 4 > vertexConstantsRegistersCount) {
				vertexConstantsRegistersCount = firstRegister + 4;
				vertexConstants.length = vertexConstantsRegistersCount << 2;
			}
			
			var m0:Number = forcePerspective ? camera.m0P : camera.m0;
			var m5:Number = forcePerspective ? camera.m5P : camera.m5;
			var m10:Number = forcePerspective ? camera.m10P : camera.m10;
			var m14:Number = forcePerspective ? camera.m14P : camera.m14;
			if (transform != null) {
				vertexConstants[offset] = transform.a*m0; offset++;
				vertexConstants[offset] = transform.b*m0; offset++;
				vertexConstants[offset] = transform.c*m0; offset++;
				vertexConstants[offset] = transform.d*m0; offset++;
				vertexConstants[offset] = transform.e*m5; offset++;
				vertexConstants[offset] = transform.f*m5; offset++;
				vertexConstants[offset] = transform.g*m5; offset++;
				vertexConstants[offset] = transform.h*m5; offset++;
				vertexConstants[offset] = transform.i*m10; offset++;
				vertexConstants[offset] = transform.j*m10; offset++;
				vertexConstants[offset] = transform.k*m10; offset++;
				vertexConstants[offset] = transform.l*m10 + m14; offset++;
				if (!camera.orthographic || forcePerspective) {
					vertexConstants[offset] = transform.i; offset++;
					vertexConstants[offset] = transform.j; offset++;
					vertexConstants[offset] = transform.k; offset++;
					vertexConstants[offset] = transform.l;
				} else {
					vertexConstants[offset] = 0; offset++;
					vertexConstants[offset] = 0; offset++;
					vertexConstants[offset] = 0; offset++;
					vertexConstants[offset] = 1;
				}
			} else {
				vertexConstants[offset] = m0; offset++;
				vertexConstants[offset] = 0; offset++;
				vertexConstants[offset] = 0; offset++;
				vertexConstants[offset] = 0; offset++;
				
				vertexConstants[offset] = 0; offset++;
				vertexConstants[offset] = m5; offset++;
				vertexConstants[offset] = 0; offset++;
				vertexConstants[offset] = 0; offset++;
				
				vertexConstants[offset] = 0; offset++;
				vertexConstants[offset] = 0; offset++;
				vertexConstants[offset] = m10; offset++;
				vertexConstants[offset] = m14; offset++;
				
				vertexConstants[offset] = 0; offset++;
				vertexConstants[offset] = 0; offset++;
				if (!camera.orthographic || forcePerspective) {
					vertexConstants[offset] = 1; offset++;
					vertexConstants[offset] = 0;
				} else {
					vertexConstants[offset] = 0; offset++;
					vertexConstants[offset] = 1;
				}
			}
		}

		FlashGE function setFragmentConstantsFromVector(firstRegister:int, data:Vector.<Number>, numRegisters:int):void
		{
			if (uint(firstRegister) > (28 - numRegisters)) Global.throwError("Register index " + firstRegister + " is out of bounds.");
			var offset:int = firstRegister << 2;
			if (firstRegister + numRegisters > fragmentConstantsRegistersCount) {
				fragmentConstantsRegistersCount = firstRegister + numRegisters;
				fragmentConstants.length = fragmentConstantsRegistersCount << 2;
			}
			for (var i:int = 0, len:int = numRegisters << 2; i < len; i++) {
				fragmentConstants[offset] = data[i];
				offset++;
			}
		}
		
		FlashGE function setFragmentConstantsFromNumbers(firstRegister:int, x:Number, y:Number, z:Number, w:Number = 1):void
		{
			if (uint(firstRegister) > 27) Global.throwError("Register index " + firstRegister + " is out of bounds.");
			var offset:int = firstRegister << 2;
			if (firstRegister + 1 > fragmentConstantsRegistersCount) {
				fragmentConstantsRegistersCount = firstRegister + 1;
				fragmentConstants.length = fragmentConstantsRegistersCount << 2;
			}
			fragmentConstants[offset] = x; offset++;
			fragmentConstants[offset] = y; offset++;
			fragmentConstants[offset] = z; offset++;
			fragmentConstants[offset] = w;
		}
		
		FlashGE function setFragmentConstantsFromTransform(firstRegister:int, transform:Transform3D):void
		{
			if (uint(firstRegister) > 25) Global.throwError("Register index " + firstRegister + " is out of bounds.");
			var offset:int = firstRegister << 2;
			if (firstRegister + 3 > fragmentConstantsRegistersCount) {
				fragmentConstantsRegistersCount = firstRegister + 3;
			}
			fragmentConstants[offset] = transform.a; offset++;
			fragmentConstants[offset] = transform.b; offset++;
			fragmentConstants[offset] = transform.c; offset++;
			fragmentConstants[offset] = transform.d; offset++;
			fragmentConstants[offset] = transform.e; offset++;
			fragmentConstants[offset] = transform.f; offset++;
			fragmentConstants[offset] = transform.g; offset++;
			fragmentConstants[offset] = transform.h; offset++;
			fragmentConstants[offset] = transform.i; offset++;
			fragmentConstants[offset] = transform.j; offset++;
			fragmentConstants[offset] = transform.k; offset++;
			fragmentConstants[offset] = transform.l;
		}

	}
}
