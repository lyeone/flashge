/*@author lye 123*/
package FlashGE.render 
{
	import flash.display3D.Context3D;
	import flash.events.MouseEvent;
	import flash.geom.Vector3D;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Global;
	import FlashGE.common.Transform3D;
	
	use namespace FlashGE;
	
	public class ViewPort
	{
		static private const localCoords:Vector3D = new Vector3D();
		
		FlashGE var backgroundColor:uint;
		FlashGE var backgroundAlpha:Number;
		FlashGE var antiAlias:int;
		FlashGE var viewWidth:int;
		FlashGE var viewHeight:int;
		FlashGE var usedContext3D:Context3D;
		
		public function ViewPort(width:int, height:int, backgroundColor:uint = 0, backgroundAlpha:Number = 1, antiAlias:int = 0)
		{
			if (width < 50) width = 50;
			if (height < 50) height = 50;
			viewWidth = width;
			viewHeight = height;
			this.backgroundColor = backgroundColor;
			this.backgroundAlpha = backgroundAlpha;
			this.antiAlias = antiAlias;
		}
		
		public function setAntiAlias(v:int):void
		{
			antiAlias = v;
		}
		
		FlashGE function configureContext3D(context3D:Context3D, camera:Camera3D):Boolean 
		{
			var context3DProperties:Context3DProperties = camera.context3DProperties;
			if (viewWidth != context3DProperties.backBufferWidth || 
				viewHeight != context3DProperties.backBufferHeight ||
				antiAlias != context3DProperties.backBufferAntiAlias ||
				usedContext3D != context3D ) {
				usedContext3D = context3D;
				context3DProperties.backBufferWidth = viewWidth;
				context3DProperties.backBufferHeight = viewHeight;
				context3DProperties.backBufferAntiAlias = antiAlias;
				var driverInfo:String = context3D.driverInfo.toLowerCase();
				if (driverInfo.indexOf("opengl") >= 0 || driverInfo.indexOf("software") >= 0 ) {
					context3DProperties.isOpenGL = true;					
				} else {
					context3DProperties.isOpenGL = false;
				}
				context3D.configureBackBuffer(viewWidth, viewHeight, antiAlias);
				CONFIG::DEBUG {
					Global.log("Context3D driver:"+driverInfo);
				}
				return true;
			}
			return false;
		}
		
		private function calculateLocalCoords(camera:Camera3D, transform:Transform3D, z:Number, mouseEvent:MouseEvent):void
		{
			var x:Number;
			var y:Number;
			if (!camera.orthographic) {
				x = z*(mouseEvent.localX - viewWidth*0.5)/camera.focalLength;
				y = z*(mouseEvent.localY - viewHeight*0.5)/camera.focalLength;
			} else {
				x = mouseEvent.localX - viewWidth*0.5;
				y = mouseEvent.localY - viewHeight*0.5;
			}
			localCoords.x = transform.a*x + transform.b*y + transform.c*z + transform.d;
			localCoords.y = transform.e*x + transform.f*y + transform.g*z + transform.h;
			localCoords.z = transform.i*x + transform.j*y + transform.k*z + transform.l;
		}
		
		public function getWidth():Number {
			return viewWidth;
		}
		
		public function setWidth(value:Number):void {
			if (value < 50) value = 50;
			viewWidth = value;
		}
		
		public function getHeight():Number {
			return viewHeight;
		}
		
		public function setHeight(value:Number):void {
			if (value < 50) value = 50;
			viewHeight = value;
		}
	}
}