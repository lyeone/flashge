/*@author lye 123*/

package FlashGE.render 
{
	import flash.display3D.Program3D;

	public class Context3DProperties 
	{
		public var isConstrained:Boolean = false;
		public var backBufferWidth:int = -1;
		public var backBufferHeight:int = -1;
		public var backBufferAntiAlias:int = -1;
		public var isOpenGL:Boolean = false;
		public var usedBuffers:uint = 0;
		public var usedTextures:uint = 0;

		public var program3D:Program3D;
		public var culling:String;
		public var blendSource:String;
		public var blendDestination:String;
	}
}
