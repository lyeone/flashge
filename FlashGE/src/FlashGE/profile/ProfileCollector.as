package FlashGE.profile
{
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	
	public class ProfileCollector
	{
		private var infos:Dictionary = new Dictionary;
		
		public function clear():void
		{
			infos = new Dictionary;
		}
		public function begin(key:String):void
		{
			return;
			var item:ProfileItem = infos[key];
			if (item == null) {
				infos[key] = item = new ProfileItem;
				item.textField.mouseEnabled = false;
				item.textField.mouseWheelEnabled = false;
				item.textField.autoSize = "left";
				item.textField.textColor = 0xffffff;
				item.textField.text = key;
			}
			
			item.keyName = key;
			item.times++;
			item.startTimer = getTimer();
			//item.stackTrace = (new Error).getStackTrace();
		}
		
		public function getItem(key:String):ProfileItem
		{
			return infos[key];
		}
		
		public function getItems():Dictionary
		{
			return infos;
		}
		
		public function end(key:String, useArray:Boolean = false):void
		{
			return;
			var item:ProfileItem = infos[key];
			if (item == null) {
				return;
			}
			
			item.endTimer = getTimer();
			item.deltaTimer = (item.endTimer - item.startTimer)
			item.totalTimer += item.deltaTimer;
			if (item.deltaTimer < item.minTimer) {
				item.minTimer = item.deltaTimer;
			}
			
			if (item.deltaTimer > item.maxTimer) {
				item.maxTimer = item.deltaTimer;
			}	
			if (useArray) {
				item.deltaTimerList.push(item.deltaTimer);
				if (item.deltaTimerList.length >= 200) {
					item.deltaTimerList.splice(0, 1);
				}
			}
		}
		
		public function toString(filter:String = null, clearMaxTimer:Boolean = true):String
		{
			var vecList:Vector.<ProfileItem> = new Vector.<ProfileItem>;
			for each(var i:ProfileItem in this.infos) {
				if (filter == null || i.keyName.search(filter) >= 0) {
					vecList.push(i);
				}
			}
			function sortFunc(a:ProfileItem, b:ProfileItem):int
			{
				return a.maxTimer > b.maxTimer ? -1 : 1;
			}
			vecList.sort(sortFunc);
			var ret:String = "\nProfileCollector(begin):\n";
			for each(var item:ProfileItem in vecList) {
				ret += ("\t" + "\ttotalTimer:" + item.totalTimer + "\tdeltaTimer:" + item.deltaTimer + "\tmaxTimer:" + item.maxTimer + "\ttimes:" + item.times + "\t" + item.keyName + "\n");
				if (clearMaxTimer) {
					item.maxTimer = -Number.MAX_VALUE;
				}
			}
			return ret + "ProfileCollector(end)";
		}
	}
}
