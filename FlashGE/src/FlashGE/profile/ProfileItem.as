package FlashGE.profile
{
	import flash.text.TextField;

	public class ProfileItem
	{
		public var stackTrace:String;

		public var keyName:String;
		public var startTimer:Number;
		public var endTimer:Number;
		
		public var deltaTimer:Number;
		public var totalTimer:Number = 0;
		public var maxTimer:Number = -Number.MAX_VALUE;
		public var minTimer:Number = Number.MAX_VALUE;
		public var times:int = 0;
		public var deltaTimerList:Array = new Array;
		public var textField:TextField = new TextField;
	}
}
