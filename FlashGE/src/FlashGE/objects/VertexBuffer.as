// 2014-3-10 lye
package FlashGE.objects {

	import flash.display3D.VertexBuffer3D;
	import flash.utils.ByteArray;

	public class VertexBuffer {
		public var vertBuffer3D:VertexBuffer3D;
		public var attributes:Array;
		public var data:ByteArray;
	}
}
