// 2014-3-10 lye

package FlashGE.objects
{
	import flash.display3D.textures.TextureBase;
	
	import FlashGE.FlashGE;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;

	public class Grid 
	{
		public var fileName:String;
		FlashGE var x:Number;
		FlashGE var y:Number;
		public var uScale:Number = 1;
		public var vScale:Number = 1;
		public var backOffsetU:Number;
		public var backOffsetV:Number;
		public var backScaleU:Number;
		public var backScaleV:Number;
		FlashGE var uvScale:Vector.<Number> = new Vector.<Number>(4);
		FlashGE var pos:Vector.<Number> = new Vector.<Number>(4);
		FlashGE var curTexture:TextureFile;
		public var nextSimple:Grid;
		public var textureBase:TextureBase;
		
	}
}