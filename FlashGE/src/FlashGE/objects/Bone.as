// 2014-3-10 lye

package FlashGE.objects
{
	import FlashGE.FlashGE;
	import FlashGE.animation.KeyFrame;
	import FlashGE.common.Transform3D;

	use namespace FlashGE;
	
	public class Bone extends Entity
	{
		FlashGE var bindPoseTransform:Transform3D = new Transform3D();
		FlashGE var boneID:int = -1;
		FlashGE var parentBoneID:int = -1;
		FlashGE var curKeyFrame:KeyFrame;
		FlashGE var parentBone:Bone;
		FlashGE var calcFrame:int = -1;
		
		public function toString():String
		{
			return "Bone(" + this.name + ")";
		}
	}
}