package FlashGE.objects
{
	import FlashGE.FlashGE;

	use namespace FlashGE;

	public class Linkable extends Entity
	{
		FlashGE var linkedNode:Entity;
		FlashGE var linkedHolder:Entity;
		
		override public function getLinkedNode():Entity
		{
			return linkedNode;
		}
		
		override public function getLinkedHolder():Entity
		{
			return linkedHolder;
		}
		
		override public function setLinkedNode(node:Entity, holder:Entity):void
		{
			if (node == this.linkedHolder && this.linkedHolder == holder) return;
			
			if (node != null) {
				node.refCountLinked++;
			} 

			if(linkedNode != null) {
				linkedNode.refCountLinked--;
			}
			
			if (linkedHolder != null) {
				linkedHolder.onUnlinkedObject(this, this.linkedNode);
			}
			linkedNode = node;
			linkedHolder = holder;
			if (linkedHolder != null) {
				linkedHolder.onLinkedObject(this, this.linkedNode);
			}
		}
	}
}