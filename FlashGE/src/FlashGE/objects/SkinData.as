/*@author lye 123*/

package FlashGE.objects
{
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import flash.utils.Endian;
	
	import FlashGE.FlashGE;
	import FlashGE.common.AxisAlignedBox;
	import FlashGE.common.Colour;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.materials.compiler.Procedure;
	import FlashGE.materials.compiler.VariableType;
	import FlashGE.resources.Geometry;
	
	use namespace FlashGE;

	public class SkinData
	{
		static public var transformProcedures:Dictionary = new Dictionary;
		static public var transformReciveShadowProcedures:Dictionary = new Dictionary;
		static public var transformShadowProcedures:Dictionary = new Dictionary;
		static public var transformPositionProcedures:Dictionary = new Dictionary;

		FlashGE var renderedBonesCount:int = 0;
		FlashGE var surfaceBonesList:Vector.<Vector.<Bone>>;
		FlashGE var surfaceBoneNamesList:Vector.<Vector.<String>>;
		FlashGE var surfacePositionProcedures:Vector.<Procedure>;
		FlashGE var surfaceTransformProcedures:Vector.<Procedure>;
		FlashGE var surfaceTransformReciveShadowProcedures:Vector.<Procedure>;
		FlashGE var surfaceShadowTransformProcedures:Vector.<Procedure>;
		FlashGE var maxInfluences:int = 0;
		FlashGE var meshName:String;
		FlashGE var geometry:Geometry;
		FlashGE var surfaces:Vector.<Surface>;
		FlashGE var surfacesLength:int = 0;
		FlashGE var diffuseTexture:String;
		FlashGE var boundBox:AxisAlignedBox;
		FlashGE var boneNameList:Vector.<String>;
		FlashGE var openAlphaTest:Boolean = false;
		
		FlashGE var programCachedID:String;
		//FlashGE var glossiness:Number = 0;
		
		public function dispose():void
		{
		}
		
		public function toString():String
		{
			var ret:String = "SkinData(meshName=" + this.meshName;
			ret += (",renderedBonesCount="+renderedBonesCount);
			ret += (",surfacesLength="+surfacesLength);
			ret += (",diffuseTexture="+diffuseTexture);
			ret += ")";
			return ret;
				
		}
		
		public function SkinData(maxInfluences:int) 
		{
			this.maxInfluences = maxInfluences;

			surfaceBonesList = new Vector.<Vector.<Bone>>();
			surfaceBoneNamesList = new Vector.<Vector.<String>>();
			surfaceTransformProcedures = new Vector.<Procedure>();
			surfacePositionProcedures = new Vector.<Procedure>();
			surfaceShadowTransformProcedures = new Vector.<Procedure>();
		}

		private function divideSurface(limit:uint, iterations:uint, surface:Surface, 
									   jointsOffsets:Vector.<uint>, jointBufferVertexSize:uint, inVertices:ByteArray, 
									   outVertices:ByteArray, outIndices:Vector.<uint>,
									   outSurfaces:Vector.<Surface>, outJointsMaps:Vector.<Dictionary>):uint 
		{
			var indexBegin:uint = surface.indexBegin;
			var indexCount:uint = surface.trianglesNum*3;
			var i:int, j:int, count:int, jointsLength:int, index:uint;
			var indices:Vector.<uint> = geometry.indicesBuffer;
			var groups:Dictionary = new Dictionary();
			var group:Dictionary;

			var key:*, key2:*;
			var jointIndex:uint;
			var weight:Number;
			for (i = indexBegin,count = indexBegin + indexCount; i < count; i += 3) {
				group = groups[i] = new Dictionary();
				var jointsGroupLength:uint = 0;
				for (var n:int = 0; n < 3; n++) {
					index = indices[int(i + n)];
					for (j = 0,jointsLength = jointsOffsets.length; j < jointsLength; j++) {
						inVertices.position = jointBufferVertexSize*index + jointsOffsets[j];
						jointIndex = uint(inVertices.readFloat());
						weight = inVertices.readFloat();
						if (weight > 0) {
							group[jointIndex] = true;
						}
					}
				}
				for (key in group) {
					jointsGroupLength++;
				}
				if (jointsGroupLength > limit) {
					Global.throwError("SkinData::divideSurface Unable to divide Skin.");
				}
			}
			var localNumJoints:uint;

			var facesGroups:Dictionary = optimizeGroups(groups, limit, iterations);
			var newIndex:uint = 0;
			var newIndexBegin:uint;
			for (key in facesGroups) {
				var faces:Dictionary = facesGroups[key];
				localNumJoints = 0;
				group = groups[key];
				for (key2 in group) {
					if (group[key2] is Boolean) {
						group[key2] = 5+ 2*localNumJoints++;
					}
				}
				var locatedIndices:Dictionary = new Dictionary();
				for (key2 in faces) {
					for (i = 0; i < 3; i++) {
						index = indices[int(key2 + i)];
						if (locatedIndices[index] != null) {
							outIndices.push(locatedIndices[index]);
							continue;
						}
						locatedIndices[index] = newIndex;
						outIndices.push(newIndex++);
						outVertices.writeBytes(inVertices, index*jointBufferVertexSize, jointBufferVertexSize);
						outVertices.position -= jointBufferVertexSize;
						var origin:uint = outVertices.position;
						var sumWeight:Number = 0;

						for (j = 0; j < jointsLength; j++) {
							outVertices.position = origin + jointsOffsets[j];
							jointIndex = uint(outVertices.readFloat());
							weight = outVertices.readFloat();
							outVertices.position -= 8;
							if (weight > 0) {
								outVertices.writeFloat(group[jointIndex]);
								outVertices.writeFloat(weight);
								sumWeight += weight;
							}
						}

						if (sumWeight != 1) {
							for (j = 0; j < jointsLength; j++) {
								outVertices.position = origin + jointsOffsets[j] + 4;
								weight = outVertices.readFloat();
								if (weight > 0) {
									outVertices.position -= 4;
									outVertices.writeFloat(weight/sumWeight);
								}
							}
						}
						outVertices.position = origin + jointBufferVertexSize;
					}
				}
				var resSurface:Surface = new Surface();
				resSurface.indexBegin = newIndexBegin;
				resSurface.trianglesNum = (outIndices.length - newIndexBegin)/3;
				outSurfaces.push(resSurface);
				outJointsMaps.push(group);
				newIndexBegin = outIndices.length;
			}
			return newIndex;
		}

		private function optimizeGroups(groups:Dictionary, limit:uint, iterations:uint = 1):Dictionary
		{
			var key:*;
			var inKey:*;
			var facesGroups:Dictionary = new Dictionary();
			for (var i:int = 1; i < iterations + 1; i++) {
				var minLike:Number = 1 - i/iterations;
				for (key in groups) {
					var group1:Dictionary = groups[key];
					for (inKey in groups) {
						if (key == inKey) continue;
						var group2:Dictionary = groups[inKey];
						var like:Number = calculateLikeFactor(group1, group2, limit);
						if (like >= minLike) {
							delete groups[inKey];
							for (var copyKey:* in group2) {
								group1[copyKey] = true;
							}
							var indices:Dictionary = facesGroups[key];
							if (indices == null) {
								indices = facesGroups[key] = new Dictionary();
								indices[key] = true;
							}

							var indices2:Dictionary = facesGroups[inKey];
							if (indices2 != null) {
								delete facesGroups[inKey];
								for (copyKey in indices2) {
									indices[copyKey] = true;
								}
							} else {
								indices[inKey] = true;
							}
						}
					}
				}
			}
			return facesGroups;
		}

		private function calculateLikeFactor(group1:Dictionary, group2:Dictionary, limit:uint):Number
		{
			var key:*;
			var unionCount:uint;
			var intersectCount:uint;
			var group1Count:uint;
			var group2Count:uint;
			for (key in group1) {
				unionCount++;
				if (group2[key] != null) {
					intersectCount++;
				}
				group1Count++;
			}
			for (key in group2) {
				if (group1[key] == null) {
					unionCount++
				}
				group2Count++;
			}
			if (unionCount > limit) return -1;
			return intersectCount/unionCount;
		}

		public function divide(limit:uint, iterations:uint, reciveShadow:Boolean):void
		{
			if (surfaceBoneNamesList == null || maxInfluences <= 0 ) return;
			var jointsBuffer:int = geometry.findVertexStreamByAttribute(Defines.JOINTS[0]);
			var jointsOffsets:Vector.<uint> = new Vector.<uint>();
			var jointOffset:int = 0;
			if (jointsBuffer >= 0) {
				jointOffset = geometry.getAttributeOffset(Defines.JOINTS[0])*4;
				jointsOffsets.push(jointOffset);
				jointsOffsets.push(jointOffset + 8);
			} else {
				Global.throwError("Cannot divide skin, joints[0] must be binded");
			}
			var jTest:int = geometry.findVertexStreamByAttribute(Defines.JOINTS[1]);
			if (jTest >= 0) {
				jointOffset = geometry.getAttributeOffset(Defines.JOINTS[1])*4;
				jointsOffsets.push(jointOffset);
				if (jointsBuffer != jTest) {
					Global.throwError("Cannot divide skin, all joinst must be in the same buffer");
				}
			}

			var outSurfaces:Vector.<Surface> = new Vector.<Surface>();
			var totalVertices:ByteArray = new ByteArray();
			totalVertices.endian = Endian.LITTLE_ENDIAN;
			var totalIndices:Vector.<uint> = new Vector.<uint>();
			var totalIndicesLength:uint = 0;
			var lastMaxIndex:uint = 0;
			var key:*;
			var lastSurfaceIndex:uint = 0;
			var lastIndicesCount:uint = 0;
			surfaceBoneNamesList.length = 0;
			var jointsBufferNumMappings:int = geometry.vertexStreamsList[jointsBuffer].attributes.length;
			var jointsBufferData:ByteArray = geometry.vertexStreamsList[jointsBuffer].data;
			for (var i:int = 0; i < surfacesLength; i++) {
				var outIndices:Vector.<uint> = new Vector.<uint>();
				var outVertices:ByteArray = new ByteArray();
				var outJointsMaps:Vector.<Dictionary> = new Vector.<Dictionary>();
				outVertices.endian = Endian.LITTLE_ENDIAN;
				var maxIndex:uint = divideSurface(limit, iterations, surfaces[i], jointsOffsets,
					jointsBufferNumMappings*4, jointsBufferData, outVertices, outIndices, outSurfaces, outJointsMaps);
				for (var j:int = 0, count:int = outIndices.length; j < count; j++) {
					totalIndices[totalIndicesLength++] = lastMaxIndex + outIndices[j];
				}

				for (j = 0,count = outJointsMaps.length; j < count; j++) {
					var maxJoints:uint = 0;
					var vec:Vector.<String> = surfaceBoneNamesList[j + lastSurfaceIndex] = new Vector.<String>();
					var joints:Dictionary = outJointsMaps[j];
					for (key in joints) {
						var index:uint = uint((joints[key] - 5)/2);
						if (vec.length < index) vec.length = index + 1;
						vec[index] = boneNameList[uint((key - 5)/2)];
						maxJoints++;
					}
				}
				for (j = lastSurfaceIndex; j < outSurfaces.length; j++) {
					outSurfaces[j].indexBegin += lastIndicesCount;

				}
				lastSurfaceIndex += outJointsMaps.length;
				lastIndicesCount += outIndices.length;
				totalVertices.writeBytes(outVertices, 0, outVertices.length);
				lastMaxIndex += maxIndex;
			}
			surfaces = outSurfaces;
			surfacesLength = outSurfaces.length;
			surfaceTransformProcedures.length = surfacesLength;
			surfaceShadowTransformProcedures.length = surfacesLength;
			calculateSurfacesProcedures(reciveShadow);
			var newGeometry:Geometry = Geometry.create();
			newGeometry.indicesBuffer = totalIndices;
			
			for (i = 0; i < geometry.vertexStreamsList.length; i++) {
				var attributes:Array = geometry.vertexStreamsList[i].attributes;
				newGeometry.addVertexStream(attributes);
				if (i == jointsBuffer) {
					newGeometry.vertexStreamsList[i].data = totalVertices;
				} else {
					var data:ByteArray = new ByteArray();
					data.endian = Endian.LITTLE_ENDIAN;
					data.writeBytes(geometry.vertexStreamsList[i].data);
					newGeometry.vertexStreamsList[i].data = data;
				}
			}
			newGeometry.verticesCount = totalVertices.length/(newGeometry.vertexStreamsList[0].attributes.length << 2);
			geometry.release();
			geometry = newGeometry;
		}
		
		FlashGE function calculateSurfacesProcedures(recieveShadow:Boolean):void 
		{
			surfaceBonesList.length = surfacesLength;
			for (var i:int = 0; i < surfacesLength; i++) {
				var numJoints:int = surfaceBoneNamesList[i].length;
				surfaceBonesList[i] = new Vector.<Bone>;
				surfaceBonesList[i].length = numJoints;
				this.surfaces[i].materialKeyBase = String(numJoints);
				surfacePositionProcedures[i] = calculatePositionProcedure(maxInfluences, numJoints);	
				surfaceTransformProcedures[i] = calculateTransformProcedure(maxInfluences, numJoints);				
				surfaceShadowTransformProcedures[i] = calculateShadowTransformProcedure(maxInfluences, numJoints);
				if (recieveShadow) {
					if (surfaceTransformReciveShadowProcedures == null) {
						surfaceTransformReciveShadowProcedures = new Vector.<Procedure>;
					}
					surfaceTransformReciveShadowProcedures[i] = calculateTransformReciveShadowProcedures(maxInfluences, numJoints);
				}
			}
		}
		
		FlashGE function calculateSurfacesProceduresReciveShadow():void
		{
			for (var i:int = 0; i < surfacesLength; i++) {
				var numJoints:int = surfaceBoneNamesList[i].length;
				
				if (surfaceTransformReciveShadowProcedures == null) {
					surfaceTransformReciveShadowProcedures = new Vector.<Procedure>;
				}
				surfaceTransformReciveShadowProcedures[i] = calculateTransformReciveShadowProcedures(maxInfluences, numJoints);
			}
		}
		
		public function calculatePreSurfacesProcedures(numJoints:int):void
		{
			calculatePositionProcedure(maxInfluences, numJoints);
			calculateTransformProcedure(maxInfluences, numJoints);
			calculateShadowTransformProcedure(maxInfluences, numJoints);
		}
		
		private function calculatePositionProcedure(maxInfluences:int, numJoints:int):Procedure 
		{
			var res:Procedure = transformPositionProcedures[maxInfluences | (numJoints << 16)];
			if (res != null) {
				return res;
			}
			
			res = transformPositionProcedures[maxInfluences | (numJoints << 16)] = new Procedure(null, "SkinPositionProcedure");
			var array:Array = [];
			var j:int = 0;
			//array[j++] = "mov t1.w, i0.w";
			array[j++] = "sge t1.w, i0.w, i0.w"; // x  1
			for (var i:int = 0; i < maxInfluences; i++) {
				var joint:int = int(i/2);
				
				if (i%2 == 0) {
					j = decodeMatrix(array, j, joint, "x");
					if (i == 0) {
						array[j++] = "m34 t0.xyz, i0, t2";
						array[j++] = "mul o0, t0.xyz, a" + joint + ".y";
					} else {
						array[j++] = "m34 t0.xyz, i0, t2";
						array[j++] = "mul t0.xyz, t0.xyz, a" + joint + ".y";
						array[j++] = "add o0, o0, t0.xyz";
					}
				} else {
					j = decodeMatrix(array, j, joint, "z");
					array[j++] = "m34 t0.xyz, i0, t2";
					array[j++] = "mul t0.xyz, t0.xyz, a" + joint + ".w";
					array[j++] = "add o0, o0, t0.xyz";
				}
			}
			array[j++] = "mov o0.w, i0.w";
			res.compileFromArray(array);
			res.assignConstantsArray(numJoints*2 + 5);
			for (i = 0; i < maxInfluences; i += 2) {
				res.assignVariableName(VariableType.ATTRIBUTE, int(i/2), "joint" + i);
			}
			return res;
		}
		
		public function makeRotation(axisX:Number, axisY:Number, axisZ:Number, angle:Number):void
		{
			/*
			var sin_val:Number, cos_val:Number, temp:Number;
			var cx:Number, cy:Number, cz:Number;
			var sx:Number, sy:Number, sz:Number;
			
			sin_val = Math.sin(angle);
			cos_val = Math.cos(angle);
			temp = 1 - cos_val;
			
			cx = temp * axisX, 
			cy = temp * axisY, 
			cz = temp * axisZ;
			sx = sin_val * axisX, 
			sy = sin_val * axisY, 
			sz = sin_val * axisZ;
			
			a = cx * axisX + cos_val;
			b = cx * axisY + sz;
			c = cx * axisZ - sy;
			
			e = cy * axisX - sz;
			f = cy * axisY + cos_val;
			g = cy * axisZ + sx;
			
			i = cz * axisX + sy;
			j = cz * axisY - sx;
			k = cz * axisZ + cos_val;
			
			d = h = l = 0;
			*/
		}
		// t0,t2,t5,t6
		// c0:axisX, axisY, axisZ, cos_val
		// c1:x, y, z, sin_val
		// t1.x = 1
		private function decodeMatrix(array:Array, j:int, joint:int, index:String):int
		{
			var c0:String= "c[a" + joint + "."+index+"]";
			var c1Index:String = "add t0.w, t1.w, a"+joint+"."+index;
			var c1:String= "c[t0.w]";
			//array[j++] = "mov t6, " + c0;     // t6(axisX, axisY, axisZ, cos_val)
			array[j++] = "sub t0.w, t1.w, "+c0+".w"; // t0(temp = 1 - cos_val)
			array[j++] = "mul t0.xyz, t0.www, "+c0+".xyz"; // temp*(axisX, axisY, axisZ) = t0(cx, cy, cz)
			
			array[j++] = c1Index;
			array[j++] = "mov t0.w," + c1 + ".w";
			array[j++] = "mul t1.xyz,"+c0 +".xyz, t0.www";// sin_val*(axisX, axisY, axisZ) = t1(sx, sy, sz)
			
			array[j++] = "mul t2.xyz, t0.xxx, " + c0 + ".xyz";
			array[j++] = "add t2.x, t2.x, " + c0 + ".w";
			array[j++] = "add t2.y, t2.y, t1.z";
			array[j++] = "sub t2.z, t2.z, t1.y";
			array[j++] = c1Index;
			array[j++] = "mov t2.w, "+ c1 + ".x";

			array[j++] = "mul t3.xyz, t0.yyy, "+c0+".xyz";
			array[j++] = "sub t3.x, t3.x, t1.z";
			array[j++] = "add t3.y, t3.y, "+c0+".w";
			array[j++] = "add t3.z, t3.z, t1.x";
			array[j++] = c1Index;
			array[j++] = "mov t3.w, "+ c1 + ".y";
			
			array[j++] = "mul t4.xyz, t0.zzz, "+c0+".xyz";
			array[j++] = "add t4.x, t4.x, t1.y";
			array[j++] = "sub t4.y, t4.y, t1.x";
			array[j++] = "add t4.z, t4.z, "+c0+".w";
			array[j++] = c1Index;
			array[j++] = "mov t4.w, "+ c1 + ".z";
			
			return j;
		}
		
		private function decodeNormal(array:Array, j:int):int
		{
			/*
			half3 decode (half4 enc, float3 view)
			{
				half4 nn = enc*half4(2,2,0,0) + half4(-1,-1,1,-1);
				half l = dot(nn.xyz,-nn.xyw);
				nn.z = l;
				nn.xy *= sqrt(l);
				return nn.xyz * 2 + half3(0,0,-1);
			}
			*/
			// t1.w = 1
			array[j++] = "add t0.xy, i1.xy, i1.xy";
			array[j++] = "sub t0.xy, t0.xy, t1.ww";
			array[j++] = "mov t0.z, t1.w";
			array[j++] = "neg t0.w, t1.w";
			array[j++] = "neg t1.xyz, t0.xyw";
			array[j++] = "dp3 t1.z, t0.xyz, t1.xyz";
			array[j++] = "sqt t1.x, t1.z";
			array[j++] = "mul t1.xy, t0.xy, t1.xx";
			array[j++] = "add t1.xyz, t1.xyz, t1.xyz";
			array[j++] = "sub t1.z, t1.z, t1.w";
			
			
			return j;
		}
		
		private function calculateTransformReciveShadowProcedures(maxInfluences:int, numJoints:int):Procedure
		{
			var res:Procedure = transformReciveShadowProcedures[maxInfluences | (numJoints << 16)];
			if (res != null) return res;
			res = transformReciveShadowProcedures[maxInfluences | (numJoints << 16)] = new Procedure(null, "SkinTransformReciveShadowProcedure");
			
			var array:Array = [];
			var j:int = 0;

			array[j++] = "sge t1.w, i0.w, i0.w";
			
			for (var i:int = 0; i < maxInfluences; i ++) {
				var joint:int = int(i/2);
				if (i%2 == 0) {
					j = decodeMatrix(array, j, joint, "x");
					if (i == 0) {
						array[j++] = "m34 t0.xyz, i0, t2";
						array[j++] = "mul o0, t0.xyz, a" + joint + ".y";
						
						j = decodeNormal(array, j);
						array[j++] = "m33 t0.xyz, t1.xyz, t2";
						array[j++] = "mul o1, t0.xyz, a" + joint + ".y";
					} else {
						array[j++] = "m34 t0.xyz, i0, t2";
						array[j++] = "mul t0.xyz, t0.xyz, a" + joint + ".y";
						array[j++] = "add o0, o0, t0.xyz";
						
						j = decodeNormal(array, j);
						array[j++] = "m33 t0.xyz, t1.xyz, t2";
						array[j++] = "mul t0.xyz, t0.xyz, a" + joint + ".y";
						array[j++] = "add o1, o1, t0.xyz";
					}
				} else {
					j = decodeMatrix(array, j, joint, "z");
					array[j++] = "m34 t0.xyz, i0, t2";
					array[j++] = "mul t0.xyz, t0.xyz, a" + joint + ".w";
					array[j++] = "add o0, o0, t0.xyz";
					
					j = decodeNormal(array, j);
					array[j++] = "m33 t0.xyz, t1.xyz, t2";
					array[j++] = "mul t0.xyz, t0.xyz, a" + joint + ".w";
					array[j++] = "add o1, o1, t0.xyz";
				}
			}
			array[j++] = "mov o0.w, i0.w";
			res.compileFromArray(array);
			res.assignConstantsArray(numJoints*2 + 5);
			for (i = 0; i < maxInfluences; i += 2) {
				res.assignVariableName(VariableType.ATTRIBUTE, int(i/2), "joint" + i);
			}
			return res;
		}
		
		private function calculateTransformProcedure(maxInfluences:int, numJoints:int):Procedure 
		{
			var res:Procedure = transformProcedures[maxInfluences | (numJoints << 16)];
			if (res != null) return res;
			res = transformProcedures[maxInfluences | (numJoints << 16)] = new Procedure(null, "SkinTransformProcedure");
			
			var array:Array = [];
			var j:int = 0;
			array[j++] = "sge t1.w, i0.w, i0.w"; // x  1
			
			for (var i:int = 0; i < maxInfluences; i ++) {
				var joint:int = int(i/2);
				if (i%2 == 0) {
					j = decodeMatrix(array, j, joint, "x");
					if (i == 0) {
						array[j++] = "m34 t0.xyz, i0, t2";
						array[j++] = "mul o0, t0.xyz, a" + joint + ".y";
						
						j = decodeNormal(array, j);
						array[j++] = "m33 t0.xyz, t1.xyz, t2";
						array[j++] = "mul o1, t0.xyz, a" + joint + ".y";
					} else {
						array[j++] = "m34 t0.xyz, i0, t2";
						array[j++] = "mul t0.xyz, t0.xyz, a" + joint + ".y";
						array[j++] = "add o0, o0, t0.xyz";
						
						j = decodeNormal(array, j);
						array[j++] = "m33 t0.xyz, t1.xyz, t2";
						array[j++] = "mul t0.xyz, t0.xyz, a" + joint + ".y";
						array[j++] = "add o1, o1, t0.xyz";
					}
				} else {
					j = decodeMatrix(array, j, joint, "z");
					array[j++] = "m34 t0.xyz, i0, t2";
					array[j++] = "mul t0.xyz, t0.xyz, a" + joint + ".w";
					array[j++] = "add o0, o0, t0.xyz";
					
					j = decodeNormal(array, j);
					array[j++] = "m33 t0.xyz, t1.xyz, t2";
					array[j++] = "mul t0.xyz, t0.xyz, a" + joint + ".w";
					array[j++] = "add o1, o1, t0.xyz";
				}
			}
			array[j++] = "mov o0.w, i0.w";
			res.compileFromArray(array);
		//	res.assignConstantsArray(numJoints*2);
			for (i = 0; i < maxInfluences; i += 2) {
				res.assignVariableName(VariableType.ATTRIBUTE, int(i/2), "joint" + i);
			}
			return res;
		}
		
		private function calculateShadowTransformProcedure(maxInfluences:int, numJoints:int):Procedure 
		{
			var res:Procedure = transformShadowProcedures[maxInfluences | (numJoints << 16)];
			if (res != null) return res;
			res = transformShadowProcedures[maxInfluences | (numJoints << 16)] = new Procedure(null, "SkinTransformProcedure");
			var array:Array = [];
			var j:int = 0;
			array[j++] = "sge t1.w, i0.w, i0.w"; // x  1
			for (var i:int = 0; i < maxInfluences; i ++) {
				var joint:int = int(i/2);
				if (i%2 == 0) {
					j = decodeMatrix(array, j, joint, "x");
					if (i == 0) {
						array[j++] = "m34 t0.xyz, i0, t2";
						array[j++] = "mul o0, t0.xyz, a" + joint + ".y";						
					} else {
						array[j++] = "m34 t0.xyz, i0, t2";
						array[j++] = "mul t0.xyz, t0.xyz, a" + joint + ".y";
						array[j++] = "add o0, o0, t0.xyz";						
					}
				} else {
					j = decodeMatrix(array, j, joint, "z");
					array[j++] = "m34 t0.xyz, i0, t2";
					array[j++] = "mul t0.xyz, t0.xyz, a" + joint + ".w";
					array[j++] = "add o0, o0, t0.xyz";
				}
			}
			array[j++] = "mov o0.w, i0.w";
			res.compileFromArray(array);
			//res.assignConstantsArray(numJoints*2);
			for (i = 0; i < maxInfluences; i += 2) {
				res.assignVariableName(VariableType.ATTRIBUTE, int(i/2), "joint" + i);
			}
			return res;
		}
	}
}
