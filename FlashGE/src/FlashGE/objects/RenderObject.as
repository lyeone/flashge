// 2014-3-10 lye
package FlashGE.objects
{
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.animation.SkeletonAni;
	import FlashGE.common.AxisAlignedBox;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.effects.Effect;
	import FlashGE.effects.EffectGroup;
	import FlashGE.materials.Material;
	import FlashGE.render.Camera3D;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;
	public class RenderObject extends Linkable
	{
		public static var hideLinkedEffect:Boolean = false;
		FlashGE var linkedNodesList:Dictionary = new Dictionary;
		FlashGE var linkedPiecesList:Dictionary;
		FlashGE var linkedContext:Dictionary;
		
		FlashGE var resBasePath:String;
		FlashGE var model:Model;
		FlashGE var skeletonAni:SkeletonAni;
		FlashGE var skeleton:Skeleton;
		
		FlashGE var aniLoop:Boolean = true;
		FlashGE var aniSpeed:Number = 1;
		FlashGE var aniCurFrame:int = 0;
		FlashGE var aniPaused:Boolean;
		FlashGE var inverseAni:Boolean = false;
		FlashGE var aniPreTimer:Number;
		
		FlashGE var castShadow:Boolean = true;
		
		FlashGE var boundingBoxUpdateListener:Function;
		
		FlashGE var mainEffect:Effect;
		FlashGE var mainSprite:RectSprite;
		
		FlashGE var hasReadyForRender:Boolean = false;
		FlashGE var renderListenersList:Vector.<Function>;
		FlashGE var forceRenderModel:Boolean = false;
		FlashGE var openShadow:Boolean = false;
		FlashGE var needAniReadyForModel:Boolean = false;
		FlashGE var openDefaultSilhouette:Boolean = false;
		FlashGE var maxFramesCountSilhouette:int;
		
		FlashGE var parallelEffects:Vector.<ParallelEffectContext>;
		FlashGE var parallelEffectsCount:int = 0;
		FlashGE var originScales:Object;
		FlashGE var aniDelayFrames:int = 0;
		
		FlashGE var nextLoop:Boolean;
		FlashGE var nextAniLoadListener:Function;
		FlashGE var nextSkeletonAni:SkeletonAni;
		FlashGE var nextAniFrameListener:Function;
		FlashGE var nextSkeletonAniName:String;
		FlashGE var nextForceResetFrame:Boolean;
		
		FlashGE var linkedObjsCount:int = 0;
		FlashGE var parallelEffectsCountUsed:int = 0;
		FlashGE var effectChildrenCount:int = 0;
		
		FlashGE var standAniName:String;
		
		FlashGE var forceCalcAllBones:Boolean = false;
		FlashGE var onlyFollowPos:Boolean = false;
		
		FlashGE var linkedChildEffect:Vector.<Effect>
		FlashGE var hasDisposed:Boolean = false;
		FlashGE var curAlpha:Number = -1;
		FlashGE var lockLastAni:Boolean = false;
		FlashGE var ignoreScale:Boolean = false;
		FlashGE var linkedChildEffectContext:Vector.<LinkedEffectContext>;
		
		FlashGE var aniFrameListeners:Dictionary;
		FlashGE var sceneSilhouette:SceneSilhouette;
		FlashGE var isLinkedPiece:Boolean = false;
		FlashGE var hasDefaultSilhouette:Boolean = false;
		FlashGE var ignoreSkeleton:Boolean = false;
		FlashGE var skeletonAniCreate:Function;
		FlashGE var forceRenderIgnoreAlpha:Boolean = false;
		
		public function toString():String
		{
			var ret:String;
			var i:int, n:int;
			ret = "RenderObject("
				+ "name=" + this.name
				+ ",scales("+this.scaleX+"," + this.scaleY + "," + this.scaleZ+")"
				+ ",rotates("+this.rotationX+"," + this.rotationY+ "," + this.rotationZ+")"
				+ ",pos("+this.x+"," + this.y+ "," + this.z+")"
				+ ",local2Camera=" + this.localToCameraTransform
				+ ",resBasePath="+this.resBasePath 
				+ ",ani=" + this.skeletonAni
				+ ",aniLoop=" + this.aniLoop 
				+ ",aniCurFrame="+this.aniCurFrame
				+ ",aniPaused="+this.aniPaused
				+ ",inverseAni="+this.inverseAni
				+ ",visible=" + this.visible
				+ ",curAlpha=" + this.curAlpha
				+ ",hasDisposed=" + this.hasDisposed
				+ ",model=" + this.model
				+ ",effect=" + this.mainEffect;
			
			if (this.linkedNodesList != null) {
				ret += ",linkedNodesList(";
				for(var key0:* in this.linkedNodesList) {
					ret += ("key="+ key0);
					var objList0:Dictionary = linkedNodesList[key0];
					for (var objectKey0:* in objList0) {
						var object0:Entity = objectKey0 as Entity;
						ret += (",obj="+object0);
					}
				}
				
				ret += ")";
			}
			
			if (this.linkedChildEffect != null) {
				ret += ",linkedChildEffect(";
				for (i = 0, n = linkedChildEffect.length; i < n; i++) {
					if (this.linkedChildEffect[i] != null) {			
						ret += ("," + linkedChildEffect[i]);
					}
				}
				ret += ")";
			}
			
			var pec:ParallelEffectContext;
			if (this.parallelEffects != null) {
				ret += ",parallelEffects(";
				for (i = 0, n = parallelEffects.length; i < n; i++) {
					if (this.parallelEffects[i] != null) {
						ret += ("," + this.parallelEffects[i]);
					}
				}
				
				ret += ")";
			}
			
			if (this.linkedChildEffectContext != null) {
				ret += ",linkedChildEffectContext(";
				for (i=0, n=linkedChildEffectContext.length; i < n; i++) {
					if (this.linkedChildEffectContext[i] != null) {
						ret += ("," + this.linkedChildEffectContext[i]);
					}
				}
				
				ret += ")";
			}
			
			return ret;
		}
		
		public function setForceRenderIgnoreAlpha(ignore:Boolean):void
		{
			this.forceRenderIgnoreAlpha = ignore;
		}
		
		public function getHasDefaultSilhouette():Boolean
		{
			return this.hasDefaultSilhouette;
		}
		
		override public function setIgnoreScale(v:Boolean):void
		{
			this.ignoreScale = v;	
		}
		
		override public function isIgnoreScale():Boolean
		{
			return this.ignoreScale;
		}
		
		public function setLockLastAni(lock:Boolean):void
		{
			if (lock == false && this.lockLastAni == true) {
				this.aniPreTimer = 0;
				this.aniDelayFrames = 0;
			}
			this.lockLastAni = lock;
		}
		
		public function setBoundingBox(cx:Number, cy:Number, cz:Number):void
		{
			if(this.boundBox == null) {
				this.boundBox = new AxisAlignedBox();
			}
			
			this.boundBox.minX =-cx * 0.5;
			this.boundBox.maxX = cx * 0.5;
			
			this.boundBox.minY =-cy * 0.5;
			this.boundBox.maxY = cy * 0.5;
			
			this.boundBox.minZ = 0;
			this.boundBox.maxZ = cz;
			if (this.boundingBoxUpdateListener != null) {
				this.boundingBoxUpdateListener(this.boundBox);
			}
		}
		
		public function releaseSkeleton():void
		{
			disposeModel();
			this.skeleton = null;
			this.skeletonAni = null;
			this.resBasePath = null;
		}
				
		public function getMainSprite():RectSprite
		{
			return this.mainSprite;	
		}
		
		public function setForceCalcAllBones(calc:Boolean):void
		{
			this.forceCalcAllBones = calc;
		}
		
		public function setNeedAniReadyForModel(v:Boolean):void
		{
			this.needAniReadyForModel = v;
		}
		
		public function setStandAniName(aniName:String):void
		{
			this.standAniName = aniName;
		}
		
		public function setAniDelayFrames(delays:int):void
		{
			aniDelayFrames = delays;
		}
		
		public function setAniSpeed(speed:Number):void
		{
			this.aniSpeed = speed;
		}
				
		public function getAniSpeed():Number
		{
			return this.aniSpeed;
		}
		
		override public function resetMaterials():void
		{
			var subMesh:Entity;
			var mat:Material;
			
			if (this.model != null) {
				for each(subMesh in this.model.subMeshList) {
					subMesh.resetMaterials();
				}
			}
			
			for each (var piece:RenderObject in this.linkedPiecesList) {
				piece.resetMaterials();
			}
			
			syncOriginScale();
		}
		
		public function setOriginScale(scales:Object):void
		{
			this.originScales = scales;
		}
		
		public function getOriginScales():Object
		{
			return this.originScales;
		}
		
		public function syncOriginScale():void
		{
			if (this.originScales != null) {
				this.setScale(this.originScales.x, this.originScales.y, this.originScales.z);
			}
		}
		
		public function addParallelEffect(frameIndex:int, fxFileName:String, 
										  linkedNodeName:String = null,
										  useParent:Boolean = false,
										  effectName:String = null, linkedHolder:RenderObject = null,
										  disposeListener:Function = null, userData:int = -1, 
										  parallelAniName:String = null,
										  useDir:Boolean = false,
										  loaderListener:Function = null, 
										  ignorRenderCount:Boolean = false,
										  noLink:Boolean = false):Effect
		{
			var ef:Effect = Effect.createFromFile(fxFileName, effectName, loaderListener, userData);
			if (disposeListener != null) {
				ef.addDisposeListener(disposeListener);
			}
			ef.setLoop(false);
			if (ignorRenderCount == false && this.getRenderCount() == 0) {
				ef.loadState = Defines.LOAD_FAILD;
				ef.dispose();
				return null;
			}
			
			var find:Boolean = false;
			var pec:ParallelEffectContext = new ParallelEffectContext;
			pec.linkedHolder = linkedHolder;
			pec.ef = ef;
			pec.linkedNodeName = linkedNodeName;
			pec.frameIndex = frameIndex;
			pec.played = 0;
			pec.useParent = useParent;
			pec.parallelAniName = parallelAniName;
			pec.useDir = useDir;
			pec.noLink = noLink;
			addParallelEffectPEC(pec);
			return ef;
		}
		
		private function addParallelEffectPEC(pec:ParallelEffectContext):void
		{
			this.parallelEffects = this.parallelEffects || new Vector.<ParallelEffectContext>;
			if (pec.useParent) {
				pec.ef.setPosition(this.x, this.y, this.z);
				pec.ef.setRotation(this.rotationX, this.rotationY, this.rotationZ);
			}
			for (var i:int = 0; i < parallelEffectsCount; i++) {
				if (parallelEffects[i] == null) {
					parallelEffects[i] = pec;
					parallelEffectsCountUsed++;
					return;
				}
			}
			
			parallelEffects.push(pec);
			parallelEffectsCount++;
			parallelEffectsCountUsed++;
		}
		
		public function removeParallelEffects(played:int):void
		{
			if (parallelEffects == null) {
				return;
			}
			
			for (var pecIndex:int = 0, n:int = parallelEffects.length; pecIndex < n; pecIndex++) {
				var pec:ParallelEffectContext = parallelEffects[pecIndex];
				if (pec != null && (played==-1 ||pec.played == played)) {
					pec.ef.dispose();
					this.parallelEffects[pecIndex] = null;
					parallelEffectsCountUsed--;
				}
			}
		}
		
		public function removeParallelEffectsByAniName(aniName:String, played:int = -1):void
		{
			if (parallelEffects == null) {
				return;
			}
			
			for (var pecIndex:int = 0, n:int = parallelEffects.length; pecIndex < n; pecIndex++) {
				var pec:ParallelEffectContext = parallelEffects[pecIndex];
				if (pec != null && pec.parallelAniName == aniName && pec.ef != null && (played == -1 || pec.played == played)) {
					pec.ef.dispose();
					this.parallelEffects[pecIndex] = null;
					parallelEffectsCountUsed--;
				}
			}
		}
		
		private function renderParallelEffect(minFrameIndex:int, maxFrameIndex:int, camera:Camera3D, frameChanged:int):void
		{
			for (var pecIndex:int = 0; pecIndex < parallelEffectsCount; pecIndex++) {
				var pec:ParallelEffectContext = this.parallelEffects[pecIndex];
				if (pec == null) continue;
				if (pec.parallelAniName != null && this.skeletonAni !=null && 
					pec.parallelAniName != this.skeletonAni.getName() && pec.played == 0) {
					continue;
				}
				
				if (((pec.frameIndex >= minFrameIndex && pec.frameIndex <= maxFrameIndex) || pec.frameIndex==-1 )&& pec.played == false) {
					pec.played = 1;
					if (pec.linkedHolder !=null&&pec.linkedHolder != this) {
						pec.frameIndex = -1;
						if (pec.useDir) {
							pec.ef.setRotation(this.rotationX, this.rotationY, this.rotationZ);
						}
						pec.linkedHolder.addParallelEffectPEC(pec);
						this.parallelEffects[pecIndex] = null;
						continue;
					}
				} 
				
				if (pec.played) {
					
					if (pec.ef.loadState == Defines.LOAD_ING) continue;
					
					if (pec.ef.loadState != Defines.LOAD_OK) {
						pec.ef.dispose();
						this.parallelEffects[pecIndex] = null;
						continue;
					}
					
					if (pec.ef.getLinkedNode() == null) {
						var initHolder:Entity = this;
						var node:Entity = initHolder.parent;
						if (!pec.useParent) {
							node = initHolder.getNodeByName(pec.linkedNodeName);
							if (node == null) {
								node = initHolder.getNodeByName(pec.ef.effectProp.linkName);
							}
						}
						
						if (node == null) {
							node = initHolder;
						}
						
						if (pec.noLink) {
							pec.ef.parent = node;
						} else {
							pec.ef.setLinkedNode(node, initHolder);
						}
						if (this.skeleton != null && this.model != null && (node is Bone)) {
							this.skeleton.calculateBone(this.model, node as Bone);
						}
					}
					pec.ef.forceFrameChanged = frameChanged;
					if (pec.noLink) {
						renderLinkedObjectItem(pec.ef, pec.ef.parent, camera);
					} else {
						renderLinkedObjectItem(pec.ef, pec.ef.getLinkedNode(), camera);
					}
					if (pec.ef.loadState == Defines.LOAD_NULL) {
						pec.ef.dispose();
						this.parallelEffects[pecIndex] = null;
					}
				}
			}
		}
		
		public function showShadow(show:Boolean):void
		{
			openShadow = show;
		}
		
		public function showDefaultSilhouette(show:Boolean, maxCount:int = 180, 
											  texFile:String = null, 
											  sizeWidth:Number = 0, sizeHeight:Number = 0):void
		{
			this.openDefaultSilhouette = show;
			maxFramesCountSilhouette = maxCount;
			if (texFile != null) {
				if (sceneSilhouette == null) {
					sceneSilhouette = new SceneSilhouette;
				}
				sceneSilhouette.setItem(texFile, sizeWidth, sizeHeight);
			}
		}
		
		public function RenderObject(skeletonFile:String = null)
		{
			setName("none");
			if (skeletonFile!=null) {
				this.skeleton = Skeleton.load(skeletonFile);
			}
		}
		
		public function setForceRenderModel(render:Boolean):void
		{
			this.forceRenderModel = render;
		}
		
		public function addRenderListener(listener:Function):void
		{
			if (listener == null) return;
			
			this.renderListenersList = this.renderListenersList || new Vector.<Function>;
			for (var i:int = 0, n:int = this.renderListenersList.length; i < n; i++) {
				if (this.renderListenersList[i] == null) {
					this.renderListenersList[i] = listener;
					return;
				}
			}
			
			this.renderListenersList.push(listener);
		}
		
		public function removeRenderListener(listener:Function):void
		{
			if (listener == null || this.renderListenersList==null) return;
			
			for (var i:int = 0, n:int = this.renderListenersList.length; i < n; i++) {
				if (this.renderListenersList[i] == listener) {
					this.renderListenersList[i] = null;
					break;
				}
			}
		}
		
		
		public function setBoudingBoxUpdateListener(listener:Function):void
		{
			if (this.boundBox!=null && listener!=null) {
				listener(this.boundBox);
			} else {
				this.boundingBoxUpdateListener = listener;	
			}
		}
		
		public function setCastShadow(cast:Boolean):void
		{
			this.castShadow = cast;
		}
		
		public function getModel():Model
		{
			return this.model;
		}
		
		public function getResPath():String
		{
			return this.resBasePath;
		}
		
		public function setInverseAni(inverse:Boolean):void
		{
			this.inverseAni = inverse;
			this.aniCurFrame = -1;
			this.aniPreTimer = 0;
		}
		
		/*
		@function listerner(curAniFrame:int):void {}
		*/
		public function addAniFrameListener(aniName:String, listener:Function):void
		{
			if (aniName == null || listener == null) {
				return;
			}
			aniFrameListeners = aniFrameListeners || new Dictionary;
			var list:Vector.<Function> = this.aniFrameListeners[aniName];
			if (list == null) {
				this.aniFrameListeners[aniName] = list = new Vector.<Function>;
			}
			
			var i:int, n:int;
			for (i = 0, n = list.length; i < n; i++) {
				if (list[i] == listener) {
					return;
				}
			}
			
			for (i = 0, n = list.length; i < n; i++) {
				if (list[i] == null) {
					list[i] = listener;
					return;
				}
			}
			
			list.push(listener);
		}
		
		public function removeAniFrameListeners(listener:Function):void
		{
			if (aniFrameListeners == null || listener == null) {
				return;
			}
			
			var i:int, n:int;
			for each(var list:* in this.aniFrameListeners) {
				
				for (i = 0, n = list.length; i < n; i++) {
					if (list[i] == listener) {
						list[i] = null;
					}
				}
			}
		}
		
		public function removeAniFrameListener(aniName:String, listener:Function):void
		{
			if (aniFrameListeners == null || aniName == null) {
				return;
			}
			
			var list:Vector.<Function> = this.aniFrameListeners[aniName];
			if (list == null) {
				return;
			}
			
			var i:int, n:int;
			for (i = 0, n = list.length; i < n; i++) {
				if (list[i] == listener) {
					list[i] = null;
					return;
				}
			}
		}
		
		public function removeAllAniFrameListeners():void
		{
			aniFrameListeners = null; 
		}
		
		public function dispose():void
		{
			if (hasDisposed) {
				return;
			}
			
			this.originScales = null;
			
			if (this.linkedChildEffect != null) {
			
				for (var i:int = 0, n:int = this.linkedChildEffect.length; i < n; i++) {
					if (this.linkedChildEffect[i] != null) {
						this.linkedChildEffect[i].removeDisposeListener(onLinkEffectDisposed);
						this.linkedChildEffect[i].dispose();
						this.linkedChildEffect[i] = null;
					}
				}
			}
			this.disposeModel();
			if (this.nextSkeletonAni!= null) {
				this.nextSkeletonAni.removeLoadListener(this.onNextAniLoaded);
				this.nextSkeletonAni.release();
				this.nextSkeletonAni = null;
			}
			this.nextAniLoadListener = null;
			this.nextAniFrameListener = null;
			this.skeletonAni = null;
			this.boundingBoxUpdateListener = null;
			this.removeFromParent();
			this.aniFrameListeners = null;
			this.disposeAllLinkObject();
			this.removeAllPieces();
			if (getLinkedHolder()) {
				getLinkedHolder().removeLinkObject(this);
			}
			
			linkedNodesList = new Dictionary;
			hasDisposed = true;
			removeAllLinkedPieces();
		}
		
		public function disposeModel():void
		{
			if (this.model != null) {
				this.model.dispose();
				this.model = null;
			}
			if (this.skeletonAni != null) {
				this.skeletonAni.release();
				this.skeletonAni = null;
			}
			this.removeAniFrameListener(nextSkeletonAniName, nextAniFrameListener);
			if (nextSkeletonAni != null) {
				nextSkeletonAni.removeLoadListener(onNextAniLoaded);
				nextSkeletonAni.release();
				nextSkeletonAni = null;
			}
			
			nextAniLoadListener = null;
			nextAniFrameListener = null;
			nextSkeletonAniName = null;
		}
		
		public function removeAllPieces():void
		{
			disposeModel();
			if (this.skeletonAni != null) {
				this.skeletonAni.release();
				this.skeletonAni = null;
			}
			this.skeleton = null;
			this.resBasePath = null;
			this.boundBox = null;
			if (this.mainEffect!=null) {
				this.mainEffect.dispose();
				this.mainEffect = null;
			}
			
			if (mainSprite != null) {
				mainSprite.dispose();
				mainSprite = null;
			}
			
			this.removeAniFrameListener(nextSkeletonAniName, nextAniFrameListener);
			if (nextSkeletonAni != null) {
				nextSkeletonAni.removeLoadListener(onNextAniLoaded);
				nextSkeletonAni.release();
				nextSkeletonAni = null;
			}
			
			nextAniLoadListener = null;
			nextAniFrameListener = null;
			nextSkeletonAniName = null;
		}
		
		public function disposeAllLinkObject():void
		{
			for(var key0:* in this.linkedNodesList) {
				var objList0:Dictionary = linkedNodesList[key0];
				for (var objectKey0:* in objList0) {
					var object0:Object = objectKey0 as Object ;
					if (object0.dispose != null) {
						object0.dispose();
					}
				}
			}
			this.linkedNodesList = new Dictionary;
			this.linkedObjsCount = 0;
			
			if (this.linkedContext != null) {
				for(var key:* in this.linkedContext) {
					var objList:Dictionary = linkedContext[key];
					for (var objectKey:* in objList) {
						var object:Object = objectKey as Object ;
						if (object.dispose != null) {
							object.dispose();
						}
					}
				}
				
				this.linkedContext = null;
			}
		}
		
		public function getLinkedObject(nodeName:String):Dictionary
		{
			var node:Entity = this.getNodeByName(nodeName);
			if (node == null) {
				if (this.linkedContext!=null)
					return this.linkedContext[nodeName];
				return null;
			}
			
			var ret:Dictionary = this.linkedNodesList[node];
			if (ret == null) {
				if (this.linkedContext!=null)
					ret = this.linkedContext[nodeName];
			}
			return ret;
		}
		
		override public function switchShowByType(className:Class, visible:Boolean):void
		{
			super.switchShowByType(className, visible);
			
			for(var key0:* in this.linkedNodesList) {
				var objList0:Dictionary = linkedNodesList[key0];
				for (var objectKey0:* in objList0) {
					var object0:Entity = objectKey0 as Entity;
					if (object0 is className) {
						object0.setVisible(visible);
					}
				}
			}
			
			if (this.linkedContext != null) {
				for(var key:* in this.linkedContext) {
					var objList:Dictionary = linkedContext[key];
					for (var objectKey:* in objList) {
						var object:Entity = objectKey as Entity;
						if (object is className) {
							object.setVisible(visible);
						}
					}
				}
			}
		}
		
		override public function linkObject(nodeName:String, object:Entity, aniFrame:int):Boolean
		{
			object.removeFromParent();
			var objList:Dictionary;
			
			object.transformChanged = true;
			if (this.skeleton !=null && this.skeleton.loadState == Defines.LOAD_OK) {
				var node:Entity = this.getNodeByName(nodeName);
				if (node == null) {
					return false;
				}
				objList = this.linkedNodesList[node];
				if (objList == null ) {
					objList = new Dictionary;
					linkedNodesList[node] = objList;
				}
				linkedObjsCount++;
				object.setLinkedNode(node, this);
				objList[object] = aniFrame;
				return true;
			} else if (this.skeleton == null || this.skeleton.loadState == Defines.LOAD_ING) {
				linkedContext = linkedContext || new Dictionary;
				objList = linkedContext[nodeName];
				if (objList == null) {
					objList = new Dictionary;
					linkedContext[nodeName] = objList;
				}
				objList[object] = aniFrame;
				return true;
			}
			return false;
		}
		
		public function linkEffect(ef:Effect, aniFrame:int):Boolean
		{
			if (ef.loadState != Defines.LOAD_OK || ef.effectProp.linkName == null || ef.effectProp.linkName == "") {
				return false;
			}
			return this.linkObject(ef.effectProp.linkName, ef, aniFrame);
		}

		private function onLinkEffectDisposed(ef:Effect):void
		{
			if (this.linkedChildEffect == null) return;
			
			for (var i:int = 0, n:int = this.linkedChildEffect.length; i < n; i++) {
				if (this.linkedChildEffect[i] == ef) {
					this.linkedChildEffect[i] = null;
					ef.removeDisposeListener(onLinkEffectDisposed);
				}
			}
		}
		
		
		private function addLinkEffect(ef:Effect):void
		{
			this.linkedChildEffect ||= new Vector.<Effect>;
			var i:int;
			var n:int;
			var curEf:Effect;
			for (i = 0, n = this.linkedChildEffect.length; i < n; i++) {
				curEf = this.linkedChildEffect[i];
				if (curEf != null) {
					if (curEf.effectGroup == ef.effectGroup) {
						curEf.dispose();
						this.linkedChildEffect[i] = null;
						break;
					}
				}
			}
			
			for (i = 0, n = this.linkedChildEffect.length; i < n; i++) {
				curEf = this.linkedChildEffect[i];
				if (curEf == null) {
					this.linkedChildEffect[i] = ef;
					ef.addDisposeListener(onLinkEffectDisposed);
					return;
				}
			}
			
			ef.addDisposeListener(onLinkEffectDisposed);
			this.linkedChildEffect.push(ef);
		}
		
		public function removeLinkedEffectByName(nodeName:String, efLinkeNode:String, fxFileName:String, effectName:String):void
		{
			var i:int, n:int;
			var ef:Effect;
			if (nodeName == null) {
				nodeName = efLinkeNode;
				if (efLinkeNode == "" || efLinkeNode == null) {
					if (linkedChildEffect != null) {
						for (i = 0, n = this.linkedChildEffect.length; i < n; i++) {
							ef = this.linkedChildEffect[i];
							if (ef != null && ef.effectGroup != null && ef.effectGroup.fileName == fxFileName && ef.effectName) {
								ef.removeDisposeListener(onLinkEffectDisposed);
								ef.dispose();
								this.linkedChildEffect[i] = null;
							}
						}
					}
					return;
				}
			}
			
			if (nodeName == null || nodeName == "") {
				return;
			}
			
			if (linkedNodesList != null) {
				for(var key:* in linkedNodesList) {
					var node:Entity = key as Entity;
					if (node.getName() == nodeName) {
						var objList:Dictionary = linkedNodesList[node];
						for (var objectKey:* in objList) {
							ef = objectKey as Effect;
							if (ef != null && ef.effectGroup != null && ef.effectGroup.fileName == fxFileName && ef.effectName) {
								ef.dispose();
								return;
							}
						}
					}
				}
			}
			
			if (this.linkedContext != null) {
			
				for(var key0:* in linkedContext) {
					var nodeNameKey:String = key0 as String;
					if (nodeNameKey == nodeName) {
						var objList0:Dictionary = linkedContext[key0];
						for (var objectKey0:* in objList0) {
							ef = objectKey0 as Effect;
							if (ef != null && ef.effectGroup != null && ef.effectGroup.fileName == fxFileName && ef.effectName) {
								delete objList0[objectKey0];
								ef.dispose();
								return;
							}
						}
					}
				}
			}
		}
		
		public function disposeAllEquipEffect():void
		{
			var i:int, n:int;
			var ef:Effect;
			
			if (linkedChildEffect != null) {
				for (i = 0, n = this.linkedChildEffect.length; i < n; i++) {
					ef = this.linkedChildEffect[i];
					if (ef != null && ef.isEquip) {
						ef.removeDisposeListener(onLinkEffectDisposed);
						ef.dispose();
						this.linkedChildEffect[i] = null;
					}
				}
			}
			
			if (linkedNodesList != null) {
				for(var key:* in linkedNodesList) {
					var node:Entity = key as Entity;
					var objList:Dictionary = linkedNodesList[node];
					for (var objectKey:* in objList) {
						ef = objectKey as Effect;
						if (ef != null && ef.isEquip) {
							ef.dispose();
						}
					}
				}
			}
			
			if (this.linkedContext != null) {
				
				for(var key0:* in linkedContext) {
					var nodeNameKey:String = key0 as String;
					var objList0:Dictionary = linkedContext[key0];
					for (var objectKey0:* in objList0) {
						ef = objectKey0 as Effect;
						if (ef != null && ef.isEquip) {
							delete objList0[objectKey0];
							ef.dispose();
							return;
						}
					}
				}
			}
		}
		
		private function linkEffectByNameImpl(ef:Effect, nodeName:String, 
											aniFrame:int, 
											unique:Boolean = false):void
		{
			if (hasDisposed==true) {
				ef.dispose();
				return;
			}
			ef.setUserData(aniFrame);
			
			if (ef.loadState == Defines.LOAD_OK) {
				if (unique) {
					removeLinkedEffectByName(nodeName, ef.effectProp.linkName, ef.effectGroup.fileName, ef.effectName);
				}
				if (nodeName==null) {
					if (ef.loadState == Defines.LOAD_OK) {
						if (ef.effectProp.linkName=="" || ef.effectProp.linkName == null) {
							
							addLinkEffect(ef);
							
						} else {
							if (!linkEffect(ef, ef.getUserData())) {
								ef.dispose();
							}
						}
					}
				} else {
					if (!linkObject(nodeName, ef, ef.getUserData())) {
						ef.dispose();
					}
				}
			} else {
				ef.dispose();
			}
		}
		
		private function onLinkEffectLoaded(ef:Effect, isOK:Boolean):void
		{
			if (linkedChildEffectContext == null) {
				ef.dispose();
				return;
			}
			
			for (var i:int = 0, n:int = this.linkedChildEffectContext.length; i < n; i++) {
				var item:LinkedEffectContext = this.linkedChildEffectContext[i];
				if (item != null && item.ef == ef) {
					this.linkedChildEffectContext[i] = null;
					this.linkEffectByNameImpl(ef, item.nodeName, item.aniFrame, item.unique);
					return;
				}
			}
			
			ef.dispose();
		}
		
		public function linkEffectByName(fxFileName:String, aniFrame:int, 
										 nodeName:String = null, 
										 effectName:String = null,
										 unique:Boolean = false, 
										 isEquip:Boolean = false):Effect
		{
			linkedChildEffectContext = linkedChildEffectContext || new Vector.<LinkedEffectContext>;
			
			var ef:Effect = Effect.createFromFile(fxFileName, effectName);
			ef.isEquip = isEquip;
			
			if (ef.isLoaded()) {
				linkEffectByNameImpl(ef, nodeName, aniFrame, unique);
			} else {
				ef.loadListener = onLinkEffectLoaded;
				var item:LinkedEffectContext = new LinkedEffectContext;
				item.ef = ef;
				item.aniFrame = aniFrame;
				item.effectName = effectName;
				item.unique = unique;
				item.fxFileName = fxFileName;
				item.nodeName = nodeName;
				for (var i:int = 0, n:int = this.linkedChildEffectContext.length; i < n; i++) {
					if (this.linkedChildEffectContext[i] == null) {
						this.linkedChildEffectContext[i] = item;
						return item.ef;
					}
				}
				
				this.linkedChildEffectContext.push(item);
			}
			return ef;
		}
		
		override public function isOnlyFollowPos():Boolean
		{
			return this.onlyFollowPos;
		}
		
		override public function setOnlyFollowPos(value:Boolean):void
		{
			onlyFollowPos = value;
		}
		
		private function checkLinkedContext():void
		{
			if (linkedContext != null && (this.skeleton.loadState == Defines.LOAD_OK ||this.skeleton.loadState == Defines.LOAD_FAILD)) {
				var temp:Dictionary = this.linkedContext;
				this.linkedContext = null;
				
				for(var key0:* in temp) {
					var nodeName:String = key0 as String;
					
					var objList0:Dictionary = temp[key0];
					for (var objectKey0:* in objList0) {
						if (!linkObject(nodeName, objectKey0 as Entity, objList0[objectKey0])) {
							//if (this.skeleton.allBonesCount > 0) {
							//	linkObject(this.skeleton.allBones[0].name, objectKey0 as Entity, objList0[objectKey0]);
							//} else {
							this.addChild(objectKey0 as Entity);
							//}
						}
					}
				}
			}
		}
		
		public function checkCalcBoneTransform(bone:Bone):void
		{
			//if (bone.calcFrame == Global.frameCount) return;
			if (this.model == null || this.skeleton == null) return;
			this.skeleton.calcBoneTransform(this.model, bone);
		}
		
		private function renderLinkObjects(camera:Camera3D, curFrame:int):void
		{
			
			var tempTrans:Transform3D;
			
			for(var key:* in linkedNodesList) {
				var node:Entity = key as Entity;
				
				var objList:Dictionary = linkedNodesList[node];
				for (var objectKey:* in objList) {
					var linkAniFrame:int = objList[objectKey];
					var object:Entity = objectKey as Entity; 
					if (linkAniFrame >= 0) {
						if (curFrame == linkAniFrame) {
							if (object.getParent() != camera.getRootNode()) {
								tempTrans = camera.pool.popTransform3D();
								tempTrans.combine(camera.localToGlobalTransform, node.localToCameraTransform);
								object.setPosition(tempTrans.d, tempTrans.h, tempTrans.l);
								
								tempTrans.combine(camera.localToGlobalTransform, this.localToCameraTransform);
								var scalex:Number = Math.sqrt(tempTrans.a*tempTrans.a + tempTrans.e*tempTrans.e + tempTrans.i*tempTrans.i);
								var scaley:Number = Math.sqrt(tempTrans.b*tempTrans.b + tempTrans.f*tempTrans.f + tempTrans.j*tempTrans.j);
								var scalez:Number = Math.sqrt(tempTrans.c*tempTrans.c + tempTrans.g*tempTrans.g + tempTrans.k*tempTrans.k);
								object.setScale(1/scalex, 1/scaley, 1/scalez);
								object.setLinkedTransform(tempTrans);
								camera.pool.pushTransform3D(tempTrans);
								this.removeLinkObject(object);
								camera.getRootNode().addChild(object);
							}
						}
					} else if ((this.forceRenderIgnoreAlpha || (!this.forceRenderIgnoreAlpha && this.curAlpha != 0)) && object.isVisible()){
						renderLinkedObjectItem(object, node, camera);
					}
				}
			}
		}
		
		public function renderLinkedObjectItem(object:Entity, node:Entity, camera:Camera3D):void
		{
			if (object is RenderObject) {
				if ((object as RenderObject).isLinkedPiece && this.renderCount == 0) {
					return;
				}
			} else if (object is Effect) {
				if ((object as Effect).isEquip && (this.renderCount == 0 || RenderObject.hideLinkedEffect)) {
					return;
				}
			}
			
			if (object.transformChanged) {
				object.setLinkedTransform(null);
				object.composeTransforms();
			}
			
			var tempTrans:Transform3D;
			var bone:Bone = node as Bone;
			var transformA:Transform3D;
			var transformB:Transform3D;
			if (bone != null && object.isOnlyFollowPos() && bone.curKeyFrame != null && bone.curKeyFrame.transform != null) {
				tempTrans = camera.pool.popTransform3D();
				//tempTrans.copy(model.localToCameraTransform);
				transformA = model.localToCameraTransform;
				transformB = bone.curKeyFrame.transform;
				tempTrans.a = transformA.a;
				tempTrans.b = transformA.b;
				tempTrans.c = transformA.c;
				tempTrans.d = transformA.a*transformB.d + transformA.b*transformB.h + transformA.c*transformB.l + transformA.d;
				tempTrans.e = transformA.e;
				tempTrans.f = transformA.f;
				tempTrans.g = transformA.g;
				tempTrans.h = transformA.e*transformB.d + transformA.f*transformB.h + transformA.g*transformB.l + transformA.h;
				tempTrans.i = transformA.i;
				tempTrans.j = transformA.j;
				tempTrans.k = transformA.k;
				tempTrans.l = transformA.i*transformB.d + transformA.j*transformB.h + transformA.k*transformB.l + transformA.l;
				
				object.localToCameraTransform.combine(tempTrans, object.transform);
				object.cameraToLocalTransform.copy(object.localToCameraTransform);
				object.cameraToLocalTransform.invert();
				camera.pool.pushTransform3D(tempTrans);
				
			} else if(bone != null && object.isIgnoreScale() && bone.curKeyFrame != null && bone.curKeyFrame.transform != null) {
				tempTrans = camera.pool.popTransform3D();
				transformA = node.localToCameraTransform;
				var scaleX:Number = Math.sqrt(transformA.a * transformA.a + transformA.b * transformA.b + transformA.c * transformA.c);
				var scaleY:Number = Math.sqrt(transformA.e * transformA.e + transformA.f * transformA.f + transformA.g * transformA.g);
				var scaleZ:Number = Math.sqrt(transformA.i * transformA.i + transformA.j * transformA.j + transformA.k * transformA.k);
				tempTrans.a = transformA.a / scaleX;
				tempTrans.b = transformA.b / scaleX;
				tempTrans.c = transformA.c / scaleX;
				tempTrans.d = transformA.d;
				tempTrans.e = transformA.e / scaleY;
				tempTrans.f = transformA.f / scaleY;
				tempTrans.g = transformA.g / scaleY;
				tempTrans.h = transformA.h;
				tempTrans.i = transformA.i / scaleZ;
				tempTrans.j = transformA.j / scaleZ;
				tempTrans.k = transformA.k / scaleZ;
				tempTrans.l = transformA.l;
				
				object.localToCameraTransform.combine(tempTrans, object.transform);
				object.cameraToLocalTransform.copy(object.localToCameraTransform);
				object.cameraToLocalTransform.invert();
				camera.pool.pushTransform3D(tempTrans);
			} else if (object.isOnlyFollowPos() && node.parent != null){
				
				tempTrans = camera.pool.popTransform3D();
				transformA = node.parent.localToCameraTransform;
				transformB = node.transform;
				tempTrans.a = transformA.a;
				tempTrans.b = transformA.b;
				tempTrans.c = transformA.c;
				tempTrans.d = transformA.a*transformB.d + transformA.b*transformB.h + transformA.c*transformB.l + transformA.d;
				tempTrans.e = transformA.e;
				tempTrans.f = transformA.f;
				tempTrans.g = transformA.g;
				tempTrans.h = transformA.e*transformB.d + transformA.f*transformB.h + transformA.g*transformB.l + transformA.h;
				tempTrans.i = transformA.i;
				tempTrans.j = transformA.j;
				tempTrans.k = transformA.k;
				tempTrans.l = transformA.i*transformB.d + transformA.j*transformB.h + transformA.k*transformB.l + transformA.l;
				
				object.localToCameraTransform.combine(tempTrans, object.transform);
				object.cameraToLocalTransform.copy(object.localToCameraTransform);
				object.cameraToLocalTransform.invert();
				camera.pool.pushTransform3D(tempTrans);
				
			} else {
				
				object.localToCameraTransform.combine(node.localToCameraTransform, object.transform);
				object.cameraToLocalTransform.combine(object.inverseTransform, node.cameraToLocalTransform);
			}
			
			object.calculateVisibility(camera);
			object.calculateChildrenVisibility(camera);
			object.collectRenderUnits(camera, false);
			object.collectChildrenRenderUnits(camera, false);
		}
		
		public function zeroNextAniLoadListener():void
		{
			this.nextAniLoadListener = null;	
		}
		
		private var allAniForbidDispose:Boolean = false;
		public function setAllAniForbidDispose(v:Boolean):void
		{
			this.allAniForbidDispose = v;
		}
		
		public function playAni(aniName:String, loop:Boolean = true, 
								loadListener:Function = null, 
								aniFrameListener:Function = null,
								failedIfFirstLoad:Boolean = false,
								moveToHightPriority:Boolean = false, 
								forceResetFrame:Boolean = false):void
		{
			if (this.model==null || aniName==null || aniName=="") {
				return;
			}

			if (this.standAniName != null ) {
				if (aniName == "stand" && aniName != this.standAniName) {
					aniName = this.standAniName;
				}
			}
			
			if (this.resBasePath == null) {
				var temp:String = model.getName();
				if (temp == null) {
					return;
				}
				
				this.resBasePath = temp.slice(0, temp.indexOf(temp.split("/").pop()));
			}
			
			nextLoop = loop;
			nextAniLoadListener = loadListener;
			nextAniFrameListener = aniFrameListener;
			nextSkeletonAniName = aniName;
			nextForceResetFrame = forceResetFrame;
			this.addAniFrameListener(aniName, aniFrameListener);

			if (nextSkeletonAni != null) {
				nextSkeletonAni.removeLoadListener(onNextAniLoaded);
				nextSkeletonAni.release();
				nextSkeletonAni = null;
			}
			
			if (this.skeletonAniCreate == null) {
				this.skeletonAniCreate = SkeletonAni.create;
			}
			
			var nextAniFileName:String = this.resBasePath + aniName + ".ani";
			nextSkeletonAni = this.skeletonAniCreate(this.skeleton, nextAniFileName, onNextAniLoaded, false, failedIfFirstLoad, false, moveToHightPriority);
			if (nextSkeletonAni.hasDisposed) {
				nextSkeletonAni = null;
			}
		}
		
		private function onNextAniLoaded(ani:SkeletonAni, isOK:Boolean):void
		{
			var curListener:Function;
			if (isOK == false) {
				this.removeAniFrameListener(nextSkeletonAniName, nextAniFrameListener);
				if (nextSkeletonAni != null) {
					nextSkeletonAni.removeLoadListener(onNextAniLoaded);
					nextSkeletonAni.release();
					nextSkeletonAni = null;
				}
				nextAniFrameListener = null;
				nextSkeletonAniName = null;
				
				if (nextAniLoadListener != null) {
					curListener = this.nextAniLoadListener
					nextAniLoadListener = null;
					curListener(ani, false);
				}
				return;
			}
			
			if (nextAniLoadListener != null) {
				curListener = this.nextAniLoadListener
				nextAniLoadListener = null;
				curListener(ani, true);
			}
			
			if (this.skeleton == null) {
				this.skeleton = Skeleton.load(this.resBasePath + "skeleton.ske");	
			}
			
			if (this.inverseAni == false) {
				if (this.skeletonAni != ani || this.aniLoop != nextLoop || nextForceResetFrame) {
					this.aniCurFrame = 0;
					this.aniCurFrameTimer = 0;
				}
			}
			
			this.aniLoop = nextLoop;
			ani.addRef();
			if (this.skeletonAni != null) {
				this.skeletonAni.release();
				this.skeletonAni = null;
			}
			this.skeletonAni = ani;
			if (allAniForbidDispose) {
				this.skeletonAni.addRef();
			}
			
			this.aniPreTimer = 0;
			this.skeletonAni.skeleton = this.skeleton;
			
			if (nextSkeletonAni != null) {
				nextSkeletonAni.removeLoadListener(onNextAniLoaded);
				nextSkeletonAni.release();
				nextSkeletonAni = null;
			}
			nextAniFrameListener = null;
			nextSkeletonAniName = null;
		}
		
		public function setAniLoop(loop:Boolean):void
		{
			this.aniLoop = loop;
		}
		
		public function gotoAniFrame(aniFrame:uint):void
		{
			this.aniCurFrame = aniFrame;
			this.aniCurFrameTimer = -1;
			this.aniPreTimer = 0;
		}
		
		override protected function addToList(child:Entity):void
		{
			super.addToList(child);
			if (child is Effect) {
				this.effectChildrenCount++;
			}
		}
		
		override public function removeLinkObject(object:Entity):void
		{
			var objList:Dictionary = linkedNodesList[object.getLinkedNode()];
			if (objList != null ) {
				delete objList[object];
				linkedObjsCount--;
			}
			object.setLinkedNode(null, null);
			if (this.linkedContext != null) {
				for each (var objListB:* in this.linkedContext) {
					delete objListB[object];
				}
			}
		}
		
		override protected function removeFromList(child:Entity):Entity
		{
			var ret:Entity = super.removeFromList(child);
			if (ret is Effect) {
				this.effectChildrenCount--;
			}
			return ret;
		}
		
		override public function onLinkedObject(obj:Entity, linkedNode:Entity):void
		{
			var ef:Effect = obj as Effect;
			if (ef != null && ef.effectProp.linkedNamesList != null) {
				var linkedNameList:Vector.<String> = ef.effectProp.linkedNamesList;
				for (var i:int = 0, n:int = linkedNameList.length; i < n; i++) {
					var node:Entity = this.getNodeByName(linkedNameList[i]);
					if (node != null) {
						node.refCountLinked++;
					}
				}
			}
		}
				
		override public function onUnlinkedObject(obj:Entity, linkedNode:Entity):void
		{
			var ef:Effect = obj as Effect;
			if (ef != null && ef.effectProp.linkedNamesList != null) {
				var linkedNameList:Vector.<String> = ef.effectProp.linkedNamesList;
				for (var i:int = 0, n:int = linkedNameList.length; i < n; i++) {
					var node:Entity = this.getNodeByName(linkedNameList[i]);
					if (node != null) {
						node.refCountLinked--;
					}
				}
			}
			
		}
		private var renderCount:int = 0;
		public function getRenderCount():int
		{
			return this.renderCount;
		}
		
		FlashGE var preAniFrameIndex:int = -1;
		public function getPreAniFrameIndex():int
		{
			return this.preAniFrameIndex;
		}
		
		FlashGE var aniReachEnd:Boolean = false;
		public function getAniReachEnd():Boolean
		{
			return this.aniReachEnd;
		}
		
		//public var frameBox:FrameBox = new FrameBox;
		FlashGE var frameTick:int = -1;
		private var aniCurFrameTimer:Number = 0;
		override FlashGE function collectRenderUnits(camera:Camera3D, isShadowPass:Boolean):int 
		{
			hasDefaultSilhouette = false;
			if (frameTick == -1) {
				frameTick = Global.frameCount;
			} else if (frameTick == Global.frameCount) {
				return renderCount;
			}

			frameTick = Global.frameCount;
			var drawCount:int = 0;
			var aniFrame:int = 0;
			var aniPlayed:Boolean = false;
			var i:int;
			var n:int;
			
			if (this.model != null) {
				model.cameraToLocalTransform = (this.cameraToLocalTransform);
				model.localToCameraTransform = (this.localToCameraTransform);
			}
			
			if (this.skeleton != null) {
				checkLinkedContext();
			}
			
			var renderParallelEffectFrameMin:int = -1;
			var renderParallelEffectFrameMax:int = -1;
			var hasCalcBones:Boolean = false;
			var hasCalcAni:Boolean = false;
			var hasAniFrameChanged:int = 0;
			if (this.aniPreTimer == 0 && this.aniCurFrame == 0) {
				preAniFrameIndex = -1;
			} else {
				preAniFrameIndex = this.aniCurFrame;
			}
			aniReachEnd = false;
			if (this.skeletonAni !=null && skeletonAni.isLoaded()) {
				if (this.skeletonAni.skeleton != this.skeleton) {
					this.skeletonAni.skeleton = this.skeleton;
				}
				var maxAniFrameCount:int = this.skeletonAni.getMaxFrame();
				if (aniCurFrame == -1) {
					if (this.inverseAni) {
						aniCurFrame = maxAniFrameCount - 1;
						aniCurFrameTimer = (maxAniFrameCount - 1)/skeletonAni.interval;
					} else {
						aniCurFrame = 0;
						aniCurFrameTimer = 0;
					}
				} else if (aniCurFrameTimer == -1) {
					aniCurFrameTimer = aniCurFrame/skeletonAni.interval;
				}
				
				var curDeltaTimer:Number = 0;
				if (aniPaused == false) {
					if (lockLastAni) {
						aniCurFrame = maxAniFrameCount - 1;
						aniCurFrameTimer = maxAniFrameCount/skeletonAni.interval;
					} else {
						var curTimer:Number = getTimer();
						if (this.aniPreTimer == 0) {
							aniPreTimer = curTimer;
						}
						
						curDeltaTimer = camera.curDeltaTimer;
						if (inverseAni) {
							aniCurFrameTimer -= curDeltaTimer;
						} else {
							aniCurFrameTimer += curDeltaTimer;
						}
						
						if (camera.aniSpeedExtra > 0) {
							aniCurFrame = int(aniCurFrameTimer * skeletonAni.interval * camera.aniSpeedExtra);
						} else {
							aniCurFrame = int(aniCurFrameTimer * skeletonAni.interval * this.aniSpeed);
						}
						

						aniPreTimer = curTimer;
					}
				}
				
				if (this.inverseAni) {
					if (aniCurFrameTimer < 0) {
						aniReachEnd = true;
						if (this.aniLoop) {
							aniCurFrame = maxAniFrameCount - 1;
							aniCurFrameTimer = aniCurFrame / skeletonAni.interval;
						} else {
							aniCurFrame = 0;
							aniCurFrameTimer = 0;
						}
					}
				} else {
					if (aniCurFrame >= maxAniFrameCount) {
						aniReachEnd = true;
						if (this.aniLoop) {
							aniCurFrame = aniCurFrame % maxAniFrameCount;
						} else {
							aniCurFrame = maxAniFrameCount - 1;
						}
					}
				}
				
				if (!camera.closeCollectRenderUnits) {
					this.skeletonAni.updateAni(aniCurFrame);
				}

				hasCalcAni = true;
				
				aniFrame = aniCurFrame;
				aniPlayed = true;

				if (this.model != null) {
					if (this.parallelEffectsCountUsed > 0 || 
						this.linkedObjsCount > 0 || 
						this.linkedContext != null || 
						this.effectChildrenCount > 0 ||
						this.forceCalcAllBones == true) {
						hasCalcBones = true;
						this.skeleton.calculateBonesTransforms(this.model, this.forceCalcAllBones);
					}
				}
				renderParallelEffectFrameMin = preAniFrameIndex;
				renderParallelEffectFrameMax = aniReachEnd ? maxAniFrameCount - 1 : aniFrame;
				drawCount++;
				if (aniDelayFrames > 0) {
					aniDelayFrames--;
				}
			}
			
			var canRenderModel:Boolean = true;
			if (forceRenderModel==false && drawCount == 0) {
				if (this.skeletonAni != null) {
					
					if (!this.skeletonAni.isLoaded() && this.skeletonAni.isNeedLoadOK()) {
						canRenderModel = false;
					}
				}
			}
			
			renderCount = 0;
			if (canRenderModel && aniDelayFrames == 0 && this.model != null && this.model.isLoaded() && this.isVisible()) {
				drawCount++;
				
				if ((this.skeletonAni != null && this.skeletonAni.loadState == Defines.LOAD_OK &&
					this.skeleton != null && this.skeleton.loadState == Defines.LOAD_OK) || 
					this.skeleton == null || 
					this.skeleton.loadState == Defines.LOAD_FAILD ||
					this.ignoreSkeleton) {
					if (model.loadedCount == 0) {
						renderCount = model.collectRenderUnits(camera, isShadowPass);
					}
				}
				
				if (hasCalcAni == true && hasCalcBones==false && (this.parallelEffectsCountUsed > 0 || 
					this.linkedObjsCount > 0 || 
					this.linkedContext != null || 
					this.effectChildrenCount > 0 ||
					this.forceCalcAllBones == true)) {
					this.skeleton.calculateBonesTransforms(this.model, this.forceCalcAllBones);
				}
				renderLinkObjects(camera, aniFrame);
				
				if (this.renderListenersList!=null) {
					for (i = 0, n = renderListenersList.length; i < n; i++) {
						if (this.renderListenersList[i] !=null) {
							this.renderListenersList[i](this);
						}
					}
				}
				
				if (this.castShadow && this.isVisible() && camera.shadowCalculor != null && !camera.closeCollectRenderUnits && this.curAlpha != 0) {					
					
					if (this.parent != null) {
						
						camera.shadowCalculor.addCaster(this, this.parent.localToCameraTransform);
						
					} else if (this.getLinkedNode()!=null) {
						
						camera.shadowCalculor.addCasterByPos(this, this.getLinkedNode().localToCameraTransform, 0, 0, 0);
					}
				}
			}
			
			this.renderParallelEffect(renderParallelEffectFrameMin, renderParallelEffectFrameMax, camera, hasAniFrameChanged);
			
			if (aniPlayed && this.aniFrameListeners != null && (hasAniFrameChanged || this.aniLoop == false)) {
				var aniList:Vector.<Function> = aniFrameListeners[this.skeletonAni.getName()];
				if (aniList != null) {
					for (var ani:int = 0, listNum:int = aniList.length; ani < listNum; ani++) {
						var callFun:Function = aniList[ani];
						if (callFun != null) {
							callFun(this, this.skeletonAni, aniFrame, this.skeletonAni.getMaxFrame());
						}
					}
				}
			}
			
			/*
			if (frameBox.parent == null && this.skeletonAni != null && this.parent != null) {
				this.addChild(frameBox);
			}
			if (this.boundBox != null) {
				frameBox.setScaleX((this.boundBox.maxX - this.boundBox.minX)*0.05);
				frameBox.setScaleY((this.boundBox.maxY - this.boundBox.minY)*0.05);
				frameBox.setScaleZ((this.boundBox.maxZ - this.boundBox.minZ)*0.05);
				frameBox.setPosition(0,0,this.boundBox.minZ + (this.boundBox.maxZ - this.boundBox.minZ)*0.5);
				frameBox.collectRenderUnits(camera, false);
				frameBox.collectChildrenRenderUnits(camera, false);
			}
			*/
			if (!camera.closeCollectRenderUnits) {
				if (drawCount == 2 && renderCount > 0) {
					
					if (this.boundBox==null && this.skeleton != null && this.skeleton.aabb!=null) {
						this.boundBox = this.skeleton.aabb.clone();
						if (this.boundingBoxUpdateListener!=null) {
							this.boundingBoxUpdateListener(this.boundBox);
						}
					}
					
					if (openShadow == true && this.parent != null && isShadowPass == false && (this.curAlpha >= 0.0001 || this.curAlpha == -1)) {					
						camera.getNormalShadow().addItem(this.x, this.y, this.z);					
					}
					if (openDefaultSilhouette == true) {
						openDefaultSilhouette = false;
					}
				} else if (renderCount == 0) {
					
					if (openDefaultSilhouette) {
						if (maxFramesCountSilhouette > 0) {
							maxFramesCountSilhouette--	
						}
					}
					
					if (this.parent != null && openDefaultSilhouette && maxFramesCountSilhouette == 0) {
						if (sceneSilhouette == null) {
							if (this.parent != camera.rootNode) {
								
								var tranCamera:Transform3D = this.parent.localToCameraTransform;
								
								var camX:Number = this.x*tranCamera.a + this.y*tranCamera.b + this.z*tranCamera.c + tranCamera.d;
								var camY:Number = this.x*tranCamera.e + this.y*tranCamera.f + this.z*tranCamera.g + tranCamera.h;
								var camZ:Number = this.x*tranCamera.i + this.y*tranCamera.j + this.z*tranCamera.k + tranCamera.l;
								
								tranCamera = camera.rootNode.cameraToLocalTransform;
								var newX:Number = camX*tranCamera.a + camY*tranCamera.b + camZ*tranCamera.c + tranCamera.d;
								var newY:Number = camX*tranCamera.e + camY*tranCamera.f + camZ*tranCamera.g + tranCamera.h;
								var newZ:Number = camX*tranCamera.i + camY*tranCamera.j + camZ*tranCamera.k + tranCamera.l;
								camera.getDefaultSilhouette().addItem(newX, newY, newZ);
							} else {
								camera.getDefaultSilhouette().addItem(this.x, this.y, this.z);					
							}
							hasDefaultSilhouette = true;
						} else if(this.parent != null) {
							sceneSilhouette.setPosition(this.x, this.y, this.z);
							if (sceneSilhouette.transformChanged) {
								sceneSilhouette.composeTransforms(false);
							}
							
							sceneSilhouette.localToCameraTransform.combine(this.parent.localToCameraTransform, sceneSilhouette.transform);
							sceneSilhouette.collectRenderUnits(camera, false);
							hasDefaultSilhouette = true;
						}
						
						if (hasDefaultSilhouette) {
							if (castShadow == true && camera.shadowCalculor == null && this.parent != null) {					
								camera.getNormalShadow().addItem(this.x, this.y, this.z);					
							}
						}
					}
				}
			}						
			if (this.boundBox==null && this.mainSprite != null && this.mainSprite.boundBox != null) {
				this.boundBox = this.mainSprite.boundBox.clone();
				if (this.boundingBoxUpdateListener != null) {
					this.boundingBoxUpdateListener(this.boundBox);
				}
			}
			
			if (this.boundBox == null && this.mainEffect != null && mainEffect.isLoaded()) {
				setBoundingBox(20, 20, 180);
			}
			
			if (this.linkedChildEffect != null) {

				for (i = 0, n = this.linkedChildEffect.length; i < n; i++) {
					var ef:Effect = this.linkedChildEffect[i];
					if (ef != null) {
						if (ef.isOnlyFollowPos()) {
							var tempTrans:Transform3D = camera.pool.popTransform3D();
							var transformA:Transform3D = this.localToCameraTransform;
							var transformB:Transform3D = ef.transform;
							tempTrans.a = transformA.a;
							tempTrans.b = transformA.b;
							tempTrans.c = transformA.c;
							tempTrans.d = transformA.a*transformB.d + transformA.b*transformB.h + transformA.c*transformB.l + transformA.d;
							tempTrans.e = transformA.e;
							tempTrans.f = transformA.f;
							tempTrans.g = transformA.g;
							tempTrans.h = transformA.e*transformB.d + transformA.f*transformB.h + transformA.g*transformB.l + transformA.h;
							tempTrans.i = transformA.i;
							tempTrans.j = transformA.j;
							tempTrans.k = transformA.k;
							tempTrans.l = transformA.i*transformB.d + transformA.j*transformB.h + transformA.k*transformB.l + transformA.l;
							
							ef.localToCameraTransform.combine(tempTrans, ef.transform);
							ef.cameraToLocalTransform.copy(ef.localToCameraTransform);
							ef.cameraToLocalTransform.invert();
							camera.pool.pushTransform3D(tempTrans);
						} else {
							ef.cameraToLocalTransform.copy(this.cameraToLocalTransform);
							ef.localToCameraTransform.copy(this.localToCameraTransform);
							ef.calculateVisibility(camera);
							ef.calculateChildrenVisibility(camera);
							ef.collectRenderUnits(camera, false);
							ef.collectChildrenRenderUnits(camera, false);
						}
					}
				}
			}
			hasReadyForRender = drawCount == 2;
		/*
			var box:AxisAlignedBox = this.getBoundBox();
			if (box != null && this.parent ) {
				if (frameBox == null) {
					frameBox = new FrameBox();
					camera.rootNode.addChild(frameBox);
				}
				frameBox.setRotation(this.rotationX, this.rotationY, this.rotationZ);
				frameBox.setPosition(this.x, this.y, this.z);
				frameBox.setScaleX(0.05 * (box.maxX - box.minX));
				frameBox.setScaleY(0.05 * (box.maxY - box.minY));
				frameBox.setScaleZ(0.05 * (box.maxZ - box.minZ));
				
			}
				*/
			
			return renderCount;
		}
		
		public function isReadyForRender():Boolean
		{
			return hasReadyForRender;
		}
		
		public function getCurFrame():int
		{
			return this.aniCurFrame;
		}
		
		public function getSkeletonAni():SkeletonAni
		{
			return this.skeletonAni;
		}
		
		public function getNextSkeletonAni():SkeletonAni
		{
			return this.nextSkeletonAni;
		}
		
		public function getSkeleton():Skeleton
		{
			return this.skeleton;
		}
		
		override public function getNodeByName(name:String):Entity
		{
			if (name == null) return null;
			
			var node:Entity = super.getNodeByName(name);
			if (node==null && this.model != null) {
				node = this.model.getNodeByName(name);
			}
			
			return node;
		}
		
		public function getAniMaxFrame():int
		{
			return this.skeletonAni != null ? this.skeletonAni.getMaxFrame() : int.MAX_VALUE;
		}
		
		public function pauseAni(v:Boolean):void
		{
			if (v == false && this.aniPaused == true) {
				this.aniPreTimer = 0;
				this.aniDelayFrames = 0;
			}
			this.aniPaused = v;
		}
		
		public function resetAni():void
		{
			pauseAni(false);
			this.aniCurFrame = 0;
			this.aniCurFrameTimer = 0;
			this.aniPreTimer = 0;
			this.aniDelayFrames = 0;
		}
		
		public function linkPiece(userData:int, nodeName:String, pieceFileName:String, 
								  double:Boolean = false, scale:Number = 1.0, 
								  uvContext:Object = null, outPiece:Vector.<RenderObject> = null):void
		{
			if (pieceFileName == null || pieceFileName == "") return;
			if (nodeName == null || nodeName == "") return;
			
			this.linkedPiecesList = this.linkedPiecesList || new Dictionary;
			var obj:RenderObject;
			
			if (double == true) {
				for (var i:int = 1; i <=2; i++) {
					obj = this.linkedPiecesList[i<<16 | userData];
					if (obj == null) {
						obj = this.linkedPiecesList[i<<16 | userData] = new RenderObject;
						obj.setName(pieceFileName);
						obj.isLinkedPiece = true;
					}
					obj.addPiece(pieceFileName, i<<16 | userData, true, false, Defines.MATERIAL_MODEL, true);
					obj.setScale(scale, scale, scale);
					this.linkObject(nodeName+String(i), obj, -1);
					
					if (obj.getModel() != null) {
						obj.playAni("stand");
						obj.setCastShadow(this.castShadow);
					}
					
					if (outPiece != null) {
						outPiece[i-1] = obj;
					}
					
					obj.setAlpha(this.curAlpha);
				}
			} else {
				obj = this.linkedPiecesList[userData];
				if (obj == null) {
					obj = this.linkedPiecesList[userData] = new RenderObject;
					obj.setName(pieceFileName);
					obj.isLinkedPiece = true;
				}
				obj.addPiece(pieceFileName, userData, true, false, Defines.MATERIAL_MODEL, true, false, uvContext);
				obj.setScale(scale, scale, scale);
				this.linkObject(nodeName, obj, -1);
				if (obj.getModel() != null) {
					obj.playAni("stand");
					obj.setCastShadow(this.castShadow);
				}
				if (outPiece != null) {
					outPiece[0] = obj;
				}
				obj.setAlpha(this.curAlpha);
			}
			
		}
		
		public function relinkPiece(userData:int, nodeName:String, double:Boolean, scale:Number = 1.0):void
		{
			var pieceFileName:String = this.removeLinkPiece(userData);
			if (pieceFileName == null) return;
			
			this.linkPiece(userData, nodeName, pieceFileName, double, scale);
		}
				
		public function removeAllLinkedPieces(onlyEquipFx:Boolean = false):void
		{
			this.curAlpha = -1;
			if (this.linkedPiecesList != null) {
			
				for each (var obj:RenderObject in this.linkedPiecesList) {
					obj.dispose();
				}
				
				this.linkedPiecesList = null;
			}
			
			if (mainEffect != null) {
				mainEffect.dispose();
				mainEffect = null;
			}
			
			var i:int, n:int;
			if (linkedChildEffect != null) {
				for (i = 0, n = this.linkedChildEffect.length; i < n; i++) {
					var ef:Effect = this.linkedChildEffect[i];
					if (ef != null) {
						if ((onlyEquipFx && ef.isEquip) || !onlyEquipFx) {
							ef.removeDisposeListener(onLinkEffectDisposed);
							ef.dispose();
							this.linkedChildEffect[i] = null;
						}
					}
				}
			}
			
			if (linkedChildEffectContext != null) {
				for (i = 0, n = this.linkedChildEffectContext.length; i < n; i++) {
					var item:LinkedEffectContext = this.linkedChildEffectContext[i];
					if (item != null && item.ef != null) {
						item.ef.removeDisposeListener(onLinkEffectDisposed);
						item.ef.dispose();
						this.linkedChildEffectContext[i] = null;
					}
				}
			}
		}
		
		public function getLinkedPiecesList():Dictionary
		{
			return this.linkedPiecesList;
		}
		
		public function removeLinkPiece(userData:int):String
		{
			if (this.linkedPiecesList == null) return null;
			var o:RenderObject;
			var pieceFileName:String;
			for (var i:int =0; i < 3; i++) {
				o = this.linkedPiecesList[i<<16|userData];
				if (o!=null) {
					if (pieceFileName==null) pieceFileName = o.getName();
					o.dispose();
				}
				delete this.linkedPiecesList[i<<16|userData];
			}
			
			return pieceFileName;
		}
		
		public function addPiece(pieceFileName:String, userData:int=0, useLight:Boolean = true, 
								 recieveShadow:Boolean = false, 
								 materialType:int = Defines.MATERIAL_MODEL,
								 needCheckRes:Boolean = true, autoSize:Boolean = false, 
								 uvContext:Object = null,
								 insertFirst:Boolean = false, 
								 moveToHighPriority:Boolean = false):void
		{
			if (pieceFileName == null || pieceFileName == "") return;
			
			var tag:String = pieceFileName.substr(pieceFileName.length - 3, 3); 
			if (tag == ".fx") {
				addEffect(pieceFileName, userData);
			} else if(tag == "mes") {
				
				if (needCheckRes) {
					var temp:String = pieceFileName;
					var resBasePathTemp:String = temp.slice(0, temp.indexOf( temp.split("/").pop()));
					
					if (this.resBasePath != resBasePathTemp) {
						this.removeAllPieces();
						this.resBasePath = resBasePathTemp;
					}
				}
				
				if (this.skeleton == null) {
					this.skeleton = Skeleton.load(this.resBasePath + "skeleton.ske", null, true);	
				}
				
				if (this.model == null) {
					this.model = new Model(this.skeleton);
				}
				
				this.model.addPiece(pieceFileName, userData, useLight, recieveShadow, materialType, uvContext, insertFirst, moveToHighPriority);
			} else {
				this.boundBox=null;
				if (TextureFile.forceATF) {
					if (tag == "tga") {
						pieceFileName = pieceFileName.replace(".tga", ".atf");
					}
				}
				var tex:TextureFile = TextureFile.createFromFile(pieceFileName);
				if (mainSprite == null) {
					mainSprite = RectSprite.create(autoSize);
					mainSprite.setTexture(tex);
					mainSprite.getMaterial().setSrcBlend(Defines.BLEND_SOURCE_ALPHA);
					mainSprite.getMaterial().setDstBlend(Defines.BLEND_ONE_MINUS_SOURCE_ALPHA);
					mainSprite.getMaterial().setRenderPriority(Defines.PRIORITY_EFFECT);
					this.addChild(mainSprite);
				} else {
					mainSprite.setTexture(tex);
				}
				
				tex.release();
			}
		}
		
		private function addEffect(effectFileName:String, userData:int):void
		{
			EffectGroup.create(effectFileName, onEffectGroupLoad, false);
		}
		
		private function onEffectGroupLoad(effectGroup:EffectGroup):void
		{
			if (mainEffect != null) {
				mainEffect.dispose();
				mainEffect = null;
			}
			
			if (this.hasDisposed) {
				return;
			}
			
			if (effectGroup.nameIndexs != null && effectGroup.nameIndexs.length > 0) {
				mainEffect = Effect.createFromGroup(effectGroup, effectGroup.nameIndexs[0]);
				mainEffect.loop = true;
				this.addChild(mainEffect);
			}
		}
		
		public function setDirXY(dirx:Number, diry:Number):void
		{
			this.setRotationZ(getRadian(dirx, diry));
		}
		
		public function setColors(r:Number, g:Number, b:Number):void
		{
			if (this.model != null) {
				this.model.setAllMaterialColor(r, g, b);
			}
		}
		
		public function getAlpha():Number
		{
			return this.curAlpha;	
		}
		
		public function setAlpha(alpha:Number, force:Boolean = false):void
		{
			if (this.model != null && (this.curAlpha != alpha || force)) {
				this.curAlpha = alpha;
				this.model.setAllMaterialAlpha(alpha);
				
				for each (var piece:RenderObject in this.linkedPiecesList) {
					piece.setAlpha(alpha, force);
				}
			}
		}
				
		public function clone():RenderObject
		{
			var ret:RenderObject = new RenderObject;
			ret.resBasePath = this.resBasePath;
			ret.skeleton = this.skeleton;
			ret.skeletonAni = this.skeletonAni;
			if (ret.skeletonAni != null) {
				ret.skeletonAni.addRef();
			}
			ret.aniLoop = this.aniLoop;
			ret.aniCurFrame = this.aniCurFrame;
			ret.aniCurFrameTimer = this.aniCurFrameTimer;
			ret.aniFrameListeners = null;
			ret.aniPaused = this.aniPaused;
			ret.inverseAni = this.inverseAni;
			ret.castShadow = this.castShadow;
			ret.mainSprite = this.mainSprite;
			ret.hasReadyForRender = this.hasReadyForRender;
			ret.forceRenderModel = this.forceRenderModel;
			ret.x = this.x;
			ret.y = this.y;
			ret.z = this.z;
			ret.rotationX = this.rotationX;
			ret.rotationY = this.rotationY;
			ret.rotationZ = this.rotationZ;
			ret.scaleX = this.scaleX;
			ret.scaleY = this.scaleY;
			ret.scaleZ = this.scaleZ;
			ret.transformChanged = true;
			
			if (this.model!=null) {
				ret.model = this.model.clone();
			}
			
			if (this.mainEffect != null && this.mainEffect.effectGroup != null) {
				ret.addPiece(this.mainEffect.effectGroup.fileName, 0, false);
			}
			
			if (this.mainSprite && this.mainSprite.getMaterial().textureFilesCount > 0) {
				
				var tex:TextureFile = this.mainSprite.getMaterial().getTexture(0);
				tex.addRef();
				ret.mainSprite = RectSprite.create(false);
				ret.mainSprite.setTexture(tex);
				ret.mainSprite.getMaterial().setSrcBlend(Defines.BLEND_SOURCE_ALPHA);
				ret.mainSprite.getMaterial().setDstBlend(Defines.BLEND_ONE_MINUS_SOURCE_ALPHA);
				ret.mainSprite.getMaterial().setRenderPriority(Defines.PRIORITY_EFFECT);
				ret.addChild(ret.mainSprite);
				tex.release();
			}
			return ret;
		}
	}
}

import FlashGE.effects.Effect;
import FlashGE.objects.RenderObject;

internal class ParallelEffectContext
{
	public var ef:Effect = null;
	public var linkedNodeName:String;
	public var frameIndex:int = 0;
	public var played:int = 0;
	public var useParent:Boolean = false;
	public var linkedHolder:RenderObject = null;
	public var parallelAniName:String;
	public var useDir:Boolean = false;
	public var noLink:Boolean;
	
	public function toString():String
	{
		var ret:String = "ParallelEffectContext(";
		ret += ("linkeNodeName=" + this.linkedNodeName);
		ret += (",frameIndex=" + this.frameIndex);
		ret += (",played=" + this.played);
		ret += (",useParent=" + this.useParent);
		ret += (",parallelAniName=" + this.parallelAniName);
		ret += (",useDir=" + this.useDir);
		ret += ")";
		return ret;
	}
}

internal class LinkedEffectContext
{
	public var ef:Effect;
	public var fxFileName:String;
	public var effectName:String;
	public var aniFrame:int;
	public var nodeName:String;
	public var unique:Boolean;
}