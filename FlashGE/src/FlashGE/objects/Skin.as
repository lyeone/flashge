/*@author lye 123*/

package FlashGE.objects
{
	import flash.display3D.Context3D;
	import flash.display3D.Context3DTriangleFace;
	import flash.utils.Dictionary;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.loaders.MeshLoader;
	import FlashGE.materials.Material;
	import FlashGE.materials.ShaderProgram;
	import FlashGE.render.Camera3D;
	import FlashGE.render.RenderUnit;
	import FlashGE.resources.Geometry;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;

	public class Skin extends Entity
	{
		FlashGE var skinData:SkinData;
		FlashGE var surfaces:Vector.<Surface>;
		FlashGE var surfacesLength:int = 0;
		FlashGE var skeleton:Skeleton;
		FlashGE var meshLoader:MeshLoader;
		FlashGE var dataIndex:int = 0;
		FlashGE var curGeometry:Geometry;
		FlashGE var hasDisposed:Boolean = false;
		private var pool:Pool;
		
		public function Skin() 
		{
		}
		
		public function dispose():void
		{
			if (pool == null) {
				pool = Pool.getInstance();
			}
			this.hasDisposed = true;
			this.isValid = false;
			if (this.surfaces != null) {
				for (var i:int = 0; i < this.surfacesLength; i++) {
					this.surfaces[i].clearRenderUnits();
					pool.pushObject(pool.ClassSurface, this.surfaces[i]);
				}
				this.surfaces.length = 0;
			}
			
			this.surfacesLength = 0;
			this.skinData = null;
			
			if (this.curGeometry != null) {
				this.curGeometry.release(true);
				this.curGeometry = null;
			}
			
			if (this.material != null) {
				this.material.release();
				this.material = null;
			}
			
			if (this.meshLoader != null) {
				this.meshLoader.release();
				this.meshLoader = null;
			}
			pool.pushObject(pool.ClassSkin, this);
		}
		
		override public function clearRenderUnits():void
		{
			for (var i:int = 0; i < this.surfacesLength; i++) {
				this.surfaces[i].clearRenderUnits();
			}
		}
		
		override FlashGE function calcProgramKey(camera:Camera3D):void
		{
			if (this.programKey == null) {
				this.programKey = "skin";
			}
		}
		
		public function toString():String
		{
			var ret:String = "Skin(name=" + this.name;
			ret += (",dataIndex=" + this.dataIndex);
			ret += (",surfacesLength=" + this.surfacesLength);
			ret += (",hasDisposed="+this.hasDisposed);
			ret += (",meshLoader=" + this.meshLoader);
			ret += (",skinData=" + this.skinData);
			ret += ")";
			return ret;
		}

		override FlashGE function collectRenderUnits(camera:Camera3D, isShadowPass:Boolean):int 
		{
			if (this.hasDisposed == true ||
				this.isValid == false ||
				this.skinData == null || this.curGeometry == null || this.skeleton==null || this.skeleton.allBones ==null) 
				return 0;
			
			if (this.skeleton.loadState != Defines.LOAD_OK) {
				return 0;
			}
			var renderCount:int = 0;
			for (var i:int = 0; i < this.surfacesLength; i++) {
				var surface:Surface = this.surfaces[i];
				positionProcedure = skinData.surfacePositionProcedures[i];
				transformProcedure = skinData.surfaceTransformProcedures[i];
				if (skinData.surfaceTransformReciveShadowProcedures != null) {
					transformReciveShadowProcedure = skinData.surfaceTransformReciveShadowProcedures[i];
				} else if (transformReciveShadowProcedure != null) {
					transformReciveShadowProcedure = null;
				}
				renderCount += this.material.collectDraws(camera, surface, this.curGeometry);
				CONFIG::CACHE_PROGRAM_KEY {
					if (surface.materialKey != null && 
						skinData.surfaces[i].materialKey == null && 
						this.material.type == Defines.MATERIAL_MODEL) {
						skinData.surfaces[i].materialKey = surface.materialKey;
					}
				}
			}
			
			CONFIG::CACHE_PROGRAM_KEY {
				if (this.material.programCachedID != null && 
					this.skinData.programCachedID == null && 
					this.material.type == Defines.MATERIAL_MODEL) {
					this.skinData.programCachedID = this.material.programCachedID;
				}
			}
			return renderCount;
		}
		
		override FlashGE function setTransformConstants(renderUnit:RenderUnit, 
														surface:Surface, 
														shader:ShaderProgram, 
														camera:Camera3D, 
														setContext:uint = 0xffffffff):void 
		{
			if (this.skinData == null || this.skeleton == null || 
				this.skeleton.loadState != Defines.LOAD_OK || shader == null) {
				return;
			}
				
			var i:int, count:int;
			var context3D:Context3D = camera.context3D;
			if (setContext&0x1) {

				var attribute:int = 0;
				var g:Geometry = this.curGeometry;
				if (skinData.maxInfluences > 0) {
					attribute = Defines.JOINTS[0];
					renderUnit.setVertexBufferAt(
						shader.cJoint0,
						g.getVertexBuffer(attribute, context3D), 
						g.attrOffsetsList[attribute], 
						Defines.FORMATS[attribute]);
				}
				
				if (skinData.maxInfluences > 2) {
					attribute = Defines.JOINTS[1];
					renderUnit.setVertexBufferAt(
						shader.cJoint2,
						g.getVertexBuffer(attribute, context3D),
						g.attrOffsetsList[attribute],
						Defines.FORMATS[attribute]);
				}
			}
			
			if (setContext&0x2) {
				var surfaceIndex:int = surface.surfaceIndex;
				var surfaceBones:Vector.<Bone> = skinData.surfaceBonesList[surfaceIndex];
				
				var boneNameList:Dictionary = this.skeleton.boneNameList;
				var allBones:Vector.<Bone> = this.skeleton.allBones;
				var id:int;
				var surfaceBoneNamesList:Vector.<String> = skinData.surfaceBoneNamesList[surfaceIndex];
				for (i = 0,count = surfaceBones.length; i < count; i++) {
					var bone:Bone = surfaceBones[i];
					if (bone == null) {
						id = boneNameList[surfaceBoneNamesList[i]];
						bone = surfaceBones[i] = allBones[id];
					}
					
					if (bone == null || bone.curKeyFrame == null) {
						//Global.throwError("Skin::setTransformConstants")
						//renderUnit.setVertexConstantsFromTransform(i*2, Transform3D.IDENTITY);
						renderUnit.setVertexConstantsFromNumbers(5 +i*2, 0, 0, 1, 1);
						renderUnit.setVertexConstantsFromNumbers(5 +i*2+1, 0, 0, 0, 0);
					} else {
						//renderUnit.setVertexConstantsFromTransform(i*2, bone.curKeyFrame.boneTransform);
						renderUnit.setVertexConstantsFromNumbers(5 +i*2, bone.curKeyFrame.axisX, bone.curKeyFrame.axisY, bone.curKeyFrame.axisZ, bone.curKeyFrame.cos_angle);
						renderUnit.setVertexConstantsFromNumbers(5 +i*2+1, bone.curKeyFrame.x, bone.curKeyFrame.y, bone.curKeyFrame.z, bone.curKeyFrame.sin_angle);
					}
					
				}
			}
		}
		
		override public function resetMaterials():void
		{
			if (this.meshLoader == null) return;
			
			var mat:Material = Material.createMaterial();
			this.setMaterial(mat);
			if (skinData.diffuseTexture != null && skinData.diffuseTexture.length > 0) {
				var diffuseTexture:TextureFile = TextureFile.createFromFile(skinData.diffuseTexture); 
				mat.setTexture(0, diffuseTexture);
				diffuseTexture.release();
			}
			
			mat.setAlphaTest(skinData.openAlphaTest ? 1 : 0);
			mat.setCulling(Context3DTriangleFace.FRONT);
			mat.enableUseLight(meshLoader.useLight);
			mat.setRecieveShadow(meshLoader.recieveShadow);
			mat.setMipIndex(2);
			mat.setType(meshLoader.materialType);
			mat.setUVV0(0);
			mat.setUVV1(0);
			
			if (meshLoader.uvContext != null && skinData.meshName != null) {
				var uv:Object = meshLoader.uvContext[skinData.meshName];
				if (uv != null) {
					mat.setUVV0(Number(uv[1]));
					mat.setUVV1(Number(uv[2]));
				}
			}
			mat.release();
		}
		
		override public function setMaterial(src:Material):void
		{
			super.setMaterial(src);
			for (var i:int = 0, n:int = this.surfacesLength; i < n; i++) {
				this.surfaces[i].materialKey = null;
			}
		}
		
		public function initFromMeshLoader(loader:MeshLoader, dataIndex:int):void
		{
			this.dataIndex = dataIndex;
			this.meshLoader = loader;
			this.meshLoader.addRef();
			this.userData = loader.userData;
			this.setName(loader.meshNameList[dataIndex]);
			var skinData:SkinData = loader.skinDataList[dataIndex];
			if (this.curGeometry != null) {
				this.curGeometry.release(true);
			}
			this.curGeometry = skinData.geometry;
			if (this.curGeometry != null) {
				this.curGeometry.addRef();
			}
			
			this.skinData = skinData;
			this.useLights = true;
			this.surfaces = new Vector.<Surface>;
			this.surfaces.length = this.surfacesLength = skinData.surfacesLength;
			if (pool == null) {
				pool = Pool.getInstance();
			}
			for (var i:int = 0; i < surfacesLength; i++) {
				var src:Surface = skinData.surfaces[i];
				var dst:Surface = pool.popObject(pool.ClassSurface);
				dst.indexBegin = src.indexBegin;
				dst.object = this;
				dst.trianglesNum = src.trianglesNum;
				dst.texIndex = src.texIndex;
				dst.surfaceIndex = i;
				dst.materialKeyBase = src.materialKeyBase;
				dst.materialKey = src.materialKey;
				dst.renderUnit0 = null;
				dst.renderUnit1 = null;
				
				surfaces[i] = dst;
			}
			
			resetMaterials();
			this.material.programCachedID = this.skinData.programCachedID;
		}
	}
}
