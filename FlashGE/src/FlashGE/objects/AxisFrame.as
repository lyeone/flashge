package FlashGE.objects
{
	import flash.geom.Vector3D;
	
	import FlashGE.FlashGE;

	use namespace FlashGE;
	
	public class AxisFrame extends Entity
	{
		public function AxisFrame(thickness:Number = 1)
		{
			var pointsx:Vector.<Vector3D> = Vector.<Vector3D>( [
				//x轴
				new Vector3D(0, 0, 0), new Vector3D(100, 0, 0),
				new Vector3D(100, 0, 0), new Vector3D(90, 4, 0),
				new Vector3D(100, 0, 0), new Vector3D(90, -4, 0)
			] );
			var coordinatex:WireFrame = WireFrame.createLinesList(pointsx, 0xFF0000, 1, thickness);
			this.addChild(coordinatex);
			
			var pointsy:Vector.<Vector3D> = Vector.<Vector3D>( [
				//x轴
				new Vector3D(0, 0, 0), new Vector3D(0, 100, 0),
				new Vector3D(0, 100, 0), new Vector3D(-4, 90, 0),
				new Vector3D(0, 100, 0), new Vector3D(4, 90, 0)
			] );
			var coordinatey:WireFrame = WireFrame.createLinesList(pointsy, 0x00FF00, 1, thickness);
			this.addChild(coordinatey);
			
			var pointsz:Vector.<Vector3D> = Vector.<Vector3D>( [
				//x轴
				new Vector3D(0, 0, 0), new Vector3D(0, 0, 100),
				new Vector3D(0, 0, 100), new Vector3D(-4, 0, 90),
				new Vector3D(0, 0, 100), new Vector3D(4, 0, 90)
			] );
			var coordinatez:WireFrame = WireFrame.createLinesList(pointsz, 0x0000FF, 1, thickness);
			this.addChild(coordinatez);	
		}
		
		public function dispose():void
		{
			for (var child:Entity = this.childrenList; child = child.next; child!=null) {
				if (child is AxisFrame) {
					WireFrame(child).dispose();
				}
			}
			
			this.childrenList = null;
		}
	}
}