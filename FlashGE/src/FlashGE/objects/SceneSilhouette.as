package FlashGE.objects
{
	import flash.display3D.Context3D;
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Context3DVertexBufferFormat;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.Program3D;
	import flash.display3D.VertexBuffer3D;
	import flash.display3D.textures.TextureBase;
	import flash.utils.ByteArray;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.materials.compiler.Procedure;
	import FlashGE.objects.Entity;
	import FlashGE.render.Camera3D;
	import FlashGE.render.RenderUnit;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;
	public class SceneSilhouette extends Entity
	{
		private var baseTransform:Transform3D = new Transform3D;
		private var renderUnit:RenderUnit;
		private var curIndexBuffer:IndexBuffer3D;
		private var curVertexBuffer:VertexBuffer3D;
		private var curProgram3D:Program3D;
		private var context3DUsed:Context3D;
		private var textureFile:TextureFile;
		private var sizeWidth:Number;
		private var sizeHeight:Number;
		private var vertexCodeBytes:ByteArray;
		private var fragmentCodeBytes:ByteArray;
		private var pool:Pool;
		public function SceneSilhouette()
		{
		}
		
		public function setBaseTransform(transform:Transform3D):void
		{
			this.baseTransform.copy(transform);	
		}
		
		public function setItem(texFile:String, sizeWidth:Number, sizeHeight:Number):void
		{
			if (renderUnit == null) {
				if (pool == null) {
					pool = Pool.getInstance();
				}
				renderUnit = pool.popRenderUnit();
				renderUnit.srcBlend = Context3DBlendFactor.SOURCE_ALPHA;
				renderUnit.dstBlend = Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA;
				renderUnit.object = this;
				renderUnit.numTriangles = 2;
				renderUnit.vertexConstantsRegistersCount = 4;
			}
			
			if (textureFile != null) {
				textureFile.release();
				textureFile = null;
			}
			
			textureFile = TextureFile.createFromFile(texFile, false, false, null, true, false, true);
			renderUnit.texturesLength = 0;
			renderUnit.setTextureAt(0, textureFile);
			context3DUsed = null;
			
			this.sizeWidth = sizeWidth;
			this.sizeHeight = sizeHeight;
		}
		
		public function create3DBuffers(context3D:Context3D):void
		{
			if (curIndexBuffer != null) {
				curIndexBuffer.dispose();
				curIndexBuffer = null;
			}
			
			if (curVertexBuffer != null) {
				curVertexBuffer.dispose();
				curVertexBuffer = null;
			}
			
			if (curProgram3D != null) {
				curProgram3D.dispose();
				curProgram3D = null;
			}
			
			
			var texFormatID:int = this.textureFile.getFormatID();
			var minX:Number =-this.textureFile.getRealWidth() * 0.5;
			var maxX:Number = this.textureFile.getAlignWidth() + minX;
			var maxY:Number = this.textureFile.getRealHeight() * 0.5;
			var minY:Number = -(this.textureFile.getAlignHeight() - maxY);
			var i:int, n:int;
			var vertices:Vector.<Number> = new Vector.<Number>();
			var indices:Vector.<uint> = new Vector.<uint>();
			vertices.push(
				 minX, 0, minY, 0, 1,
				 minX, 0, maxY, 0, 0,
				 maxX, 0, maxY, 1, 0,
				 maxX, 0, minY, 1, 1);
			
			indices.push(3, 1, 0, 1, 3, 2);
			
			curIndexBuffer = context3D.createIndexBuffer(6);
			CONFIG::DEBUG {
				Global.indexBuffersCount++;
			}
			curIndexBuffer.uploadFromVector(indices, 0, 6);
			
			curVertexBuffer = context3D.createVertexBuffer(4, 5);
			CONFIG::DEBUG {
				Global.vertexBuffersCount++;
			}
			curVertexBuffer.uploadFromVector(vertices, 0, 4);
			
			if (this.vertexCodeBytes == null || this.fragmentCodeBytes == null) {
				var shaderIndex:int = 0;
				var vertShaderArray:Array = [];			
				vertShaderArray[shaderIndex++] = "m44 o0, a0, c0";
				vertShaderArray[shaderIndex++] = "mov v0, a1";
				
				shaderIndex = 0;
				var fragShaderArray:Array = [];
				fragShaderArray[shaderIndex++] = "tex t0, v0.xy, s0 <2d, linear, repeat, miplinear, "+Defines.TEX_FORMATS[texFormatID]+">";
				fragShaderArray[shaderIndex++] = "mov o0, t0";
				
				var vertProcedure:Procedure = new Procedure(vertShaderArray);
				var fragProcedure:Procedure = new Procedure(fragShaderArray);
				this.vertexCodeBytes = vertProcedure.getByteCode(Context3DProgramType.VERTEX);
				this.fragmentCodeBytes = fragProcedure.getByteCode(Context3DProgramType.FRAGMENT);
			}
			
			curProgram3D = context3D.createProgram();
			curProgram3D.upload(this.vertexCodeBytes, this.fragmentCodeBytes);
			
			renderUnit.vertexBuffersLength = 0
			renderUnit.setVertexBufferAt(0, curVertexBuffer, 0, Context3DVertexBufferFormat.FLOAT_3);
			renderUnit.setVertexBufferAt(1, curVertexBuffer, 3, Context3DVertexBufferFormat.FLOAT_2);
			
			renderUnit.indexBuffer = curIndexBuffer;
			renderUnit.program = curProgram3D;
		}
		
		
		override FlashGE function collectRenderUnits(camera:Camera3D, isShadowPass:Boolean):int 
		{
			if (renderUnit == null || textureFile == null) {
				return 0;
			}
			
			var texture3D:TextureBase = textureFile.getTexture(camera.context3D);
			if (texture3D == null) {
				return 0;
			}
			
			if (context3DUsed != camera.context3D) {
				context3DUsed = camera.context3D;
				
				this.setScale(sizeWidth/this.textureFile.getRealWidth(), 1, sizeHeight/this.textureFile.getRealHeight());
				this.create3DBuffers(context3DUsed);
			}
			
			if (curIndexBuffer == null || curVertexBuffer == null || curProgram3D == null) {
				return 0;
			}
			
			renderUnit.setProjectionConstants(camera, 0, this.localToCameraTransform);
			camera.renderer.addRenderUnit(renderUnit, Defines.PRIORITY_BEFORE_OPAQUE);
			return 1;
		}
		
	}
}