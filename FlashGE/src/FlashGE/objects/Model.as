/*@author lye 123*/

package FlashGE.objects
{
	import flash.utils.Dictionary;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.loaders.MeshLoader;
	import FlashGE.materials.Material;
	import FlashGE.render.Camera3D;
	
	use namespace FlashGE;
	
	public class Model extends Entity
	{
		FlashGE var skeleton:Skeleton;
		FlashGE var loadState:int = Defines.LOAD_NULL;
		FlashGE var meshLoaders:Dictionary;
		FlashGE var loadedCount:int = 0;
		
		FlashGE var subMeshList:Vector.<Entity> = new Vector.<Entity>;
		FlashGE var hasDisposed:Boolean = false;
		private var pool:Pool;
		public function Model(skeleton:Skeleton)
		{
			this.skeleton = skeleton;
		}
		
		public function toString():String
		{
			var ret:String = "Model(loadState="+loadState;
			
			for each(var subMesh:Entity in this.subMeshList) {
				ret += ("," + subMesh);
			}
			ret += ")";
			
			return ret;
		}
		
		public function getBoneByName(boneName:String):Bone
		{
			if (skeleton==null) {
				return null;
			}
			
			var bone:Bone;
			for (var i:int = 0; i < skeleton.allBonesCount; i++) {
				bone = skeleton.allBones[i];
				if (bone!=null && bone.getName() == boneName)
					return bone;
			}
			return null;
		}
		
		override public function getNodeByName(nodeName:String):Entity
		{
			var node:Entity = getBoneByName(nodeName);
			if (node!=null)
				return node;
			
			return getChildByName(nodeName);
		}
		
		public function isLoaded():Boolean
		{
			return this.loadState == Defines.LOAD_FAILD || this.loadState == Defines.LOAD_OK;
		}

		public function setAllMaterialColor(r:Number, g:Number, b:Number):void
		{
			var mat:Material;
			for each(var subMesh:Entity in this.subMeshList) {
				if (subMesh != null) {
					mat = subMesh.getMaterial();
					if (mat != null) {
						mat.setDiffuseColor(r, g, b);
					}
				}
			}
		}		
		
		override FlashGE function collectRenderUnits(camera:Camera3D, isShadowPass:Boolean):int 
		{
			if (hasDisposed) {
				return 0;
			}
			var renderCount:int = 0;
			for each(var subMesh:Entity in this.subMeshList) {
				if (subMesh != null) {
					subMesh.localToCameraTransform = this.localToCameraTransform;
					subMesh.cameraToLocalTransform = this.cameraToLocalTransform;
					renderCount += subMesh.collectRenderUnits(camera, isShadowPass);
				}
			}
			return renderCount;
		}
		
		FlashGE function removeSubMeshByUserData(userData:int):void
		{
			var subMesh:Entity;
			for (var i:int = 0, n:int = subMeshList.length; i < n; i++) {
				subMesh = subMeshList[i];
				if (subMesh != null && (subMesh.userData == userData || userData ==-1)) {
					subMesh.removeFromParent();
					if (subMesh.hasOwnProperty("dispose")) {
						(subMesh as Object).dispose();
					}
					subMeshList[i] = null;
				}
			}
		}
		
		FlashGE function onMeshLoaded(loader:MeshLoader):void
		{
			if (hasDisposed) {
				return;
			}
			
			if (loadedCount > 0) {
				loadedCount--;
			}
			removeSubMeshByUserData(loader.userData);
			var i:int = 0;
			var n:int = 0;
			if (pool == null) {
				pool = Pool.getInstance();
			}
			if (loader.skinDataList !=null) {
				for (i=0, n = loader.skinDataList.length; i < n; i++) {
					var skin:Skin = pool.popObject(pool.ClassSkin);
					skin.skeleton = this.skeleton;
					skin.hasDisposed = false;
					skin.isValid = true;
					skin.setForceVisible(true);
					skin.initFromMeshLoader(loader, i);
					this.addSubMesh(skin);
				}
			}
			
			if (loader.geometryList != null) {
				for (i=0, n= loader.geometryList.length; i < n; i++) {
					var mesh:Mesh = pool.popObject(pool.ClassMesh);
					mesh.hasDisposed = false;
					mesh.isValid = true;
					mesh.setForceVisible(true);
					mesh.initFromMeshLoader(loader, i);
					this.addSubMesh(mesh);
				}
			}
			
			this.loadState = Defines.LOAD_OK;
		}
		
		FlashGE function dispose():void
		{
			hasDisposed = true;
			
			this.removeSubMeshByUserData(-1);
			
			
			if (this.meshLoaders != null) {
				for (var loader:*  in this.meshLoaders) {
					loader.release();
					loader.delListener(onMeshLoaded);
				}
				this.meshLoaders = null;
			}
			loadedCount = 0;
		}
		
		FlashGE function clone():Model
		{
			var ret:Model = new Model(this.skeleton);
			ret.loadState = this.loadState;
			var srcMesh:Mesh;
			var srcSkin:Skin;
			var subMesh:Entity;
			if (pool == null) {
				pool = Pool.getInstance();
			}
			for (var i:int = 0, n:int = subMeshList.length; i < n; i++) {
				subMesh = subMeshList[i];
				if ((srcMesh = (subMesh as Mesh)) != null) {
					var mesh:Mesh = pool.popObject(pool.ClassMesh);
					mesh.hasDisposed = false;
					mesh.isValid = true;
					mesh.setForceVisible(true);
					mesh.initFromMeshLoader(srcMesh.meshLoader, srcMesh.dataIndex);
					
					ret.addSubMesh(mesh);
				} else if ((srcSkin = (subMesh as Skin)) != null){
					var skin:Skin = pool.popObject(pool.ClassSkin);
					skin.hasDisposed = false;
					skin.isValid = true;
					skin.skeleton = this.skeleton;
					skin.setForceVisible(true);
					skin.initFromMeshLoader(srcSkin.meshLoader, srcSkin.dataIndex);
					ret.addSubMesh(skin);
				}
			}
			
			if (this.meshLoaders != null) {
				ret.meshLoaders = new Dictionary
				for (var loader:*  in this.meshLoaders) {
					loader.addRef();
					ret.meshLoaders[loader] = loader;
				}
			}
			
			ret.loadedCount = 0;
			return ret;
		}
		
		private function addSubMesh(subMesh:Entity):void
		{
			for (var i:int = 0, n:int = subMeshList.length; i < n; i++) {
				if (subMeshList[i]== null) {
					subMeshList[i] = subMesh;
					return;
				}
			}
			this.subMeshList.push(subMesh);
		}
		
		public function addPiece(fileName:String, userData:int, useLight:Boolean, 
								 recieveShadow:Boolean, materialType:int, uvContext:Object,
								 insertFirst:Boolean, moveToHighPriority:Boolean = false):void
		{
			for (var k:* in meshLoaders) {
				if (k.userData == userData) {
					if (loadedCount > 0) {
						loadedCount--;
					}
					k.delListener(onMeshLoaded);
					k.release();
					delete meshLoaders[k];
					break;
				}
			}
			
			loadedCount++;
			var mesh:MeshLoader = MeshLoader.loadMesh(fileName, userData, useLight, recieveShadow, materialType, onMeshLoaded, uvContext, insertFirst, false, moveToHighPriority);
			if (meshLoaders == null) {
				meshLoaders = new Dictionary;
			}
			meshLoaders[mesh] = mesh;
		}
		
		public function setAllMaterialAlpha(alpha:Number):void
		{
			var mat:Material;
			for each(var subMesh:Entity in this.subMeshList) {
				if (subMesh != null) {
					mat = subMesh.getMaterial();
					if (mat != null) {
						if (alpha < 1.0) {
							mat.setAlpha(alpha);
							mat.setBlend(Defines.BLEND_SOURCE_ALPHA, Defines.BLEND_ONE_MINUS_SOURCE_ALPHA);
							mat.setRenderPriority(Defines.PRIORITY_AFTER_EFFECT);
						} else {
							mat.setAlpha(1);
							mat.setBlend(Defines.BLEND_ONE, Defines.BLEND_ZERO);
							mat.setRenderPriority(Defines.PRIORITY_OPAQUE);
						}
					}
				}
			}
		}
	}
}
