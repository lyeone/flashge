// 2014-3-10 lye

package FlashGE.objects
{
	import flash.geom.Vector3D;
	
	import FlashGE.FlashGE;
	import FlashGE.common.AxisAlignedBox;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.common.Pos3D;
	import FlashGE.common.Transform3D;
	import FlashGE.effects.Effect;
	import FlashGE.effects.KeyPair;
	import FlashGE.materials.Material;
	import FlashGE.materials.ShaderProgram;
	import FlashGE.materials.compiler.Linker;
	import FlashGE.materials.compiler.Procedure;
	import FlashGE.render.Camera3D;
	import FlashGE.render.RenderUnit;
	
	use namespace FlashGE;
	
	public class Entity
	{
		protected var material:Material;
		
		FlashGE var useLights:Boolean = false;
		FlashGE var visible:Boolean = true;
		FlashGE var name:String;
		FlashGE var forceVisible:Boolean = false;
		FlashGE var boundBox:AxisAlignedBox;
		
		FlashGE var x:Number = 0;
		FlashGE var y:Number = 0;
		FlashGE var z:Number = 0;
		FlashGE var rotationX:Number = 0;
		FlashGE var rotationY:Number = 0;
		FlashGE var rotationZ:Number = 0;
		FlashGE var scaleX:Number = 1;
		FlashGE var scaleY:Number = 1;
		FlashGE var scaleZ:Number = 1;
		FlashGE var parent:Entity;
		FlashGE var childrenList:Entity;
		FlashGE var next:Entity;
		FlashGE var transform:Transform3D = new Transform3D();
		FlashGE var inverseTransform:Transform3D = new Transform3D();
		FlashGE var transformChanged:Boolean = true;
		FlashGE var cameraToLocalTransform:Transform3D = new Transform3D();
		FlashGE var localToCameraTransform:Transform3D = new Transform3D();
		//FlashGE var localToCameraChanged:Boolean = true;
		FlashGE var culledType:int = -1;
		FlashGE var transformProcedure:Procedure;
		FlashGE var positionProcedure:Procedure;
		FlashGE var transformReciveShadowProcedure:Procedure;
		FlashGE var programKey:String;
		FlashGE var userData:int = -1;
		FlashGE var isValid:Boolean = true;
		
		FlashGE var refCountLinked:int = 0;
		public function getTransform():Transform3D
		{
			return this.transform;
		}
		
		public function getLocalToCamera():Transform3D
		{
			return this.localToCameraTransform;
		}
		
		public function getCameraToLocal():Transform3D
		{
			return this.cameraToLocalTransform;
		}	
		
		FlashGE var boundBoxScaled:AxisAlignedBox;
		
		public function getInterpolationValue(percent:Number, keyPairs:Vector.<KeyPair>, outValue:KeyPair):Boolean
		{
			var len:uint = keyPairs.length;
			var r:Number, j:Number;
			var item:Vector.<Number>;
			var n:uint;
			for (var i:uint = 1; i < len; i++){
				if( percent >= (keyPairs[i-1].time / 100.0) &&
					percent <= (keyPairs[i].time / 100.0) ) {
					var curPercent:Number = (percent*100 - keyPairs[i-1].time) / 
						(keyPairs[i].time - keyPairs[i-1].time);
					n = keyPairs[i].items.length;
					item = keyPairs[i].items;
					if (outValue.items.length != n) {
						outValue.items.length = n;
					}
					for (j=0; j<n; j++){
						outValue.items[j] = ( keyPairs[i-1].items[j] + 
							(keyPairs[i].items[j] - keyPairs[i-1].items[j] ) * curPercent);
					}
					return false;
				}
			}
			if (len > 0) {
				var tmp:KeyPair = keyPairs[len-1];
				if ( percent*100 < tmp.time ) {
					tmp = keyPairs[0];
				}
				item = tmp.items;
				n = item.length;
				if (outValue.items.length != n) {
					outValue.items.length = n;
				}
				for (j=0; j<n; j++){
					outValue.items[j] = item[j];
				}
			}
			
			return true;
		}	
		
		public function getBoundBox():AxisAlignedBox
		{
			if (this.boundBox==null) return null;
			
			if (boundBoxScaled==null) {
				boundBoxScaled = new AxisAlignedBox;
			}
			
			boundBoxScaled.minX = this.boundBox.minX;
			boundBoxScaled.minY = this.boundBox.minY;
			boundBoxScaled.minZ = this.boundBox.minZ;
			boundBoxScaled.maxX = this.boundBox.maxX;
			boundBoxScaled.maxY = this.boundBox.maxY;
			boundBoxScaled.maxZ = this.boundBox.maxZ;
			boundBoxScaled.scale(this.scaleX, this.scaleY, this.scaleZ);
			
			return this.boundBoxScaled;	
		}
		
		
		public function setUserData(v:int):void
		{
			this.userData = v;	
		}
		
		public function getUserData():int
		{
			return this.userData;
		}
		
		public function getMaterial():Material
		{
			return this.material;
		}
		
		public function setMaterial(src:Material):void
		{
			if (this.material != null) {
				this.material.release();
				this.material = null;
			}
			
			if (src != null) {
				this.material = src;
				this.material.markProgramDirty();
				this.material.addRef();				
			}
		}
		
		public function setVisible(v:Boolean):void
		{
			this.visible = v;
		}
		
		public function isVisible():Boolean
		{
			return this.visible;
		}
		
		public function setForceVisible(visible:Boolean):void
		{
			this.forceVisible = visible;	
		}
		
		public function getLinkedNode():Entity
		{
			Global.throwError("Entity::getLinkedNode invalid operation.");
			return null;
		}
		
		public function getLinkedHolder():Entity
		{
			Global.throwError("Entity::getLinkedHolder invalid operation.");
			return null;
		}
		
		public function setLinkedNode(node:Entity, holder:Entity):void
		{
			Global.throwError("Entity::setLinkedNode invalid operation.");
		}
		
		public function isCulled():Boolean
		{
			return this.culledType <0;
		}
				
		public function getName():String
		{
			return this.name;
		}
		
		public function setName(v:String):void
		{
			this.name = v;
		}
		
		public function getX():Number 
		{
			return x;
		}
		
		public function setX(value:Number):void
		{
			if (x != value) {
				x = value;
				transformChanged = true;
			}
		}
		
		public function getY():Number 
		{
			return y;
		}
		
		public function setY(value:Number):void
		{
			if (y != value) {
				y = value;
				transformChanged = true;
			}
		}
		
		public function getZ():Number 
		{
			return z;
		}
		
		public function setZ(value:Number):void 
		{
			if (z != value) {
				z = value;
				transformChanged = true;
			}
		}
		
		
		public function setPosition(x:Number, y:Number, z:Number):void
		{
			if (this.transformChanged==false) {
				this.transformChanged = this.x != x || this.y != y || this.z != z;
			}
			this.x = x;
			this.y = y;
			this.z = z;
		}
		
		
		public function setPositionOffset(dx:Number, dy:Number, dz:Number):void
		{
			this.setPosition(x+dx, y+dy, z+dz);
		}
		
		public function getPosition(out:Vector3D):void
		{
			out.setTo(this.x, this.y, this.z);	
		}
		
		public function getScale(out:Vector3D):void
		{
			out.setTo(this.scaleX, this.scaleY, this.scaleZ);	
		}
		
		public function getRotation(out:Vector3D):void
		{
			out.setTo(this.rotationX, this.rotationY, this.rotationZ);	
		}
		
		public function setRotation(x:Number, y:Number, z:Number):void
		{
			if (this.transformChanged==false) {
				this.transformChanged = this.rotationX != x || this.rotationY != y || this.rotationZ != z;
			}
			this.rotationX = x;
			this.rotationY = y;
			this.rotationZ = z;
		}
		
		public function getRotationX():Number
		{
			return rotationX;
		}
		
		public function setRotationX(value:Number):void
		{
			if (rotationX != value) {
				rotationX = value;
				transformChanged = true;
			}
		}
		
		public function getRotationY():Number 
		{
			return rotationY;
		}
		
		public function setRotationY(value:Number):void 
		{
			if (rotationY != value) {
				rotationY = value;
				transformChanged = true;
			}
		}
		
		public function getRotationZ():Number {
			return rotationZ;
		}
		
		public function setRotationZ(value:Number):void {
			if (rotationZ != value) {
				rotationZ = value;
				transformChanged = true;
			}
		}
		
		public function setScale(x:Number, y:Number, z:Number):void
		{
			if (this.transformChanged==false) {
				this.transformChanged = this.scaleX != x || this.scaleY != y || this.scaleZ != z;
			}
			this.scaleX = x;
			this.scaleY = y;
			this.scaleZ = z;
		}
		
		public function getScaleX():Number 
		{
			return scaleX;
		}
		
		public function setScaleX(value:Number):void 
		{
			if (scaleX != value) {
				scaleX = value;
				transformChanged = true;
			}
		}
		
		public function getScaleY():Number
		{
			return scaleY;
		}
		
		public function setScaleY(value:Number):void
		{
			if (scaleY != value) {
				scaleY = value;
				transformChanged = true;
			}
		}
		
		public function getScaleZ():Number
		{
			return scaleZ;
		}
		
		public function setScaleZ(value:Number):void
		{
			if (scaleZ != value) {
				scaleZ = value;
				transformChanged = true;
			}
		}
		
		public function getRadian(dirA:Number, dirB:Number):Number
		{
			var r:Number = 0;
			if (dirA >0 && dirB >0) {
				r = Math.asin(dirB);
				return (r+Math.PI/2);
			} else if(dirA < 0 && dirB>0) {
				r = Math.asin(dirB);
				return (Math.PI*3/2 - r);
			} else if(dirA < 0 && dirB <0) {
				r = Math.asin(-dirB);
				return (Math.PI*3/2 + r);
			} else if(dirA > 0 && dirB < 0) {
				r = Math.asin(-dirB);
				return (Math.PI/2-r);
			} else if(dirA==0&&dirB!=0){
				if (dirB > 0) return (Math.PI);
				else return (0);
			} else if(dirA!=0&&dirB==0) {
				if (dirA >0) return (Math.PI/2);
				else return (-Math.PI/2);
			}
			return (0);
		}
		
		protected var dirTransform:Transform3D;
		public function setDir(x:Number, y:Number, z:Number):void
		{
			if (this.dirTransform == null) {
				this.dirTransform = new Transform3D;
			}
			if (y == 1) {
				
				dirTransform.fromAxisAngle(0, 0, 1, Math.PI);
				this.transformChanged = true;
				
			} else {
				
				var pool:Pool = Pool.getInstance();
				var va:Vector3D = pool.popVector3D();
				va.setTo(0, -1, 0);
				var vb:Vector3D = pool.popVector3D();
				vb.setTo(x, y, z);
				
				var vc:Vector3D = va.crossProduct(vb);
				
				var dot:Number = Math.acos(-y);
				
				dirTransform.fromAxisAngle(vc.x, vc.y, vc.z, dot);
				this.transformChanged = true;
				
				pool.pushVector3D(va);
				pool.pushVector3D(vb);
			}
		}
		
		FlashGE function updateBoundBox(boundBox:AxisAlignedBox, transform:Transform3D = null):void
		{
		}
		
		public function getParent():Entity
		{
			return parent;
		}
		
		public function removeFromParent():void
		{
			if (parent != null) {
				parent.removeFromList(this);
				parent = null;
			}
		}
		
		public function addChild(child:Entity):Entity
		{
			if (child == null) throw new TypeError("Parameter child must be non-null.");
			if (child == this) throw new ArgumentError("An object cannot be added as a child of itself.");
			for (var container:Entity = parent; container != null; container = container.parent) {
				if (container == child) throw new ArgumentError("An object cannot be added as a child to one of it's children (or children's children, etc.).");
			}
			
			if (child.parent != this) {
				
				if (child.parent != null) child.parent.removeChild(child);
				
				addToList(child);
				child.parent = this;
			} else {
				child = removeFromList(child);
				if (child == null) throw new ArgumentError("Cannot add child.");
				addToList(child);
			}
			return child;
		}
		
		public function removeChildsByType(cls:Class):void
		{
			var childs:Vector.<Entity>;
			for (var current:Entity = childrenList; current != null; current = current.next) {
				if (current is cls) {
					childs = childs || new Vector.<Entity>;
					childs.push(current);
				}
			}
			
			if (childs == null) return;
			
			for (var i:int = 0, n:int = childs.length; i < n; i++) {
				removeChild(childs[i]);
				if (childs[i].hasOwnProperty("dispose")) {
					(childs[i] as Object).dispose();
				}
			}
		}
		
		
		public function removeChild(child:Entity):Entity
		{
			if (child == null) throw new TypeError("Parameter child must be non-null.");
			if (child.parent != this) throw new ArgumentError("The supplied Object3D must be a child of the caller.");
			child = removeFromList(child);
			if (child == null) throw new ArgumentError("Cannot remove child.");
			child.parent = null;
			return child;
		}
		
		public function getChildByName(name:String):Entity
		{
			for (var child:Entity = childrenList; child != null; child = child.next) {
				if (child.name == name) {
					return child;
				} else {
					var ret:Entity = child.getChildByName(name);
					if (ret !=null)
						return child;
				}
			}
			
			return null;
		}
		
		public function contains(child:Entity):Boolean
		{
			if (child == this) {
				return true;
			}
			
			for (var object:Entity = childrenList; object != null; object = object.next) {
				if (object.contains(child)) {
					return true;
				}
			}
			
			return false;
		}
				
		protected function addToList(child:Entity):void
		{
			child.next = null;
			if (null == childrenList) {
				childrenList = child;
				return;
			} 
			
			for (var current:Entity = childrenList; current != null; current = current.next) {
				if (current.next == null) {
					current.next = child;
					return;
				}
			}			
		}
		
		protected function removeFromList(child:Entity):Entity
		{
			var prev:Entity;
			for (var current:Entity = childrenList; current != null; current = current.next) {
				if (current == child) {
					if (prev != null) {
						prev.next = current.next;
					} else {
						childrenList = current.next;
					}
					current.next = null;
					return child;
				}
				prev = current;
			}
			return null;
		}
		
		FlashGE function composeTransforms(calcInverse:Boolean = true):void
		{
			if (this.dirTransform == null) {
				
				// Matrix
				var cosX:Number = Math.cos(rotationX);
				var sinX:Number = Math.sin(rotationX);
				var cosY:Number = Math.cos(rotationY);
				var sinY:Number = Math.sin(rotationY);
				var cosZ:Number = Math.cos(rotationZ);
				var sinZ:Number = Math.sin(rotationZ);
				var cosZsinY:Number = cosZ*sinY;
				var sinZsinY:Number = sinZ*sinY;
				var cosYscaleX:Number = cosY*scaleX;
				var sinXscaleY:Number = sinX*scaleY;
				var cosXscaleY:Number = cosX*scaleY;
				var cosXscaleZ:Number = cosX*scaleZ;
				var sinXscaleZ:Number = sinX*scaleZ;
				transform.a = cosZ*cosYscaleX;
				transform.b = cosZsinY*sinXscaleY - sinZ*cosXscaleY;
				transform.c = cosZsinY*cosXscaleZ + sinZ*sinXscaleZ;
				transform.d = x;
				transform.e = sinZ*cosYscaleX;
				transform.f = sinZsinY*sinXscaleY + cosZ*cosXscaleY;
				transform.g = sinZsinY*cosXscaleZ - cosZ*sinXscaleZ;
				transform.h = y;
				transform.i = -sinY*scaleX;
				transform.j = cosY*sinXscaleY;
				transform.k = cosY*cosXscaleZ;
				transform.l = z;
				if (calcInverse) {
					var sinXsinY:Number = sinX*sinY;
					cosYscaleX = cosY/scaleX;
					cosXscaleY = cosX/scaleY;
					sinXscaleZ = -sinX/scaleZ;
					cosXscaleZ = cosX/scaleZ;
					inverseTransform.a = cosZ*cosYscaleX;
					inverseTransform.b = sinZ*cosYscaleX;
					inverseTransform.c = -sinY/scaleX;
					inverseTransform.d = -inverseTransform.a*x - inverseTransform.b*y - inverseTransform.c*z;
					inverseTransform.e = sinXsinY*cosZ/scaleY - sinZ*cosXscaleY;
					inverseTransform.f = cosZ*cosXscaleY + sinXsinY*sinZ/scaleY;
					inverseTransform.g = sinX*cosY/scaleY;
					inverseTransform.h = -inverseTransform.e*x - inverseTransform.f*y - inverseTransform.g*z;
					inverseTransform.i = cosZ*sinY*cosXscaleZ - sinZ*sinXscaleZ;
					inverseTransform.j = cosZ*sinXscaleZ + sinY*sinZ*cosXscaleZ;
					inverseTransform.k = cosY*cosXscaleZ;
					inverseTransform.l = -inverseTransform.i*x - inverseTransform.j*y - inverseTransform.k*z;
				}
			} else {
				this.transform.compose(0,0,0,0,0,0,this.scaleX, this.scaleY,this.scaleZ);
				this.transform.append(this.dirTransform);
				this.transform.d = this.x;
				this.transform.h = this.y;
				this.transform.l = this.z;
				if (calcInverse) {
					this.inverseTransform.copy(this.transform);
					this.inverseTransform.invert();
				}
			}
			transformChanged = false;			
		}
		
		FlashGE function calculateVisibility(camera:Camera3D):void
		{
		}
		
		FlashGE var ignoreCalcVisibility:Boolean = false;
		FlashGE var forceCalcChildren:Boolean = false;
		
		public function setIgnoreCalcVisibility(ignore:Boolean):void
		{
			this.ignoreCalcVisibility = ignore;	
		}
		
		public function setForceCalcChildren(calc:Boolean):void
		{
			this.forceCalcChildren = calc;	
		}
		
		FlashGE function calculateChildrenVisibility(camera:Camera3D):void
		{
			for (var child:Entity = childrenList; child != null; child = child.next) {
				if (child.visible && (child.ignoreCalcVisibility == false || this.forceCalcChildren)) {

					if (child.transformChanged) {
						child.composeTransforms();
						//child.localToCameraChanged = true;
					}
					
					//if (this.localToCameraChanged || child.localToCameraChanged) {
						child.cameraToLocalTransform.combine(child.inverseTransform, cameraToLocalTransform);
						child.localToCameraTransform.combine(localToCameraTransform, child.transform);
					//}
					
					if (child.forceVisible) {
						child.culledType = 63;
					} else {
						if (child.boundBox != null) {
							camera.calculateFrustum(child.cameraToLocalTransform);
							child.culledType = child.boundBox.checkFrustumCulling(camera.frustum, 63);
						} else {
							child.culledType = 63;
						}
					}
					
					if (child.culledType >= 0) {
						child.calculateVisibility(camera);
					}
					
					child.calculateChildrenVisibility(camera);
					//child.localToCameraChanged = false;
				}
			}
			//this.localToCameraChanged = false
		}
		
		
		FlashGE function collectRenderUnits(camera:Camera3D, isShadowPass:Boolean):int 
		{
			return 0;
		}
		
		FlashGE function collectChildrenRenderUnits(camera:Camera3D, isShadowPass:Boolean):int 
		{
			var renderCount:int = 0;
			var child:Entity = childrenList;
			while (child!=null) {
				var next:Entity = child.next;
				
				if (child.visible) {
					
					if (child.culledType >= 0) {
						renderCount += child.collectRenderUnits(camera, isShadowPass);
					}
					
					if (child.culledType !=-2) {
						renderCount += child.collectChildrenRenderUnits(camera, isShadowPass);
					}
				}
				
				child = next;
			}
			return renderCount;
		}
		
		public function setIgnoreScale(v:Boolean):void
		{
			Global.throwError("Entity::setIgnoreScale invalid operation!");
		}
		
		public function isOnlyFollowPos():Boolean
		{
			return false;
		}
		
		public function isIgnoreScale():Boolean
		{
			return false;
		}
		
		public function setOnlyFollowPos(value:Boolean):void
		{
			Global.throwError("Entity::setOnlyFollowPos invalid operation!");
		}
		
		FlashGE function setTransformConstants(renderUnit:RenderUnit, 
											   surface:Surface, 
											   shader:ShaderProgram,
											   camera:Camera3D, 
											   setContext:uint = 0xffffffff):void
		{
		}
		
		FlashGE function calcProgramKey(camera:Camera3D):void
		{
			this.programKey = "entity";
		}
		
		public function getNodeByName(nodeName:String):Entity
		{
			return getChildByName(nodeName);
		}
		
		public function resetMaterials():void
		{
		}
		
		public function removeLinkObject(object:Entity):void
		{}
		
		public function linkObject(linkedName:String, object:Entity, aniFrame:int):Boolean
		{
			return false;
		}
		
		public function switchShowByType(className:Class, visible:Boolean):void
		{
			for (var o:Entity = this.childrenList; o!=null; o = o.next) {
				if ( o is className ) {
					o.visible = visible; 
				}
			}
		}
		
		FlashGE function appendPositionTransformProcedure(vertexShader:Linker):String
		{
			vertexShader.declareVariable("tTransformedPosition");
			vertexShader.addProcedure(this.positionProcedure);
			vertexShader.setInputParams(this.positionProcedure, "aPosition");
			vertexShader.setOutputParams(this.positionProcedure, "tTransformedPosition");
			return "tTransformedPosition";
		}
		
		public function localToGlobal(point:Pos3D):void
		{
			if (transformChanged) {
				composeTransforms();
			}
			var pool:Pool = Pool.getInstance();
			var temp:Transform3D = pool.popTransform3D();
			temp.copy(transform);
			var root:Entity = this;
			while (root.parent != null) {
				root = root.parent;
				if (root.transformChanged) 
					root.composeTransforms();
				temp.append(root.transform);
			}
			
			var outX:Number = temp.a*point.x + temp.b*point.y + temp.c*point.z + temp.d;
			var outY:Number = temp.e*point.x + temp.f*point.y + temp.g*point.z + temp.h;
			var outZ:Number = temp.i*point.x + temp.j*point.y + temp.k*point.z + temp.l;
			pool.pushTransform3D(temp);
			point.x = outX;
			point.y = outY;
			point.z = outZ;
		}
		
		public function globalToLocal(point:Vector3D):Vector3D 
		{
			var pool:Pool = Pool.getInstance();
			var temp:Transform3D = pool.popTransform3D();
			if (transformChanged) {
				composeTransforms();
			}
			temp.copy(inverseTransform);
			var root:Entity = this;
			while (root.parent != null) {
				root = root.parent;
				if (root.transformChanged) root.composeTransforms();
				temp.prepend(root.inverseTransform);
			}
			var res:Vector3D = pool.popVector3D();
			res.x = temp.a*point.x + temp.b*point.y + temp.c*point.z + temp.d;
			res.y = temp.e*point.x + temp.f*point.y + temp.g*point.z + temp.h;
			res.z = temp.i*point.x + temp.j*point.y + temp.k*point.z + temp.l;
			pool.pushVector3D(res);
			pool.pushTransform3D(temp);
			return res;
		}
		
		public function setLinkedTransform(tempTrans:Transform3D):void
		{
			// TODO Auto Generated method stub
		}
		
		public function onUnlinkedObject(param0:Entity, linkedNode:Entity):void
		{
			// TODO Auto Generated method stub
			
		}
		
		public function onLinkedObject(param0:Entity, linkedNode:Entity):void
		{
			// TODO Auto Generated method stub
			
		}
		
		public function clearRenderUnits():void
		{
			// TODO Auto Generated method stub
			
		}
	}
}
