// 2014-3-10 lye
package FlashGE.objects
{
	import flash.display3D.Context3D;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Context3DVertexBufferFormat;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.Program3D;
	import flash.display3D.VertexBuffer3D;
	import flash.display3D.textures.TextureBase;
	import flash.utils.ByteArray;
	
	import FlashGE.FlashGE;
	import FlashGE.common.AxisAlignedBox;
	import FlashGE.common.Colour;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.materials.Material;
	import FlashGE.materials.compiler.Procedure;
	import FlashGE.render.Camera3D;
	import FlashGE.render.RenderUnit;
	import FlashGE.render.Renderer;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;
	
	public class RectSprite extends Mesh
	{
		static private var program3Ds:Vector.<Program3D> = new Vector.<Program3D>;
		static private var programShadow3Ds:Vector.<Program3D> = new Vector.<Program3D>;
		static private var vertexByteCodeShadow:Vector.<ByteArray> = new Vector.<ByteArray>;
		static private var fragmentByteCodeShadow:Vector.<ByteArray> = new Vector.<ByteArray>;
		static private var vertexByteCode:Vector.<ByteArray> = new Vector.<ByteArray>;
		static private var fragmentByteCode:Vector.<ByteArray> = new Vector.<ByteArray>;
		
		static private var vertexBuffer3D:VertexBuffer3D;
		static private var indexBuffer3D:IndexBuffer3D;
		static private var usedContext3D:Context3D;
		
		private var autoMatchTextureSize:Boolean = true;
		private var width:Number = 1;
		private var height:Number = 1;
		
		private var autoCenterHeight:Boolean = true;
		private var color:Colour;
		private var alphaColor:Colour;
		
		private var usedContext3D:Context3D;
		private var curVertexBuffer3D:VertexBuffer3D;
		private var curIndexBuffer3D:IndexBuffer3D;
		private var curProgram3D:Program3D;
		private var curRenderUnit:RenderUnit;
		private var curNeedReset:Boolean = true;
		
		static public function create(autoCenterHeight:Boolean = true):RectSprite
		{
			var ret:RectSprite = new RectSprite;
			ret.autoCenterHeight = autoCenterHeight;
			return ret;
		}
		
		override public function dispose():void
		{
			super.dispose();			
			if (curRenderUnit != null) {
				curRenderUnit.release();
				curRenderUnit = null;
			}
		}
		
		public function changeSize(width:Number, height:Number):void
		{
			if (this.width != width && this.height!=height) {
				boundBox = null;
			}
			this.width = width;
			this.height = height;
			curNeedReset = true;
		}
		
		public function setAlphaColor(r:Number, g:Number, b:Number, a:Number):void
		{
			if (this.alphaColor == null) this.alphaColor = new Colour;
			this.alphaColor.setRGBA(r, g, b, a);
		}
		
		public function setColor(r:Number, g:Number, b:Number, a:Number):void
		{
			if (this.color == null) this.color = new Colour;
			this.color.setRGBA(r, g, b, a);
		}
		
		public function setTexture(tex:TextureFile, autoMatchTextSize:Boolean = true):void
		{
			this.boundBox = null;
			if (this.material == null) {
				this.material = Material.createMaterial();
			}
			this.autoMatchTextureSize = autoMatchTextSize;
			this.material.setTexture(0, tex);
			this.curNeedReset = true;
		}
		
		public function getTexture():TextureFile
		{
			return this.material != null ? this.material.getTexture(0) : null;
		}
		
		
		private function getVertexBuffer3D(context3D:Context3D):VertexBuffer3D
		{
			if (RectSprite.vertexBuffer3D != null) {
				return RectSprite.vertexBuffer3D;
			}
			
			var vertsBuffer:Vector.<Number> = new <Number>[
				-0.5,0,-0.5,0,1,
				-0.5,0, 0.5,0,0,
				 0.5,0, 0.5,1,0,
				 0.5,0,-0.5,1,1
			];
			
			var vertexBuffer:VertexBuffer3D;
			try {
				vertexBuffer = context3D.createVertexBuffer(4, 5);
				CONFIG::DEBUG {
					Global.vertexBuffersCount++;
				}
				vertexBuffer.uploadFromVector(vertsBuffer, 0, 4);
			} catch(e:Error) {
				if (vertexBuffer != null) {
					vertexBuffer.dispose();
					CONFIG::DEBUG {
						Global.vertexBuffersCount--;
					}
				}
				
				if (e.errorID == 3672) {
					Global.needDisposeContext3d = true;
				}
				CONFIG::DEBUG {
					Global.warning("RectSprite::getVertexBuffer3D" + e.errorID + "," + e.message);
				}
				return null;
			}
			
			if (RectSprite.vertexBuffer3D != null) {
				RectSprite.vertexBuffer3D.dispose();
				CONFIG::DEBUG {
					Global.vertexBuffersCount--;
				}
			}
			RectSprite.vertexBuffer3D = vertexBuffer;
			return vertexBuffer;
		}
		
		private function getIndexBuffer3D(context3D:Context3D):IndexBuffer3D
		{
			if (RectSprite.indexBuffer3D != null) {
				return RectSprite.indexBuffer3D;
			}
			
			var indicesBuffer:Vector.<uint> = new <uint>[
				0,1,3,2,3,1
			]
			
			var indexBuffer:IndexBuffer3D;
			
			try {
				indexBuffer = context3D.createIndexBuffer(6);
				CONFIG::DEBUG {
					Global.indexBuffersCount++;
				}
				indexBuffer.uploadFromVector(indicesBuffer, 0, 6);
			} catch (e:Error) {
				
				if (e.errorID == 3672) {
					Global.needDisposeContext3d = true;
				}
				if (indexBuffer != null) {
					indexBuffer.dispose();
					CONFIG::DEBUG {
						Global.indexBuffersCount--;
					}
				}
				return null;
			}
			
			if (RectSprite.indexBuffer3D != null) {
				RectSprite.indexBuffer3D.dispose();
				
				CONFIG::DEBUG {
					Global.indexBuffersCount--;
				}
			}
			RectSprite.indexBuffer3D = indexBuffer;
			
			return indexBuffer;
		}
		
		private function getPrograme3D(context3D:Context3D, index:int, hasShadow:Boolean):Program3D
		{
			var programes:Vector.<Program3D> = hasShadow ? RectSprite.programShadow3Ds : RectSprite.program3Ds;
			var curVertexByteCodes:Vector.<ByteArray> = hasShadow ? RectSprite.vertexByteCodeShadow : RectSprite.vertexByteCode;
			var curFragmentByteCodes:Vector.<ByteArray> = hasShadow ? RectSprite.fragmentByteCodeShadow : RectSprite.fragmentByteCode;
			if (index >= programes.length) {
				programes.length = index + 1;
				curVertexByteCodes.length = index + 1;
				curFragmentByteCodes.length = index + 1;
			}
			
			var programPass:Program3D = programes[index];
			if (programPass != null) {
				return programPass;
			}
			
			var curVertexByteCode:ByteArray = curVertexByteCodes[index];
			var curFragmentByteCode:ByteArray = curFragmentByteCodes[index];
			if (curVertexByteCode == null || curFragmentByteCode == null) {
				var vertexProgram:Array = [
					"mul t0.x, a0.x, c0.x",
					"mov t0.y, a0.y",
					"mul t0.z, a0.z, c0.y",
					"mov t0.w, a0.w",
					"add t0.z, t0.z, c0.z",
					"m44 o0, t0, c2",
					
					"mov t0, a1",
					"mul t0.xy, t0.xy, c1.zw",
					"mov v0, t0"
				];
				
				var vertexProgramShadow:Array = [
					"mul t0.x, a0.x, c0.x",
					"mov t0.y, a0.y",
					"mul t0.z, a0.z, c0.y",
					"mov t0.w, a0.w",
					"add t0.z, t0.z, c0.z",
					"m44 o0, t0, c2",
					"m34 v1.xyz, t0, c6",
					
					"mov v1.w, a0.w",
					"mov t0, a1",
					"mul t0.xy, t0.xy, c1.zw",
					"mov v0, t0"
				];
				
				var fragmentProgram:Array = [
					"tex t0, v0, s0 <2d,clamp,linear, miplinear," + Defines.TEX_FORMATS[index] + ">",
					"mul t0, t0, c0",
					
					"sge t1.x, t0.x, t0.x",
					"sub t1.x, t1.x, c1.w",
					"mov t1.y, c1.w",
					"mul t0.xyz, t0.xyz, t1.xxx",
					"mul t1, t1.yyyy, c1",
					
					"add t0.xyz, t0.xyz, t1.xyz",
					"mov o0, t0"
				];
				
				var fragmentProgramShadow:Array = [
					"tex t1, v0, s0 <2d,clamp,linear, miplinear," + Defines.TEX_FORMATS[index] + ">",
					"mul t1, t1, c2",
					
					"mov t0.zw, v1.zz",
					"tex t0.xy, v1, s1 <2d, clamp, near, nomip>",
					"dp3 t0.x, t0.xyz, c0.xyz",
					"sub t0.y, c1.x, t0.z",
					"mul t0.y, t0.y, c1.y",
					"sat t0.xy, t0.xy",
					"mul t0.x, t0.x, t0.y",
					"sub t0.x, c1.z, t0.x",
					"max t0.x, t0.x, c1.w",
					"mul t0, t1, t0.x",
					
					"sge t1.x, t0.x, t0.x",
					"sub t1.x, t1.x, c3.w",
					"mov t1.y, c3.w",
					"mul t0.xyz, t0.xyz, t1.xxx",
					"mul t1, t1.yyyy, c3",
					
					"add t0.xyz, t0.xyz, t1.xyz",
					"mov o0, t0"
				];
				
				var proc:Procedure;
				
				proc = new Procedure(hasShadow ? vertexProgramShadow : vertexProgram);
				curVertexByteCode = proc.getByteCode(Context3DProgramType.VERTEX);
				
				proc = new Procedure(hasShadow ? fragmentProgramShadow : fragmentProgram);
				curFragmentByteCode = proc.getByteCode(Context3DProgramType.FRAGMENT);
				
				curVertexByteCodes[index] = curVertexByteCode;
				curFragmentByteCodes[index] = curFragmentByteCode;
			}
			
			programPass = programes[index] = context3D.createProgram();
			programPass.upload(curVertexByteCode, curFragmentByteCode);
			return programPass;
		}
		
		override FlashGE function collectRenderUnits(camera:Camera3D, isShadowPass:Boolean):int 
		{
			var hasShadow:Boolean = camera.shadowCalculor!=null && camera.shadowCalculor.shadowMapFile != null;
			
			var mat:Material = this.getMaterial();
			var tex:TextureFile;
			if (mat == null || (tex = mat.getTexture(0)) == null || tex.getLoadState() != Defines.LOAD_OK) {
				return 0;
			}
			
			var texAlignWidth:int = tex.getAlignWidth();
			var texAlignHeight:int = tex.getAlignHeight();
			var texWidth:int = tex.getRealWidth();
			var texHeight:int = tex.getRealHeight();
			
			if (this.autoMatchTextureSize) {
				this.changeSize(texWidth, texHeight);
				this.autoMatchTextureSize = false;
			}
			
			var texture3D:TextureBase = tex.getTexture(camera.context3D);
			if (texture3D == null) {
				return 0;
			}
			
			var context3D:Context3D = camera.context3D;
			if (RectSprite.usedContext3D != context3D) {
				
				RectSprite.usedContext3D = context3D;
				
				if (RectSprite.indexBuffer3D != null) {
					RectSprite.indexBuffer3D.dispose();
					RectSprite.indexBuffer3D = null;
					
					CONFIG::DEBUG {
						Global.indexBuffersCount--;
					}
				}
				
				if (RectSprite.vertexBuffer3D !=null) {
					RectSprite.vertexBuffer3D.dispose();
					RectSprite.vertexBuffer3D = null;
					CONFIG::DEBUG {
						Global.vertexBuffersCount--;
					}
				}
				var i:int;
				var n:int;
				var program3DsList:Vector.<Program3D> = RectSprite.programShadow3Ds;
				if (program3Ds != null) {
					for (i = 0, n = program3DsList.length; i < n; i++) {
						if (program3DsList[i] != null) {
							program3DsList[i].dispose();
							program3DsList[i] = null;
						}
					}
				}
				
				program3DsList = RectSprite.program3Ds;
				if (program3DsList != null) {
					for (i = 0, n = program3DsList.length; i < n; i++) {
						if(program3DsList[i] != null) {
							program3DsList[i].dispose();
							program3DsList[i] = null;
						}
					}
				}
			}
			
			var context3DChanged:Boolean = (this.usedContext3D != context3D);
			var texFormatID:int = tex.getFormatID();
			if (this.curProgram3D == null || context3DChanged) {
				this.curProgram3D = this.getPrograme3D(context3D, texFormatID, hasShadow);
			}
			
			if (this.curIndexBuffer3D == null || context3DChanged) {
				this.curIndexBuffer3D = this.getIndexBuffer3D(context3D);
			}
			
			if (this.curVertexBuffer3D == null || context3DChanged) {
				this.curVertexBuffer3D = this.getVertexBuffer3D(context3D);
			}
			
			if (this.curProgram3D == null || this.curIndexBuffer3D == null || this.curVertexBuffer3D == null) {
				return 0;
			}
			
			this.usedContext3D = context3D;
			
			if (boundBox == null) {
				boundBox = new AxisAlignedBox;
				boundBox.minX =-this.width * 0.5;
				boundBox.maxX = this.width * 0.5;
				boundBox.minY = boundBox.minX;
				boundBox.maxY = boundBox.maxX;
				if(this.autoCenterHeight) {
					boundBox.minZ = -this.height * 0.5;
					boundBox.maxZ = this.height * 0.5;
				} else {
					boundBox.minZ = 0;
					boundBox.maxZ = this.height;
				}
			}
			
			var renderer:Renderer = camera.renderer;
			var renderUnit:RenderUnit = curRenderUnit;
			var needReset:Boolean = false;
			
			if (renderUnit == null) {
				renderUnit = curRenderUnit = camera.pool.popRenderUnit();
				needReset = true;
			}
			
			if (context3DChanged || needReset || this.curNeedReset) {		
				this.curNeedReset = false;
				renderUnit.object = this;
				renderUnit.firstIndex = 0;
				renderUnit.vertexBuffersLength = 0;
				renderUnit.texturesLength = 0;
				renderUnit.fragmentConstantsRegistersCount = 0;
				renderUnit.vertexConstantsRegistersCount = 0;
				renderUnit.program = this.curProgram3D
				renderUnit.indexBuffer = this.curIndexBuffer3D;
				renderUnit.numTriangles = 2;
				renderUnit.setVertexConstantsFromNumbers(0, this.width, this.height, this.autoCenterHeight ? 0: this.height*0.5  , 1);
				renderUnit.setVertexConstantsFromNumbers(1, 1, 1, texWidth/texAlignWidth, texHeight/texAlignHeight);
				renderUnit.setVertexBufferAt(0, this.curVertexBuffer3D, 0, Context3DVertexBufferFormat.FLOAT_3);
				renderUnit.setVertexBufferAt(1, this.curVertexBuffer3D, 3, Context3DVertexBufferFormat.FLOAT_2);
				renderUnit.setTextureAt(0, tex);
				
				if (renderUnit.culling != "none") {
					renderUnit.culling = "none";
				}
				
				renderUnit.srcBlend = Defines.BLEND_FACTORS[mat.getSrcBlend()];
				renderUnit.dstBlend = Defines.BLEND_FACTORS[mat.getDstBlend()];
				
				if (hasShadow) {
					renderUnit.setFragmentConstantsFromNumbers(0, -255*Defines.DIFFERENCE_MULTIPLIER, -Defines.DIFFERENCE_MULTIPLIER, 
						Defines.BIASD_MULTIPLIER*255*Defines.DIFFERENCE_MULTIPLIER, 1/16.0);
					renderUnit.setFragmentConstantsFromNumbers(1, 0.9999, Defines.DIFFERENCE_MULTIPLIER, 1, Defines.SHADER_OPAQUE);
					renderUnit.setTextureAt(1, camera.shadowCalculor.shadowMapFile);
				}
			}
			
			renderUnit.setProjectionConstants(camera, 2, this.localToCameraTransform);				
			var r:int = 0;
			if (hasShadow) {
				var shadowMapTransform:Transform3D = camera.pool.popTransform3D();
				shadowMapTransform.combine(camera.shadowCalculor.cameraToShadowMapUVProjection, this.localToCameraTransform);
				renderUnit.setVertexConstantsFromTransform(6, shadowMapTransform);
				camera.pool.pushTransform3D(shadowMapTransform);
				r = 2;
			}
			
			if (color != null) {
				renderUnit.setFragmentConstantsFromNumbers(r, 
					color.r * camera.mulColorFactor + camera.addColorFactor, 
					color.g * camera.mulColorFactor + camera.addColorFactor, 
					color.b * camera.mulColorFactor + camera.addColorFactor, 
					color.a);
			} else {
				renderUnit.setFragmentConstantsFromNumbers(r, 
					1 * camera.mulColorFactor + camera.addColorFactor, 
					1 * camera.mulColorFactor + camera.addColorFactor, 
					1 * camera.mulColorFactor + camera.addColorFactor, 
					1);
			}
			r++;
			
			if (this.alphaColor != null) {
				renderUnit.setFragmentConstantsFromNumbers(r, alphaColor.r, alphaColor.g, alphaColor.b, alphaColor.a);
			} else {
				renderUnit.setFragmentConstantsFromNumbers(r, 0, 0, 0, 0);
			}
			renderer.addRenderUnit(renderUnit, mat.getRenderPriority());
			return 1;
		}
	}
}