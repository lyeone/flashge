// 2014-3-10 lye

package FlashGE.objects
{
	import flash.display3D.Context3D;
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Context3DCompareMode;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Context3DTriangleFace;
	import flash.display3D.Context3DVertexBufferFormat;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.Program3D;
	import flash.display3D.VertexBuffer3D;
	import flash.display3D.textures.TextureBase;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import flash.utils.Endian;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.materials.Procedures;
	import FlashGE.materials.ShaderProgram;
	import FlashGE.materials.compiler.Linker;
	import FlashGE.materials.compiler.Procedure;
	import FlashGE.materials.compiler.VariableType;
	import FlashGE.render.Camera3D;
	import FlashGE.render.Context3DProperties;
	import FlashGE.resources.Geometry;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;

	public class RenderScene 
	{
		FlashGE var camera:Camera3D;
		FlashGE var visibleGrids:Object;
		FlashGE var isHSL:Boolean = true;
		FlashGE var texFormatID:int = 0;
		FlashGE var programs:Vector.<ShaderProgram> = new Vector.<ShaderProgram>(8);
		FlashGE var programsSimple:Vector.<ShaderProgram> = new Vector.<ShaderProgram>(8);
		FlashGE var sharedGeometry:Geometry;
		
		FlashGE var cHSLC0:Vector.<Number> = new <Number>[0,2,0.5,1];
		FlashGE var cHSLC1:Vector.<Number> = new <Number>[1/3.0,1/6.0,2/3.0,0];
		FlashGE var cHSLC2:Vector.<Number> = new Vector.<Number>(4);
		FlashGE var cHSLC3:Vector.<Number> = new Vector.<Number>(4);
		FlashGE var cDecode02:Vector.<Number> = new <Number>[0, 0, 1, 1];
		FlashGE var alpha:Number = 1;
		FlashGE var red:Number = 1;
		FlashGE var green:Number = 1;
		FlashGE var blue:Number = 1;
		FlashGE var backGroundTexture:TextureFile;
		public var forbidRenderGrids:Boolean = false;
		public var cameraPixelX:Number;
		public var cameraPixelY:Number;
		private var atfFlag:int = -1;
		public var isIdle:Boolean = false;
		public var curSceneTextures:Dictionary = new Dictionary;
		public var curTileWidth:int = 256;
		public var curTileHeight:int = 256;
		public var preTileWidth:int = 256;
		public var preTileHeight:int = 256;
		public function printVisibleGrids():String
		{
			var ret:String = "RenderScene::printVisibleGrids(begin)\n";
			var grid:Grid;
			if (this.visibleGrids != null) {
				for each (var k:* in this.visibleGrids) {
					if (k == null) {
						continue;
					}
					
					grid = k as Grid;
					ret += (grid.fileName + "\n");
				}
			}
			ret += "RenderScene::printVisibleGrids(end)\n";
			return ret;
		}
		
		private function markSceneTexture(tex:TextureFile):void
		{
			if (curSceneTextures[tex] == true) {
				return;
			}
			
			tex.addRef();
			this.curSceneTextures[tex] = true;
		}
		
		public function releaseAllSceneTextures():void
		{
			for (var key:TextureFile in curSceneTextures) {
				key.release();
			}
			curSceneTextures = new Dictionary;
		}
		
		public var isGridTextureLoadIdle:int = 0;
		public function onGridTextureLoaded(tex:TextureFile, isOK:Boolean):void
		{
			if (isGridTextureLoadIdle > 0) {
				isGridTextureLoadIdle--;
			}
		}
		
		FlashGE function renderGrids():void
		{
			if (forbidRenderGrids) {
				return;
			}
			if (this.visibleGrids == null) {
				return;
			}			
			if (camera.closeCollectRenderUnits) {
				return;
			}
			
			if (this.preTileHeight != this.curTileHeight ||
				this.preTileWidth != this.curTileWidth) {
				if (sharedGeometry != null) {
					sharedGeometry.release();
					sharedGeometry = null;
				}
			}
			if (this.sharedGeometry == null) {
				this.createGeometry();
			}
			
			if (atfFlag == -1) {
				atfFlag = TextureFile.forceATF ? 1 : 0;
			}
			
			if (atfFlag == 1) {
				this.texFormatID = 0;
			} else {
				this.texFormatID = 2;
			}
			
			var _contextProperties:Context3DProperties = camera.renderer._contextProperties;
			var context3D:Context3D = camera.context3D;
			var programIndex:int;
			if (isHSL) {
				programIndex = 4 + texFormatID;
			} else {
				programIndex = texFormatID;
			}
			
			var curProgram:ShaderProgram = programs[programIndex];
			if (curProgram == null) {
				this.createShader(programIndex);
				curProgram = programs[programIndex];
			}
			
			var positionBuffer:VertexBuffer3D = sharedGeometry.getVertexBuffer(Defines.DIFFUSE, context3D);
			var uvBuffer:VertexBuffer3D = sharedGeometry.getVertexBuffer(Defines.TEXCOORDS[0], context3D);
			var curTexture:TextureFile;	
			var indexBuffer:IndexBuffer3D = sharedGeometry.getIndexBuffer(context3D);
			
			var program3D:Program3D = null;
			program3D = curProgram.getProgram3D(context3D);
			
			context3D.setDepthTest(true, Context3DCompareMode.ALWAYS);			
			context3D.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ZERO);
			
			if (_contextProperties.culling != Context3DTriangleFace.NONE) {
				context3D.setCulling(Context3DTriangleFace.NONE);
				_contextProperties.culling = Context3DTriangleFace.NONE;
			}
			
			context3D.setVertexBufferAt(0, positionBuffer, 0, Context3DVertexBufferFormat.FLOAT_4);
			context3D.setVertexBufferAt(1, uvBuffer, 4, Context3DVertexBufferFormat.FLOAT_2);
			
			if (_contextProperties.program3D != program3D) {
				context3D.setProgram(program3D);
				_contextProperties.program3D = program3D;
			}
			
			context3D.setVertexBufferAt(2, null);
			context3D.setVertexBufferAt(3, null);
			context3D.setVertexBufferAt(4, null);
			context3D.setTextureAt(1, null);
			context3D.setTextureAt(2, null);
			
			setProgramPublicConstants(context3D, curProgram);
			
			var simpleGrid:Grid;
			var preSimpleGrid:Grid;
			var grid:Grid;
			var preTextureBase:TextureBase;
			for each (var k:* in this.visibleGrids) {
				if (k == null) {
					continue;
				}
				grid = k as Grid;
				curTexture = this.backGroundTexture;
				
				if (grid.curTexture == null && grid.fileName != null) {
					
					var hasFile:TextureFile = TextureFile.hasFile(grid.fileName);
					if (hasFile != null) {
						
						var loadState:int = hasFile.getLoadState();
						if (loadState == Defines.LOAD_FAILD || loadState == Defines.LOAD_OK) {
							grid.curTexture = TextureFile.createFromFile(grid.fileName, false, false, 
								null,
								false, isIdle, false, false, false);
							markSceneTexture(grid.curTexture);
						}
						
					} else {
						if (isGridTextureLoadIdle < 3) {
							isGridTextureLoadIdle++;
							grid.curTexture = TextureFile.createFromFile(grid.fileName, false, false, 
								onGridTextureLoaded,
								false, isIdle, false, false, false);
							markSceneTexture(grid.curTexture);
						}
					}
				}
				
				if (grid.curTexture != null) {
					curTexture = grid.curTexture;
				}
				
				var textureBase:TextureBase = curTexture.getTexture(context3D);
				if (textureBase == null) {
					curTexture = this.backGroundTexture;
					textureBase = curTexture.getTexture(context3D);
					if (textureBase == null) {
						continue;
					}
				}
				
				if (curTexture == this.backGroundTexture) {
					grid.uvScale[0] = grid.backScaleU
					grid.uvScale[1] = grid.backScaleV;
					grid.uvScale[2] = grid.backOffsetU;
					grid.uvScale[3] = grid.backOffsetV;
				} else {
					grid.uvScale[0] = grid.uScale;
					grid.uvScale[1] = grid.vScale;
					grid.uvScale[2] = 0;
					grid.uvScale[3] = 0;
				}
				
				grid.pos[0] = grid.x - cameraPixelX;
				grid.pos[1] = grid.y + cameraPixelY;
				grid.pos[2] = grid.pos[3] = 0;
				grid.textureBase = textureBase;
				if (grid.uvScale[0] == 1 && grid.uvScale[1] == 1 && 
					grid.uvScale[2] == 0 && grid.uvScale[3] == 0) {
					
					grid.nextSimple = null;
					if (simpleGrid == null) {
						simpleGrid = grid;
						preSimpleGrid = grid;
					} else {
						preSimpleGrid.nextSimple = grid;
						preSimpleGrid = grid;
					}
				} else {
				
					context3D.setProgramConstantsFromVector(Context3DProgramType.VERTEX, 
						curProgram.cUVScale, grid.uvScale, 1);
					
					context3D.setProgramConstantsFromVector(Context3DProgramType.VERTEX, 
						curProgram.cDecode01,grid.pos, 1);
					if (preTextureBase != textureBase) {
						context3D.setTextureAt(0, textureBase);
						preTextureBase = textureBase;
					}
					context3D.drawTriangles(indexBuffer, 0, 2);
					CONFIG::DEBUG {
						Global.drawsCount++;
					}
				}
			}
			
			if (simpleGrid != null) {
				curProgram = programsSimple[programIndex];
				program3D = curProgram.getProgram3D(context3D);
				context3D.setProgram(program3D);
				setProgramPublicConstants(context3D, curProgram);
				
				while (simpleGrid != null) {
					context3D.setProgramConstantsFromVector(Context3DProgramType.VERTEX, 
						curProgram.cDecode01, simpleGrid.pos, 1);
					
					if (preTextureBase != simpleGrid.textureBase) {
						context3D.setTextureAt(0, simpleGrid.textureBase);
						preTextureBase = simpleGrid.textureBase;
					}
					context3D.drawTriangles(indexBuffer, 0, 2);
					simpleGrid = simpleGrid.nextSimple;
					CONFIG::DEBUG {
						Global.drawsCount++;
					}
				}
			}
			
			context3D.setVertexBufferAt(0, null);
			context3D.setVertexBufferAt(1, null);
			context3D.setTextureAt(0, null);
		}
		
		private function setProgramPublicConstants(context3D:Context3D, curProgram:ShaderProgram):void
		{
			if (curProgram.cHSLC0 >= 0) {
				context3D.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, curProgram.cHSLC0, cHSLC0, 1);
			}
			
			if (curProgram.cHSLC1 >= 0) {
				context3D.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, curProgram.cHSLC1, cHSLC1, 1);
			}		
			
			if (curProgram.cHSLC2 >= 0) {
				context3D.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, curProgram.cHSLC2, cHSLC2, 1);
			}
			
			if (curProgram.cHSLC3 >= 0) {
				this.cHSLC3[0] = this.red * camera.mulColorFactor *camera.sunColorSceneR+ camera.addColorFactor;
				this.cHSLC3[1] = this.green * camera.mulColorFactor*camera.sunColorSceneG + camera.addColorFactor;
				this.cHSLC3[2] = this.blue * camera.mulColorFactor*camera.sunColorSceneB + camera.addColorFactor;
				this.cHSLC3[3] = this.alpha;
				
				context3D.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, curProgram.cHSLC3, cHSLC3, 1);
			}
			
			cDecode02[0] = camera.view.viewWidth  * 0.5;
			cDecode02[1] = camera.view.viewHeight * 0.5;
			cDecode02[2] = 1.0;
			cDecode02[3] = 1.0;
			context3D.setProgramConstantsFromVector(Context3DProgramType.VERTEX, 
				curProgram.cDecode02, cDecode02, 1);
			
		}
		
		private function createShader(index:int):void
		{
			createShaderImpl(index, Procedures.vertexShaderHSLGrid, programs);
			createShaderImpl(index, Procedures.vertexShaderHSLGridSimple, programsSimple);
		}
		
		
		private function createShaderImpl(index:int, procV:Procedure, outPrograms:Vector.<ShaderProgram>):void
		{
			var procF:Procedure;
			if (isHSL) {
				procF = Procedures.getPixelShaderHSL(false, texFormatID);
			} else {
				procF = Procedures.getPixelShaderNoneHSL(texFormatID);
			}
			
			var positionVar:String = "aPosition";
			var vertexLinker:Linker = new Linker(Context3DProgramType.VERTEX);
			
			vertexLinker.declareVariable(positionVar, VariableType.ATTRIBUTE);
			vertexLinker.addProcedure(procV);
			vertexLinker.setInputParams(procV, positionVar);
			
			var fragmentLinker:Linker = new Linker(Context3DProgramType.FRAGMENT);
			fragmentLinker.addProcedure(procF);	
			fragmentLinker.varyings = vertexLinker.varyings;
			var curProgram:ShaderProgram = new ShaderProgram(vertexLinker, fragmentLinker);
			outPrograms[index] = curProgram;
			
		}
		
		private function createGeometry():void
		{
			var g:Geometry = Geometry.create();
			var attributes:Array = [];
			attributes[0] = Defines.DIFFUSE;
			attributes[1] = Defines.DIFFUSE;
			attributes[2] = Defines.DIFFUSE;
			attributes[3] = Defines.DIFFUSE;
			attributes[4] = Defines.TEXCOORDS[0];
			attributes[5] = Defines.TEXCOORDS[0];
			g.addVertexStream(attributes);
			
			var indices:Vector.<uint> = new Vector.<uint>();
			var x:int;
			var y:int;
			var wEdges:int = 2;
			var lEdges:int = 2;
			var halfWidth:Number = this.curTileWidth;
			var halfLength:Number = -this.curTileHeight;
			var segmentUSize:Number = 1;
			var segmentVSize:Number = 1;
			
			var vertices:ByteArray = new ByteArray();
			vertices.endian = Endian.LITTLE_ENDIAN;

			for (x = 0; x < wEdges; x++) {
				for (y = 0; y < lEdges; y++) {
					vertices.writeFloat(x*halfWidth);
					vertices.writeFloat(y*halfLength);
					vertices.writeFloat(0.9);
					vertices.writeFloat(1);
					vertices.writeFloat(x*segmentUSize);
					vertices.writeFloat(y*segmentVSize);
				}
			}
			
			for (x = 0; x < wEdges; x++) {
				for (y = 0; y < lEdges; y++) {
					if (x < 1 && y < 1) {
						createFace(indices, 
							x*lEdges + y + 1, 
							(x + 1)*lEdges + y + 1, 
							(x + 1)*lEdges + y, 
							x*lEdges + y);
						
					}
				}
			}		
			
			g.indicesBuffer = indices;
			g.vertexStreamsList[0].data = vertices;
			g.verticesCount = 4;
			this.preTileWidth = this.curTileWidth;
			this.preTileHeight = this.preTileHeight;
			this.sharedGeometry = g;
		}
		
		protected function createFace(indices:Vector.<uint>, a:int, b:int, c:int, d:int):void
		{
			indices.push(a);
			indices.push(b);
			indices.push(c);
			indices.push(a);
			indices.push(c);
			indices.push(d);
		}
	}
}