// 2014-3-10 lye
package FlashGE.objects
{
	import flash.display3D.Context3D;
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Context3DCompareMode;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Context3DVertexBufferFormat;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.Program3D;
	import flash.display3D.VertexBuffer3D;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.materials.compiler.Procedure;
	import FlashGE.render.Camera3D;
	import FlashGE.render.RenderUnit;
	import FlashGE.resources.TextureFile;

	CONFIG::DEBUG {
	import FlashGE.common.Global;
	}
	
	use namespace FlashGE;
	
	public class GaussianBlurRect extends Entity
	{
		private var curIndexBuffer:IndexBuffer3D;
		private var curVertexBuffer:VertexBuffer3D;
		private var curProgram3D:Program3D;
		private var context3DUsed:Context3D;
		private var vertexCodeBytes:ByteArray;
		private var fragmentCodeBytes:ByteArray;
		
		private var blurRadius:int = 2;
		private var blurTexture:TextureFile;
		private var blurFactor:Number = 1.5;
		private var renderUnit:RenderUnit;
		private var textureDirty:Boolean = true;
		
		public function dispose():void
		{
			if (renderUnit != null) {
				renderUnit.release();
				renderUnit = null;
			}
			
			if (this.parent != null) {
				this.parent.removeChild(this);
			}
			
			if (blurTexture != null) {
				blurTexture.release();
				blurTexture = null;
			}
		}
		
		public function create3DBuffers(context3D:Context3D):void
		{
			if (renderUnit != null) {
				renderUnit.vertexBuffersLength = 0;				
				renderUnit.indexBuffer = null;
				renderUnit.program = null;
			}
			
			if (curIndexBuffer != null) {
				curIndexBuffer.dispose();
				curIndexBuffer = null;
			}
			
			if (curVertexBuffer != null) {
				curVertexBuffer.dispose();
				curVertexBuffer = null;
			}
			
			if (curProgram3D != null) {
				curProgram3D.dispose();
				curProgram3D = null;
			}
			
			var i:int, n:int;
			var vertices:Vector.<Number> = new Vector.<Number>();
			var indices:Vector.<uint> = new Vector.<uint>();
			vertices.push(
					-1,-1, 0, 0, 1, 0,
					-1, 1, 0, 0, 0, 0,
					 1, 1, 0, 1, 0, 0,
					 1,-1, 0, 1, 1, 0);
				
			indices.push(3, 1, 0, 1, 3, 2);
			
			curIndexBuffer = context3D.createIndexBuffer(6);
			CONFIG::DEBUG {
				Global.indexBuffersCount++;
			}
			curIndexBuffer.uploadFromVector(indices, 0, 6);
			
			curVertexBuffer = context3D.createVertexBuffer(4, 6);
			CONFIG::DEBUG {
				Global.vertexBuffersCount++;
			}
			curVertexBuffer.uploadFromVector(vertices, 0, 4);
			
			// c0 : alignWidth, alignHeight, viewWidth, viewHeight
			// c1 : x, y, z, 0
			if (this.vertexCodeBytes == null || this.fragmentCodeBytes == null) {
				var constIndex:int = 1;
				var vIndex:int = 0
				var x:int, y:int;
				var shaderIndex:int = 0;
				var vertShaderArray:Array = [];
				vertShaderArray[shaderIndex++] = "mov o0, a0";
				vertShaderArray[shaderIndex++] = "mov v0, a1";
				
				shaderIndex = 0;
				var fragShaderArray:Array = [];
				var texFormatID:int = this.blurTexture.getFormatID();
				var texFormat:String = Defines.TEX_FORMATS[texFormatID];
				constIndex = 1;
				var subIndex:int = 0;
				var firstTime:Boolean = true;
				fragShaderArray[shaderIndex++] = "sub t2, v0.xy, c0.xy";
				for (y = -blurRadius; y <= blurRadius; y++) {
					
					for (x = -blurRadius; x <= blurRadius; x++) {
						
						fragShaderArray[shaderIndex++] = "tex t0, t2.xy, s0 <2d, linear,clamp, miplinear, "+texFormat+">";
						if (firstTime) {
							firstTime = false;
							if (subIndex == 0) {
								fragShaderArray[shaderIndex++] = "mul t1, t0, c"+constIndex+".xxxx";	
							} else if (subIndex == 1) {
								fragShaderArray[shaderIndex++] = "mul t1, t0, c"+constIndex+".yyyy";	
							} else if (subIndex == 2) {
								fragShaderArray[shaderIndex++] = "mul t1, t0, c"+constIndex+".zzzz";	
							} else if (subIndex == 3) {
								fragShaderArray[shaderIndex++] = "mul t1, t0, c"+constIndex+".wwww";	
							}
							
						} else {
							if (subIndex == 0) {
								fragShaderArray[shaderIndex++] = "mul t0, t0, c"+constIndex+".xxxx";	
							} else if (subIndex == 1) {
								fragShaderArray[shaderIndex++] = "mul t0, t0, c"+constIndex+".yyyy";	
							} else if (subIndex == 2) {
								fragShaderArray[shaderIndex++] = "mul t0, t0, c"+constIndex+".zzzz";	
							} else if (subIndex == 3) {
								fragShaderArray[shaderIndex++] = "mul t0, t0, c"+constIndex+".wwww";	
							}
							
							if (y != blurRadius || x != blurRadius) {
								fragShaderArray[shaderIndex++] = "add t1, t1, t0";
							}
						}
						subIndex++;
						if (subIndex >= 4){
							subIndex = 0;
							constIndex++;
						}
						
						if (x != blurRadius) {
							fragShaderArray[shaderIndex++] = "add t2.x, t2.x, c0.z";
						}
					}
					
					if (y != blurRadius) {
						fragShaderArray[shaderIndex++] = "add t2.y, t2.y, c0.w";
						fragShaderArray[shaderIndex++] = "sub t2.x, v0.x, c0.x";
					}
				}
				
				if (blurRadius == 0) {
					
					fragShaderArray[shaderIndex++] = "tex t1, v0.xy, s0 <2d, linear,clamp, miplinear, "+texFormat+">";
					fragShaderArray[shaderIndex++] = "mov o0, t0";
				} else {
					fragShaderArray[shaderIndex++] = "add o0, t1, t0";
				}
				
				var vertProcedure:Procedure = new Procedure(vertShaderArray);
				var fragProcedure:Procedure = new Procedure(fragShaderArray);
				this.vertexCodeBytes = vertProcedure.getByteCode(Context3DProgramType.VERTEX);
				this.fragmentCodeBytes = fragProcedure.getByteCode(Context3DProgramType.FRAGMENT);
			}
			
			curProgram3D = context3D.createProgram();
			curProgram3D.upload(this.vertexCodeBytes, this.fragmentCodeBytes);
			context3DUsed = context3D;
		}
		
		public function setTextSprite(tex:TextureFile):void
		{
			if (blurTexture != null) {
				blurTexture.release();
				blurTexture = null;
			}

			blurTexture = tex;
			if (blurTexture != null) {
				blurTexture.addRef();
			}
			textureDirty = true;
		}
		
		override FlashGE function collectRenderUnits(camera:Camera3D, isShadowPass:Boolean):int 
		{
			if (this.blurTexture == null || this.blurTexture.getTexture(camera.context3D) == null) {
				return 0;
			}
			
			var needSet:Boolean = false;
			if (context3DUsed != camera.context3D) {
				context3DUsed = camera.context3D;
				this.create3DBuffers(context3DUsed);
				needSet = true;
			}
			
			if (curIndexBuffer == null || curVertexBuffer == null || curProgram3D == null) {
				return 0;
			}
			
			if (renderUnit == null) {
				renderUnit = camera.pool.popRenderUnit();
				renderUnit.object = this;
				renderUnit.srcBlend = Context3DBlendFactor.ONE;
				renderUnit.dstBlend = Context3DBlendFactor.ZERO;
				renderUnit.numTriangles = 2;
				if (renderUnit.culling != "front") {
					renderUnit.culling = "front";
				}
				needSet = true;
				
				calcConstants();
			}
			
			if (needSet) {
				renderUnit.vertexBuffersLength = 0;				
				renderUnit.setVertexBufferAt(0, curVertexBuffer, 0, Context3DVertexBufferFormat.FLOAT_3);
				renderUnit.setVertexBufferAt(1, curVertexBuffer, 3, Context3DVertexBufferFormat.FLOAT_3);
				
				renderUnit.indexBuffer = curIndexBuffer;
				renderUnit.program = curProgram3D;
				textureDirty = true;
			}
			
			if (textureDirty) {
				textureDirty = false;
				renderUnit.releaseAllTextures();
				renderUnit.setTextureAt(0, blurTexture);
				var du:Number = 1.0 / blurTexture.getAlignWidth();
				var dv:Number = 1.0 / blurTexture.getAlignHeight();
				renderUnit.setFragmentConstantsFromNumbers(0, this.blurRadius * du, this.blurRadius * dv, du, dv);
			}
			
			camera.renderer.addRenderUnit(renderUnit, Defines.PRIORITY_SKY);
			return 1;
		}
		
		public function renderToTexture(camera:Camera3D, target:TextureFile):void
		{
			if (this.blurTexture == null || target == null) {
				return;
			}
			var context:Context3D = camera.context3D;
			var rect:Rectangle = new Rectangle;
			rect.x = 0;
			rect.y = 0;
			rect.width = blurTexture.getAlignWidth();
			rect.height = blurTexture.getAlignHeight();
			context.setScissorRectangle(rect);
			context.setRenderToTexture(target.getTexture(context), true);
			context.clear(0, 0, 0, 1);
			context.setDepthTest(false, Context3DCompareMode.ALWAYS);
			
			context.setBlendFactors(renderUnit.srcBlend, renderUnit.dstBlend);
			context.setCulling(renderUnit.culling);
			context.setVertexBufferAt(0, renderUnit.vertexBuffers[0], renderUnit.vertexBuffersOffsets[0], renderUnit.vertexBuffersFormats[0]);
			context.setVertexBufferAt(1, renderUnit.vertexBuffers[1], renderUnit.vertexBuffersOffsets[1], renderUnit.vertexBuffersFormats[1]);
			for (var v:int = 2; v < 8; v++) {
				context.setVertexBufferAt(v, null);
			}
			context.setProgramConstantsFromVector(Context3DProgramType.FRAGMENT, 0, renderUnit.fragmentConstants, renderUnit.fragmentConstantsRegistersCount);
			
			context.setTextureAt(0, blurTexture.getTexture(context));
			for (var s:int = 1; s < 8; s++) {
				context.setTextureAt(s, null);
			}
			context.setProgram(curProgram3D);
			//context.enableErrorChecking = true;
			context.drawTriangles(renderUnit.indexBuffer, renderUnit.firstIndex, renderUnit.numTriangles);
			context.setTextureAt(0, null);
			context.setVertexBufferAt(0, null);
			context.setVertexBufferAt(1, null);
			context.setScissorRectangle(null);
			context.setRenderToBackBuffer();	
		}
		
		public function calcConstants():void
		{
			const M_PI:Number = Math.PI;
			var r:Number = blurFactor;
			var a:Number = 1.0 / (2 * M_PI * r * r);
			var b:Number;
			var x:int;
			var y:int;
			var sum:Number = 0;
			var constantsCount:int = (blurRadius*2 + 1) * (blurRadius*2 + 1);
			constantsCount = constantsCount + 4 - constantsCount % 4;
			var constants:Vector.<Number> = new Vector.<Number>(constantsCount);
			for (y = -blurRadius; y <= blurRadius; y++) {
				
				for (x = -blurRadius; x <= blurRadius; x++) {
					
					b = Math.exp( -(x * x + y * y) / (2 * r * r));
					sum += b;
				}
			}
			
			var curIndex:int = 0;
			for (y = -blurRadius; y <= blurRadius; y++) {
				
				for (x = -blurRadius; x <= blurRadius; x++) {
					
					b = (Math.exp( -(x * x + y * y) / (2.0 * r * r))) / sum;
					constants[curIndex++] = b;
				}
			}
			
			var numReisters:int = constantsCount / 4;
			renderUnit.setFragmentConstantsFromVector(1, constants, numReisters);
		}
	}
}