// 2014-3-10 lye
package FlashGE.objects
{
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.utils.ByteArray;
	import flash.utils.CompressionAlgorithm;
	import flash.utils.Dictionary;
	import flash.utils.Endian;
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.animation.SkeletonAni;
	import FlashGE.common.AxisAlignedBox;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.loaders.RawDataReader;
	import FlashGE.resources.LoaderMgr3D;

	use namespace FlashGE;
	
	public class Skeleton
	{
		static FlashGE var skeletons:Dictionary = new Dictionary;
		
		FlashGE var allBonesCount:int = 0;
		FlashGE var allBones:Vector.<Bone>= null;
		FlashGE var boneNameList:Dictionary = null;
		FlashGE var fileName:String;
		FlashGE var aabb:AxisAlignedBox;
		FlashGE var loadState:int = Defines.LOAD_NULL;
		FlashGE var allSockets:Vector.<Bone> = null;
		FlashGE var listeners:Vector.<Function> = new Vector.<Function>;
		FlashGE var getCurTimer:Function = getTimer;
		
		public function Skeleton()
		{
		}
		
		public function toString():String
		{
			var i:int, n:int;
			var ret:String = "Skeleton:(" + this.fileName;
			ret +=(",allBonesCount=" +allBonesCount);
			if (allBones != null) {
				for (i = 0, n= this.allBones.length; i < n; i++) {
					ret += (",i"+i + this.allBones[i]);
				}
			}
			ret += ")";
			return ret;
		}
		
		static public function printAll():String
		{
			var ske:Skeleton = null;
			var ret:String = "Skeleton::printAll cache:\n";
			for each(ske in Skeleton.skeletons) {
				ret += ske.toString() + "\n";
			}
			
			ret += "Skeleton::printAll end\n";
			return ret;
		}
		
		public static function load(fileName:String, loadListener:Function = null, 
									insertFirst:Boolean = false,
									idleLoader:Boolean = false, 
									moveToHighPriority:Boolean = false):Skeleton
		{			
			var ret:Skeleton = Skeleton.skeletons[fileName];
			if (ret == null) {
				ret = new Skeleton;
				if (loadListener != null) {
					ret.addListener(loadListener);
				}
				Skeleton.skeletons[fileName] = ret;
				ret.createFromFile(fileName, insertFirst, idleLoader, moveToHighPriority);
			} else {
				if (idleLoader == false) {
					LoaderMgr3D.getInstance().checkIdle(fileName, insertFirst, moveToHighPriority);
				}
				if (loadListener != null) {
					ret.addListener(loadListener);
				}
			}
			
			return ret;
		}
		
		public function addListener(callback:Function):void
		{
			if (this.loadState == Defines.LOAD_OK || this.loadState == Defines.LOAD_FAILD) {
				callback(this, this.loadState == Defines.LOAD_OK);
			} else {
				this.listeners.push(callback);
			}		
		}
		
		private function postListeners(isOK:Boolean):void
		{
			for (var i:int = 0, len:int = this.listeners.length; i < len; i++) {
				var callback:Function = this.listeners[i];
				callback(this, isOK);
			}
			this.listeners.length = 0;
		}
		
		private function createFromFile(fileName:String, insertFirst:Boolean, idleLoader:Boolean, moveToHighPriority:Boolean):void
		{
			this.loadState = Defines.LOAD_ING;
			this.fileName = fileName;
		
			if (moveToHighPriority) {
				LoaderMgr3D.getInstance().pushHighPriorityLoader(this.fileName, onLoadComplete, true, true, insertFirst);
			} else if (idleLoader) {
				LoaderMgr3D.getInstance().pushIdleLoader(this.fileName, onLoadComplete, true, true, insertFirst);
			} else {
				LoaderMgr3D.getInstance().pushLoader(this.fileName, onLoadComplete, true, true, insertFirst);
			}
			
		}
		
		FlashGE var dataContext:RawDataContext;
		private function onLoadComplete(rawData:ByteArray, isHighPriority:Boolean = false):void
		{
			if (rawData == null) {
				this.loadState = Defines.LOAD_FAILD;
				postListeners(false);
				return;
			}
			
			dataContext = new RawDataContext;
			dataContext.raw = rawData;
			dataContext.raw.endian = Endian.LITTLE_ENDIAN;
			dataContext.raw.position = 0;
			RawDataReader.getInstance().addListener(parseRawData);
		}
		
		private function parseRawData(timer:int):int
		{
			try {
				var curTimer:int = getCurTimer();
				if (dataContext.version == -1) {
					var tag:String = dataContext.raw.readUTFBytes(7);
					if (tag == "deflate") {
						var temp:ByteArray = new ByteArray;
						temp.endian = Endian.LITTLE_ENDIAN;
						temp.position = 0;
						dataContext.raw.readBytes(temp, 0, dataContext.raw.length - 7);
						temp.uncompress(CompressionAlgorithm.ZLIB);
						dataContext.raw = temp;
					}
					dataContext.raw.position = 0;				
					dataContext.version = dataContext.raw.readUnsignedShort();
					if (getCurTimer() - curTimer >= timer) {
						return 0;
					}
				}
				
				var i:int;
				var len:int;
				
				if (dataContext.bonesReaded == false) {
					dataContext.bonesReaded = true;
					aabb = new AxisAlignedBox;
					aabb.minX = dataContext.raw.readShort()
					aabb.minY = dataContext.raw.readShort();
					aabb.minZ = dataContext.raw.readShort();
					aabb.maxX = dataContext.raw.readShort();
					aabb.maxY = dataContext.raw.readShort();
					aabb.maxZ = dataContext.raw.readShort();
					allBonesCount = dataContext.raw.readUnsignedByte();
					var name:String;
					
					if (allBonesCount > 0) {
						this.allBones = new Vector.<Bone>;
						this.boneNameList = new Dictionary;
					}
					
					dataContext.idToName = new Vector.<String>(allBonesCount);
					for (i=0; i < allBonesCount; i++) {
						len = dataContext.raw.readUnsignedByte();
						name = dataContext.raw.readUTFBytes(len);
						this.boneNameList[name] = i;
						dataContext.idToName[i] = name;
					}
					
					this.allBones.length = allBonesCount;
					if (getCurTimer() - curTimer >= timer) {
						return 0;
					}
				}
				
				var bone:Bone;
				while (dataContext.boneIndex < allBonesCount) {
					bone = new Bone;
					bone.boneID = dataContext.raw.readByte();
					bone.parentBoneID = dataContext.raw.readByte();
					bone.setName(dataContext.idToName[bone.boneID]);
					
					var axisX:Number = dataContext.raw.readFloat();
					var axisY:Number = dataContext.raw.readFloat();
					var axisZ:Number = dataContext.raw.readFloat();
					var angle:Number = dataContext.raw.readFloat();
					
					bone.bindPoseTransform.makeRotation(axisX, axisY, axisZ, angle);
					bone.bindPoseTransform.d = dataContext.raw.readFloat();
					bone.bindPoseTransform.h = dataContext.raw.readFloat();
					bone.bindPoseTransform.l = dataContext.raw.readFloat();
					
					this.allBones[bone.boneID] = bone;

					if (bone.getName().charAt(0) == "#") {
						allSockets = allSockets || new Vector.<Bone>;
						allSockets.push(bone);
					}
					dataContext.boneIndex++;
					if (getCurTimer() - curTimer >= timer) {
						return 0;
					}
				}
				
				for (i = 0; i < allBonesCount; i++) {
					bone = this.allBones[i];
					if (bone.parentBoneID >= 0) {
						bone.parentBone = this.allBones[bone.parentBoneID];
					}
				}
				
				dataContext = null;
				RawDataReader.getInstance().removeListener(parseRawData);
				this.loadState = Defines.LOAD_OK;
				postListeners(true);
			} catch (e:Error) {
				
				dataContext = null;
				RawDataReader.getInstance().removeListener(parseRawData);
				this.loadState = Defines.LOAD_FAILD;
				postListeners(false);
			}
			return timer - (getCurTimer() - curTimer);
		}
		
		public function getBoneByName(boneName:String):Bone
		{
			if (this.boneNameList == null) {
				return null;
			}
			var o:Object = this.boneNameList[boneName];
			if (o==null) return null;
			
			var index:int = o as int;
			if (index>=allBonesCount) {
				return null;
			}
			
			return this.allBones[index];
		}
		
		FlashGE function calcBoneTransform(model:Entity, bone:Bone):void
		{
			var curFrameCount:int = Global.frameCount;
			if (bone.name.charAt(0) == "#") {
				
				var parentBone:Bone = bone.parentBone;
				if (parentBone.curKeyFrame != null && parentBone.curKeyFrame.transform != null) {

					parentBone.localToCameraTransform.combine(model.localToCameraTransform, parentBone.curKeyFrame.transform);
					//parentBone.calcFrame = curFrameCount;
					
					bone.localToCameraTransform.copy(bone.bindPoseTransform);
					bone.localToCameraTransform.append(bone.parentBone.localToCameraTransform);
					bone.cameraToLocalTransform.copy(bone.localToCameraTransform);
					bone.cameraToLocalTransform.invert();
					//bone.calcFrame = curFrameCount;
				}
			} else {
				if (bone.curKeyFrame != null && bone.curKeyFrame.transform != null) {
					bone.localToCameraTransform.combine(model.localToCameraTransform, bone.curKeyFrame.transform);
					bone.cameraToLocalTransform.copy(bone.localToCameraTransform);
					bone.cameraToLocalTransform.invert();
					//bone.calcFrame = curFrameCount;
				}
			}
		}
		
		FlashGE function calculateBone(model:Entity, bone:Bone):void
		{
			if (bone == null || model == null) {
				return;
			}
			
			var parentBone:Bone = bone.parentBone;
			if (bone.name.charAt(0) != "#") {
				parentBone = null;
			}
			
			if (parentBone != null && parentBone.curKeyFrame != null && parentBone.curKeyFrame.transform != null) {
				parentBone.localToCameraTransform.combine(model.localToCameraTransform, parentBone.curKeyFrame.transform);
				
				bone.localToCameraTransform.copy(bone.bindPoseTransform);
				bone.localToCameraTransform.append(bone.parentBone.localToCameraTransform);
				bone.cameraToLocalTransform.copy(bone.localToCameraTransform);
				bone.cameraToLocalTransform.invert();
			} else if (bone.curKeyFrame != null && bone.curKeyFrame.transform != null) {
				bone.localToCameraTransform.combine(model.localToCameraTransform, bone.curKeyFrame.transform);
				bone.cameraToLocalTransform.copy(bone.localToCameraTransform);
				bone.cameraToLocalTransform.invert();
			}
		}
		
		FlashGE function calculateBonesTransforms(model:Entity, forceCalcAllBones:Boolean):void 
		{
			if (this.loadState != Defines.LOAD_OK || this.allBonesCount==0) return;
			
			var curFrameCount:int = Global.frameCount;
			
			for(var i:int = 0; i < this.allBonesCount; i++) {
				var bone:Bone = this.allBones[i];
				//if (bone.calcFrame == curFrameCount) continue;
				if (bone.curKeyFrame != null && bone.curKeyFrame.transform != null && (bone.refCountLinked > 0 || forceCalcAllBones==true)) {

					
					bone.localToCameraTransform.combine(model.localToCameraTransform, bone.curKeyFrame.transform);
					bone.cameraToLocalTransform.copy(bone.localToCameraTransform);
					bone.cameraToLocalTransform.invert();
					//bone.calcFrame = curFrameCount;
				}
			}
			
			var parentBone:Bone;
			if (this.allSockets != null) {
				for (i=0; i < this.allSockets.length; i++) {
					bone = this.allSockets[i];
					//if (bone.calcFrame == curFrameCount) continue;
					if (bone.refCountLinked > 0 || forceCalcAllBones) {
						parentBone = bone.parentBone;
						if (parentBone.curKeyFrame != null && parentBone.curKeyFrame.transform != null) {
							parentBone.localToCameraTransform.combine(model.localToCameraTransform, parentBone.curKeyFrame.transform);
							parentBone.calcFrame = curFrameCount;
						
							bone.localToCameraTransform.copy(bone.bindPoseTransform);
							bone.localToCameraTransform.append(bone.parentBone.localToCameraTransform);
							bone.cameraToLocalTransform.copy(bone.localToCameraTransform);
							bone.cameraToLocalTransform.invert();
							//bone.calcFrame = curFrameCount;
						}
					}
				}
			}
		}
	}
}

import flash.utils.ByteArray;

internal class RawDataContext
{
	var raw:ByteArray;
	var version:int = -1;
	var bonesReaded:Boolean = false;
	var idToName:Vector.<String>;
	var boneIndex:int = 0;
}
