package FlashGE.objects
{
	import flash.geom.Vector3D;
	
	public class FrameBox extends Entity
	{
		public function FrameBox(showAxis:Boolean = false)
		{
			var points1:Vector.<Vector3D> = Vector.<Vector3D>( [
				new Vector3D(-10, -10, -10), new Vector3D(10, -10, -10)
			] );
			var coordinate1:WireFrame = WireFrame.createLinesList(points1, 0xFFFFFF, 1, 0.1);
			this.addChild(coordinate1);
			
			var points2:Vector.<Vector3D> = Vector.<Vector3D>( [
				new Vector3D(-10, -10, 10), new Vector3D(10, -10, 10)
			] );
			var coordinate2:WireFrame = WireFrame.createLinesList(points2, 0xFFFFFF, 1, 0.1);
			this.addChild(coordinate2);
			
			var points3:Vector.<Vector3D> = Vector.<Vector3D>( [
				new Vector3D(-10, 10, 10), new Vector3D(10, 10, 10)
			] );
			var coordinate3:WireFrame = WireFrame.createLinesList(points3, 0xFFFFFF, 1, 0.1);
			this.addChild(coordinate3);	
			
			var points4:Vector.<Vector3D> = Vector.<Vector3D>( [
				new Vector3D(-10, 10, -10), new Vector3D(10, 10, -10)
			] );
			var coordinate4:WireFrame = WireFrame.createLinesList(points4, 0xFFFFFF, 1, 0.1);
			this.addChild(coordinate4);
			
			var points5:Vector.<Vector3D> = Vector.<Vector3D>( [
				new Vector3D(-10, -10, -10), new Vector3D(-10, 10, -10)
			] );
			var coordinate5:WireFrame = WireFrame.createLinesList(points5, 0xFFFFFF, 1, 0.1);
			this.addChild(coordinate5);			
			
			var points6:Vector.<Vector3D> = Vector.<Vector3D>( [
				new Vector3D(-10, -10, 10), new Vector3D(-10, 10, 10)
			] );
			var coordinate6:WireFrame = WireFrame.createLinesList(points6, 0xFFFFFF, 1, 0.1);
			this.addChild(coordinate6);
			
			var points7:Vector.<Vector3D> = Vector.<Vector3D>( [
				new Vector3D(-10, -10, -10), new Vector3D(-10, -10, 10)
			] );
			var coordinate7:WireFrame = WireFrame.createLinesList(points7, 0xFFFFFF, 1, 0.1);
			this.addChild(coordinate7);
			
			var points8:Vector.<Vector3D> = Vector.<Vector3D>( [
				new Vector3D(-10, 10, -10), new Vector3D(-10, 10, 10)
			] );
			var coordinate8:WireFrame = WireFrame.createLinesList(points8, 0xFFFFFF, 1, 0.1);
			this.addChild(coordinate8);
			
			
			var points9:Vector.<Vector3D> = Vector.<Vector3D>( [
				new Vector3D(10, -10, -10), new Vector3D(10, 10, -10)
			] );
			var coordinate9:WireFrame = WireFrame.createLinesList(points9, 0xFFFFFF, 1, 0.1);
			this.addChild(coordinate9);			
			
			var points10:Vector.<Vector3D> = Vector.<Vector3D>( [
				new Vector3D(10, -10, 10), new Vector3D(10, 10, 10)
			] );
			var coordinate10:WireFrame = WireFrame.createLinesList(points10, 0xFFFFFF, 1, 0.1);
			this.addChild(coordinate10);
			
			var points11:Vector.<Vector3D> = Vector.<Vector3D>( [
				new Vector3D(10, -10, -10), new Vector3D(10, -10, 10)
			] );
			var coordinate11:WireFrame = WireFrame.createLinesList(points11, 0xFFFFFF, 1, 0.1);
			this.addChild(coordinate11);
			
			var points12:Vector.<Vector3D> = Vector.<Vector3D>( [
				new Vector3D(10, 10, -10), new Vector3D(10, 10, 10)
			] );
			var coordinate12:WireFrame = WireFrame.createLinesList(points12, 0xFFFFFF, 1, 0.1);
			this.addChild(coordinate12);
			
			if (showAxis)
				this.addChild(new AxisFrame);
		}
		
	}
}

