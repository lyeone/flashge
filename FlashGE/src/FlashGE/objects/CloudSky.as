// 2014-3-10 lye
package FlashGE.objects
{
	import flash.display3D.Context3D;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Context3DVertexBufferFormat;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.Program3D;
	import flash.display3D.VertexBuffer3D;
	import flash.utils.ByteArray;
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.common.AxisAlignedBox;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.materials.Material;
	import FlashGE.materials.compiler.Procedure;
	import FlashGE.render.Camera3D;
	import FlashGE.render.RenderUnit;
	import FlashGE.render.Renderer;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;
	
	public class CloudSky extends Mesh
	{
		static private var vertexBuffer:VertexBuffer3D;
		static private var indexBuffer:IndexBuffer3D;
		static private var shaderProgram:Program3D;
		static private var vertexCodeBytes:ByteArray;
		static private var fragmentCodeBytes:ByteArray;
		
		private var cloudSpeed:Number = 1;
		private var context3DUsed:Context3D;
		
		private var renderUnit:RenderUnit;
		static public function create():CloudSky
		{
			var ret:CloudSky = new CloudSky;
			return ret;
		}
		
		public function CloudSky()
		{
		}
				
		public function setTexture(skyFilename:String):void
		{
			if (this.material == null) {
				this.material = Material.createMaterial();
			}
			
			var tex:TextureFile =  TextureFile.createFromFile(skyFilename);
			this.material.setTexture(0, tex);
			tex.release();
		}
		
		private function create3D(camera:Camera3D, texFormat:int):Program3D
		{
			var context:Context3D = camera.context3D;
			
			if (CloudSky.vertexBuffer != null) {
				CloudSky.vertexBuffer.dispose();
				CloudSky.vertexBuffer = null;
			}
			
			if (CloudSky.indexBuffer != null) {
				CloudSky.indexBuffer.dispose();
				CloudSky.indexBuffer = null;
			}
			
			var verts:Vector.<Number> = new <Number>[
				-1,-1,0.9,0.0,0.5,
				-1, 1,0.9,0.0,0,
				 1, 1,0.9,0.5,0,
				 1,-1,0.9,0.5,0.5
			];
			
			var indices:Vector.<uint> = new <uint>[
				3,1,0,1,3,2
			];
			
		
			if (shaderProgram != null) {
				shaderProgram.dispose();
				shaderProgram = null;	
			}
			
			shaderProgram = context.createProgram();
			
			if (CloudSky.vertexCodeBytes == null || CloudSky.fragmentCodeBytes == null) {
				var vertexProgram:Array = [
					"mov v0, a1",
					"mov o0, a0"
				];
				
				// c0:scales0,weights0,scales1,weights1
				// c1:0.5, 255, 1.0, 0
				// c2:corver(0.45), sharpness(0.94)
				// c3:cloudColor
				// c4:skyColor
				var fragmentProgram:Array = [
					"add t3, v0, c5.xy",
					"mul t1, c0.xxxx, t3",
					//"frc t1, t1",
					"tex t0, t1, s0 <2d,repeat,linear, miplinear," + Defines.TEX_FORMATS[texFormat] + ">",
					"sub t0.x, t0.x, c1.x",
					"mul t2.x, t0.x, c0.y",
					
					"add t3, v0, c5.zw",
					"mul t1, c0.zzzz, t3",
					//"frc t1, t1",
					"tex t0, t1, s0 <2d,repeat,linear, miplinear," + Defines.TEX_FORMATS[texFormat] + ">",
					"sub t0.x, t0.x, c1.x",
					"mul t0.x, t0.x, c0.w",
					"add t2.x, t2.x, t0.x",
					
					"mul t2.x, t2.x, c1.xxxx",
					"add t2.x, t2.x, c1.xxxx",
					"sub t2.x, t2.x, c2.x",
					"max t2.x, t2.x, c1.w",
					"mul t2.x, t2.x, c1.y",
					"mov t3.x, c2.y",
					"pow t2.x, t3.x, t2.x",
					"sub t2.x, t2.x, c1.z",
					"neg t2.x, t2.x",
					
					"mov t4, c4",
					"mov t1, c3",
					"sub t1, t1, t4",
					"mul t1, t1, t2.xxxx",
					"add t1, t1, t4",
					
					"mov t4, c6",
					"sub t2.x, t1.w, t4.y",
					"kil t2.x",
					
					"mov o0, t1"
				];
				
				var proc:Procedure;
				
				proc = new Procedure(vertexProgram);
				CloudSky.vertexCodeBytes = proc.getByteCode(Context3DProgramType.VERTEX);
				
				proc = new Procedure(fragmentProgram);
				CloudSky.fragmentCodeBytes = proc.getByteCode(Context3DProgramType.FRAGMENT);
			}
			
			shaderProgram.upload(CloudSky.vertexCodeBytes, CloudSky.fragmentCodeBytes);
			
			var vertexBuffer:VertexBuffer3D;
			var indexBuffer:IndexBuffer3D;
			
			try {
				vertexBuffer = context.createVertexBuffer(4, 5);
				CONFIG::DEBUG {
					Global.vertexBuffersCount++;
				}
				vertexBuffer.uploadFromVector(verts, 0, 4);
				indexBuffer = context.createIndexBuffer(6);
				CONFIG::DEBUG {
					Global.indexBuffersCount++;
				}
				indexBuffer.uploadFromVector(indices, 0, 6);
				
				CloudSky.vertexBuffer = vertexBuffer;
				CloudSky.indexBuffer = indexBuffer;
			} catch(e:Error) {
				
				if (e.errorID == 3672) {
					Global.needDisposeContext3d = true;
				}
				if (vertexBuffer != null) {
					vertexBuffer.dispose();
					CONFIG::DEBUG {
						Global.vertexBuffersCount--;
					}
				}
				
				if (indexBuffer != null) {
					indexBuffer.dispose();
					CONFIG::DEBUG {
						Global.indexBuffersCount--;
					}
				}
				
				CONFIG::DEBUG {
					Global.warning("CloudSky::create3D" + e.errorID + "," + e.message);
				}
					
				CloudSky.vertexBuffer = null;
				CloudSky.indexBuffer = null;
			}
			return shaderProgram;
		}
		
		public function setCloudSpeed(speed:Number):void
		{
			cloudSpeed = speed;
		}
		
		override FlashGE function collectRenderUnits(camera:Camera3D, isShadowPass:Boolean):int 
		{
			var mat:Material = this.getMaterial();
			if (mat == null || mat.textureFilesCount == 0) {
				return 0;
			}

			var tex:TextureFile = mat.getTexture(0);
			if (tex == null || tex.getTexture(camera.context3D) == null) {
				return 0;	
			}
			
			var texFormatID:int = mat.getTexture(0).getFormatID();
			var curProgram:Program3D = CloudSky.shaderProgram;
			var changed:Boolean = false;
			if (shaderProgram == null || context3DUsed != camera.context3D) {				
				curProgram = create3D(camera, texFormatID);
				context3DUsed = camera.context3D;
				changed = true;
			}
			
			var renderer:Renderer = camera.renderer;
			if (renderUnit == null) {
				renderUnit = camera.pool.popRenderUnit();
				renderUnit.object = this;
				renderUnit.firstIndex = 0;
				renderUnit.culling = "front";
				renderUnit.numTriangles = 2;
			}
			
			if (changed) {			
				renderUnit.program = curProgram;
				renderUnit.indexBuffer = CloudSky.indexBuffer;
				renderUnit.vertexBuffersLength = 0;
				renderUnit.setVertexBufferAt(0, CloudSky.vertexBuffer, 0, Context3DVertexBufferFormat.FLOAT_3);
				renderUnit.setVertexBufferAt(1, CloudSky.vertexBuffer, 3, Context3DVertexBufferFormat.FLOAT_2);
				renderUnit.setFragmentConstantsFromNumbers(0, 1, 1, 2, 0.5);
				renderUnit.setFragmentConstantsFromNumbers(1, 0.5, 255, 1.0,0);
				renderUnit.setFragmentConstantsFromNumbers(2, 0.50, 0.94, 0);
				renderUnit.setFragmentConstantsFromNumbers(3, 1, 1, 1, 0.612);
				renderUnit.setFragmentConstantsFromNumbers(4, 0, 0.4966, 1, 1);
				renderUnit.setFragmentConstantsFromNumbers(6, 1, 0.00, 0, 0);
				renderUnit.srcBlend = Defines.BLEND_FACTORS[mat.getSrcBlend()];
				renderUnit.dstBlend = Defines.BLEND_FACTORS[mat.getDstBlend()];
			}
			
			var time:Number = getTimer();
			renderUnit.setFragmentConstantsFromNumbers(5,-time*cloudSpeed/100000, -time*cloudSpeed/100000, -time*cloudSpeed/1000000, -time*cloudSpeed/100000);
			renderUnit.texturesLength = 0;
			renderUnit.setTextureAt(0, mat.getTexture(0));
			
			renderer.addRenderUnit(renderUnit, Defines.PRIORITY_BACKGROUD);
		
			return 1;
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			if (renderUnit != null) {
				renderUnit.release();
			}
		}
	}
}