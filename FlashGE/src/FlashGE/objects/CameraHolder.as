package FlashGE.objects
{
	import FlashGE.objects.Entity;
	import FlashGE.render.Camera3D;

	public class CameraHolder extends Entity
	{
		public function CameraHolder(camera:Camera3D)
		{
			super();
			this.addChild(camera);
		}
		
		public function relayout():void
		{
			
		}
	}
}