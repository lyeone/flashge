// 2014-3-10 lye
package FlashGE.objects 
{
	import FlashGE.FlashGE;
	import FlashGE.common.Pool;
	import FlashGE.render.RenderUnit;

	use namespace FlashGE;

	public class Surface 
	{
		FlashGE var materialKey:String;	
		FlashGE var materialKeyBase:String = "";	
		FlashGE var indexBegin:int = 0;
		FlashGE var trianglesNum:int = 0;
		FlashGE var object:Entity;
		FlashGE var texIndex:int = 0;
		FlashGE var renderUnit0:RenderUnit;
		FlashGE var renderUnit1:RenderUnit;
		FlashGE var surfaceIndex:int = 0;
		FlashGE var forceResetUnit:Boolean = false;
		
		public function clearRenderUnits():void
		{
			if (this.renderUnit0 != null) {
				this.renderUnit0.release();
				this.renderUnit0 = null;
			}
			
			if (this.renderUnit1 != null) {
				this.renderUnit1.release();
				this.renderUnit1 = null;
			}
			
			this.materialKey = null;
		}
	}
}
