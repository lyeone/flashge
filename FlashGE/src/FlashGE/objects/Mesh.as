
package FlashGE.objects {

	import flash.utils.getQualifiedClassName;
	
	import FlashGE.FlashGE;
	import FlashGE.common.AxisAlignedBox;
	import FlashGE.common.Defines;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.loaders.MeshContext;
	import FlashGE.loaders.MeshLoader;
	import FlashGE.materials.Material;
	import FlashGE.render.Camera3D;
	import FlashGE.resources.Geometry;
	import FlashGE.resources.TextureFile;

	use namespace FlashGE;

	public class Mesh extends Entity
	{
		FlashGE var geometry:Geometry;
		FlashGE var surfaces:Vector.<Surface> = new Vector.<Surface>();
		FlashGE var surfacesLength:int = 0;
		FlashGE var meshLoader:MeshLoader;
		FlashGE var dataIndex:int = 0;
		FlashGE var hasDisposed:Boolean = false;
		FlashGE var meshContext:MeshContext;
		
		public function toString():String
		{
			var ret:String = "Mesh(name=" + this.name;
			ret += (",dataIndex=" + this.dataIndex);
			ret += (",surfacesLength=" + this.surfacesLength);
			ret += (",hasDisposed="+this.hasDisposed);
			ret += (",meshLoader=" + this.meshLoader);
			ret += ")";
			return ret;
		}
		
		public function setGeometry(newGeometry:Geometry):void
		{
			if (this.surfaces != null) {
				for (var i:int = 0; i < this.surfacesLength; i++) {
					this.surfaces[i].clearRenderUnits();
				}
				//this.surfaces.length = 0;
			}
			//this.surfacesLength = 0;
			if (this.geometry != null) {
				this.geometry.release();
				this.geometry = null;
			}
			
			this.geometry = newGeometry;
			if (this.geometry != null) {
				this.geometry.addRef();
			}
		}
		
		public function dispose():void
		{
			this.hasDisposed = true;
			this.isValid = false;
			if (this.surfaces != null) {
				for (var i:int = 0; i < this.surfacesLength; i++) {
					this.surfaces[i].clearRenderUnits();
				}
				this.surfaces.length = 0;
			}
			
			if (this.geometry != null) {
				this.geometry.release(this.meshLoader != null);
				this.geometry = null;
			}
			
			if (this.material != null) {
				this.material.release();
				this.material = null;
			}
			this.surfacesLength = 0;
			
			if (this.meshLoader != null) {
				this.meshLoader.release();
				this.meshLoader = null;
			}
			
			if (getQualifiedClassName(this).indexOf("::Mesh") >=0) {
				var pool:Pool = Pool.getInstance();
				pool.pushObject(pool.ClassMesh, this);
			}
		}
		
		override public function clearRenderUnits():void
		{
			for (var i:int = 0; i < this.surfacesLength; i++) {
				this.surfaces[i].clearRenderUnits();
			}
		}
		
		public function addSurface(indexBegin:uint, numTriangles:uint, texIndex:int = -1):Surface 
		{
			var res:Surface = new Surface();
			res.object = this;
			res.indexBegin = indexBegin;
			res.trianglesNum = numTriangles;
			res.texIndex = texIndex;
			res.renderUnit0 = null;
			res.renderUnit1 = null;
			surfaces[surfacesLength++] = res;
			return res;
		}

		public function getSurface(index:int):Surface 
		{
			return surfaces[index];
		}

		public function get numSurfaces():int 
		{
			return surfacesLength;
		}
		
		override FlashGE function calcProgramKey(camera:Camera3D):void
		{
			this.programKey = "mesh";
		}

		override FlashGE function updateBoundBox(boundBox:AxisAlignedBox, transform:Transform3D = null):void 
		{
			if (geometry != null) {
				geometry.updateBoundBox(boundBox, transform);
			}
		}
		
		override FlashGE function collectRenderUnits(camera:Camera3D, isShadowPass:Boolean):int 
		{
			if (this.isValid == false) {
				return 0;
			}
			
			var renderCount:int = 0;
			var mat:Material = this.getMaterial();
			for (var i:int = 0; i < this.surfacesLength; i++) {
				var surface:Surface = surfaces[i];
				renderCount += this.getMaterial().collectDraws(camera, surface, geometry);
				CONFIG::CACHE_PROGRAM_KEY {
					if (meshContext != null && meshContext.materialKey == null && this.material.type == Defines.MATERIAL_MODEL) {
						meshContext.materialKey = surface.materialKey;
					}
				}
			}
			CONFIG::CACHE_PROGRAM_KEY {
				if (meshContext != null && this.material.programCachedID != null && this.meshContext.programCachedID == null && this.material.type == Defines.MATERIAL_MODEL) {
					this.meshContext.programCachedID = this.material.programCachedID;
				}
			}
			return renderCount;
		}
		
		override public function setMaterial(src:Material):void
		{
			super.setMaterial(src);
			for (var i:int = 0, n:int = this.surfacesLength; i < n; i++) {
				this.surfaces[i].materialKey = null;
			}
		}
		
		override public function resetMaterials():void
		{
			if (this.meshLoader == null) {
				return;
			}
			var mat:Material = Material.createMaterial();
			this.setMaterial(mat);
			var index:int = 0;
			if (meshContext.diffuseTexture.length > 0) {
				var tex:TextureFile = TextureFile.createFromFile(meshContext.diffuseTexture, true);
				mat.setTexture(index++, tex);
				tex.release();
			}
			
			mat.setOpenDoubleAlpha(false);
			mat.setSrcBlend(Defines.BLEND_ONE);
			mat.setDstBlend(Defines.BLEND_ZERO);
			mat.setMipIndex(2);
			mat.setCloseUVTrans(true);
			mat.setAlphaTest(meshContext.openAlphaTest ? 1 : 0);
			mat.enableUseLight(meshLoader.useLight);
			mat.setRecieveShadow(meshLoader.recieveShadow);
			mat.setType(meshLoader.materialType);
			mat.setUVV0(0);
			mat.setUVV1(0);
			
			if (meshLoader.uvContext != null) {
				var name:String = meshLoader.meshNameList[this.dataIndex];
				var uv:Object = meshLoader.uvContext[name];
				if (uv != null) {
					mat.setUVV0(Number(uv[1]));
					mat.setUVV1(Number(uv[2]));
				}
			}
			mat.release();
		}
		
		public function initFromMeshLoader(loader:MeshLoader, dataIndex:int):void
		{
			var g:Geometry = loader.geometryList[dataIndex];
			
			this.dataIndex = dataIndex;
			this.meshLoader = loader;
			this.meshLoader.addRef();
			this.userData = loader.userData;
			this.setGeometry(g);
			this.addSurface(0, g.indicesBuffer.length/3);
			this.setName(loader.meshNameList[dataIndex]);
			this.meshContext = loader.meshContextList[dataIndex];
			this.surfaces[0].materialKey = this.meshContext.materialKey;
			resetMaterials();
			this.material.programCachedID = this.meshContext.programCachedID;
		}
	}
}
