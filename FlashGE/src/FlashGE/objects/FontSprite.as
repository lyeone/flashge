// 2014-3-10 lye
package FlashGE.objects
{
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display3D.Context3D;
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Context3DVertexBufferFormat;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.Program3D;
	import flash.display3D.VertexBuffer3D;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.common.Transform3D;
	import FlashGE.materials.compiler.Procedure;
	import FlashGE.render.Camera3D;
	import FlashGE.render.RenderUnit;
	import FlashGE.resources.TextureFile;

	CONFIG::DEBUG {
	import FlashGE.common.Global;
	}
	
	use namespace FlashGE;
	
	public class FontSprite extends Entity
	{
		static private var curIndexBuffer:IndexBuffer3D;
		static private var curVertexBuffer:VertexBuffer3D;
		static private var curProgram3D:Program3D;
		static private var context3DUsed:Context3D;
		static private var vertexBytesCode:ByteArray;
		static private var fragmentBytesCode:ByteArray;
		
		private var fontTexture:TextureFile;
		private var renderUnit:RenderUnit;
		private var baseTransform:Transform3D
		private var alignWidth:Number;
		private var alignHeight:Number;
		private var realWidth:Number;
		
		private var needUpload:Boolean = true;
		private var usedTextSprite:DisplayObject;
		
		private var usedContexted:Context3D;
		
		public function getTexture():TextureFile
		{
			return this.fontTexture;
		}
		
		public function dispose():void
		{
			if (renderUnit != null) {
				renderUnit.release();
				renderUnit = null;
			}
			
			if (this.parent != null) {
				this.parent.removeChild(this);
			}
			
			if (fontTexture != null) {
				fontTexture.dispose();
				fontTexture = null;
			}
			baseTransform = null;
			
			this.usedTextSprite = null;
			this.needUpload = true;
		}
		
		public function getNeedUpload():Boolean
		{
			return this.needUpload;
		}
		
		public function setBaseTransform(base:Transform3D):void
		{
			this.baseTransform = base;
		}
		
		public function create3DBuffers(context3D:Context3D):void
		{
			if (renderUnit != null) {				
				renderUnit.vertexBuffersLength = 0;				
				renderUnit.indexBuffer = null;
				renderUnit.program = null;
			}
			
			if (curIndexBuffer != null) {
				curIndexBuffer.dispose();
				curIndexBuffer = null;
			}
			
			if (curVertexBuffer != null) {
				curVertexBuffer.dispose();
				curVertexBuffer = null;
			}
			
			if (curProgram3D != null) {
				curProgram3D.dispose();
				curProgram3D = null;
			}
			
			var i:int, n:int;
			var vertices:Vector.<Number> = new Vector.<Number>();
			var indices:Vector.<uint> = new Vector.<uint>();
			vertices.push(
					-1,-1, 0, 0, 1, 0,
					-1, 1, 0, 0, 0, 0,
					 1, 1, 0, 1, 0, 0,
					 1,-1, 0, 1, 1, 0);
				
			indices.push(3, 1, 0, 1, 3, 2);
			
			curIndexBuffer = context3D.createIndexBuffer(6);
			CONFIG::DEBUG {
				Global.indexBuffersCount++;
			}
			curIndexBuffer.uploadFromVector(indices, 0, 6);
			
			curVertexBuffer = context3D.createVertexBuffer(4, 6);
			CONFIG::DEBUG {
				Global.vertexBuffersCount++;
			}
			curVertexBuffer.uploadFromVector(vertices, 0, 4);
			
			// c0 : alignWidth, alignHeight, viewWidth, viewHeight
			// c1 : x, y, z, 0
			if (FontSprite.fragmentBytesCode == null || FontSprite.vertexBytesCode == null) {
				var shaderIndex:int = 0;
				var vertShaderArray:Array = [];
				vertShaderArray[shaderIndex++] = "mov t1, c0";
				vertShaderArray[shaderIndex++] = "mov t2, c1";
				vertShaderArray[shaderIndex++] = "div t3.xy, t2.xy, t1.zw";
				
				vertShaderArray[shaderIndex++] = "div t1.xy, t1.xy, t1.zw";
				vertShaderArray[shaderIndex++] = "mov t0, a0";
				vertShaderArray[shaderIndex++] = "mul t0.xy, t0.xy, t1.xy";
				vertShaderArray[shaderIndex++] = "add t0.xy, t0.xy, t3.xy";
				//vertShaderArray[shaderIndex++] = "add t0.xy, t0.xy, t3.xy";
				
				vertShaderArray[shaderIndex++] = "mov t0.z, t2.z";
				vertShaderArray[shaderIndex++] = "mov o0, t0";
				
				vertShaderArray[shaderIndex++] = "mov v0, a1";
				
				shaderIndex = 0;
				var fragShaderArray:Array = [];
				fragShaderArray[shaderIndex++] = "tex t0, v0.xy, s0 <2d, nearest, repeat, nomip>";
				fragShaderArray[shaderIndex++] = "mov o0, t0";
				
				var vertProcedure:Procedure = new Procedure(vertShaderArray);
				var fragProcedure:Procedure = new Procedure(fragShaderArray);
				
				FontSprite.vertexBytesCode = vertProcedure.getByteCode(Context3DProgramType.VERTEX);
				FontSprite.fragmentBytesCode = fragProcedure.getByteCode(Context3DProgramType.FRAGMENT); 
			}
			
			curProgram3D = context3D.createProgram();
			curProgram3D.upload(FontSprite.vertexBytesCode, FontSprite.fragmentBytesCode);
			context3DUsed = context3D;
		}
		
		public function setTextSprite(textSprite:DisplayObject):void
		{
			if (usedTextSprite != textSprite) {
				usedTextSprite = textSprite;
				needUpload = true;
			}
		}
		public function setNeedUpload(v:Boolean):void
		{
			this.needUpload = true;
		}
		
		public function uploadTexture():void 
		{
			if (usedTextSprite == null || usedTextSprite.width == 0 || usedTextSprite.height == 0) {
				return;
			}
			
			var bmd:BitmapData = new BitmapData(usedTextSprite.width, usedTextSprite.height, true, 0);
			bmd.draw(usedTextSprite);
			var rect:Rectangle = new Rectangle(0, 0, bmd.width, bmd.height);
			var bitmapBytes:ByteArray = new ByteArray;
			bitmapBytes.endian = Endian.LITTLE_ENDIAN;
			bmd.copyPixelsToByteArray(rect, bitmapBytes);
			bitmapBytes.position = 0;
			if (fontTexture == null) {
				fontTexture = new TextureFile;
			}
			
			fontTexture.uploadFromByteArray(bitmapBytes, bmd.width, bmd.height, true);	
			bmd.dispose();
			this.alignWidth = fontTexture.getAlignWidth();
			this.alignHeight = fontTexture.getAlignHeight();
			this.realWidth = fontTexture.getRealWidth();
		}
		
		override FlashGE function collectRenderUnits(camera:Camera3D, isShadowPass:Boolean):int 
		{
			if (camera.closeCollectRenderUnits) {
				return 0;
			}
			
			if (this.needUpload) {
				this.needUpload = false;
				uploadTexture();
			}
			
			if (this.fontTexture == null || this.fontTexture.getTexture(camera.context3D) == null) {
				return 0;
			}
			
			var needSet:Boolean = false;
			if (context3DUsed != camera.context3D) {
				context3DUsed = camera.context3D;
				this.create3DBuffers(context3DUsed);
				needSet = true;
			}
			
			if (curIndexBuffer == null || curVertexBuffer == null || curProgram3D == null) {
				return 0;
			}
			
			if (renderUnit == null) {
				renderUnit = camera.pool.popRenderUnit();
				renderUnit.object = this;
				renderUnit.srcBlend = Context3DBlendFactor.SOURCE_ALPHA;
				renderUnit.dstBlend = Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA;
				renderUnit.numTriangles = 2;
				if (renderUnit.culling != "front") {
					renderUnit.culling = "front";
				}
				needSet = true;
			}
			
			if (this.usedContexted != camera.context3D) {
				this.usedContexted = camera.context3D;
				needSet = true;
			}

			if (needSet) {
				renderUnit.vertexBuffersLength = 0;				
				renderUnit.setVertexBufferAt(0, curVertexBuffer, 0, Context3DVertexBufferFormat.FLOAT_3);
				renderUnit.setVertexBufferAt(1, curVertexBuffer, 3, Context3DVertexBufferFormat.FLOAT_3);
				renderUnit.indexBuffer = curIndexBuffer;
				renderUnit.program = curProgram3D;
				renderUnit.texturesLength = 0;
				renderUnit.setTextureAt(0, fontTexture);
			}
			
			renderUnit.setVertexConstantsFromNumbers(0, this.alignWidth, this.alignHeight, camera.view.viewWidth, camera.view.viewHeight);
			
			var alignX:int = int((this.x - camera.view.viewWidth * 0.5) * 2 + this.alignWidth- this.realWidth);
			if (camera.view.viewWidth % 2 == 0) {
				alignX = alignX % 2 + alignX;
			}
			
			var alignY:int = int((camera.view.viewHeight * 0.5 - this.y) * 2);// + fontTexture.getAlignHeight() - fontTexture.getRealHeight());
			if (camera.view.viewHeight % 2 == 0) {
				alignY = alignY % 2 + alignY;
			}
			if (this.baseTransform != null) {
				var zz:Number = this.baseTransform.l*camera.m10 + camera.m14;
				renderUnit.setVertexConstantsFromNumbers(1, alignX, alignY, zz * 0.9, 0);
			} else {
				renderUnit.setVertexConstantsFromNumbers(1, alignX, alignY, 0, 0);
			}
			
			camera.renderer.addRenderUnit(renderUnit, Defines.PRIORITY_FONT);
			return 1;
		}
	}
}