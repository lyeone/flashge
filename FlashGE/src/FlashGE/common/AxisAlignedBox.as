package FlashGE.common
{
	import flash.geom.Vector3D;
	
	import FlashGE.FlashGE;

	use namespace FlashGE;

	public class AxisAlignedBox 
	{
		public var minX:Number = 1e+22;
		public var minY:Number = 1e+22;
		public var minZ:Number = 1e+22;
		public var maxX:Number = -1e+22;
		public var maxY:Number = -1e+22;
		public var maxZ:Number = -1e+22;
		
		public function reset():void
		{
			minX = 1e+22;
			minY = 1e+22;
			minZ = 1e+22;
			maxX = -1e+22;
			maxY = -1e+22;
			maxZ = -1e+22;
		}
		
		public function scale(scaleX:Number, scaleY:Number, scaleZ:Number):void
		{
			minX *= scaleX;
			maxX *= scaleX;
			
			minY *= scaleY;
			maxY *= scaleY;
			
			minZ *= scaleZ;
			maxZ *= scaleZ;
		}

		public function toString():String
		{
			return "AxisAlignedBox[" + this.minX + "," + this.minY+ "," + this.minZ + "-" + this.maxX + "," + this.maxY + "," + this.maxZ + "]";
		}
		
		FlashGE function checkFrustumCulling(frustum:FrustumPlane, culling:int):int
		{
			var side:int = 1;
			for (var plane:FrustumPlane = frustum; plane != null; plane = plane.next) {
				if (culling & side) {
					if (plane.x >= 0)
						if (plane.y >= 0)
							if (plane.z >= 0) {
								if (maxX*plane.x + maxY*plane.y + maxZ*plane.z <= plane.offset) 
									return -1;
								if (minX*plane.x + minY*plane.y + minZ*plane.z > plane.offset) 
									culling &= (63 & ~side);
							} else {
								if (maxX*plane.x + maxY*plane.y + minZ*plane.z <= plane.offset) 
									return -1;
								if (minX*plane.x + minY*plane.y + maxZ*plane.z > plane.offset) 
									culling &= (63 & ~side);
							}
						else
							if (plane.z >= 0) {
								if (maxX*plane.x + minY*plane.y + maxZ*plane.z <= plane.offset) 
									return -1;
								if (minX*plane.x + maxY*plane.y + minZ*plane.z > plane.offset) 
									culling &= (63 & ~side);
							} else {
								if (maxX*plane.x + minY*plane.y + minZ*plane.z <= plane.offset)
									return -1;
								if (minX*plane.x + maxY*plane.y + maxZ*plane.z > plane.offset) 
									culling &= (63 & ~side);
							}
					else if (plane.y >= 0)
						if (plane.z >= 0) {
							if (minX*plane.x + maxY*plane.y + maxZ*plane.z <= plane.offset) 
								return -1;
							if (maxX*plane.x + minY*plane.y + minZ*plane.z > plane.offset) 
								culling &= (63 & ~side);
						} else {
							if (minX*plane.x + maxY*plane.y + minZ*plane.z <= plane.offset) 
								return -1;
							if (maxX*plane.x + minY*plane.y + maxZ*plane.z > plane.offset) 
								culling &= (63 & ~side);
						}
					else if (plane.z >= 0) {
						if (minX*plane.x + minY*plane.y + maxZ*plane.z <= plane.offset) 
							return -1;
						if (maxX*plane.x + maxY*plane.y + minZ*plane.z > plane.offset) 
							culling &= (63 & ~side);
					} else {
						if (minX*plane.x + minY*plane.y + minZ*plane.z <= plane.offset) 
							return -1;
						if (maxX*plane.x + maxY*plane.y + maxZ*plane.z > plane.offset) 
							culling &= (63 & ~side);
					}
				}
				side <<= 1;
			}
			return culling;
		}


		FlashGE function checkSphere(sphere:Vector3D):Boolean 
		{
			return sphere.x + sphere.w > minX && 
				sphere.x - sphere.w < maxX && 
				sphere.y + sphere.w > minY && 
				sphere.y - sphere.w < maxY && 
				sphere.z + sphere.w > minZ && 
				sphere.z - sphere.w < maxZ;
		}

		public function clone():AxisAlignedBox
		{
			var res:AxisAlignedBox = new AxisAlignedBox();
			res.minX = minX;
			res.minY = minY;
			res.minZ = minZ;
			res.maxX = maxX;
			res.maxY = maxY;
			res.maxZ = maxZ;
			return res;
		}
	}
}
