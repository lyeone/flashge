// 2014-3-10 lye
package FlashGE.common
{
	import flash.display.Sprite;
	
	import FlashGE.FlashGE;
	import FlashGE.effects.MaterialUnit;
	import FlashGE.effects.ModelUnit;
	import FlashGE.effects.ParticleUnit;
	import FlashGE.effects.PolyTrailUnit;
	import FlashGE.effects.SpriteUnit;
	
	CONFIG::LOGFIELD {
	import flash.filters.GlowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	}
	public class Global
	{
		static public var frameCount:uint = 0;
		static public var FPS:int = 30;
		static public const MAX_VIDEO_MEMORY:int = 100 * 1024 * 1024;
		static public var useCacheData:Boolean = true;
		static public var videoMemoryCount:int = 0;
		static public var mainSprite:Sprite;
		static public var needDisposeContext3d:Boolean = false;
		static public var particlesErrorCount:int = 0;
		
		CONFIG::LOGFIELD {
			static private var logField:TextField;
		}
		// debug
		CONFIG::DEBUG {
			
			static FlashGE var geometrysCount:int = 0;
			static FlashGE var vertexBuffersCount:int = 0;
			static FlashGE var indexBuffersCount:int = 0;
			static FlashGE var texturesCount:int = 0;
			static FlashGE var particlesCount:int = 0;
			static FlashGE var maxParticlesCount:int = 0;
			static FlashGE var drawsCount:int = 0;
		}
	
		static public function initStaticContext():void
		{
			SpriteUnit.initStaticContext();
			MaterialUnit.initStaticContext();
			ModelUnit.initStaticContext();
			ParticleUnit.initStaticContext();
			PolyTrailUnit.initStaticContext();
		}
	
		import flash.utils.getTimer;
		public static function log(...args):void
		{
			CONFIG::LOGFIELD {
			
				if (logField == null) {
					logField = new TextField;
					logField.textColor = 0x0080C0;
					logField.defaultTextFormat = new TextFormat("微软雅黑,Arial", 13);
					logField.filters = [new GlowFilter(0xffffff, 0.8, 2, 2, 8)];
					//logField.autoSize = TextFieldAutoSize.LEFT;
					
					mainSprite.stage.addChild(logField);
				}
				
				logField.width = 800;
				logField.height = 200;
				logField.appendText(flash.utils.getTimer() + "\t"+ String(args) + "\n");
				logField.scrollV = logField.maxScrollV;
			}
			trace("["+String(args)+"]");
		}
		
		public static function warning(...arg):void
		{
			Global.log(Global.frameCount+ "warning[" +arg + "]");
		}
		
		public static function throwError(msg:*, id:* = 0):void
		{
			Global.log(Global.frameCount, "throwError[", msg, id, "]");
			CONFIG::DEBUG {
				throw new Error(msg, id);
			}
		}
	}
}