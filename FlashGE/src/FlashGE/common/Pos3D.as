package FlashGE.common
{
	
	import FlashGE.FlashGE;

	use namespace FlashGE;

	public class Pos3D 
	{
		public function Pos3D(x:Number = 0, y:Number = 0, z:Number = 0)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}
		
		public function normalize(newLen:Number):void
		{
			var len:Number = length();
			if (len != 0) {
				len = newLen / len;
				x *= len;
				y *= len;
				z *= len;
			}
		}
		
		public function clone():Pos3D
		{
			var ret:Pos3D = new Pos3D;
			ret.x = this.x;
			ret.y = this.y;
			ret.z = this.z;
			return ret;
		}
		
		public function copy(src:Pos3D):void
		{
			this.x = src.x;
			this.y = src.y;
			this.z = src.z;
		}
		
		public function subtract(v:Pos3D, out:Pos3D):Pos3D
		{
			if (out == null) {
				out = new Pos3D;
			}
			out.x = this.x - v.x;
			out.y = this.y - v.y;
			out.z = this.z - v.z;
			return out;
		}
		
		public function add(v:Pos3D, out:Pos3D):Pos3D
		{
			if (out == null) {
				out = new Pos3D;
			}
			out.x = this.x + v.x;
			out.y = this.y + v.y;
			out.z = this.z + v.z;
			return out;
		}
		
		public function crossProduct(b:Pos3D, out:Pos3D):Pos3D
		{
			if (out == null) {
				out = new Pos3D;
			}
			
			out.x = this.y * b.z - this.z * b.y;
			out.y = this.z * b.x - this.x * b.z;
			out.z = this.x * b.y - this.y * b.x;
			
			return out;
		}
		
		public function scaleBy(v:Number):void
		{
			
		}
		
		public function length():Number
		{
			return Math.sqrt( x * x + y * y + z * z);
		}
		
		public var x:Number = 0;
		public var y:Number = 0;
		public var z:Number = 0;
		
		public function setTo(a:Number, b:Number, c:Number):void
		{
			this.x = a;
			this.y = b;
			this.z = c;
		}
	}
}