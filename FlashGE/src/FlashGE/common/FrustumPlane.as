package FlashGE.common 
{	
	public class FrustumPlane 
	{
		public var x:Number;
		public var y:Number;
		public var z:Number;
		public var offset:Number;		
		public var next:FrustumPlane;		
	}
}
