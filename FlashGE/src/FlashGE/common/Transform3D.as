package FlashGE.common
{
	import flash.geom.Vector3D;
	import flash.utils.Dictionary;
	
	import FlashGE.FlashGE;

	use namespace FlashGE;

	public class Transform3D 
	{
		static public var IDENTITY:Transform3D = new Transform3D;
		
		public var a:Number = 1;
		public var b:Number = 0;
		public var c:Number = 0;
		public var d:Number = 0;

		public var e:Number = 0;
		public var f:Number = 1;
		public var g:Number = 0;
		public var h:Number = 0;

		public var i:Number = 0;
		public var j:Number = 0;
		public var k:Number = 1;
		public var l:Number = 0;
		
		public function identity():void 
		{
			a = 1;
			b = 0;
			c = 0;
			d = 0;
			e = 0;
			f = 1;
			g = 0;
			h = 0;
			i = 0;
			j = 0;
			k = 1;
			l = 0;
		}
		
		public function toString():String
		{
			return "Transform3d:" +a + ',' + b + ',' + c + ',' + d + ',' +
				e + ',' + f + ',' + g + ',' + h + ',' +
				i + ',' + j + ',' + k + ',' + l;
		}
		
		public function compose(x:Number, y:Number, z:Number, 
								rotationX:Number, rotationY:Number, rotationZ:Number, 
								scaleX:Number, scaleY:Number, scaleZ:Number):void
		{
			var cosX:Number = Math.cos(rotationX);
			var sinX:Number = Math.sin(rotationX);
			var cosY:Number = Math.cos(rotationY);
			var sinY:Number = Math.sin(rotationY);
			var cosZ:Number = Math.cos(rotationZ);
			var sinZ:Number = Math.sin(rotationZ);
			var cosZsinY:Number = cosZ*sinY;
			var sinZsinY:Number = sinZ*sinY;
			var cosYscaleX:Number = cosY*scaleX;
			var sinXscaleY:Number = sinX*scaleY;
			var cosXscaleY:Number = cosX*scaleY;
			var cosXscaleZ:Number = cosX*scaleZ;
			var sinXscaleZ:Number = sinX*scaleZ;
			a = cosZ*cosYscaleX;
			b = cosZsinY*sinXscaleY - sinZ*cosXscaleY;
			c = cosZsinY*cosXscaleZ + sinZ*sinXscaleZ;
			d = x;
			e = sinZ*cosYscaleX;
			f = sinZsinY*sinXscaleY + cosZ*cosXscaleY;
			g = sinZsinY*cosXscaleZ - cosZ*sinXscaleZ;
			h = y;
			i =-sinY*scaleX;
			j = cosY*sinXscaleY;
			k = cosY*cosXscaleZ;
			l = z;
		}
		
		public function composePosition(x:Number, y:Number, z:Number):void
		{
			a = 1;
			b = 0;
			c = 0;
			d = x;
			e = 0;
			f = 1;
			g = 0;
			h = y;
			i = 0;
			j = 0;
			k = 1;
			l = z;
		}
		
		public function composeScale(scaleX:Number, scaleY:Number, scaleZ:Number):void
		{
			a = scaleX;
			b = 0;
			c = 0;
			d = 0;
			e = 0;
			f = scaleY;
			g = 0;
			h = 0;
			i = 0;
			j = 0;
			k = scaleZ;
			l = 0;
		}

		public function composeInverse(x:Number, y:Number, z:Number, 
									   rotationX:Number, rotationY:Number, rotationZ:Number, 
									   scaleX:Number, scaleY:Number, scaleZ:Number):void
		{
			var cosX:Number = Math.cos(rotationX);
			var sinX:Number = Math.sin(-rotationX);
			var cosY:Number = Math.cos(rotationY);
			var sinY:Number = Math.sin(-rotationY);
			var cosZ:Number = Math.cos(rotationZ);
			var sinZ:Number = Math.sin(-rotationZ);
			var sinXsinY:Number = sinX*sinY;
			var cosYscaleX:Number = cosY/scaleX;
			var cosXscaleY:Number = cosX/scaleY;
			var sinXscaleZ:Number = sinX/scaleZ;
			var cosXscaleZ:Number = cosX/scaleZ;
			a = cosZ*cosYscaleX;
			b = -sinZ*cosYscaleX;
			c = sinY/scaleX;
			d = -a*x - b*y - c*z;
			e = sinZ*cosXscaleY + sinXsinY*cosZ/scaleY;
			f = cosZ*cosXscaleY - sinXsinY*sinZ/scaleY;
			g = -sinX*cosY/scaleY;
			h = -e*x - f*y - g*z;
			i = sinZ*sinXscaleZ - cosZ*sinY*cosXscaleZ;
			j = cosZ*sinXscaleZ + sinY*sinZ*cosXscaleZ;
			k = cosY*cosXscaleZ;
			l = -i*x - j*y - k*z;
		}
		
		public function invert():void 
		{
			var ta:Number = a;
			var tb:Number = b;
			var tc:Number = c;
			var td:Number = d;
			var te:Number = e;
			var tf:Number = f;
			var tg:Number = g;
			var th:Number = h;
			var ti:Number = i;
			var tj:Number = j;
			var tk:Number = k;
			var tl:Number = l;
			var det:Number = 1/(-tc*tf*ti + tb*tg*ti + tc*te*tj - ta*tg*tj - tb*te*tk + ta*tf*tk);
			a = (-tg*tj + tf*tk)*det;
			b = (tc*tj - tb*tk)*det;
			c = (-tc*tf + tb*tg)*det;
			d = (td*tg*tj - tc*th*tj - td*tf*tk + tb*th*tk + tc*tf*tl - tb*tg*tl)*det;
			e = (tg*ti - te*tk)*det;
			f = (-tc*ti + ta*tk)*det;
			g = (tc*te - ta*tg)*det;
			h = (tc*th*ti - td*tg*ti + td*te*tk - ta*th*tk - tc*te*tl + ta*tg*tl)*det;
			i = (-tf*ti + te*tj)*det;
			j = (tb*ti - ta*tj)*det;
			k = (-tb*te + ta*tf)*det;
			l = (td*tf*ti - tb*th*ti - td*te*tj + ta*th*tj + tb*te*tl - ta*tf*tl)*det;
		}
		
		public function initFromVector(vector:Vector.<Number>):void 
		{
			a = vector[0];
			b = vector[1];
			c = vector[2];
			d = vector[3];
			e = vector[4];
			f = vector[5];
			g = vector[6];
			h = vector[7];
			i = vector[8];
			j = vector[9];
			k = vector[10];
			l = vector[11];
		}
		
		public function append(transform:Transform3D):void 
		{
			var ta:Number = a;
			var tb:Number = b;
			var tc:Number = c;
			var td:Number = d;
			var te:Number = e;
			var tf:Number = f;
			var tg:Number = g;
			var th:Number = h;
			var ti:Number = i;
			var tj:Number = j;
			var tk:Number = k;
			var tl:Number = l;
			a = transform.a*ta + transform.b*te + transform.c*ti;
			b = transform.a*tb + transform.b*tf + transform.c*tj;
			c = transform.a*tc + transform.b*tg + transform.c*tk;
			d = transform.a*td + transform.b*th + transform.c*tl + transform.d;
			e = transform.e*ta + transform.f*te + transform.g*ti;
			f = transform.e*tb + transform.f*tf + transform.g*tj;
			g = transform.e*tc + transform.f*tg + transform.g*tk;
			h = transform.e*td + transform.f*th + transform.g*tl + transform.h;
			i = transform.i*ta + transform.j*te + transform.k*ti;
			j = transform.i*tb + transform.j*tf + transform.k*tj;
			k = transform.i*tc + transform.j*tg + transform.k*tk;
			l = transform.i*td + transform.j*th + transform.k*tl + transform.l;
		}
		
		public function appendPosition(x:Number, y:Number, z:Number):void
		{
			d += x;
			h += y;
			l += z;
		}
		
		public function appendScales(scaleX:Number, scaleY:Number, scaleZ:Number):void
		{
			a *= scaleX;
			b *= scaleX;
			c *= scaleX;
			d *= scaleX;
			e *= scaleY;
			f *= scaleY;
			g *= scaleY;
			h *= scaleY;
			i *= scaleZ;
			j *= scaleZ;
			k *= scaleZ;
			l *= scaleZ;
		}

		public function prepend(transform:Transform3D):void 
		{
			var ta:Number = a;
			var tb:Number = b;
			var tc:Number = c;
			var td:Number = d;
			var te:Number = e;
			var tf:Number = f;
			var tg:Number = g;
			var th:Number = h;
			var ti:Number = i;
			var tj:Number = j;
			var tk:Number = k;
			var tl:Number = l;
			a = ta*transform.a + tb*transform.e + tc*transform.i;
			b = ta*transform.b + tb*transform.f + tc*transform.j;
			c = ta*transform.c + tb*transform.g + tc*transform.k;
			d = ta*transform.d + tb*transform.h + tc*transform.l + td;
			e = te*transform.a + tf*transform.e + tg*transform.i;
			f = te*transform.b + tf*transform.f + tg*transform.j;
			g = te*transform.c + tf*transform.g + tg*transform.k;
			h = te*transform.d + tf*transform.h + tg*transform.l + th;
			i = ti*transform.a + tj*transform.e + tk*transform.i;
			j = ti*transform.b + tj*transform.f + tk*transform.j;
			k = ti*transform.c + tj*transform.g + tk*transform.k;
			l = ti*transform.d + tj*transform.h + tk*transform.l + tl;

		}

		public function combine(transformA:Transform3D, transformB:Transform3D):void
		{
			a = transformA.a*transformB.a + transformA.b*transformB.e + transformA.c*transformB.i;
			b = transformA.a*transformB.b + transformA.b*transformB.f + transformA.c*transformB.j;
			c = transformA.a*transformB.c + transformA.b*transformB.g + transformA.c*transformB.k;
			d = transformA.a*transformB.d + transformA.b*transformB.h + transformA.c*transformB.l + transformA.d;
			e = transformA.e*transformB.a + transformA.f*transformB.e + transformA.g*transformB.i;
			f = transformA.e*transformB.b + transformA.f*transformB.f + transformA.g*transformB.j;
			g = transformA.e*transformB.c + transformA.f*transformB.g + transformA.g*transformB.k;
			h = transformA.e*transformB.d + transformA.f*transformB.h + transformA.g*transformB.l + transformA.h;
			i = transformA.i*transformB.a + transformA.j*transformB.e + transformA.k*transformB.i;
			j = transformA.i*transformB.b + transformA.j*transformB.f + transformA.k*transformB.j;
			k = transformA.i*transformB.c + transformA.j*transformB.g + transformA.k*transformB.k;
			l = transformA.i*transformB.d + transformA.j*transformB.h + transformA.k*transformB.l + transformA.l;
		}

		public function calculateInversion(source:Transform3D):void
		{
			var ta:Number = source.a;
			var tb:Number = source.b;
			var tc:Number = source.c;
			var td:Number = source.d;
			var te:Number = source.e;
			var tf:Number = source.f;
			var tg:Number = source.g;
			var th:Number = source.h;
			var ti:Number = source.i;
			var tj:Number = source.j;
			var tk:Number = source.k;
			var tl:Number = source.l;
			var det:Number = 1/(-tc*tf*ti + tb*tg*ti + tc*te*tj - ta*tg*tj - tb*te*tk + ta*tf*tk);
			a = (-tg*tj + tf*tk)*det;
			b = (tc*tj - tb*tk)*det;
			c = (-tc*tf + tb*tg)*det;
			d = (td*tg*tj - tc*th*tj - td*tf*tk + tb*th*tk + tc*tf*tl - tb*tg*tl)*det;
			e = (tg*ti - te*tk)*det;
			f = (-tc*ti + ta*tk)*det;
			g = (tc*te - ta*tg)*det;
			h = (tc*th*ti - td*tg*ti + td*te*tk - ta*th*tk - tc*te*tl + ta*tg*tl)*det;
			i = (-tf*ti + te*tj)*det;
			j = (tb*ti - ta*tj)*det;
			k = (-tb*te + ta*tf)*det;
			l = (td*tf*ti - tb*th*ti - td*te*tj + ta*th*tj + tb*te*tl - ta*tf*tl)*det;
		}

		public function copy(source:Transform3D):void 
		{
			a = source.a;
			b = source.b;
			c = source.c;
			d = source.d;
			e = source.e;
			f = source.f;
			g = source.g;
			h = source.h;
			i = source.i;
			j = source.j;
			k = source.k;
			l = source.l;
		}
		
		public function fromAxisAngle(axisX:Number, axisY:Number, axisZ:Number, angle:Number):void
		{
			var sin_a : Number = Math.sin(angle / 2);
			var cos_a : Number = Math.cos(angle / 2);
			
			var x:Number = axisX * sin_a;
			var y:Number = axisY * sin_a;
			var z:Number = axisZ * sin_a;
			var w:Number = cos_a;
			
			var mag : Number = 1.0 / Math.sqrt(x * x + y * y + z * z + w * w);
			
			x *= mag;
			y *= mag;
			z *= mag;
			w *= mag;
			
			var xy2 : Number = 2.0 * x * y, xz2 : Number = 2.0 * x * z, xw2 : Number = 2.0 * x * w;
			var yz2 : Number = 2.0 * y * z, yw2 : Number = 2.0 * y * w, zw2 : Number = 2.0 * z * w;
			var xx : Number = x * x, yy : Number = y * y, zz : Number = z * z, ww : Number = w * w;
			// a, b, c, d
			// e, f, g, h
			// i, j, k, l
			a = xx - yy - zz + ww;
			b = xy2 - zw2;
			c = xz2 + yw2;
			d = 0;
			
			e = xy2 + zw2;
			f = -xx + yy - zz + ww;
			g = yz2 - xw2;
			h = 0;
			
			i = xz2 - yw2;
			j = yz2 + xw2;
			k = -xx - yy + zz + ww;
			l = 0;		
		}
		
		public function makeRotation(axisX:Number, axisY:Number, axisZ:Number, angle:Number):void
		{
			var sin_val:Number, cos_val:Number, temp:Number;
			var cx:Number, cy:Number, cz:Number;
			var sx:Number, sy:Number, sz:Number;
			
			sin_val = Math.sin(angle);
			cos_val = Math.cos(angle);
			temp = 1 - cos_val;
			
			cx = temp * axisX, cy = temp * axisY, cz = temp * axisZ;
			sx = sin_val * axisX, sy = sin_val * axisY, sz = sin_val * axisZ;
			
			a = cx * axisX + cos_val;
			b = cx * axisY + sz;
			c = cx * axisZ - sy;
			
			e = cy * axisX - sz;
			f = cy * axisY + cos_val;
			g = cy * axisZ + sx;
			
			i = cz * axisX + sy;
			j = cz * axisY - sx;
			k = cz * axisZ + cos_val;
			
			d = h = l = 0;
		}
		
		public function composeQuat(x:Number, y:Number, z:Number, 
										  scalex:Number, scaley:Number, scalez:Number,
										  quatx:Number, quaty:Number, quatz:Number, quatw:Number):void
		{
			var qi2:Number = 2*quatx*quatx;
			var qj2:Number = 2*quaty*quaty;
			var qk2:Number = 2*quatz*quatz;
			var qij:Number = 2*quatx*quaty;
			var qjk:Number = 2*quaty*quatz;
			var qki:Number = 2*quatz*quatx;
			var qri:Number = 2*quatw*quatx;
			var qrj:Number = 2*quatw*quaty;
			var qrk:Number = 2*quatw*quatz;
			
			var aa:Number = 1 - qj2 - qk2;
			var bb:Number = qij - qrk;
			var ee:Number = qij + qrk;
			var ff:Number = 1 - qi2 - qk2;
			var ii:Number = qki - qrj;
			var jj:Number = qjk + qri;
			var kk:Number = 1 - qi2 - qj2;
			
			var rotatex:Number = 0;
			var rotatey:Number = 0;
			var rotatez:Number = 0;
			
			if (-1 < ii && ii < 1) {
				rotatex = Math.atan2(jj, kk);
				rotatey = -Math.asin(ii);
				rotatez = Math.atan2(ee, aa);
			} else {
				rotatex = 0;
				rotatey = ((ii <= -1) ? Math.PI : -Math.PI)*0.5;
				rotatez = Math.atan2(-bb, ff);
			}
			
			this.compose(x, y, z, rotatex, rotatey, rotatez, scalex, scaley, scalez);
		}
		
		public function fromEulerAngles(ax:Number, ay:Number, az:Number):void
		{
			var halfX:Number = ax*.5, halfY:Number = ay*.5, halfZ:Number = az*.5;
			var cosX:Number = Math.cos(halfX), sinX:Number = Math.sin(halfX);
			var cosY:Number = Math.cos(halfY), sinY:Number = Math.sin(halfY);
			var cosZ:Number = Math.cos(halfZ), sinZ:Number = Math.sin(halfZ);
			
			var w:Number = cosX*cosY*cosZ + sinX*sinY*sinZ;
			var x:Number = sinX*cosY*cosZ - cosX*sinY*sinZ;
			var y:Number = cosX*sinY*cosZ + sinX*cosY*sinZ;
			var z:Number = cosX*cosY*sinZ - sinX*sinY*cosZ;
			
			var rawData:Vector.<Number>;// = Matrix3DUtils.RAW_DATA_CONTAINER;
			var xy2:Number = 2.0*x*y, xz2:Number = 2.0*x*z, xw2:Number = 2.0*x*w;
			var yz2:Number = 2.0*y*z, yw2:Number = 2.0*y*w, zw2:Number = 2.0*z*w;
			var xx:Number = x*x, yy:Number = y*y, zz:Number = z*z, ww:Number = w*w;
			
			a = xx - yy - zz + ww;
			b = xy2 - zw2;
			c = xz2 + yw2;
			d = 0;
			
			e = xy2 + zw2;
			f = -xx + yy - zz + ww;
			g = yz2 - xw2;
			h = 0;
			
			i = xz2 - yw2;
			j = yz2 + xw2;
			k = -xx - yy + zz + ww;
			l = 0;
			/**
			rawData[3] = 0.0;
			rawData[7] = 0.0;
			rawData[11] = 0;
			rawData[15] = 1;
			 * */
		}
		
		/** 顶点变换 **/
		static private var tmp_x:Number;
		static private var tmp_y:Number;
		static private var tmp_z:Number;
		static public function transVector3D(t:Transform3D, v:Vector3D, out_v:Vector3D):void
		{
			tmp_x = t.a * v.x + t.b * v.y + t.c * v.z + t.d;
			tmp_y = t.e * v.x + t.f * v.y + t.g * v.z + t.h;
			tmp_z = t.i * v.x + t.j * v.y + t.k * v.z + t.l;
			out_v.x = tmp_x; out_v.y = tmp_y; out_v.z = tmp_z;
		}
		
		public function transPos3D( v:Pos3D, out_v:Pos3D):void
		{
			tmp_x = this.a * v.x + this.b * v.y + this.c * v.z + this.d;
			tmp_y = this.e * v.x + this.f * v.y + this.g * v.z + this.h;
			tmp_z = this.i * v.x + this.j * v.y + this.k * v.z + this.l;
			out_v.x = tmp_x; out_v.y = tmp_y; out_v.z = tmp_z;
		}
		
		/** 分解矩阵 **/
		static private var tmp_cols0:Pos3D = new Pos3D;
		static private var tmp_cols1:Pos3D = new Pos3D;
		static private var tmp_cols2:Pos3D = new Pos3D;
		public function decomposePos3D(out_pos:Pos3D, out_scale:Pos3D, out_rota:Transform3D):void
		{
			out_pos.x = this.d;
			out_pos.y = this.h;
			out_pos.z = this.l;
			
			tmp_cols0.setTo(this.a, this.b, this.c);
			tmp_cols1.setTo(this.e, this.f, this.g);
			tmp_cols2.setTo(this.i, this.j, this.k);
			
			var scaleX:Number = tmp_cols0.length();
			var scaleY:Number = tmp_cols1.length();
			var scaleZ:Number = tmp_cols2.length();
			
			if (out_scale != null) {
				out_scale.x = scaleX;
				out_scale.y = scaleY;
				out_scale.z = scaleZ;
			}
			
			if(scaleX != 0) {
				out_rota.a = this.a / scaleX;
				out_rota.b = this.b / scaleX;
				out_rota.c = this.c / scaleX;
				out_rota.d = 0;
			} else {
				out_rota.a = 0;
				out_rota.b = 0;
				out_rota.c = 0;
				out_rota.d = 0;
			}
			
			if(scaleY != 0) {
				out_rota.e = this.e / scaleY;
				out_rota.f = this.f / scaleY;
				out_rota.g = this.g / scaleY;
				out_rota.h = 0;
			} else {
				out_rota.e = 0;
				out_rota.f = 0;
				out_rota.g = 0;
				out_rota.h = 0;
			}
			
			if(scaleZ != 0) {
				out_rota.i = this.i / scaleZ;
				out_rota.j = this.j / scaleZ;
				out_rota.k = this.k / scaleZ;
				out_rota.l = 0;
			} else {
				out_rota.i = 0;
				out_rota.j = 0;
				out_rota.k = 0;
				out_rota.l = 0;
			}
		}
		
		static public function decompose(trans:Transform3D, out_pos:Vector3D, out_scale:Vector3D, out_rota:Transform3D):void
		{
			out_pos.x = trans.d;
			out_pos.y = trans.h;
			out_pos.z = trans.l;
			
			tmp_cols0.setTo(trans.a, trans.b, trans.c);
			tmp_cols1.setTo(trans.e, trans.f, trans.g);
			tmp_cols2.setTo(trans.i, trans.j, trans.k);
			
			var scaleX:Number = tmp_cols0.length();
			var scaleY:Number = tmp_cols1.length();
			var scaleZ:Number = tmp_cols2.length();
			
			if (out_scale != null) {
				out_scale.x = scaleX;
				out_scale.y = scaleY;
				out_scale.z = scaleZ;
			}
			
			if(scaleX != 0) {
				out_rota.a = trans.a / scaleX;
				out_rota.b = trans.b / scaleX;
				out_rota.c = trans.c / scaleX;
				out_rota.d = 0;
			} else {
				out_rota.a = 0;
				out_rota.b = 0;
				out_rota.c = 0;
				out_rota.d = 0;
			}
			
			if(scaleY != 0) {
				out_rota.e = trans.e / scaleY;
				out_rota.f = trans.f / scaleY;
				out_rota.g = trans.g / scaleY;
				out_rota.h = 0;
			} else {
				out_rota.e = 0;
				out_rota.f = 0;
				out_rota.g = 0;
				out_rota.h = 0;
			}
			
			if(scaleZ != 0) {
				out_rota.i = trans.i / scaleZ;
				out_rota.j = trans.j / scaleZ;
				out_rota.k = trans.k / scaleZ;
				out_rota.l = 0;
			} else {
				out_rota.i = 0;
				out_rota.j = 0;
				out_rota.k = 0;
				out_rota.l = 0;
			}
		}
		
	}
}
