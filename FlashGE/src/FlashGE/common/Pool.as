// 2014-3-10 lye
package FlashGE.common
{ 
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Matrix3D;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.geom.Vector3D;
	import flash.system.ApplicationDomain;
	
	import FlashGE.FlashGE;
	import FlashGE.effects.Effect;
	import FlashGE.effects.Particle;
	import FlashGE.materials.Material;
	import FlashGE.objects.Mesh;
	import FlashGE.objects.Skin;
	import FlashGE.objects.Surface;
	import FlashGE.render.RenderUnit;
	
	use namespace FlashGE;
	public final class Pool 
	{ 
		private const ClassShape:int = 0;
		private const ClassSprite:int = 1;
		private const ClassPoint:int = 2;
		private const ClassVector:int = 3;
		private const ClassTransform:int = 4;
		private const ClassVector3D:int = 5;
		private const ClassDrawUnit:int = 6;
		private const ClassMatrix3D:int = 7;
		private const ClassParticle:int = 8;
		private const ClassMaterial:int = 9;
		private const ClassEffect:int = 10;
		private const ClassMatrix:int = 11;
		private const ClassRectangle:int = 12;
		public const ClassSkin:int = 13;
		public const ClassMesh:int = 14;
		public const ClassSurface:int = 15;
		private const ClassNum:int = 16;
		
		private var Index2ClassName:Vector.<String>= new Vector.<String>(ClassNum);
		private var Index2Class:Vector.<Class> = new Vector.<Class>(ClassNum);
		private var Index2Objects:Vector.<Vector.<Object>> = new Vector.<Vector.<Object>>(ClassNum);
		private var Index2Counter:Vector.<int> = new Vector.<int>(ClassNum);		
		CONFIG::FPS {
			FlashGE var particlesCount:int = 0;
		}
			static private var inst:Pool;
			static public function getInstance():Pool
			{
				if (inst == null) {
					inst = new Pool;
					inst.initialize();
				}
				return inst;
			}
			
			public function initialize():void 
			{
				Index2Class[ClassShape] = Shape;
				Index2Class[ClassSprite] = Sprite;
				Index2Class[ClassPoint] = Point;
				Index2Class[ClassVector] = Vector;
				Index2Class[ClassTransform] = Transform3D;
				Index2Class[ClassVector3D] = Vector3D;
				Index2Class[ClassDrawUnit] = RenderUnit;
				Index2Class[ClassMatrix3D] = Matrix3D;
				Index2Class[ClassParticle] = Particle;
				Index2Class[ClassMaterial] = Material;
				Index2Class[ClassEffect] = Effect;
				Index2Class[ClassMatrix] = Matrix;
				Index2Class[ClassRectangle] = Rectangle;
				Index2Class[ClassMesh] = Mesh;
				Index2Class[ClassSkin] = Skin;
				Index2Class[ClassSurface] = Surface;
				Index2Class[ClassRectangle] = Rectangle;
			}
			
			public function popRectangle():Rectangle
			{
				return this.popObject(this.ClassRectangle);
			}
			
			public function pushRectangle(o:Rectangle):void
			{
				this.pushObject(this.ClassRectangle, o);	
			}
			
			public function popPoint():Point
			{
				return this.popObject(this.ClassPoint);
			}
			
			public function pushPoint(o:Point):void
			{
				this.pushObject(this.ClassPoint, o);	
			}
			
			public function popMatrix():Matrix
			{
				return this.popObject(this.ClassMatrix);	
			}
			
			public function pushMatrix(o:Matrix):void
			{
				this.pushObject(this.ClassMatrix, o);	
			}
			
			public function popEffect():Effect
			{
				return this.popObject(this.ClassEffect);	
			}
			
			public function pushEffect(o:Effect):void
			{
				this.pushObject(this.ClassEffect, o);
			}
			
			public function popMaterial(vv:String = ""):Material
			{
				var ret:Material = this.popObject(this.ClassMaterial);	
				return ret;
			}
			
			public function pushMaterial(o:Material):void
			{
				this.pushObject(this.ClassMaterial, o);
			}
			
			public function popParticle():Particle
			{
				CONFIG::FPS {
					this.particlesCount++;
				}
					return this.popObject(this.ClassParticle);
			}
			
			public function pushParticle(o:Particle):void
			{
				CONFIG::FPS {
					this.particlesCount--;
				}
					this.pushObject(this.ClassParticle, o);
			}
			
			public function printParticles():void
			{
				print(this.ClassParticle);
			}
			
			public function popMatrix3D():Matrix3D
			{
				return this.popObject(this.ClassMatrix3D);
			}
			
			public function pushMatrix3D(o:Matrix3D):void
			{
				this.pushObject(this.ClassMatrix3D, o);
			}
			
			public function popVector3D():Vector3D
			{
				return this.popObject(this.ClassVector3D);
			}
			
			public function pushVector3D(o:Vector3D):void
			{
				this.pushObject(this.ClassVector3D, o);
			}
			
			public function popTransform3D():Transform3D
			{
				var ret:Transform3D = this.popObject(this.ClassTransform);			
				return ret;
			}
			
			public function pushTransform3D(o:Transform3D):void
			{
				this.pushObject(this.ClassTransform, o);
			}
			
			public function popRenderUnit():RenderUnit
			{
				var o:RenderUnit = this.popObject(this.ClassDrawUnit);
				o.reuse();
				return o;
			}
			
			public function pushRenderUnit(o:RenderUnit):void
			{
				this.pushObject(this.ClassDrawUnit, o);
			}
			
			public function popObject(classIndex:int, applicationDomain:ApplicationDomain=null):* 
			{ 
				if (classIndex >= this.ClassNum) {
					return null;
				}
				
				var cls:Class = this.Index2Class[classIndex];
				if (cls==null) {
					if(applicationDomain == null) {
						applicationDomain = ApplicationDomain.currentDomain;
					}
					cls = applicationDomain.getDefinition(Index2ClassName[classIndex]) as Class;
					this.Index2Class[classIndex] = cls;
				}
				
				var counter:int =this.Index2Counter[classIndex];
				var objs:Vector.<Object> =this.Index2Objects[classIndex];
				if (counter > 0) {
					var index:int = this.Index2Counter[classIndex];
					index = index - 1;
					var out:Object = objs[index];
					this.Index2Counter[classIndex] = index;
					return out;
				}
				
				if (objs == null) {
					objs = new Vector.<Object>;
					this.Index2Objects[classIndex] = objs;
				}
				
				var i:int = 1;
				while (--i > -1) {
					objs.unshift( new cls() );
				}
				this.Index2Counter[classIndex] = 1;
				return popObject(classIndex, applicationDomain);
			}
			
			public function pushObject(classIndex:int, disposed:Object):void
			{ 
				this.Index2Objects[classIndex][this.Index2Counter[classIndex]++] = disposed; 
			} 
			
			public function print(classIndex:int):void
			{
				if (Index2Class[classIndex] == null) {
					return;
				}
				
				var objectsList:Vector.<Object> = this.Index2Objects[classIndex];
				if (objectsList != null && objectsList.length != 0) {
					trace(classIndex, "this.print", Index2Class[classIndex], objectsList.length);
				}
			}
			
			public function printAll():void
			{
				for (var i:int = 0; i < this.ClassNum; i++) {
					this.print(i);
				}
			}
			
			public function printMaterials():void
			{
				var classIndex:int = this.ClassMaterial;
				
				var objectsList:Vector.<Object> = this.Index2Objects[classIndex];
				if (objectsList == null) {
					trace(Global.frameCount, "this.print begin", classIndex, objectsList);
					trace(Global.frameCount, "this.print end", classIndex, 0);
				} else {
					trace(Global.frameCount, "this.print begin", classIndex, objectsList.length);
					for (var i:int = 0, n:int = objectsList.length; i < n; i++) {
						var obj:Material = objectsList[i] as Material;
						if (obj.refCount != 0) {
							trace("\t" + obj);
						}
					}
					trace(Global.frameCount, "this.print end", classIndex, objectsList.length);
				}
			}
	} 
}