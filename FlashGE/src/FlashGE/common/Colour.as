package FlashGE.common
{
	public class Colour
	{
		public var r:Number = 0;
		public var g:Number = 0;
		public var b:Number = 0;
		public var a:Number = 0;
		public function Colour(r:Number = 0, g:Number = 0, b:Number = 0, a:Number = 0)
		{
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = a;
		}
		
		public function copy(src:Colour):void
		{
			this.r = src.r;
			this.g = src.g;
			this.b = src.b;
			this.a = src.a;
		}
		
		public function clone():Colour
		{
			var ret:Colour = new Colour;
			ret.r = this.r;
			ret.g = this.g;
			ret.b = this.b;
			ret.a = this.a;
			return ret;
		}
		
		public function setRGB(r:Number, g:Number, b:Number):void
		{
			this.r = r;
			this.g = g;
			this.b = b;			
		}
		public function setRGBA(r:Number, g:Number, b:Number, a:Number):void
		{
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = a;
		}
	}
}