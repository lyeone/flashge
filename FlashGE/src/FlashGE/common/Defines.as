// 2014-3-10 lye
package FlashGE.common
{
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.Context3DCompareMode;
	import flash.display3D.Context3DTriangleFace;
	import flash.display3D.Context3DVertexBufferFormat;

	public class Defines
	{
		static public var baseResPath:String = "";
		static public var createRequest:Function;
		static public var playSound:Function;
		static public var stopSound:Function;
		
		//static public const AMBIENT_COLOR:Number = 0.688235319;
		static public const DIFFUSE_COLOR:Number = 0.508235319;
		static public const DIFFERENCE_MULTIPLIER:Number = 32768;
		// 0.899999976
		static public const BIASD_MULTIPLIER:Number = 0.98;
		static public const SHADER_OPAQUE:Number = 0.8;
		static public const MATERIAL_MODEL:int = 0;
		static public const MATERIAL_EFFECT:int = 1;
		static public const MATERIAL_HSL:int = 2;
		static public const MATERIAL_FOG:int = 4;
		static public const MATERIAL_BASIC:int = 5;
		static public const MATERIAL_WING:int = 6;
		static public const MATERIAL_COLOR:int = 7;
		static public const MATERIAL_WATER:int = 8;
		
		static public const ALIGN_NOOP:int = 0
		static public const ALIGN_LEFT:int = 1;
		static public const ALIGN_RIGHT:int = 2;
		static public const ALIGN_BOTTOM:int = 3;
		static public const ALIGN_TOP:int = 4;
		static public const ALIGN_CENTER:int = 5;
		
		static public const DISABLED:int = 0;
		static public const SIMPLE:int = 1;
		static public const ADVANCED:int = 2;
		
		static public const LOAD_NULL:int = 0;
		static public const LOAD_OK:int = 1;
		static public const LOAD_FAILD:int = 2;
		static public const LOAD_ING:int = 3;
		
		static public const PRIORITY_SKY:int = 10;
		static public const PRIORITY_BACKGROUD:int = 13;
		static public const PRIORITY_BEFORE_OPAQUE:int = 16;
		static public const PRIORITY_OPAQUE:int = 20;
		static public const PRIORITY_DECALS:int = 30;
		static public const PRIORITY_TRANSPARENT_SORT:int = 40;
		static public const PRIORITY_NEXT_LAYER:int = 50;
		static public const PRIORITY_TRANSPARENT_SORT_TRUE:int = 60;
		static public const PRIORITY_EFFECT:int = 70;
		static public const PRIORITY_FONT:int = 75;
		static public const PRIORITY_AFTER_EFFECT:int = 80;
		static public const PRIORITY_SCENE:int = 90;


		static public const LESS_EQUAL:int = 0;
		static public const LESS:int = 1;
		static public const GREATER_EQUAL:int = 2;
		static public const GREATER:int = 3;
		static public const EQUAL:int = 4;
		static public const NOT_EQUAL:int = 5;
		static public const ALWAYS:int = 6;
		static public const NEVER:int = 7;
		
		static public const TEX_MIX_TYPE_MUL:int = 0;
		static public const TEX_MIX_TYPE_ADD:int = 1;
		static public const TEX_MIX_TYPE_SUB:int = 2;		
		
		static public const RENDER_TYPE_NONE:int = 0;
		static public const RENDER_TYPE_ALPHA:int = 1;
		static public const RENDER_TYPE_ADD:int = 2;
		static public const RENDER_TYPE_MAX:int = 3;
		static public const RENDER_TYPE_FILTER:int = 4;
		
		static public const BLEND_DESTINATION_ALPHA:int = 0;
		static public const BLEND_DESTINATION_COLOR:int = 1;
		static public const BLEND_ONE:int = 2;
		static public const BLEND_ONE_MINUS_DESTINATION_ALPHA:int = 3;
		static public const BLEND_ONE_MINUS_SOURCE_ALPHA:int = 4;
		static public const BLEND_ONE_MINUS_SOURCE_COLOR:int = 5;
		static public const BLEND_SOURCE_ALPHA:int = 6;
		static public const BLEND_SOURCE_COLOR:int = 7;
		static public const BLEND_ZERO:int = 8;
		
		static public const TEX_OP_MUL:int = 0;
		static public const TEX_OP_ADD:int = 1;
		static public const TEX_OP_SUB:int = 2
		static public const TEX_OP_ALPHA:int = 3;
		
		static public const UV_TYPE_NONE:int = 0;
		static public const UV_TYPE_TRANS:int = 1;
		static public const UV_TYPE_FRAME:int = 2;
		
		static public const REPLACE_ALPHA_DEFAULT:int = 0;
		static public const REPLACE_ALPHA_R:int = 1;
		static public const REPLACE_ALPHA_G:int = 2;
		static public const REPLACE_ALPHA_B:int = 3;
		
		static public const FOLLOW_ALL:int = 0;
		static public const FOLLOW_POS:int = 1;
		
		static public const FILTERS:Vector.<String> = Vector.<String>(["linear", "nearest"]);
		static public const REPEATS:Vector.<String> = Vector.<String>(["repeat","clamp", "wrap"]);
		static public const TEX_FORMATS:Vector.<String> = Vector.<String>(["dxt1","dxt5", ""]);
		static public const MIPS:Vector.<String> = Vector.<String>(["miplinear","mipnearest", "mipnone"]);
		
		static public const POSITION:int = 1;
		static public const NORMAL:int = 2; 
		static public const JOINTS:Vector.<int> = Vector.<int>([3,4]);
		static public const TEXCOORDS:Vector.<int> = Vector.<int>([5,6,7,8,9,10,11,12]);		
		static public const DIFFUSE:int = 13;
		
		static public const FORMATS:Vector.<String> = Vector.<String>([
			Context3DVertexBufferFormat.FLOAT_1,	//NONE
			Context3DVertexBufferFormat.FLOAT_3,	//POSITION
			Context3DVertexBufferFormat.FLOAT_2,	//NORMAL
			Context3DVertexBufferFormat.FLOAT_4,	//JOINTS[0]
			Context3DVertexBufferFormat.FLOAT_2,	//JOINTS[1]
			Context3DVertexBufferFormat.FLOAT_2,	//TEXCOORDS[0]
			Context3DVertexBufferFormat.FLOAT_2,	//TEXCOORDS[1]
			Context3DVertexBufferFormat.FLOAT_2,	//TEXCOORDS[2]
			Context3DVertexBufferFormat.FLOAT_2,	//TEXCOORDS[3]
			Context3DVertexBufferFormat.FLOAT_2,	//TEXCOORDS[4]
			Context3DVertexBufferFormat.FLOAT_2,	//TEXCOORDS[5]
			Context3DVertexBufferFormat.FLOAT_2,	//TEXCOORDS[6]
			Context3DVertexBufferFormat.FLOAT_2, 	//TEXCOORDS[7]
			Context3DVertexBufferFormat.FLOAT_4, 	//DIFFUSE
		]);
		
		static public const BLEND_FACTORS:Vector.<String> = Vector.<String>([
			Context3DBlendFactor.DESTINATION_ALPHA,
			Context3DBlendFactor.DESTINATION_COLOR,
			Context3DBlendFactor.ONE,
			Context3DBlendFactor.ONE_MINUS_DESTINATION_ALPHA,
			Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA,
			Context3DBlendFactor.ONE_MINUS_SOURCE_COLOR,
			Context3DBlendFactor.SOURCE_ALPHA,
			Context3DBlendFactor.SOURCE_COLOR,
			Context3DBlendFactor.ZERO
		]);
		
		
		static public const ZTestFunctions:Vector.<String> = Vector.<String>([
			Context3DCompareMode.LESS_EQUAL,
			Context3DCompareMode.LESS,
			Context3DCompareMode.GREATER_EQUAL,
			Context3DCompareMode.GREATER,
			Context3DCompareMode.EQUAL,
			Context3DCompareMode.NOT_EQUAL,
			Context3DCompareMode.ALWAYS,
			Context3DCompareMode.NEVER
		]);
		
		static public var CULLING_TYPE:Array = [
			Context3DTriangleFace.FRONT,
			Context3DTriangleFace.BACK,
			Context3DTriangleFace.FRONT_AND_BACK,
			Context3DTriangleFace.NONE];
		
		static public function getAttributeStride(attribute:int):int
		{
			switch(FORMATS[attribute]) {
				case Context3DVertexBufferFormat.FLOAT_1:
					return 1;
				case Context3DVertexBufferFormat.FLOAT_2:
					return 2;
				case Context3DVertexBufferFormat.FLOAT_3:
					return 3;
				case Context3DVertexBufferFormat.FLOAT_4:
					return 4;
			}
			return 0;
		}
		
	}
}