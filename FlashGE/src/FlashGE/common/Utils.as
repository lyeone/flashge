// 2014-3-10 lye
package FlashGE.common
{
	import FlashGE.FlashGE;
	import FlashGE.effects.KeyPair;

	use namespace FlashGE;

	public class Utils 
	{

		private static const toRootTransform:Transform3D = new Transform3D();
		private static const fromRootTransform:Transform3D = new Transform3D();

		private static const RAD2DEG:Number = 180/Math.PI;
		private static const DEG2RAD:Number = Math.PI/180;

		public static function limit(n:Number,a:Number, b:Number):Number
		{
			if (n<a)
				return a;
			if (n>b)
				return b;
			return n;
		}
		
		public static function getInterpolationValue(percent:Number, keyPairs:Vector.<KeyPair>,
											  outValue:KeyPair):Boolean
		{
			if (outValue.items == null) {
				outValue.items = new Vector.<Number>;
			}
			var len:int = keyPairs.length;
			var r:Number, j:Number;
			var item:Vector.<Number>;
			var n:int;
			for (var i:int = 1; i < len; i++){
				if( percent >= (keyPairs[i-1].time / 100.0) &&
					percent <= (keyPairs[i].time / 100.0) ) {
					var curPercent:Number = (percent*100 - keyPairs[i-1].time) / 
						(keyPairs[i].time - keyPairs[i-1].time);
					n = keyPairs[i].items.length;
					item = keyPairs[i].items;
					outValue.items.length = n;
					for (j=0; j<n; j++){
						outValue.items[j] = ( keyPairs[i-1].items[j] + 
							(keyPairs[i].items[j] - keyPairs[i-1].items[j] ) * curPercent);
					}
					return false;
				}
			}
			if (len > 0) {
				var tmp:KeyPair = keyPairs[len-1];
				if ( percent*100 < tmp.time ) {
					tmp = keyPairs[0];
				}
				item = tmp.items;
				n = item.length;
				outValue.items.length = n;
				for (j=0; j<n; j++){
					outValue.items[j] = item[j];
				}
			}
			
			return true;
		}	
		
		public static function toRadians(degrees:Number):Number
		{
			return degrees * DEG2RAD;
		}

		public static function toDegrees(radians:Number):Number
		{
			return radians * RAD2DEG;
		}
	}
}
