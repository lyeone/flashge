package FlashGE.common
{
	import flash.utils.ByteArray;
	import flash.utils.Endian;

	public class BitmapByteArray
	{
		public var width:int;
		public var height:int;
		public var mipsList:Vector.<ByteArray> = new Vector.<ByteArray>;
		public function BitmapByteArray()
		{
		}
		
		
		public function init(bytes:ByteArray, width:int, height:int, closeMipMap:Boolean):void
		{
			bytes.endian = Endian.LITTLE_ENDIAN;
			bytes.position = 0;
			mipsList.length = 1;
			mipsList[0] = bytes;
			this.width = width;
			this.height = height;
			if (!closeMipMap) {
				createMipsImpl(bytes, width, height);
			}
		}
		
		public function dispose():void
		{
			if (mipsList != null) {
				for (var i:int = 0, n:int = this.mipsList.length; i < n; i++) {
					mipsList[i].length = 0;
				}
				mipsList.length = 0;
				mipsList = null;
			}
		}
		
		private function createMipsImpl(src:ByteArray, width:int, height:int):void
		{
			var cr0:int,cr1:int,cr2:int,cr3:int; 
			var r:int, g:int, b:int, a:int;
			var result:int;
			var dst:ByteArray = new ByteArray;
			dst.endian = Endian.LITTLE_ENDIAN;
			for (var i:int = 0; i < height; i+=2) {
				
				for (var j:int = 0; j < width; j+=2) {
					src.position = width * i * 4 + j * 4;
					
					if (src.position >= src.length) {
						cr0 = 0;
					} else {
						cr0 = src.readUnsignedInt();
					}
					
					if (src.position >= src.length) {
						cr1 = 0;
					} else {
						cr1 = src.readUnsignedInt();
					}
					
					src.position = width * 4 * (i + 1) + j * 4;
					
					if (src.position >= src.length) {
						cr2 = 0;
					} else {
						cr2 = src.readUnsignedInt();
					}
					
					if (src.position >= src.length) {
						cr3 = 0;
					} else {
						cr3 = src.readUnsignedInt();
					}
					
					r = (((cr0 >> 24) & 0xff) + ((cr1 >>24) & 0xff) + ((cr2 >> 24) & 0xff) + ((cr3 >>24) & 0xff)) >> 2;
					g = (((cr0 >> 16) & 0xff) + ((cr1 >>16) & 0xff) + ((cr2 >> 16) & 0xff) + ((cr3 >>16) & 0xff)) >> 2;
					b = (((cr0 >> 8) & 0xff)  + ((cr1 >>8) & 0xff)  + ((cr2 >> 8) & 0xff)  + ((cr3 >>8) & 0xff)) >> 2;
					a = (((cr0) & 0xff)  + ((cr1) & 0xff)  + ((cr2) & 0xff)  + ((cr3) & 0xff)) >> 2;
					result = (r << 24) | (g << 16) | (b << 8) | a;
					dst.writeUnsignedInt(result);
				}
			}
			dst.position = 0;
			src.position = 0;
			mipsList.push(dst);
			
			var mipWidth:int = width > 1 ? width / 2 : 1;
			var mipHeight:int = height > 1 ? height / 2 : 1;
			
			if (mipWidth == 1 && mipHeight == 1) {
				return;
			}
			
			this.createMipsImpl(dst, mipWidth, mipHeight);
		}
	}
}