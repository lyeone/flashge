package FlashGE.loaders
{
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.utils.ByteArray;
	import flash.utils.CompressionAlgorithm;
	import flash.utils.Dictionary;
	import flash.utils.Endian;
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Colour;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.objects.SkinData;
	import FlashGE.objects.Surface;
	import FlashGE.resources.Geometry;
	import FlashGE.resources.LoaderMgr3D;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;
	
	public class MeshLoader
	{
		static private var meshLoaderList:Dictionary = new Dictionary;
		static private var recycledFiles:Dictionary = new Dictionary;
		
		FlashGE var url:String;
		FlashGE var userData:int = 0;
		FlashGE var materialType:int = 0;
		
		FlashGE var skinDataList:Vector.<SkinData>;
		FlashGE var geometryList:Vector.<Geometry>;
		FlashGE var meshContextList:Vector.<MeshContext>;
		FlashGE var meshNameList:Vector.<String>;
		FlashGE var loadState:int = Defines.LOAD_NULL;
		FlashGE var listeners:Vector.<Function> = new Vector.<Function>;
		FlashGE var useLight:Boolean;
		FlashGE var recieveShadow:Boolean;
		FlashGE var dataContext:RawDataContext;
		FlashGE var getCurTimer:Function = getTimer;
		FlashGE var uvContext:Object;
		private var refCount:int = 0;
		private var usedFrame:int = 0;
		
		static public function loadMesh(fileName:String, userData:int, 
										useLight:Boolean, recieveShadow:Boolean, 
										materialType:int, loadListener:Function,
										uvContext:Object, 
										insertFirst:Boolean = false,
										idleLoader:Boolean = false,
										moveToHighPriority:Boolean = false, 
										ignoreSetParamsIfExist:Boolean = false):MeshLoader
		{
			var useCache:Boolean = Global.useCacheData;
			
			var loader:MeshLoader = MeshLoader.recycledFiles[fileName];
			if (loader != null) {
				delete MeshLoader.recycledFiles[fileName];
				MeshLoader.meshLoaderList[fileName] = loader;
			} else {
				loader = MeshLoader.meshLoaderList[fileName];
			}
			
			if (loader == null) {
				loader = new MeshLoader(userData, useLight, recieveShadow, materialType);
				if (useCache) {
					MeshLoader.meshLoaderList[fileName] = loader;
				}
				loader.addRef();
				loader.loadState = Defines.LOAD_ING;
				loader.uvContext = uvContext;
				loader.addListener(loadListener);
				loader.url = fileName;
				loader.load(fileName, insertFirst, idleLoader, moveToHighPriority);
			} else {
				
				loader.addRef();
				
				if (idleLoader == false) {
					LoaderMgr3D.getInstance().checkIdle(fileName, insertFirst, moveToHighPriority);
				}

				if (!ignoreSetParamsIfExist) {
					loader.userData = userData;
					loader.useLight = useLight;
					loader.recieveShadow = recieveShadow;
					loader.materialType = materialType;
					loader.uvContext = uvContext;
				}
				loader.addListener(loadListener);
			}
			return loader;
		}
		
		public function addRef():void
		{
			this.usedFrame = Global.frameCount;
			this.refCount++;
		}
		
		public function release():void
		{
			this.refCount--;
			if (this.refCount == 0) {
				if (Global.useCacheData && this.url != null && this.url.length > 0) {
					delete meshLoaderList[this.url];
					recycledFiles[this.url] = this;
				} else {
					this.dispose();
				}
			}
		}
		
		public function dispose():void
		{
			RawDataReader.getInstance().removeListener(parseRawData);
			
			var i:int = 0, n:int = 0;
			if (skinDataList != null) {
				for (i = 0, n = this.skinDataList.length; i < n; i++) {
					skinDataList[i].dispose();
				}
				skinDataList = null;
			}
			
			if (geometryList != null) {
				for (i = 0, n = this.geometryList.length; i < n; i++) {
					geometryList[i].dispose()
				}
				geometryList = null;
			}
			
			meshContextList = null;
			meshNameList = null;
			this.postListeners();
		}
		
		static public function checkMemory():void
		{
			var tmpMesh:MeshLoader = null;
			var hasDisposed:Boolean = false;
			for each(tmpMesh in MeshLoader.recycledFiles) {
				if (hasDisposed==false) {
					hasDisposed = true;
				}
				tmpMesh.dispose();
			}
			
			if (hasDisposed) {
				MeshLoader.recycledFiles = new Dictionary;
			}
		}
		
		static public function printAll():String
		{
			var ret:String;
			
			var tmpMesh:MeshLoader;
			ret += "MeshLoader::printAll cache:\n";
			for each(tmpMesh in MeshLoader.meshLoaderList) {
				ret += tmpMesh.toString() + "\n";
			}
			
			ret += "MeshLoader::printAll recycled:\n";
			for each(tmpMesh in MeshLoader.recycledFiles) {
				ret += tmpMesh.toString() + "\n";
			}
			
			ret += "MeshLoader::printAll end\n";
			return ret;
		}
		
		public function MeshLoader(userData:int, useLight:Boolean, recieveShadow:Boolean, materialType:int)
		{
			this.userData = userData;
			this.useLight = useLight;
			this.recieveShadow = recieveShadow;
			this.materialType = materialType;
		}
		
		public function toString():String
		{
			return "MeshLoader(refCount=" + this.refCount + ",url=" + this.url + ")";
		}
		
		public function enumAllTextures(outTextures:Dictionary):void
		{
			var i:int, n:int, meshContext:MeshContext, skinData:SkinData;
			if (this.meshContextList != null) {
				for (i = 0, n = this.meshContextList.length; i < n; i++) {
					meshContext = this.meshContextList[i];
					outTextures[meshContext.diffuseTexture] = true;
				}
			}
			if (this.skinDataList != null) {
				for (i = 0, n = this.skinDataList.length; i < n; i++) {
					skinData = this.skinDataList[i];
					outTextures[skinData.diffuseTexture] = true;
				}
			}
		}
		
		public function load(url:String, insertFirst:Boolean = false,
							 idleLoader:Boolean = false,
							 moveToHighPriority:Boolean = false):void
		{
			if (url.indexOf(".mes") >= 0) {
				if (moveToHighPriority) {
					LoaderMgr3D.getInstance().pushHighPriorityLoader(url, onLoadComplete, true, false, insertFirst);
				} else if (idleLoader) {
					LoaderMgr3D.getInstance().pushIdleLoader(url, onLoadComplete, true, false, insertFirst);
				} else {
					LoaderMgr3D.getInstance().pushLoader(url, onLoadComplete, true, false, insertFirst);
				}
			} else {
				onLoadComplete(null);
			}
		}	
		
		
		private function onLoadComplete(rawData:ByteArray, isHighPriority:Boolean = false):void
		{
			this.usedFrame = Global.frameCount;
			if (rawData == null) {
				this.loadState = Defines.LOAD_FAILD;
				postListeners();
				CONFIG::DEBUG {
					Global.log("Not exist file:"+url);
				}
			} else {
				dataContext = new RawDataContext;
				dataContext.rawData = rawData;
				dataContext.rawData.endian = Endian.LITTLE_ENDIAN;
				dataContext.rawData.position = 0;
				dataContext.step = -1;
				RawDataReader.getInstance().addListener(parseRawData);
			}
		}
		
		public function getLoadState():int
		{
			return this.loadState;
		}
		
		private function parseRawData(timer:int):int
		{
			var i:int;
			var j:int;
			var len:int;
			try {
				
				var curTimer:int = getCurTimer();
				if (dataContext.curResPath == null) {
					var tempUrl:String = this.url;
					dataContext.curResPath = tempUrl.slice(0, tempUrl.indexOf(tempUrl.split("/").pop()));
					var tag:String = dataContext.rawData.readUTFBytes(7);
					if (tag == "deflate") {
						var temp:ByteArray = new ByteArray;
						temp.endian = Endian.LITTLE_ENDIAN;
						temp.position = 0;
						dataContext.rawData.readBytes(temp, 0, dataContext.rawData.length - 7);
						temp.uncompress(CompressionAlgorithm.ZLIB);
						dataContext.rawData = temp;
					}
					dataContext.rawData.position = 0;
					if (getCurTimer() - curTimer >= timer) {
						return 0;
					}
				}
				
				if (dataContext.attributes == null) {
					dataContext.version = dataContext.rawData.readUnsignedShort();
					var exportType:int = dataContext.rawData.readUnsignedShort();
					/*
					ePosition = 0x1<<1,
					eNormal = 0x2 <<2,
					eTangent = 0x3<<3,
					eBone = 0x4<<4,
					eUV = 0x5<<5, 
					*/
					
					if (exportType&(1<<4)) {
						var bonesNum:int = dataContext.rawData.readUnsignedByte();
						dataContext.boneNameList = new Vector.<String>;
						dataContext.boneNameList.length = bonesNum;
						for (i =0; i < bonesNum; i++) {
							len = dataContext.rawData.readUnsignedByte();
							dataContext.boneNameList[i] = dataContext.rawData.readUTFBytes(len);
						}
					}
					
					dataContext.attributes = [];
					dataContext.attrIndex = 0;
					dataContext.vertPicth = 0;
					if (exportType&(1<<1)) {
						dataContext.attributes[dataContext.attrIndex++] = Defines.POSITION;
						dataContext.attributes[dataContext.attrIndex++] = Defines.POSITION;
						dataContext.attributes[dataContext.attrIndex++] = Defines.POSITION;
						dataContext.vertPicth+=3;
					}
					
					if (exportType&(1<<5)) {
						dataContext.attributes[dataContext.attrIndex++] = Defines.TEXCOORDS[0];
						dataContext.attributes[dataContext.attrIndex++] = Defines.TEXCOORDS[0];
						dataContext.vertPicth+=2;
					}
					
					if (exportType&(1<<2)) {
						dataContext.attributes[dataContext.attrIndex++] = Defines.NORMAL;
						dataContext.attributes[dataContext.attrIndex++] = Defines.NORMAL;
						//dataContext.attributes[dataContext.attrIndex++] = Defines.NORMAL;
						dataContext.vertPicth+=2;
					}
					
					if (exportType&(1<<3)) {
						//dataContext.attributes[dataContext.attrIndex++] = Defines.TANGENT4;
						//dataContext.attributes[dataContext.attrIndex++] = Defines.TANGENT4;
						//dataContext.attributes[dataContext.attrIndex++] = Defines.TANGENT4;
						//dataContext.attributes[dataContext.attrIndex++] = Defines.TANGENT4;
						//dataContext.vertPicth+=4;
					}
					
					if (getCurTimer() - curTimer >= timer) {
						return 0;
					}
				}
				
				if (dataContext.meshsCount == -1) {
					dataContext.meshsCount = dataContext.rawData.readUnsignedByte();
				}
				
				while (dataContext.perMeshIndex < dataContext.meshsCount) {
					if (dataContext.step == -1) {
						dataContext.step = 0;
						dataContext.stepIsSkin = dataContext.rawData.readUnsignedByte();
						dataContext.stepCurAttr = dataContext.attributes;
						dataContext.stepOpenAlphaTest = true;
						dataContext.stepMeshBoneNameList = null;
					
						if (dataContext.stepIsSkin == 1) {
							dataContext.stepCurAttr = dataContext.attributes.slice();
							var curi:int = dataContext.attrIndex;
							dataContext.stepCurAttr[curi++] = Defines.JOINTS[0];
							dataContext.stepCurAttr[curi++] = Defines.JOINTS[0];
							dataContext.stepCurAttr[curi++] = Defines.JOINTS[0];
							dataContext.stepCurAttr[curi++] = Defines.JOINTS[0];
							dataContext.stepCurAttr[curi++] = Defines.JOINTS[1];
							dataContext.stepCurAttr[curi++] = Defines.JOINTS[1];
						}
					
						len = dataContext.rawData.readUnsignedByte();
						dataContext.stepMeshName = dataContext.rawData.readUTFBytes(len);
						//Global.log(this.url, dataContext.perMeshIndex, meshName);
						len = dataContext.rawData.readUnsignedByte();
						dataContext.stepDiffuseTexture = dataContext.rawData.readUTFBytes(len);
						if (len > 0) {
							dataContext.stepDiffuseTexture = dataContext.curResPath + dataContext.stepDiffuseTexture.toLowerCase();
						}		
						
						dataContext.stepVertsNum = dataContext.rawData.readUnsignedInt();
						dataContext.stepVertBytes = dataContext.stepIsSkin==1 ? dataContext.stepVertsNum*(dataContext.vertPicth+6)*4 : dataContext.stepVertsNum*dataContext.vertPicth*4;
						
						dataContext.stepGeo = Geometry.create(dataContext.stepVertsNum);
						dataContext.stepGeo.addVertexStream(dataContext.stepCurAttr);
						
						if (getCurTimer() - curTimer >= timer) {
							return 0;
						}
					}
					
					if (dataContext.step == 0) {
						dataContext.step = 1;
						dataContext.rawData.readBytes(dataContext.stepGeo.vertexStreamsList[0].data, 0, dataContext.stepVertBytes);
						if (getCurTimer() - curTimer >= timer) {
							return 0;
						}
					}
					
					if (dataContext.step == 1) {
						if (dataContext.stepMeshBoneNameList == null) {
							len = dataContext.rawData.readByte();
							dataContext.stepMeshBoneNameList = new Vector.<String>(len);
						}
						
						for (dataContext.stepNameIndex = 0; dataContext.stepNameIndex < len; dataContext.stepNameIndex++) {
							var curIndex:int = dataContext.rawData.readByte();
							dataContext.stepMeshBoneNameList[dataContext.stepNameIndex] = dataContext.boneNameList[curIndex];
						}
						dataContext.step = 2;
						if (getCurTimer() - curTimer >= timer) {
							return 0;
						}
					}
					
					if (dataContext.step == 2) {
						if (dataContext.stepVertsNum > 65535) {
							dataContext.stepIndicesCount = dataContext.rawData.readUnsignedInt();
							dataContext.stepGeo.indicesBuffer.length = dataContext.stepIndicesCount * 3;
						} else {
							
							dataContext.stepIndicesCount = dataContext.rawData.readUnsignedShort();
							dataContext.stepGeo.indicesBuffer.length = dataContext.stepIndicesCount * 3;
						}
						dataContext.step = 3;
						dataContext.stepIndicesJ =0;
					}
					
					if (dataContext.step == 3) {
					
						if (dataContext.stepVertsNum > 65535) {
							while (dataContext.stepIndicesJ < dataContext.stepIndicesCount) {
								j = dataContext.stepIndicesJ;
								dataContext.stepGeo.indicesBuffer[j*3] = dataContext.rawData.readUnsignedInt();
								dataContext.stepGeo.indicesBuffer[j*3+1] = dataContext.rawData.readUnsignedInt();
								dataContext.stepGeo.indicesBuffer[j*3+2] = dataContext.rawData.readUnsignedInt();
								dataContext.stepIndicesJ++;
								if (getCurTimer() - curTimer >= timer) {
									return 0;
								}
							}
						} else {
							
							while (dataContext.stepIndicesJ < dataContext.stepIndicesCount) {
								j = dataContext.stepIndicesJ;
								dataContext.stepGeo.indicesBuffer[j*3] = dataContext.rawData.readUnsignedShort();
								dataContext.stepGeo.indicesBuffer[j*3+1] = dataContext.rawData.readUnsignedShort();
								dataContext.stepGeo.indicesBuffer[j*3+2] = dataContext.rawData.readUnsignedShort();
								dataContext.stepIndicesJ++;
								if (getCurTimer() - curTimer >= timer) {
									return 0;
								}
							}
						}
						
						if (getCurTimer() - curTimer >= timer) {
							return 0;
						}
						dataContext.step = 4;
					}
					
					if (dataContext.step == 4) {
						if (meshNameList == null) {
							meshNameList = new Vector.<String>;
						}

						meshNameList.push(dataContext.stepMeshName);
						if (dataContext.stepIsSkin==1) {
							skinDataList = skinDataList || new Vector.<SkinData>;
							var skinData:SkinData = new SkinData(3);
							skinData.geometry = dataContext.stepGeo;
							
							if (TextureFile.forceATF) {
								if (dataContext.stepDiffuseTexture != null) {
									skinData.diffuseTexture = dataContext.stepDiffuseTexture.replace(".tga", ".atf");
								}
							} else {
								skinData.diffuseTexture = dataContext.stepDiffuseTexture;
							}
							
							var sur:Surface = new Surface;
							sur.indexBegin = 0;
							sur.trianglesNum = dataContext.stepGeo.indicesBuffer.length/3;
							skinData.surfaces = new Vector.<Surface>;
							skinData.surfaces.push(sur);
							skinData.surfacesLength = 1;
							skinData.boneNameList = dataContext.stepMeshBoneNameList;
							skinData.renderedBonesCount = dataContext.stepMeshBoneNameList.length;
							skinData.surfaceBoneNamesList.length = 1;
							skinData.surfaceBoneNamesList[0] = dataContext.stepMeshBoneNameList.slice();
							skinData.meshName = dataContext.stepMeshName;
							skinData.openAlphaTest = dataContext.stepOpenAlphaTest;
			
							if(skinData.renderedBonesCount > 58) {
								skinData.divide(58, 1, recieveShadow);
							}
							
							skinData.calculateSurfacesProcedures(recieveShadow);
							skinDataList.push(skinData);
							
						} else {
							geometryList = geometryList || new Vector.<Geometry>;
							meshContextList = meshContextList || new Vector.<MeshContext>;
							
							var mat:MeshContext = new MeshContext;
							if (TextureFile.forceATF) {
								if (dataContext.stepDiffuseTexture != null) {
									mat.diffuseTexture = dataContext.stepDiffuseTexture.replace(".tga", ".atf");
								}
							} else {
								mat.diffuseTexture = dataContext.stepDiffuseTexture;
							}
							
							mat.openAlphaTest = dataContext.stepOpenAlphaTest;
							meshContextList.push(mat);
							
							geometryList.push(dataContext.stepGeo);
						}
						
						dataContext.step = 5;
					}
					dataContext.perMeshIndex++;
					dataContext.step = -1;
					if (getCurTimer() - curTimer >= timer) {
						return 0;
					}
				}
				
				postListeners();
				dataContext.dispose();
				dataContext = null;
				this.loadState = Defines.LOAD_OK;
			} catch (e:Event) {
				
				postListeners();
				dataContext.dispose();
				dataContext = null;
				this.loadState = Defines.LOAD_FAILD;
			}
			
			RawDataReader.getInstance().removeListener(parseRawData);
			return timer - (getCurTimer() - curTimer);
		}
		
		private function postListeners():void
		{
			if (this.listeners != null) {
				for (var i:int = 0, len:int = this.listeners.length; i < len; i++) {
					var callback:Function = this.listeners[i];
					if (callback != null) {
						callback(this);
					}
				}
				this.listeners.length = 0;
				this.listeners = null;
			}
		}
		
		public function addListener(callback:Function):void
		{
			if (callback == null) {
				return;
			}
			
			if (this.loadState == Defines.LOAD_OK || this.loadState == Defines.LOAD_FAILD) {
				callback(this);
			} else {
				this.listeners.push(callback);
			}
		}
		
		public function delListener(callback:Function):void
		{
			if (this.listeners != null) {
				for (var i:int = 0, len:int = this.listeners.length; i < len; i++) {
					if (this.listeners[i] == callback) {
						this.listeners[i] = null;
						return;
					}
				}
			}
		}
	}
}

import flash.utils.ByteArray;

import FlashGE.resources.Geometry;

internal class RawDataContext
{
	public var rawData:ByteArray;
	public var curResPath:String;
	public var perMeshIndex:int = 0;
	public var meshsCount:int = -1;
	public var attributes:Array; 
	public var boneNameList:Vector.<String>;
	public var attrIndex:int = 0;
	public var vertPicth:int = 0
	public var version:int = 0;
	public var vertsNum:int = 0;
	
	public var step:int = -1;
	public var stepIsSkin:int;
	public var stepCurAttr:Array;
	public var stepOpenAlphaTest:Boolean;
	public var stepMeshName:String;
	public var stepVertsNum:int;
	public var stepVertBytes:int;
	public var stepGeo:Geometry;
	
	public var stepMeshBoneNameList:Vector.<String>;
	public var stepNameIndex:int;
	public var stepIndicesJ:int;
	public var stepIndicesCount:int;
	public var stepDiffuseTexture:String;
	
	public function dispose():void
	{
		rawData = null;
		attributes = null; 
		boneNameList = null;
		stepCurAttr = null
		stepGeo = null;	
		stepMeshBoneNameList = null;
	}
}
