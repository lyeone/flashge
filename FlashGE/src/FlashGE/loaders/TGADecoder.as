package FlashGE.loaders
{
	import flash.display.BitmapData;
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	
	import FlashGE.FlashGE;
	import FlashGE.common.BitmapByteArray;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	
	use namespace FlashGE;
	
	public class TGADecoder {
		//___________________________________________________________ const
		
		// constant value for _imageType
		private const TYPE_NONE:uint = 0x00;
		private const TYPE_INDEX_COLOR:uint = 0x01;
		private const TYPE_FULL_COLOR:uint = 0x02;
		private const TYPE_RLE_BIT:uint = 0x08;
		
		private const DIR_RIGHT_UP:int = 0;
		private const DIR_LEFT_UP:int = 1;
		private const DIR_RIGHT_DOWN:int = 2;
		private const DIR_LEFT_DOWN:int = 3;
		
		//___________________________________________________________ vars
		
		
		private var _idLength:int; // byte
		private var _colorMapType:int; // byte
		private var _imageType:int; // byte
		private var _colorMapIndex:int; // short
		private var _colorMapLength:int; // short
		private var _colorMapSize:int; // byte
		private var _originX:int; // short
		private var _originY:int; // short
		FlashGE var _width:int; // short
		FlashGE var _height:int; // short
		private var _bitDepth:int; // byte
		private var _descriptor:int; // byte
		public function get pixelDirection():int {
			// descriptor:
			//   4th bit: 0 = left to right, 1 = right to left
			//   5th bit: 0 = bottom up, 1 = top down
			return (_descriptor >> 4) & 3;
		}
		
		
		public function TGADecoder()
		{}
		
		public function decode(srcBytes:ByteArray):BitmapByteArray
		{
			srcBytes.position = 0;
			srcBytes.endian = Endian.LITTLE_ENDIAN;
			
			_idLength = srcBytes.readByte();
			_colorMapType = srcBytes.readByte();
			_imageType = srcBytes.readByte();
			_colorMapIndex = srcBytes.readShort();
			_colorMapLength = srcBytes.readShort();
			_colorMapSize = srcBytes.readByte()
			_originX = srcBytes.readShort();
			_originY = srcBytes.readShort();
			_width = srcBytes.readShort();
			_height = srcBytes.readShort();
			_bitDepth = srcBytes.readByte();
			_descriptor = srcBytes.readByte();
			
			//_bitmap = new BitmapData(_width, _height);
			
			// ignore unsupported formats.
			if ((_imageType & TYPE_FULL_COLOR) == 0
				|| (_imageType & TYPE_RLE_BIT) != 0) {
				Global.throwError("Unsupported tga format.");
			}
			var dstWidth:int;
			var dstHeight:int;
			{
				var wLog2Num:Number = 0;
				var hLog2Num:Number = 0;
				var wLog2:int = 0;
				var hLog2:int = 0;
				wLog2Num = Math.log(_width)/Math.LN2;
				hLog2Num = Math.log(_height)/Math.LN2;
				wLog2 = Math.ceil(wLog2Num);
				hLog2 = Math.ceil(hLog2Num);
				
				if (wLog2 != wLog2Num || hLog2 != hLog2Num || wLog2 > 11 || hLog2 > 11) {
					wLog2 = (wLog2 > 11) ? 11 : wLog2;
					hLog2 = (hLog2 > 11) ? 11 : hLog2;
					dstWidth = 1 << wLog2;
					dstHeight = 1 << hLog2;
				} else {
					dstWidth = _width;
					dstHeight = _height;
				}
			}
			
			var dstBytes:ByteArray = new ByteArray;
			dstBytes.endian = Endian.LITTLE_ENDIAN;
			dstBytes.position = 0;
			if (_bitDepth == 32) {
				loadBitmap32(srcBytes, dstBytes, dstWidth, dstHeight);
			} else if (_bitDepth == 24) {
				loadBitmap24(srcBytes, dstBytes, dstWidth, dstHeight);
			}
			//_bitmap.unlock();
			if (dstWidth != _width || dstHeight != _height) {
				dstBytes.length = dstWidth * dstHeight * 4;
			}
			
			var out:BitmapByteArray = new BitmapByteArray;
			out.init(dstBytes, dstWidth, dstHeight, false);
			return out;
		}
		
		private function loadBitmap32(srcBytes:ByteArray, dstBytes:ByteArray, dstWidth:int, dstHeight:int):void {
			var x:int, y:int;
			switch (pixelDirection) {
				case DIR_RIGHT_UP:
					for (y = _height - 1; y >= 0; --y) {
						dstBytes.position = dstWidth * 4 * y;
						for (x = 0; x < _width; ++x) {
							dstBytes.writeInt(srcBytes.readUnsignedInt());
						}
					}
					break;
				case DIR_LEFT_UP:
					for (y = _height - 1; y >= 0; --y) {
						dstBytes.position = dstWidth * 4 * y;
						for (x = _width - 1; x >= 0; --x) {
							dstBytes.writeInt(srcBytes.readUnsignedInt());
						}
					}
					break;
				case DIR_RIGHT_DOWN:
					for (y = 0; y < _height; ++y) {
						dstBytes.position = dstWidth * 4 * y;
						for (x = 0; x < _width; ++x) {
							dstBytes.writeInt(srcBytes.readUnsignedInt());
						}
					}
					break;
				case DIR_LEFT_DOWN:
					for (y = 0; y < _height; ++y) {
						dstBytes.position = dstWidth * 4 * y;
						for (x = _width - 1; x >= 0; --x) {
							dstBytes.writeInt(srcBytes.readUnsignedInt());
						}
					}
					break;
			}
		}
		
		
		private function loadBitmap24(srcBytes:ByteArray, dstBytes:ByteArray, dstWidth:int, dstHeight:int):void {
			var x:int, y:int;
			var r:uint, g:uint, b:uint;
			var cr:uint;
			
			switch (pixelDirection) {
				case DIR_RIGHT_UP:
					for (y = _height - 1; y >= 0; --y) {
						dstBytes.position = dstWidth * 4 * y;
						for (x = 0; x < _width; ++x) {
							b = srcBytes.readUnsignedByte();
							g = srcBytes.readUnsignedByte();
							r = srcBytes.readUnsignedByte();
							cr = 0xFF000000 | (r << 16) | (g << 8) | b;
							dstBytes.writeInt(cr);
						}
					}
					break;
				case DIR_LEFT_UP:
					for (y = _height - 1; y >= 0; --y) {
						dstBytes.position = dstWidth * 4 * y;
						for (x = _width - 1; x >= 0; --x) {
							b = srcBytes.readUnsignedByte();
							g = srcBytes.readUnsignedByte();
							r = srcBytes.readUnsignedByte();
							cr = 0xFF000000 | (r << 16) | (g << 8) | b;
							dstBytes.writeInt(cr);
						}
					}
					break;
				case DIR_RIGHT_DOWN:
					for (y = 0; y < _height; ++y) {
						dstBytes.position = dstWidth * 4 * y;
						for (x = 0; x < _width; ++x) {
							b = srcBytes.readUnsignedByte();
							g = srcBytes.readUnsignedByte();
							r = srcBytes.readUnsignedByte();
							cr = 0xFF000000 | (r << 16) | (g << 8) | b;
							dstBytes.writeInt(cr);
						}
					}
					break;
				case DIR_LEFT_DOWN:
					for (y = 0; y < _height; ++y) {
						dstBytes.position = dstWidth * 4 * y;
						for (x = _width - 1; x >= 0; --x) {
							b = srcBytes.readUnsignedByte();
							g = srcBytes.readUnsignedByte();
							r = srcBytes.readUnsignedByte();
							cr = 0xFF000000 | (r << 16) | (g << 8) | b;
							dstBytes.writeInt(cr);
						}
					}
					break;
			}
		}
	}
}