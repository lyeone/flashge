package FlashGE.loaders
{
	import FlashGE.FlashGE;
	
	use namespace FlashGE;
	public class MeshContext
	{
		public var openAlphaTest:Boolean;
		public var diffuseTexture:String;
		public var materialKey:String;
		public var programCachedID:String;
	}
}