package FlashGE.loaders
{
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.animation.SkeletonAni;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;
	public class RawDataReader
	{
		static private var inst:RawDataReader;
		FlashGE var listenersList:Vector.<Function> = new Vector.<Function>;
		FlashGE var listenersListHigh:Vector.<Function> = new Vector.<Function>;
		
		public var forbidCheckMemory:Boolean = false;
		private var TextureFileCheckMemory:Function;
		private var SkeletonAniCheckMemory:Function;
		private var MeshLoaderCheckMemory:Function;
		private var memCheckTick:int = 0;
		
		public function RawDataReader()
		{
		}
		
		static public function getInstance():RawDataReader
		{
			if (inst == null) {
				inst = new RawDataReader;
			}
			return inst;
		}
		
		
		public function addListener(listener:Function, highPriority:Boolean = false):void
		{
			var list:Vector.<Function> = highPriority ? this.listenersListHigh : this.listenersList;

			for (var i:int = 0, n:int = list.length; i < n; /*i++*/) {
				if (list[i] == null) {
					list.splice(i, 1);
					n--;
				} else {
					i++;
				}
			}
			
			list.push(listener);
		}
		
		public function check(deltaTimer:int):void
		{
			var rawListener:Function = null;
			var i:int, n:int;
			for (i= 0, n= this.listenersListHigh.length; i < n; i++) {
				if ((rawListener = this.listenersListHigh[i]) != null) {
					if ((deltaTimer = rawListener(deltaTimer)) <= 0 ) {
						return;
					}
				}
			}
			
			for (i = 0, n = this.listenersList.length; i < n; i++) {
				if ((rawListener = this.listenersList[i]) != null) {
					if ((deltaTimer = rawListener(deltaTimer)) <= 0 ) {
						return;
					}
				}
			}
			
			for (i = 0, n = listenersListHigh.length; i < n; /*i++*/) {
				if (listenersListHigh[i] == null) {
					listenersListHigh.splice(i, 1);
					n--;
				} else {
					i++;
				}
			}
			
			for (i = 0, n = listenersList.length; i < n; /*i++*/) {
				if (listenersList[i] == null) {
					listenersList.splice(i, 1);
					n--;
				} else {
					i++;
				}
			}
			
			if (!forbidCheckMemory) {
				
				var curTimer:int = getTimer();
				if (memCheckTick == 0) {
					memCheckTick = curTimer;
				}
				if (curTimer - memCheckTick > 60000) {
					memCheckTick = curTimer;
					if (TextureFileCheckMemory == null) {
						TextureFileCheckMemory = TextureFile.checkMemory;
					}
					this.TextureFileCheckMemory();
					
					if (SkeletonAniCheckMemory == null) {
						SkeletonAniCheckMemory = SkeletonAni.checkMemory;
					}
					this.SkeletonAniCheckMemory();
					
					if (MeshLoaderCheckMemory == null) {
						MeshLoaderCheckMemory = MeshLoader.checkMemory;
					}
					this.MeshLoaderCheckMemory();
				}
			}
		}
		
		public static function disposeAllRes():void
		{
			TextureFile.checkMemory();
			SkeletonAni.checkMemory();
			MeshLoader.checkMemory();
		}
		
		public function removeListener(rawListener:Function):void
		{
			var i:int, n:int;
			for (i = 0, n = this.listenersList.length; i < n; i++) {
				if (rawListener == this.listenersList[i]) {
					this.listenersList[i] = null;
					return;
				}
			}
			
			for (i = 0, n = this.listenersListHigh.length; i < n; i++) {
				if (rawListener == this.listenersListHigh[i]) {
					this.listenersListHigh[i] = null;
					return;
				}
			}
		}
	}
}