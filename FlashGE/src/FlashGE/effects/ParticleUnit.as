/*@author lye 123*/

package FlashGE.effects
{
	import flash.display.Loader;
	import flash.display3D.Context3D;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Context3DTriangleFace;
	import flash.display3D.Context3DVertexBufferFormat;
	import flash.geom.Matrix3D;
	import flash.geom.Vector3D;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.common.AxisAlignedBox;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.ParticleUnitProp;
	import FlashGE.effects.unitProp.SpriteUnitProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.materials.ShaderProgram;
	import FlashGE.materials.compiler.Procedure;
	import FlashGE.objects.Entity;
	import FlashGE.render.Camera3D;
	import FlashGE.render.RenderUnit;
	import FlashGE.resources.LoaderMgr3D;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;
	
	public class ParticleUnit extends EffectUnitBase 
	{
		static public var buffer3DList:Dictionary;
		static public var limitParticlesCount:Boolean = false;
		
		static FlashGE var color:KeyPair;
		static FlashGE var scale:KeyPair;
		static FlashGE var offset:KeyPair;
		static FlashGE var rotate:KeyPair;
			
		FlashGE var nextInSystem:ParticleUnit;
		FlashGE var particleList:Particle;
		FlashGE var aabb:AxisAlignedBox;
		FlashGE var particlesCounter:int;
		FlashGE var preEmitFrame:int;
		FlashGE var curParticlesCount:int;
		FlashGE var buffers3d:ParticleContext3DBuffer;
		FlashGE var getCurTimer:Function;
		
		FlashGE const MAX_RATIO:Number = 1 / uint.MAX_VALUE;
		FlashGE var randomSeed:uint = 0;
		private var pool:Pool;
		public function getRandom():Number
		{
			if (this.unitProp == null) {
				return Math.random();
			}
			
			var randomSeedFixed:uint = (this.unitProp as ParticleUnitProp).randomSeed
			if (randomSeedFixed == 0) {
				return Math.random();
			}
			
			if (randomSeed == 0) {
				randomSeed = randomSeedFixed;
			}
			
			randomSeed ^= (randomSeed << 21);
			randomSeed ^= (randomSeed >>> 35);
			randomSeed ^= (randomSeed << 4);
			return (randomSeed * MAX_RATIO);
		}
		
		public static function initBuffer3DList(key:String, context:Context3D):void
		{
			if (buffer3DList == null) {
				buffer3DList = new Dictionary;
			}
			var loaderMgr:LoaderMgr3D = LoaderMgr3D.getInstance()
			var curAllFxList:Dictionary = loaderMgr.getAllFxList();
			if (curAllFxList == null) {
				return;
			}
			var item:Object = curAllFxList[key];
			if (item != null) {
				var realKey:String = key.replace("particle_", "");
				var shader:ParticleContext3DBuffer = buffer3DList[realKey];
				if (shader == null) {
					var allFxRawData:ByteArray = loaderMgr.getAllFxRawData();
					allFxRawData.position = item.offset;
					shader = new ParticleContext3DBuffer;
					shader.read(allFxRawData);
					
					var vertices:Vector.<Number> = new Vector.<Number>();
					var indices:Vector.<uint> = new Vector.<uint>();
					var curLimit:int = shader.curLimit;
					var curStepIndex:int = shader.curStepIndex;
					var i:int;
					for (i = 0; i < curLimit; i++) {
						vertices.push(
							-0.5,-0.5, 0, 0,0,i*curStepIndex,
							-0.5, 0.5, 0, 0,1,i*curStepIndex,
							0.5, 0.5, 0, 1,1,i*curStepIndex,
							0.5,-0.5, 0, 1,0,i*curStepIndex);
						indices.push(i*4, i*4 + 1, i*4 + 3, i*4 + 2, i*4 + 3, i*4 + 1);
					}
					
					shader.indices = indices;
					shader.vertices = vertices;
					
					if (context != null) {
						if (shader.curIndexBuffer != null) {
							shader.curIndexBuffer.dispose();
							shader.curIndexBuffer = null;
						}
						
						if (shader.curVertexBuffer != null) {
							shader.curVertexBuffer.dispose();
							shader.curVertexBuffer = null;
						}
						
						if (shader.curProgram3D != null) {
							shader.curProgram3D.dispose();
							shader.curProgram3D = null;
						}
						
						shader.curIndexBuffer = context.createIndexBuffer(curLimit*6);
						shader.curIndexBuffer.uploadFromVector(indices, 0, curLimit*6);
						shader.curVertexBuffer = context.createVertexBuffer(curLimit * 4, 6);
						shader.curVertexBuffer.uploadFromVector(vertices, 0, curLimit*4);
						shader.curProgram3D = context.createProgram();
						shader.curProgram3D.upload(shader.vertexBytes, shader.fragmentBytes);
						shader.curContext3D = context;
					}
					buffer3DList[realKey] = shader;
				}
			}
		}
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_PARTICLE;
		}
		
		static public function initStaticContext():void
		{
			ParticleUnit.buffer3DList = new Dictionary;
			
			ParticleUnit.color = new KeyPair;
			ParticleUnit.color.init(0);
			
			ParticleUnit.scale = new KeyPair;
			ParticleUnit.scale.init(0);
			
			ParticleUnit.offset = new KeyPair;
			ParticleUnit.offset.init(0);
			
			ParticleUnit.rotate = new KeyPair;
			ParticleUnit.rotate.init(0);
		}
		
		override public function contructor(unitProp:UnitBaseProp = null):void
		{
			super.contructor(unitProp);
			
			this.aabb = new AxisAlignedBox();
			this.preEmitFrame = -1;
			this.curParticlesCount = 0;
			this.getCurTimer = flash.utils.getTimer;
			
			if (this.unitProp == null) {
				this.unitProp = new ParticleUnitProp;
				this.unitProp.contructor();
			}
			
			this.unitProp.uvUOffset = 0;
			this.unitProp.uvVOffset = 0;
			
			boundBox = new AxisAlignedBox();
			boundBox.minX = -100;
			boundBox.minY = -100;
			boundBox.minZ = -20;
			boundBox.maxX = 100;
			boundBox.maxY = 100;
			boundBox.maxZ = 200;
		}
		
		override public function rotateFaceNormal(dx:Number, dy:Number, dz:Number):void
		{
			var prop:ParticleUnitProp = this.getUnitProp() as ParticleUnitProp;
			prop.useNewFaceNormal = 1;
			prop.faceNormal.x += dx;
			prop.faceNormal.y += dy;
			prop.faceNormal.z += dz;
			prop.calcNormal();
		}
		
		override public function resetFaceNormal():void
		{
			var prop:ParticleUnitProp = this.getUnitProp() as ParticleUnitProp;
			prop.useNewFaceNormal = 1;
			prop.faceNormal.x  = 0;
			prop.faceNormal.y  = 0;
			prop.faceNormal.z  = 0;
			prop.calcNormal();
		}
		
		
		public function getParticlesCount():uint
		{
			var i:uint = 0;
			var p:Particle = this.particleList;
			while(p != null) {
				i++;
				//trace("particle curTime, Life:\t" + (p.preTime-p.startTime) + "\t" + p.lifeTime);
				p = p.next;
			}
			return i;
		}
		
		public function calculate(camera:Camera3D, curUnitFrame:int):Boolean 
		{
			if (curUnitFrame > unitProp.lifeFramesNum && particleList==null) {
				return false;
			}
			
			var prop:ParticleUnitProp = this.unitProp as ParticleUnitProp;
			var curFrame:int = Global.frameCount;
			var n:int = preEmitFrame < 0 ? 1 : (curFrame - preEmitFrame ) / prop.emitFrequency;
			
			if (curUnitFrame <= system.effectProp.lifeFramesNum && 
				curUnitFrame <= unitProp.lifeFramesNum && ( n >= 1 )) {
				if (curParticlesCount < 60) {
					fireParticles(camera);
				}
				preEmitFrame = curFrame;
			}
			
			if (particleList != null) {
				updateParticles(camera);
			}
			
			if (curUnitFrame >= unitProp.lifeFramesNum) {
				randomSeed = 0;
			}
			
			return true;
		}
		
		public function updateParticles(camera:Camera3D):void
		{
			var b:int = 0, a:int;
			var percent:Number, deltaMilliseconds:Number;
			var pre:Particle, particle:Particle, next:Particle;
			var emitDir:Vector3D;
			
			particle = particleList;
			pre = null;
			
			var renderUnit:RenderUnit;
			var beginRegister:int = 0;
			
			var prop:ParticleUnitProp = this.unitProp as ParticleUnitProp;
			var limit:int = buffers3d.curLimit;
			particlesCounter = 0;
			this.curParticlesCount = 0;
			while (particle!=null ) {
				var curMilliseconds:int = getCurTimer();
				deltaMilliseconds = system.isPaused() ? particle.preUpdateMilliseconds - particle.startMilliseconds : curMilliseconds - particle.startMilliseconds;
				if (deltaMilliseconds > particle.lifeMilliseconds) {
					if (pre == null) {
						//head
						//pre = particle.next;
						particleList = particle.next;
					} else {
						//mid
						pre.next = particle.next;
					}
					
					next = particle.next;
					particle.rotateS = 0;
					particle.next = null;
					pool.pushParticle(particle);
					particle = next;
					continue;
				}
				
				if (!system.isPaused()) {
					particle.preUpdateMilliseconds = curMilliseconds;
				}
				
				if (!camera.closeCollectRenderUnits) {
					particle.beforeCalcTrans(this, camera);
					
					percent = deltaMilliseconds / particle.lifeMilliseconds;
					var deltaSeconds:Number = deltaMilliseconds / 1000.0;
					
					this.getInterpolationValue(percent, prop.scaleKeys, scale);
					this.getInterpolationValue(percent, prop.colorKeys, color);
					this.getInterpolationValue(percent, prop.rotateKeys, rotate);
					var curTime2:Number = deltaSeconds * deltaSeconds;
					
					var sx:Number, sy:Number, sz:Number;
					var localx:Number, localy:Number, localz:Number;
					
					emitDir = particle.curEmitDir;
					//TODO: acceleration havn't conside X-space
					sx = emitDir.x * deltaSeconds + 0.5*particle.accelerationDir.x * particle.acceleration * curTime2;					
					sy = emitDir.y * deltaSeconds + 0.5*particle.accelerationDir.y * particle.acceleration * curTime2;					
					sz = emitDir.z * deltaSeconds + 0.5*particle.accelerationDir.z * particle.acceleration * curTime2;
					
					localx = particle.localX + sx;
					localy = particle.localY + sy;
					localz = particle.localZ + sz;	
					particle.x = localx;
					particle.y = localy;
					particle.z = localz;
					
					var factor:Number = rotate.items[0];
					
					particle.rotateS = (particle.rotateV*factor) * deltaSeconds;
					particle.width = particle.orgWidth * scale.items[0];
					particle.height = particle.orgHeight * scale.items[1];
					
					particle.red = color.items[0];
					particle.green = color.items[1];
					particle.blue = color.items[2];
					particle.alpha = color.items[3];
					
					if (prop.uvType == Defines.UV_TYPE_FRAME) {
						var pos:int = deltaSeconds*prop.uvFPS;
						if (prop.uvLoop) {
							pos = pos%prop.uvRangeLength;
							if (pos < 0) 
								pos += prop.uvRangeLength;
						} else {
							if (pos < 0) 
								pos = 0;
							if (pos >= prop.uvRangeLength) 
								pos = prop.uvRangeLength - 1;
						}
						pos += prop.uvRangeBegin;
						var col:int = pos%prop.uvColumnsCount;
						var row:int = pos/prop.uvColumnsCount;
						particle.uvOffsetX = col/prop.uvColumnsCount;
						particle.uvOffsetY = row/prop.uvRowsCount;
					}			
					
					if (renderUnit == null) {
						renderUnit = pool.popRenderUnit();
					}
					
					if (particlesCounter >= limit) {
						flush(renderUnit, camera);
						renderUnit.release();
						renderUnit = pool.popRenderUnit();
						particlesCounter = 0;
						beginRegister = 0;
					}
					
					beginRegister = particle.calcTrans(this, camera, renderUnit, beginRegister);
					
					renderUnit.setVertexConstantsFromNumbers(beginRegister++, particle.uvScaleX, particle.uvScaleY, particle.uvOffsetX, particle.uvOffsetY);
					renderUnit.setVertexConstantsFromNumbers(beginRegister++, particle.red, particle.green, particle.blue, particle.alpha);
					particlesCounter++;
					curParticlesCount++;
					
					pre = particle;
					CONFIG::DEBUG {
						Global.particlesCount++;
					}
				}
				particle = particle.next;				
			}
			
			if (particlesCounter > 0) {
				if (!camera.closeCollectRenderUnits) {
					flush(renderUnit, camera);
				}
			}
			if (renderUnit != null) {
				renderUnit.release();
			}
		}
		public function fireParticles(camera:Camera3D):void
		{
			if (system.isPaused()) {
				return;
			}
			
			var prop:ParticleUnitProp = this.unitProp as ParticleUnitProp;
			var localx:Number;
			var localy:Number;
			var localz:Number;
			var temp:Transform3D;
			var emiiterIntensity:uint = prop.emitMinIntensity + getRandom()*(prop.emitMaxIntensity-prop.emitMinIntensity);	
			var frameCount:uint = Global.frameCount;
			for (var i:uint = 0; i < emiiterIntensity; i++) {
				
				var particle:Particle = pool.popParticle();
				
				initParticle(particle);
				particle.initTrans(this, camera);
				particle.preUpdateMilliseconds = particle.startMilliseconds = getCurTimer();
				particle.orgWidth = prop.minWidth + getRandom()*(prop.maxWidth-prop.minWidth);
				particle.orgHeight = prop.sizeFactor * particle.orgWidth;// + getRandom()*(maxHeight-sizeFactor);
				particle.lifeMilliseconds = (prop.minLife + getRandom()*(prop.maxLife-prop.minLife))*(1.0 / 24.0)*1000;
				particle.acceleration = prop.minAcceleration + getRandom()*(prop.maxAcceleration-prop.minAcceleration);
				
				this.getInterpolationValue(0, prop.rotateKeys, rotate);
				var factor:Number = prop.rotateVMin + getRandom() * (prop.rotateVMax - prop.rotateVMin);
				particle.rotateV = factor;
				
				if (prop.dirType == UnitBaseProp.DIR_TYPE_CAMERA) {
					temp = this.getFollowNode().localToCameraTransform;
					
					localx = particle.orgLocalX;
					localy = particle.orgLocalY;
					localz = particle.orgLocalZ;
					particle.x = localx + temp.d;
					particle.y = localy + temp.h;
					particle.z = localz + temp.l;
					
				} else {
					
					particle.x = particle.localX;
					particle.y = particle.localY;
					particle.z = particle.localZ;
				}
				
				var pos:int = 0;
				pos += prop.uvRangeBegin;
				var col:int = pos % prop.uvColumnsCount;
				var row:int = pos / prop.uvColumnsCount;
				
				particle.width = particle.orgWidth * prop.scaleKeys[0].items[0];
				particle.height = particle.orgHeight * prop.scaleKeys[0].items[1];
				particle.originX = 0.5;
				particle.originY = 0.5;
				if (prop.uvType == Defines.UV_TYPE_FRAME) {
					particle.uvScaleX = 1/prop.uvColumnsCount;
					particle.uvScaleY = 1/prop.uvRowsCount;
					particle.uvOffsetX = col/prop.uvColumnsCount;
					particle.uvOffsetY = row/prop.uvRowsCount;
				} else {
					particle.uvScaleX = prop.uvUScale;
					particle.uvScaleY = prop.uvVScale;
					particle.uvOffsetX = prop.uvUOffset;
					particle.uvOffsetY = prop.uvVOffset;
				}
				
				particle.red = prop.colorKeys[0].items[0];
				particle.green = prop.colorKeys[0].items[1];
				particle.blue = prop.colorKeys[0].items[2];
				particle.alpha = prop.colorKeys[0].items[3];
				
				particle.next = particleList;
				particleList = particle;
			}
		}
		
		public function initParticle(particle:Particle):void
		{
			var x:Number, y:Number, z:Number, len:Number, temp:Number, angle:Number;
			var prop:ParticleUnitProp = this.unitProp as ParticleUnitProp;
			//emit pos
			if (prop.emitType == ParticleUnitProp.EMIT_TYPE_POINT) {
				
				if (prop.emitTypeValueX == 0 &&
					prop.emitTypeValueY == 0 &&
					prop.emitTypeValueZ == 0) {
					particle.localX = 0;
					particle.localY = 0;
					particle.localZ = 0;
				} else {
					len = getRandom()* Math.sqrt(prop.emitTypeValueX * prop.emitTypeValueX +
						prop.emitTypeValueY * prop.emitTypeValueY +
						prop.emitTypeValueZ * prop.emitTypeValueZ);
					particle.localX = prop.emitTypeValueX * len;
					particle.localY = prop.emitTypeValueY * len;
					particle.localZ = prop.emitTypeValueZ * len;
				}
				
			} else if (prop.emitType == ParticleUnitProp.EMIT_TYPE_CYCLE_EDGE || 
				prop.emitType == ParticleUnitProp.EMIT_TYPE_CYCLE) {
				x = ( getRandom() - 0.5 ) * 2;
				y = ( getRandom() - 0.5 ) * 2;
				z = ( getRandom() - 0.5 ) * 2;
				
				particle.localX = (y*prop.emitPlane.z -prop.emitPlane.y*z);
				particle.localY = (x*prop.emitPlane.z -prop.emitPlane.x*z);
				particle.localZ = (x*prop.emitPlane.y -prop.emitPlane.x*y);
				if (prop.emitType == ParticleUnitProp.EMIT_TYPE_CYCLE_EDGE){
					temp = prop.emitTypeValueX;
				}
				else {
					temp = getRandom()*prop.emitTypeValueX;
				}
				
				len = temp / Math.sqrt(particle.localX*particle.localX + particle.localY*particle.localY
					+ particle.localZ *particle.localZ);
				
				particle.localX*=len;
				particle.localY*=len;
				particle.localZ*=len;
			} else if(prop.emitType == ParticleUnitProp.EMIT_TYPE_SPHERE_EDGE || 
				prop.emitType == ParticleUnitProp.EMIT_TYPE_SPHERE ) {
				x = ( getRandom() - 0.5 ) * 2;
				y = ( getRandom() - 0.5 ) * 2;
				z = ( getRandom() - 0.5 ) * 2;
				
				if (prop.emitType == ParticleUnitProp.EMIT_TYPE_SPHERE_EDGE) {
					temp = prop.emitTypeValueX
				} else {
					temp = getRandom()*prop.emitTypeValueX;
				}
				
				len = temp / Math.sqrt(x*x + y*y + z*z);
				particle.localX = x*len;
				particle.localY = y*len;
				particle.localZ = z*len;
			} else if (prop.emitType == ParticleUnitProp.EMIT_TYPE_RECT) {
				x = ( getRandom() - 0.5 ) * 2;
				y = ( getRandom() - 0.5 ) * 2;
				z = 0;//( getRandom() - 0.5 ) * 2;
				
				particle.localX = x*prop.emitTypeValueX;
				particle.localY = y*prop.emitTypeValueX*prop.emitTypeValueY;
				particle.localZ = z*prop.emitTypeValueX;
			} else if (prop.emitType == ParticleUnitProp.EMIT_TYPE_CURVE) {
				x = (getRandom() - 0.5) * 2 * prop.emitTypeValueX;
				y = Math.pow(x, prop.emitTypeValueY) * prop.emitTypeValueZ;
				z = 0;
				particle.localX = x;
				particle.localY = y;
				particle.localZ = z;
			}
			
			var percent:Number = (system.getCurFrame() - this.getBeginFrame()) / prop.lifeFramesNum;
			this.getInterpolationValue(percent, prop.offsetKeys, offset);
			var minDir:Vector3D = prop.minEmitDir;
			var maxDir:Vector3D = prop.maxEmitDir;
			//emit direction
			if (prop.emitDirType == ParticleUnitProp.EMITDIR_TYPE_DEFAULT ){
				
				len = getRandom();
				particle.emitDirLocal.x = minDir.x + len*(maxDir.x - minDir.x);
				particle.emitDirLocal.y = minDir.y + len*(maxDir.y - minDir.y);
				particle.emitDirLocal.z = minDir.z + len*(maxDir.z - minDir.z);
				x = particle.emitDirLocal.x;
				y = particle.emitDirLocal.y;
				z = particle.emitDirLocal.z;
				particle.curEmitDirLen = Math.sqrt(x*x + y*y + z*z);
				particle.emitDirLocal.x = x / particle.curEmitDirLen;
				particle.emitDirLocal.y = y / particle.curEmitDirLen;
				particle.emitDirLocal.z = z / particle.curEmitDirLen;
			}
			else if(prop.emitDirType == ParticleUnitProp.EMITDIR_TYPE_EMITPLANE_NORMAL) {
				particle.emitDirLocal.x = prop.emitPlane.x;
				particle.emitDirLocal.y = prop.emitPlane.y;
				particle.emitDirLocal.z = prop.emitPlane.z;
				particle.curEmitDirLen = minDir.z + getRandom()*(maxDir.z - minDir.z);
			}
			else if(prop.emitDirType == ParticleUnitProp.EMITDIR_TYPE_USE_POS ){
				x = particle.localX;
				y = particle.localY;
				z = particle.localZ;
				particle.emitDirLocal.x = x;
				particle.emitDirLocal.y = y;
				particle.emitDirLocal.z = z;
				len = 1 / Math.sqrt(x*x + y*y + z*z);
				particle.emitDirLocal.x*=len;
				particle.emitDirLocal.y*=len;
				particle.emitDirLocal.z*=len;
				particle.curEmitDirLen = minDir.z + getRandom()*(maxDir.z - minDir.z);
			}
			
			var tempEmitDir:Vector3D = particle.emitDirLocal;
			
			if (!prop.completelyFollow) {
				
				if (prop.faceType == UnitBaseProp.DIR_TYPE_EMIT_VELOCITY ||
					prop.faceType == UnitBaseProp.DIR_TYPE_FLY_VELOCITY ||
					prop.faceType == UnitBaseProp.DIR_TYPE_FLY_VELOCITY_CAMERA) {
					
					particle.calcAxisTransform(tempEmitDir);
				}
			}
			//direction
			if (prop.emitDirTypeValue > 0) {
				var orgDir:Vector3D = pool.popVector3D();
				orgDir.x = tempEmitDir.x;
				orgDir.y = tempEmitDir.y;
				orgDir.z = tempEmitDir.z;
				var randDir:Vector3D = pool.popVector3D();
				randDir.x = orgDir.x + 1;
				randDir.y = orgDir.y + 2;
				randDir.z = orgDir.z + 3;
				randDir.normalize();
				var nDir:Vector3D = randDir.crossProduct(orgDir);
				
				var rotate:Matrix3D = pool.popMatrix3D();
				rotate.identity();
				rotate.appendRotation(getRandom() * 360, orgDir);
				nDir = rotate.transformVector(nDir);
				nDir.normalize();
				nDir.scaleBy(prop.emitDirTypeValue);
				
				particle.emitDirLocal = orgDir.add(nDir);				
				x = particle.emitDirLocal.x;
				y = particle.emitDirLocal.y;
				z = particle.emitDirLocal.z;
				len = 1.0 / Math.sqrt(x*x + y*y + z*z);
				particle.emitDirLocal.x = len * x;
				particle.emitDirLocal.y = len * y;
				particle.emitDirLocal.z = len * z;
				
				pool.pushVector3D(randDir);
				pool.pushMatrix3D(rotate);
				pool.pushVector3D(orgDir);
			}
			
			//acceleration
			if (prop.accelerationType == ParticleUnitProp.ACCELERATION_TYPE_DEFAULT){
				particle.accelerationDir.x = prop.accelerationDir.x;
				particle.accelerationDir.y = prop.accelerationDir.y;
				particle.accelerationDir.z = prop.accelerationDir.z;
			}
			else if(prop.accelerationType == ParticleUnitProp.ACCELERATION_TYPE_V){
				particle.accelerationDir.x = tempEmitDir.x;
				particle.accelerationDir.y = tempEmitDir.y;
				particle.accelerationDir.z = tempEmitDir.z;
			}
			else if(prop.accelerationType == ParticleUnitProp.ACCELERATION_TYPE_OPPOSITE_V){
				particle.accelerationDir.x = -tempEmitDir.x;
				particle.accelerationDir.y = -tempEmitDir.y;
				particle.accelerationDir.z = -tempEmitDir.z;
			}
			
			len = Math.sqrt(particle.accelerationDir.x * particle.accelerationDir.x + 
				particle.accelerationDir.y * particle.accelerationDir.y + 
				particle.accelerationDir.z * particle.accelerationDir.z);
			if (len != 0)
				len = 1/len;
			particle.accelerationDir.x*=len;
			particle.accelerationDir.y*=len;
			particle.accelerationDir.z*=len;
			
			particle.localX += offset.items[0];
			particle.localY += offset.items[1];
			particle.localZ += offset.items[2];
			
			particle.orgLocalX = particle.localX;
			particle.orgLocalY = particle.localY;
			particle.orgLocalZ = particle.localZ;
			
			particle.selfOriginRotate = prop.selfOriginRotateMin + getRandom()*(prop.selfOriginRotateMax - prop.selfOriginRotateMin);
		}
		
		FlashGE function calculateAABB():void 
		{
			aabb.minX = boundBox.minX;//*scale + _position.x;
			aabb.minY = boundBox.minY;//*scale + _position.y;
			aabb.minZ = boundBox.minZ;//*scale + _position.z;
			aabb.maxX = boundBox.maxX;//*scale + _position.x;
			aabb.maxY = boundBox.maxY;//*scale + _position.y;
			aabb.maxZ = boundBox.maxZ;//*scale + _position.z;
		}
		
		
		override FlashGE function updateOneFrame(camera:Camera3D):void 
		{
			var prop:ParticleUnitProp = this.unitProp as ParticleUnitProp
			var curSystemFrame:int = system.getCurFrame();
			if (!isShow() || curSystemFrame < int(getBeginFrame()) || 
				this.material == null || this.material.textureFilesCount == 0) {
				return;
			}
			
			var texFile:TextureFile = this.material.getTexture(0);
			if (texFile == null || texFile.getTexture(camera.context3D) == null) {
				return;
			}
			
			if (ParticleUnit.limitParticlesCount) {
				return;
			}
			
			if (buffers3d == null || buffers3d.curContext3D != camera.context3D) {
				var index0:int;
				var index1:int;
				if (prop.blendType == ParticleUnitProp.BLEND_TYPE_DEFAULT) {
					index0 = prop.repeatIndex; 	   // mul
					index1 = prop.repeatIndex + 3; // mul2
				} else {
					index0 = prop.repeatIndex + 6; // add
					index1 = prop.repeatIndex + 9; // add2
				}
				
				this.buffers3d = checkAndCreate3D(index0, prop.repeatIndex, texFile.getFormatID(), camera.context3D);
			}
			
			if (this.buffers3d == null) {
				return;
			}
			
			var curUnitFrame:int = curSystemFrame - int(this.getBeginFrame());
			if (pool == null) {
				pool = Pool.getInstance();
			}
			calculate(camera, curUnitFrame);
			
		}

		override public function dispose():void
		{
			super.dispose();
			
			var tmp:Particle;
			var particle:Particle = this.particleList;
			while (particle!=null ) {
				particle.rotateS = 0;
				tmp = particle.next;
				particle.next = null;
				pool.pushParticle(particle);
				particle = tmp;
			}
			this.particleList = null;
		}
		
		private function compileProgram(mode:String, program:Array):ByteArray {
			var proc:Procedure = new Procedure(program);
			return proc.getByteCode(mode);
		}

		private function addMatMulMat(shaderArray:Array, arrayIndex:int, ret:String, tmp:String, step:int,
									  row0:String, row1:String, row2:String):int
		{
			shaderArray[arrayIndex++] = "mul "+ret+".xyzw, "+row0+".xyzw, c[t7.x].xxxx";
			shaderArray[arrayIndex++] = "mul "+tmp+".xyzw, "+row1+".xyzw, c[t7.x].yyyy";
			shaderArray[arrayIndex++] = "add "+ret+", "+ret+", " + tmp;
			shaderArray[arrayIndex++] = "mul "+tmp+".xyzw, "+row2+".xyzw, c[t7.x].zzzz";
			shaderArray[arrayIndex++] = "add "+ret+", "+ret+", " + tmp;
			shaderArray[arrayIndex++] = "add "+ret+".w, "+ret+".w, c[t7.x].w";
			return arrayIndex;
		}
		
		private function getShaderArray(prop:ParticleUnitProp, shaderArray:Array, arrayIndex:int, outValue:Object):int
		{
			if (prop.faceType == UnitBaseProp.DIR_TYPE_PARENT) {
				return getShaderArrayParent(prop, shaderArray, arrayIndex, outValue);
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_WORLD) {
				return getShaderArrayWorld(prop, shaderArray, arrayIndex, outValue);
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_CAMERA) {
				return getShaderArrayCamera(prop, shaderArray, arrayIndex, outValue);
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_EMIT_VELOCITY) {
				return getShaderArrayEmitVelocity(prop, shaderArray, arrayIndex, outValue);
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_FLY_VELOCITY) {
				return getShaderArrayFlyVelocity(prop, shaderArray, arrayIndex, outValue);
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_FLY_VELOCITY_CAMERA) {
				return getShaderArrayFlyVelocity(prop, shaderArray, arrayIndex, outValue);
			}
			
			Global.throwError("ParticleUnit::getShaderArray invalid faceType:" + prop.faceType);
			return arrayIndex;
		}
		
		private function getShaderArrayFlyVelocity(prop:ParticleUnitProp, shaderArray:Array, arrayIndex:int, outValue:Object):int
		{
			var stepIndex:int = 3;
			shaderArray[arrayIndex++] = "dp4 t0.x, a0, c[a1.z]";
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			shaderArray[arrayIndex++] = "dp4 t0.y, a0, c[t7.x]";
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			shaderArray[arrayIndex++] = "dp4 t0.z, a0, c[t7.x]";
			shaderArray[arrayIndex++] = "mov t0.w, a0.w";
			shaderArray[arrayIndex++] = "m44 o0, t0, c124";
			outValue.stepIndex = stepIndex;
			
			return arrayIndex++;
		}
		
		private function getShaderArrayEmitVelocity(prop:ParticleUnitProp, shaderArray:Array, arrayIndex:int, outValue:Object):int
		{
			var stepIndex:int = 0;
			
			//transform.fromAxisAngle(prop.selfRotateAxis.x, prop.selfRotateAxis.y, 
			//	prop.selfRotateAxis.z, this.selfOriginRotate + rotateS);
			
			//this.localToCameraTransform.append(transform);
			shaderArray[arrayIndex++] = "mov t3, c[a1.z]";
			stepIndex++;
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			shaderArray[arrayIndex++] = "mul t0.xyzw, t3.xyzw, c[a1.z+"+stepIndex+"].xyzw"; // a, b, c, d
			stepIndex++;
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			shaderArray[arrayIndex++] = "mul t1.xyzw, t3.xyzw, c[a1.z+"+stepIndex+"].xyzw"; // e, f, g, h
			stepIndex++;
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			shaderArray[arrayIndex++] = "mul t2.xyzw, t3.xyzw, c[a1.z+"+stepIndex+"].xyzw"; // i, j, k, l
			stepIndex++;
			
			//transform.compose(0, 0, 0, this.rotationX, this.rotationY, this.rotationZ, 1, 1, 1);
			
			//this.localToCameraTransform.append(transform);
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			
			
			//this.localToCameraTransform.append(prop.normalTrans);
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
			
			//this.localToCameraTransform.append(this.axisTransform);
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			
			//this.localToCameraTransform.appendPosition(this.x, this.y, this.z);
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			shaderArray[arrayIndex++] = "add t3.w, t3.w, c[t7.x].x";
			shaderArray[arrayIndex++] = "add t4.w, t4.w, c[t7.x].y";
			shaderArray[arrayIndex++] = "add t5.w, t5.w, c[t7.x].z";
			stepIndex++;
			
			//this.localToCameraTransform.append(camera.getRootNode().localToCameraTransform);
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
			shaderArray[arrayIndex++] = "dp4 t0.x, a0, t0";
			shaderArray[arrayIndex++] = "dp4 t0.y, a0, t1";
			shaderArray[arrayIndex++] = "dp4 t0.z, a0, t2";
			shaderArray[arrayIndex++] = "mov t0.w, a0.w";
			shaderArray[arrayIndex++] = "m44 o0, t0, c124";
			outValue.stepIndex = stepIndex;
			return arrayIndex;
		}
		
		private function getShaderArrayCamera(prop:ParticleUnitProp, shaderArray:Array, arrayIndex:int, outValue:Object):int
		{
			var stepIndex:int = 0;	

			if (prop.followLinkNodeScale) {
								
				shaderArray[arrayIndex++] = "mov t3, c[a1.z]";
				stepIndex++;
				
				shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
				shaderArray[arrayIndex++] = "mul t0.xyzw, t3.xyzw, c[t7.x].xyzw"; // a, b, c, d
				stepIndex++;
				
				shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
				shaderArray[arrayIndex++] = "mul t1.xyzw, t3.xyzw, c[t7.x].xyzw"; // e, f, g, h
				stepIndex++;
				
				shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
				shaderArray[arrayIndex++] = "mul t2.xyzw, t3.xyzw, c[t7.x].xyzw"; // i, j, k, l
				stepIndex++;
				
			} else {
				
				shaderArray[arrayIndex++] = "mov t0.xyzw, c[a1.z].xyzw"; // a, b, c, d
				stepIndex++;
				
				shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
				shaderArray[arrayIndex++] = "mov t1.xyzw, c[t7.x].xyzw"; // e, f, g, h
				stepIndex++;
				
				shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
				shaderArray[arrayIndex++] = "mov t2.xyzw, c[t7.x].xyzw"; // i, j, k, l
				stepIndex++;
			}
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
			shaderArray[arrayIndex++] = "dp4 t0.x, a0, t0";
			shaderArray[arrayIndex++] = "dp4 t0.y, a0, t1";
			shaderArray[arrayIndex++] = "dp4 t0.z, a0, t2";
			shaderArray[arrayIndex++] = "mov t0.w, a0.w";
			shaderArray[arrayIndex++] = "m44 o0, t0, c124";
			
			outValue.stepIndex = stepIndex;
			return arrayIndex;
		}
		
		private function getShaderArrayWorld(prop:ParticleUnitProp, shaderArray:Array, arrayIndex:int, outValue:Object):int
		{
			var stepIndex:int = 0;	
			
			shaderArray[arrayIndex++] = "mov t3, c[a1.z]";
			stepIndex++;
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			shaderArray[arrayIndex++] = "mul t0.xyzw, t3.xyzw, c[t7.x].xyzw"; // a, b, c, d
			stepIndex++;
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";			
			shaderArray[arrayIndex++] = "mul t1.xyzw, t3.xyzw, c[t7.x].xyzw"; // e, f, g, h
			stepIndex++;
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			shaderArray[arrayIndex++] = "mul t2.xyzw, t3.xyzw, c[t7.x].xyzw"; // i, j, k, l
			stepIndex++;
			
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			shaderArray[arrayIndex++] = "add t0.w, t0.w, c[t7.x].x";
			shaderArray[arrayIndex++] = "add t1.w, t1.w, c[t7.x].y";
			shaderArray[arrayIndex++] = "add t2.w, t2.w, c[t7.x].z";
			stepIndex++;
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			
			shaderArray[arrayIndex++] = "dp4 t0.x, a0, t3";
			shaderArray[arrayIndex++] = "dp4 t0.y, a0, t4";
			shaderArray[arrayIndex++] = "dp4 t0.z, a0, t5";
			shaderArray[arrayIndex++] = "mov t0.w, a0.w";
			shaderArray[arrayIndex++] = "m44 o0, t0, c124";
			
			outValue.stepIndex = stepIndex;
			return arrayIndex;
		}
		
		private function getShaderArrayParent(prop:ParticleUnitProp, shaderArray:Array, arrayIndex:int, outValue:Object):int
		{
			/*
			localToCameraTransform:
			t0:(a,e,i,0)
			t1:(b,f,j,0)
			t2:(c,g,k,0)
			t3:(d,h,l,1)
			*/
			
			var stepIndex:int = 0;
			
			// t0(width, height, 1, 1)
			// a, f, k, 1
			shaderArray[arrayIndex++] = "mov t3, c[a1.z]";
			stepIndex++;
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			shaderArray[arrayIndex++] = "mul t0.xyzw, t3.xyzw, c[t7.x].xyzw"; // a, b, c, d
			stepIndex++;
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			shaderArray[arrayIndex++] = "mul t1.xyzw, t3.xyzw, c[t7.x].xyzw"; // e, f, g, h
			stepIndex++;
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			shaderArray[arrayIndex++] = "mul t2.xyzw, t3.xyzw, c[t7.x].xyzw"; // i, j, k, l
			stepIndex++;
			
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;

			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
			shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
			
			if (!prop.followLinkNodeScale) {
				shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
				shaderArray[arrayIndex++] = "mov t3, c[t7.x]";stepIndex++;
				shaderArray[arrayIndex++] = "rcp t3, t3";
				shaderArray[arrayIndex++] = "mul t0, t0, t3.xxxx";
				shaderArray[arrayIndex++] = "mul t1, t1, t3.yyyy";
				shaderArray[arrayIndex++] = "mul t2, t2, t3.zzzz";
			}
			/*
			if (prop.completelyFollow) {
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
				
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
				
				shaderArray[arrayIndex++] = "add t0.w, t0.w, c[a1.z+"+stepIndex+"].x";
				shaderArray[arrayIndex++] = "add t1.w, t1.w, c[a1.z+"+stepIndex+"].y";
				shaderArray[arrayIndex++] = "add t2.w, t2.w, c[a1.z+"+stepIndex+"].z";
				stepIndex++;
				
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
				shaderArray[arrayIndex++] = "dp4 t0.x, a0, t3";
				shaderArray[arrayIndex++] = "dp4 t0.y, a0, t4";
				shaderArray[arrayIndex++] = "dp4 t0.z, a0, t5";
				shaderArray[arrayIndex++] = "mov t0.w, a0.w";
				shaderArray[arrayIndex++] = "m44 o0, t0, c124";
			} else */
			{
				
				shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
				shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
				shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", stepIndex, "t0", "t1", "t2");stepIndex++;
				
				shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
				shaderArray[arrayIndex++] = "add t3.w, t3.w, c[t7.x].x";
				shaderArray[arrayIndex++] = "add t4.w, t4.w, c[t7.x].y";
				shaderArray[arrayIndex++] = "add t5.w, t5.w, c[t7.x].z";
				stepIndex++;
				
				shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
				shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
				shaderArray[arrayIndex++] = "add t7.x, t7.x, c121.w";
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", stepIndex, "t3", "t4", "t5");stepIndex++;
				shaderArray[arrayIndex++] = "dp4 t0.x, a0, t0";
				shaderArray[arrayIndex++] = "dp4 t0.y, a0, t1";
				shaderArray[arrayIndex++] = "dp4 t0.z, a0, t2";
				shaderArray[arrayIndex++] = "mov t0.w, a0.w";
				shaderArray[arrayIndex++] = "m44 o0, t0, c124";
			}
			outValue.stepIndex = stepIndex;
			// (17 or 20) + 2
			return arrayIndex;
		}
		
		private function checkAndCreate3D(index:int, texOpIndex:int, 
										  texFormatID:int, 
										  context:Context3D):ParticleContext3DBuffer
		{
			var prop:ParticleUnitProp = this.unitProp as ParticleUnitProp;
			var ret:ParticleContext3DBuffer;
			var programes:Dictionary = ParticleUnit.buffer3DList;
			/*var key:String = index  //8
				+ "|" + texFormatID //4
				+ "|" + prop.faceType //4
				+ "|" + prop.followLinkNodeScale//2 
				+ "|" + prop.completelyFollow;//2
			*/
			var key:int = (index << 20) | (texFormatID << 12) | (prop.faceType << 8) | (int(prop.followLinkNodeScale) << 4) | (int(prop.completelyFollow) << 2);
			ret = programes[key];
			
			if (ret != null) {
				if (ret.curContext3D == context) {
					return ret;
				} else {
					if (ret.indices != null && ret.vertices != null && ret.vertexBytes != null && ret.fragmentBytes != null) {
						if (ret.curIndexBuffer != null) {
							ret.curIndexBuffer.dispose();
							ret.curIndexBuffer = null;
						}
						
						if (ret.curVertexBuffer != null) {
							ret.curVertexBuffer.dispose();
							ret.curVertexBuffer = null;
						}
						
						if (ret.curProgram3D != null) {
							ret.curProgram3D.dispose();
							ret.curProgram3D = null;
						}
						ret.curIndexBuffer = context.createIndexBuffer(ret.curLimit*6);
						ret.curIndexBuffer.uploadFromVector(ret.indices, 0, ret.curLimit*6);
						ret.curVertexBuffer = context.createVertexBuffer(ret.curLimit * 4, 6);
						ret.curVertexBuffer.uploadFromVector(ret.vertices, 0, ret.curLimit*4);
						ret.curProgram3D = context.createProgram();
						ret.curProgram3D.upload(ret.vertexBytes, ret.fragmentBytes);
						ret.curContext3D = context;
						return ret;
					}
				}
			}
			
			if (ret == null) {
				ret = new ParticleContext3DBuffer;
			}
			
			if (ret.vertexBytes == null || ret.fragmentBytes == null) {
			
				var fragProgram:Array = [];
				var i:int = 0;
				var tm:Vector.<String> = Defines.REPEATS;
				
				var texFormat:String = Defines.TEX_FORMATS[texFormatID];
				fragProgram[i++] = "tex t0, v0, s0 <2d,"+tm[texOpIndex]+",linear,miplinear,"+texFormat+">";
				if (index < 3) {
					fragProgram[i++] = "mul t0, t0, v1";
					fragProgram[i++] = "sub t2.x, t0.w, c0.w";
					fragProgram[i++] = "kil t2.x";
					fragProgram[i++] = "mov o0, t0";
				} else if(index < 6) {
					fragProgram[i++] = "mul t0, t0, v1";
					fragProgram[i++] = "slt t2.x, t0.w, c0.w";
					fragProgram[i++] = "mul t0.w, t0.w, t2.x";
					fragProgram[i++] = "mov o0, t0";
				} else if(index < 9) {
					fragProgram[i++] = "mov t0.w, t0.x";
					fragProgram[i++] = "sub t2.x, t0.w, c0.w";
					fragProgram[i++] = "kil t2.x";
					fragProgram[i++] = "mov o0, t0";
				} else {
					fragProgram[i++] = "mov t0.w, t0.x";
					fragProgram[i++] = "slt t2.x, t0.w, c0.w";
					fragProgram[i++] = "mul t0.w, t0.w, t2.x";
					fragProgram[i++] = "mov o0, t0";
				}
				
				var shaderArray:Array = [];
				var shaderIndex:int = 0;
				var outValue:Object = new Object;
				
				shaderArray[shaderIndex++] = "mov t7.x, a1.z";
				shaderIndex = this.getShaderArray(prop, shaderArray, shaderIndex, outValue); 
				
				/*
				shaderArray[shaderIndex++] = "mov t2, c[a1.z+"+outValue.stepIndex+"]";outValue.stepIndex++;// uvScaleX, uvScaleY, uvOffsetX, uvOffsetY
				shaderArray[shaderIndex++] = "mul t1.xy, a1.xy, t2.xy";
				shaderArray[shaderIndex++] = "add t1.xy, t1.xy, t2.zw";
				shaderArray[shaderIndex++] = "mov t1.zw, a1.zw";
				shaderArray[shaderIndex++] = "mov v0, t1";
				shaderArray[shaderIndex++] = "mov v1, c[a1.z+"+outValue.stepIndex+"]";outValue.stepIndex++;// red, green, blue, alpha
				*/
				shaderArray[shaderIndex++] = "add t7.x, t7.x, c121.w";
				shaderArray[shaderIndex++] = "mov t2, c[t7.x]";outValue.stepIndex++;// uvScaleX, uvScaleY, uvOffsetX, uvOffsetY
				shaderArray[shaderIndex++] = "mul t1.x, a1.x, t2.x";
				shaderArray[shaderIndex++] = "mul t1.y, a1.y, t2.y";
				
				shaderArray[shaderIndex++] = "sub t1.x, t1.x, c121.y";
				shaderArray[shaderIndex++] = "sub t1.y, t1.y, c121.z";
				shaderArray[shaderIndex++] = "mov t0.z, c121.x";
				shaderArray[shaderIndex++] = "sin t0.x, t0.z"; // sin
				shaderArray[shaderIndex++] = "cos t0.y, t0.z"; // cos
				
				shaderArray[shaderIndex++] = "mul t0.z, t1.x, t0.y"; // x*cos
				shaderArray[shaderIndex++] = "mul t0.w, t1.y, t0.x"; // y*sin
				shaderArray[shaderIndex++] = "sub t3.x, t0.z, t0.w"; // X
				
				shaderArray[shaderIndex++] = "mul t0.z, t1.x, t0.x"; // x*sin
				shaderArray[shaderIndex++] = "mul t0.w, t1.y, t0.y"; // y*cos
				shaderArray[shaderIndex++] = "add t3.y, t0.z, t0.w"; // Y 						
				
				shaderArray[shaderIndex++] = "add t3.x, t3.x, c121.y";
				shaderArray[shaderIndex++] = "add t3.y, t3.y, c121.z";
				
				shaderArray[shaderIndex++] = "add t1.x, t3.x, t2.z";
				shaderArray[shaderIndex++] = "add t1.y, t3.y, t2.w";
				shaderArray[shaderIndex++] = "mov t1.zw, a1.zw";
				shaderArray[shaderIndex++] = "mov v0, t1";			
				shaderArray[shaderIndex++] = "add t7.x, t7.x, c121.w";
				shaderArray[shaderIndex++] = "mov v1, c[t7.x]";outValue.stepIndex++;// red, green, blue, alpha
				
				ret.vertexBytes = compileProgram(Context3DProgramType.VERTEX, shaderArray);
				ret.fragmentBytes = compileProgram(Context3DProgramType.FRAGMENT, fragProgram);
			}
			
			var curStepIndex:int;
			var curLimit:int;
			if (outValue == null) {
				curStepIndex = ret.curStepIndex
				curLimit = ret.curLimit;
				
			} else {
				curStepIndex = outValue.stepIndex;
				curLimit = (123 / curStepIndex);
				ret.curLimit = curLimit;
				ret.curStepIndex = curStepIndex;
			}
			var vertices:Vector.<Number> = new Vector.<Number>();
			var indices:Vector.<uint> = new Vector.<uint>();
			for (i = 0; i < curLimit; i++) {
				vertices.push(
					-0.5,-0.5, 0, 0,0,i*curStepIndex,
					-0.5, 0.5, 0, 0,1,i*curStepIndex,
					 0.5, 0.5, 0, 1,1,i*curStepIndex,
					 0.5,-0.5, 0, 1,0,i*curStepIndex);
				indices.push(i*4, i*4 + 1, i*4 + 3, i*4 + 2, i*4 + 3, i*4 + 1);
			}
			
			ret.indices = indices;
			ret.vertices = vertices;
			
			try {
				if (ret.curIndexBuffer != null) {
					ret.curIndexBuffer.dispose();
					ret.curIndexBuffer = null;
				}
				ret.curIndexBuffer = context.createIndexBuffer(curLimit*6);
				CONFIG::DEBUG {
					Global.indexBuffersCount++;
				}
				ret.curIndexBuffer.uploadFromVector(indices, 0, curLimit*6);
				if (ret.curVertexBuffer != null) {
					ret.curVertexBuffer.dispose();
					ret.curVertexBuffer = null;
				}
				ret.curVertexBuffer = context.createVertexBuffer(curLimit * 4, 6);
				CONFIG::DEBUG {
					Global.vertexBuffersCount++;
				}
				ret.curVertexBuffer.uploadFromVector(vertices, 0, curLimit*4);
				
				if (ret.curProgram3D != null) {
					ret.curProgram3D.dispose();
					ret.curProgram3D = null;
				}
				ret.curProgram3D = context.createProgram();
				ret.curProgram3D.upload(ret.vertexBytes, ret.fragmentBytes);
				ret.curContext3D = context;
				
				
			} catch(e:Error) {
				if (ret.curIndexBuffer != null) {
					CONFIG::DEBUG {
						Global.indexBuffersCount--;
					}
					ret.curIndexBuffer.dispose();
				}
				
				if (ret.curVertexBuffer != null) {
					CONFIG::DEBUG {
						Global.vertexBuffersCount--;
					}
					ret.curVertexBuffer.dispose();
				}
				
				if (ret.curProgram3D != null) {
					ret.curProgram3D.dispose();
				}
				
				if (e.errorID == 3672) {
					if (Global.particlesErrorCount == 3) {
						Global.needDisposeContext3d = true;
					}
					Global.particlesErrorCount++;
				}
				
				CONFIG::DEBUG {
					Global.warning("ParticleUnit::checkAndCreate3D" + e.errorID + "," + e.message + "," + curLimit);
				}
				return null;
			}
			
			
			programes[key] = ret;
			return ret;
		}
		
		private function flush(renderUnit:RenderUnit, camera:Camera3D):void 
		{
			var texFile:TextureFile = this.material.getTexture(0);//.getTexture(camera.context3D);
			if (texFile == null || texFile.getTexture(camera.context3D) == null)
				return;
			
			var prop:ParticleUnitProp = this.unitProp as ParticleUnitProp;
			// Fill
			renderUnit.indexBuffer = buffers3d.curIndexBuffer;
			renderUnit.firstIndex = 0;
			renderUnit.object = this;
			renderUnit.numTriangles = particlesCounter << 1;
			
			var index0:int;
			var index1:int;
			
			if (prop.blendType == ParticleUnitProp.BLEND_TYPE_DEFAULT) {
				index0 = prop.repeatIndex; 	  // mul
				index1 = prop.repeatIndex + 3; // mul2
			}
			else {
				index0 = prop.repeatIndex + 6; //add
				index1 = prop.repeatIndex + 9; //add2
			}
			
			renderUnit.program = buffers3d.curProgram3D;
			
			// Set streams
			renderUnit.setVertexBufferAt(0, buffers3d.curVertexBuffer, 0, Context3DVertexBufferFormat.FLOAT_3);
			renderUnit.setVertexBufferAt(1, buffers3d.curVertexBuffer, 3, Context3DVertexBufferFormat.FLOAT_3);
			// Set constants
			
			renderUnit.setVertexConstantsFromNumbers(121, prop.uvAngle, 0.5, 0.5, 1);
			renderUnit.setProjectionConstants(camera, 124);
			renderUnit.setFragmentConstantsFromNumbers(0, 0, 0, 0, 1);
			// Set textures
			renderUnit.releaseAllTextures();
			renderUnit.setTextureAt(0, texFile);
			// Set blending
			renderUnit.srcBlend = Defines.BLEND_FACTORS[prop.blendSrcType];
			renderUnit.dstBlend = Defines.BLEND_FACTORS[prop.blendDstType];
			renderUnit.culling = Context3DTriangleFace.NONE;
						
			if (this.unitProp.updateZ == 1) {
				camera.renderer.addRenderUnit(renderUnit, Defines.PRIORITY_NEXT_LAYER);
			} else if (this.unitProp.updateZ == 2) {
				camera.renderer.addRenderUnit(renderUnit, Defines.PRIORITY_TRANSPARENT_SORT);
			} else {
				camera.renderer.addRenderUnit(renderUnit, Defines.PRIORITY_OPAQUE);
			}

			var buffer:ParticleContext3DBuffer = checkAndCreate3D(index1, prop.texOpIndex, texFile.getFormatID(), camera.context3D);
			if (buffer != null) {
				var unitTemp:RenderUnit = pool.popRenderUnit();
				unitTemp.ref = renderUnit;
				renderUnit.addRef();
				unitTemp.program = buffer.curProgram3D;
				
				if (this.unitProp.updateZ == 1) {
					camera.renderer.addRenderUnit(unitTemp, Defines.PRIORITY_NEXT_LAYER);
				} else if (this.unitProp.updateZ == 2) {
					camera.renderer.addRenderUnit(unitTemp, Defines.PRIORITY_TRANSPARENT_SORT);
				} else {
					camera.renderer.addRenderUnit(unitTemp, Defines.PRIORITY_TRANSPARENT_SORT);
				}
				unitTemp.release();
			}
		}
		
		override public function clone():Entity
		{
			var unit:ParticleUnit = new ParticleUnit;
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override protected function clonePropertiesFrom(source:Entity):void
		{
			super.clonePropertiesFrom(source);
			var srcUnit:ParticleUnit = source as ParticleUnit;
		}
		
		override public function syncParamsToMaterial():void
		{
			super.syncParamsToMaterial();
			this.buffers3d = null;
			this.randomSeed = 0;
		}
	}
}