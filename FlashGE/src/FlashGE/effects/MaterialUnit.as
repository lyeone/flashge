package FlashGE.effects
{
	import flash.utils.Dictionary;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.MaterialUnitProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.materials.Material;
	import FlashGE.objects.Entity;
	import FlashGE.objects.Model;
	import FlashGE.objects.RenderObject;
	import FlashGE.render.Camera3D;
	
	use namespace FlashGE;
	public class MaterialUnit extends EffectUnitBase
	{	
		static private var offsetKey:KeyPair;
		static private var scaleKey:KeyPair;
		static private var colorKey:KeyPair;
		static private var rotateKey:KeyPair;
		
		private var cloneHoster:RenderObject;
		private var originScaleX:Number;
		private var originScaleY:Number;
		private var originScaleZ:Number;
		private var preRenderObject:RenderObject;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_MATERIAL;
		}
		
		static public function initStaticContext():void
		{
			MaterialUnit.offsetKey = new KeyPair;
			MaterialUnit.offsetKey.init(0);
			
			MaterialUnit.scaleKey = new KeyPair;
			MaterialUnit.scaleKey.init(0);
			
			MaterialUnit.colorKey = new KeyPair;
			MaterialUnit.colorKey.init(0);
			
			MaterialUnit.rotateKey = new KeyPair;
			MaterialUnit.rotateKey.init(0);
		}
		
		override public function contructor(unitProp:UnitBaseProp=null):void
		{
			super.contructor(unitProp);
			
			this.originScaleX = 0;
			this.originScaleY = 0;
			this.originScaleZ = 0;
			
			if (unitProp==null) {
				var newProp:MaterialUnitProp = new MaterialUnitProp;
				newProp.contructor();
				this.setProp(newProp);
			} else {
				this.setProp(unitProp);
			}
		}
		
		override public function dispose():void
		{
			clear();
			super.dispose();
		}
		
		override public function  syncParamsToMaterial():void
		{
			super.syncParamsToMaterial();
			clear();
		}
		
		private function clear(clearPreRenderObj:Boolean = true):void
		{
			if (this.cloneHoster != null) {
				this.cloneHoster.dispose();
				this.cloneHoster = null;
			}
			
			if (clearPreRenderObj && preRenderObject != null) {
				//preRenderObject.setRotationX(this.originRotateX);
				//preRenderObject.setRotationY(this.originRotateY);
				//preRenderObject.setRotationZ(this.originRotateZ);
				preRenderObject.setScaleX(this.originScaleX);
				preRenderObject.setScaleY(this.originScaleY);
				preRenderObject.setScaleZ(this.originScaleZ);
				
				var prop:MaterialUnitProp = this.unitProp as MaterialUnitProp;
				if (prop.cloneHoster != MaterialUnitProp.CLONE_NONE_KEEP) {
					preRenderObject.resetMaterials();
				}
				if (prop.cloneHoster == MaterialUnitProp.CLONE_NONE_FORCE_HIDE) {
					preRenderObject.setVisible(false);
				} else if(prop.cloneHoster == MaterialUnitProp.CLONE_NONE_FORCE_SHOW) {
					preRenderObject.setVisible(true);
				}
				preRenderObject = null;
			}
		}
		
		private function backupOrigins(obj:RenderObject):void
		{
			var scales:Object = obj.getOriginScales();
			if (scales != null ){
				this.originScaleX = scales.x;
				this.originScaleY = scales.y;
				this.originScaleZ = scales.z;
			} else {
				this.originScaleX = obj.getScaleX();
				this.originScaleY = obj.getScaleY();
				this.originScaleZ = obj.getScaleZ();
			}
		}
		
		override FlashGE function updateOneFrame(camera:Camera3D):void
		{
			
			var isEditor:Boolean = CONFIG::EDITOR;
			if (isEditor) {
				var percent:Number = getPercent();
				var prop:MaterialUnitProp = unitProp as MaterialUnitProp;
				if (preRenderObject != null && percent >= 1 && prop.cloneHoster == MaterialUnitProp.CLONE_NONE_FORCE_HIDE) {
					preRenderObject.setVisible(false);
				}
			}
		}
		
		override FlashGE function updateBeforRender(camera:Camera3D):void
		{
			if (!isShow()) {
				return;
			}
			
			var percent:Number = getPercent();
			if (percent < 0 || percent >= 1) {
				if (!system.loop) {
					if (percent >= 1) {
						this.clear(false);
					}
					
					return;
				} else {
					percent = 0;
				}
			}
			
			var prop:MaterialUnitProp = unitProp as MaterialUnitProp;
			var hoster:Entity = system.getLinkedHolder();
			if (hoster == null) {
				hoster = system.getLinkedNode();
			}
			
			if (hoster == null) {
				hoster = system.getParent();
			}
			
			var mat:Material;
			var child:Entity;
			var model:Model;
			var renderObject:RenderObject;
			if (hoster != null && cloneHoster==null && prop.cloneHoster == MaterialUnitProp.CLONE_ALL) {
				renderObject = hoster as RenderObject;
				if (renderObject != null && renderObject.getParent() != null) {
					this.cloneHoster = renderObject.clone();
					this.cloneHoster.setCastShadow(false);
					backupOrigins(this.cloneHoster);
					renderObject.getParent().addChild(this.cloneHoster);
				}				
			} else {
				
				if(preRenderObject == null) {
					renderObject = hoster as RenderObject;
					if (renderObject != null && renderObject.getParent() != null && renderObject.getModel()!=null) {
						this.preRenderObject = renderObject;
						backupOrigins(this.preRenderObject);
					}
				}
			}
			
			var dst:RenderObject = cloneHoster != null ? cloneHoster : preRenderObject;
			if (dst != null) {
				
				this.getInterpolationValue(percent, prop.offsetKeys, offsetKey);
				this.getInterpolationValue(percent, prop.rotateKeys, rotateKey);
				this.getInterpolationValue(percent, prop.scaleKeys, scaleKey);
				this.getInterpolationValue(percent, prop.colorKeys, colorKey);
				model = dst.getModel();
				for each(var subMesh:Entity in model.subMeshList) {
					
					mat = subMesh.getMaterial();
					if (mat != null) {
						initMaterial(mat, prop);
					}
				}
				
				var linkedPieces:Dictionary = dst.getLinkedPiecesList();
				if (linkedPieces != null) {
					for each (var piece:RenderObject in linkedPieces) {
						model = piece.getModel();
						if (model != null) {
							for each(subMesh in model.subMeshList) {
								mat = subMesh.getMaterial();
								if (mat != null) {
									initMaterial(mat, prop);
								}
							}
						}
					}
				}
				
				dst.setScale(this.originScaleX + scaleKey.items[0], 
					this.originScaleY + scaleKey.items[1], 
					this.originScaleZ + scaleKey.items[2]);
				//dst.setRotation(this.originRotateX + rotateKey.items[0], 
				//	this.originRotateY + rotateKey.items[1], 
				//	this.originRotateZ + rotateKey.items[2]);
				//dst.setPosition(this.originX + offsetKey.items[0], 
				//	this.originY + offsetKey.items[1], 
				//	this.originZ + offsetKey.items[2]);
			}
		}
		
		private function initMaterial(mat:Material, prop:MaterialUnitProp):void
		{
			mat.setCloseUVTrans(false);
			mat.setUseUVType(prop.uvType);
			mat.setSrcBlend(prop.blendSrcType);
			mat.setDstBlend(prop.blendDstType);
			mat.setAlphaTest(prop.alphaTest);
			mat.setCulling(Defines.CULLING_TYPE[0]);
			//mat.setOpenDoubleAlpha(true);
			mat.setUseVertDiffuse(false);
			mat.setUVUOffset(0);
			mat.setUVVOffset(0);
			mat.setMipIndex(prop.mipIndex);
			mat.setColor(colorKey.items[0], colorKey.items[1], colorKey.items[2], colorKey.items[3]);
			
			mat.setType(Defines.MATERIAL_EFFECT);
			mat.setRenderPriority(Defines.PRIORITY_TRANSPARENT_SORT_TRUE);
			mat.setOpenDoubleAlpha(false);
		}
	}
}