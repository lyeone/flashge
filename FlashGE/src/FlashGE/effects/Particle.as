/*@author lye 123*/

package FlashGE.effects 
{
	import flash.geom.Vector3D;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Global;
	import FlashGE.common.Transform3D;
	import FlashGE.effects.unitProp.ParticleUnitProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.render.Camera3D;
	import FlashGE.render.RenderUnit;
	
	use namespace FlashGE;
	
	public class Particle
	{
		public var localX:Number = 0;
		public var localY:Number = 0;
		public var localZ:Number = 0;
		
		public var orgLocalX:Number = 0;
		public var orgLocalY:Number = 0;
		public var orgLocalZ:Number = 0;
		
		public var x:Number = 0;
		public var y:Number = 0;
		public var z:Number = 0;
		
		public var rotationX:Number = 0;
		public var rotationY:Number = 0;
		public var rotationZ:Number = 0;
		public var rotationRatio:Number = 0;
		
		public var emitDirLocal:Vector3D = new Vector3D;
		public var curEmitDir:Vector3D = new Vector3D();
		public var curEmitDirLen:Number = 1;
		
		public var accelerationDir:Vector3D = new Vector3D();
		public var acceleration:Number = 0;
		
		public var orgWidth:Number = 0;
		public var orgHeight:Number = 0;		
		public var width:Number = 0;
		public var height:Number = 0;
		public var originX:Number = 0;
		public var originY:Number = 0;
		
		public var uvScaleX:Number = 0;
		public var uvScaleY:Number = 0;
		public var uvOffsetX:Number = 0;
		public var uvOffsetY:Number = 0;
		
		public var red:Number = 0;
		public var green:Number = 0;
		public var blue:Number = 0;
		public var alpha:Number = 0;
		public var startMilliseconds:int = 0;
		public var preUpdateMilliseconds:int = 0;
		public var lifeMilliseconds:int = 0;
		public var selfOriginRotate:Number = 0;
		
		public var next:Particle;
		
		public var axisTransform:Transform3D;
		public var localToCameraTransform:Transform3D = new Transform3D;
		
		static public var tempTransA:Transform3D = new Transform3D;
		static public var tempTransB:Transform3D = new Transform3D;
		static private var tempTrans:Transform3D = new Transform3D;
		static private var transform:Transform3D = new Transform3D;
		static private var tempx:Number = 0;
		static private var tempy:Number = 0;
		static private var tempz:Number = 0;
		
		static private var rotateAxisTemp:Vector3D = new Vector3D(1,0,0);
		//static private const axisxTemp:Vector3D = new Vector3D(1,0,0);
		
		public var rotateV:Number = 0;	
		public var rotateS:Number = 0;
		public var linkNodeTrans:Transform3D = new Transform3D;
		public var preCameraPosX:Number = 0;
		public var preCameraPosY:Number = 0;
		public var preCameraPosZ:Number = 0;
		public var frameCount:int = 0;
		private var math_sqrt:Function;
		private var math_acos:Function;
		public function Particle()
		{
			math_sqrt = Math.sqrt;
			math_acos = Math.acos;
			if (axisTransform != null) {
				axisTransform.identity();	
			} else {
				axisTransform = new Transform3D;
			}
			
		}
		
		public function initTrans(unit:ParticleUnit, camera:Camera3D):void
		{
			this.linkNodeTrans.combine(camera.localToGlobalTransform,
				unit.getFollowNode().localToCameraTransform);
			frameCount = 0;
		}
		
		public function beforeCalcTrans(unit:ParticleUnit, camera:Camera3D):void
		{
			frameCount++;
						
			var prop:ParticleUnitProp = unit.unitProp as ParticleUnitProp;
			
			if (prop.completelyFollow) {
				tempTransA.combine(camera.localToGlobalTransform, unit.getFollowNode().localToCameraTransform);
			} else {
				tempTransA.copy(this.linkNodeTrans);
			}
			
			var trans:Transform3D = prop.completelyFollow ? unit.getFollowNode().localToCameraTransform : this.linkNodeTrans;
			
			if (prop.dirType == UnitBaseProp.DIR_TYPE_PARENT) {
				
				tempx = this.orgLocalX;
				tempy = this.orgLocalY;
				tempz = this.orgLocalZ;
				this.localX = tempx*tempTransA.a + tempy*tempTransA.b + tempz*tempTransA.c + tempTransA.d;
				this.localY = tempx*tempTransA.e + tempy*tempTransA.f + tempz*tempTransA.g + tempTransA.h;
				this.localZ = tempx*tempTransA.i + tempy*tempTransA.j + tempz*tempTransA.k + tempTransA.l;	
				
				tempx = this.emitDirLocal.x;
				tempy = this.emitDirLocal.y;
				tempz = this.emitDirLocal.z;
				
				this.curEmitDir.x = tempx*tempTransA.a + tempy*tempTransA.b + tempz*tempTransA.c;
				this.curEmitDir.y = tempx*tempTransA.e + tempy*tempTransA.f + tempz*tempTransA.g;
				this.curEmitDir.z = tempx*tempTransA.i + tempy*tempTransA.j + tempz*tempTransA.k;
				var len:Number = this.curEmitDirLen / math_sqrt(this.curEmitDir.x * this.curEmitDir.x + 
					this.curEmitDir.y * this.curEmitDir.y + 
					this.curEmitDir.z * this.curEmitDir.z);
				
				if (prop.accelerationType == ParticleUnitProp.ACCELERATION_TYPE_V) {
					this.accelerationDir.x = this.curEmitDir.x;
					this.accelerationDir.y = this.curEmitDir.y;
					this.accelerationDir.z = this.curEmitDir.z;
				} else if (prop.accelerationType == ParticleUnitProp.ACCELERATION_TYPE_OPPOSITE_V) {
					this.accelerationDir.x = -this.curEmitDir.x;
					this.accelerationDir.y = -this.curEmitDir.y;
					this.accelerationDir.z = -this.curEmitDir.z;
				}
				
				this.curEmitDir.x*=len;
				this.curEmitDir.y*=len;
				this.curEmitDir.z*=len;
				
				len = math_sqrt(this.accelerationDir.x * this.accelerationDir.x + 
					this.accelerationDir.y * this.accelerationDir.y + 
					this.accelerationDir.z * this.accelerationDir.z);
				if (len!=0)
					len = 1/len;
				this.accelerationDir.x*=len;
				this.accelerationDir.y*=len;
				this.accelerationDir.z*=len;
				
				if (prop.faceType == UnitBaseProp.DIR_TYPE_EMIT_VELOCITY) {
					this.calcAxisTransform(this.curEmitDir);
				}
				
			} else {
				
				var rScalex:Number = math_sqrt(trans.a*trans.a + trans.e*trans.e + trans.i*trans.i);
				var rScaley:Number = math_sqrt(trans.b*trans.b + trans.f*trans.f + trans.j*trans.j);
				var rScalez:Number = math_sqrt(trans.c*trans.c + trans.g*trans.g + trans.k*trans.k);
				
				this.localX = this.orgLocalX * rScalex + tempTransA.d;
				this.localY = this.orgLocalY * rScaley + tempTransA.h;
				this.localZ = this.orgLocalZ * rScalez + tempTransA.l;
				
				this.curEmitDir.x = this.emitDirLocal.x * this.curEmitDirLen;
				this.curEmitDir.y = this.emitDirLocal.y * this.curEmitDirLen;
				this.curEmitDir.z = this.emitDirLocal.z * this.curEmitDirLen;
			}			
		}
		
		public function calcAxisTransform(emitDir:Vector3D):void
		{
			//var tempEmitDir:Vector3D = Pool.popVector3D();
			var tempEmitDirX:Number = emitDir.x;
			var tempEmitDirY:Number = emitDir.y;
			var tempEmitDirZ:Number = emitDir.z;
			var n:Number = 1.0/math_sqrt(tempEmitDirX*tempEmitDirX + 
				tempEmitDirY*tempEmitDirY + 
				tempEmitDirZ*tempEmitDirZ);
			tempEmitDirX*=n;
			tempEmitDirY*=n;
			tempEmitDirZ*=n;
			
			//var rotateAxis:Vector3D = Pool.popVector3D();
			//var axisx:Vector3D = Pool.popVector3D();
			//axisx.x = 1; 
			//axisx.y = 0;
			//axisx.z = 0;
			
			var rotateAxisX:Number = 0;
			var rotateAxisY:Number =-1;
			var rotateAxisZ:Number = 0;
			
			if (tempEmitDirX > -0.99) {
				
				rotateAxisX = 0;
				rotateAxisY =-tempEmitDirZ;
				rotateAxisZ = tempEmitDirY;
				n = 1.0/math_sqrt(rotateAxisY*rotateAxisY + rotateAxisZ*rotateAxisZ);
				
				rotateAxisY*=n;
				rotateAxisZ*=n;
			}
			
			if (this.axisTransform == null) {
				this.axisTransform = new Transform3D;
			}
			
			this.axisTransform.fromAxisAngle(rotateAxisX, rotateAxisY, rotateAxisZ, math_acos(tempEmitDirX)); 
			
			//Pool.pushVector3D(rotateAxis);
			//Pool.pushVector3D(axisx);
			//Pool.pushVector3D(tempEmitDir);
		}
		

		
		// prop.faceType==UnitBaseProp.DIR_TYPE_PARENT
		public function calcTransParent(unit:ParticleUnit, camera:Camera3D, renderUnit:RenderUnit, beginRegister:int):int
		{
			/*
			localToCameraTransform:
			t0:(a,e,i,0)
			t1:(b,f,j,0)
			t2:(c,g,k,0)
			t3:(d,h,l,1)
			*/
			var prop:ParticleUnitProp = unit.unitProp as ParticleUnitProp;
			
			//this.localToCameraTransform.composeScale(this.width, this.height, 1);
			renderUnit.setVertexConstantsFromNumbers(beginRegister, this.width, this.height, 1, 1);
			beginRegister++;
			
			// t0(width, height, 1, 1)
			// a, f, k, 1
			
			transform.fromAxisAngle(prop.selfRotateAxis.x, prop.selfRotateAxis.y, 
				prop.selfRotateAxis.z, this.selfOriginRotate + rotateS);

			//this.localToCameraTransform.append(transform);			
			renderUnit.setVertexConstantsFromTransform(beginRegister, transform);
			beginRegister+=3;

			transform.compose(0, 0, 0, this.rotationX, this.rotationY, this.rotationZ, 1, 1, 1);
			//this.localToCameraTransform.append(transform);
			renderUnit.setVertexConstantsFromTransform(beginRegister, transform);
			//renderUnit.setVertexConstantsFromNumbers(beginRegister, transform.a, transform.e, transform.i, 1
			beginRegister+=3;

			//this.localToCameraTransform.append(prop.normalTrans);
			renderUnit.setVertexConstantsFromTransform(beginRegister, prop.normalTrans);
			beginRegister+=3;
			
			if (!prop.followLinkNodeScale) {
				
				var trans:Transform3D = prop.completelyFollow ? unit.getFollowNode().localToCameraTransform : this.linkNodeTrans;
				var rScalex:Number = math_sqrt(trans.a*trans.a + trans.e*trans.e + trans.i*trans.i);
				var rScaley:Number = math_sqrt(trans.b*trans.b + trans.f*trans.f + trans.j*trans.j);
				var rScalez:Number = math_sqrt(trans.c*trans.c + trans.g*trans.g + trans.k*trans.k);
				
				//this.localToCameraTransform.appendScales(1/rScalex, 1/rScaley, 1/rScalez);
				
				renderUnit.setVertexConstantsFromNumbers(beginRegister, rScalex, rScaley, rScalez, 1);
				
				beginRegister++;
			}
			
			if (prop.completelyFollow) {
				tempTransA.combine(camera.localToGlobalTransform, unit.getFollowNode().localToCameraTransform);
				tempTransA.d = tempTransA.h = tempTransA.l = 0;
				
				//this.localToCameraTransform.append(tempTransA);
				//renderUnit.setVertexConstantsFromTransform(beginRegister, unit.getFollowNode().localToCameraTransform);
				//beginRegister+=3;
				
				renderUnit.setVertexConstantsFromTransform(beginRegister, tempTransA);
				beginRegister+=3;
				
				renderUnit.setVertexConstantsFromNumbers(beginRegister, this.x, this.y, this.z, 1);
				beginRegister++;
				
				renderUnit.setVertexConstantsFromTransform(beginRegister, camera.getRootNode().localToCameraTransform);
				beginRegister+=3;
			} else {
				tempTransA.copy(this.linkNodeTrans);
				tempTransA.d = tempTransA.h = tempTransA.l = 0;
				
				//this.localToCameraTransform.append(tempTransA);
				renderUnit.setVertexConstantsFromTransform(beginRegister, tempTransA);
				beginRegister+=3;
				
				renderUnit.setVertexConstantsFromNumbers(beginRegister, this.x, this.y, this.z, 1);
				beginRegister++;
				
				renderUnit.setVertexConstantsFromTransform(beginRegister, camera.getRootNode().localToCameraTransform);
				beginRegister+=3;
			}
			
			// (17 or 20) + 2
			//tempTransA.d = tempTransA.h = tempTransA.l = 0;
			//this.localToCameraTransform.append(tempTransA);
			
			//this.localToCameraTransform.appendPosition(this.x, this.y, this.z);
			//this.localToCameraTransform.append(camera.getRootNode().localToCameraTransform);
			return beginRegister;
		}// end calcTransParent
		
		//prop.faceType==UnitBaseProp.DIR_TYPE_WORLD
		public function calcTransWorld(unit:ParticleUnit, camera:Camera3D, renderUnit:RenderUnit, beginRegister:int):int
		{
			var prop:ParticleUnitProp = unit.unitProp as ParticleUnitProp;
			
			if (prop.followLinkNodeScale) {
				
				var trans:Transform3D = prop.completelyFollow ? unit.getFollowNode().localToCameraTransform : this.linkNodeTrans;
				
				var rScalex:Number = math_sqrt(trans.a*trans.a + trans.e*trans.e + trans.i*trans.i);
				var rScaley:Number = math_sqrt(trans.b*trans.b + trans.f*trans.f + trans.j*trans.j);
				var rScalez:Number = math_sqrt(trans.c*trans.c + trans.g*trans.g + trans.k*trans.k);	
				
				//this.localToCameraTransform.composeScale(this.width*rScalex, this.height*rScaley, rScalez);
				renderUnit.setVertexConstantsFromNumbers(beginRegister, this.width*rScalex, this.height*rScaley, rScalez, 1);
				beginRegister++;
			} else {
				
				//this.localToCameraTransform.composeScale(this.width, this.height, 1);
				renderUnit.setVertexConstantsFromNumbers(beginRegister, this.width, this.height, 1, 1);
				beginRegister++;
			}
			
			transform.fromAxisAngle(prop.selfRotateAxis.x, prop.selfRotateAxis.y, prop.selfRotateAxis.z, this.selfOriginRotate + rotateS);
			//this.localToCameraTransform.append(transform);
			renderUnit.setVertexConstantsFromTransform(beginRegister, transform);
			beginRegister+=3;
			
			transform.compose(0, 0, 0, this.rotationX, this.rotationY, this.rotationZ, 1, 1, 1);
			
			//this.localToCameraTransform.append(transform);
			renderUnit.setVertexConstantsFromTransform(beginRegister, transform);
			beginRegister+=3;
			//this.localToCameraTransform.append(prop.normalTrans);
			renderUnit.setVertexConstantsFromTransform(beginRegister, prop.normalTrans);
			beginRegister+=3;
			//this.localToCameraTransform.appendPosition(this.x, this.y, this.z);
			renderUnit.setVertexConstantsFromNumbers(beginRegister, this.x, this.y, this.z, 1);
			beginRegister++;
			//this.localToCameraTransform.append(camera.getRootNode().localToCameraTransform);
			renderUnit.setVertexConstantsFromTransform(beginRegister, camera.getRootNode().localToCameraTransform);
			beginRegister+=3;
			
			return beginRegister;
		} // end calcTransWorld
		
		//prop.faceType==UnitBaseProp.DIR_TYPE_CAMERA
		public function calcTransCamera(unit:ParticleUnit, camera:Camera3D, renderUnit:RenderUnit, beginRegister:int):int
		{
			var prop:ParticleUnitProp = unit.unitProp as ParticleUnitProp;
			
			var trans:Transform3D = prop.completelyFollow ? unit.getFollowNode().localToCameraTransform : this.linkNodeTrans;
			
			if (prop.followLinkNodeScale) {
				
				var rScalex:Number = math_sqrt(trans.a*trans.a + trans.e*trans.e + trans.i*trans.i);
				var rScaley:Number = math_sqrt(trans.b*trans.b + trans.f*trans.f + trans.j*trans.j);
				var rScalez:Number = math_sqrt(trans.c*trans.c + trans.g*trans.g + trans.k*trans.k);
				
				//this.localToCameraTransform.composeScale(rScalex, rScaley, rScalez);
				renderUnit.setVertexConstantsFromNumbers(beginRegister, rScalex, rScaley, rScalez, 1);
				beginRegister++;
				tempTransB.fromAxisAngle(prop.selfRotateAxis.x, prop.selfRotateAxis.y, 
					prop.selfRotateAxis.z, this.selfOriginRotate + rotateS);
				
				//this.localToCameraTransform.append(tempTransB);
				renderUnit.setVertexConstantsFromTransform(beginRegister, tempTransB);
				beginRegister+=3;
			} else {
				this.localToCameraTransform.fromAxisAngle(prop.selfRotateAxis.x, prop.selfRotateAxis.y, 
					prop.selfRotateAxis.z, this.selfOriginRotate + rotateS);
				
				renderUnit.setVertexConstantsFromTransform(beginRegister, localToCameraTransform);
				beginRegister+=3;
			}
			
			//this.localToCameraTransform.append(prop.normalTrans);
			renderUnit.setVertexConstantsFromTransform(beginRegister, prop.normalTrans);
			beginRegister+=3;

			var globalTrans:Transform3D = camera.globalToLocalTransform;
			var tempX:Number = x*globalTrans.a + y*globalTrans.b + z*globalTrans.c + globalTrans.d;
			var tempY:Number = x*globalTrans.e + y*globalTrans.f + z*globalTrans.g + globalTrans.h;
			var tempZ:Number = x*globalTrans.i + y*globalTrans.j + z*globalTrans.k + globalTrans.l;
			tempTransA.compose(tempX, tempY, tempZ, 
				this.rotationX, this.rotationY, this.rotationZ, 
				this.width, this.height, 1);
			
			//this.localToCameraTransform.append(tempTransA);
		
			renderUnit.setVertexConstantsFromTransform(beginRegister, tempTransA);
			beginRegister+=3;
			return beginRegister;
		} // end calcTransCamera
		
		// prop.faceType==UnitBaseProp.DIR_TYPE_EMIT_VELOCITY
		public function calcTransEmitVelocity(unit:ParticleUnit, camera:Camera3D, renderUnit:RenderUnit, beginRegister:int):int
		{
			var prop:ParticleUnitProp = unit.unitProp as ParticleUnitProp;
				
			if (prop.followLinkNodeScale) {
				var trans:Transform3D = prop.completelyFollow ? unit.getFollowNode().localToCameraTransform : this.linkNodeTrans;
			
				var rScalex:Number;
				var rScaley:Number;
				var rScalez:Number;	
				rScalex = math_sqrt(trans.a*trans.a + trans.e*trans.e + trans.i*trans.i);
				rScaley = math_sqrt(trans.b*trans.b + trans.f*trans.f + trans.j*trans.j);
				rScalez = math_sqrt(trans.c*trans.c + trans.g*trans.g + trans.k*trans.k);
				
				//this.localToCameraTransform.composeScale(rScalex, rScaley, rScalez);
				//this.localToCameraTransform.appendScales(this.width, this.height, 1);
				
				renderUnit.setVertexConstantsFromNumbers(beginRegister, rScalex*width, rScaley*height, rScalez, 1);
				beginRegister++;
			} else {
				//this.localToCameraTransform.identity();
				//this.localToCameraTransform.appendScales(this.width, this.height, 1);
				
				renderUnit.setVertexConstantsFromNumbers(beginRegister, width, height, 1, 1);
				beginRegister++;
			}
			
			transform.fromAxisAngle(prop.selfRotateAxis.x, prop.selfRotateAxis.y, 
				prop.selfRotateAxis.z, this.selfOriginRotate + rotateS);
			
		//	this.localToCameraTransform.append(transform);
			renderUnit.setVertexConstantsFromTransform(beginRegister, transform);
			beginRegister+=3;
			
			transform.compose(0, 0, 0, 
				this.rotationX, this.rotationY, this.rotationZ, 
				1, 1, 1);
			
		//	this.localToCameraTransform.append(transform);
			renderUnit.setVertexConstantsFromTransform(beginRegister, transform);
			beginRegister+=3;
			
		//	this.localToCameraTransform.append(prop.normalTrans);
			renderUnit.setVertexConstantsFromTransform(beginRegister, prop.normalTrans);
			beginRegister+=3;
			
		//	this.localToCameraTransform.append(this.axisTransform);
			renderUnit.setVertexConstantsFromTransform(beginRegister, this.axisTransform);
			beginRegister+=3;
			
		//	this.localToCameraTransform.appendPosition(this.x, this.y, this.z);
			renderUnit.setVertexConstantsFromNumbers(beginRegister, this.x, this.y, this.z, 1);
			beginRegister++;
		//	this.localToCameraTransform.append(camera.getRootNode().localToCameraTransform);
			renderUnit.setVertexConstantsFromTransform(beginRegister, camera.getRootNode().localToCameraTransform);
			beginRegister+=3;
			
			return beginRegister;
		}// end calcTransEmitVelocity
		
		// prop.faceType==UnitBaseProp.DIR_TYPE_FLY_VELOCITY
		public function calcTransFlyVelocity(unit:ParticleUnit, camera:Camera3D, renderUnit:RenderUnit, beginRegister:int):int
		{
			var prop:ParticleUnitProp = unit.unitProp as ParticleUnitProp;
			
			var trans:Transform3D = prop.completelyFollow ? unit.getFollowNode().localToCameraTransform : this.linkNodeTrans;
			
			var rScalex:Number;
			var rScaley:Number;
			var rScalez:Number;			
			var needIdentity:Boolean = true
			if (prop.followLinkNodeScale) {
				
				needIdentity = false;
				rScalex = math_sqrt(trans.a*trans.a + trans.e*trans.e + trans.i*trans.i);
				rScaley = math_sqrt(trans.b*trans.b + trans.f*trans.f + trans.j*trans.j);
				rScalez = math_sqrt(trans.c*trans.c + trans.g*trans.g + trans.k*trans.k);
				
				//this.localToCameraTransform.composeScale(rScalex, rScaley, rScalez);
				
				//this.localToCameraTransform.composeScale(this.width, this.height, 1);
				this.localToCameraTransform.composeScale(this.width*rScalex, this.height*rScaley, 1*rScalez);

			} else {
				//this.localToCameraTransform.identity();
				
				this.localToCameraTransform.composeScale(this.width, this.height, 1);
			}
			
			transform.fromAxisAngle(prop.selfRotateAxis.x, prop.selfRotateAxis.y, 
				prop.selfRotateAxis.z, this.selfOriginRotate + rotateS);
			
			this.localToCameraTransform.append(transform);
			
			transform.compose(0, 0, 0, this.rotationX, this.rotationY, this.rotationZ, 1, 1, 1);
			
			this.localToCameraTransform.append(transform);
			
			this.localToCameraTransform.append(prop.normalTrans);
			
			this.localToCameraTransform.append(this.axisTransform);
			
			this.localToCameraTransform.appendPosition(this.x, this.y, this.z);
			this.localToCameraTransform.append(camera.getRootNode().localToCameraTransform);
			
			tempTransA.combine(camera.localToGlobalTransform, this.localToCameraTransform);
			tempx = tempTransA.d;
			tempy = tempTransA.h;
			tempz = tempTransA.l;
			
			if (this.frameCount>=2) {				
				
				this.preCameraPosX = tempx - this.preCameraPosX;
				this.preCameraPosY = tempy - this.preCameraPosY;
				this.preCameraPosZ = tempz - this.preCameraPosZ;
				
				var n:Number = 1.0/math_sqrt(this.preCameraPosX*this.preCameraPosX + 
					this.preCameraPosY*this.preCameraPosY +
					this.preCameraPosZ*this.preCameraPosZ);
				
				this.preCameraPosX*=n;
				this.preCameraPosY*=n;
				this.preCameraPosZ*=n;
				
				if (this.preCameraPosX <= -0.99) {
					rotateAxisTemp.x = 0;
					rotateAxisTemp.y =-1;
					rotateAxisTemp.z = 0;
				} else {
					rotateAxisTemp.x = 0;
					rotateAxisTemp.y = -this.preCameraPosZ;
					rotateAxisTemp.z =  this.preCameraPosY;
					n = 1.0/math_sqrt(rotateAxisTemp.y*rotateAxisTemp.y +
						rotateAxisTemp.z*rotateAxisTemp.z);
					rotateAxisTemp.y *= n;
					rotateAxisTemp.z *= n;					
				}
				
				this.axisTransform.fromAxisAngle(rotateAxisTemp.x, rotateAxisTemp.y, rotateAxisTemp.z, math_acos(this.preCameraPosX));
			} 
			
			if (this.frameCount < 4) {
				this.localToCameraTransform.compose(0,0,0,0,0,0,0,0,0);
			}
			
			this.preCameraPosX = tempx;
			this.preCameraPosY = tempy;
			this.preCameraPosZ = tempz;
			
			renderUnit.setVertexConstantsFromTransform(beginRegister, localToCameraTransform);
			beginRegister+=3;
			
			return beginRegister;
		}
		
		// DIR_TYPE_FLY_VELOCITY_CAMERA
		public function calcTransFlyVelocityCamera(unit:ParticleUnit, camera:Camera3D, renderUnit:RenderUnit, beginRegister:int):int
		{
			var prop:ParticleUnitProp = unit.unitProp as ParticleUnitProp;
			
			var trans:Transform3D = prop.completelyFollow ? unit.getFollowNode().localToCameraTransform : this.linkNodeTrans;
			
			var rScalex:Number;
			var rScaley:Number;
			var rScalez:Number;			
			var needIdentity:Boolean = true
			if (prop.followLinkNodeScale) {
				
				needIdentity = false;
				rScalex = math_sqrt(trans.a*trans.a + trans.e*trans.e + trans.i*trans.i);
				rScaley = math_sqrt(trans.b*trans.b + trans.f*trans.f + trans.j*trans.j);
				rScalez = math_sqrt(trans.c*trans.c + trans.g*trans.g + trans.k*trans.k);
				
				this.localToCameraTransform.composeScale(rScalex, rScaley, rScalez);
			}
			
			if(needIdentity) {
				this.localToCameraTransform.identity();
			}
			
			this.localToCameraTransform.appendScales(this.width, this.height, 1);
			
			transform.fromAxisAngle(prop.selfRotateAxis.x, prop.selfRotateAxis.y, 
				prop.selfRotateAxis.z, this.selfOriginRotate + rotateS);
			
			this.localToCameraTransform.append(transform);
			
			transform.compose(0, 0, 0, 
				this.rotationX, this.rotationY, this.rotationZ,
				1, 1, 1);
			
			this.localToCameraTransform.append(transform);
			
			this.localToCameraTransform.append(prop.normalTrans);
			
			this.localToCameraTransform.append(this.axisTransform);
			
			this.localToCameraTransform.appendPosition(this.x, this.y, this.z);
			this.localToCameraTransform.append(camera.getRootNode().localToCameraTransform);
			
			tempTransA.combine(camera.localToGlobalTransform, this.localToCameraTransform);
			tempx = tempTransA.d;
			tempy = tempTransA.h;
			tempz = tempTransA.l;
			
			if (this.frameCount>=2) {
				
				if (this.axisTransform==null) {
					this.axisTransform = new Transform3D;
				}
				
				this.preCameraPosX = tempx - this.preCameraPosX;
				this.preCameraPosY = tempy - this.preCameraPosY;
				this.preCameraPosZ = tempz - this.preCameraPosZ;
				
				var n:Number = 1.0/math_sqrt(this.preCameraPosX*this.preCameraPosX + 
					this.preCameraPosY*this.preCameraPosY +
					this.preCameraPosZ*this.preCameraPosZ);
				
				this.preCameraPosX*=n;
				this.preCameraPosY*=n;
				this.preCameraPosZ*=n;
				
				if (this.preCameraPosX <= -0.99) {
					rotateAxisTemp.x = 0;
					rotateAxisTemp.y =-1;
					rotateAxisTemp.z = 0;
				} else {
					rotateAxisTemp.x = 0;
					rotateAxisTemp.y = -this.preCameraPosZ;
					rotateAxisTemp.z =  this.preCameraPosY;
					n = 1.0/math_sqrt(rotateAxisTemp.y*rotateAxisTemp.y +
						rotateAxisTemp.z*rotateAxisTemp.z);
					rotateAxisTemp.y *= n;
					rotateAxisTemp.z *= n;					
				}
				
				this.axisTransform.fromAxisAngle(rotateAxisTemp.x, rotateAxisTemp.y, rotateAxisTemp.z, math_acos(this.preCameraPosX));
				
				n =  1.0 / math_sqrt(this.axisTransform.a*this.axisTransform.a + 
					this.axisTransform.e*this.axisTransform.e +
					this.axisTransform.i*this.axisTransform.i);
				var dirXx:Number = this.axisTransform.a * n;
				var dirXy:Number = this.axisTransform.e * n;
				var dirXz:Number = this.axisTransform.i * n;
				
				var dirTempX:Number = camera.localToGlobalTransform.d - tempx;
				var dirTempY:Number = camera.localToGlobalTransform.h - tempy;
				var dirTempZ:Number = camera.localToGlobalTransform.l - tempz;
				n = 1.0 / math_sqrt(dirTempX*dirTempX +	dirTempY*dirTempY +	dirTempZ*dirTempZ);				
				dirTempX*=n;
				dirTempY*=n;
				dirTempZ*=n;
				
				var dirYx:Number = dirXy * dirTempZ - dirXz* dirTempY;
				var dirYy:Number = dirXz * dirTempX - dirXx* dirTempZ;
				var dirYz:Number = dirXx * dirTempY - dirXy* dirTempX;
				n = 1.0 / math_sqrt(dirYx*dirYx + dirYy*dirYy +	dirYz*dirYz);
				dirYx*=n;
				dirYy*=n;
				dirYz*=n;
				
				var dirZx:Number = dirYy * dirXz - dirYz* dirXy;
				var dirZy:Number = dirYz * dirXx - dirYx* dirXz;
				var dirZz:Number = dirYx * dirXy - dirYy* dirXx;
				n = 1.0 / math_sqrt(dirZx*dirZx + dirZy*dirZy +	dirZz*dirZz);
				dirZx*=n;
				dirZy*=n;
				dirZz*=n;
				
				var ts:Transform3D = this.axisTransform;		
				
				ts.a = dirXx; ts.b = dirYx; ts.c = dirZx;
				ts.e = dirXy; ts.f = dirYy; ts.g = dirZy;
				ts.i = dirXz; ts.j = dirYz; ts.k = dirZz;
			} 
			
			if (this.frameCount < 4) {
				this.localToCameraTransform.compose(0,0,0,0,0,0,0,0,0);
			}
			this.preCameraPosX = tempx;
			this.preCameraPosY = tempy;
			this.preCameraPosZ = tempz;
			renderUnit.setVertexConstantsFromTransform(beginRegister, localToCameraTransform);
			beginRegister+=3;
			
			return beginRegister;
		}
		
		public function calcTrans(unit:ParticleUnit, camera:Camera3D, renderUnit:RenderUnit, beginRegister:int):int
		{
			var prop:ParticleUnitProp = unit.unitProp as ParticleUnitProp;
			if (prop.faceType == UnitBaseProp.DIR_TYPE_PARENT) {
				
				return this.calcTransParent(unit, camera, renderUnit, beginRegister);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_WORLD) {
				
				return this.calcTransWorld(unit, camera, renderUnit, beginRegister);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_CAMERA) {
				
				return this.calcTransCamera(unit, camera, renderUnit, beginRegister);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_EMIT_VELOCITY) {
				
				return this.calcTransEmitVelocity(unit, camera, renderUnit, beginRegister);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_FLY_VELOCITY) {
				
				return this.calcTransFlyVelocity(unit, camera, renderUnit, beginRegister);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_FLY_VELOCITY_CAMERA) {
				
				return this.calcTransFlyVelocityCamera(unit, camera, renderUnit, beginRegister);
				
			}
			
			Global.throwError("calcTrans invalid faceType="+prop.faceType);
			return beginRegister;
		}
	}
}
