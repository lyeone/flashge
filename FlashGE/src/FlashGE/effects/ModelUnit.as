/*@author lye 123*/

package FlashGE.effects
{
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.ModelUnitProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.materials.Material;
	import FlashGE.objects.Entity;
	import FlashGE.objects.Model;
	import FlashGE.objects.RenderObject;
	import FlashGE.render.Camera3D;

	use namespace FlashGE;
	
	public class ModelUnit extends EffectUnitBase
	{		
		static private var offsetKey:KeyPair;
		static private var scaleKey:KeyPair;
		static private var colorKey:KeyPair;
		static private var rotateKey:KeyPair;
		
		private var renderObject:RenderObject;
		private var needSyncParams:Boolean;
		private var framesCount:int;
		private var startPlayTimer:int = -1;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_MODEL;
		}
		
		static public function initStaticContext():void
		{
			ModelUnit.offsetKey = new KeyPair;
			ModelUnit.offsetKey.init(0);
			
			ModelUnit.scaleKey = new KeyPair;
			ModelUnit.scaleKey.init(0);
			
			ModelUnit.colorKey = new KeyPair;
			ModelUnit.colorKey.init(0);
			
			ModelUnit.rotateKey = new KeyPair;
			ModelUnit.rotateKey.init(0);
		}
		
		override public function contructor(unitProp:UnitBaseProp = null):void
		{
			super.contructor(unitProp);
			
			this.needSyncParams = true;
			this.framesCount = 0;
			
			if (unitProp==null) {
				var newProp:ModelUnitProp = new ModelUnitProp;
				newProp.contructor();
				this.setProp(newProp);
			} else {
				this.setProp(unitProp);
			}
			
		}
		
		private function getRenderObject():RenderObject
		{
			if (this.renderObject==null) {
				this.renderObject = new RenderObject();
			}
			return this.renderObject;
		}
		
		override public function dispose():void
		{
			super.dispose();
			if (this.renderObject!=null) {
				this.renderObject.dispose();
				this.renderObject = null;
			}
		}
		
		public function get aniLoop():Boolean
		{
			return (unitProp as ModelUnitProp).aniLoop;
		}
		
		public function set aniLoop(loop:Boolean):void
		{
			var prop:ModelUnitProp = unitProp as ModelUnitProp;
			prop.aniLoop = loop;	
			getRenderObject().aniLoop = loop;
		}
		
		public function setModelUrl(url:String):void
		{
			getRenderObject().addPiece(url);
			(this.unitProp as ModelUnitProp).modelName = url;
		}
		
		public function getModelUrl():String
		{
			return (this.unitProp as ModelUnitProp).modelName;
		}
		
		public function setModelAni(url:String):void
		{
			(this.unitProp as ModelUnitProp).aniName = url;
			var aniName:String = url.split("/").pop();
			aniName = aniName.substr(0, aniName.length - 4);
			getRenderObject().playAni(aniName, (unitProp as ModelUnitProp).aniLoop);
		}
		
		public function getModelAni():String
		{
			return (this.unitProp as ModelUnitProp).aniName;
		}
			
		override FlashGE function updateOneFrame(camera:Camera3D):void
		{
			if (!this.isValid) {
				return;
			}
			
			if (!isShow()) {
				return;
			}
			
			var isEditor:Boolean = CONFIG::EDITOR;
			var percent:Number = getPercent();
			if (percent < 0 || percent > 1) {
				if (system.loop == false) {
					this.startPlayTimer = -1;
				}
				if (isEditor) {
					this.startPlayTimer = -1;
				}
				return;
			}

			var renderObj:RenderObject = this.getRenderObject();
			var model:Model = renderObj.getModel();
			if (model == null || !model.isLoaded()) {
				return;
			}
			
			if (this.startPlayTimer == -1) {
				this.startPlayTimer = getTimer();
			}
			if (needSyncParams == true) {
				needSyncParams = false;
				this.syncParamsToMaterial();
			}
			
			var prop:ModelUnitProp = unitProp as ModelUnitProp;
			
			this.getInterpolationValue(percent, prop.offsetKeys, offsetKey);
			this.getInterpolationValue(percent, prop.rotateKeys, rotateKey);
			this.getInterpolationValue(percent, prop.scaleKeys, scaleKey);
			this.getInterpolationValue(percent, prop.colorKeys, colorKey);
			this.setScale(scaleKey.items[0], scaleKey.items[1], scaleKey.items[2]);
			this.setRotation(rotateKey.items[0], rotateKey.items[1], rotateKey.items[2]);
			this.setPosition(offsetKey.items[0], offsetKey.items[1], offsetKey.items[2]);
			
			var curUnitFrame:Number = system.getCurFrame() - this.getBeginFrame();
				
			calcUV(curUnitFrame);
			
			for each(var subMesh:Entity in model.subMeshList) {
				
				var mat:Material = subMesh.getMaterial();
				if (mat != null) {
					mat.setColor(colorKey.items[0], colorKey.items[1], colorKey.items[2], colorKey.items[3]);
					mat.setUVContext(this.uvCurUOffset, this.uvCurVOffset, this.uvCurUScale, this.uvCurVScale, this.uvCurV0, this.uvCurV1);
					mat.setType(Defines.MATERIAL_EFFECT);
					mat.setRenderPriority(Defines.PRIORITY_EFFECT);
					mat.setOpenDoubleAlpha(prop.updateZ==1);
					mat.setStartPlayTimer(this.startPlayTimer);
				}
			}
			
			if (transformChanged == true) {
				composeTransforms();
				renderObj.transform.copy(this.transform);
				renderObj.inverseTransform.copy(this.inverseTransform);
				this.localToCameraTransform.combine(this.getFollowNode().localToCameraTransform, this.transform);
			}

			renderObj.gotoAniFrame(curUnitFrame);
			renderObj.cameraToLocalTransform.combine(renderObj.inverseTransform, this.getFollowNode().cameraToLocalTransform);
			renderObj.localToCameraTransform.combine(this.getFollowNode().localToCameraTransform, renderObj.transform);
			
			renderObj.calculateVisibility(camera);
			renderObj.calculateChildrenVisibility(camera);
			renderObj.collectRenderUnits(camera, false);
			renderObj.collectChildrenRenderUnits(camera, false);
			if (system.curFrame == 0) {
				this.framesCount = 0;
			}
			
			if (percent == 1 && system.loop == false) {
				this.startPlayTimer = -1;
			}
			
			if (isEditor) {
				if (percent == 1) {
					this.startPlayTimer = -1;
				}
			}
		}
		
		override public function clone():Entity
		{
			var unit:ModelUnit = new ModelUnit;
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
	
		override public function setProp(unitProp:UnitBaseProp):void
		{
			this.unitProp = unitProp;
			this.needSyncParams = true;
			var prop:ModelUnitProp = unitProp as ModelUnitProp;
			this.getRenderObject().addPiece(prop.modelName);
			var aniName:String = prop.aniName.split("/").pop();
			aniName = aniName.substr(0, aniName.length - 4);
			this.getRenderObject().playAni(aniName, this.aniLoop);
			this.getRenderObject().ignoreSkeleton = true;
		}
		
		override public function syncParamsToMaterial():void
		{
			if (this.renderObject == null) {
				return;
			}
			this.framesCount = 0;
			var prop:ModelUnitProp = unitProp as ModelUnitProp;
			var texResA:String;
			var texResB:String;
			var n:int;
			var mat:Material;
			var curMeshName:String;
			var model:Model = this.getRenderObject().getModel();
			if (model == null) return;
				
			for each(var subMesh:Entity in model.subMeshList) {
				
				mat = subMesh.getMaterial();
				if (mat == null) continue;
				
				curMeshName = subMesh.getName();
				
				texResA = null;
				texResB = null;
				for (n = 0; n < ModelUnitProp.SKIN_NUM; n++) {
					if (curMeshName == prop.skinNames[n]) {
						texResA = prop.skinTexsA[n];
						texResB = prop.skinTexsB[n];
						break;
					}
				}
								
				var i:int = 0
				if (texResA!=null) {
					mat.setTextureByFileName(i++, texResA);
				}
				
				if (texResB!=null) {
					mat.setTextureByFileName(i, texResB);
				}
				
				mat.setCloseUVTrans(false);
				mat.setUseUVType(prop.uvType);
				mat.setUV2UOffset(prop.uv2UOffset);
				mat.setUV2VOffset(prop.uv2VOffset);
				mat.setUVAngle(prop.uvAngle);
				mat.setDstBlend(prop.blendDstType);
				mat.setSrcBlend(prop.blendSrcType);
				mat.setRenderPriority(prop.renderPriority);
				mat.setTexOpIndex(prop.texOpIndex);
				mat.setRepeatIndex(prop.repeatIndex);
				mat.setMipIndex(prop.mipIndex);
				mat.setFilterIndex(prop.filterIndex);
				mat.setAlphaTest(prop.alphaTest);
				mat.setCulling(Defines.CULLING_TYPE[prop.cullingType]);
				mat.setAlphaIntensity(prop.alphaIntensity);
				mat.setAlphaSharpness(prop.alphaSharpness);
				mat.setOpenDoubleAlpha(false);
				mat.setUseVertDiffuse(false);
				mat.setType(Defines.MATERIAL_MODEL);
			}
		}
	}
}