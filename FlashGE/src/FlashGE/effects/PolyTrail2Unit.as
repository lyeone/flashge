/*@author lye 123*/

package FlashGE.effects
{
	import FlashGE.FlashGE;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.Polytrail2UnitProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.materials.Material;
	import FlashGE.objects.Entity;
	import FlashGE.render.Camera3D;
	
	use namespace FlashGE;
	
	public class PolyTrail2Unit extends EffectUnitBase
	{
		private var m_PolyTrail:PolyTrail2;
		private var offsetKey:KeyPair;
		private var colorKey:KeyPair;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_POLYTRAIL2;
		}
				
		override public function contructor(unitProp:UnitBaseProp = null):void
		{
			super.contructor(unitProp);
			
			this.m_PolyTrail = null;
			this.offsetKey = new KeyPair;
			this.offsetKey.init(0);
						
			this.colorKey = new KeyPair;
			this.colorKey.init(0);
						
			if (unitProp == null) {
				var newProp:Polytrail2UnitProp = new Polytrail2UnitProp;
				newProp.contructor();
				this.setProp(newProp);
			} else {
				this.setProp(unitProp);
			}
		}
		
		override public function dispose():void
		{
			if (m_PolyTrail != null) {
				m_PolyTrail.dispose();
				m_PolyTrail = null;
			}
			super.dispose();
		}
		
		override FlashGE function updateOneFrame(camera:Camera3D):void
		{
			var curSysFrame:Number = system.getCurFrame();
			if (!isShow() || this.getFollowNode() ==null ) return;
			
			var percent:Number = getPercent();
			if (percent < 0 || percent > 1) {				
				return;
			}
			
			
			if(m_PolyTrail == null)
			{
				m_PolyTrail = new PolyTrail2();
				m_PolyTrail.contructor();
				m_PolyTrail.setMaterial(this.material);
			}
			
			var prop:Polytrail2UnitProp = this.unitProp as Polytrail2UnitProp;
			var curUnitFrame:Number = percent * this.getLifeFramesNum();
			
			if (m_PolyTrail.draw(this, camera, percent) == false) return;
					
			this.getInterpolationValue(percent, prop.offsetKeys, offsetKey);
			//this.getInterpolationValue(percent, prop.rotateKeys, rotateKey);
			//this.getInterpolationValue(percent, prop.scaleKeys, scaleKey);
			this.getInterpolationValue(percent, prop.colorKeys, colorKey);
			
			calcUV(curUnitFrame);
			
			var mat:Material = this.material;
			mat.setColor(colorKey.items[0], colorKey.items[1], colorKey.items[2], colorKey.items[3]);
			mat.setUVContext(this.uvCurUOffset, this.uvCurVOffset, this.uvCurUScale, this.uvCurVScale, this.uvCurV0, this.uvCurV1);
			//mat.setCurTime(curUnitFrame);
			mat.setOpenDoubleAlpha(prop.updateZ==1);
			
			var transform:Transform3D = camera.pool.popTransform3D();
			transform.compose(offsetKey.items[0], offsetKey.items[1], offsetKey.items[2], 
				0,0,0, 1, 1, 1);
			m_PolyTrail.localToCameraTransform.combine(camera.globalToLocalTransform, transform);			
			camera.pool.pushTransform3D(transform);
			
			m_PolyTrail.updateOneFrame(this, camera, false);
		}
		
		override public function clone():Entity
		{
			var unit:PolyTrail2Unit = new PolyTrail2Unit;
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function syncParamsToMaterial():void
		{
			super.syncParamsToMaterial();
			this.material.setUseVertDiffuse(true);
		}
	}
}