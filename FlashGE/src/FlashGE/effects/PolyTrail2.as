/*@author lye 123*/

package FlashGE.effects
{
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.common.Pos3D;
	import FlashGE.common.Transform3D;
	import FlashGE.effects.unitProp.Polytrail2UnitProp;
	import FlashGE.objects.Bone;
	import FlashGE.objects.Entity;
	import FlashGE.objects.Mesh;
	import FlashGE.objects.Surface;
	import FlashGE.objects.VertexBuffer;
	import FlashGE.render.Camera3D;
	import FlashGE.resources.Geometry;
	
	use namespace FlashGE;
	
	public class PolyTrail2 extends Mesh
	{
		static private var dataPool:VertexDataPool;
		static private var vertListCache1:Vector.<Pos3D> = new Vector.<Pos3D>;
		static private var vertListCache2:Vector.<Pos3D> = new Vector.<Pos3D>;
		static private var vertListCacheLength:int = 0;
		
		static private var uvBuffer:Vector.<Number> = new Vector.<Number>;
		static private var uvBufferLength:int = 0;
		
		static private var indexBuffer:Vector.<uint> = new Vector.<uint>;
		static private var indexBufferLength:int = 0;
		
		static private var vertStreamList:ByteArray;
		static private var vertStreamListLength:int = 0;
		
		private var preNumIndices:int;
		private var curPoolPtr:VertexDataPool;
		private var m_Surface:Surface;
		private var frameDataListHeader:PolyTrailVertexData;
		private var frameDataListTail:PolyTrailVertexData;
		private var frameDataListLength:int;
		
		private var m_nPreTime:Number;
		private var m_nLastPushTime:Number;
		private var m_vLastPos:Pos3D;
		private var m_fLastPercent:Number;
		
		private var colorkey:KeyPair;
		private var widthkey:KeyPair;
		private var getCurTimer:Function;
		
		private var curFrameDirDelta:Pos3D;
		private var curFramePos:Pos3D;
		private var curFrameTrans:Transform3D;
		private var curFrameOutPos:Pos3D;
		
		public function contructor():void
		{
			this.preNumIndices = -1;
			this.curPoolPtr = null;
			this.frameDataListLength = 0;
			
			this.m_nPreTime = 0;
			this.m_nLastPushTime = 0;
			this.m_vLastPos = new Pos3D();
			this.m_fLastPercent = 0.0;
			
			this.colorkey = new KeyPair;
			this.colorkey.init(0);
			
			this.widthkey = new KeyPair;
			this.widthkey.init(0);
			
			this.getCurTimer = getTimer;
			
			this.curFrameDirDelta = new Pos3D;
			this.curFramePos = new Pos3D;
			this.curFrameTrans = new Transform3D;
			this.curFrameOutPos = new Pos3D;
			
			var geometry:Geometry = Geometry.create();
			var attributes:Array = new Array();
			attributes[0] = Defines.POSITION;
			attributes[1] = Defines.POSITION;
			attributes[2] = Defines.POSITION;
			attributes[3] = Defines.TEXCOORDS[0];
			attributes[4] = Defines.TEXCOORDS[0];
			attributes[5] = Defines.DIFFUSE;
			attributes[6] = Defines.DIFFUSE;
			attributes[7] = Defines.DIFFUSE;
			attributes[8] = Defines.DIFFUSE;
			geometry.addVertexStream(attributes);
			this.setGeometry(geometry);
			geometry.release();
			
			m_Surface = addSurface(0, 1);
			curPoolPtr = PolyTrail2.dataPool;
			if (curPoolPtr == null) {
				curPoolPtr = PolyTrail2.dataPool = new VertexDataPool;
			}
		}

		public function draw(unit:PolyTrail2Unit, camera:Camera3D, percent:Number):Boolean
		{
			var prop:Polytrail2UnitProp = unit.unitProp as Polytrail2UnitProp;
			var curtime:Number = getCurTimer();
			var curData:PolyTrailVertexData;
			var nextData:PolyTrailVertexData;
			if(m_fLastPercent >= percent) {
				curData = this.frameDataListHeader;
				while(curData!=null) {
					nextData = curData.next;
					curPoolPtr.push(curData);
					curData = nextData;
				}
				this.frameDataListTail = null;
				this.frameDataListHeader = null;
				frameDataListLength = 0;
			}
			m_fLastPercent = percent;
			
			/** 清理过时路径 **/			
			while(frameDataListLength > 0) {
				curData = this.frameDataListHeader;
				if(curData.istemp || curtime - curData.time > prop.trailLife) {
					nextData = this.frameDataListHeader.next;
					curPoolPtr.push(this.frameDataListHeader);
					this.frameDataListHeader = nextData;
					frameDataListLength--
				} else {
					break;
				}
			}
			
			if (frameDataListLength==0) {
				this.frameDataListHeader = this.frameDataListTail = null;
			}
			/** 获取挂接点位置 **/
			curFrameDirDelta.setTo(prop.trailRotateX, prop.trailRotateY, prop.trailRotateZ);
			var systemLinkNode:Entity = unit.system.getLinkedHolder();
			var linknode:Entity = unit.system.getUnitByName(unit.getFollowUnitName());
			var bonenode:Entity = systemLinkNode ? systemLinkNode.getNodeByName(prop.trailLinkName) : null;
			
			if(linknode != null) {
				curFrameTrans.combine(camera.localToGlobalTransform, linknode.localToCameraTransform);
				curFrameTrans.decomposePos3D(curFramePos, null, curFrameTrans);
				curFrameTrans.transPos3D(curFrameDirDelta, curFrameDirDelta);
			} else if(bonenode != null) {
				var joint:Bone = bonenode as Bone;
				if (joint !=null) {
					//curtrans.combine(systemLinkNode.localToCameraTransform, joint.curKeyFrame.boneTransform);
					curFrameTrans.copy(joint.localToCameraTransform);
				} else {
					curFrameTrans.combine(systemLinkNode.localToCameraTransform, bonenode.transform);
				}
				curFrameTrans.append(camera.localToGlobalTransform);
				curFrameTrans.decomposePos3D(curFramePos, null, curFrameTrans);
				curFrameTrans.transPos3D(curFrameDirDelta, curFrameDirDelta);
			}
			
			/** 获取旋转信息 **/
			if(prop.trailFaceTo == Polytrail2UnitProp.FACETO_NORMAL) {
				
			} else if(prop.trailFaceTo == Polytrail2UnitProp.FACETO_CAMERA) {
				camera.localToGlobal(curFrameDirDelta);
			}
			
			curFrameDirDelta.normalize(1);
			
			if( (curFramePos.x != m_vLastPos.x || curFramePos.y != m_vLastPos.y || curFramePos.z != m_vLastPos.z))
			{	
				var newData:PolyTrailVertexData = curPoolPtr.pop();
				if (newData.pos == null) {
					newData.pos = curFramePos.clone();
				} else {
					newData.pos.copy(curFramePos);
				}
				newData.time = curtime;
				if (newData.dirdelta == null) {
					newData.dirdelta = curFrameDirDelta.clone();
				} else {
					newData.dirdelta.copy(curFrameDirDelta);
				}
				newData.istemp = false;
				newData.next = null;
				if(curtime - m_nLastPushTime >= prop.trailStepTime)
				{
					m_nLastPushTime = curtime;
					m_vLastPos.copy(curFramePos);
				} else {
					newData.istemp = true;
				}
				
				if (frameDataListTail==null) {
					frameDataListHeader = newData;
					frameDataListTail = newData;
				} else {
					frameDataListTail.next = newData;
					frameDataListTail = newData;
				}
				frameDataListLength++;
			}
			return setupContext(unit, camera);
		}
		
		override FlashGE function collectRenderUnits(camera:Camera3D, isShadowPass:Boolean):int 
		{
			return 0;	
		}
		
		FlashGE function updateOneFrame(unit:PolyTrail2Unit, camera:Camera3D, isShadowPass:Boolean):int 
		{
			if (this.isValid == false) {
				return 0;
			}
			
			var renderCount:int;
			CONFIG::CACHE_PROGRAM_KEY {
				if (unit.unitProp.programCachedID != null && this.material.programCachedID == null) {
					this.material.programCachedID = unit.unitProp.programCachedID;
					this.m_Surface.materialKey = unit.unitProp.materialKey;
					this.programKey = unit.unitProp.programKey;
				}
			}
			renderCount = this.getMaterial().collectDraws(camera, this.m_Surface, geometry);
			CONFIG::CACHE_PROGRAM_KEY {
				if (renderCount > 0) {
					if (unit.unitProp.programCachedID == null) {
						unit.unitProp.programCachedID = this.material.programCachedID;
						unit.unitProp.materialKey = this.m_Surface.materialKey;
						unit.unitProp.programKey = this.programKey;
					}
				}
			}
			return renderCount;
		}
		
		public function setupContext(unit:PolyTrail2Unit, camera:Camera3D):Boolean
		{
			if (frameDataListLength < 2) {
				return false;
			}
			
			var curtime:Number = getCurTimer();
			var curtraillift:Number = curtime - frameDataListHeader.time;
			
			var i:int;
			var prop:Polytrail2UnitProp = unit.unitProp as Polytrail2UnitProp;
			
			var d0:PolyTrailVertexData, d1:PolyTrailVertexData;
			var dir:Pos3D;
			var percent:Number = 0;
			
			var dataListLength:int = frameDataListLength; 
			var vertList1:Vector.<Pos3D> = PolyTrail2.vertListCache1;
			var vertList2:Vector.<Pos3D> = PolyTrail2.vertListCache2;
			var vertListLengthTemp:int = PolyTrail2.vertListCacheLength;
			var vertIndex:int = 0;
			var curDataPtr:PolyTrailVertexData = frameDataListHeader;
			for(i = 0; i < dataListLength; i++) {
				d0 = curDataPtr;
				d1 = curDataPtr.next;
				curDataPtr = curDataPtr.next;
				
				percent = (curtime - d0.time) / curtraillift;
				if (percent < 0) percent = 0;
				if (percent > 1) percent = 1;
				this.getInterpolationValue(percent, prop.trailWidthKeys, widthkey);
				
				if(d1 != null) {
					dir = d1.pos.subtract(d0.pos, dir);
					dir.crossProduct(d0.dirdelta, dir);
					dir.normalize(widthkey.items[0]);
				}
				
				if(prop.trailWidthExtend == Polytrail2UnitProp.WIDTHEXTEND_ALL) {
					//vertex1_list.push( d0.pos.subtract(dir) );
					//vertex2_list.push( d0.pos.add(dir) );
					if (vertIndex >= vertListLengthTemp) {
						vertList1[vertIndex] = d0.pos.subtract(dir, null);
						vertList2[vertIndex] = d0.pos.add(dir, null);
						vertListLengthTemp++;
					} else {
						d0.pos.subtract(dir, vertList1[vertIndex]);	
						d0.pos.add(dir, vertList2[vertIndex]);
					}
					
					vertIndex++;
				} else if(prop.trailWidthExtend == Polytrail2UnitProp.WIDTHEXTEND_LEFT) {
					//vertex1_list.push( d0.pos.subtract(dir) );
					//vertex2_list.push( d0.pos );
					if (vertIndex >= vertListLengthTemp) {
						vertList1[vertIndex] = d0.pos.subtract(dir, null);
						vertList2[vertIndex] = d0.pos.clone();
						vertListLengthTemp++;
					} else {
						d0.pos.subtract(dir, vertList1[vertIndex]);	
						vertList2[vertIndex].copy(d0.pos);
					}
					vertIndex++;
				} else if(prop.trailWidthExtend == Polytrail2UnitProp.WIDTHEXTEND_RIGHT) {
					//vertex1_list.push( d0.pos );
					//vertex2_list.push( d0.pos.add(dir) );
					if (vertIndex >= vertListLengthTemp) {
						vertList1[vertIndex] = d0.pos.clone();
						vertList2[vertIndex] = d0.pos.add(dir, null);
						vertListLengthTemp++;
					} else {
						vertList1[vertIndex].copy(d0.pos);
						d0.pos.add(dir, vertList2[vertIndex]);
					}
					vertIndex++;
				}
				
				if(i == 0 || i == dataListLength - 1) {
					//vertex1_list.push(vertex1_list[vertex1_list.length - 1]);
					//vertex2_list.push(vertex2_list[vertex2_list.length - 1]);
					
					if (vertIndex >= vertListLengthTemp) {
						vertList1[vertIndex] = vertList1[vertIndex - 1].clone();
						vertList2[vertIndex] = vertList2[vertIndex - 1].clone();
						vertListLengthTemp++;
					} else {
						
						vertList1[vertIndex].copy(vertList1[vertIndex - 1]);
						vertList2[vertIndex].copy(vertList2[vertIndex - 1]);
					}
					vertIndex++;
				}
			} // end for
			
			PolyTrail2.vertListCacheLength = vertListLengthTemp;
			var t1:Number = 0, t2:Number = 0, t3:Number = 0, len:Number = 0, count:Number = 0, alphaf:Number = 0;
			var v0:Pos3D, v1:Pos3D, v2:Pos3D, v3:Pos3D;
			var color_r:Number = 0, color_g:Number = 0, color_b:Number = 0, color_a:Number = 0;			
			var v1t:Number = 0, v2t:Number = 0, v0t:Number = 0, v3t:Number = 0;
			
			var sum_length:Number = 0;
			var posLX:Number = 0, posLY:Number = 0, posLZ:Number = 0,
				posRX:Number = 0, posRY:Number = 0, posRZ:Number = 0;
			
			var vertex1ListLength:int = vertIndex;
			var vertexListLength:int;
			var uvListLength:int;
			
			var vertBufferLengthTemp:int = 0;
			
			var uvBufferLengthTemp:int = 0;
			var uvBufferTemp:Vector.<Number> = PolyTrail2.uvBuffer;
			
			var indexBufferLengthTemp:int = 0;
			var indexBufferTemp:Vector.<uint> = PolyTrail2.indexBuffer;
			
			var vertStreamListTemp:ByteArray = PolyTrail2.vertStreamList;
			var vertStreamListLengthTemp:int = 0;
			if (vertStreamListTemp == null) {
				vertStreamListTemp = PolyTrail2.vertStreamList = new ByteArray;
				vertStreamListTemp.endian = Endian.LITTLE_ENDIAN;
			}
			
			var numMappings:int = 9;//geometry.vertexStreamsList[0].attributes.length;
			var vertIndexCounter:int = 0;
			
			// 处理图片 uv 偏移
			var uoffset:Number = this.material.getUVUOffset();
			var voffset:Number = this.material.getUVVOffset();
			
			var tempX:Number = 0, tempY:Number = 0, tempZ:Number = 0;
			
			curDataPtr = frameDataListHeader;
			switch(prop.trailUvType)
			{
				case Polytrail2UnitProp.UVTYPE_DRAW:	// 贴图方式：拉伸
					
					for(i = 0; i < vertex1ListLength - 3; i++)
					{
						d0 = curDataPtr;
						d1 = curDataPtr.next;
						curDataPtr = curDataPtr.next;
						
						len = d1.pos.subtract(d0.pos, curFrameOutPos).length();
						t1 = 0.0;
						while( true )
						{
							t1 = t1 + 1 / prop.trailSmoothFactor;
							if (t1 > 1) t1 = 1;
							t2 = t1 * t1;
							t3 = t2 * t1;
							v1t = 2 - 5*t2 + 3 * t3;
							v2t = t1 + 4 * t2 - 3 * t3;
							v0t = 2*t2 - t1 -t3;
							v3t = t3 - t2;
							percent = ((curtime - d0.time) + (d0.time - d1.time) * t1) / curtraillift;
							if (percent < 0 ) percent = 0;
							
							this.getInterpolationValue(percent, prop.trailColorKeys, colorkey);
							color_r = colorkey.items[0];
							color_g = colorkey.items[1];
							color_b = colorkey.items[2];
							color_a = colorkey.items[3];
							
							v0 = vertList1[i + 0];
							v1 = vertList1[i + 1];
							v2 = vertList1[i + 2];
							v3 = vertList1[i + 3];
							//catmullVector3D(t, v0, v1, v2, v3, outPos);
							//return (0.5 *( (2 * v1.x) + (-v0.x + v2.x) * t1 +(2*v0.x - 5*v1.x + 4*v2.x - v3.x) * t2 +(-v0.x + 3*v1.x- 3*v2.x + v3.x) * t3));
							//catmullBase(t1, t2, t3, v0.x, v1.x, v2.x, v3.x);
														
							tempX = 0.5* (v0.x * v0t + v1.x * v1t + v2.x * v2t + v3.x * v3t);
							tempY = 0.5* (v0.y * v0t + v1.y * v1t + v2.y * v2t + v3.y * v3t);
							tempZ = 0.5* (v0.z * v0t + v1.z * v1t + v2.z * v2t + v3.z * v3t);
							
							//vpos_l = outPos;
							vertStreamListTemp.position = 4 * (numMappings * vertStreamListLengthTemp);
							//vertex_list.push(outPos.x, outPos.y, outPos.z);
							vertStreamListTemp.writeFloat(tempX);
							vertStreamListTemp.writeFloat(tempY);
							vertStreamListTemp.writeFloat(tempZ);
							//uv_list.push(sum_length + len * t, 0);
							vertStreamListTemp.position += 8;
							uvBufferTemp[uvBufferLengthTemp++] = sum_length + len * t1;
							uvBufferTemp[uvBufferLengthTemp++] = 0;
							
							//diffuse_list.push(color_r, color_g, color_b, color_a);
							vertStreamListTemp.writeFloat(color_r);
							vertStreamListTemp.writeFloat(color_g);
							vertStreamListTemp.writeFloat(color_b);
							vertStreamListTemp.writeFloat(color_a);
							v0 = vertList2[i + 0];
							v1 = vertList2[i + 1];
							v2 = vertList2[i + 2];
							v3 = vertList2[i + 3];
							//catmullVector3D(t, v0, v1, v2, v3, outPos);
							//tempX = catmullBase(t1, t2, t3, v0.x, v1.x, v2.x, v3.x);
							//tempY = catmullBase(t1, t2, t3, v0.y, v1.y, v2.y, v3.y);
							//tempZ = catmullBase(t1, t2, t3, v0.z, v1.z, v2.z, v3.z);v1t = 2 - 5*t2 + 3 * t3;
							
							tempX = 0.5* (v0.x * v0t + v1.x * v1t + v2.x * v2t + v3.x * v3t);
							tempY = 0.5* (v0.y * v0t + v1.y * v1t + v2.y * v2t + v3.y * v3t);
							tempZ = 0.5* (v0.z * v0t + v1.z * v1t + v2.z * v2t + v3.z * v3t);
							//vpos_r = outPos;
							//vertex_list.push(outPos.x, outPos.y, outPos.z);
							vertStreamListTemp.writeFloat(tempX);
							vertStreamListTemp.writeFloat(tempY);
							vertStreamListTemp.writeFloat(tempZ);
							//uv_list.push(sum_length + len * t, 1);
							vertStreamListTemp.position += 8;
							uvBufferTemp[uvBufferLengthTemp++] = sum_length + len * t1;
							uvBufferTemp[uvBufferLengthTemp++] = 1;
							//diffuse_list.push(color_r, color_g, color_b, color_a);
							vertStreamListTemp.writeFloat(color_r);
							vertStreamListTemp.writeFloat(color_g);
							vertStreamListTemp.writeFloat(color_b);
							vertStreamListTemp.writeFloat(color_a);
							vertStreamListLengthTemp+=2;
							vertBufferLengthTemp+=6;
							if( t1 >= 1.0 )
								break;
						}
						sum_length += len;
					}
					
					if(sum_length > 0) {
						uvListLength = uvBufferLengthTemp;
						for(i = 0, vertIndexCounter = 0; i < uvListLength; i += 2, vertIndexCounter++) {
							
							vertStreamListTemp.position = 4 * (numMappings * vertIndexCounter + 3);
							vertStreamListTemp.writeFloat(uvBufferTemp[i]/sum_length - uoffset);
							vertStreamListTemp.writeFloat(uvBufferTemp[i+1] - voffset);
						}
					}
					
					vertexListLength = vertBufferLengthTemp;
					indexBufferLengthTemp = 0;
					for(i = 0; i < vertexListLength / 6 - 1; i+=2) {
						//index_list.push(i + 0, i + 1, i + 2);
						indexBufferTemp[indexBufferLengthTemp++] = i + 0;
						indexBufferTemp[indexBufferLengthTemp++] = i + 1;
						indexBufferTemp[indexBufferLengthTemp++] = i + 2;
						
						//index_list.push(i + 2, i + 1, i + 3);
						indexBufferTemp[indexBufferLengthTemp++] = i + 2;
						indexBufferTemp[indexBufferLengthTemp++] = i + 1;
						indexBufferTemp[indexBufferLengthTemp++] = i + 3;
					}
					
					break;
				
				case Polytrail2UnitProp.UVTYPE_GRID:	// 贴图方式：格子
					
					for(i = 0; i < vertex1ListLength - 3; i++)
					{
						d0 = curDataPtr;
						d1 = curDataPtr.next;
						curDataPtr = curDataPtr.next;
						
						len = d1.pos.subtract(d0.pos, curFrameOutPos).length();
						
						t1 = 0.0; count = 0;
						sum_length = 0;
						while( true )
						{
							
							vertStreamListTemp.position = 4 * (numMappings * vertStreamListLengthTemp);
							//vertex_list.push(vpos_l.x, vpos_l.y, vpos_l.z);
							vertStreamListTemp.writeFloat(posLX);
							vertStreamListTemp.writeFloat(posLY);
							vertStreamListTemp.writeFloat(posLZ);
							//uv_list.push(sum_length + len * t, 0);
							vertStreamListTemp.position += 8;
							uvBufferTemp[uvBufferLengthTemp++] = sum_length + len * t1;
							uvBufferTemp[uvBufferLengthTemp++] = 0;
							//diffuse_list.push(color_r, color_g, color_b, color_a);
							vertStreamListTemp.writeFloat(color_r);
							vertStreamListTemp.writeFloat(color_g);
							vertStreamListTemp.writeFloat(color_b);
							vertStreamListTemp.writeFloat(color_a);
							
							//vertex_list.push(vpos_r.x, vpos_r.y, vpos_r.z);
							vertStreamListTemp.writeFloat(posRX);
							vertStreamListTemp.writeFloat(posRY);
							vertStreamListTemp.writeFloat(posRZ);
							//uv_list.push(sum_length + len * t, 1);
							vertStreamListTemp.position += 8;
							uvBufferTemp[uvBufferLengthTemp++] = sum_length + len * t1;
							uvBufferTemp[uvBufferLengthTemp++] = 1;
							//diffuse_list.push(color_r, color_g, color_b, color_a);
							vertStreamListTemp.writeFloat(color_r);
							vertStreamListTemp.writeFloat(color_g);
							vertStreamListTemp.writeFloat(color_b);
							vertStreamListTemp.writeFloat(color_a);
							
							t1 = t1 + 1 / prop.trailSmoothFactor;
							if (t1 > 1) t1 = 1;
							t2 = t1 * t1;
							t3 = t2 * t1;
							v1t = 2 - 5*t2 + 3 * t3;
							v2t = t1 + 4 * t2 - 3 * t3;
							v0t = 2*t2 - t1 -t3;
							v3t = t3 - t2;
							percent = ((curtime - d0.time) + (d0.time - d1.time) * t1) / curtraillift;
							if (percent < 0) percent = 0;
							
							this.getInterpolationValue(percent, prop.trailColorKeys, colorkey);
							color_r = colorkey.items[0];
							color_g = colorkey.items[1];
							color_b = colorkey.items[2];
							color_a = colorkey.items[3];
							
							v0 = vertList1[i + 0];
							v1 = vertList1[i + 1];
							v2 = vertList1[i + 2];
							v3 = vertList1[i + 3];
							//catmullVector3D(t, v0, v1, v2, v3, vpos_l);
							//posLX = catmullBase(t1, t2, t3, v0.x, v1.x, v2.x, v3.x);
							//posLY = catmullBase(t1, t2, t3, v0.y, v1.y, v2.y, v3.y);
							//posLZ = catmullBase(t1, t2, t3, v0.z, v1.z, v2.z, v3.z);
							posLX = 0.5* (v0.x * v0t + v1.x * v1t + v2.x * v2t + v3.x * v3t);
							posLY = 0.5* (v0.y * v0t + v1.y * v1t + v2.y * v2t + v3.y * v3t);
							posLZ = 0.5* (v0.z * v0t + v1.z * v1t + v2.z * v2t + v3.z * v3t);
							//vpos_l.setTo(vpos.x, vpos.y, vpos.z);
							//vertex_list.push(vpos_l.x, vpos_l.y, vpos_l.z);
							vertStreamListTemp.writeFloat(posLX);
							vertStreamListTemp.writeFloat(posLY);
							vertStreamListTemp.writeFloat(posLZ);
							//uv_list.push(sum_length + len * t, 0);
							vertStreamListTemp.position += 8;
							uvBufferTemp[uvBufferLengthTemp++] = sum_length + len * t1;
							uvBufferTemp[uvBufferLengthTemp++] = 0;
							//diffuse_list.push(color_r, color_g, color_b, color_a);
							vertStreamListTemp.writeFloat(color_r);
							vertStreamListTemp.writeFloat(color_g);
							vertStreamListTemp.writeFloat(color_b);
							vertStreamListTemp.writeFloat(color_a);
							
							v0 = vertList2[i + 0];
							v1 = vertList2[i + 1];
							v2 = vertList2[i + 2];
							v3 = vertList2[i + 3];
							//catmullVector3D(t, v0, v1, v2, v3, vpos_r);
							//posRX = catmullBase(t1, t2, t3, v0.x, v1.x, v2.x, v3.x);
							//posRY = catmullBase(t1, t2, t3, v0.y, v1.y, v2.y, v3.y);
							//posRZ = catmullBase(t1, t2, t3, v0.z, v1.z, v2.z, v3.z);
							posRX = 0.5* (v0.x * v0t + v1.x * v1t + v2.x * v2t + v3.x * v3t);
							posRY = 0.5* (v0.y * v0t + v1.y * v1t + v2.y * v2t + v3.y * v3t);
							posRZ = 0.5* (v0.z * v0t + v1.z * v1t + v2.z * v2t + v3.z * v3t);
							//vpos_r.setTo(vpos.x, vpos.y, vpos.z);
							//vertex_list.push(vpos_r.x, vpos_r.y, vpos_r.z);
							vertStreamListTemp.writeFloat(posRX);
							vertStreamListTemp.writeFloat(posRY);
							vertStreamListTemp.writeFloat(posRZ);
							//uv_list.push(sum_length + len * t, 1);
							vertStreamListTemp.position += 8;
							uvBufferTemp[uvBufferLengthTemp++] = sum_length + len * t1;
							uvBufferTemp[uvBufferLengthTemp++] = 1;
							//diffuse_list.push(color_r, color_g, color_b, color_a);
							vertStreamListTemp.writeFloat(color_r);
							vertStreamListTemp.writeFloat(color_g);
							vertStreamListTemp.writeFloat(color_b);
							vertStreamListTemp.writeFloat(color_a);
							
							vertStreamListLengthTemp+=4;
							vertBufferLengthTemp+=12;
							count += 8;
							if( t1 >= 1.0 )
								break;
						} // end while
						
						sum_length = len;
						if(sum_length > 0)
						{
							uvListLength = uvBufferLengthTemp; 
							
							for(var iTemp:int = uvListLength - count; iTemp < uvListLength; iTemp += 2) {
								vertStreamListTemp.position = 4 * (numMappings * iTemp/2 + 3);
								vertStreamListTemp.writeFloat(uvBufferTemp[i]/sum_length - uoffset);
								vertStreamListTemp.writeFloat(uvBufferTemp[i+1] - voffset);
								//uvBufferTemp[iTemp] /= sum_length;
							}
						}
					} // end for i
					
					vertexListLength = vertBufferLengthTemp;
					for(i = 0; i <  vertexListLength / 3 / 4; i++)
					{
						//index_list.push(i * 4 + 0, i * 4 + 1, i * 4 + 2);
						//index_list.push(i * 4 + 2, i * 4 + 1, i * 4 + 3);
						var baseI:int = i * 4;
						indexBufferTemp[indexBufferLengthTemp++] = baseI + 0;
						indexBufferTemp[indexBufferLengthTemp++] = baseI + 1;
						indexBufferTemp[indexBufferLengthTemp++] = baseI + 2;
						
						indexBufferTemp[indexBufferLengthTemp++] = baseI + 2;
						indexBufferTemp[indexBufferLengthTemp++] = baseI + 1;
						indexBufferTemp[indexBufferLengthTemp++] = baseI + 3;
					}
					
					break;
			}
			
			if (indexBufferLengthTemp == 0) {
				return false;
			}
			
			if (geometry.context3D == null || geometry.context3D != camera.context3D) {
				geometry.dispose3D();
				geometry.context3D = camera.context3D;
			}
			var vertBuffer:VertexBuffer;
			try {
				var numIndices:int = indexBufferLengthTemp;
				if (geometry.indexBuffer3D == null) {
					geometry.indexBuffer3D = camera.context3D.createIndexBuffer(numIndices);
					CONFIG::DEBUG {
						Global.indexBuffersCount++;
					}
				} else if(preNumIndices != numIndices) {
					geometry.indexBuffer3D.dispose();
					geometry.indexBuffer3D = camera.context3D.createIndexBuffer(numIndices);
				}
				geometry.indexBuffer3D.uploadFromVector(indexBufferTemp, 0, numIndices);
				var curVerticesCount:int = vertBufferLengthTemp / 3;
				vertBuffer = geometry.vertexStreamsList[0];
				if (vertBuffer.vertBuffer3D == null) {
					
					vertBuffer.vertBuffer3D = camera.context3D.createVertexBuffer(curVerticesCount, numMappings);
					CONFIG::DEBUG {
						Global.vertexBuffersCount++;
					}
				} else if (geometry.verticesCount != curVerticesCount) {
					vertBuffer.vertBuffer3D.dispose();
					vertBuffer.vertBuffer3D = camera.context3D.createVertexBuffer(curVerticesCount, numMappings);
				}
				
				vertBuffer.vertBuffer3D.uploadFromByteArray(vertStreamListTemp, 0, 0, curVerticesCount);
			
			} catch(e:Error) {
				
				if(geometry.indexBuffer3D != null) {
					CONFIG::DEBUG {
						Global.indexBuffersCount--;
					}
					geometry.indexBuffer3D.dispose();
					geometry.indexBuffer3D = null;
				}
				
				vertBuffer = geometry.vertexStreamsList[0]
				if(vertBuffer.vertBuffer3D != null) {
					CONFIG::DEBUG {
						Global.vertexBuffersCount--;
					}
					vertBuffer.vertBuffer3D.dispose();
					vertBuffer.vertBuffer3D = null;
				}
				
				if (e.errorID == 3672) {
					Global.needDisposeContext3d = true;
				}
				
				CONFIG::DEBUG {
					Global.warning("PolyTrail2::setupContext" + e.errorID + "," + e.message);
				}
				return false;
			}
			
			preNumIndices = numIndices;
			geometry.verticesCount = curVerticesCount;
			m_Surface.trianglesNum = numIndices / 3;
			m_Surface.forceResetUnit = true;
			if (geometry.verticesCount == 2) {
				return false;
			}
			return true;
		}
	}
}
import FlashGE.common.Pos3D;

internal class PolyTrailVertexData
{
	public var pos:Pos3D;
	public var time:Number = 0;
	public var dirdelta:Pos3D;
	public var istemp:Boolean;
	public var next:PolyTrailVertexData;
}

internal class VertexDataPool
{
	private var head:PolyTrailVertexData;
	public function push(data:PolyTrailVertexData):void
	{
		data.next = head;
		head = data;
	}
	
	public function pop():PolyTrailVertexData
	{
		var ret:PolyTrailVertexData;
		var next:PolyTrailVertexData;
		if (head != null) {
			next = head.next;
			ret = head;
			head = next;
		} else {
			ret = new PolyTrailVertexData;
		}
		return ret;
	}
}