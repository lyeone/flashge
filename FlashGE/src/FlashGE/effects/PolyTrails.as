/*@author lye 123*/
package FlashGE.effects
{
	import flash.geom.Vector3D;
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Transform3D;
	import FlashGE.effects.unitProp.PolytrailUnitProp;
	import FlashGE.objects.Entity;
	import FlashGE.objects.Mesh;
	import FlashGE.objects.Surface;
	import FlashGE.render.Camera3D;
	import FlashGE.resources.Geometry;
	
	use namespace FlashGE;

	public class PolyTrails extends Mesh
	{
		private var surface:Surface;
		private var prePosAx:int = int.MAX_VALUE;
		private var prePosAy:int = int.MAX_VALUE;
		private var prePosAz:int = int.MAX_VALUE;
		
		private var prePosBx:int = int.MAX_VALUE;
		private var prePosBy:int = int.MAX_VALUE;
		private var prePosBz:int = int.MAX_VALUE;
		
		private var slerpResult:Array = [];
		private var pointListA:Array = [];
		private var pointListB:Array = [];
		private var pointTick:uint = 0;
		
		private var preTime:Number = 0;
		private var manualPlay:Boolean = false;
		
		private var m_vDataList:Array = [];
		private var getCurTimer:Function = getTimer;
		
		static private const _attributes:Array = [
			Defines.POSITION,
			Defines.POSITION,
			Defines.POSITION,
			Defines.TEXCOORDS[0],
			Defines.TEXCOORDS[0],
			Defines.DIFFUSE,
			Defines.DIFFUSE,
			Defines.DIFFUSE,
			Defines.DIFFUSE
		];
		
		/******************* catmull *******************/
		static private var s_t2:Number;
		static private var s_t3:Number;
		static public function catmullBase(t:Number, p0:Number, p1:Number, p2:Number, p3:Number):Number
		{
			s_t2 = t * t;
			s_t3 = s_t2 * t;
			return (0.5 *( (2 * p1) + (-p0 + p2) * t +(2*p0 - 5*p1 + 4*p2 - p3) * s_t2 +(-p0 + 3*p1- 3*p2 + p3) * s_t3));
		}
		
		static private var s_x:Number;
		static private var s_y:Number;
		static private var s_z:Number;
		static public function catmullVector3D(
			t:Number, p0:Vector3D, p1:Vector3D, p2:Vector3D, p3:Vector3D, out_v:Vector3D):void
		{
			out_v.setTo(
				catmullBase(t, p0.x, p1.x, p2.x, p3.x),
				catmullBase(t, p0.y, p1.y, p2.y, p3.y),
				catmullBase(t, p0.z, p1.z, p2.z, p3.z));
		}
		/******************* catmull *******************/
		
		public function PolyTrails()
		{
			var geometry:Geometry = Geometry.create();
			geometry.addVertexStream(_attributes);
			this.setGeometry(geometry);
			geometry.release();
			surface = addSurface(0, 1);
		}
		
		private var curtrans1:Transform3D = new Transform3D;
		
		public function draw(unit:PolyTrailUnit, camera:Camera3D):void
		{
			var prop:PolytrailUnitProp = unit.unitProp as PolytrailUnitProp;
			var curtime:Number = getCurTimer();
			
			/** 清理过时路径 **/
			while(m_vDataList.length > 0)
			{
				if( curtime - (m_vDataList[0] as PolyTrailVertexData).time > prop.trailLife )
					m_vDataList.shift();
				else
					break;
			}
			
			/** 提取骨骼坐标 **/
			var bonenode1:Entity = unit.system.getNodeByName(prop.linkNameA);
			var bonenode2:Entity = unit.system.getNodeByName(prop.linkNameB);
			if(bonenode1 == null || bonenode2 == null)
				return;

			var polyVertexData:PolyTrailVertexData = new PolyTrailVertexData;
			polyVertexData.time = curtime;
			
			curtrans1.combine(bonenode1.getParent().localToCameraTransform, bonenode1.transform);
			curtrans1.append(camera.localToGlobalTransform);
			polyVertexData.pos1.setTo(curtrans1.d,curtrans1.h,curtrans1.l);
			
			curtrans1.combine(bonenode2.getParent().localToCameraTransform, bonenode2.transform);
			curtrans1.append(camera.localToGlobalTransform);
			polyVertexData.pos2.setTo(curtrans1.d, curtrans1.h, curtrans1.l);
			
			m_vDataList.push(polyVertexData);
			
			if(m_vDataList.length < 2)
				return;
			
			var vertex_list:Vector.<Number> = new Vector.<Number>;
			var diffuse_list:Vector.<Number> = new Vector.<Number>;
			var uv_list:Vector.<Number> = new Vector.<Number>;
			var index_list:Vector.<uint> = new Vector.<uint>;
			
			var i:int, t:Number, len:Number;
			var d0:PolyTrailVertexData, d1:PolyTrailVertexData, d2:PolyTrailVertexData, d3:PolyTrailVertexData;
			var datalistlength:Number = m_vDataList.length;
			var sum_length:Number = 0;
			var vpos:Vector3D = new Vector3D;
			for(i = 0; i < datalistlength - 2; i++)
			{
				d0 = m_vDataList[Math.max(0, i - 1)];
				d1 = m_vDataList[i];
				d2 = m_vDataList[i + 1];
				d3 = m_vDataList[Math.min(datalistlength, i + 2)];
				
				len = (d2.pos1.subtract(d1.pos1).length + d2.pos2.subtract(d1.pos2).length) / 2;
				t = 0.0;
				while( true )
				{
					t = Math.min( t + 1 / prop.smoothFactor, 1.0);
					
					catmullVector3D(t, d0.pos1, d1.pos1, d2.pos1, d3.pos1, vpos);
					vertex_list.push(vpos.x, vpos.y, vpos.z);
					uv_list.push(sum_length + len * t, 0);
					diffuse_list.push(1, 1, 1, 1);
					
					catmullVector3D(t, d0.pos2, d1.pos2, d2.pos2, d3.pos2, vpos);
					vertex_list.push(vpos.x, vpos.y, vpos.z);
					uv_list.push(sum_length + len * t, 1);
					diffuse_list.push(1, 1, 1, 1);
					
					if( t >= 1.0 )
						break;
				}
				sum_length += len;
			}
			
			if(sum_length > 0)
			{
				for(i = 0; i < uv_list.length; i += 2)
					uv_list[i] /= sum_length;
			}
			for(i = 0; i < vertex_list.length / 3 / 2 - 1; i++)
			{
				index_list.push(i * 2 + 0, i * 2 + 1, i * 2 + 2);
				index_list.push(i * 2 + 2, i * 2 + 1, i * 2 + 3);
			}
			
			// 处理图片 uv 偏移
			var uoffset:Number = this.material.getUVUOffset();
			var voffset:Number = this.material.getUVVOffset();
			for(i = 0; i < uv_list.length; i += 2)
			{
				uv_list[i] -= uoffset;
				uv_list[i + 1] -= voffset;
			}
			geometry.dispose3D();
			geometry.indicesBuffer = index_list;
			geometry.setVerticesCount(vertex_list.length / 3);
			geometry.setAttributeValues(Defines.POSITION, vertex_list);
			geometry.setAttributeValues(Defines.TEXCOORDS[0], uv_list);
			geometry.setAttributeValues(Defines.DIFFUSE, diffuse_list);
			surface.trianglesNum = this.geometry.getNumTriangles();
			surface.forceResetUnit = true;

		}
		//override FlashGE function collectRenderUnits(camera:Camera3D, isShadowPass:Boolean):int 
		//{
		//	return 0;	
		//}
		
		FlashGE function updateOneFrame(unit:PolyTrail2Unit, camera:Camera3D, isShadowPass:Boolean):int 
		{
			if (this.isValid == false) {
				return 0;
			}
			
			var renderCount:int;
			CONFIG::CACHE_PROGRAM_KEY {
				if (unit.unitProp.programCachedID != null && this.material.programCachedID == null) {
					this.material.programCachedID = unit.unitProp.programCachedID;
					this.surface.materialKey = unit.unitProp.materialKey;
					this.programKey = unit.unitProp.programKey;
				}
			}
			
			renderCount = this.getMaterial().collectDraws(camera, this.surface, geometry);
			CONFIG::CACHE_PROGRAM_KEY {
				if (renderCount > 0) {
					if (unit.unitProp.programCachedID == null) {
						unit.unitProp.programCachedID = this.material.programCachedID;
						unit.unitProp.materialKey = this.surface.materialKey;
						unit.unitProp.programKey = this.programKey;
					}
				}
			}
			
			return renderCount;
		}
		/*********************************************************/
		/**
		 * 下面的代码是原来的刀光算法
		 */
		/*********************************************************/
		
		public function getBindPos(unit:PolyTrailUnit, camera:Camera3D, bindName:String):Vector3D
		{
			var tempx:Number = 0, tempy:Number = 0, tempz:Number = 0,
				worldx:Number = 0, worldy:Number = 0, worldz:Number = 0;
			
			var linkedHolder:Entity = unit.system.getLinkedHolder()
			if (linkedHolder==null) return null;
			
			var child:Entity = linkedHolder.getNodeByName(bindName);
			if (child==null) return null;
			
			var transform:Transform3D = child.localToCameraTransform;
			worldx = transform.d;
			worldy = transform.h;
			worldz = transform.l;
			
			tempx = worldx;
			tempy = worldy;
			tempz = worldz;
			transform = camera.localToGlobalTransform;
			worldx = tempx*transform.a + tempy*transform.b + tempz*transform.c + transform.d;
			worldy = tempx*transform.e + tempy*transform.f + tempz*transform.g + transform.h;
			worldz = tempx*transform.i + tempy*transform.j + tempz*transform.k + transform.l;

			return new Vector3D(worldx, worldy, worldz);
		}
		
		public function draw2(unit:PolyTrailUnit, camera:Camera3D):Boolean
		{
			var prop:PolytrailUnitProp = unit.unitProp as PolytrailUnitProp
			//if (!unit.system.isPause() ) {
				var posA:Vector3D = getBindPos(unit, camera, prop.linkNameA);
				if (posA == null) return false;
				var posB:Vector3D = getBindPos(unit, camera, prop.linkNameB);
				if (posB == null) return false;
				var posAx:int = int(posA.x);
				var posAy:int = int(posA.y);
				var posAz:int = int(posA.z);
				var posBx:int = int(posB.x);
				var posBy:int = int(posB.y);
				var posBz:int = int(posB.z);
				if(this.prePosAx!=posAx || this.prePosAy!=posAy || this.prePosAz!=posAz ||
				   this.prePosBx!=posBx || this.prePosBy!=posBy || this.prePosBz!=posBz ) {
					addPoints(unit, posA, posB);
					this.prePosAx = posAx;
					this.prePosAy = posAy;
					this.prePosAz = posAz;
					this.prePosBx = posBx;
					this.prePosBy = posBy;
					this.prePosBz = posBz;
					manualPlay = true;
				}
			//}

			this.setupContext(unit, camera);
			manualPlay = false;
			return true;
		}
		
		public function addPoints(unit:PolyTrailUnit, v1:Vector3D, v2:Vector3D):void
		{
			var prop:PolytrailUnitProp = unit.unitProp as PolytrailUnitProp
			pointListA.push(v1);
			pointListB.push(v2);
			pointTick++;
			
			//if (pointTick==4) {
			if (pointListA.length>=2) {
				MakeSpline(unit);
				pointTick = 0;
			}
			//}
			
			if (pointListA.length > prop.length ) {
				pointListA.shift();
				pointListA.shift();
				pointListB.shift();
				pointListB.shift();			
			}
		}
		
		public function getTime(unit:PolyTrailUnit):Number
		{
			var oldTime:Number = preTime;
			if (unit.system.isPaused() && !manualPlay )
				return preTime;
			preTime = getCurTimer();
			return preTime;
		}
		
		private function setupContext(unit:PolyTrailUnit, camera:Camera3D):void
		{			
			var prop:PolytrailUnitProp = unit.unitProp as PolytrailUnitProp
			var vertices:Vector.<Number> = new Vector.<Number>;
			var vertDiffuse:Vector.<Number> = new Vector.<Number>;
			var uvs:Vector.<Number> = new Vector.<Number>;
			var indices:Vector.<uint> = new Vector.<uint>;       
			var i:int = 0;
			var j:int = 0;
			var startTime:Number;
			var curTime:Number = getTime(unit);
			for (i = 0; i < slerpResult.length; i++) {
				startTime = (slerpResult[i] as TrailPoint).startTime;
				if (curTime - startTime > prop.trailLife ) {					
					slerpResult.splice(i, 1);
					i--;
				}
			}
			
			
			var n:int = this.slerpResult.length;
			var dx:Number = 0;
			var useRepeat:Boolean = prop.texWidth != 0;
			
			var precx:Number, precy:Number, precz:Number;
			for (i = n-1; i >= 0; i--, j++) {
				var trail:TrailPoint = slerpResult[i];
				var t:Number = curTime - trail.startTime;
				var tt:Number = t*t;
				var percent:Number = t / prop.trailLife;
				var s:Number = trail.flyV*t + 0.5 * trail.flyA * tt;
				var x:Number = trail.p.x + s * trail.dir.x;
				var y:Number = trail.p.y + s * trail.dir.y;
				var z:Number = trail.p.z + s * trail.dir.z; 

				vertices.push(x, y, z);
				
				var a:Number = prop.minAlpha + (prop.maxAlpha - prop.minAlpha) *percent;
				vertDiffuse.push(1, 1, 1, a);
				if (!useRepeat) {
					uvs.push(Number(j) / Number(n) - prop.uvUOffset, j%2- prop.uvVOffset);
				} else {
					uvs.push(dx / prop.texWidth - prop.uvUOffset, j%2- prop.uvVOffset);
					if (j>0 && j%2==0){
						dx += Math.sqrt(Math.pow(precx - x, 2) + Math.pow(precy-y,2) + Math.pow(precz-z, 2));
						
						precx = x;
						precy = y;
						precz = z;
					} else if(j==0) {
						precx = x;
						precy = y;
						precz = z;
					}
				}
				if ( j < n-2) {
					if (j%2==0)
						indices.push(j, j+1, j+2);
					else
						indices.push(j, j+2, j+1);
				}
			}
			this.geometry.dispose3D();
			this.geometry.indicesBuffer = indices;
			this.geometry.setVerticesCount(vertices.length / 3);
			this.geometry.setAttributeValues(Defines.POSITION, vertices);
			this.geometry.setAttributeValues(Defines.TEXCOORDS[0], uvs);
			this.geometry.setAttributeValues(Defines.DIFFUSE, vertDiffuse);
			this.material.setUseVertDiffuse(true);
			surface.trianglesNum = this.geometry.getNumTriangles();
			surface.forceResetUnit = true;
		}
		
		public function MakeSpline(unit:PolyTrailUnit):void
		{
			var prop:PolytrailUnitProp = unit.unitProp as PolytrailUnitProp
			var pKey:Array;
			var trailA:TrailPoint;
			var trailB:TrailPoint;
			var a:int;
			var b:int;
			var c:int;
			var d:int;
			var nMaxVertex:int = pointListA.length;
			
			var curColNum:int = 0;
			//slerpResult = []
			for (var i:int =0; i < nMaxVertex-1; i++)
			{
				a = i - 1;  
				if( a < 0 ) 
					a = 0;
				
				b = i;
				c = i+1;
				d = i+2;    
				if(d >= nMaxVertex)   
					d = nMaxVertex - 1;
				
				for( var j:int = 0; j < prop.smoothFactor+1; j ++ )
				{
					pKey = pointListA;
					
					trailA = slerpResult[curColNum]
					if(trailA == null) {
						trailA = new TrailPoint;
						slerpResult[curColNum] = trailA;
						trailA.startTime = getTime(unit);
					}
					
					SplineSlerp(trailA.p, pKey[a], pKey[b], pKey[c], pKey[d], j / prop.smoothFactor);
					trailA.flyV = prop.flyV1;
					trailA.flyA = prop.flyA1;
					curColNum++;
					
					pKey = pointListB;
					trailB = slerpResult[curColNum]
					if(trailB == null) {
						trailB = new TrailPoint;
						slerpResult[curColNum] = trailB;
						trailB.startTime = getTime(unit);
					}
					SplineSlerp(trailB.p, pKey[a], pKey[b], pKey[c], pKey[d], j / prop.smoothFactor);
					trailB.flyV = prop.flyV2;
					trailB.flyA = prop.flyA2;
					curColNum++;
					
					var dir:Vector3D = trailA.p.subtract(trailB.p);
					dir.normalize();
					trailA.dir = dir;
					trailB.dir = new Vector3D(-dir.x, -dir.y, -dir.z);
				}
			}
		}
		
		private static function SplineSlerp( pvOut:Vector3D, v1:Vector3D, v2:Vector3D,
											 v3:Vector3D, v4:Vector3D, t:Number ):void
		{
			var t2:Number = t * t;
			var t3:Number = t2 * t;
			
			var m0:Vector3D, m1:Vector3D;
			var alpha:Number = 0;
			
			m0 = v2.subtract(v1);
			m0.incrementBy(v3);
			m0.decrementBy(v2);
			m0.scaleBy((1 - alpha) / 2.0);
						
			m1 = v3.subtract(v2);
			m1.incrementBy(v4);
			m1.decrementBy(v3);
			m1.scaleBy((1 - alpha) / 2.0);
						
			pvOut.copyFrom(v2);
			pvOut.scaleBy( 2*t3 - 3*t2 + 1);
			
			m0.scaleBy(t3 - 2*t2 + t);
			m1.scaleBy(t3-t2);
			
			pvOut.incrementBy(m0);
			pvOut.incrementBy(m1);
			
			m1.copyFrom(v3);
			m1.scaleBy(-2 * t3 + 3 * t2);
			
			pvOut.incrementBy(m1);
		}
	}
}

import flash.geom.Vector3D;

internal class TrailPoint
{
	public var p:Vector3D = new Vector3D;
	public var startTime:Number;
	public var dir:Vector3D = new Vector3D;
	public var flyV:Number;
	public var flyA:Number;	
}

internal class PolyTrailVertexData
{
	public var pos1:Vector3D = new Vector3D;
	public var pos2:Vector3D = new Vector3D;
	public var time:Number;
	/*
	public function PolyTrailVertexData(pos1:Vector3D, pos2:Vector3D, time:Number)
	{
		this.pos1 = pos1;
		this.pos2 = pos2;
		this.time = time;
	}
	*/
}