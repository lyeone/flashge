/*@author lye 123*/

package FlashGE.effects 
{	
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.common.Transform3D;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.objects.Entity;
	import FlashGE.objects.Linkable;
	import FlashGE.objects.Model;
	import FlashGE.objects.RenderObject;
	import FlashGE.render.Camera3D;
	
	use namespace FlashGE;
	
	public class Effect extends Linkable
	{
		FlashGE var unitsList:Vector.<EffectUnitBase> = new Vector.<EffectUnitBase>;
		FlashGE var effectGroup:EffectGroup = null;
		FlashGE var effectProp:EffectProp = null;
		FlashGE var effectName:String = null;
		FlashGE var curFrame:int = 0;
		FlashGE var loadState:int = Defines.LOAD_NULL;
		FlashGE var loop:Boolean = false;
		FlashGE var paused:Boolean = false;
		FlashGE var loadListener:Function;
		FlashGE var updateListener:Function;
		FlashGE var disposeListeners:Vector.<Function>;
		FlashGE var maxLifeFrameNum:uint = uint.MAX_VALUE;
		FlashGE var randNodesList:Vector.<Entity>;
		FlashGE var randNodesListInited:Boolean = false;
		FlashGE var randNodesHolder:RenderObject;
		FlashGE var linkedTrans:Transform3D;
		FlashGE var aspectDst:Number = 1;
		FlashGE var outKey:KeyPair;
		FlashGE var offsetPosX:Number = 0;
		FlashGE var offsetPosY:Number = 0;
		FlashGE var offsetPosZ:Number = 0;
		FlashGE var framePosX:Number = 0;
		FlashGE var framePosY:Number = 0;
		FlashGE var framePosZ:Number = 0;
		FlashGE var frameScaleX:Number = 1;
		FlashGE var frameScaleY:Number = 1;
		FlashGE var frameScaleZ:Number = 1;
		FlashGE var hasDisposed:Boolean = false;
		FlashGE var isEquip:Boolean = false;
		FlashGE var onlyFollowPos:Boolean = false;
		
		static public var preloadEffects:Dictionary;
		
		public function Effect()
		{
			super();
		}
		
		override public function setOnlyFollowPos(value:Boolean):void
		{
			this.onlyFollowPos = value;
		}
		
		public function toString():String
		{
			var ret:String;
			if (this.effectGroup != null) {
				ret = "Effect(effectGroup=" + this.effectGroup.fileName + ",effectName=" + this.effectName;	
			} else {
				ret = "Effect(effectName=" + this.effectName;
			}
			ret += (",curFrame=" + curFrame);
			ret += (",loadState" + loadState);
			ret += (",loop" + loop);
			ret += ")";
			return ret;
		}
		
		public function setOffsetPos(x:Number, y:Number, z:Number):void
		{
			if (this.transformChanged==false) {
				this.transformChanged = this.offsetPosX != x || 
					this.offsetPosY != y || this.offsetPosZ != z;
			}
			this.offsetPosX = x;
			this.offsetPosY = y;
			this.offsetPosZ = z;
		}
		
		public function setFramePos(x:Number, y:Number, z:Number):void
		{
			if (this.transformChanged==false) {
				this.transformChanged = this.framePosX != x || 
					this.framePosY != y || this.framePosZ != z;
			}
			this.framePosX = x;
			this.framePosY = y;
			this.framePosZ = z;
		}
		
		public function setFrameScale(x:Number, y:Number, z:Number):void
		{
			if (this.transformChanged==false) {
				this.transformChanged = this.frameScaleX != x || 
					this.frameScaleY != y || this.frameScaleZ != z;
			}
			this.frameScaleX= x;
			this.frameScaleY = y;
			this.frameScaleZ = z;
		}
		
		public function setDstPos(flyDstLen:uint):void
		{
			this.aspectDst = 1;
			if (this.effectProp == null) {
				return;
			}
			if (this.effectProp.maxDstLen == uint.MAX_VALUE) {
				this.effectProp.calcMaxDstLen();
			}
			if (this.effectProp.maxDstLen != uint.MAX_VALUE) {
				aspectDst = flyDstLen / this.effectProp.maxDstLen;
			}
		}
		
		public function setRandNodesHolder(holder:RenderObject):void
		{
			this.randNodesHolder = holder;
		}
		
		public function getUsedX():Number 
		{
			return this.x + this.offsetPosX + this.framePosX;
		}
		
		public function getUsedY():Number 
		{
			return this.y + this.offsetPosY + this.framePosY;
		}
		
		public function getUsedZ():Number 
		{
			return this.z + this.offsetPosZ + this.framePosZ;
		}
		
		public function getRandNodesHolder():RenderObject
		{
			if (this.randNodesHolder!=null) return this.randNodesHolder;
			
			var ret:RenderObject = this.getLinkedHolder() as RenderObject;
			if (ret !=null) return ret;
			
			ret = this.parent as RenderObject;
			return ret;
		}
		
		public function getEffectName():String
		{
			return this.effectName;
		}
		
		public function setMaxLifeFrameNum(v:uint):void
		{
			this.maxLifeFrameNum = v;
		}
		
		public function getEffectGroup():EffectGroup
		{
			return this.effectGroup;
		}
		
		public function setLoop(loop:Boolean):void
		{
			this.loop = loop;
		}
		
		public function getLoop():Boolean
		{
			return this.loop;
		}
		
		public function setPause(pause:Boolean):void
		{
			this.paused = pause;	
		}
		
		public function isPaused():Boolean
		{
			return this.paused;	
		}
		
		public function isLoaded():Boolean
		{
			return this.loadState == Defines.LOAD_FAILD || this.loadState == Defines.LOAD_OK;
		}
		
		public function getLoadState():int
		{
			return this.loadState;
		}
		
		public function dispose():void
		{
			this.hasDisposed = true;
			this.updateListener = null;
			
			var i:int, n:int;
			
			if (this.unitsList!=null) {
				for (i = 0, n = this.unitsList.length; i < n; i++) {
					this.unitsList[i].dispose();
				}
				this.unitsList = null;
			}
			this.removeFromParent();
			if (this.effectGroup != null) {
				this.effectGroup.removeListener(this.onLoaded);
			}
			
			this.loadState = Defines.LOAD_NULL;
			
			if (this.disposeListeners != null) {
				for (i =0, n = this.disposeListeners.length; i < n; i++) {
					var disposeFun:Function = this.disposeListeners[i];
					if (disposeFun != null) {
						disposeFun(this);
						this.disposeListeners[i] = null;
					}
				}
			}
		}
		
		public function checkSave():void
		{
			if (this.unitsList!=null) {
				for (var i:int = 0,n:int = this.unitsList.length; i < n; i++) {
					this.unitsList[i].checkSave();
				}
			}
		}
		
		public function addUnit(unit:EffectUnitBase):void
		{
			unit.system = this;
			unitsList.push(unit);
			this.effectProp.unitPropList.push(unit.unitProp);
		}
		
		public function removeUnit(unitIndex:int):void
		{
			var unit:EffectUnitBase = this.unitsList[unitIndex];
			this.unitsList.splice(unitIndex, 1);
			unit.dispose();
			this.effectProp.unitPropList.splice(unitIndex, 1);
		}
		
		public function swapUnit(unitIndexA:int, uintIndexB:int):void
		{
			var a:Object = this.unitsList[unitIndexA];
			var b:Object = this.unitsList[uintIndexB];
			this.unitsList[uintIndexB] = a;
			this.unitsList[unitIndexA] = b;
			
			a = this.effectProp.unitPropList[unitIndexA];
			b = this.effectProp.unitPropList[uintIndexB];
			this.effectProp.unitPropList[uintIndexB] = a;
			this.effectProp.unitPropList[unitIndexA] = b;
		}
		
		public function getUnitByName(unitName:String):EffectUnitBase
		{
			var n:uint = this.unitsList.length;
			for (var i:uint=0; i <n; i++) {
				if (this.unitsList[i].unitProp.name == unitName)
					return this.unitsList[i];
			}
			
			return null;
		}
		
		override public function removeFromParent():void
		{
			super.removeFromParent();
			var holder:Entity = this.getLinkedHolder();
			if (holder != null) {
				holder.removeLinkObject(this);
			}
		}
		
		private function load(fileName:String, effectName:String, loadListener:Function, insertFirst:Boolean):void
		{
			this.effectName = effectName;
			this.loadListener = loadListener;
			this.loadState = Defines.LOAD_ING;
			this.effectGroup = EffectGroup.create(fileName, onLoaded, insertFirst);	
		}
		
		static public function createFromFile(fileName:String, effectName:String = null, loadListener:Function = null, userData:int = -1, insertFirst:Boolean = false):Effect
		{
			var ret:Effect;
			if (preloadEffects != null && effectName == null) {
				ret = preloadEffects[fileName];
				if (ret != null) {
					delete preloadEffects[fileName];
					if (!ret.hasDisposed) {
						return ret;
					}
				}
			}
			
			ret = new Effect;
			ret.setUserData(userData);
			ret.load(fileName, effectName, loadListener, insertFirst);
			return ret;
		}
		
		static public function createFromGroup(effectGroup:EffectGroup, effectName:String):Effect
		{
			var ret:Effect = new Effect;
			ret.effectGroup = effectGroup;
			ret.effectName = effectName;
			ret.effectProp = effectGroup.getEffectProp(effectName);
			ret.loadState = Defines.LOAD_OK;
			ret.initUnits();
			return ret;
		}
		
		public function appendGroup(src:EffectGroup):void
		{
			var nameIndexs:Vector.<String> = src.getNameIndexs();
			if (nameIndexs == null) return;
			
			for (var j:int = 0; j < nameIndexs.length; j++) {
				var efName:String = nameIndexs[j];
				var ef:Effect = Effect.createFromGroup(src, efName);
				var aspect:Number = this.effectProp.lifeFramesNum / ef.effectProp.lifeFramesNum;
				for (var i:int = 0; i < ef.unitsList.length; i++) {
					this.addUnit(ef.unitsList[i]);
					ef.unitsList[i].unitProp.lifeFramesNum*=aspect;
				}
			}
		}		
		
		private function initUnits():void
		{
			if (this.effectProp.offsetKeysValid || this.effectProp.scaleKeysValid) {
				calcFrameValues(0);
			}
			
			var unit:EffectUnitBase;			
			for (var i:int=0,unitNum:uint = this.effectProp.unitPropList.length; i < unitNum; i++) {
				var unitProp:UnitBaseProp = this.effectProp.unitPropList[i];	
				var unitType:uint = unitProp.TYPE();
				if (unitType == EffectProp.TYPE_CAMERA_SHAKE){
					unit = new CameraShake();
				} else if(unitType == EffectProp.TYPE_PARTICLE){
					unit = new ParticleUnit();
				} else if(unitType == EffectProp.TYPE_POLYTRAIL){
					unit = new PolyTrailUnit();
				} else if(unitType == EffectProp.TYPE_SPRITE){
					unit = new SpriteUnit();
				} else if(unitType == EffectProp.TYPE_MODEL) {
					unit = new ModelUnit();
				} else if (unitType == EffectProp.TYPE_SCENE_CAMERA) {
					unit = new SceneCamera();
				} else if(unitType == EffectProp.TYPE_EMPTY) {
					unit = new EmptyUnit();
				} else if(unitType == EffectProp.TYPE_POLYTRAIL2){
					unit = new PolyTrail2Unit();
				} else if(unitType == EffectProp.TYPE_CAMERA_TRACK) {
					unit = new CameraTrack();
				} else if(unitType == EffectProp.TYPE_MATERIAL) {
					unit = new MaterialUnit();
				} else if(unitType == EffectProp.TYPE_SCENE) {
					unit = new SceneUnit();
				} else if(unitType == EffectProp.TYPE_WING) {
					unit = new WingUnit();
				} else if(unitType == EffectProp.TYPE_SOUND) {
					unit = new SoundUnit();
				} else if(unitType == EffectProp.TYPE_WATER) {
					unit = new WaterUnit();
				}
				unit.contructor(unitProp);
				unit.system = this;
				unitsList.push(unit);
			}
		}
		
		public function getCurFrame():Number 
		{
			return this.curFrame;
		}
		
		public function getTotalFrame():int
		{
			return this.effectProp.lifeFramesNum;
		}
			
		public function setCurFrame(frame:Number, mod:Boolean):void
		{
			if (mod) {
				this.curFrame = frame %(this.effectProp.lifeFramesNum+1);
			} else {
				if (frame > this.effectProp.lifeFramesNum) {
					frame = this.effectProp.lifeFramesNum;
				} else if (frame < 0) {
					frame = 0;
				}
				
				this.curFrame = frame;
			}
			
			this.calcFrameValues(this.curFrame/this.effectProp.maxOffsetFrame);
		}
		
		private function onLoaded(group:EffectGroup):void
		{		
			if (this.hasDisposed) {
				this.loadState = Defines.LOAD_FAILD;
				return;
			}
			
			if (group.loadState == Defines.LOAD_OK) {
				this.effectGroup = group;
				if (this.effectName == null) {
					this.effectName = group.nameIndexs[0];	
				}
				this.effectProp = effectGroup.getEffectProp(this.effectName);
				if (this.effectProp == null) {
					this.loadState = Defines.LOAD_FAILD;
				} else {
					this.loadState = Defines.LOAD_OK;
					this.initUnits();
				}
			} else {
				this.loadState = Defines.LOAD_FAILD;
			}
			
			if (this.loadListener != null) {
				this.loadListener(this, this.loadState == Defines.LOAD_OK);
				this.loadListener = null;
			}
		}
		
		public function setUpdateListener(listener:Function):void
		{
			this.updateListener = listener;	
		}
		
		public function addDisposeListener(listener:Function):void
		{
			if (listener == null) return;
			
			this.disposeListeners = this.disposeListeners || new Vector.<Function>;
			for (var i:int =0, n:int = this.disposeListeners.length; i < n; i++) {
				if (this.disposeListeners[i] == null) {
					this.disposeListeners[i] = listener;
					return;
				}
			}
			
			this.disposeListeners.push(listener);
		}
		
		public function removeDisposeListener(listener:Function):void
		{
			if (listener == null || this.disposeListeners == null) return;
			
			for (var i:int =0, n:int = this.disposeListeners.length; i < n; i++) {
				if (this.disposeListeners[i] == listener) {
					this.disposeListeners[i] = null;
					return;
				}
			}
		}
		
		public function isLifeEnd():Boolean
		{
			return this.loop==false && this.curFrame + 1 == this.effectProp.lifeFramesNum;
		}
		
		override public function setLinkedTransform(tempTrans:Transform3D):void
		{
			if (tempTrans==null) {
				this.linkedTrans = null;
			} else {
				linkedTrans = linkedTrans || new Transform3D;
				linkedTrans.copy(tempTrans);
			}
		}
		
		override FlashGE function collectChildrenRenderUnits(camera:Camera3D, isShadowPass:Boolean):int
		{
			var renderCount:int = super.collectChildrenRenderUnits(camera, isShadowPass);
			
			if (this.loadState == Defines.LOAD_FAILD) {
				dispose();
				return 0;
			}
			
			if (this.unitsList==null || this.loadState != Defines.LOAD_OK) {
				return 0;
			}
			
			for (var i:int = 0, n:int = this.unitsList.length; i < n; i++) {
				this.unitsList[i].updateBeforRender(camera);
			}
			
			return renderCount;
		}
		
		override FlashGE function composeTransforms(calcInverse:Boolean = true):void
		{
			var curX:Number = this.x + this.offsetPosX + this.framePosX;
			var curY:Number = this.y + this.offsetPosY + this.framePosY;
			var curZ:Number = this.z + this.offsetPosZ + this.framePosZ;
			
			var curScaleX:Number = this.scaleX * this.frameScaleX;
			var curScaleY:Number = this.scaleY * this.frameScaleY;
			var curScaleZ:Number = this.scaleZ * this.frameScaleZ;
			
			if (this.linkedTrans!=null) {
				this.transform.compose(0,0,0,0,0,0, curScaleX, curScaleY, curScaleZ);
				this.transform.append(this.linkedTrans);
				this.transform.d = curX;
				this.transform.h = curY;
				this.transform.l = curZ;
				if (calcInverse) {
					this.inverseTransform.copy(this.transform);
					this.inverseTransform.invert();
				}
			} else {
				
				if (this.dirTransform == null) {
					
					var cosX:Number = Math.cos(rotationX);
					var sinX:Number = Math.sin(rotationX);
					var cosY:Number = Math.cos(rotationY);
					var sinY:Number = Math.sin(rotationY);
					var cosZ:Number = Math.cos(rotationZ);
					var sinZ:Number = Math.sin(rotationZ);
					var cosZsinY:Number = cosZ*sinY;
					var sinZsinY:Number = sinZ*sinY;
					var cosYscaleX:Number = cosY*curScaleX;
					var sinXscaleY:Number = sinX*curScaleY;
					var cosXscaleY:Number = cosX*curScaleY;
					var cosXscaleZ:Number = cosX*curScaleZ;
					var sinXscaleZ:Number = sinX*curScaleZ;
					transform.a = cosZ*cosYscaleX;
					transform.b = cosZsinY*sinXscaleY - sinZ*cosXscaleY;
					transform.c = cosZsinY*cosXscaleZ + sinZ*sinXscaleZ;
					transform.d = curX;
					transform.e = sinZ*cosYscaleX;
					transform.f = sinZsinY*sinXscaleY + cosZ*cosXscaleY;
					transform.g = sinZsinY*cosXscaleZ - cosZ*sinXscaleZ;
					transform.h = curY;
					transform.i = -sinY*curScaleX;
					transform.j = cosY*sinXscaleY;
					transform.k = cosY*cosXscaleZ;
					transform.l = curZ;
					if (calcInverse) {
						var sinXsinY:Number = sinX*sinY;
						cosYscaleX = cosY/curScaleX;
						cosXscaleY = cosX/curScaleY;
						sinXscaleZ =-sinX/curScaleZ;
						cosXscaleZ = cosX/curScaleZ;
						inverseTransform.a = cosZ*cosYscaleX;
						inverseTransform.b = sinZ*cosYscaleX;
						inverseTransform.c =-sinY/curScaleX;
						inverseTransform.d =-inverseTransform.a*curX - inverseTransform.b*curY - inverseTransform.c*curZ;
						inverseTransform.e = sinXsinY*cosZ/curScaleY - sinZ*cosXscaleY;
						inverseTransform.f = cosZ*cosXscaleY + sinXsinY*sinZ/curScaleY;
						inverseTransform.g = sinX*cosY/curScaleY;
						inverseTransform.h =-inverseTransform.e*curX - inverseTransform.f*curY - inverseTransform.g*curZ;
						inverseTransform.i = cosZ*sinY*cosXscaleZ - sinZ*sinXscaleZ;
						inverseTransform.j = cosZ*sinXscaleZ + sinY*sinZ*cosXscaleZ;
						inverseTransform.k = cosY*cosXscaleZ;
						inverseTransform.l =-inverseTransform.i*curX - inverseTransform.j*curY - inverseTransform.k*curZ;
					}
				} else {
					
					//curX = this.x + this.offsetPosX;
					//curY = this.y + this.offsetPosY;
					//curZ = this.z + this.offsetPosZ;
					
					this.transform.compose(0, 0, 0,this.rotationX, this.rotationY,this.rotationZ, curScaleX, curScaleY, curScaleZ);
					this.transform.append(this.dirTransform);
					
					var localX:Number = this.offsetPosX + this.framePosX,
						localY:Number = this.offsetPosY + this.framePosY,
						localZ:Number = this.offsetPosZ + this.framePosZ;
					var offsetX:Number = localX*dirTransform.a + localY*dirTransform.b + localZ*dirTransform.c;
					var offsetY:Number = localX*dirTransform.e + localY*dirTransform.f + localZ*dirTransform.g;
					var offsetZ:Number = localX*dirTransform.i + localY*dirTransform.j + localZ*dirTransform.k;
					
					this.transform.d = this.x + offsetX;
					this.transform.h = this.y + offsetY;
					this.transform.l = this.z + offsetZ;
					if (calcInverse) {
						this.inverseTransform.copy(this.transform);
						this.inverseTransform.invert();
					}
				}
			}
		}
		
		override public function isOnlyFollowPos():Boolean
		{
			return effectProp!=null&&effectProp.followType == Defines.FOLLOW_POS;
		}
		
		private var aniPreTimer:Number = 0;
		private var deltaAniTimer:Number = 0;
		private var lockAniFrame:Boolean = false;
		FlashGE var forceFrameChanged:int = 0;
		public function setLockAniFrame(lock:Boolean):void
		{
			if (lock == false && this.lockAniFrame == true) {
				aniPreTimer = 0;
				deltaAniTimer = 0;
			}
			this.lockAniFrame = lock;
		}
		
		override FlashGE function collectRenderUnits(camera:Camera3D, isShadowPass:Boolean):int 
		{
			
			if (this.loadState == Defines.LOAD_FAILD) {
				dispose();
				return 0;
			}
			
			if (this.loadState != Defines.LOAD_OK) {
				return 0;
			}
			
			if (this.updateListener != null) {
				this.updateListener(this, this.curFrame, this.effectProp.lifeFramesNum);
				if (this.loadState != Defines.LOAD_OK) {
					return 0;
				}
			}

			if (this.maxLifeFrameNum != uint.MAX_VALUE && this.maxLifeFrameNum > 0) {
				this.maxLifeFrameNum--;
				if (this.maxLifeFrameNum == 0) {
					this.dispose();
					return 0;
				}
			}
			
			if (this.curFrame > this.effectProp.lifeFramesNum) {
				if (this.loop == true) {
					
					this.curFrame = 0;
					this.randNodesListInited = false;
					calcFrameValues(0);
				} else {
					this.dispose();
					return 0;
				}
			}
			
			// update
			for (var i:int = 0, n:int = this.unitsList.length; i < n; i++) {
				this.unitsList[i].updateOneFrame(camera);
			}
			
			if (lockAniFrame == false) {
				
				var curDeltaTimer:Number = 0;
				
				var curTimer:Number = getTimer();
				if (this.aniPreTimer != 0) {
					curDeltaTimer = curTimer - this.aniPreTimer;
				}
				
				this.deltaAniTimer += curDeltaTimer;
				this.aniPreTimer = curTimer;
				
				var deltaAniFrame:int = this.deltaAniTimer / 33.33;
				
				if (deltaAniFrame > 0) {
					deltaAniTimer = deltaAniTimer - deltaAniFrame * 33.33;
				}
				
				if (camera.enableEffectUseDeltaTimer) {
					deltaAniFrame = camera.curDeltaTimer / 33.33;
				}
				
				if (deltaAniFrame < 0) {
					deltaAniFrame = 0;
				}
				
				if (deltaAniFrame >= 1 || forceFrameChanged > 0) {
					
					if (forceFrameChanged > 1) {
						this.curFrame+=forceFrameChanged;
						if (this.curFrame > this.effectProp.lifeFramesNum) {
							this.curFrame = this.effectProp.lifeFramesNum + 1;
						}
					} else {
						this.curFrame++;
					}
					deltaAniFrame = 0;
				}
			}

			if (this.aspectDst != Number.MIN_VALUE) {
				
				if (this.effectProp.offsetKeysValid || this.effectProp.scaleKeysValid) {
					calcFrameValues(this.curFrame / this.effectProp.maxOffsetFrame);
				}
			}
			
			return n;
			
		}
		
		public function resetAllParams():void
		{
			for (var i:int = 0, n:int = this.unitsList.length; i < n; i++) {
				this.unitsList[i].resetAllParams();
			}
		}
		
		private function calcFrameValues(percent:Number):void
		{
			if (percent > 1.0) percent = 1.0;
		
			if (outKey == null) {
				outKey = new KeyPair;
				outKey.init(0);
			}
			
			if (this.effectProp.offsetKeysValid) {
				this.getInterpolationValue(percent, this.effectProp.offsetKeys, outKey);
				this.setFramePos(outKey.items[0], outKey.items[1] * this.aspectDst, outKey.items[2]);
			}
			
			if (this.effectProp.scaleKeysValid) {
				this.getInterpolationValue(percent, this.effectProp.scaleKeys, outKey);
				this.setFrameScale(outKey.items[0], outKey.items[1] * this.aspectDst, outKey.items[2]);
			}
		}
		
		public function getRandomNodeCount():int
		{
			if (randNodesListInited == false) {
				initRandomNode();
			}
			return this.randNodesList.length;
		}
		
		private function initRandomNode():void
		{
			var host:RenderObject = this.getRandNodesHolder();
			if (host == null) return;
			
			var model:Model = host.getModel();
			if (model == null) return;
			randNodesList = randNodesList || new Vector.<Entity>;
			randNodesList.length = 0;
			for each(var subMesh:Entity in model.subMeshList) {
				if (subMesh.isVisible() && subMesh.getName().charAt(0) == "#" ) {
					randNodesList.push(subMesh);
				}
			}
			randNodesListInited = true;
		}
		
		public function randomNode():Entity
		{
			if (randNodesListInited == false) {
				initRandomNode();
			}
			
			if (randNodesList.length == 0) return null;
			
			var index:int = Math.random() * randNodesList.length;
			var ret:Entity = randNodesList[index];
			randNodesList.splice(index, 1);
			return ret;
		}
	}
}
