/*@author lye 123*/

package FlashGE.effects
{	
	import flash.geom.Vector3D;
	import flash.media.Camera;
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.common.AxisAlignedBox;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.SpriteUnitProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.materials.Material;
	import FlashGE.objects.Entity;
	import FlashGE.objects.Surface;
	import FlashGE.render.Camera3D;
	import FlashGE.render.RenderUnit;
	import FlashGE.resources.Geometry;
	
	use namespace FlashGE;
	
	public class SpriteUnit extends EffectUnitBase
	{		
		static FlashGE var tempTransA:Transform3D;
		static FlashGE var tempTransB:Transform3D;
		static FlashGE var offsetKey:KeyPair;
		static FlashGE var scaleKey:KeyPair;
		static FlashGE var colorKey:KeyPair;
		static FlashGE var rotateKey:KeyPair;
		
		static FlashGE var uniformGeometry:Geometry;
		FlashGE var geometry:Geometry;
		FlashGE var surface:Surface;
		FlashGE var rotateS:Number = 0;
		FlashGE var preUnitFrameTimer:int = -1;
		FlashGE var manualColor:Boolean = false;
		FlashGE var baseTransformTemp:Transform3D;
		FlashGE var basePos:Vector3D;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_SPRITE;
		}
		
		override public function dispose():void
		{
			super.dispose();
			
			if (this.geometry != null) {
				this.geometry.release(true);
				this.geometry = null;
			}

			if (this.surface != null) {
				this.surface.clearRenderUnits();
				this.surface = null;
			}
			
			baseTransformTemp = null;
			
			if (basePos!=null) {
				Pool.getInstance().pushVector3D(basePos);
				basePos = null;
			}
		}
		
		static public function initStaticContext():void
		{
			SpriteUnit.tempTransA = new Transform3D;
			SpriteUnit.tempTransB = new Transform3D;
			
			SpriteUnit.offsetKey = new KeyPair;
			SpriteUnit.offsetKey.init(0);
			
			SpriteUnit.scaleKey = new KeyPair;
			SpriteUnit.scaleKey.init(0);
			
			SpriteUnit.colorKey = new KeyPair;
			SpriteUnit.colorKey.init(0);
			
			SpriteUnit.rotateKey = new KeyPair;
			SpriteUnit.rotateKey.init(0);
		}
		
		override FlashGE function calcProgramKey(camera:Camera3D):void
		{
			if (programKey == null) {
				var isEditor:Boolean = CONFIG::EDITOR;
				if (camera.context3DProperties.isOpenGL || isEditor) {
					this.programKey = "sp";
				} else {
					var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;
					this.programKey = "sp" + String((prop.faceType << 16) | (prop.followType<<8)|(prop.completelyFollow ? 1 : 0));
				}
			}
		}
		
		override public function contructor(unitProp:UnitBaseProp = null):void
		{
			super.contructor(unitProp);
			
			this.rotateS = 0;
			this.preUnitFrameTimer = -1;
			this.manualColor = false;
			
			if (unitProp == null) {
				var newProp:SpriteUnitProp = new SpriteUnitProp;
				newProp.contructor();
				this.setProp(newProp);
			}
		}
		
		override public function rotateFaceNormal(dx:Number, dy:Number, dz:Number):void
		{
			var prop:SpriteUnitProp = this.getUnitProp() as SpriteUnitProp;
			prop.usePreVersion = 2;
			prop.dirNormal.x += dx;
			prop.dirNormal.y += dy;
			prop.dirNormal.z += dz;
			prop.calcNormal();
		}
		
		override public function resetFaceNormal():void
		{
			var prop:SpriteUnitProp = this.getUnitProp() as SpriteUnitProp;
			prop.usePreVersion = 2;
			prop.dirNormal.x  = 0;
			prop.dirNormal.y  = 0;
			prop.dirNormal.z  = 0;
			prop.calcNormal();
		}
		
		override public function clearRenderUnits():void
		{
			getMaterial().markProgramDirty();
			this.programKey = null;
		}
		
		override public function syncParamsToMaterial():void
		{
			super.syncParamsToMaterial();
			if (this.surface != null) {
				this.surface.clearRenderUnits();
			}
			getMaterial().markProgramDirty();
			this.programKey = null;
			
			var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;
			initGeometry(prop.width, prop.height);
		}
		
		override public function resetAllParams():void
		{
			this.rotateS = 0;
			this.preUnitFrameTimer = -1;
			baseTransformTemp = null;
			
			if (basePos!=null) {
				Pool.getInstance().pushVector3D(basePos);
				basePos = null;
			}
		}
		
		public function initGeometry(spWidth:Number = 100, spLength:Number = 100):void
		{
			var curProp:SpriteUnitProp = this.unitProp as SpriteUnitProp;
			curProp.width = spWidth;
			curProp.height = spLength;
			if (this.geometry != null) {
				this.geometry.release(true);
			}
			this.geometry = uniformGeometry;
			
			if (this.geometry == null) {
				var x:int;
				var y:int;
				
				var vertices:ByteArray = new ByteArray();
				vertices.endian = Endian.LITTLE_ENDIAN;
				
				for (x = 0; x < 2; x++) {
					for (y = 0; y < 2; y++) {
						vertices.writeFloat(x - 0.5);
						vertices.writeFloat(y - 0.5);
						vertices.writeFloat(0);
						vertices.writeFloat(x - 0.5);
						vertices.writeFloat(1 - y - 0.5);
					}
				}
				
				var indices:Vector.<uint> = new Vector.<uint>();
				for (x = 0; x < 2; x++) {
					for (y = 0; y < 2; y++) {
						if (x < 1 && y < 1) {
							createFace(indices, x*2 + y, 
								(x + 1)*2 + y, (x + 1)*2 + y + 1, x*2 + y + 1);
						}
					}
				}
				
				uniformGeometry = Geometry.create();
				var attributes:Array = [
					Defines.POSITION,
					Defines.POSITION,
					Defines.POSITION,
					Defines.TEXCOORDS[0],
					Defines.TEXCOORDS[0]
				];
				
				uniformGeometry.addVertexStream(attributes);
				uniformGeometry.indicesBuffer = indices;
				uniformGeometry.vertexStreamsList[0].data = vertices;
				uniformGeometry.verticesCount = 4;
				
				this.geometry = uniformGeometry;
			}
			this.geometry.addRef();
			if (surface != null) {
				surface.clearRenderUnits();
			} else {
				surface = new Surface();
			}
			
			surface.object = this;
			surface.indexBegin = 0;
			surface.trianglesNum = 2;
			
			if (boundBox == null) {
				boundBox = new AxisAlignedBox();
			}
			
			boundBox.minX =-spWidth*0.5;
			boundBox.minY =-spLength*0.5;
			boundBox.minZ = 0;
			boundBox.maxX = spWidth*0.5;
			boundBox.maxY = spLength*0.5;
			boundBox.maxZ = 0;
		}
		
		private function createFace(indices:Vector.<uint>, a:int, b:int, c:int, d:int):void
		{
			indices.push(a);
			indices.push(b);
			indices.push(c);
			indices.push(a);
			indices.push(c);
			indices.push(d);
		}
		
		public function setManualColor(manual:Boolean):void
		{
			this.manualColor = manual;
		}
		
		protected function calcTransformsGPU(renderUnit:RenderUnit, constIndex:int, 
										  percent:Number, camera:Camera3D, exScaleX:Number, exScaleY:Number):int
		{
			var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;
			if (prop.faceType == UnitBaseProp.DIR_TYPE_PARENT && 
				prop.followType ==UnitBaseProp.DIR_TYPE_PARENT) {
				
				return this.calcParentParent(renderUnit, constIndex, percent, camera, exScaleX, exScaleY);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_PARENT && 
				prop.followType ==UnitBaseProp.DIR_TYPE_WORLD) {
				
				return this.calcParentWorld(renderUnit, constIndex, percent, camera, exScaleX, exScaleY);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_WORLD && 
				prop.followType ==UnitBaseProp.DIR_TYPE_PARENT) {
				
				return this.calcWorldParent(renderUnit, constIndex, percent, camera, exScaleX, exScaleY);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_WORLD && 
				prop.followType ==UnitBaseProp.DIR_TYPE_WORLD) {
				
				return this.calcWorldWorld(renderUnit, constIndex, percent, camera, exScaleX, exScaleY);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_CAMERA && 
				prop.followType ==UnitBaseProp.DIR_TYPE_PARENT) {
				
				return this.calcCameraParent(renderUnit, constIndex, percent, camera, exScaleX, exScaleY);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_CAMERA && 
				prop.followType ==UnitBaseProp.DIR_TYPE_WORLD) {
				
				return this.calcCameraWorld(renderUnit, constIndex, percent, camera, exScaleX, exScaleY);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_H_CAMERA && 
				prop.followType ==UnitBaseProp.DIR_TYPE_PARENT) {
				
				return this.calcHCameraParent(renderUnit, constIndex, percent, camera, exScaleX, exScaleY);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_H_CAMERA && 
				prop.followType ==UnitBaseProp.DIR_TYPE_WORLD) {
				
				return this.calcHCameraWorld(renderUnit, constIndex, percent, camera, exScaleX, exScaleY);
			}
			
			if (percent >=1 && system.loop==false) {
				this.rotateS = 0;
				this.preUnitFrameTimer = -1;
			}
			
			return 0;
		}
		
		override FlashGE function updateOneFrame(camera:Camera3D):void
		{			
			if (!isShow()) {
				return;
			}
			
			var percent:Number = getPercent();
			if (percent < 0 || percent > 1) {
				return;
			}
						
			var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;
			this.getInterpolationValue(percent, prop.colorKeys, colorKey);
			var constIndex:int = 0;
			if (surface != null && surface.renderUnit0 == null) {
				surface.renderUnit0 = camera.pool.popRenderUnit();
				surface.renderUnit0.firstIndex = 0;
				surface.renderUnit0.numTriangles = 2;
				surface.object = this;
			}
			
			
			var isEditor:Boolean = CONFIG::EDITOR;
			if (camera.context3DProperties.isOpenGL || isEditor) {
				
				calcTransforms(percent, camera, prop.width, prop.height);	
			} else {
				this.calcTransformsGPU(surface.renderUnit0, constIndex, 
					percent, camera, prop.width, prop.height);
			}
			if (surface !=null && this.material != null) {
				var curUnitFrame:Number = percent * this.unitProp.lifeFramesNum;
				calcUV(curUnitFrame);
				var tm:Material = this.material;
				if (manualColor==false) {
					tm.setColor(colorKey.items[0], colorKey.items[1], colorKey.items[2], colorKey.items[3]);
				}
				tm.setUVContext(this.uvCurUOffset, this.uvCurVOffset, this.uvCurUScale, this.uvCurVScale, this.uvCurV0, this.uvCurV1);
				//tm.setCurTime(curUnitFrame);
				tm.setOpenDoubleAlpha(prop.updateZ ==1);
				
				var needCollect:Boolean = true;
				if (beforeRenderListener != null) {
					needCollect = beforeRenderListener(this, camera);
				}
				
				if (needCollect == true) {
					
					CONFIG::CACHE_PROGRAM_KEY {
						if (this.material.programCachedID == null && this.unitProp.programCachedID != null) {
							this.material.programCachedID = this.unitProp.programCachedID;
						}
							
						if (this.surface.materialKey==null && this.unitProp.materialKey != null) {
							this.surface.materialKey = this.unitProp.materialKey;
						}
				
						if (this.programKey == null && this.unitProp.programKey != null) {
							this.programKey = this.unitProp.programKey;
						}
					}

					var renderCount:int = this.material.collectDraws(camera, surface, geometry);
					
					CONFIG::CACHE_PROGRAM_KEY {
						if (renderCount > 0) {
							if (this.unitProp.programCachedID == null) {
								this.unitProp.programCachedID = this.material.programCachedID;
							}
							
							if (this.unitProp.materialKey == null) {
								this.unitProp.materialKey = this.surface.materialKey;
							}
							
							if (this.unitProp.programKey == null) {
								this.unitProp.programKey = this.programKey;
							}
						}
					}
				}
				
			}
			
			this.calculateChildrenVisibility(camera);			
		}
		
		override public function clone():Entity
		{
			var unit:SpriteUnit = new SpriteUnit();
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override protected function clonePropertiesFrom(source:Entity):void
		{
			super.clonePropertiesFrom(source);
			var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;
			this.initGeometry(prop.width, prop.height);
		}
		
		private function addMatMulMat(shaderArray:Array, arrayIndex:int, ret:String, tmp:String, constIndex:int,
									  row0:String, row1:String, row2:String):int
		{
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mul "+ret+".xyzw, "+row0+".xyzw, c"+constIndex+".xxxx";
			shaderArray[arrayIndex++] = "mul "+tmp+".xyzw, "+row1+".xyzw, c"+constIndex+".yyyy";
			shaderArray[arrayIndex++] = "add "+ret+", "+ret+", " + tmp;
			shaderArray[arrayIndex++] = "mul "+tmp+".xyzw, "+row2+".xyzw, c"+constIndex+".zzzz";
			shaderArray[arrayIndex++] = "add "+ret+", "+ret+", " + tmp;
			shaderArray[arrayIndex++] = "add "+ret+".w, "+ret+".w, c"+constIndex+".w";
			return arrayIndex;
		}
		
		protected function calcBase(percent:Number, camera:Camera3D, exScaleX:Number, exScaleY:Number):Entity
		{
			var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;
			
			var curFrameTimer:int = this.system.getCurFrame() - this.getBeginFrame();
			if (this.preUnitFrameTimer < 0) {
				this.preUnitFrameTimer = curFrameTimer;
			}
			if (percent == 0) {
				if (prop.completelyFollow == false) {
					if (baseTransformTemp == null) {
						baseTransformTemp = new Transform3D;
					}
					
					if (basePos == null) {
						basePos = camera.pool.popVector3D();
					}
					
					baseTransformTemp.combine(camera.localToGlobalTransform, this.getFollowNode().localToCameraTransform);
					basePos.setTo(baseTransformTemp.d, baseTransformTemp.h, baseTransformTemp.l);
					baseTransformTemp.d = baseTransformTemp.h = baseTransformTemp.l = 0;
				} else if (basePos != null) {
					basePos.setTo(0,0,0);
				}
			}			
			
			this.getInterpolationValue(percent, prop.offsetKeys, offsetKey);
			this.getInterpolationValue(percent, prop.rotateKeys, rotateKey);
			this.getInterpolationValue(percent, prop.scaleKeys, scaleKey);
			
			var baseNode:Entity = camera.getRootNode();
			if (prop.completelyFollow) {
				this.x = offsetKey.items[0]; 
				this.y = offsetKey.items[1]; 
				this.z = offsetKey.items[2];
				baseNode = this.getFollowNode();
			} else if (this.basePos!=null) {
				this.x = offsetKey.items[0] + this.basePos.x;
				this.y = offsetKey.items[1] + this.basePos.y;
				this.z = offsetKey.items[2] + this.basePos.z;
			} else {
				this.x = offsetKey.items[0];
				this.y = offsetKey.items[1];
				this.z = offsetKey.items[2];
			}
			
			this.rotationX = 0; 
			this.rotationY = 0;
			this.rotationZ = 0;
			
			this.scaleX = scaleKey.items[0] * exScaleX;
			this.scaleY = scaleKey.items[1] * exScaleY;
			this.scaleZ = scaleKey.items[2];
			
			this.composeTransforms();
			this.transform.append(prop.normalTrans);
			
			var deltaFrame:Number;
			if (curFrameTimer >= preUnitFrameTimer) {
				deltaFrame = curFrameTimer - preUnitFrameTimer;
			} else {
				deltaFrame = this.getLifeFramesNum() - preUnitFrameTimer + curFrameTimer + 1;
			}
			
			preUnitFrameTimer = curFrameTimer;
						
			if (percent == 0 && this.system.loop == false) {
				this.rotateS = 0;
			}
			
			if (this.system.isPaused()) {
				var isEditor:Boolean = CONFIG::EDITOR;
				if (isEditor) {
					var curUnitFrame:Number = curFrameTimer - this.getBeginFrame();
					var scaleKeyItem:KeyPair = new KeyPair;
					scaleKeyItem.init(0);
					rotateS = 0;
					for (var p:int = 0; p < curUnitFrame; p++) {
						var percentItem:Number = p / this.getLifeFramesNum();
						this.getInterpolationValue(percentItem, prop.rotateKeys, scaleKeyItem);
						rotateS += (scaleKeyItem.items[0]/24.0);
					}
				}
				
			} else {
			
				if (percent != 0) {
					rotateS += (rotateKey.items[0]*(deltaFrame/24.0));
				}
			}
		
			return baseNode;
		}
		
		//-----------------------------------------------------------------------------------------------------		
		override FlashGE function getShaderArray(shaderArrray:Array, shaderArrayIndex:int, outValue:Object):int
		{
			var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;
			if (prop.faceType == UnitBaseProp.DIR_TYPE_PARENT && 
				prop.followType ==UnitBaseProp.DIR_TYPE_PARENT) {
				
				return this.calcParentParentShader(prop, shaderArrray, shaderArrayIndex, outValue);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_PARENT && 
				prop.followType ==UnitBaseProp.DIR_TYPE_WORLD) {
				
				return this.calcParentWorldShader(prop, shaderArrray, shaderArrayIndex, outValue);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_WORLD && 
				prop.followType ==UnitBaseProp.DIR_TYPE_PARENT) {
				
				return this.calcWorldParentShader(shaderArrray, shaderArrayIndex, outValue);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_WORLD && 
				prop.followType ==UnitBaseProp.DIR_TYPE_WORLD) {
				
				return this.calcWorldWorldShader(shaderArrray, shaderArrayIndex, outValue);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_CAMERA && 
				prop.followType ==UnitBaseProp.DIR_TYPE_PARENT) {
				
				return this.calcCameraParentShader(shaderArrray, shaderArrayIndex, outValue);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_CAMERA && 
				prop.followType ==UnitBaseProp.DIR_TYPE_WORLD) {
				
				return this.calcCameraWorldShader(shaderArrray, shaderArrayIndex, outValue);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_H_CAMERA && 
				prop.followType ==UnitBaseProp.DIR_TYPE_PARENT) {
				
				return this.calcHCameraParentShader(shaderArrray, shaderArrayIndex, outValue);
				
			} else if (prop.faceType == UnitBaseProp.DIR_TYPE_H_CAMERA && 
				prop.followType ==UnitBaseProp.DIR_TYPE_WORLD) {
				
				return this.calcHCameraWorldShader(shaderArrray, shaderArrayIndex, outValue);
			}
			
			return shaderArrayIndex;
		}
		
		//-----------------------------------------------------------------------------------------------------
		// face:hcamera follow:world
		protected function calcHCameraWorldShader(shaderArray:Array, arrayIndex:int, outValue:Object):int
		{
			var constIndex:int = 0;
			arrayIndex = this.fromAxisAngle(shaderArray, arrayIndex, constIndex, "t3", "t0", "t1", "t2");
			constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov t3, c"+constIndex;
			shaderArray[arrayIndex++] = "mul t0.xyzw, t3.xyzw, t0.xyzw"; // a, b, c, d
			shaderArray[arrayIndex++] = "mul t1.xyzw, t3.xyzw, t1.xyzw"; // e, f, g, h
			shaderArray[arrayIndex++] = "mul t2.xyzw, t3.xyzw, t2.xyzw"; // i, j, k, l
			constIndex++;
			
			//this.localToCameraTransform.append(tempTransA);	
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			
			//this.localToCameraTransform.append(camera.getRootNode().localToCameraTransform); 
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			
			//tempTrans = baseNode.localToCameraTransform;
			//localXTemp = this.x*tempTrans.a + this.y*tempTrans.b + this.z*tempTrans.c + tempTrans.d;
			//localYTemp = this.x*tempTrans.e + this.y*tempTrans.f + this.z*tempTrans.g + tempTrans.h;
			//localZTemp = this.x*tempTrans.i + this.y*tempTrans.j + this.z*tempTrans.k + tempTrans.l;
			//this.localToCameraTransform.d = localXTemp;
			//this.localToCameraTransform.h = localYTemp;
			//this.localToCameraTransform.l = localZTemp;			
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov t6, c" +constIndex;constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t0.w, t6, c" +constIndex;constIndex++;
			
			//shaderArray[arrayIndex++] = "mov t6, c[" +constIndex+ "]";constIndex++;
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t1.w, t6, c" +constIndex;constIndex++;
						
			//shaderArray[arrayIndex++] = "mov t6, c[" +constIndex+ "]";constIndex++;
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t2.w, t6, c" +constIndex;constIndex++;
			
			shaderArray[arrayIndex++] = "dp4 t0.x, a0, t0";
			shaderArray[arrayIndex++] = "dp4 t0.y, a0, t1";
			shaderArray[arrayIndex++] = "dp4 t0.z, a0, t2";
			shaderArray[arrayIndex++] = "mov t0.w, a0.w";
			outValue.constIndex = constIndex;
			return arrayIndex;
		}
		
		// face:hcamera follow:parent
		protected function calcHCameraParentShader(shaderArray:Array, arrayIndex:int, outValue:Object):int
		{
			var constIndex:int = 0;
			arrayIndex = this.fromAxisAngle(shaderArray, arrayIndex, constIndex, "t3", "t0", "t1", "t2");
			constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov t3, c"+constIndex;
			shaderArray[arrayIndex++] = "mul t0.xyzw, t3.xyzw, t0.xyzw"; // a, b, c, d
			shaderArray[arrayIndex++] = "mul t1.xyzw, t3.xyzw, t1.xyzw"; // e, f, g, h
			shaderArray[arrayIndex++] = "mul t2.xyzw, t3.xyzw, t2.xyzw"; // i, j, k, l
			constIndex++;
			
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			

			arrayIndex = composeScale(false, shaderArray, arrayIndex, constIndex, "t6", "t0", "t1", "t2");
			constIndex+=3;
			shaderArray[arrayIndex++] = "mul t3.xyzw, t6.xyzw, t3.xyzw"; // a, b, c, d
			shaderArray[arrayIndex++] = "mul t4.xyzw, t6.xyzw, t4.xyzw"; // e, f, g, h
			shaderArray[arrayIndex++] = "mul t5.xyzw, t6.xyzw, t5.xyzw"; // i, j, k, l
			
			//this.localToCameraTransform.append(camera.getRootNode().localToCameraTransform); 
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			
			//tempTrans = baseNode.localToCameraTransform;
			//localXTemp = this.x*tempTrans.a + this.y*tempTrans.b + this.z*tempTrans.c + tempTrans.d;
			//localYTemp = this.x*tempTrans.e + this.y*tempTrans.f + this.z*tempTrans.g + tempTrans.h;
			//localZTemp = this.x*tempTrans.i + this.y*tempTrans.j + this.z*tempTrans.k + tempTrans.l;
			//this.localToCameraTransform.d = localXTemp;
			//this.localToCameraTransform.h = localYTemp;
			//this.localToCameraTransform.l = localZTemp;
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov t6, c" +constIndex;constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t0.w, t6, c" +constIndex;constIndex++;
			
			//shaderArray[arrayIndex++] = "mov t6, c[" +constIndex+ "]";constIndex++;
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t1.w, t6, c" +constIndex;constIndex++;
			
			//shaderArray[arrayIndex++] = "mov t6, c[" +constIndex+ "]";constIndex++;
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t2.w, t6, c" +constIndex;constIndex++;
			
			shaderArray[arrayIndex++] = "dp4 t0.x, a0, t0";
			shaderArray[arrayIndex++] = "dp4 t0.y, a0, t1";
			shaderArray[arrayIndex++] = "dp4 t0.z, a0, t2";
			shaderArray[arrayIndex++] = "mov t0.w, a0.w";
			
			outValue.constIndex = constIndex;
			return arrayIndex;
		}// end calcHCameraParentShader
		
		// face:camera follow:world
		protected function calcCameraWorldShader(shaderArray:Array, arrayIndex:int, outValue:Object):int
		{
			var constIndex:int = 0;
			arrayIndex = this.fromAxisAngle(shaderArray, arrayIndex, constIndex, "t3", "t0", "t1", "t2");
			constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov t3, c"+constIndex;
			shaderArray[arrayIndex++] = "mul t0.xyzw, t3.xyzw, t0.xyzw"; // a, b, c, d
			shaderArray[arrayIndex++] = "mul t1.xyzw, t3.xyzw, t1.xyzw"; // e, f, g, h
			shaderArray[arrayIndex++] = "mul t2.xyzw, t3.xyzw, t2.xyzw"; // i, j, k, l
			constIndex++;
			
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov t6, c" +constIndex;constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t3.w, t6, c" +constIndex;constIndex++;
			shaderArray[arrayIndex++] = "add t0.w, t0.w, t3.w";
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t3.w, t6, c" +constIndex;constIndex++;
			shaderArray[arrayIndex++] = "add t1.w, t1.w, t3.w";
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t3.w, t6, c" +constIndex;constIndex++;
			shaderArray[arrayIndex++] = "add t2.w, t2.w, t3.w";
			
			shaderArray[arrayIndex++] = "dp4 t0.x, a0, t0";
			shaderArray[arrayIndex++] = "dp4 t0.y, a0, t1";
			shaderArray[arrayIndex++] = "dp4 t0.z, a0, t2";
			shaderArray[arrayIndex++] = "mov t0.w, a0.w";

			outValue.constIndex = constIndex;
			return arrayIndex;
		}
		
		// face:Camera follow:parent
		protected function calcCameraParentShader(shaderArray:Array, arrayIndex:int, outValue:Object):int
		{
			var constIndex:int = 0;
			arrayIndex = this.fromAxisAngle(shaderArray, arrayIndex, constIndex, "t3", "t0", "t1", "t2");
			constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov t3, c"+constIndex;
			shaderArray[arrayIndex++] = "mul t0.xyzw, t3.xyzw, t0.xyzw"; // a, b, c, d
			shaderArray[arrayIndex++] = "mul t1.xyzw, t3.xyzw, t1.xyzw"; // e, f, g, h
			shaderArray[arrayIndex++] = "mul t2.xyzw, t3.xyzw, t2.xyzw"; // i, j, k, l
			constIndex++;
			
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", constIndex, "t3", "t4", "t5");constIndex++;

			arrayIndex = composeScale(false, shaderArray, arrayIndex, constIndex, "t6", "t3", "t4", "t5");
			constIndex+=3;
			shaderArray[arrayIndex++] = "mul t0.xyzw, t6.xyzw, t0.xyzw"; // a, b, c, d
			shaderArray[arrayIndex++] = "mul t1.xyzw, t6.xyzw, t1.xyzw"; // e, f, g, h
			shaderArray[arrayIndex++] = "mul t2.xyzw, t6.xyzw, t2.xyzw"; // i, j, k, l
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov t6, c" +constIndex;constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t0.w, t6, c" +constIndex;constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t1.w, t6, c" +constIndex;constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t2.w, t6, c" +constIndex;constIndex++;
			
			shaderArray[arrayIndex++] = "dp4 t0.x, a0, t0";
			shaderArray[arrayIndex++] = "dp4 t0.y, a0, t1";
			shaderArray[arrayIndex++] = "dp4 t0.z, a0, t2";
			shaderArray[arrayIndex++] = "mov t0.w, a0.w";
			
			outValue.constIndex = constIndex;
			return arrayIndex;		
		}// end calcCameraParentShader
		
		// face:world follow:world
		protected function calcWorldWorldShader(shaderArray:Array, arrayIndex:int, outValue:Object):int
		{
			var constIndex:int = 0;
			arrayIndex = this.fromAxisAngle(shaderArray, arrayIndex, constIndex, "t3", "t0", "t1", "t2");
			constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov t3, c"+constIndex;
			shaderArray[arrayIndex++] = "mul t0.xyzw, t3.xyzw, t0.xyzw"; // a, b, c, d
			shaderArray[arrayIndex++] = "mul t1.xyzw, t3.xyzw, t1.xyzw"; // e, f, g, h
			shaderArray[arrayIndex++] = "mul t2.xyzw, t3.xyzw, t2.xyzw"; // i, j, k, l
			constIndex++;
			
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			
			tempTransB.compose(0, 0, 0, this.rotationX, this.rotationY, this.rotationZ, 1, 1, 1);

			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov t6, c" +constIndex;constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t3.w, t6, c" +constIndex;constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t4.w, t6, c" +constIndex;constIndex++;

			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t5.w, t6, c" +constIndex;constIndex++;
			
			shaderArray[arrayIndex++] = "dp4 t0.x, a0, t3";
			shaderArray[arrayIndex++] = "dp4 t0.y, a0, t4";
			shaderArray[arrayIndex++] = "dp4 t0.z, a0, t5";
			shaderArray[arrayIndex++] = "mov t0.w, a0.w";

			outValue.constIndex = constIndex;
			return arrayIndex;
		}
		
		// face:world follow:parent
		protected function calcWorldParentShader(shaderArray:Array, arrayIndex:int, outValue:Object):int
		{
			var constIndex:int = 0;
			arrayIndex = this.fromAxisAngle(shaderArray, arrayIndex, constIndex, "t3", "t0", "t1", "t2");
			constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mul t0.xyzw, t0.xyzw, c"+constIndex; // a, b, c, d
			shaderArray[arrayIndex++] = "mul t1.xyzw, t1.xyzw, c"+constIndex; // e, f, g, h
			shaderArray[arrayIndex++] = "mul t2.xyzw, t2.xyzw, c"+constIndex; // i, j, k, l
			constIndex++;
			
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", constIndex, "t3", "t4", "t5");constIndex++;

			arrayIndex = composeScale(false, shaderArray, arrayIndex, constIndex, "t6", "t3", "t4", "t5");
			constIndex+=3;
						
			shaderArray[arrayIndex++] = "mul t0.xyzw, t6.xyzw, t0.xyzw"; // a, b, c, d
			shaderArray[arrayIndex++] = "mul t1.xyzw, t6.xyzw, t1.xyzw"; // e, f, g, h
			shaderArray[arrayIndex++] = "mul t2.xyzw, t6.xyzw, t2.xyzw"; // i, j, k, l
			
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov t6, c" +constIndex; constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t3.w, t6, c" +constIndex; constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t4.w, t6, c" +constIndex; constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "dp4 t5.w, t6, c" +constIndex; constIndex++;
						
			shaderArray[arrayIndex++] = "dp4 t0.x, a0, t3";
			shaderArray[arrayIndex++] = "dp4 t0.y, a0, t4";
			shaderArray[arrayIndex++] = "dp4 t0.z, a0, t5";
			shaderArray[arrayIndex++] = "mov t0.w, a0.w";
			outValue.constIndex = constIndex;
			return arrayIndex;
		}
		
		// face:parent follow:world
		protected function calcParentWorldShader(prop:SpriteUnitProp, shaderArray:Array, arrayIndex:int, outValue:Object):int
		{
			var constIndex:int = 0;
			arrayIndex = this.fromAxisAngle(shaderArray, arrayIndex, constIndex, "t3", "t0", "t1", "t2");
			constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov t3, c"+constIndex;
			shaderArray[arrayIndex++] = "mul t0.xyzw, t3.xyzw, t0.xyzw"; // a, b, c, d
			shaderArray[arrayIndex++] = "mul t1.xyzw, t3.xyzw, t1.xyzw"; // e, f, g, h
			shaderArray[arrayIndex++] = "mul t2.xyzw, t3.xyzw, t2.xyzw"; // i, j, k, l
			constIndex++;
			
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			
			if (prop.completelyFollow==false && baseTransformTemp!=null) {
				
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", constIndex, "t3", "t4", "t5");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", constIndex, "t3", "t4", "t5");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", constIndex, "t3", "t4", "t5");constIndex++;
				
				arrayIndex = composeScale(true, shaderArray, arrayIndex, constIndex, "t6", "t3", "t4", "t5");
				constIndex+=3;
				
				shaderArray[arrayIndex++] = "mul t0.xyzw, t6.xyzw, t0.xyzw"; // a, b, c, d
				shaderArray[arrayIndex++] = "mul t1.xyzw, t6.xyzw, t1.xyzw"; // e, f, g, h
				shaderArray[arrayIndex++] = "mul t2.xyzw, t6.xyzw, t2.xyzw"; // i, j, k, l
				
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				
				shaderArray[arrayIndex++] = "dp4 t0.x, a0, t3";
				shaderArray[arrayIndex++] = "dp4 t0.y, a0, t4";
				shaderArray[arrayIndex++] = "dp4 t0.z, a0, t5";
				shaderArray[arrayIndex++] = "mov t0.w, a0.w";
				
			} else {
				
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
								
				arrayIndex = composeScale(true, shaderArray, arrayIndex, constIndex, "t6", "t0", "t1", "t2");
				constIndex+=3;
				
				shaderArray[arrayIndex++] = "mul t0.xyzw, t6.xyzw, t3.xyzw"; // a, b, c, d
				shaderArray[arrayIndex++] = "mul t1.xyzw, t6.xyzw, t4.xyzw"; // e, f, g, h
				shaderArray[arrayIndex++] = "mul t2.xyzw, t6.xyzw, t5.xyzw"; // i, j, k, l
				
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				
				shaderArray[arrayIndex++] = "dp4 t0.x, a0, t3";
				shaderArray[arrayIndex++] = "dp4 t0.y, a0, t4";
				shaderArray[arrayIndex++] = "dp4 t0.z, a0, t5";
				shaderArray[arrayIndex++] = "mov t0.w, a0.w";
			}
			outValue.constIndex = constIndex;
			return arrayIndex;
		}		
		
		// face:parent follow:parent
		protected function calcParentParentShader(prop:SpriteUnitProp, shaderArray:Array, arrayIndex:int, outValue:Object):int
		{
			var constIndex:int = 0;
			arrayIndex = this.fromAxisAngle(shaderArray, arrayIndex, constIndex, "t3", "t0", "t1", "t2");
			constIndex++;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov t3, c"+constIndex;
			shaderArray[arrayIndex++] = "mul t0.xyzw, t3.xyzw, t0.xyzw"; // a, b, c, d
			shaderArray[arrayIndex++] = "mul t1.xyzw, t3.xyzw, t1.xyzw"; // e, f, g, h
			shaderArray[arrayIndex++] = "mul t2.xyzw, t3.xyzw, t2.xyzw"; // i, j, k, l
			constIndex++;
			
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
			
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", constIndex, "t3", "t4", "t5");constIndex++;
			
			if (prop.completelyFollow==false && baseTransformTemp!=null) {
						
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
						
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", constIndex, "t3", "t4", "t5");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", constIndex, "t3", "t4", "t5");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", constIndex, "t3", "t4", "t5");constIndex++;
				
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				
				shaderArray[arrayIndex++] = "dp4 t0.x, a0, t3";
				shaderArray[arrayIndex++] = "dp4 t0.y, a0, t4";
				shaderArray[arrayIndex++] = "dp4 t0.z, a0, t5";
				shaderArray[arrayIndex++] = "mov t0.w, a0.w";
			} else {
				
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t3", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t4", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t5", "t6", constIndex, "t0", "t1", "t2");constIndex++;
				
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t0", "t6", constIndex, "t3", "t4", "t5");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t1", "t6", constIndex, "t3", "t4", "t5");constIndex++;
				arrayIndex = addMatMulMat(shaderArray, arrayIndex, "t2", "t6", constIndex, "t3", "t4", "t5");constIndex++;
				
				shaderArray[arrayIndex++] = "dp4 t0.x, a0, t0";
				shaderArray[arrayIndex++] = "dp4 t0.y, a0, t1";
				shaderArray[arrayIndex++] = "dp4 t0.z, a0, t2";
				shaderArray[arrayIndex++] = "mov t0.w, a0.w";
			}
			
			outValue.constIndex = constIndex;
			return arrayIndex;			
		}
		
		public function composeScale(rsq:Boolean, shaderArray:Array, arrayIndex:int, 
										constIndex:int, outR:String, 
										tempR0:String, tempR1:String, tempR2:String):int
		{
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov " + tempR0 + ",c" + constIndex; constIndex++; // a, b, c, d
			shaderArray[arrayIndex++] = "mul " + tempR0 + "," + tempR0 + "," + tempR0;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov " + tempR1 + ",c" + constIndex; constIndex++; // e, f, g, h
			shaderArray[arrayIndex++] = "mul " + tempR1 + "," + tempR1 + "," + tempR1;
			
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov " + tempR2 + ",c" + constIndex; constIndex++; // i, j, k, l
			shaderArray[arrayIndex++] = "mul " + tempR2 + "," + tempR2 + "," + tempR2;
			
			shaderArray[arrayIndex++] = "add " + outR + ".x," + tempR0 + ".x," + tempR1 + ".x";
			shaderArray[arrayIndex++] = "add " + outR + ".x," + outR + ".x," + tempR2 + ".x";
			
			shaderArray[arrayIndex++] = "add " + outR + ".y," + tempR0 + ".y," + tempR1 + ".y";
			shaderArray[arrayIndex++] = "add " + outR + ".y," + outR + ".y," + tempR2 + ".y";
			
			shaderArray[arrayIndex++] = "add " + outR + ".z," + tempR0 + ".z," + tempR1 + ".z";
			shaderArray[arrayIndex++] = "add " + outR + ".z," + outR + ".z," + tempR2 + ".z";
			shaderArray[arrayIndex++] = "sge " + outR + ".w," + tempR0 + ".z," + tempR1 + ".z";
			if (rsq) {
				shaderArray[arrayIndex++] = "rsq " + outR + ".xyz," + outR + ".xyz";
			}
			return arrayIndex;
		}

		public function fromAxisAngle(shaderArray:Array, arrayIndex:int, constIndex:int, tempR:String, outR0:String, outR1:String, outR2:String):int
		{
			//outR0= a, b, c, d
			//outR1= e, f, g, h
			//outR2= i, j, k, l
						
			shaderArray[arrayIndex++] = "#c"+constIndex + "=c"+constIndex;
			shaderArray[arrayIndex++] = "mov " + tempR + ", c"+constIndex;
			shaderArray[arrayIndex++] = "sin " + tempR + ".w, " + tempR + ".w";
			shaderArray[arrayIndex++] = "mul " + tempR + ".xyz, " + tempR + ".xyz " + tempR + ".w";
			
			shaderArray[arrayIndex++] = "mov " + tempR + ".w, c"+constIndex+".w";
			shaderArray[arrayIndex++] = "cos " + tempR + ".w, "+tempR + ".w";
			shaderArray[arrayIndex++] = "dp4 " + outR0 + ".x," + tempR + "," + tempR;
			shaderArray[arrayIndex++] = "rsq " + outR0 + ".x," + outR0 + ".x"; // mag
			shaderArray[arrayIndex++] = "mul " + tempR + "," + tempR + "," + outR0 + ".xxxx"; // (x, y, z, w)*=mag 
			
			shaderArray[arrayIndex++] = "mul " + outR2 + "," + tempR + "," + tempR; // outR2 = (xx, yy, zz, ww)
			
			shaderArray[arrayIndex++] = "sub " + outR0 + ".x," + outR2 + ".x," + outR2 + ".y";
			shaderArray[arrayIndex++] = "sub " + outR0 + ".x," + outR0 + ".x," + outR2 + ".z";
			shaderArray[arrayIndex++] = "add " + outR0 + ".x," + outR0 + ".x," + outR2 + ".w"; // a  r0.x
			
			shaderArray[arrayIndex++] = "sub " + outR1 + ".y," + outR2 + ".y," + outR2 + ".x";
			shaderArray[arrayIndex++] = "sub " + outR1 + ".y," + outR1 + ".y," + outR2 + ".z";
			shaderArray[arrayIndex++] = "add " + outR1 + ".y," + outR1 + ".y," + outR2 + ".w"; // f r1.y
			
			shaderArray[arrayIndex++] = "add " + outR0 + ".w," + outR2 + ".z," + outR2 + ".w";
			shaderArray[arrayIndex++] = "sub " + outR0 + ".w," + outR0 + ".w," + outR2 + ".x";
			shaderArray[arrayIndex++] = "add " + outR0 + ".w," + outR0 + ".w," + outR2 + ".y"; // k 临时存储  r0.w
			
			shaderArray[arrayIndex++] = "mul " + outR2 + "," + tempR + ".xzxy," + tempR + ".ywzw";
			shaderArray[arrayIndex++] = "add " + outR2 + "," + outR2 + "," + outR2; // outR2 = (xy2,zw2,xz2,yw2)
			
			shaderArray[arrayIndex++] = "add " + outR0 + ".z," + outR2 + ".z," + outR2 + ".w"; // c r0.z
			shaderArray[arrayIndex++] = "add " + outR1 + ".x," + outR2 + ".x," + outR2 + ".y"; // e r1.x
			shaderArray[arrayIndex++] = "sub " + outR0 + ".y," + outR2 + ".x," + outR2 + ".y"; // b r0.y
			shaderArray[arrayIndex++] = "sub " + outR2 + ".x," + outR2 + ".z," + outR2 + ".w"; // i r2.x
			
			shaderArray[arrayIndex++] = "mul " + tempR + ".xy," + tempR + ".xy," + tempR + ".wz";
			shaderArray[arrayIndex++] = "add " + tempR + ".xy," + tempR + ".xy," + tempR + ".xy"; // outR2 = (xw2,yz2)
			
			shaderArray[arrayIndex++] = "add " + outR2 + ".y," + tempR + ".x," + tempR + ".y"; // j
			shaderArray[arrayIndex++] = "sub " + outR1 + ".z," + tempR + ".y," + tempR + ".x"; // g
			shaderArray[arrayIndex++] = "mov " + outR2 + ".z," + outR0 + ".w";
			shaderArray[arrayIndex++] = "slt " + outR0 + ".w," + outR0 + ".w," + outR0 + ".w";
			shaderArray[arrayIndex++] = "slt " + outR1 + ".w," + outR0 + ".w," + outR0 + ".w";
			shaderArray[arrayIndex++] = "slt " + outR2 + ".w," + outR0 + ".w," + outR0 + ".w";
			return arrayIndex;
		}
		
		//-----------------------------------------------------------------------------------------------------
		// face:hcamera follow:world
		protected function calcHCameraWorld(renderUnit:RenderUnit, constIndex:int,
											 percent:Number, camera:Camera3D, exScaleX:Number, exScaleY:Number):int
		{
			var baseNode:Entity = this.calcBase(percent, camera, exScaleX, exScaleY);
			var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;			
			var localXTemp:Number = 0, localYTemp:Number = 0, localZTemp:Number = 0;
			var scaleXTemp:Number = 0, scaleYTemp:Number = 0, scaleZTemp:Number = 0;
			var tempTrans:Transform3D;
			
			//rotation
			var rot:Vector3D = camera.pool.popVector3D();
			var parent:Entity = camera.getParent();
			rot.setTo(parent.rotationX, parent.rotationY, parent.rotationZ);
			
			var tranCamera:Transform3D = camera.pool.popTransform3D();
			tranCamera.fromEulerAngles(rot.x, rot.y, rot.z);
			camera.pool.pushVector3D(rot);
			
			var camX:Number = camera.x*tranCamera.a + camera.y*tranCamera.b + camera.z*tranCamera.c + tranCamera.d;
			var camY:Number = camera.x*tranCamera.e + camera.y*tranCamera.f + camera.z*tranCamera.g + tranCamera.h;
			var camZ:Number = camera.x*tranCamera.i + camera.y*tranCamera.j + camera.z*tranCamera.k + tranCamera.l;
			camera.pool.pushTransform3D(tranCamera);
			
			var tranTemp:Transform3D = camera.pool.popTransform3D();
			var dirA:Vector3D = camera.pool.popVector3D();
			dirA.x = dirA.y = 0; dirA.z = 1;
			var dirB:Vector3D = camera.pool.popVector3D();
			dirB.x = camX-this.x;
			dirB.y = camY-this.y;
			dirB.z = 0;
			dirB.normalize();
			
			var selfRot:Number = Math.atan(dirB.y/dirB.x)+prop.selfOriginRotate;
			if (dirB.x < 0)
				selfRot = Math.PI+selfRot;
			tempTransB.fromEulerAngles(0, 0, selfRot);
			
			if (dirB.x ==0 && dirB.y==0 && dirB.z==1) {
				tranTemp.fromAxisAngle(0, -1, 0, 0);
			} else {
				var dirN:Vector3D = dirA.crossProduct(dirB);
				dirN.normalize();
				var dot:Number = dirA.dotProduct(dirB);
				tranTemp.fromAxisAngle(dirN.x, dirN.y, dirN.z, Math.acos(dot));
			}
			tempTransA.combine(tranTemp, tempTransB);
			camera.pool.pushTransform3D(tranTemp);
			camera.pool.pushVector3D(dirA);
			camera.pool.pushVector3D(dirB);
			
			renderUnit.setVertexConstantsFromNumbers(constIndex, prop.selfRotateAxis.x, prop.selfRotateAxis.y, prop.selfRotateAxis.z, rotateS*0.5);
			constIndex++;
			
			renderUnit.setVertexConstantsFromNumbers(constIndex, this.scaleX, this.scaleY, this.scaleZ, 1);
			constIndex++;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, tempTransA);
			constIndex+=3;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, camera.getRootNode().localToCameraTransform);
			constIndex+=3;
						
			renderUnit.setVertexConstantsFromNumbers(constIndex, this.x, this.y, this.z, 1);
			constIndex++;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, baseNode.localToCameraTransform);
			constIndex+=3;
			
			return constIndex;
		}
		
		// face:hcamera follow:parent
		protected function calcHCameraParent(renderUnit:RenderUnit, constIndex:int,
											 percent:Number, camera:Camera3D, exScaleX:Number, exScaleY:Number):int
		{
			var baseNode:Entity = this.calcBase(percent, camera, exScaleX, exScaleY);
			var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;
			
			var cameraX:Number, cameraY:Number, cameraZ:Number;
			
			var localXTemp:Number = 0, localYTemp:Number = 0, localZTemp:Number = 0;
			var scaleXTemp:Number = 0, scaleYTemp:Number = 0, scaleZTemp:Number = 0;
			var tempTrans:Transform3D;
			
			var rot:Vector3D = camera.pool.popVector3D();
			var parent:Entity = camera.getParent();
			rot.setTo(parent.rotationX, parent.rotationY, parent.rotationZ);
			
			var tranCamera:Transform3D = camera.pool.popTransform3D();
			tranCamera.fromEulerAngles(rot.x, rot.y, rot.z);
			camera.pool.pushVector3D(rot);
			
			var camX:Number = camera.x*tranCamera.a + camera.y*tranCamera.b + camera.z*tranCamera.c + tranCamera.d;
			var camY:Number = camera.x*tranCamera.e + camera.y*tranCamera.f + camera.z*tranCamera.g + tranCamera.h;
			var camZ:Number = camera.x*tranCamera.i + camera.y*tranCamera.j + camera.z*tranCamera.k + tranCamera.l;
			camera.pool.pushTransform3D(tranCamera);
			
			var tranTemp:Transform3D = camera.pool.popTransform3D();
			var dirA:Vector3D = camera.pool.popVector3D();
			dirA.x = dirA.y = 0; dirA.z = 1;
			var dirB:Vector3D = camera.pool.popVector3D();
			dirB.x = camX-this.x;
			dirB.y = camY-this.y;
			dirB.z = 0;
			dirB.normalize();
			
			var selfRot:Number = Math.atan(dirB.y/dirB.x)+prop.selfOriginRotate;
			if (dirB.x < 0)
				selfRot = Math.PI+selfRot;
			tempTransB.fromEulerAngles(0, 0, selfRot);
			
			if (dirB.x ==0 && dirB.y==0 && dirB.z==1) {
				tranTemp.fromAxisAngle(0, -1, 0, 0);
			} else {
				var dirN:Vector3D = dirA.crossProduct(dirB);
				dirN.normalize();
				var dot:Number = dirA.dotProduct(dirB);
				tranTemp.fromAxisAngle(dirN.x, dirN.y, dirN.z, Math.acos(dot));
			}
			tempTransA.combine(tranTemp, tempTransB);
			camera.pool.pushTransform3D(tranTemp);
			camera.pool.pushVector3D(dirA);
			camera.pool.pushVector3D(dirB);
			
			renderUnit.setVertexConstantsFromNumbers(constIndex, prop.selfRotateAxis.x, prop.selfRotateAxis.y, prop.selfRotateAxis.z, rotateS*0.5);
			constIndex++;
			
			renderUnit.setVertexConstantsFromNumbers(constIndex, this.scaleX, this.scaleY, this.scaleZ, 1);
			constIndex++;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, tempTransA);
			constIndex+=3;
			
			tempTrans = baseNode.localToCameraTransform;

			renderUnit.setVertexConstantsFromTransform(constIndex, baseNode.localToCameraTransform);
			constIndex+=3;			
			
			renderUnit.setVertexConstantsFromTransform(constIndex, camera.getRootNode().localToCameraTransform);
			constIndex+=3;
			
			renderUnit.setVertexConstantsFromNumbers(constIndex, this.x, this.y, this.z, 1);
			constIndex++;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, baseNode.localToCameraTransform);
			constIndex+=3;
			
			return constIndex;
		}
		
		// face:camera follow:world
		protected function calcCameraWorld(renderUnit:RenderUnit, constIndex:int,
										   percent:Number, camera:Camera3D, exScaleX:Number, exScaleY:Number):int
		{
			var baseNode:Entity = this.calcBase(percent, camera, exScaleX, exScaleY);
			var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;			

			renderUnit.setVertexConstantsFromNumbers(constIndex, prop.selfRotateAxis.x, prop.selfRotateAxis.y, prop.selfRotateAxis.z,(prop.selfOriginRotate + rotateS)*0.5);
			constIndex++;
			
			renderUnit.setVertexConstantsFromNumbers(constIndex,this.scaleX, this.scaleY, this.scaleZ, 1);
			constIndex++;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, prop.normalTrans);
			constIndex+=3;
			
			tempTransB.compose(0, 0, 0, this.rotationX+Math.PI, this.rotationY, this.rotationZ, 1, 1, 1);

			renderUnit.setVertexConstantsFromTransform(constIndex, tempTransB);
			constIndex+=3;
					
			renderUnit.setVertexConstantsFromNumbers(constIndex, this.x, this.y, this.z, 1);
			constIndex++;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, baseNode.localToCameraTransform);
			constIndex+=3;

			return constIndex;
		}
		
		// face:Camera follow:parent
		protected function calcCameraParent(renderUnit:RenderUnit, constIndex:int,
											percent:Number, camera:Camera3D, exScaleX:Number, exScaleY:Number):int
		{
			var baseNode:Entity = this.calcBase(percent, camera, exScaleX, exScaleY);
			var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;

			renderUnit.setVertexConstantsFromNumbers(constIndex, prop.selfRotateAxis.x, prop.selfRotateAxis.y, prop.selfRotateAxis.z,(prop.selfOriginRotate + rotateS)*0.5);
			constIndex++;
			renderUnit.setVertexConstantsFromNumbers(constIndex,this.scaleX, this.scaleY, this.scaleZ, 1);
			constIndex++;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, prop.normalTrans);
			constIndex+=3;
			
			tempTransB.compose(0, 0, 0, this.rotationX+Math.PI, this.rotationY, this.rotationZ, 1, 1, 1);
			renderUnit.setVertexConstantsFromTransform(constIndex, tempTransB);
			constIndex+=3;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, baseNode.localToCameraTransform);
			constIndex+=3;
			
			renderUnit.setVertexConstantsFromNumbers(constIndex, this.x, this.y, this.z, 1);
			constIndex++;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, baseNode.localToCameraTransform);
			constIndex+=3;
	
			return constIndex;
		}
		
		// face:world follow:world
		protected function calcWorldWorld(renderUnit:RenderUnit, constIndex:int, 
										  percent:Number, camera:Camera3D, exScaleX:Number, exScaleY:Number):int
		{
			var baseNode:Entity = this.calcBase(percent, camera, exScaleX, exScaleY);
			var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;
			
			renderUnit.setVertexConstantsFromNumbers(constIndex, prop.selfRotateAxis.x, prop.selfRotateAxis.y, prop.selfRotateAxis.z,(prop.selfOriginRotate + rotateS)*0.5);
			constIndex++;
			
			renderUnit.setVertexConstantsFromNumbers(constIndex,this.scaleX, this.scaleY, this.scaleZ, 1);
			constIndex++;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, prop.normalTrans);
			constIndex+=3;
			
			tempTransB.compose(0, 0, 0, this.rotationX, this.rotationY, this.rotationZ, 1, 1, 1);
			renderUnit.setVertexConstantsFromTransform(constIndex, tempTransB);
			constIndex+=3;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, camera.getRootNode().localToCameraTransform);
			constIndex+=3;
			
			renderUnit.setVertexConstantsFromNumbers(constIndex, this.x, this.y, this.z, 1);
			constIndex+=1;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, baseNode.localToCameraTransform);
			constIndex+=3;
			
			return constIndex;
		}
		
		// face:world follow:parent
		protected function calcWorldParent(renderUnit:RenderUnit, constIndex:int, 
										   percent:Number, camera:Camera3D, exScaleX:Number, exScaleY:Number):int
		{
			var baseNode:Entity = this.calcBase(percent, camera, exScaleX, exScaleY);
			var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;

			renderUnit.setVertexConstantsFromNumbers(constIndex, prop.selfRotateAxis.x, prop.selfRotateAxis.y, prop.selfRotateAxis.z,(prop.selfOriginRotate + rotateS)*0.5);
			constIndex++;
			
			renderUnit.setVertexConstantsFromNumbers(constIndex,this.scaleX, this.scaleY, this.scaleZ, 1);
			constIndex++;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, prop.normalTrans);
			constIndex+=3;
			
			tempTransB.compose(0, 0, 0, this.rotationX, this.rotationY, this.rotationZ, 1, 1, 1);
			renderUnit.setVertexConstantsFromTransform(constIndex, tempTransB);
			constIndex+=3;	
			
			renderUnit.setVertexConstantsFromTransform(constIndex, baseNode.localToCameraTransform);
			constIndex+=3;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, camera.getRootNode().localToCameraTransform); 
			constIndex+=3;
						
			renderUnit.setVertexConstantsFromNumbers(constIndex, this.x, this.y, this.z, 1);
			constIndex+=1;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, baseNode.localToCameraTransform);
			constIndex+=3;

			return constIndex;
		} // end calcWorldParent
		
		// face:parent follow:world
		protected function calcParentWorld(renderUnit:RenderUnit, constIndex:int, 
										   percent:Number, 
										   camera:Camera3D, 
										   exScaleX:Number, exScaleY:Number):int
		{
			var baseNode:Entity = this.calcBase(percent, camera, exScaleX, exScaleY);
			var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;
			
			renderUnit.setVertexConstantsFromNumbers(constIndex, prop.selfRotateAxis.x, prop.selfRotateAxis.y, prop.selfRotateAxis.z,(prop.selfOriginRotate + rotateS)*0.5);
			constIndex++;
			
			renderUnit.setVertexConstantsFromNumbers(constIndex,this.scaleX, this.scaleY, this.scaleZ, 1);
			constIndex++;

			renderUnit.setVertexConstantsFromTransform(constIndex, prop.normalTrans);
			constIndex+=3;
			
			tempTransB.compose(0, 0, 0, this.rotationX, this.rotationY, this.rotationZ, 1, 1, 1);

			renderUnit.setVertexConstantsFromTransform(constIndex, tempTransB);
			constIndex+=3;
			
			if (prop.completelyFollow == false && baseTransformTemp != null) {
				
				renderUnit.setVertexConstantsFromTransform(constIndex, baseTransformTemp);
				constIndex+=3;
				
				tempTransB.compose(this.x, this.y, this.z, 0, 0, 0, 1, 1, 1);
				renderUnit.setVertexConstantsFromTransform(constIndex, tempTransB);
				constIndex+=3;
								
				renderUnit.setVertexConstantsFromTransform(constIndex, baseNode.localToCameraTransform);
				constIndex+=3;

				renderUnit.setVertexConstantsFromTransform(constIndex, baseNode.localToCameraTransform);
				constIndex+=3;
			} else {
				
				tempTransB.compose(this.x, this.y, this.z, 0, 0, 0, 1, 1, 1);

				renderUnit.setVertexConstantsFromTransform(constIndex, tempTransB);
				constIndex+=3;
				
				renderUnit.setVertexConstantsFromTransform(constIndex, baseNode.localToCameraTransform);
				constIndex+=3;
				
				renderUnit.setVertexConstantsFromTransform(constIndex, baseNode.localToCameraTransform);
				constIndex+=3;
			}
			
			return constIndex;
		}
		
		// face:parent follow:parent
		protected function calcParentParent(renderUnit:RenderUnit, constIndex:int, percent:Number, camera:Camera3D, exScaleX:Number, exScaleY:Number):int
		{
			var baseNode:Entity = this.calcBase(percent, camera, exScaleX, exScaleY);
			var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;
			
			renderUnit.setVertexConstantsFromNumbers(constIndex, prop.selfRotateAxis.x, prop.selfRotateAxis.y, prop.selfRotateAxis.z,(prop.selfOriginRotate + rotateS)*0.5);
			constIndex++;
			
			renderUnit.setVertexConstantsFromNumbers(constIndex,this.scaleX, this.scaleY, this.scaleZ, 1);
			constIndex++;
			
			renderUnit.setVertexConstantsFromTransform(constIndex, prop.normalTrans);
			constIndex+=3;

			tempTransB.compose(0, 0, 0, this.rotationX, this.rotationY, this.rotationZ, 1, 1, 1);

			renderUnit.setVertexConstantsFromTransform(constIndex, tempTransB);
			constIndex+=3;

			if (prop.completelyFollow==false && baseTransformTemp!=null) {

				renderUnit.setVertexConstantsFromTransform(constIndex, baseTransformTemp);
				constIndex+=3;
				
				tempTransB.compose(this.x, this.y, this.z, 0, 0, 0, 1, 1, 1);

				renderUnit.setVertexConstantsFromTransform(constIndex, tempTransB);
				constIndex+=3;
				
				renderUnit.setVertexConstantsFromTransform(constIndex, baseNode.localToCameraTransform);
				constIndex+=3;
			} else {
			
				tempTransB.compose(this.x, this.y, this.z, 0, 0, 0, 1, 1, 1);

				renderUnit.setVertexConstantsFromTransform(constIndex, tempTransB);
				constIndex+=3;
				
				renderUnit.setVertexConstantsFromTransform(constIndex, baseNode.localToCameraTransform);
				constIndex+=3;
			}
			
			return constIndex;
		}
		
		
		protected var preRotateTimerS:int = -1;
		protected function calcTransforms(percent:Number, camera:Camera3D, exScaleX:Number, exScaleY:Number):void
		{
			var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;
			var curFrameTimer:int = this.system.getCurFrame() - this.getBeginFrame();
			
			if (this.preUnitFrameTimer < 0) {
				this.preUnitFrameTimer = curFrameTimer;
				if (prop.completelyFollow == false) {
					if (baseTransformTemp == null) {
						baseTransformTemp = new Transform3D;
					}
					
					if (basePos == null) {
						basePos = camera.pool.popVector3D();
					}
					
					baseTransformTemp.combine(camera.localToGlobalTransform, this.getFollowNode().localToCameraTransform);
					basePos.setTo(baseTransformTemp.d, baseTransformTemp.h, baseTransformTemp.l);
					baseTransformTemp.d = baseTransformTemp.h = baseTransformTemp.l = 0;
				}
			}
			
			
			this.getInterpolationValue(percent, prop.offsetKeys, offsetKey);
			this.getInterpolationValue(percent, prop.rotateKeys, rotateKey);
			this.getInterpolationValue(percent, prop.scaleKeys, scaleKey);
			
			var baseNode:Entity = camera.getRootNode();
			if (prop.completelyFollow) {
				this.x = offsetKey.items[0]; 
				this.y = offsetKey.items[1]; 
				this.z = offsetKey.items[2];
				baseNode = this.getFollowNode();
			} else if (this.basePos!=null) {
				this.x = offsetKey.items[0] + this.basePos.x;
				this.y = offsetKey.items[1] + this.basePos.y;
				this.z = offsetKey.items[2] + this.basePos.z;
			} else {
				this.x = offsetKey.items[0];
				this.y = offsetKey.items[1];
				this.z = offsetKey.items[2];
			}
			
			this.rotationX = 0; 
			this.rotationY = 0;
			this.rotationZ = 0;
			
			this.scaleX = scaleKey.items[0] * exScaleX;
			this.scaleY = scaleKey.items[1] * exScaleY;
			this.scaleZ = scaleKey.items[2];
			
			this.composeTransforms();
			this.transform.append(prop.normalTrans);
			
			var deltaFrame:Number;
			if (curFrameTimer > preUnitFrameTimer) {
				deltaFrame = curFrameTimer - preUnitFrameTimer;
			} else {
				deltaFrame = this.getLifeFramesNum() - preUnitFrameTimer + curFrameTimer + 1;
			}
			
			preUnitFrameTimer = curFrameTimer;
			
			if (percent == 0 && this.system.loop == false) {
				this.rotateS = 0;
			}
			
			if (this.system.isPaused()) {
				var isEditor:Boolean = CONFIG::EDITOR;
				if (isEditor) {
					var curUnitFrame:Number = curFrameTimer - this.getBeginFrame();
					var scaleKeyItem:KeyPair = new KeyPair;
					scaleKeyItem.init(0);
					rotateS = 0;
					for (var p:int = 0; p < curUnitFrame; p++) {
						var percentItem:Number = p / this.getLifeFramesNum();
						this.getInterpolationValue(percentItem, prop.rotateKeys, scaleKeyItem);
						rotateS += (scaleKeyItem.items[0]/24.0);
					}
				}
				
			} else {
				
				if (percent != 0) {
					var curTimerS:int = getTimer();
					if (preRotateTimerS == -1) {
						preRotateTimerS = curTimerS;
					}
					rotateS += (rotateKey.items[0]*(preRotateTimerS - curTimerS)/1000.0);
					preRotateTimerS = curTimerS;
				}
			}	
			
			
			var cameraX:Number, cameraY:Number, cameraZ:Number;
			
			var localXTemp:Number = 0, localYTemp:Number = 0, localZTemp:Number = 0;
			var scaleXTemp:Number = 0, scaleYTemp:Number = 0, scaleZTemp:Number = 0;
			var tempTrans:Transform3D;
			
			var selfTrans:Transform3D = camera.pool.popTransform3D();
			if (prop.faceType == UnitBaseProp.DIR_TYPE_PARENT || 
				prop.faceType == UnitBaseProp.DIR_TYPE_WORLD ||
				prop.faceType == UnitBaseProp.DIR_TYPE_CAMERA) {
				
				selfTrans.compose(0,0,0,0,0,0,this.scaleX, this.scaleY, this.scaleZ);
				tempTransB.fromAxisAngle(prop.selfRotateAxis.x, prop.selfRotateAxis.y, 
					prop.selfRotateAxis.z,prop.selfOriginRotate + rotateS);
				selfTrans.append(tempTransB);	
				selfTrans.append(prop.normalTrans);
				if (prop.faceType == UnitBaseProp.DIR_TYPE_CAMERA) {
					tempTransB.compose(0, 0, 0, this.rotationX+Math.PI, this.rotationY, this.rotationZ, 1, 1, 1);
				} else {
					tempTransB.compose(0, 0, 0, this.rotationX, this.rotationY, this.rotationZ, 1, 1, 1);
				}
				selfTrans.append(tempTransB);					
				
			} else if(prop.faceType == UnitBaseProp.DIR_TYPE_H_CAMERA) {
				
				//rotation
				var rot:Vector3D = camera.pool.popVector3D();
				var parent:Entity = camera.getParent();
				rot.setTo(parent.rotationX, parent.rotationY, parent.rotationZ);
				
				var tranCamera:Transform3D = camera.pool.popTransform3D();
				tranCamera.fromEulerAngles(rot.x, rot.y, rot.z);
				camera.pool.pushVector3D(rot);
				
				var camX:Number = camera.x*tranCamera.a + camera.y*tranCamera.b + camera.z*tranCamera.c + tranCamera.d;
				var camY:Number = camera.x*tranCamera.e + camera.y*tranCamera.f + camera.z*tranCamera.g + tranCamera.h;
				var camZ:Number = camera.x*tranCamera.i + camera.y*tranCamera.j + camera.z*tranCamera.k + tranCamera.l;
				camera.pool.pushTransform3D(tranCamera);
				
				var tranTemp:Transform3D = camera.pool.popTransform3D();
				var dirA:Vector3D = camera.pool.popVector3D();
				dirA.x = dirA.y = 0; dirA.z = 1;
				var dirB:Vector3D = camera.pool.popVector3D();
				dirB.x = camX-this.x;
				dirB.y = camY-this.y;
				dirB.z = 0;
				dirB.normalize();
				
				var selfRot:Number = Math.atan(dirB.y/dirB.x)+prop.selfOriginRotate;
				if (dirB.x < 0)
					selfRot = Math.PI+selfRot;
				tempTransB.fromEulerAngles(0, 0, selfRot);
				
				if (dirB.x ==0 && dirB.y==0 && dirB.z==1) {
					tranTemp.fromAxisAngle(0, -1, 0, 0);
				} else {
					var dirN:Vector3D = dirA.crossProduct(dirB);
					dirN.normalize();
					var dot:Number = dirA.dotProduct(dirB);
					tranTemp.fromAxisAngle(dirN.x, dirN.y, dirN.z, Math.acos(dot));
				}
				tempTransA.combine(tranTemp, tempTransB);
				camera.pool.pushTransform3D(tranTemp);
				camera.pool.pushVector3D(dirA);
				camera.pool.pushVector3D(dirB);
				
				selfTrans.compose(0,0,0,0,0,0,this.scaleX, this.scaleY, this.scaleZ);
				tempTransB.fromAxisAngle(
					prop.selfRotateAxis.x, 
					prop.selfRotateAxis.y, 
					prop.selfRotateAxis.z, rotateS);
				selfTrans.append(tempTransB);
				selfTrans.append(tempTransA);
			}			
			
			if (prop.followType == UnitBaseProp.DIR_TYPE_PARENT) {
				if( prop.faceType == UnitBaseProp.DIR_TYPE_PARENT) {
					
					if (prop.completelyFollow==false && baseTransformTemp!=null) {
						selfTrans.append(baseTransformTemp);
					}
					tempTransB.compose(this.x, this.y, this.z, 0, 0, 0, 1, 1, 1);
					selfTrans.append(tempTransB);
					
					selfTrans.append(baseNode.localToCameraTransform);
					this.localToCameraTransform.copy(selfTrans);
					
				} else if (prop.faceType == UnitBaseProp.DIR_TYPE_WORLD) {
					
					tempTrans = baseNode.localToCameraTransform;
					localXTemp = this.x*tempTrans.a + this.y*tempTrans.b + this.z*tempTrans.c + tempTrans.d;
					localYTemp = this.x*tempTrans.e + this.y*tempTrans.f + this.z*tempTrans.g + tempTrans.h;
					localZTemp = this.x*tempTrans.i + this.y*tempTrans.j + this.z*tempTrans.k + tempTrans.l;
					
					scaleXTemp = Math.sqrt(tempTrans.a*tempTrans.a + tempTrans.e*tempTrans.e + tempTrans.i*tempTrans.i);
					scaleYTemp = Math.sqrt(tempTrans.b*tempTrans.b + tempTrans.f*tempTrans.f + tempTrans.j*tempTrans.j);
					scaleZTemp = Math.sqrt(tempTrans.c*tempTrans.c + tempTrans.g*tempTrans.g + tempTrans.k*tempTrans.k);
					
					this.localToCameraTransform.compose(0,0,0,0,0,0,scaleXTemp, scaleYTemp, scaleZTemp);
					this.localToCameraTransform.append(selfTrans); 
					this.localToCameraTransform.append(camera.getRootNode().localToCameraTransform); 
					this.localToCameraTransform.d = localXTemp;
					this.localToCameraTransform.h = localYTemp;
					this.localToCameraTransform.l = localZTemp;
					
				} else if (prop.faceType == UnitBaseProp.DIR_TYPE_CAMERA) {
					tempTrans = baseNode.localToCameraTransform;
					
					cameraX = this.x*tempTrans.a + this.y*tempTrans.b + this.z*tempTrans.c + tempTrans.d;
					cameraY = this.x*tempTrans.e + this.y*tempTrans.f + this.z*tempTrans.g + tempTrans.h;
					cameraZ = this.x*tempTrans.i + this.y*tempTrans.j + this.z*tempTrans.k + tempTrans.l;
					
					scaleXTemp = Math.sqrt(tempTrans.a*tempTrans.a + tempTrans.e*tempTrans.e + tempTrans.i*tempTrans.i);
					scaleYTemp = Math.sqrt(tempTrans.b*tempTrans.b + tempTrans.f*tempTrans.f + tempTrans.j*tempTrans.j);
					scaleZTemp = Math.sqrt(tempTrans.c*tempTrans.c + tempTrans.g*tempTrans.g + tempTrans.k*tempTrans.k);
					
					this.localToCameraTransform.copy(selfTrans);
					
					tempTransB.compose(0,0,0,0,0,0,scaleXTemp, scaleYTemp, scaleZTemp);
					this.localToCameraTransform.append(tempTransB);
					this.localToCameraTransform.d = cameraX;
					this.localToCameraTransform.h = cameraY;
					this.localToCameraTransform.l = cameraZ;
					
				} else if (prop.faceType == UnitBaseProp.DIR_TYPE_H_CAMERA) {
					tempTrans = baseNode.localToCameraTransform;
					localXTemp = this.x*tempTrans.a + this.y*tempTrans.b + this.z*tempTrans.c + tempTrans.d;
					localYTemp = this.x*tempTrans.e + this.y*tempTrans.f + this.z*tempTrans.g + tempTrans.h;
					localZTemp = this.x*tempTrans.i + this.y*tempTrans.j + this.z*tempTrans.k + tempTrans.l;
					
					scaleXTemp = Math.sqrt(tempTrans.a*tempTrans.a + tempTrans.e*tempTrans.e + tempTrans.i*tempTrans.i);
					scaleYTemp = Math.sqrt(tempTrans.b*tempTrans.b + tempTrans.f*tempTrans.f + tempTrans.j*tempTrans.j);
					scaleZTemp = Math.sqrt(tempTrans.c*tempTrans.c + tempTrans.g*tempTrans.g + tempTrans.k*tempTrans.k);
					
					this.localToCameraTransform.compose(0,0,0,0,0,0,scaleXTemp, scaleYTemp, scaleZTemp);
					this.localToCameraTransform.append(selfTrans);
					this.localToCameraTransform.append(camera.getRootNode().localToCameraTransform); 
					this.localToCameraTransform.d = localXTemp;
					this.localToCameraTransform.h = localYTemp;
					this.localToCameraTransform.l = localZTemp;
				}
				
			} else if (prop.followType == UnitBaseProp.DIR_TYPE_WORLD) {
				
				if( prop.faceType == UnitBaseProp.DIR_TYPE_PARENT) {
					
					if (prop.completelyFollow==false && baseTransformTemp!=null) {
						selfTrans.append(baseTransformTemp);
					}
					tempTransB.compose(this.x, this.y, this.z, 0, 0, 0, 1, 1, 1);
					selfTrans.append(tempTransB);
					
					tempTrans = baseNode.localToCameraTransform;
					scaleXTemp = Math.sqrt(tempTrans.a*tempTrans.a + tempTrans.e*tempTrans.e + tempTrans.i*tempTrans.i);
					scaleYTemp = Math.sqrt(tempTrans.b*tempTrans.b + tempTrans.f*tempTrans.f + tempTrans.j*tempTrans.j);
					scaleZTemp = Math.sqrt(tempTrans.c*tempTrans.c + tempTrans.g*tempTrans.g + tempTrans.k*tempTrans.k);
					tempTransB.compose(0,0,0,0,0,0,1/scaleXTemp, 1/scaleYTemp, 1/scaleZTemp);
					selfTrans.append(tempTransB);
					selfTrans.append(baseNode.localToCameraTransform);
					this.localToCameraTransform.copy(selfTrans);
					
				} else if (prop.faceType == UnitBaseProp.DIR_TYPE_WORLD) {
					tempTrans = baseNode.localToCameraTransform;
					localXTemp = this.x*tempTrans.a + this.y*tempTrans.b + this.z*tempTrans.c + tempTrans.d;
					localYTemp = this.x*tempTrans.e + this.y*tempTrans.f + this.z*tempTrans.g + tempTrans.h;
					localZTemp = this.x*tempTrans.i + this.y*tempTrans.j + this.z*tempTrans.k + tempTrans.l;						
					
					this.localToCameraTransform.copy(selfTrans); 
					this.localToCameraTransform.append(camera.getRootNode().localToCameraTransform); 
					this.localToCameraTransform.d = localXTemp;
					this.localToCameraTransform.h = localYTemp;
					this.localToCameraTransform.l = localZTemp;
				} else if (prop.faceType == UnitBaseProp.DIR_TYPE_CAMERA) {
					tempTrans = baseNode.localToCameraTransform;
					
					cameraX = this.x*tempTrans.a + this.y*tempTrans.b + this.z*tempTrans.c + tempTrans.d;
					cameraY = this.x*tempTrans.e + this.y*tempTrans.f + this.z*tempTrans.g + tempTrans.h;
					cameraZ = this.x*tempTrans.i + this.y*tempTrans.j + this.z*tempTrans.k + tempTrans.l;
					
					this.localToCameraTransform.copy(selfTrans);
					tempTransB.compose(cameraX,cameraY,cameraZ,0,0,0,1, 1, 1);
					this.localToCameraTransform.append(tempTransB);
					
				} else if (prop.faceType == UnitBaseProp.DIR_TYPE_H_CAMERA) {
					tempTrans = baseNode.localToCameraTransform;
					localXTemp = this.x*tempTrans.a + this.y*tempTrans.b + this.z*tempTrans.c + tempTrans.d;
					localYTemp = this.x*tempTrans.e + this.y*tempTrans.f + this.z*tempTrans.g + tempTrans.h;
					localZTemp = this.x*tempTrans.i + this.y*tempTrans.j + this.z*tempTrans.k + tempTrans.l;
					
					this.localToCameraTransform.copy(selfTrans);
					this.localToCameraTransform.append(camera.getRootNode().localToCameraTransform); 
					this.localToCameraTransform.d = localXTemp;
					this.localToCameraTransform.h = localYTemp;
					this.localToCameraTransform.l = localZTemp;
				}
			}
			
			camera.pool.pushTransform3D(selfTrans);
			
			if (percent >=1 && this.system.loop == false) {
				this.rotateS = 0;
				this.preUnitFrameTimer = -1;
			}
		}

	}
}