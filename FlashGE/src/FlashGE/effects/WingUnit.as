package FlashGE.effects
{
	import flash.geom.Vector3D;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.SpriteUnitProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.effects.unitProp.WingUnitProp;
	import FlashGE.materials.Material;
	import FlashGE.objects.Entity;
	import FlashGE.objects.Model;
	import FlashGE.objects.RenderObject;
	import FlashGE.render.Camera3D;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;
	
	public class WingUnit extends SpriteUnit
	{
		private var renderObj:RenderObject;		
		private var needSync:Boolean;
		private var startPlayTimer:int = -1;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_WING;
		}
		
		override public function contructor(unitProp:UnitBaseProp = null):void
		{
			needSync = true;
			if (unitProp == null) {
				var newProp:WingUnitProp = new WingUnitProp;
				newProp.contructor();
				unitProp = newProp;
				this.setProp(newProp);
			} else {
				this.setProp(unitProp);
			}
			super.contructor(unitProp);
			this.syncParamsToMaterial();
		}
		
		override public function dispose():void
		{
			super.dispose();
			if (this.renderObj != null) {
				this.renderObj.dispose();
				this.renderObj = null;
			}
		}
		
		override public function initGeometry(width:Number = 100, length:Number = 100):void
		{
		}
		
		override public function setProp(unitProp:UnitBaseProp):void
		{
			if (this.unitProp == unitProp) return;
			
			this.unitProp = unitProp;
			var prop:WingUnitProp = unitProp as WingUnitProp;
			this.getRenderObject().addPiece(prop.modelName);
			if (prop.aniName != null) {
				var aniName:String = prop.aniName.split("/").pop();
				aniName = aniName.substr(0, aniName.length - 4);
				this.getRenderObject().playAni(aniName, true);
			}
		}
		
		override public function clone():Entity
		{
			var unit:WingUnit = new WingUnit();
			unit.contructor();
			unit.clonePropertiesFrom(this);
			var prop:WingUnitProp = unit.unitProp as WingUnitProp;
			unit.getRenderObject().addPiece(prop.modelName);
			if (prop.aniName != null) {
				var aniName:String = prop.aniName.split("/").pop();
				aniName = aniName.substr(0, aniName.length - 4);
				unit.getRenderObject().playAni(aniName, true);
			}
			unit.syncParamsToMaterial();
			return unit;
		}
		
		private function getRenderObject():RenderObject
		{
			if (this.renderObj == null) {
				this.renderObj = new RenderObject();
			}
			return this.renderObj;
		}
		
		public function setModelUrl(url:String):void
		{
			var obj:RenderObject = this.getRenderObject();
			obj.removeAllLinkedPieces();
			obj.addPiece(url);
			(this.unitProp as WingUnitProp).modelName = url;
		}
		
		public function setModelAni(url:String):void
		{
			(this.unitProp as WingUnitProp).aniName = url;
			var aniName:String = url.split("/").pop();
			aniName = aniName.substr(0, aniName.length - 4);
			getRenderObject().playAni(aniName, true);
		}
		
		public function getModelAni():String
		{
			return (this.unitProp as WingUnitProp).aniName;
		}
		
		public function getModelUrl():String
		{
			return (this.unitProp as WingUnitProp).modelName;
		}
		
		override FlashGE function updateOneFrame(camera:Camera3D):void
		{
			if (!isShow()) {
				return;
			}
			
			var isEditor:Boolean = CONFIG::EDITOR;
			var percent:Number = getPercent();
			if (percent < 0 || percent > 1) {
				if (system.loop == false) {
					this.startPlayTimer = -1;
				}
				if (isEditor) {
					this.startPlayTimer = -1;
				}
				return;
			}
			
			if (renderObj == null) {
				return;
			}
			
			if (this.startPlayTimer == -1) {
				this.startPlayTimer = getTimer();
			}
			
			var model:Model = this.getRenderObject().getModel();
			if (model == null || model.isLoaded()==false) return;
			if (needSync) {
				this.syncParamsToMaterial();
				needSync = false;
			}
			
			var prop:WingUnitProp = unitProp as WingUnitProp;
			this.getInterpolationValue(percent, prop.colorKeys, colorKey);
			this.calcTransforms(percent, camera, 1, 1);
			
			var mat:Material;
			for each(var subMesh:Entity in model.subMeshList) {
				mat = subMesh.getMaterial();
				if (mat != null) {
				mat.setColor(colorKey.items[0], colorKey.items[1], colorKey.items[2], colorKey.items[3]);
				mat.setStartPlayTimer(this.startPlayTimer);
				}
			}
			
			var curUnitFrame:Number = this.getLifeFramesNum() * percent;
			if (prop.followHolderAni) {
				var holder:RenderObject = system.linkedHolder as RenderObject;
				if (holder == null) {
					holder = system.parent as RenderObject;
				}
				if (holder != null) {
					if (holder.getSkeletonAni() != null) {
						
						if (holder.frameTick == Global.frameCount) {
							renderObj.gotoAniFrame(holder.aniCurFrame-1);
						} else {
							renderObj.gotoAniFrame(holder.aniCurFrame);
						}
						var srcFileName:String;
						var dstFileName:String;
						if (renderObj.getSkeletonAni() == null) {
							if (renderObj.nextSkeletonAni == null) {
								renderObj.playAni(holder.getSkeletonAni().name);
							} else {
								srcFileName = renderObj.nextSkeletonAni.fileName;
								dstFileName = holder.getSkeletonAni().fileName;
								if (srcFileName != dstFileName) {
									renderObj.playAni(holder.getSkeletonAni().name);
								}
							}
						} else {
							
							srcFileName = renderObj.getSkeletonAni().fileName;
							dstFileName = holder.getSkeletonAni().fileName;
							if (srcFileName != dstFileName) {
								if (renderObj.nextSkeletonAni == null) {
									renderObj.playAni(holder.getSkeletonAni().name);
								} else {
									srcFileName = renderObj.nextSkeletonAni.fileName;
									if (srcFileName != dstFileName) {
										
										renderObj.playAni(holder.getSkeletonAni().name);
									}
								}
							}
						}
					}
				}
			} else {
				renderObj.pauseAni(true);
				renderObj.gotoAniFrame(curUnitFrame);
			}
			renderObj.localToCameraTransform.copy(this.localToCameraTransform);
			renderObj.collectRenderUnits(camera, false);
			if (percent == 1 && system.loop == false) {
				this.startPlayTimer = -1;
			}
			
			if (isEditor) {
				if (percent == 1) {
					this.startPlayTimer = -1;
				}
			}
		}
		
		override public function checkSave():void
		{
			if (this.renderObj == null) return;
			
			var model:Model = this.renderObj.getModel();
			if (model == null) return;
			
			var names:Dictionary = new Dictionary;
			var prop:WingUnitProp = unitProp as WingUnitProp;
			
			for each(var subMesh:Entity in model.subMeshList) {
				names[subMesh.getName()] = true;
			}
			
			var dels:Vector.<String> = new Vector.<String>;
			
			for (var key:String in prop.waveContext) {
				if (names[key] == false) {
					dels.push(key);
				}
			}
			
			for (var i:int = 0, n:int = dels.length; i < n; i++) {
				delete prop.waveContext[dels[i]];
			}
		}
		
		
		override public function syncParamsToMaterial():void
		{
			var mat:Material;
			var curMeshName:String;
			var model:Model = this.getRenderObject().getModel();
			if (model == null || model.isLoaded()==false) return;
			var prop:WingUnitProp = unitProp as WingUnitProp;
			needSync = false;
			for each(var subMesh:Entity in model.subMeshList) {
				curMeshName = subMesh.getName();
				var context:Object = prop.waveContext[curMeshName];
				if (context == null) continue;
				
				mat = subMesh.getMaterial();
				if (mat == null) continue;
				mat.setSrcBlend(prop.blendSrcType);
				mat.setDstBlend(prop.blendDstType);
				mat.setUVV0(context.v0);
				mat.setUVV1(context.v1);
				mat.setType(Defines.MATERIAL_WING);
				mat.setRenderPriority(prop.renderPriority);
				mat.setWaveContext(0, context.x.amplitude, context.x.wavelength, context.x.attenuation, context.alphaFactor);
				mat.setWaveContext(1, context.y.amplitude, context.y.wavelength, context.y.attenuation);
				mat.setWaveContext(2, context.z.amplitude, context.z.wavelength, context.z.attenuation);
				mat.setRepeatIndex(context.repeatIndex);
				var tex:TextureFile;
				if (context.distortionTexture!=null && context.distortionTexture!="" ) {
					tex= TextureFile.createFromFile(context.distortionTexture);
					mat.setTexture(1, tex);
					tex.release();
				} else {
					mat.setTexture(1, null);
				}
				
				if (context.replacedTexture!=null && context.replacedTexture!="" ) {
					tex = TextureFile.createFromFile(context.replacedTexture);
					mat.setTexture(0, tex);
					tex.release();
				} else {
					mat.setTexture(0, null);
				}
				
				mat.setWaveContext(3, context.layerSpeedU0, context.layerSpeedV0, context.layerSpeedU1, context.layerSpeedV1);
				mat.setWaveContext(4, context.distortionAmountX, context.distortionAmountY, context.heightAttenuationX, context.heightAttenuationY);
				mat.setFixWaveAlpha(context.fixAlpha);
			}
			
			this.rotateS = 0;
		}
		
		override public function getFollowNode():Entity
		{
			var ret:Entity;
			if ((unitProp as WingUnitProp).followHolderAni) {
				ret = this.system.linkedHolder;
				if (ret != null) {
					return ret;
				}
			}
			
			return super.getFollowNode();
		}
		
		override protected function calcTransforms(percent:Number, camera:Camera3D, exScaleX:Number, exScaleY:Number):void
		{
			var prop:SpriteUnitProp = this.unitProp as SpriteUnitProp;
			var curFrameTimer:int = this.system.getCurFrame() - this.getBeginFrame();
			if (this.preUnitFrameTimer < 0) {
				this.preUnitFrameTimer = curFrameTimer;
				if (prop.completelyFollow == false) {
					if (baseTransformTemp == null) {
						baseTransformTemp = new Transform3D
					}
					
					if (basePos == null) {
						basePos = camera.pool.popVector3D();
					}
					
					baseTransformTemp.combine(camera.localToGlobalTransform, this.getFollowNode().localToCameraTransform);
					basePos.setTo(baseTransformTemp.d, baseTransformTemp.h, baseTransformTemp.l);
					baseTransformTemp.d = baseTransformTemp.h = baseTransformTemp.l = 0;
				}
			}
			
			
			this.getInterpolationValue(percent, prop.offsetKeys, offsetKey);
			this.getInterpolationValue(percent, prop.rotateKeys, rotateKey);
			this.getInterpolationValue(percent, prop.scaleKeys, scaleKey);
			
			var baseNode:Entity = camera.getRootNode();
			if (prop.completelyFollow) {
				this.x = offsetKey.items[0]; 
				this.y = offsetKey.items[1]; 
				this.z = offsetKey.items[2];
				baseNode = this.getFollowNode();
			} else if (this.basePos!=null) {
				this.x = offsetKey.items[0] + this.basePos.x;
				this.y = offsetKey.items[1] + this.basePos.y;
				this.z = offsetKey.items[2] + this.basePos.z;
			} else {
				this.x = offsetKey.items[0];
				this.y = offsetKey.items[1];
				this.z = offsetKey.items[2];
			}
			
			this.rotationX = 0; 
			this.rotationY = 0;
			this.rotationZ = 0;
			
			this.scaleX = scaleKey.items[0] * exScaleX;
			this.scaleY = scaleKey.items[1] * exScaleY;
			this.scaleZ = scaleKey.items[2];
			
			this.composeTransforms();
			this.transform.append(prop.normalTrans);
			
			var deltaFrame:Number;
			if (curFrameTimer >= preUnitFrameTimer) {
				deltaFrame = curFrameTimer - preUnitFrameTimer;
			} else {
				deltaFrame = this.getLifeFramesNum() - preUnitFrameTimer + curFrameTimer + 1;
			}
			
			preUnitFrameTimer = curFrameTimer;
			
			if (percent == 0 && this.system.loop == false) {
				this.rotateS = 0;
			}
			
			if (this.system.isPaused()) {
				var isEditor:Boolean = CONFIG::EDITOR;
				if (isEditor) {
					var curUnitFrame:Number = curFrameTimer - this.getBeginFrame();
					var scaleKeyItem:KeyPair = new KeyPair;
					scaleKeyItem.init(0);
					rotateS = 0;
					for (var p:int = 0; p < curUnitFrame; p++) {
						var percentItem:Number = p / this.getLifeFramesNum();
						this.getInterpolationValue(percentItem, prop.rotateKeys, scaleKeyItem);
						rotateS += (scaleKeyItem.items[0]/24.0);
					}
				}
				
			} else {
				
				if (percent != 0) {
					var curTimerS:int = getTimer();
					if (preRotateTimerS == -1) {
						preRotateTimerS = curTimerS;
					}
					rotateS += (rotateKey.items[0]*(preRotateTimerS - curTimerS)/1000.0);
					preRotateTimerS = curTimerS;
				}
			}
			
			
			var cameraX:Number, cameraY:Number, cameraZ:Number;
			
			var localXTemp:Number = 0, localYTemp:Number = 0, localZTemp:Number = 0;
			var scaleXTemp:Number = 0, scaleYTemp:Number = 0, scaleZTemp:Number = 0;
			var tempTrans:Transform3D;
			
			var selfTrans:Transform3D = camera.pool.popTransform3D();
			if (prop.faceType == UnitBaseProp.DIR_TYPE_PARENT || 
				prop.faceType == UnitBaseProp.DIR_TYPE_WORLD ||
				prop.faceType == UnitBaseProp.DIR_TYPE_CAMERA) {
				
				selfTrans.compose(0,0,0,0,0,0,this.scaleX, this.scaleY, this.scaleZ);
				tempTransB.fromAxisAngle(prop.selfRotateAxis.x, prop.selfRotateAxis.y, 
					prop.selfRotateAxis.z,prop.selfOriginRotate + rotateS);
				selfTrans.append(tempTransB);	
				selfTrans.append(prop.normalTrans);
				if (prop.faceType == UnitBaseProp.DIR_TYPE_CAMERA) {
					tempTransB.compose(0, 0, 0, this.rotationX+Math.PI, this.rotationY, this.rotationZ, 1, 1, 1);
				} else {
					tempTransB.compose(0, 0, 0, this.rotationX, this.rotationY, this.rotationZ, 1, 1, 1);
				}
				selfTrans.append(tempTransB);					
				
			} else if(prop.faceType == UnitBaseProp.DIR_TYPE_H_CAMERA) {
				
				//rotation
				var rot:Vector3D = camera.pool.popVector3D();
				var parent:Entity = camera.getParent();
				rot.setTo(parent.rotationX, parent.rotationY, parent.rotationZ);
				
				var tranCamera:Transform3D = camera.pool.popTransform3D();
				tranCamera.fromEulerAngles(rot.x, rot.y, rot.z);
				camera.pool.pushVector3D(rot);
				
				var camX:Number = camera.x*tranCamera.a + camera.y*tranCamera.b + camera.z*tranCamera.c + tranCamera.d;
				var camY:Number = camera.x*tranCamera.e + camera.y*tranCamera.f + camera.z*tranCamera.g + tranCamera.h;
				var camZ:Number = camera.x*tranCamera.i + camera.y*tranCamera.j + camera.z*tranCamera.k + tranCamera.l;
				camera.pool.pushTransform3D(tranCamera);
				
				var tranTemp:Transform3D = camera.pool.popTransform3D();
				var dirA:Vector3D = camera.pool.popVector3D();
				dirA.x = dirA.y = 0; dirA.z = 1;
				var dirB:Vector3D = camera.pool.popVector3D();
				dirB.x = camX-this.x;
				dirB.y = camY-this.y;
				dirB.z = 0;
				dirB.normalize();
				
				var selfRot:Number = Math.atan(dirB.y/dirB.x)+prop.selfOriginRotate;
				if (dirB.x < 0)
					selfRot = Math.PI+selfRot;
				tempTransB.fromEulerAngles(0, 0, selfRot);
				
				if (dirB.x ==0 && dirB.y==0 && dirB.z==1) {
					tranTemp.fromAxisAngle(0, -1, 0, 0);
				} else {
					var dirN:Vector3D = dirA.crossProduct(dirB);
					dirN.normalize();
					var dot:Number = dirA.dotProduct(dirB);
					tranTemp.fromAxisAngle(dirN.x, dirN.y, dirN.z, Math.acos(dot));
				}
				tempTransA.combine(tranTemp, tempTransB);
				camera.pool.pushTransform3D(tranTemp);
				camera.pool.pushVector3D(dirA);
				camera.pool.pushVector3D(dirB);
				
				selfTrans.compose(0,0,0,0,0,0,this.scaleX, this.scaleY, this.scaleZ);
				tempTransB.fromAxisAngle(
					prop.selfRotateAxis.x, 
					prop.selfRotateAxis.y, 
					prop.selfRotateAxis.z, rotateS);
				selfTrans.append(tempTransB);
				selfTrans.append(tempTransA);
			}			
			
			if (prop.followType == UnitBaseProp.DIR_TYPE_PARENT) {
				if( prop.faceType == UnitBaseProp.DIR_TYPE_PARENT) {
					
					if (prop.completelyFollow==false && baseTransformTemp!=null) {
						selfTrans.append(baseTransformTemp);
					}
					tempTransB.compose(this.x, this.y, this.z, 0, 0, 0, 1, 1, 1);
					selfTrans.append(tempTransB);
					
					selfTrans.append(baseNode.localToCameraTransform);
					this.localToCameraTransform.copy(selfTrans);
					
				} else if (prop.faceType == UnitBaseProp.DIR_TYPE_WORLD) {
					
					tempTrans = baseNode.localToCameraTransform;
					localXTemp = this.x*tempTrans.a + this.y*tempTrans.b + this.z*tempTrans.c + tempTrans.d;
					localYTemp = this.x*tempTrans.e + this.y*tempTrans.f + this.z*tempTrans.g + tempTrans.h;
					localZTemp = this.x*tempTrans.i + this.y*tempTrans.j + this.z*tempTrans.k + tempTrans.l;
					
					scaleXTemp = Math.sqrt(tempTrans.a*tempTrans.a + tempTrans.e*tempTrans.e + tempTrans.i*tempTrans.i);
					scaleYTemp = Math.sqrt(tempTrans.b*tempTrans.b + tempTrans.f*tempTrans.f + tempTrans.j*tempTrans.j);
					scaleZTemp = Math.sqrt(tempTrans.c*tempTrans.c + tempTrans.g*tempTrans.g + tempTrans.k*tempTrans.k);
					
					this.localToCameraTransform.compose(0,0,0,0,0,0,scaleXTemp, scaleYTemp, scaleZTemp);
					this.localToCameraTransform.append(selfTrans); 
					this.localToCameraTransform.append(camera.getRootNode().localToCameraTransform); 
					this.localToCameraTransform.d = localXTemp;
					this.localToCameraTransform.h = localYTemp;
					this.localToCameraTransform.l = localZTemp;
					
				} else if (prop.faceType == UnitBaseProp.DIR_TYPE_CAMERA) {
					tempTrans = baseNode.localToCameraTransform;
					
					cameraX = this.x*tempTrans.a + this.y*tempTrans.b + this.z*tempTrans.c + tempTrans.d;
					cameraY = this.x*tempTrans.e + this.y*tempTrans.f + this.z*tempTrans.g + tempTrans.h;
					cameraZ = this.x*tempTrans.i + this.y*tempTrans.j + this.z*tempTrans.k + tempTrans.l;
					
					scaleXTemp = Math.sqrt(tempTrans.a*tempTrans.a + tempTrans.e*tempTrans.e + tempTrans.i*tempTrans.i);
					scaleYTemp = Math.sqrt(tempTrans.b*tempTrans.b + tempTrans.f*tempTrans.f + tempTrans.j*tempTrans.j);
					scaleZTemp = Math.sqrt(tempTrans.c*tempTrans.c + tempTrans.g*tempTrans.g + tempTrans.k*tempTrans.k);
					
					this.localToCameraTransform.copy(selfTrans);
					
					tempTransB.compose(0,0,0,0,0,0,scaleXTemp, scaleYTemp, scaleZTemp);
					this.localToCameraTransform.append(tempTransB);
					this.localToCameraTransform.d = cameraX;
					this.localToCameraTransform.h = cameraY;
					this.localToCameraTransform.l = cameraZ;
					
				} else if (prop.faceType == UnitBaseProp.DIR_TYPE_H_CAMERA) {
					tempTrans = baseNode.localToCameraTransform;
					localXTemp = this.x*tempTrans.a + this.y*tempTrans.b + this.z*tempTrans.c + tempTrans.d;
					localYTemp = this.x*tempTrans.e + this.y*tempTrans.f + this.z*tempTrans.g + tempTrans.h;
					localZTemp = this.x*tempTrans.i + this.y*tempTrans.j + this.z*tempTrans.k + tempTrans.l;
					
					scaleXTemp = Math.sqrt(tempTrans.a*tempTrans.a + tempTrans.e*tempTrans.e + tempTrans.i*tempTrans.i);
					scaleYTemp = Math.sqrt(tempTrans.b*tempTrans.b + tempTrans.f*tempTrans.f + tempTrans.j*tempTrans.j);
					scaleZTemp = Math.sqrt(tempTrans.c*tempTrans.c + tempTrans.g*tempTrans.g + tempTrans.k*tempTrans.k);
					
					this.localToCameraTransform.compose(0,0,0,0,0,0,scaleXTemp, scaleYTemp, scaleZTemp);
					this.localToCameraTransform.append(selfTrans);
					this.localToCameraTransform.append(camera.getRootNode().localToCameraTransform); 
					this.localToCameraTransform.d = localXTemp;
					this.localToCameraTransform.h = localYTemp;
					this.localToCameraTransform.l = localZTemp;
				}
				
			} else if (prop.followType == UnitBaseProp.DIR_TYPE_WORLD) {
				
				if( prop.faceType == UnitBaseProp.DIR_TYPE_PARENT) {
					
					if (prop.completelyFollow==false && baseTransformTemp!=null) {
						selfTrans.append(baseTransformTemp);
					}
					tempTransB.compose(this.x, this.y, this.z, 0, 0, 0, 1, 1, 1);
					selfTrans.append(tempTransB);
					
					tempTrans = baseNode.localToCameraTransform;
					scaleXTemp = Math.sqrt(tempTrans.a*tempTrans.a + tempTrans.e*tempTrans.e + tempTrans.i*tempTrans.i);
					scaleYTemp = Math.sqrt(tempTrans.b*tempTrans.b + tempTrans.f*tempTrans.f + tempTrans.j*tempTrans.j);
					scaleZTemp = Math.sqrt(tempTrans.c*tempTrans.c + tempTrans.g*tempTrans.g + tempTrans.k*tempTrans.k);
					tempTransB.compose(0,0,0,0,0,0,1/scaleXTemp, 1/scaleYTemp, 1/scaleZTemp);
					selfTrans.append(tempTransB);
					selfTrans.append(baseNode.localToCameraTransform);
					this.localToCameraTransform.copy(selfTrans);
					
				} else if (prop.faceType == UnitBaseProp.DIR_TYPE_WORLD) {
					tempTrans = baseNode.localToCameraTransform;
					localXTemp = this.x*tempTrans.a + this.y*tempTrans.b + this.z*tempTrans.c + tempTrans.d;
					localYTemp = this.x*tempTrans.e + this.y*tempTrans.f + this.z*tempTrans.g + tempTrans.h;
					localZTemp = this.x*tempTrans.i + this.y*tempTrans.j + this.z*tempTrans.k + tempTrans.l;						
					
					this.localToCameraTransform.copy(selfTrans); 
					this.localToCameraTransform.append(camera.getRootNode().localToCameraTransform); 
					this.localToCameraTransform.d = localXTemp;
					this.localToCameraTransform.h = localYTemp;
					this.localToCameraTransform.l = localZTemp;
				} else if (prop.faceType == UnitBaseProp.DIR_TYPE_CAMERA) {
					tempTrans = baseNode.localToCameraTransform;
					
					cameraX = this.x*tempTrans.a + this.y*tempTrans.b + this.z*tempTrans.c + tempTrans.d;
					cameraY = this.x*tempTrans.e + this.y*tempTrans.f + this.z*tempTrans.g + tempTrans.h;
					cameraZ = this.x*tempTrans.i + this.y*tempTrans.j + this.z*tempTrans.k + tempTrans.l;
					
					this.localToCameraTransform.copy(selfTrans);
					tempTransB.compose(cameraX,cameraY,cameraZ,0,0,0,1, 1, 1);
					this.localToCameraTransform.append(tempTransB);
					
				} else if (prop.faceType == UnitBaseProp.DIR_TYPE_H_CAMERA) {
					tempTrans = baseNode.localToCameraTransform;
					localXTemp = this.x*tempTrans.a + this.y*tempTrans.b + this.z*tempTrans.c + tempTrans.d;
					localYTemp = this.x*tempTrans.e + this.y*tempTrans.f + this.z*tempTrans.g + tempTrans.h;
					localZTemp = this.x*tempTrans.i + this.y*tempTrans.j + this.z*tempTrans.k + tempTrans.l;
					
					this.localToCameraTransform.copy(selfTrans);
					this.localToCameraTransform.append(camera.getRootNode().localToCameraTransform); 
					this.localToCameraTransform.d = localXTemp;
					this.localToCameraTransform.h = localYTemp;
					this.localToCameraTransform.l = localZTemp;
				}
			}
			
			camera.pool.pushTransform3D(selfTrans);
			
			if (percent >=1 && system.loop == false) {
				this.preUnitFrameTimer = -1;
			}
		}
	}
}