/*@author lye 123*/
package FlashGE.effects 
{
	import flash.utils.ByteArray;

	public class KeyPair
	{
		public var time:int;
		public var items:Vector.<Number>;
		
		public function init(time:Number = 0, ... args):void
		{
			this.time = time;
			this.items = new Vector.<Number>;
			var n:int = args.length;
			for (var i:int = 0; i< n; i++) {
				items[i] = Number(args[i]);
			}
		}
		
		public function clone():KeyPair
		{
			var key:KeyPair = new KeyPair();
			key.time = this.time;
			key.items = new Vector.<Number>;
			var n:int = items.length;
			for( var i:int = 0; i < n; i++){
				key.items.push(this.items[i]);
			}
			return key;
		}
		
		public function read(rawData:ByteArray):void
		{
			items = new Vector.<Number>;
			items.length = rawData.readUnsignedByte();
			var n:int = items.length;
			for (var i:int = 0; i < n; i++){
				items[i] = rawData.readFloat();
			}
			this.time = rawData.readUnsignedByte();
		}	
		
		public function write(rawData:ByteArray):void
		{
			rawData.writeByte(items.length);
			
			var n:int = items.length;
			for (var i:int = 0; i < n; i++) {
				rawData.writeFloat(items[i]);
			}
			rawData.writeByte(this.time);
		}
	}
}