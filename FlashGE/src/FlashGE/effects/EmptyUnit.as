/*@author lye 123*/
package FlashGE.effects
{
	import flash.geom.Vector3D;
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Global;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.EmptyUnitProp;
	import FlashGE.effects.unitProp.SpriteUnitProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.objects.Entity;
	import FlashGE.objects.Model;
	import FlashGE.objects.RenderObject;
	import FlashGE.render.Camera3D;

	use namespace FlashGE;
	public class EmptyUnit extends SpriteUnit
	{
		private var followedNode:Entity;
		private var nodesList:Vector.<Entity>; 
		private var nodesListInited:Boolean;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_EMPTY;
		}
		
		override public function contructor(unitProp:UnitBaseProp=null):void
		{
			if (unitProp == null) {
				var newProp:EmptyUnitProp = new EmptyUnitProp;
				newProp.contructor();
				super.contructor(newProp);
			} else {
				super.contructor(unitProp);
			}
			
			this.nodesList = new Vector.<Entity>; 
			this.nodesListInited = false;
		}
		
		override public function setProp(unitProp:UnitBaseProp):void
		{
			this.unitProp = unitProp;
			this.syncParamsToMaterial();
		}
		
		override public function clone():Entity
		{
			var unit:EmptyUnit = new EmptyUnit();
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function syncParamsToMaterial():void
		{
			super.syncParamsToMaterial();	
			this.nodesListInited = false;
			this.followedNode = null;
		}
		
		override FlashGE function updateOneFrame(camera:Camera3D):void
		{
			var prop:EmptyUnitProp = this.unitProp as EmptyUnitProp;
			
			var percent:Number = getPercent();
			if (prop.openSignature==false) {
				
				if (percent < 0 || percent > 1) return;
				this.getInterpolationValue(percent, prop.colorKeys, colorKey);
				this.calcTransforms(percent, camera, 1, 1);
				
				if (beforeRenderListener != null) {
					beforeRenderListener(this, camera);
				}
				return;
			}
			
			var host:RenderObject = this.system.getRandNodesHolder();
			if (host == null) return;
			
			var model:Model = host.getModel();
			if (model == null) return;
			
			if (percent < 0 || percent > 1) {
				
				this.nodesListInited = false;
				if (percent > 1) {
					if (this.followedNode!=null) {
						this.followedNode.setVisible(false);
					}
				}
				
				return;
			}
			
			if (nodesListInited == false) {
				this.followedNode = system.randomNode();
				//if (this.followedNode != null&& prop.fxFileName!=null && prop.fxFileName.length > 0) {
					
					//this.followedNode.addChild(Effect.createFromFile(prop.fxFileName));
					/*
					var skin:Skin = this.followedNode as Skin;
					if (skin!=null) {
						if (skin.skinData.surfaceBoneNamesList.length >0 ) {
							var namesList:Vector.<String> = skin.skinData.surfaceBoneNamesList[0];
							if (namesList.length > 0) {
								host.linkEffectByName(prop.fxFileName, -1, namesList[0]);
							}
						}
					}
					*/
				//}

				nodesListInited = true;
			}

			if (this.followedNode == null) return;
			
			this.getInterpolationValue(percent, prop.offsetKeys, offsetKey);
			this.getInterpolationValue(percent, prop.rotateKeys, rotateKey);
			this.getInterpolationValue(percent, prop.scaleKeys,  scaleKey);

			this.followedNode.setPosition(offsetKey.items[0], offsetKey.items[1], offsetKey.items[2]);
			this.followedNode.setRotation(rotateKey.items[0], rotateKey.items[1], rotateKey.items[2]);
			this.followedNode.setScale(scaleKey.items[0], scaleKey.items[1], scaleKey.items[2]);						
		}

	}
}