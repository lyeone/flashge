/*@author lye 123*/

package FlashGE.effects
{
	import FlashGE.FlashGE;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.common.Utils;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.Polytrail2UnitProp;
	import FlashGE.effects.unitProp.PolytrailUnitProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.materials.Material;
	import FlashGE.objects.Entity;
	import FlashGE.render.Camera3D;

	use namespace FlashGE;

	public class PolyTrailUnit extends EffectUnitBase
	{		
		static private var offsetKey:KeyPair;
		static private var scaleKey:KeyPair;
		static private var colorKey:KeyPair;
		static private var rotateKey:KeyPair;
		
		public var polyTrail:PolyTrails;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_POLYTRAIL;
		}
		
		static public function initStaticContext():void
		{
			PolyTrailUnit.offsetKey = new KeyPair;
			PolyTrailUnit.offsetKey.init(0);
			
			PolyTrailUnit.scaleKey = new KeyPair;
			PolyTrailUnit.scaleKey.init(0);
			
			PolyTrailUnit.colorKey = new KeyPair;
			PolyTrailUnit.colorKey.init(0);
			
			PolyTrailUnit.rotateKey = new KeyPair;
			PolyTrailUnit.rotateKey.init(0);
		}
		
		override public function dispose():void
		{
			super.dispose();
			if (polyTrail != null) {
				polyTrail.dispose();
				polyTrail = null;
			}
		}
		
		override public function contructor(unitProp:UnitBaseProp = null):void
		{
			super.contructor(unitProp);
			
			if (unitProp == null) {
				var newProp:PolytrailUnitProp = new PolytrailUnitProp;
				newProp.contructor();
				this.setProp(newProp);
			} else {
				this.setProp(unitProp);
			}
		}
			
		override public function setMaterial(material:Material ):void
		{
			material.setUseVertDiffuse(false);
			super.setMaterial(material);
		}
		
		override FlashGE function updateOneFrame(camera:Camera3D):void
		{		
			var curSysFrame:Number = system.getCurFrame();
			if (!isShow() || this.getFollowNode() == null) {
				return;
			}
			
			var percent:Number = getPercent();
			if (percent < 0 || percent > 1) {
				return;
			}
			
			if (polyTrail == null) {
				polyTrail = new PolyTrails();
				polyTrail.setMaterial(this.material);
			}
			
			if (false == polyTrail.draw2(this, camera)) {
				return;
			}
			
			
			var prop:PolytrailUnitProp = this.unitProp as PolytrailUnitProp;
			var curUnitFrame:Number = percent * this.getLifeFramesNum();
			
			this.getInterpolationValue(percent, prop.scaleKeys, scaleKey);
			this.getInterpolationValue(percent, prop.offsetKeys, offsetKey);
			this.getInterpolationValue(percent, prop.rotateKeys, rotateKey);
			this.getInterpolationValue(percent, prop.colorKeys, colorKey);
			
			calcUV(curUnitFrame);
			
			var mat:Material = this.material;
			mat.setColor(colorKey.items[0], colorKey.items[1], colorKey.items[2], colorKey.items[3]);
			mat.setUVContext(this.uvCurUOffset, this.uvCurVOffset, this.uvCurUScale, this.uvCurVScale, this.uvCurV0, this.uvCurV1);
			//mat.setCurTime(curUnitFrame);
			mat.setOpenDoubleAlpha(prop.updateZ==1);
			
			var transform:Transform3D = camera.pool.popTransform3D();
			transform.compose(offsetKey.items[0], offsetKey.items[1], offsetKey.items[2], 0,0,0, 1, 1, 1);
			polyTrail.localToCameraTransform.combine(camera.globalToLocalTransform, transform);
			camera.pool.pushTransform3D(transform);
			polyTrail.collectRenderUnits(camera, false);
		}
		
		override public function clone():Entity
		{
			var unit:PolyTrailUnit = new PolyTrailUnit
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function syncParamsToMaterial():void
		{
			super.syncParamsToMaterial();
			this.material.setUseVertDiffuse(true);
		}
	}
}
