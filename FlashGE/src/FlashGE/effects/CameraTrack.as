/*@author lye 123*/

package FlashGE.effects
{
	import FlashGE.FlashGE;
	import FlashGE.animation.SkeletonAni;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.effects.unitProp.CameraTrackUnitProp;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.objects.Entity;
	import FlashGE.objects.Model;
	import FlashGE.objects.RenderObject;
	import FlashGE.render.Camera3D;
	
	use namespace FlashGE;
	public class CameraTrack extends EffectUnitBase
	{
		private var renderObject:RenderObject;
		
		private var originX:Number;
		private var originY:Number;
		private var originZ:Number;
		private var originRotateX:Number;
		private var originRotateY:Number;
		private var originRotateZ:Number;
		private var scaleOriginX:Number;
		private var scaleOriginY:Number;
		private var scaleOriginZ:Number;
		private var cameraBK:Camera3D;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_CAMERA_TRACK;
		}
		
		override public function contructor(unitProp:UnitBaseProp = null):void
		{
			super.contructor(unitProp);
			
			this.originX = 0;
			this.originY = 0;
			this.originZ = 0;
			this.originRotateX = 0;
			this.originRotateY = 0;
			this.originRotateZ = 0;
			this.scaleOriginX = 1;
			this.scaleOriginY = 1;
			this.scaleOriginZ = 1;
			
			renderObject = new RenderObject();
			
			if (this.unitProp==null) {
				this.unitProp = new CameraTrackUnitProp;
				this.unitProp.contructor();
			} else {
				var curProp:CameraTrackUnitProp = this.unitProp as CameraTrackUnitProp;
				setModelUrl(curProp.modelName);
				setModelAni(curProp.aniName);
			}
			
			this.setForceVisible( true);
			
		}
		public function setModelUrl(url:String):void
		{
			renderObject.addPiece(url);
			(this.unitProp as CameraTrackUnitProp).modelName = url;
		}
		
		public function getModelUrl():String
		{
			var t:Model = renderObject.getModel();
			return t!=null ? t.getName() : "";
		}
		
		public function setModelAni(url:String):void
		{
			(this.unitProp as CameraTrackUnitProp).aniName = url;
			
			var aniName:String = url.split("/").pop();
			aniName = aniName.substr(0, aniName.length - 4);
			renderObject.playAni(aniName, (unitProp as CameraTrackUnitProp).aniLoop);
		}
		
		public function getModelAni():String
		{
			var ani:SkeletonAni = renderObject.getSkeletonAni();
			return ani!=null ? ani.getFileName() : "";
		}
		
		override public function dispose():void
		{
			super.dispose();
			system.setForceVisible(false);
			revertBK();
		}
		
		private function revertBK():void
		{
			if (cameraBK!=null) {
				cameraBK.exTransform.identity();
				cameraBK.x = this.originX;
				cameraBK.y = this.originY;
				cameraBK.z = this.originZ;
				cameraBK.rotationX = this.originRotateX;
				cameraBK.rotationY = this.originRotateY;
				cameraBK.rotationZ = this.originRotateZ;
				cameraBK.scaleX = this.scaleOriginX;
				cameraBK.scaleY = this.scaleOriginY;
				cameraBK.scaleZ = this.scaleOriginZ;
				cameraBK = null;
			}
		}
		
		override public function syncParamsToMaterial():void
		{
			super.syncParamsToMaterial();
		}
		
		override FlashGE function updateOneFrame(camera:Camera3D):void
		{
			if (!this.isShow() || renderObject==null) {
				return;
			}
			
			var curSysTime:Number = system.getCurFrame();
			if (curSysTime < int(getBeginFrame())) {
				return;
			}
			
			var prop:CameraTrackUnitProp = this.unitProp as CameraTrackUnitProp;
			
			renderObject.aniLoop = prop.aniLoop;
			var joint:Entity = renderObject.getNodeByName("Bip001");
			if (joint == null)
				return;
			
			system.setForceVisible(true);
			
			transformChanged = true;
			if (transformChanged) {
				composeTransforms();
				renderObject.transform.copy(this.transform);
				renderObject.inverseTransform.copy(this.inverseTransform);
			}

			renderObject.cameraToLocalTransform.combine(renderObject.inverseTransform, this.getFollowNode().cameraToLocalTransform);
			renderObject.localToCameraTransform.combine(this.getFollowNode().localToCameraTransform, renderObject.transform);
			renderObject.calculateVisibility(camera);
			renderObject.calculateChildrenVisibility(camera);
			renderObject.collectRenderUnits(camera, false);	
			
			var transform:Transform3D = camera.pool.popTransform3D();			
			transform.combine(camera.localToGlobalTransform, joint.localToCameraTransform);	
			
			var scalex:Number = Math.sqrt(transform.a*transform.a + transform.e*transform.e + transform.i*transform.i);
			var scaley:Number = Math.sqrt(transform.b*transform.b + transform.f*transform.f + transform.j*transform.j);
			var scalez:Number = Math.sqrt(transform.c*transform.c + transform.g*transform.g + transform.k*transform.k);

			transform.a /= scalex;
			transform.e /= scalex;
			transform.i /= scalex;
			
			transform.b /= scaley;
			transform.f /= scaley;
			transform.j /= scaley;
			
			transform.c /= scalez;
			transform.g /= scalez;
			transform.k /= scalez;
			
			camera.exTransform.copy(transform);
			camera.pool.pushTransform3D(transform);
			
			if (cameraBK == null) {
				cameraBK = camera;
				this.originX = camera.x;
				this.originY = camera.y;
				this.originZ = camera.z;
				this.originRotateX = camera.rotationX;
				this.originRotateY = camera.rotationY;
				this.originRotateZ = camera.rotationZ;
				this.scaleOriginX = camera.scaleX;
				this.scaleOriginY = camera.scaleY;
				this.scaleOriginZ = camera.scaleZ;
			}
			camera.x = 0;
			camera.y = 0;
			camera.z = 0;
			camera.lookAt(0, 1, 0);
		}
	}
}
