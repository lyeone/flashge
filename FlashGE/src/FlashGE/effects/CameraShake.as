package FlashGE.effects
{	
	import FlashGE.FlashGE;
	import FlashGE.common.Pool;
	import FlashGE.common.Transform3D;
	import FlashGE.effects.unitProp.CameraShakeUnitProp;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.objects.CameraHolder;
	import FlashGE.objects.Entity;
	import FlashGE.objects.RenderObject;
	import FlashGE.render.Camera3D;
	
	use namespace FlashGE;

	public class CameraShake extends EffectUnitBase	
	{
		static private var unitsCount:int = 0;
		static public var closeCameraShake:Boolean = false;
		
		private var cameraHolder:Entity;
		private var holderX:Number;
		private var holderY:Number;
		private var holderZ:Number;
		private var tickMarked:Boolean;

		override public function contructor(unitProp:UnitBaseProp = null):void
		{
			super.contructor(unitProp);
			
			this.holderX = 0;
			this.holderY = 0;
			this.holderZ = 0;
			this.tickMarked = false;
			
			if (this.unitProp == null) {
				this.unitProp = new CameraShakeUnitProp;
				this.unitProp.contructor();
			}
		}
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_CAMERA_SHAKE;
		}
		
		override public function setVisible(v:Boolean):void
		{
			super.setVisible(v);
		}
		
		override FlashGE function updateOneFrame(camera:Camera3D):void			
		{
			if (CameraShake.closeCameraShake) return;
			
			if (CameraShake.unitsCount > 0 && tickMarked==false) return;
			
			if (cameraHolder == null) {
				cameraHolder = camera.parent;
				if (cameraHolder == null) return;
				this.holderX = cameraHolder.getX();
				this.holderY = cameraHolder.getY();
				this.holderZ = cameraHolder.getZ();
			}
			
			if (!isShow())
				return;

			if (tickMarked == false) {
				CameraShake.unitsCount++;
				tickMarked = true;
			}

			var percent:Number = getPercent();
			
			if (percent < 0) return;
			if (percent > 1) {
				cameraHolder.setPosition(this.holderX, this.holderY, this.holderZ);
				if ((cameraHolder as CameraHolder)!=null) {
					(cameraHolder as CameraHolder).relayout();
				}
				if (this.tickMarked) {
					tickMarked = false;
					CameraShake.unitsCount--;
				}
				return;
			}
			
			var offsetKey:KeyPair = new KeyPair;
			offsetKey.init(0);
			//var focusOffsetKey:KeyPair = new KeyPair(0);
			
			this.getInterpolationValue(percent, unitProp.offsetKeys, offsetKey);
			var linkedNode:Entity = this.system.getLinkedNode();
			if (linkedNode!=null) {
				var parent:RenderObject = linkedNode as RenderObject;
				if (parent != null) {
					var transform:Transform3D = camera.pool.popTransform3D();
					transform.fromEulerAngles(parent.rotationX, parent.rotationY, parent.rotationZ);
					
 					var localx:Number = offsetKey.items[0];
					var localy:Number = offsetKey.items[1];
					var localz:Number = offsetKey.items[2];
					
					offsetKey.items[0] = localx*transform.a + localy*transform.b + localz*transform.c;
					offsetKey.items[1] = localx*transform.e + localy*transform.f + localz*transform.g;
					offsetKey.items[2] = localx*transform.i + localy*transform.j + localz*transform.k;
					camera.pool.pushTransform3D(transform);
				}	
			} 
			////由PI/2开始的正弦曲线
			//offset = dir * dist * sin(PI/2 + time * 2 * PI / (duration / freq));
			var curProp:CameraShakeUnitProp = this.unitProp as CameraShakeUnitProp;
			var distPercent:Number = Math.sin(Math.PI/2 + percent * unitProp.lifeFramesNum * 2 * Math.PI/(unitProp.lifeFramesNum/curProp.freq));
			var dx:Number = offsetKey.items[0] * distPercent;
			var dy:Number = offsetKey.items[1] * distPercent;
			var dz:Number = offsetKey.items[2] * distPercent;
			
			//camera.SetCameraOffset(offsetKey.items[0], offsetKey.items[1], offsetKey.items[2]);
			//camera.SetCamType(2);
			
			var tranCamera:Transform3D = cameraHolder.parent.cameraToLocalTransform;
			var camX:Number = dx*tranCamera.a + dy*tranCamera.b + dz*tranCamera.c;
			var camY:Number = dx*tranCamera.e + dy*tranCamera.f + dz*tranCamera.g;
			var camZ:Number = dx*tranCamera.i + dy*tranCamera.j + dz*tranCamera.k;
			cameraHolder.setPosition(this.holderX+camX, this.holderY+camY, this.holderZ+camZ);
		}
		
		override public function clone():Entity
		{
			var unit:CameraShake = new CameraShake();
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override protected function clonePropertiesFrom(source:Entity):void
		{
			super.clonePropertiesFrom(source);
		}
		
		override public function dispose():void
		{
			if (this.tickMarked) {
				this.tickMarked = false;
				CameraShake.unitsCount--;
			}
			
			if (cameraHolder != null) {
				cameraHolder.setPosition(this.holderX, this.holderY, this.holderZ);
				if ((cameraHolder as CameraHolder)!=null) {
					(cameraHolder as CameraHolder).relayout();
				}
			}
			super.dispose();
		}
	}
}