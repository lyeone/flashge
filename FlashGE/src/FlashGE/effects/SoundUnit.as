/*@author lye 123*/

package FlashGE.effects
{
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.SoundUnitProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.objects.Entity;
	import FlashGE.render.Camera3D;

	use namespace FlashGE;
	
		
	
	public class SoundUnit extends EffectUnitBase	
	{
		public var hasPlayed:Boolean;
		override public function contructor(unitProp:UnitBaseProp = null):void
		{
			hasPlayed = false;
			super.contructor(unitProp);
			
			if (unitProp == null) {
				var newProp:SoundUnitProp = new SoundUnitProp;
				newProp.contructor();
				this.setProp(newProp);
			} else {
				this.setProp(unitProp);
			}
		}
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_SOUND;
		}

		override FlashGE function updateOneFrame(camera:Camera3D):void			
		{	
			if (!isShow()) {
				return;
			}
			
			var prop:SoundUnitProp = unitProp as SoundUnitProp;
			var percent:Number = getPercent();
			if (percent < 0 || percent >= 1) {
				if (percent >= 1) {
					if (Defines.stopSound != null) {
						Defines.stopSound();
					}
					hasPlayed = false;
				}
				return;
			}

			if (hasPlayed == false) {
				hasPlayed = true;
				if (Defines.stopSound != null) {
					Defines.stopSound();
				}
				
				if (Defines.playSound != null) {
					Defines.playSound(prop.soundName, false);
				}
			}
		}
		
		override public function clone():Entity
		{
			var unit:SoundUnit = new SoundUnit();
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function syncParamsToMaterial():void
		{
			super.syncParamsToMaterial();
			if (Defines.stopSound != null) {
				Defines.stopSound();
			}
		}
		
		override public function dispose():void
		{
			super.dispose();
			if (Defines.stopSound != null) {
				Defines.stopSound();
			}
		}
	}
}