/*@author lye 123*/

package FlashGE.effects
{
	import flash.geom.Vector3D;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.materials.Material;
	import FlashGE.objects.Entity;
	import FlashGE.render.Camera3D;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;
	
	public class EffectUnitBase extends Entity
	{				
		FlashGE var _show:Boolean = true;
		public var preRenderTime:Number;
		public var unitProp:UnitBaseProp;
		public var system:Effect;
		public var uvCurUScale:Number;
		public var uvCurVScale:Number;
		public var uvCurUOffset:Number;
		public var uvCurVOffset:Number;
		public var uvCurV0:Number;
		public var uvCurV1:Number;

		protected var beforeRenderListener:Function;
		
		public function TYPE():uint
		{
			return EffectProp.TYPE_NULL;
		}
		
		public function rotateFaceNormal(dx:Number, dy:Number, dz:Number):void
		{
		}
		
		
		public function resetFaceNormal():void
		{
		}
		
		public function contructor(unitProp:UnitBaseProp=null):void
		{
			_show = true;
			preRenderTime = 0;
			uvCurUScale = 1;
			uvCurVScale = 1;
			uvCurUOffset = 0.5;
			uvCurVOffset = 0.5;
			uvCurV0 = 0;
			uvCurV1 = 0;
			
			this.unitProp = unitProp;
			if (this.material != null) {
				this.material.release();
				this.material = null;
			}
			
			this.material = Material.createMaterial();
			this.material.type = Defines.MATERIAL_EFFECT;
			if (this.unitProp != null) {
				this.syncParamsToMaterial();
			}
		}
		
		public function setBeforeRenderListener(listener:Function):void
		{
			beforeRenderListener = listener;
		}
		
		public function getUnitProp():UnitBaseProp
		{
			return this.unitProp;
		}
		
		override public function getName():String
		{
			return this.unitProp.name;
		}
		
		override public function setName(v:String):void
		{
			this.unitProp.name = v;
		}
		
		public function getFollowNode():Entity
		{
			var followNode:Entity = null;
			if (this.unitProp.followHolder) {
				followNode = this.system.getLinkedHolder();
				if (followNode != null) return followNode;
			}
			
			followNode = this.system.getUnitByName(this.unitProp.followUnitName);
			if (followNode == null) {
				return this.system;
			}
			return followNode;
		}
		
		public function getFollowUnitName():String
		{
			return this.unitProp.followUnitName;
		}
		
		public function setFollowUnitName(value:String):void
		{
			this.unitProp.followUnitName = value;
		}
		
		public function isShow():Boolean
		{
			return this._show;
		}
		
		public function setShow(show:Boolean):void
		{
			this._show = show;
		}

		public function checkSave():void
		{
			
		}
		
		FlashGE function updateBeforRender(camera:Camera3D):void 
		{
		}
		
		FlashGE function updateOneFrame(camera:Camera3D):void 
		{
			Global.throwError("need override")
		}
		
		public function getBeginFrame():Number
		{
			return this.unitProp.unitBeginFrame;
		}
		
		public function setBeginFrame(value:Number):void
		{
			this.unitProp.unitBeginFrame = value;
		}
		
		public function getLifeFramesNum():Number
		{
			return this.unitProp.lifeFramesNum;
		}
		
		public function setLifeFramesNum(value:Number):void
		{
			this.unitProp.lifeFramesNum = value;
		}
		
		public function clone():Entity
		{
			Global.throwError("need override");
			return null;
		}
		
		protected function clonePropertiesFrom(source:Entity):void
		{
			// 不要调用父类的clonePropertiesFrom
			var srcUnit:EffectUnitBase = source as EffectUnitBase;
			this.unitProp.clonePropertiesFrom(srcUnit.unitProp);
			this.setName(this.unitProp.name);
		}
		
		public function setProp(unitProp:UnitBaseProp):void
		{
			this.unitProp = unitProp;
			this.syncParamsToMaterial();
		}
		
		public function dispose():void
		{
			this.isValid = false;
			this.removeFromParent();
			if (this.material != null) {
				this.material.release();
				this.material = null;
			}
			this.unitProp = null;
			this.beforeRenderListener = null;
			this.system = null;
		}
		
		public function setTexture(index:int, fileName:String):void
		{
			if (fileName!=null) {
				if (index >= this.unitProp.textureNames.length) {
					this.unitProp.textureNames.push(fileName);
				} else {
					this.unitProp.textureNames[index] = fileName;
				}
				
				var tex:TextureFile = TextureFile.createFromFile(fileName, true);
				this.getMaterial().setTexture(index, tex);
				tex.release();
				
			} else {
				
				if (index < this.unitProp.textureNames.length) {
					this.unitProp.textureNames.splice(index, 1);
				}
				this.getMaterial().setTexture(index, null);
			}
		}
		
		override public function setMaterial(material:Material):void
		{
			super.setMaterial(material);
			if (material != null) {
				var texList:Vector.<TextureFile> = material.getTextures();
				var i:int = 0;
				var n:int = texList.length;
				this.unitProp.textureNames.length = n;
				for (i = 0; i < n; i++) {
					this.unitProp.textureNames[i] = texList[i].getFileName();
				}
			}
		}
		
		public function syncParamsToMaterial():void
		{
			if (this.unitProp == null) return;
			
			var tm:Material = this.getMaterial();
			var prop:UnitBaseProp = this.unitProp;
			tm.setUseUVType(prop.uvType);
			tm.setUV2UOffset(prop.uv2UOffset);
			tm.setUV2VOffset(prop.uv2VOffset);
			tm.setUVAngle(prop.uvAngle);
			tm.setDstBlend(prop.blendDstType);
			tm.setSrcBlend(prop.blendSrcType);
			tm.setRenderPriority(prop.renderPriority);
			tm.setTexOpIndex(prop.texOpIndex);
			tm.setRepeatIndex(prop.repeatIndex);
			tm.setMipIndex(prop.mipIndex);
			tm.setFilterIndex(prop.filterIndex);
			tm.setAlphaTest(prop.alphaTest);
			tm.setAlphaIntensity(prop.alphaIntensity);
			tm.setAlphaSharpness(prop.alphaSharpness);
			tm.setType(Defines.MATERIAL_EFFECT);
			tm.setReplaceAlpha(prop.replaceAlpha);
			tm.setOpenDoubleAlpha(false);
			for (var i:int = 0, n:int = unitProp.textureNames.length; i < n; i++ ) {
				
				var tex:TextureFile = TextureFile.createFromFile(unitProp.textureNames[i]);
				this.material.setTexture(i, tex);
				tex.release();
			}
		}
		
		public function getPercent():Number
		{
			var beginFrame:int = this.getBeginFrame();
			var lifeFrame:int = this.unitProp.lifeFramesNum;
			var curSysFrame:Number = system.getCurFrame();
			if (curSysFrame < beginFrame) {
				return -1;
			}
			
			var curUnitFrame:Number = curSysFrame - beginFrame;			
			return curUnitFrame / lifeFrame;
		}
		
		public function calcUV(time:Number):void
		{
			if (this.unitProp.uvType == Defines.UV_TYPE_FRAME) {
				var pos:int = time *this.unitProp.uvFPS;
				if (this.unitProp.uvLoop) {
					pos = pos%this.unitProp.uvRangeLength;
					if (pos < 0) 
						pos += this.unitProp.uvRangeLength;
				} else {
					if (pos < 0) 
						pos = 0;
					if (pos >= this.unitProp.uvRangeLength) 
						pos = this.unitProp.uvRangeLength - 1;
				}
				pos += this.unitProp.uvRangeBegin;
				var col:int = pos%this.unitProp.uvColumnsCount;
				var row:int = pos/this.unitProp.uvColumnsCount;
				this.uvCurUOffset = col/this.unitProp.uvColumnsCount;
				this.uvCurVOffset = row/this.unitProp.uvRowsCount;
				
				if (this.unitProp.uvAutoCenter) {
					this.uvCurUOffset += 1.0/(2.0*this.unitProp.uvColumnsCount);
					this.uvCurVOffset += 1.0/(2.0*this.unitProp.uvRowsCount);
				}
				
				this.uvCurUScale = this.unitProp.uvUScale / this.unitProp.uvColumnsCount;
				this.uvCurVScale = this.unitProp.uvVScale / this.unitProp.uvRowsCount;
			} else {
				this.uvCurUScale = this.unitProp.uvUScale;
				this.uvCurVScale = this.unitProp.uvVScale;
				this.uvCurUOffset = this.unitProp.uvUOffset;
				this.uvCurVOffset = this.unitProp.uvVOffset;
			}
			
			if (this.unitProp.uvType == Defines.UV_TYPE_NONE) {
				this.uvCurV0 = 0;
				this.uvCurV1 = 0;
			} else {
				this.uvCurV0 = this.unitProp.uvV0;
				this.uvCurV1 = this.unitProp.uvV1;
			}
		}
		
		FlashGE function getShaderArray(shaderArrray:Array, shaderArrayIndex:int, outValue:Object):int
		{
			// TODO Auto Generated method stub
			return 0;
		}
		
		public function resetAllParams():void
		{
			
		}
	}
}