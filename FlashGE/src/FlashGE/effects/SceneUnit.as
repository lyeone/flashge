/*@author lye 123*/

package FlashGE.effects
{
	import flash.filters.ColorMatrixFilter;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Global;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.SceneUnitProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.objects.Entity;
	import FlashGE.render.Camera3D;

	use namespace FlashGE;
	
	public class SceneUnit extends EffectUnitBase	
	{
		private var scaleKey:KeyPair;
		private var filterArray:Array;
		private var camera:Camera3D;
		
		override public function contructor(unitProp:UnitBaseProp = null):void
		{
			super.contructor(unitProp);
			
			this.scaleKey = new KeyPair;
			this.scaleKey.init(0);
			
			if (unitProp == null) {
				var newProp:SceneUnitProp = new SceneUnitProp;
				newProp.contructor();
				this.setProp(newProp);
			} else {
				this.setProp(unitProp);
			}
		}
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_SCENE;
		}
		
		override FlashGE function updateOneFrame(camera:Camera3D):void			
		{	
			if (!isShow())
				return;
			
			var percent:Number = getPercent();
			if (percent <0 || percent >1) return;
			this.camera = camera;
			var prop:SceneUnitProp = unitProp as SceneUnitProp;
			
			this.getInterpolationValue(percent, prop.scaleKeys, scaleKey);
			
			if (prop.applyType&0x1) {
				camera.mulColorFactor = scaleKey.items[0];
				camera.addColorFactor = scaleKey.items[1];
			}
			
			if (prop.applyType&0x2) {
				filterArray = filterArray || [
					1,0,0,0,0,
					0,1,0,0,0,
					0,0,1,0,0,
					0,0,0,1,0];
	
				filterArray[0] = scaleKey.items[2];
				filterArray[6] = scaleKey.items[2];
				filterArray[12] = scaleKey.items[2];
				filterArray[4] = scaleKey.items[3];
				filterArray[9] = scaleKey.items[3];
				filterArray[14] = scaleKey.items[3];
				
				Global.mainSprite.filters = [new ColorMatrixFilter(filterArray)];
			}
		}
		
		override public function clone():Entity
		{
			var unit:SceneUnit = new SceneUnit();
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function dispose():void
		{
			if (this.camera != null) {
				camera.mulColorFactor = 1;
				camera.addColorFactor = 0;
				Global.mainSprite.filters = [];
			}
			super.dispose();
		}
		
		override public function syncParamsToMaterial():void
		{
			super.syncParamsToMaterial();
			if (this.camera != null) {
				Global.mainSprite.filters = [];
			}
		}
	}
}