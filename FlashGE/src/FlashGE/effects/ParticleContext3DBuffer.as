/*@author lye 123*/

package FlashGE.effects
{
	import flash.display3D.Context3D;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.Program3D;
	import flash.display3D.VertexBuffer3D;
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	
	import FlashGE.FlashGE;
	
	use namespace FlashGE;
	
	public class ParticleContext3DBuffer
	{
		public var curContext3D:Context3D;
		public var curVertexBuffer:VertexBuffer3D;
		public var curIndexBuffer:IndexBuffer3D;
		public var curProgram3D:Program3D;
		public var curLimit:int;
		public var vertexBytes:ByteArray;
		public var fragmentBytes:ByteArray;
		public var vertices:Vector.<Number>;
		public var indices:Vector.<uint>;
		public var curStepIndex:int;
		
		public function read(rawData:ByteArray):void
		{
			this.curLimit = rawData.readUnsignedByte();
			this.curStepIndex = rawData.readUnsignedByte();
			var bufferLen:int;
			bufferLen = rawData.readUnsignedShort();
			this.vertexBytes = new ByteArray;
			this.vertexBytes.endian = Endian.LITTLE_ENDIAN;
			rawData.readBytes(this.vertexBytes, 0, bufferLen);
			this.vertexBytes.position = 0;
			
			bufferLen = rawData.readUnsignedShort();
			this.fragmentBytes = new ByteArray;
			this.fragmentBytes.endian = Endian.LITTLE_ENDIAN;
			rawData.readBytes(this.fragmentBytes, 0, bufferLen);
			this.fragmentBytes.position = 0;
		}
		
		public function write(rawData:ByteArray):void
		{
			rawData.writeByte(this.curLimit);
			rawData.writeByte(this.curStepIndex);
			var bufferLen:int;
			bufferLen = this.vertexBytes.length;
			rawData.writeShort(bufferLen);
			rawData.writeBytes(this.vertexBytes, 0, bufferLen);
			
			bufferLen = this.fragmentBytes.length;
			rawData.writeShort(bufferLen);
			rawData.writeBytes(this.fragmentBytes, 0, bufferLen);
		}
	}
}