/*@author lye 123*/

package FlashGE.effects
{
	import FlashGE.FlashGE;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.effects.unitProp.WaterUnitProp;
	import FlashGE.objects.Entity;
	import FlashGE.render.Camera3D;

	use namespace FlashGE;
	
	public class WaterUnit extends EffectUnitBase	
	{
		override public function contructor(unitProp:UnitBaseProp = null):void
		{
			super.contructor(unitProp);
						
			if (unitProp == null) {
				var newProp:WaterUnitProp = new WaterUnitProp;
				newProp.contructor();
				this.setProp(newProp);
			} else {
				this.setProp(unitProp);
			}
		}
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_WATER;
		}
		
		override FlashGE function updateOneFrame(camera:Camera3D):void			
		{	
			if (!isShow())
				return;
			
			var percent:Number = getPercent();
			if (percent <0 || percent >1) {
				return;
			}
		}
		
		override public function clone():Entity
		{
			var unit:WaterUnit = new WaterUnit();
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function dispose():void
		{
			super.dispose();
		}
		
		override public function syncParamsToMaterial():void
		{
			super.syncParamsToMaterial();
		}
	}
}