/*@author lye 123*/
package FlashGE.effects.unitProp
{
	import flash.utils.ByteArray;
	
	import FlashGE.FlashGE;

	use namespace FlashGE;
	public class EmptyUnitProp extends SpriteUnitProp
	{
		FlashGE var openSignature:Boolean;
		FlashGE var fxFileName:String;
				
		override public function contructor():void
		{
			super.contructor();	
			this.width 	= 1;
			this.height = 1;
		}
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_EMPTY;
		}
		
		override public function clone():UnitBaseProp
		{
			var unit:EmptyUnitProp = new EmptyUnitProp();
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		
		override public function clonePropertiesFrom(source:UnitBaseProp):void
		{
			super.clonePropertiesFrom(source);
			
			this.openSignature = (source as EmptyUnitProp).openSignature;
			this.fxFileName = (source as EmptyUnitProp).fxFileName;
		}
		
		override public function readRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawData(effectProp, rawData, version);
			if (version > EffectProp.VERSION_MODIFY_EMTPY_UNIT) {
				this.openSignature = rawData.readBoolean();
				var bytesLength:int = rawData.readUnsignedByte();
				this.fxFileName = rawData.readUTFBytes(bytesLength);
			}
		}
		
		override public function writeRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.writeRawData(effectProp, rawData, version);
			
			rawData.writeBoolean(openSignature);
			var bytesLength:int = fxFileName!=null ? fxFileName.length : 0;
			rawData.writeByte(bytesLength);
			if (fxFileName != null) rawData.writeUTFBytes(fxFileName);
		}
	}
}