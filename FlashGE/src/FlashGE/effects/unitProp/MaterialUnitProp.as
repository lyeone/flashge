package FlashGE.effects.unitProp
{
	import flash.utils.ByteArray;
	
	import FlashGE.common.Defines;
	import FlashGE.effects.KeyPair;

	public class MaterialUnitProp extends UnitBaseProp
	{
		static public const CLONE_NONE:int = 0;
		static public const CLONE_ALL:int = 1;
		static public const CLONE_NONE_FORCE_HIDE:int = 2;
		static public const CLONE_NONE_FORCE_SHOW:int = 3;
		static public const CLONE_NONE_KEEP:int = 4;
		
		public var cloneHoster:int; 
		public var aniSpeed:int;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_MATERIAL;
		}
		
		override public function contructor():void
		{
			super.contructor();
			
			var scaleKey:KeyPair = new KeyPair;
			scaleKey.init(0,1,1,1);
			scaleKeys.push(scaleKey);
			
			var colorKey:KeyPair = new KeyPair;
			colorKey.init(0,1,1,1,1);
			colorKeys.push(colorKey);
			
			var rotateKey:KeyPair = new KeyPair;
			rotateKey.init(0,0,0,0);
			rotateKeys.push(rotateKey);
			
			var offsetKey:KeyPair = new KeyPair;
			offsetKey.init(0,0,0,0);
			offsetKeys.push(offsetKey);
			
			this.uvUOffset = this.uvVOffset = 0;
			this.uvAutoCenter = false;
			
			this.uvType = Defines.UV_TYPE_TRANS;
			this.renderPriority = Defines.PRIORITY_OPAQUE;
			this.blendDstType = Defines.BLEND_ZERO;
			this.mipIndex = 0;
			this.cloneHoster = 0; 
			this.aniSpeed = 1;
		}
		
		override public function clone():UnitBaseProp
		{
			var unit:MaterialUnitProp = new MaterialUnitProp();
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function clonePropertiesFrom(source:UnitBaseProp):void
		{
			super.clonePropertiesFrom(source);
			var unit:MaterialUnitProp = source as MaterialUnitProp;
			this.cloneHoster = unit.cloneHoster;
			this.aniSpeed = unit.aniSpeed;
		}
		
		override public function readRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawData(effectProp, rawData, version);
			this.cloneHoster = rawData.readByte();
			if (version > EffectProp.VERSION_ADD_ANI_SPEED) {
				this.aniSpeed = rawData.readByte();
			}
		}
		
		override public function writeRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.writeRawData(effectProp, rawData, version);
			rawData.writeByte(this.cloneHoster);
			rawData.writeByte(this.aniSpeed);
		}
	}
}