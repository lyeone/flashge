/*@author lye 123*/
package FlashGE.effects.unitProp
{
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	
	import FlashGE.effects.KeyPair;

	public class CameraShakeUnitProp extends UnitBaseProp
	{
		public var freq:Number;
		public var attenuation:Number;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_CAMERA_SHAKE;
		}
				
		override public function contructor():void
		{
			super.contructor();
			var keyPair:KeyPair = new KeyPair;
			keyPair.init(0,0,0,0);
			offsetKeys.push(keyPair);
			freq = 1;
			attenuation = 30;
		}
		
		override public function clone():UnitBaseProp
		{
			var unit:CameraShakeUnitProp = new CameraShakeUnitProp();
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function clonePropertiesFrom(source:UnitBaseProp):void
		{
			super.clonePropertiesFrom(source);
			var srcUnit:CameraShakeUnitProp = source as CameraShakeUnitProp;
			this.freq = srcUnit.freq;
			this.attenuation = srcUnit.freq;
		}
		
		override public function readRawDataPreVersion(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawDataPreVersion(effectProp, rawData, version);	
			if (version > 5) {
				this.freq = rawData.readFloat();
				this.attenuation = rawData.readFloat();
			}
		}
		
		override public function readRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawData(effectProp, rawData, version);	
			this.freq = rawData.readFloat();
			this.attenuation = rawData.readFloat();
		}
		
		override public function writeRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.writeRawData(effectProp, rawData, version);
			rawData.writeFloat(this.freq);
			rawData.writeFloat(this.attenuation);			
			
		}
	}
}