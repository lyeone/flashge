/*@author lye 123*/
package FlashGE.effects.unitProp
{
	import flash.geom.Vector3D;
	import flash.utils.ByteArray;
	
	import FlashGE.common.Transform3D;
	import FlashGE.effects.KeyPair;

	public class SpriteUnitProp extends UnitBaseProp
	{		
		public var width:Number;
		public var height:Number;
		
		public var followType:uint;
		public var dirNormal:Vector3D;
		
		public var faceType:uint;
		
		public var selfRotateAxis:Vector3D;
		public var selfOriginRotate:Number;
		public var normalTrans:Transform3D;
		
		public var tempTransA:Transform3D;
		public var tempTransB:Transform3D;
		public var usePreVersion:int;
		public var completelyFollow:Boolean;
		
		override public function contructor():void
		{
			super.contructor();
			
			var scaleKey:KeyPair = new KeyPair;
			scaleKey.init(0,1,1,1);
			scaleKeys.push(scaleKey);
			
			var colorKey:KeyPair = new KeyPair;
			colorKey.init(0,1,1,1,1);
			colorKeys.push(colorKey);
			
			var rotateKey:KeyPair = new KeyPair;
			rotateKey.init(0,0,0,0);
			rotateKeys.push(rotateKey);
			
			var offsetKey:KeyPair = new KeyPair;
			offsetKey.init(0,0,0,0);
			offsetKeys.push(offsetKey);
			
			this.name = "sprite";
			this.uvType = 0;
			this.uvColumnsCount = 1;
			this.uvRowsCount = 1;
			this.uvRangeBegin = 0;
			this.uvRangeLength = 0;
			this.uvFPS = 0;
			
			this.width = 100;
			this.height = 100;
			this.followType = DIR_TYPE_PARENT;
			this.dirNormal = new Vector3D(0,0,0);
			this.faceType = DIR_TYPE_PARENT;
			this.selfRotateAxis = new Vector3D(0,0,1);
			this.selfOriginRotate = 0;
			this.normalTrans = new Transform3D;
			this.tempTransA = new Transform3D;
			this.tempTransB = new Transform3D;
			this.usePreVersion = 2;
			this.completelyFollow = true;
		}
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_SPRITE;
		}
		
		override public function readRawDataPreVersion(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawDataPreVersion(effectProp, rawData, version);
			this.width = rawData.readFloat();
			this.height = rawData.readFloat();
			this.followType = rawData.readUnsignedByte();
			this.dirNormal.x = rawData.readFloat();
			this.dirNormal.y = rawData.readFloat();
			this.dirNormal.z = rawData.readFloat();			
			this.selfRotateAxis.x = rawData.readFloat();
			this.selfRotateAxis.y = rawData.readFloat();
			this.selfRotateAxis.z = rawData.readFloat();
			this.selfOriginRotate = rawData.readFloat();
			this.completelyFollow = true;
			if (version <= 3)
				this.usePreVersion = 1;
			else
				this.usePreVersion = (rawData.readBoolean() ? 1 : 0);
			
			if(version<=15) {
				this.faceType = this.followType;
				if (this.followType != UnitBaseProp.DIR_TYPE_PARENT)
					this.followType = UnitBaseProp.DIR_TYPE_WORLD;
			} else {
				this.faceType = rawData.readByte();
			}
			
			calcNormal();
		}
		
		override public function readRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawData(effectProp, rawData, version);
			this.width = rawData.readFloat();
			this.height = rawData.readFloat();
			this.followType = rawData.readUnsignedByte();
			this.dirNormal.x = rawData.readFloat();
			this.dirNormal.y = rawData.readFloat();
			this.dirNormal.z = rawData.readFloat();			
			this.selfRotateAxis.x = rawData.readFloat();
			this.selfRotateAxis.y = rawData.readFloat();
			this.selfRotateAxis.z = rawData.readFloat();
			this.selfOriginRotate = rawData.readFloat();
			if (version <= EffectProp.VERSION_NEW_FACE_NORMAL) {
				this.usePreVersion = rawData.readBoolean() ? 1 : 0;
			} else {
				this.usePreVersion = rawData.readByte();
			}
			
			this.faceType = rawData.readByte();
			if (version > EffectProp.VERSION_SPRITE_ADD_FOLLOW) {
				this.completelyFollow = rawData.readBoolean();
			} else {
				this.completelyFollow = true;
			}
			if (version <= EffectProp.VERSION_CHANGE_FPS) {
				var rotateAsFrame:int;
				if (version <= EffectProp.VERSION_ROTATE_AS_FRAME) {
					rotateAsFrame = 1;
				} else {
					rotateAsFrame = rawData.readByte();
				}
				if (rotateAsFrame == 1) {
					for (var i:int, n:int = this.rotateKeys.length; i < n; i++) {
						this.rotateKeys[i].items[0] /= (1/24);
					}
				}
			}
			calcNormal();
		}
		
		override public function writeRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.writeRawData(effectProp, rawData, version);	
			rawData.writeFloat(this.width);
			rawData.writeFloat(this.height);
			rawData.writeByte(this.followType);
			rawData.writeFloat(this.dirNormal.x);
			rawData.writeFloat(this.dirNormal.y);
			rawData.writeFloat(this.dirNormal.z);
			rawData.writeFloat(this.selfRotateAxis.x);
			rawData.writeFloat(this.selfRotateAxis.y);
			rawData.writeFloat(this.selfRotateAxis.z);
			rawData.writeFloat(this.selfOriginRotate);
			rawData.writeByte(this.usePreVersion);
			rawData.writeByte(this.faceType);
			rawData.writeBoolean(this.completelyFollow);
			//rawData.writeByte(this.rotateAsFrame);
		}
		
		override public function clone():UnitBaseProp
		{
			var unit:SpriteUnitProp = new SpriteUnitProp();
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function clonePropertiesFrom(source:UnitBaseProp):void
		{
			super.clonePropertiesFrom(source);
			var srcUnit:SpriteUnitProp = source as SpriteUnitProp;
			this.width = srcUnit.width;
			this.height = srcUnit.height;
			this.followType = srcUnit.followType;
			this.dirNormal = srcUnit.dirNormal.clone();
			this.selfRotateAxis = srcUnit.selfRotateAxis.clone();
			this.selfOriginRotate = srcUnit.selfOriginRotate;
			this.faceType = srcUnit.faceType;
			this.usePreVersion = srcUnit.usePreVersion;
			this.calcNormal();
			//this.rotateAsFrame = srcUnit.rotateAsFrame;
		}
		
		public function calcNormal():void
		{
			if (this.usePreVersion < 2) {
				var dirA:Vector3D = new Vector3D(0,0,1);
				var dirB:Vector3D = this.dirNormal.clone();
				dirB.normalize();
				if (dirB.x ==0 && dirB.y==0 && dirB.z==1) {
					this.normalTrans.fromAxisAngle(0, -1, 0, 0);
					return;
				}
				
				var dirN:Vector3D = dirA.crossProduct(dirB);
				dirN.normalize();
				var dot:Number = dirA.dotProduct(dirB);
				this.normalTrans.fromAxisAngle(dirN.x, dirN.y, dirN.z, Math.acos(dot));
			} else {
				this.normalTrans.compose(0,0,0, this.dirNormal.x, this.dirNormal.y, this.dirNormal.z, 1,1,1);
			}
		}
	}
}