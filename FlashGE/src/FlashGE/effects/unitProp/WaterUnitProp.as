/*@author lye 123*/
package FlashGE.effects.unitProp
{
	import flash.utils.ByteArray;
	
	import FlashGE.FlashGE;
	import FlashGE.effects.KeyPair;
	
	use namespace FlashGE;
	public class WaterUnitProp extends UnitBaseProp
	{
		public var applyType:int;
		override public function TYPE():uint
		{
			return EffectProp.TYPE_WATER;
		}
				
		override public function contructor():void
		{
			super.contructor();
			
			var scaleKey:KeyPair = new KeyPair;
			scaleKey.init(0,1,0,1,0);
			scaleKeys.push(scaleKey);
			
			var colorKey:KeyPair = new KeyPair;
			colorKey.init(0,1,1,1,1);
			colorKeys.push(colorKey);
			
			var rotateKey:KeyPair = new KeyPair;
			rotateKey.init(0,0,0,0);
			rotateKeys.push(rotateKey);
			
			var offsetKey:KeyPair = new KeyPair;
			offsetKey.init(0,0,0,0);
			offsetKeys.push(offsetKey);
			applyType = 1;
		}
		
		override public function clone():UnitBaseProp
		{
			var unit:WaterUnitProp = new WaterUnitProp;
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function clonePropertiesFrom(source:UnitBaseProp):void
		{
			super.clonePropertiesFrom(source);
			var unit:WaterUnitProp = source as WaterUnitProp;
			this.applyType = unit.applyType;
		}
		
		override public function readRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawData(effectProp, rawData, version);
			applyType = rawData.readByte();
		}
		
		override public function writeRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.writeRawData(effectProp, rawData, version);
			rawData.writeByte(this.applyType);
		}
	}
}