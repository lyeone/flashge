/*@author lye 123*/
package FlashGE.effects.unitProp
{
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.effects.EffectGroup;
	import FlashGE.effects.KeyPair;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;
	public class ModelUnitProp extends UnitBaseProp
	{
		static public var SKIN_NUM:int = 3;
		static public var LIMIT:int = 32;
		
		public var cullingType:int;
		public var skinTexsA:Vector.<String>;
		public var skinTexsB:Vector.<String>;
		public var skinNames:Vector.<String>;
		public var aniLoop:Boolean;
		public var modelName:String;
		public var aniName:String;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_MODEL;
		}
		
		override public function contructor():void
		{
			super.contructor();
			var scaleKey:KeyPair = new KeyPair;
			scaleKey.init(0,1,1,1);
			scaleKeys.push(scaleKey);
			
			var colorKey:KeyPair = new KeyPair;
			colorKey.init(0,1,1,1,1);
			colorKeys.push(colorKey);
			
			var rotateKey:KeyPair = new KeyPair;
			rotateKey.init(0,0,0,0);
			rotateKeys.push(rotateKey);
			
			var offsetKey:KeyPair = new KeyPair;
			offsetKey.init(0,0,0,0);
			offsetKeys.push(offsetKey);
			
			this.cullingType = 0;
			this.skinTexsA = new Vector.<String>(SKIN_NUM);
			this.skinTexsB = new Vector.<String>(SKIN_NUM);
			this.skinNames = new Vector.<String>(SKIN_NUM);
			for(var j:int = 0; j < ModelUnitProp.SKIN_NUM; j++) {
				this.skinTexsA[j] = this.skinTexsB[j] = this.skinNames[j] = ""
			}
			this.aniLoop = false;
			this.modelName = "";
			this.aniName = "";
			this.uvUOffset = this.uvVOffset = 0;
			this.uvType = 1;
			this.uvAutoCenter = false;
			this.renderPriority = Defines.PRIORITY_OPAQUE;
		}
		
		override public function clone():UnitBaseProp
		{
			var unit:ModelUnitProp = new ModelUnitProp;
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function clonePropertiesFrom(source:UnitBaseProp):void
		{
			super.clonePropertiesFrom(source);
			var unit:ModelUnitProp = source as ModelUnitProp;
			this.aniLoop = unit.aniLoop;
			this.cullingType = unit.cullingType;
			this.aniName = unit.aniName;
			this.modelName = unit.modelName;
			var i:int;
			for (i=0; i<SKIN_NUM;i++) {
				this.skinNames[i] = unit.skinNames[i];
				this.skinTexsA[i] = unit.skinTexsA[i];
				this.skinTexsB[i] = unit.skinTexsB[i];
			}
		}
		
		override public function readRawDataPreVersion(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawDataPreVersion(effectProp, rawData, version);
			
			if (version > 4)
				this.aniLoop = rawData.readBoolean();
			
			var bytesLength:int = rawData.readUnsignedByte();
			modelName = rawData.readUTFBytes(bytesLength);
			
			if (EffectGroup.filterFunction!=null) {
				modelName = EffectGroup.filterFunction(modelName);
			}			
			
			bytesLength = rawData.readUnsignedByte();
			aniName = rawData.readUTFBytes(bytesLength);
			if (EffectGroup.filterFunction!=null) {
				aniName = EffectGroup.filterFunction(aniName);
			}
			
			this.cullingType = rawData.readUnsignedByte();
			
			if (version >= 2) {
				var i:int = 0;
				var n:int = rawData.readUnsignedByte();
				var texFileName:String;
				for (i=0; i < n; i++){
					bytesLength = rawData.readUnsignedByte();
					skinNames[i] = rawData.readUTFBytes(bytesLength);
					
					bytesLength = rawData.readUnsignedShort();
					texFileName = rawData.readUTFBytes(bytesLength);
					if (TextureFile.forceATF && texFileName != null) {
						texFileName = texFileName.replace(".tga", ".atf");
					}
					skinTexsA[i] = EffectProp.filterURL(texFileName);
				}
				
				if (version > 10) {
					for (i=0; i < n; i++){
						bytesLength = rawData.readUnsignedShort();
						texFileName = rawData.readUTFBytes(bytesLength);
						if (TextureFile.forceATF && texFileName != null) {
							texFileName = texFileName.replace(".tga", ".atf");
						}
						skinTexsB[i] = texFileName;
					}
				}
			}
			
			
		}
		
		override public function readRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawData(effectProp, rawData, version);
			
			this.aniLoop = rawData.readBoolean();
			
			var bytesLength:int = rawData.readUnsignedByte();
			modelName = rawData.readUTFBytes(bytesLength);
			if (EffectGroup.filterFunction!=null) {
				modelName = EffectGroup.filterFunction(modelName);
			}
			
			bytesLength = rawData.readUnsignedByte();
			aniName = rawData.readUTFBytes(bytesLength);
			aniName = EffectProp.filterURL(aniName);
			
			this.cullingType = rawData.readUnsignedByte();
			
			var i:int = 0;
			var n:int = rawData.readUnsignedByte();
			var texFileName:String;
			for (i=0; i < n; i++){
				bytesLength = rawData.readUnsignedByte();
				skinNames[i] = rawData.readUTFBytes(bytesLength);
				
				bytesLength = rawData.readUnsignedShort();
				texFileName = rawData.readUTFBytes(bytesLength);
				if (EffectGroup.filterFunction!=null) {
					skinTexsA[i] = EffectGroup.filterFunction(skinTexsA[i]);
				}
				
				if (TextureFile.forceATF && texFileName != null) {
					texFileName = texFileName.replace(".tga", ".atf");
				}
				
				skinTexsA[i] = texFileName;
			}
			
			for (i=0; i < n; i++){
				bytesLength = rawData.readUnsignedShort();
				texFileName = rawData.readUTFBytes(bytesLength);
				if (EffectGroup.filterFunction != null) {
					texFileName = EffectGroup.filterFunction(skinTexsB[i]);
				}
				
				if (TextureFile.forceATF && texFileName != null) {
					texFileName = texFileName.replace(".tga", ".atf");
				}
				skinTexsB[i] = texFileName;
			}
			
		}
		
		override public function writeRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.writeRawData(effectProp, rawData, version);
			
			var i:int = 0;
			rawData.writeBoolean(this.aniLoop);
			var bytesLength:uint = this.modelName.length;
			rawData.writeByte(bytesLength);
			rawData.writeUTFBytes(this.modelName);
			bytesLength = aniName.length;
			rawData.writeByte(bytesLength);
			rawData.writeUTFBytes(aniName);
			rawData.writeByte(this.cullingType);
			var url:String;
			rawData.writeByte(SKIN_NUM);
			for (i=0; i < SKIN_NUM; i++){
				bytesLength = skinNames[i].length;
				rawData.writeByte(bytesLength);
				rawData.writeUTFBytes(skinNames[i]);
				
				url = skinTexsA[i];
				if (EffectGroup.filterFunction!=null) {
					url = EffectGroup.filterFunction(url);
				}
				bytesLength = url.length;
				rawData.writeShort(bytesLength);
				rawData.writeUTFBytes(url);
			}
			
			for (i=0; i < SKIN_NUM; i++){
				url = skinTexsB[i];
				if (EffectGroup.filterFunction!=null) {
					url = EffectGroup.filterFunction(url);
				}
				bytesLength = url.length;
				rawData.writeShort(bytesLength);
				rawData.writeUTFBytes(url);
			}
		}
		
		override public function enumAllTextures(outRes:Dictionary):void
		{
			super.enumAllTextures(outRes);
			var i:int, n:int;
			for (i = 0, n = this.skinTexsA.length; i < n; i++) {
				outRes[this.skinTexsA[i]] = true;
			}
			
			for (i = 0, n = this.skinTexsB.length; i < n; i++) {
				outRes[this.skinTexsB[i]] = true;
			}
		}
		
	}
}