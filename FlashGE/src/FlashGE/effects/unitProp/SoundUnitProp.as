/*@author lye 123*/
package FlashGE.effects.unitProp
{
	import flash.utils.ByteArray;
	
	import FlashGE.FlashGE;
	import FlashGE.effects.KeyPair;
	
	use namespace FlashGE;
	public class SoundUnitProp extends UnitBaseProp
	{
		FlashGE var soundName:String;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_SOUND;
		}
		
		override public function contructor():void
		{
			super.contructor();
			
			var scaleKey:KeyPair = new KeyPair;
			scaleKey.init(0,1,1,1);				
			scaleKeys.push(scaleKey);
			
			var colorKey:KeyPair = new KeyPair;
			colorKey.init(0,1,1,1,1);
			colorKeys.push(colorKey);
			
			var rotateKey:KeyPair = new KeyPair;
			rotateKey.init(0,0,0,0);
			rotateKeys.push(rotateKey);
			
			var offsetKey:KeyPair = new KeyPair;
			offsetKey.init(0,0,0,0);
			offsetKeys.push(offsetKey);
		}
		
		override public function clone():UnitBaseProp
		{
			var unit:SoundUnitProp = new SoundUnitProp;
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function clonePropertiesFrom(source:UnitBaseProp):void
		{
			super.clonePropertiesFrom(source);
			var unit:SoundUnitProp = source as SoundUnitProp;
			this.soundName = unit.soundName;
		}
		
		override public function readRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawData(effectProp, rawData, version);
			
			var bytesLength:int = rawData.readUnsignedShort();
			this.soundName = rawData.readUTFBytes(bytesLength);
		}
		
		override public function writeRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.writeRawData(effectProp, rawData, version);
			
			var bytesLength:int = this.soundName.length;
			rawData.writeShort(bytesLength);
			if (bytesLength > 0) {
				rawData.writeUTFBytes(soundName);
			}
		}
	}
}