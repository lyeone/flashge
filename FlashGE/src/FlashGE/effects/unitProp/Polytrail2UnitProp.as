/*@author lye 123*/
package FlashGE.effects.unitProp
{
	import flash.utils.ByteArray;
	
	import FlashGE.FlashGE;
	import FlashGE.effects.KeyPair;

	use namespace FlashGE;
	public class Polytrail2UnitProp extends UnitBaseProp
	{
		/** 朝向类型 **/ 
		public static const FACETO_NORMAL:uint = 0;
		public static const FACETO_CAMERA:uint = 1;
		
		/** 宽度扩展类型 **/
		public static const WIDTHEXTEND_ALL:uint = 0; 
		public static const WIDTHEXTEND_LEFT:uint = 1;
		public static const WIDTHEXTEND_RIGHT:uint = 2;
		
		/** 贴图方式 **/
		public static const UVTYPE_DRAW:uint = 0;
		public static const UVTYPE_GRID:uint = 1;
		
		public var trailLinkName:String;
		public var trailLife:Number;
		public var trailSmoothFactor:Number;
		public var trailFaceTo:uint;
		public var trailWidthExtend:uint;
		public var trailUvType:uint;
		public var trailRotateX:Number;
		public var trailRotateY:Number;
		public var trailRotateZ:Number;
		public var trailStepTime:Number; 
		public var trailWidthKeys:Vector.<KeyPair>;
		public var trailColorKeys:Vector.<KeyPair>;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_POLYTRAIL2
		}
		
		override public function contructor():void
		{
			super.contructor();
			
			var scaleKey:KeyPair = new KeyPair;
			scaleKey.init(0,1,1,1);
			this.scaleKeys.push(scaleKey);
			
			var colorKey:KeyPair = new KeyPair;
			colorKey.init(0,1,1,1,1);
			this.colorKeys.push(colorKey);
			
			var rotateKey:KeyPair = new KeyPair;
			rotateKey.init(0,0,0,0);
			this.rotateKeys.push(rotateKey);
			
			var offsetKey:KeyPair = new KeyPair;
			offsetKey.init(0,0,0,0);
			this.offsetKeys.push(offsetKey);
			
			this.name = "Polytrail2";
			var trailWidthKey:KeyPair = new KeyPair;
			trailWidthKey.init(0,10);
			this.trailWidthKeys = new Vector.<KeyPair>;
			this.trailWidthKeys.push(trailWidthKey);
			
			var trailColorKey:KeyPair = new KeyPair;
			trailColorKey.init(0,1,1,1,1);
			this.trailColorKeys = new Vector.<KeyPair>;
			this.trailColorKeys.push(trailColorKey);
			
			this.trailLinkName = "";
			this.trailLife = 200;
			this.trailSmoothFactor = 1;
			this.trailFaceTo = FACETO_NORMAL;
			this.trailWidthExtend = WIDTHEXTEND_ALL;
			this.trailUvType = UVTYPE_DRAW;
			this.trailRotateX = 0;
			this.trailRotateY = 0;
			this.trailRotateZ = 1;
			this.trailStepTime = 30; 
		}
		
		override public function writeRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.writeRawData(effectProp, rawData, version);
			this.writeKeys(this.trailWidthKeys, rawData);
			this.writeKeys(this.trailColorKeys, rawData);
			rawData.writeByte(this.trailLinkName.length);
			rawData.writeUTFBytes(this.trailLinkName);
			rawData.writeFloat(this.trailLife);
			rawData.writeFloat(this.trailSmoothFactor);
			rawData.writeByte(this.trailFaceTo);
			rawData.writeByte(this.trailWidthExtend);
			rawData.writeByte(this.trailUvType);
			rawData.writeFloat(this.trailRotateX);
			rawData.writeFloat(this.trailRotateY);
			rawData.writeFloat(this.trailRotateZ);
			rawData.writeFloat(this.trailStepTime);
		}
		
		override public function readRawDataPreVersion(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawDataPreVersion(effectProp, rawData, version);
			this.readKeys(this.trailWidthKeys, rawData);
			this.readKeys(this.trailColorKeys, rawData);
			
			var len:int = rawData.readUnsignedByte();
			this.trailLinkName = rawData.readUTFBytes(len);
			if (len > 0) {
				effectProp.linkedNamesList ||= new Vector.<String>
				effectProp.linkedNamesList.push(this.trailLinkName);
			}
			this.trailLife = rawData.readFloat();
			this.trailSmoothFactor = rawData.readFloat();
			this.trailFaceTo = rawData.readUnsignedByte();
			this.trailWidthExtend = rawData.readUnsignedByte();
			this.trailUvType = rawData.readUnsignedByte();
			this.trailRotateX = rawData.readFloat();
			this.trailRotateY = rawData.readFloat();
			this.trailRotateZ = rawData.readFloat();
			this.trailStepTime = rawData.readFloat();
		}
		
		override public function readRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawData(effectProp, rawData, version);
			this.readKeys(this.trailWidthKeys, rawData);
			this.readKeys(this.trailColorKeys, rawData);
			
			var len:int = rawData.readUnsignedByte();
			this.trailLinkName = rawData.readUTFBytes(len);
			if (len > 0) {
				effectProp.linkedNamesList ||= new Vector.<String>
				effectProp.linkedNamesList.push(this.trailLinkName);
			}
			this.trailLife = rawData.readFloat();
			this.trailSmoothFactor = rawData.readFloat();
			this.trailFaceTo = rawData.readUnsignedByte();
			this.trailWidthExtend = rawData.readUnsignedByte();
			this.trailUvType = rawData.readUnsignedByte();
			this.trailRotateX = rawData.readFloat();
			this.trailRotateY = rawData.readFloat();
			this.trailRotateZ = rawData.readFloat();
			this.trailStepTime = rawData.readFloat();
		}
		
		override public function clone():UnitBaseProp
		{
			var unit:Polytrail2UnitProp = new Polytrail2UnitProp;
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function clonePropertiesFrom(source:UnitBaseProp):void
		{
			function cloneKeys(keys:Vector.<KeyPair>):Vector.<KeyPair>
			{
				var ret:Vector.<KeyPair> = new Vector.<KeyPair>;
				for (var i:int = 0; i < keys.length; i++)
					ret[i] = keys[i].clone();
				return ret;
			}
			
			super.clonePropertiesFrom(source);
			var srcUnit:Polytrail2UnitProp = source as Polytrail2UnitProp;
			this.trailWidthKeys = cloneKeys(srcUnit.trailWidthKeys);
			this.trailColorKeys = cloneKeys(srcUnit.trailColorKeys);
			this.trailLinkName = srcUnit.trailLinkName;
			this.trailLife = srcUnit.trailLife;
			this.trailSmoothFactor = srcUnit.trailSmoothFactor;
			this.trailFaceTo = srcUnit.trailFaceTo;
			this.trailWidthExtend = srcUnit.trailWidthExtend;
			this.trailUvType = srcUnit.trailUvType;
			this.trailRotateX = srcUnit.trailRotateX;
			this.trailRotateY = srcUnit.trailRotateY;
			this.trailRotateZ = srcUnit.trailRotateZ;
			this.trailStepTime = srcUnit.trailStepTime;
		}
		
	}
}