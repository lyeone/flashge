/*@author lye 123*/
package FlashGE.effects.unitProp
{
	import flash.utils.ByteArray;

	public class CameraTrackUnitProp extends UnitBaseProp
	{
		public var aniLoop:Boolean;
		public var modelName:String;
		public var aniName:String;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_CAMERA_TRACK;
		}
				
		override public function contructor():void
		{
			super.contructor();
			aniLoop = true;
			modelName = "";
			aniName = "";
		}
		
		override public function clone():UnitBaseProp
		{
			var unit:CameraTrackUnitProp = new CameraTrackUnitProp();
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function clonePropertiesFrom(source:UnitBaseProp):void
		{
			super.clonePropertiesFrom(source);
			this.modelName = (source as CameraTrackUnitProp).modelName;
			this.aniName = (source as CameraTrackUnitProp).aniName;
			this.aniLoop = (source as CameraTrackUnitProp).aniLoop;
		}
		
		override public function readRawDataPreVersion(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawDataPreVersion(effectProp, rawData, version);
			var bytesLength:uint = rawData.readUnsignedShort();
			modelName = rawData.readUTFBytes(bytesLength);
			
			bytesLength = rawData.readUnsignedShort();
			aniName = rawData.readUTFBytes(bytesLength);
			this.aniLoop = rawData.readBoolean();
		}
		
		override public function readRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawData(effectProp, rawData, version);
			var bytesLength:uint = rawData.readUnsignedShort();
			modelName = rawData.readUTFBytes(bytesLength);
			
			bytesLength = rawData.readUnsignedShort();
			aniName = rawData.readUTFBytes(bytesLength);
			this.aniLoop = rawData.readBoolean();
		}
		
		override public function writeRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.writeRawData(effectProp, rawData, version);		
			var bytesLength:uint = this.modelName.length;
			rawData.writeShort(bytesLength);
			if (bytesLength > 0)
				rawData.writeUTFBytes(modelName);
			
			bytesLength = this.aniName.length;
			rawData.writeShort(bytesLength);
			if (bytesLength > 0)
				rawData.writeUTFBytes(this.aniName);
			
			rawData.writeBoolean(this.aniLoop);
		}
	}
}