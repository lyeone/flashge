/*@author lye 123*/
package FlashGE.effects.unitProp
{
	import flash.utils.ByteArray;
	
	import FlashGE.FlashGE;
	import FlashGE.effects.KeyPair;

	use namespace FlashGE;
	public class PolytrailUnitProp extends UnitBaseProp
	{
		public var linkNameA:String;
		public var linkNameB:String;
		
		public var smoothFactor:int;
		public var length:int;
		
		public var maxAlpha:Number;
		public var minAlpha:Number;
		
		public var trailLife:Number;
		
		public var flyV1:Number;
		public var flyA1:Number;
		
		public var flyV2:Number;
		public var flyA2:Number;
		public var texWidth:Number;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_POLYTRAIL;
		}
		
		override public function contructor():void
		{
			super.contructor();
			
			name = "polytrail";
			var scaleKey:KeyPair = new KeyPair;
			scaleKey.init(0,1,1,1);
			scaleKeys.push(scaleKey);
			
			var colorKey:KeyPair = new KeyPair;
			colorKey.init(0,1,1,1,1);
			colorKeys.push(colorKey);
			
			var rotateKey:KeyPair = new KeyPair;
			rotateKey.init(0,0,0,0);
			rotateKeys.push(rotateKey);
			
			var offsetKey:KeyPair = new KeyPair;
			offsetKey.init(0,0,0,0);
			offsetKeys.push(offsetKey);
			
			this.uvUOffset = this.uvVOffset = 0;
			this.linkNameA = "";
			this.linkNameB = "";
			this.smoothFactor = 8;
			this.length = 8;
			this.maxAlpha = 1.0;
			this.minAlpha = 1.0;
			this.trailLife = 300;
			this.flyV1 = 0;
			this.flyA1 = 0;
			this.flyV2 = 0;
			this.flyA2 = 0;
			this.texWidth = 0;			
		}
		
		override public function writeRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.writeRawData(effectProp, rawData, version);	
			
			var length:int = linkNameA.length;
			rawData.writeShort(length);
			rawData.writeUTFBytes(linkNameA);
			
			length = linkNameB.length;
			rawData.writeShort(length);
			rawData.writeUTFBytes(linkNameB);			
			
			rawData.writeByte(this.smoothFactor);
			rawData.writeByte(this.length);
			rawData.writeFloat(this.maxAlpha);
			rawData.writeFloat(this.minAlpha);
			rawData.writeFloat(this.trailLife);
			rawData.writeFloat(this.flyV1);
			rawData.writeFloat(this.flyV2);
			rawData.writeFloat(this.flyA1);
			rawData.writeFloat(this.flyA2);
			rawData.writeFloat(this.texWidth);
		}
		override public function readRawDataPreVersion(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawDataPreVersion(effectProp, rawData, version);
			
			var len:int = rawData.readUnsignedShort();
			linkNameA = rawData.readUTFBytes(len);
			if (len > 0) {
				effectProp.linkedNamesList ||= new Vector.<String>
				effectProp.linkedNamesList.push(this.linkNameA);
			}
			
			len = rawData.readUnsignedShort();
			linkNameB = rawData.readUTFBytes(len);
			if (len > 0) {
				effectProp.linkedNamesList ||= new Vector.<String>
				effectProp.linkedNamesList.push(this.linkNameB);
			}
			
			this.smoothFactor = rawData.readUnsignedByte();
			this.length = rawData.readUnsignedByte();
			this.maxAlpha = rawData.readFloat();
			this.minAlpha = rawData.readFloat();
			this.trailLife = rawData.readFloat();
			this.flyV1 = rawData.readFloat();
			this.flyV2 = rawData.readFloat();
			this.flyA1 = rawData.readFloat();
			this.flyA2 = rawData.readFloat();
			if (version >= 2)
				this.texWidth = rawData.readFloat();
		}
		
		override public function readRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawData(effectProp, rawData, version);
			
			var len:int = rawData.readUnsignedShort();
			linkNameA = rawData.readUTFBytes(len);
			if (len > 0) {
				effectProp.linkedNamesList ||= new Vector.<String>
				effectProp.linkedNamesList.push(this.linkNameA);
			}
			
			len = rawData.readUnsignedShort();
			linkNameB = rawData.readUTFBytes(len);
			if (len > 0) {
				effectProp.linkedNamesList ||= new Vector.<String>
				effectProp.linkedNamesList.push(this.linkNameB);
			}
			
			this.smoothFactor = rawData.readUnsignedByte();
			this.length = rawData.readUnsignedByte();
			this.maxAlpha = rawData.readFloat();
			this.minAlpha = rawData.readFloat();
			this.trailLife = rawData.readFloat();
			this.flyV1 = rawData.readFloat();
			this.flyV2 = rawData.readFloat();
			this.flyA1 = rawData.readFloat();
			this.flyA2 = rawData.readFloat();
			this.texWidth = rawData.readFloat();
		}
		
		override public function clone():UnitBaseProp
		{
			var unit:PolytrailUnitProp = new PolytrailUnitProp;
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function clonePropertiesFrom(source:UnitBaseProp):void
		{
			super.clonePropertiesFrom(source);
			var polyUnit:PolytrailUnitProp = source as PolytrailUnitProp;
			this.linkNameA = polyUnit.linkNameA;
			this.linkNameB = polyUnit.linkNameB;
			this.smoothFactor = polyUnit.smoothFactor;
			this.length = polyUnit.length;
			this.maxAlpha = polyUnit.maxAlpha;
			this.minAlpha = polyUnit.minAlpha;
			this.trailLife = polyUnit.trailLife;
			this.flyV1 = polyUnit.flyV1;
			this.flyA1 = polyUnit.flyA1;
			this.flyV2 = polyUnit.flyV2;
			this.flyA2 = polyUnit.flyA2;
		}
	}
}