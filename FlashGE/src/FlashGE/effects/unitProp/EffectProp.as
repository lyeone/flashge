/*@author lye 123*/
package FlashGE.effects.unitProp
{
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.effects.EffectGroup;
	import FlashGE.effects.KeyPair;
	import FlashGE.resources.TextureFile;

	use namespace FlashGE;
	public class EffectProp
	{		
		public static const VERSION_MODIFY_EMTPY_UNIT:uint = 1;
		public static const VERSION_SPRITE_ADD_FOLLOW:uint = 2;
		public static const VERSION_ADD_REPLACE_ALPHA:uint = 3;
		public static const VERSION_ADD_FOLLOW_HOLDER:uint = 4;
		public static const VERSION_ADD_ANI_SPEED:uint = 5;
		public static const VERSION_MODIFY_PRIORITY:uint = 6;
		public static const VERSION_ROTATE_AS_FRAME:uint = 7;
		public static const VERSION_UPDATE_Z:uint = 8;
		public static const VERSION_ADD_OFFSETS:uint = 9;
		public static const VERSION_ADD_SCALES:uint = 10;
		public static const VERSION_UPDATE_Z_PARITCLE:uint = 11;
		public static const VERSION_FOLLOW_HOLDER_ANI:uint = 12;
		public static const VERSION_CHANGE_FPS:uint = 13;
		public static const VERSION_PARTICLE_RAND_SEED:uint = 14;
		public static const VERSION_NEW_FACE_NORMAL:uint = 15;
		public static const CUR_VERSION:uint = 16;
		
		public static const TYPE_NULL:uint = 0;
		public static const TYPE_PARTICLE:uint = 1;
		public static const TYPE_SPRITE:uint = 2;
		public static const TYPE_POLYTRAIL:uint = 3;
		public static const TYPE_MODEL:uint = 4;
		public static const TYPE_EMPTY:uint = 5;
		public static const TYPE_CAMERA_SHAKE:uint = 6;
		public static const TYPE_SCENE_CAMERA:uint = 7;
		public static const TYPE_POLYTRAIL2:uint = 8;
		public static const TYPE_CAMERA_TRACK:uint = 9;
		public static const TYPE_MATERIAL:uint = 10;
		public static const TYPE_SCENE:uint = 11;
		public static const TYPE_WING:uint = 12;
		public static const TYPE_SOUND:uint = 13;
		public static const TYPE_WATER:uint = 14;
		
		FlashGE var unitPropList:Vector.<UnitBaseProp> = new Vector.<UnitBaseProp>;
		FlashGE var linkName:String = "";
		FlashGE var textures:Vector.<TextureFile> = new Vector.<TextureFile>;
		FlashGE var lifeFramesNum:Number = 24;
		FlashGE var followType:int = Defines.FOLLOW_ALL;
		FlashGE var offsetKeys:Vector.<KeyPair> = new Vector.<KeyPair>;
		FlashGE var scaleKeys:Vector.<KeyPair> = new Vector.<KeyPair>;
		FlashGE var maxDstLen:uint = uint.MAX_VALUE;
		FlashGE var offsetKeysValid:Boolean = false;
		FlashGE var scaleKeysValid:Boolean = false;
		FlashGE var dataContext:RawDataContext;
		FlashGE var linkedNamesList:Vector.<String>;
		FlashGE var getCurTimer:Function = getTimer;
		FlashGE var maxOffsetFrame:int;
		public function EffectProp()
		{
			var offsetKey:KeyPair = new KeyPair;
			offsetKey.init(0,0,0,0);
			offsetKeys.push(offsetKey);
			
			var scaleKey:KeyPair = new KeyPair;
			scaleKey.init(0,0,0,0);
			scaleKeys.push(scaleKey);
			
			maxOffsetFrame = 0;
		}
		
		public function calcMaxOffsetFrame():void
		{
			var ret:int = 0;
			for (var i:int = 0, n:int = offsetKeys.length; i < n; i++) {
				if (offsetKeys[i].time > ret) {
					ret = offsetKeys[i].time;
				}
			}
			if (ret >= 100) {
				this.maxOffsetFrame = this.lifeFramesNum;
			} else {
				this.maxOffsetFrame = (ret * this.lifeFramesNum / 100);
			}
			
		}
		
		static public function filterURL(url:String):String
		{
			if (url.indexOf(":") < 0)
				return url;
			
			var n:int = url.indexOf("/resources");
			if (n < 0)
				return url;
			
			return "." + url.slice(n);
		}
		
		public function readKeys(keys:Vector.<KeyPair>, rawData:ByteArray):int
		{
			keys.length = rawData.readUnsignedByte();
			var n:uint = keys.length;
			var ret:int = 0;
			for (var i:uint=0; i < n; i++) {
				var keyPair:KeyPair = new KeyPair;
				keyPair.init(0);
				keyPair.read(rawData);
				keys[i] = keyPair;
				if (keyPair.time > ret) {
					ret = keyPair.time;
				}
			}
			
			return ret;
		}
		
		public function writeKeys(keys:Vector.<KeyPair>, rawData:ByteArray):void
		{
			rawData.writeByte(keys.length);
			for (var i:uint=0; i < keys.length; i++) {
				var keyPair:KeyPair = keys[i];
				keyPair.write(rawData);
			}
		}
		
		public function readRawDataPreVersion(group:EffectGroup, rawData:ByteArray):void
		{
			var bytesLength:uint;
			var url:String;
			var i:uint = 0;
			
			var version:uint = rawData.readUnsignedShort();
			this.lifeFramesNum = rawData.readFloat() * 24;
			bytesLength = rawData.readUnsignedByte();
			this.linkName = rawData.readUTFBytes(bytesLength);
			
			var texNum:uint = rawData.readUnsignedByte();
			for (i=0; i < texNum; i++ )	{
				bytesLength = rawData.readUnsignedByte();
				url = rawData.readUTFBytes(bytesLength);
				url = EffectProp.filterURL(url);
				if (TextureFile.forceATF) {
					url = url.replace(".tga", ".atf");
				}
				var t:TextureFile = TextureFile.createFromFile(url);
				textures.push(t);
			}
			
			var unitNum:uint = rawData.readUnsignedByte();
			var unitType:uint;
			var unit:UnitBaseProp;
			this.unitPropList.length = unitNum;
			for (i=0; i < unitNum; i++) {
				unitType = rawData.readUnsignedByte();
				if (unitType == EffectProp.TYPE_CAMERA_SHAKE){
					unit = new CameraShakeUnitProp;
				} else if(unitType == EffectProp.TYPE_PARTICLE){
					unit = new ParticleUnitProp;
				} else if(unitType == EffectProp.TYPE_POLYTRAIL){
					unit = new PolytrailUnitProp;
				} else if(unitType == EffectProp.TYPE_SPRITE){
					unit = new SpriteUnitProp;
				} else if(unitType == EffectProp.TYPE_MODEL) {
					unit = new ModelUnitProp;
				} else if (unitType == EffectProp.TYPE_SCENE_CAMERA) {
					unit = new SceneCameraUnitProp;
				} else if(unitType == EffectProp.TYPE_EMPTY) {
					unit = new EmptyUnitProp;
				} else if(unitType == EffectProp.TYPE_POLYTRAIL2){
					unit = new Polytrail2UnitProp;
				} else if(unitType == EffectProp.TYPE_CAMERA_TRACK) {
					unit = new CameraTrackUnitProp;
				} else if(unitType == EffectProp.TYPE_MATERIAL) {
					unit = new MaterialUnitProp;
				} else if(unitType == EffectProp.TYPE_SCENE) {
					unit = new SceneUnitProp;
				} else if(unitType == EffectProp.TYPE_WING) {
					unit = new WingUnitProp;
				} else if(unitType == EffectProp.TYPE_SOUND) {
					unit = new SoundUnitProp;
				} else if(unitType == EffectProp.TYPE_WATER) {
					unit = new WaterUnitProp;
				}
				unit.contructor();
				unit.readRawDataPreVersion(this, rawData, version);
				this.unitPropList[i] = unit;
			}
		}
		
		public function readRawData(group:EffectGroup, rawData:ByteArray, timer:int):Boolean
		{
			var curTimer:int = getCurTimer();
			var bytesLength:uint;
			var url:String;
			var i:uint = 0;
			
			if (dataContext == null) {
				dataContext = new RawDataContext;
				dataContext.version = rawData.readUnsignedShort();
				this.lifeFramesNum = rawData.readFloat();
				bytesLength = rawData.readUnsignedByte();
				this.linkName = rawData.readUTFBytes(bytesLength);
				if (dataContext.version > EffectProp.VERSION_ADD_REPLACE_ALPHA) {
					this.followType = rawData.readUnsignedByte();
				}
				
				var texNum:uint = rawData.readUnsignedByte();
				for (i=0; i < texNum; i++ )	{
					bytesLength = rawData.readUnsignedByte();
					url = rawData.readUTFBytes(bytesLength);
					url = EffectProp.filterURL(url);
					if (TextureFile.forceATF) {
						url = url.replace(".tga", ".atf");
					}
					var t:TextureFile = TextureFile.createFromFile(url);
					textures.push(t);
				}
				
				dataContext.unitNum = rawData.readUnsignedByte();
				this.unitPropList.length = dataContext.unitNum;
				if (dataContext.version > EffectProp.VERSION_ADD_OFFSETS) {
					this.maxOffsetFrame = this.readKeys(this.offsetKeys, rawData);
					if (this.maxOffsetFrame >= 100) {
						this.maxOffsetFrame = this.lifeFramesNum;
					} else {
						this.maxOffsetFrame = this.maxOffsetFrame * this.lifeFramesNum / 100;
					}
				}
				
				if (dataContext.version > EffectProp.VERSION_ADD_SCALES) {
					this.readKeys(this.scaleKeys, rawData);
				}
				this.calcMaxDstLen();
				if (getCurTimer() - curTimer >= timer) {
					return false;
				}
			}
			
			var unitType:uint;
			var unit:UnitBaseProp;
			while (dataContext.unitIndex < dataContext.unitNum) {
				unitType = rawData.readUnsignedByte();
				if (unitType == EffectProp.TYPE_CAMERA_SHAKE){
					unit = new CameraShakeUnitProp;
				} else if(unitType == EffectProp.TYPE_PARTICLE){
					unit = new ParticleUnitProp;
				} else if(unitType == EffectProp.TYPE_POLYTRAIL){
					unit = new PolytrailUnitProp;
				} else if(unitType == EffectProp.TYPE_SPRITE){
					unit = new SpriteUnitProp;
				} else if(unitType == EffectProp.TYPE_MODEL) {
					unit = new ModelUnitProp;
				} else if (unitType == EffectProp.TYPE_SCENE_CAMERA) {
					unit = new SceneCameraUnitProp;
				} else if(unitType == EffectProp.TYPE_EMPTY) {
					unit = new EmptyUnitProp;
				} else if(unitType == EffectProp.TYPE_POLYTRAIL2){
					unit = new Polytrail2UnitProp;
				} else if(unitType == EffectProp.TYPE_CAMERA_TRACK) {
					unit = new CameraTrackUnitProp;
				} else if(unitType == EffectProp.TYPE_MATERIAL) {
					unit = new MaterialUnitProp;
				} else if(unitType == EffectProp.TYPE_SCENE) {
					unit = new SceneUnitProp;
				} else if(unitType == EffectProp.TYPE_WING) {
					unit = new WingUnitProp;
				} else if(unitType == EffectProp.TYPE_SOUND) {
					unit = new SoundUnitProp;
				} else if(unitType == EffectProp.TYPE_WATER) {
					unit = new WaterUnitProp;
				}
				
				unit.contructor();
				unit.readRawData(this, rawData, dataContext.version);
				if (unit.lifeFramesNum + 0.5 >= this.lifeFramesNum) {
					unit.lifeFramesNum = this.lifeFramesNum;
				}
					
				this.unitPropList[dataContext.unitIndex] = unit;
				
				dataContext.unitIndex++;
				if (getCurTimer() - curTimer >= timer) {
					return false;
				}
			}
			dataContext = null;
			return true;
		}
		
		public function writeRawData(rawData:ByteArray):void
		{
			var version:uint = EffectProp.CUR_VERSION; 
			rawData.writeShort(version);
			rawData.writeFloat(this.lifeFramesNum);
			
			var bytesLength:int;
			bytesLength = linkName.length;
			rawData.writeByte(bytesLength);
			if (bytesLength > 0 ) {
				rawData.writeUTFBytes(linkName);
			}
			
			rawData.writeByte(this.followType);
			
			var texNum:int = 0;
			rawData.writeByte(texNum);
			var i:int = 0;
			for(i=0; i < texNum; i++){
				// todo:
				var url:String = textures[i].url;
				var len:int = url.length;
				rawData.writeByte(len);
				if (EffectGroup.filterFunction!=null) {
					url = EffectGroup.filterFunction(url);
				}
				if (len > 0 ) {
					rawData.writeUTFBytes(url);
				}
			}
			
			var unitNum:int = unitPropList.length;
			rawData.writeByte(unitNum);
			this.writeKeys(this.offsetKeys, rawData);
			this.writeKeys(this.scaleKeys, rawData);
			var unitProp:UnitBaseProp;
			var unitType:uint;
			for (i = 0; i < unitNum; i++) { 
				unitProp = unitPropList[i];
				unitType = unitProp.TYPE();
				rawData.writeByte(unitType);
				unitProp.writeRawData(this, rawData, version);
			}
		}
		
		public function calcMaxDstLen():void
		{
			offsetKeysValid = false;
			scaleKeysValid = false;
			var max:Number = Number.MIN_VALUE;
			for (var i:int = 0, n:int = this.offsetKeys.length; i < n; i++) {
				var key:KeyPair = this.offsetKeys[i];
				var v:Number = Math.abs(key.items[1]);
				if (max < v) {
					max = v;
				}
			}
			
			if (max != Number.MIN_VALUE) {
				this.maxDstLen = max;
			}
			
			if (this.offsetKeys.length == 1) {
				offsetKeysValid = this.offsetKeys[0].items[1] != 0;
			} else if (this.offsetKeys.length > 1){
				offsetKeysValid = true;
			}
			
			if (this.scaleKeys.length == 1) {
				scaleKeysValid = this.scaleKeys[0].items[1] != 0;
			} else if (this.scaleKeys.length > 1){
				scaleKeysValid = true;
			}
		}
		
		public function enumAllTextures(outTextures:Dictionary):void
		{
			var i:int, n:int;
			var tex:TextureFile;
			for (i = 0, n = textures.length; i < n; i++) {
				tex = this.textures[i];
				outTextures[tex.getFileName()] = true;
			}
			
			for (i = 0, n = this.unitPropList.length; i < n; i++){
				this.unitPropList[i].enumAllTextures(outTextures);
			}
		}
	}
}

internal class RawDataContext
{
	public var unitIndex:int = 0;
	public var version:int = -1;
	public var unitNum:int = -1;
}
