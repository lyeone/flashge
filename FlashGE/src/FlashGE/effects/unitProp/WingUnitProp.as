package FlashGE.effects.unitProp
{
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	
	import FlashGE.resources.TextureFile;

	public class WingUnitProp extends SpriteUnitProp
	{
		public var modelName:String;
		public var aniName:String;
		public var followHolderAni:Boolean = false;
		public var waveContext:Dictionary = new Dictionary;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_WING;
		}
		
		public function WingUnitProp()
		{
			super();
			name = "wing";
		}
		
		override public function clone():UnitBaseProp
		{
			var unit:WingUnitProp = new WingUnitProp;
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function clonePropertiesFrom(source:UnitBaseProp):void
		{
			super.clonePropertiesFrom(source);
			var src:WingUnitProp = source as WingUnitProp;
			this.modelName = src.modelName;
			this.aniName = src.aniName;
			this.followHolderAni = src.followHolderAni;
			this.waveContext = new Dictionary;
			for (var key:String in src.waveContext) {
				var srcObj:Object = src.waveContext[key];
				
				var dstObj:Object = new Object;
				dstObj.v0 = srcObj.v0;
				dstObj.v1 = srcObj.v1;
				dstObj.alphaFactor = srcObj.alphaFactor;
				dstObj.repeatIndex = srcObj.repeatIndex;
				
				dstObj.distortionTexture = srcObj.distortionTexture;
				dstObj.replacedTexture = srcObj.replacedTexture;
				dstObj.layerSpeedU0 = srcObj.layerSpeedU0;
				dstObj.layerSpeedV0 = srcObj.layerSpeedV0;
				dstObj.layerSpeedU1 = srcObj.layerSpeedU1;
				dstObj.layerSpeedV1 = srcObj.layerSpeedV1;
				dstObj.distortionAmountX = srcObj.distortionAmountX;
				dstObj.distortionAmountY = srcObj.distortionAmountY;
				dstObj.heightAttenuationX = srcObj.heightAttenuationX;
				dstObj.heightAttenuationY = srcObj.heightAttenuationY;
				dstObj.fixAlpha = srcObj.fixAlpha;
				
				dstObj.x = new Object;
				dstObj.x.amplitude = srcObj.x.amplitude;
				dstObj.x.wavelength = srcObj.x.wavelength;
				dstObj.x.attenuation = srcObj.x.attenuation;
				
				dstObj.y = new Object;
				dstObj.y.amplitude = srcObj.y.amplitude;
				dstObj.y.wavelength = srcObj.y.wavelength;
				dstObj.y.attenuation = srcObj.y.attenuation;
				
				dstObj.z = new Object;
				dstObj.z.amplitude = srcObj.z.amplitude;
				dstObj.z.wavelength = srcObj.z.wavelength;
				dstObj.z.attenuation = srcObj.z.attenuation;
				
				this.waveContext[key] = dstObj;
			}
		}
		
		override public function readRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawData(effectProp, rawData, version);
			
			var bytesLength:int = rawData.readUnsignedByte();
			this.modelName = rawData.readUTFBytes(bytesLength);
			
			bytesLength = rawData.readUnsignedByte();
			this.aniName = rawData.readUTFBytes(bytesLength);
			
			if (version > EffectProp.VERSION_FOLLOW_HOLDER_ANI) {
				this.followHolderAni = rawData.readBoolean();	
			} else {
				this.followHolderAni = false;
			}
			
			var count:int = rawData.readByte();
			this.waveContext = new Dictionary;
			for (var i:int = 0; i < count; i++) {
				bytesLength = rawData.readUnsignedByte();
				var key:String = rawData.readUTFBytes(bytesLength);
				var obj:Object = new Object;
				obj.v0 = rawData.readFloat();
				obj.v1 = rawData.readFloat();
				obj.alphaFactor = rawData.readUnsignedShort();
				obj.repeatIndex = rawData.readUnsignedByte();
				
				bytesLength = rawData.readUnsignedByte();
				var texFileName:String = rawData.readUTFBytes(bytesLength);
				if (TextureFile.forceATF) {
					if (texFileName != null) {
						obj.replacedTexture = texFileName.replace(".tga", ".atf");
					}
				} else {
					obj.replacedTexture = texFileName
				}
				
				bytesLength = rawData.readUnsignedByte();
				texFileName = rawData.readUTFBytes(bytesLength);
				if (TextureFile.forceATF) {
					if (texFileName != null) {
						obj.distortionTexture = texFileName.replace(".tga", ".atf");
					}
				} else {
					obj.distortionTexture = texFileName
				}
				obj.layerSpeedU0 = rawData.readFloat();
				obj.layerSpeedV0 = rawData.readFloat();
				obj.layerSpeedU1 = rawData.readFloat();
				obj.layerSpeedV1 = rawData.readFloat();
				obj.distortionAmountX = rawData.readFloat();
				obj.distortionAmountY = rawData.readFloat();
				obj.heightAttenuationX = rawData.readFloat();
				obj.heightAttenuationY = rawData.readFloat();
				obj.fixAlpha = rawData.readByte() == 1 ? true : false
				
				obj.x = new Object;
				obj.x.amplitude = rawData.readFloat();
				obj.x.wavelength = rawData.readFloat();
				obj.x.attenuation = rawData.readFloat();
				obj.y = new Object;
				obj.y.amplitude = rawData.readFloat();
				obj.y.wavelength = rawData.readFloat();
				obj.y.attenuation = rawData.readFloat();
				obj.z = new Object;
				obj.z.amplitude = rawData.readFloat();
				obj.z.wavelength = rawData.readFloat();
				obj.z.attenuation = rawData.readFloat();
				waveContext[key] = obj;
			}
			
		}
		
		override public function enumAllTextures(outRes:Dictionary):void
		{
			super.enumAllTextures(outRes);
			for (var key:String in waveContext) {
				var obj:Object = waveContext[key];
				
				if (obj.replacedTexture != null) {
					outRes[obj.replacedTexture] = true;
				}
				
				if (obj.distortionTexture != null) {
					outRes[obj.distortionTexture] = true;
				}
			}
		}
		
		override public function writeRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.writeRawData(effectProp, rawData, version);
			
			var bytesLength:int = this.modelName.length;
			
			if (this.modelName== null) {
				rawData.writeByte(0);
			} else {
				bytesLength = this.modelName.length;
				rawData.writeByte(bytesLength);
				rawData.writeUTFBytes(modelName);
			}
			
			if (this.aniName== null) {
				rawData.writeByte(0);
			} else {
				bytesLength = this.aniName.length;
				rawData.writeByte(bytesLength);
				rawData.writeUTFBytes(aniName);
			}
			rawData.writeBoolean(this.followHolderAni);
			var pos:int = rawData.position;
			rawData.writeByte(0);
			
			var count:int = 0;
			for (var key:String in waveContext) {
				count++;
				var obj:Object = waveContext[key];			
				bytesLength = key.length;
				rawData.writeByte(bytesLength);
				rawData.writeUTFBytes(key);
				rawData.writeFloat(obj.v0);
				rawData.writeFloat(obj.v1);
				rawData.writeShort(obj.alphaFactor);
				rawData.writeByte(obj.repeatIndex);
				
				bytesLength = obj.replacedTexture.length;
				rawData.writeByte(bytesLength);
				rawData.writeUTFBytes(obj.replacedTexture);	
				
				bytesLength = obj.distortionTexture.length;
				rawData.writeByte(bytesLength);
				rawData.writeUTFBytes(obj.distortionTexture);
				rawData.writeFloat(obj.layerSpeedU0);
				rawData.writeFloat(obj.layerSpeedV0);
				rawData.writeFloat(obj.layerSpeedU1);
				rawData.writeFloat(obj.layerSpeedV1);
				rawData.writeFloat(obj.distortionAmountX);
				rawData.writeFloat(obj.distortionAmountY);
				rawData.writeFloat(obj.heightAttenuationX);
				rawData.writeFloat(obj.heightAttenuationY);
				rawData.writeByte(obj.fixAlpha ? 1 : 0);
				
				rawData.writeFloat(obj.x.amplitude);
				rawData.writeFloat(obj.x.wavelength);
				rawData.writeFloat(obj.x.attenuation);
				rawData.writeFloat(obj.y.amplitude);
				rawData.writeFloat(obj.y.wavelength);
				rawData.writeFloat(obj.y.attenuation);
				rawData.writeFloat(obj.z.amplitude);
				rawData.writeFloat(obj.z.wavelength);
				rawData.writeFloat(obj.z.attenuation);
			}
			
			var curPos:int = rawData.position;
			rawData.position = pos;
			rawData.writeByte(count);
			rawData.position = curPos;
		}
	}
}