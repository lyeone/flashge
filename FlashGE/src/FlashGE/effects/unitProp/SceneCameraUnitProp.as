/*@author lye 123*/
package FlashGE.effects.unitProp
{
	import FlashGE.effects.KeyPair;

	public class SceneCameraUnitProp extends UnitBaseProp
	{
		override public function TYPE():uint
		{
			return EffectProp.TYPE_SCENE_CAMERA;
		}
		
		override public function contructor():void
		{
			super.contructor();
			
			var offsetKey:KeyPair = new KeyPair;
			offsetKey.init(0,0,0,0);
			offsetKeys.push(offsetKey);
			
			var scaleKey:KeyPair = new KeyPair;
			scaleKey.init(0,0,0,0);
			scaleKeys.push(scaleKey);
			
			var rotateKey:KeyPair = new KeyPair;
			rotateKey.init(0,0,0,0);
			rotateKeys.push(rotateKey);
		}
		
		override public function clone():UnitBaseProp
		{
			var unit:SceneCameraUnitProp = new SceneCameraUnitProp();
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function clonePropertiesFrom(source:UnitBaseProp):void
		{
			super.clonePropertiesFrom(source);	
		}
	}
}