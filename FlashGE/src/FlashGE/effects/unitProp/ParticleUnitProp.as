/*@author lye 123*/
package FlashGE.effects.unitProp
{
	import flash.display3D.Context3DBlendFactor;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.Program3D;
	import flash.display3D.VertexBuffer3D;
	import flash.geom.Vector3D;
	import flash.utils.ByteArray;
	
	import FlashGE.common.Transform3D;
	import FlashGE.effects.KeyPair;
	import FlashGE.effects.Particle;
	import FlashGE.render.RenderUnit;
	
	public class ParticleUnitProp extends UnitBaseProp
	{
		static private const LIMITE:int = 31;
		static private var vertexBuffer:VertexBuffer3D;
		static private var indexBuffer:IndexBuffer3D;
		static private var diffuseMulProgram:Program3D;
		static private var diffuseAddProgram:Program3D;
		static private var diffuseBlendProgram:Program3D;
		static private var opacityBlendProgram:Program3D;		
		static public var ParticlesCount:uint = 0;
		public static const BLEND_FACTORS:* = [
			Context3DBlendFactor.ONE,
			Context3DBlendFactor.ZERO,
			Context3DBlendFactor.SOURCE_ALPHA,
			Context3DBlendFactor.SOURCE_COLOR,
			Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA,
			Context3DBlendFactor.ONE_MINUS_SOURCE_COLOR,
			Context3DBlendFactor.DESTINATION_ALPHA,
			Context3DBlendFactor.DESTINATION_COLOR,
			Context3DBlendFactor.ONE_MINUS_DESTINATION_ALPHA,
			Context3DBlendFactor.ONE_MINUS_DESTINATION_COLOR];
		
		public static const EMIT_TYPE_POINT:uint = 0;
		public static const EMIT_TYPE_CYCLE_EDGE:uint = 1;
		public static const EMIT_TYPE_CYCLE:uint = 2;
		public static const EMIT_TYPE_SPHERE_EDGE:uint = 3;
		public static const EMIT_TYPE_SPHERE:uint = 4;
		public static const EMIT_TYPE_RECT:uint = 5;
		public static const EMIT_TYPE_CURVE:uint = 6;
		
		public static const EMITDIR_TYPE_DEFAULT:uint = 0;
		public static const EMITDIR_TYPE_EMITPLANE_NORMAL:uint = 1;
		public static const EMITDIR_TYPE_USE_POS:uint = 2;
		
		public static const ACCELERATION_TYPE_DEFAULT:uint = 0;
		public static const ACCELERATION_TYPE_V:uint = 1;
		public static const ACCELERATION_TYPE_OPPOSITE_V:uint = 2;
		
		public static const BLEND_TYPE_DEFAULT:uint = 0;
		public static const BLEND_TYPE_ADD:uint = 1;
		
		private var particleList:Particle;	
		
		public var emitType:uint;
		public var emitTypeValueX:Number;
		public var emitTypeValueY:Number;
		public var emitTypeValueZ:Number;
		public var emitPlane:Vector3D;
		
		public var emitDirType:uint;
		public var emitDirTypeValue:Number;
		public var minEmitDir:Vector3D;
		public var maxEmitDir:Vector3D;
		
		public var emitFrequency:int;
		public var emitMinIntensity:uint;
		public var emitMaxIntensity:uint;
		
		public var accelerationType:uint;
		public var accelerationDir:Vector3D;
		public var minAcceleration:Number;
		public var maxAcceleration:Number;
		
		public var minWidth:Number;
		public var maxWidth:Number;
		public var sizeFactor:Number;
		public var maxHeight:Number;
		
		public var minLife:int;
		public var maxLife:int;
		
		public var srcBlend:uint;
		public var dstBlend:uint;
		
		public var blendType:uint;
		
		public var dirType:int;
		
		public var faceType:uint;
		public var faceNormal:Vector3D;
		
		public var selfRotateAxis:Vector3D;
		public var selfOriginRotateMin:Number;
		public var selfOriginRotateMax:Number;
		public var normalTrans:Transform3D;
		
		public var completelyFollow:Boolean;
		public var followLinkNodeScale:Boolean;
		
		public var rotateVMin:Number;
		public var rotateVMax:Number;
		public var randomSeed:uint;
		public var useNewFaceNormal:int = 0;
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_PARTICLE;
		}
		
		override public function contructor():void
		{
			super.contructor();
			
			this.name = "particle";
			var scaleKey:KeyPair = new KeyPair;
			scaleKey.init(0,1,1);
			this.scaleKeys.push(scaleKey);
			
			var colorKey:KeyPair = new KeyPair;
			colorKey.init(0,1,1,1,1);
			this.colorKeys.push(colorKey);
			
			var rotateKey:KeyPair = new KeyPair;
			rotateKey.init(0,0,0);
			this.rotateKeys.push(rotateKey);
			
			var offsetKey:KeyPair = new KeyPair;
			offsetKey.init(0,0,0,0);
			this.offsetKeys.push(offsetKey);
			this.randomSeed = 0;
			this.emitType = ParticleUnitProp.EMIT_TYPE_POINT;
			this.emitTypeValueX = 0;
			this.emitTypeValueY = 0;
			this.emitTypeValueZ = 2;
			this.emitPlane = new Vector3D(0,1,0);
			this.emitDirType = 0;
			this.emitDirTypeValue = 0;
			this.minEmitDir = new Vector3D(0,0,100);
			this.maxEmitDir = new Vector3D(0,0,100);
			this.emitFrequency = 1;
			this.emitMinIntensity = 1;
			this.emitMaxIntensity = 1;
			this.accelerationType = ParticleUnitProp.ACCELERATION_TYPE_DEFAULT;
			this.accelerationDir = new Vector3D(1,0,0);
			this.minAcceleration = 0;
			this.maxAcceleration = 0;
			this.minWidth = 6;
			this.maxWidth = 6;
			this.sizeFactor = 1;
			this.maxHeight = 6;
			this.minLife = 30;
			this.maxLife = 30;
			this.srcBlend = 2;
			this.dstBlend = 0;
			this.blendType = 0;
			this.dirType = DIR_TYPE_WORLD;
			this.faceType = DIR_TYPE_WORLD;
			this.faceNormal = new Vector3D(0,0,1);
			this.selfRotateAxis = new Vector3D(0,0,1);
			this.selfOriginRotateMin = 0;
			this.selfOriginRotateMax = 0;
			this.normalTrans = new Transform3D;
			this.completelyFollow = false;
			this.followLinkNodeScale = false;
			this.rotateVMin = 0;
			this.rotateVMax = 0;
			this.useNewFaceNormal = 1;
			
			this.lifeFramesNum = 24;
			this.uvType = 0;
			this.uvColumnsCount = this.uvRowsCount = 1;
			this.uvRangeBegin = this.uvRangeLength = 0;
			this.uvFPS = 0;
		}
		
		override public function readRawDataPreVersion(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawDataPreVersion(effectProp, rawData, version);
			
			var i:uint = 0;
			var bytesLength:int = rawData.readUnsignedByte();
			name = rawData.readUTFBytes(bytesLength);
			
			emitType = rawData.readUnsignedByte();
			emitTypeValueX = rawData.readFloat();
			emitTypeValueY = emitTypeValueX;
			this.readVector(emitPlane, rawData);
			emitPlane.normalize();
			
			emitDirType = rawData.readUnsignedByte();
			emitDirTypeValue = rawData.readFloat();			
			if (version > 5) {
				var emitTypeXYRatio:Number = rawData.readFloat();
				emitTypeValueY = emitTypeValueX * emitTypeXYRatio;
			}
			
			if (version <= 11) {
				var emitDir:Vector3D = new Vector3D(0,1,0);
				var emitMinVelocity:Number = 0;
				var emitMaxVelocity:Number = 0;
				this.readVector(emitDir, rawData);
				emitDir.normalize();
				
				emitMinVelocity = rawData.readFloat();
				emitMaxVelocity = rawData.readFloat();
				
				minEmitDir.x = emitDir.x * emitMinVelocity;
				minEmitDir.y = emitDir.y * emitMinVelocity;
				minEmitDir.z = emitDir.z * emitMinVelocity;
				
				maxEmitDir.x = emitDir.x * emitMaxVelocity;
				maxEmitDir.y = emitDir.y * emitMaxVelocity;
				maxEmitDir.z = emitDir.z * emitMaxVelocity;
			} else {
				this.readVector(minEmitDir, rawData);
				this.readVector(maxEmitDir, rawData);
			}
			
			emitFrequency = rawData.readFloat() * 24 + 0.5;
			if (emitFrequency <=0)emitFrequency = 1;
			
			emitMinIntensity = rawData.readUnsignedShort();
			emitMaxIntensity = rawData.readUnsignedShort();
			
			accelerationType = rawData.readUnsignedByte();
			this.readVector(accelerationDir, rawData);
			accelerationDir.normalize();
			
			minAcceleration = rawData.readFloat();
			maxAcceleration = rawData.readFloat();
			
			minWidth = rawData.readFloat();
			maxWidth = rawData.readFloat();
			sizeFactor = rawData.readFloat();
			maxHeight = rawData.readFloat();
			
			minLife = rawData.readFloat() * 24;
			maxLife = rawData.readFloat() * 24;
			
			blendType = rawData.readUnsignedByte();
			
			dirType = rawData.readUnsignedByte();
			if (version > 7) {
				this.faceType = rawData.readUnsignedByte();
				this.readVector(this.faceNormal, rawData);
				this.readVector(this.selfRotateAxis, rawData);
				this.selfOriginRotateMin = rawData.readFloat();
				if (version <= 12)
					this.selfOriginRotateMax = this.selfOriginRotateMin;
				else {
					this.selfOriginRotateMax = rawData.readFloat();
				}
				this.completelyFollow = rawData.readBoolean();
			} else {
				this.faceType = UnitBaseProp.DIR_TYPE_CAMERA;
			}
			
			if (this.dirType == UnitBaseProp.DIR_TYPE_FOLLOW) {
				this.completelyFollow = true;
				this.dirType = UnitBaseProp.DIR_TYPE_PARENT;				
			}
			
			if (version <= 8) {
				followLinkNodeScale = false;
			} else {
				this.followLinkNodeScale = rawData.readBoolean();
			}
			this.calcNormal();
			
			if (version > 13){
				this.rotateVMin = rawData.readFloat();
				this.rotateVMax = rawData.readFloat();
			} else {
				this.rotateVMin = this.rotateKeys[0].items[0];
				this.rotateVMax = this.rotateKeys[0].items[0];
				this.rotateKeys[0].items[0] = 1;
			}
			
			if (this.emitType == ParticleUnitProp.EMIT_TYPE_POINT) {
				this.emitTypeValueX = this.emitTypeValueY = this.emitTypeValueZ = 0;
			}
			
			randomSeed = 0;
		}
		
		override public function readRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.readRawData(effectProp, rawData, version);
			
			var i:uint = 0;
			var bytesLength:int = rawData.readUnsignedByte();
			name = rawData.readUTFBytes(bytesLength);
			
			emitType = rawData.readUnsignedByte();
			emitTypeValueX = rawData.readFloat();
			emitTypeValueY = rawData.readFloat();
			emitTypeValueZ = rawData.readFloat();
			this.readVector(emitPlane, rawData);
			emitPlane.normalize();
			
			emitDirType = rawData.readUnsignedByte();
			emitDirTypeValue = rawData.readFloat();
			
			this.readVector(minEmitDir, rawData);
			this.readVector(maxEmitDir, rawData);
			
			emitFrequency = rawData.readUnsignedShort()
			emitMinIntensity = rawData.readUnsignedShort();
			emitMaxIntensity = rawData.readUnsignedShort();
			
			accelerationType = rawData.readUnsignedByte();
			this.readVector(accelerationDir, rawData);
			accelerationDir.normalize();
			
			minAcceleration = rawData.readFloat();
			maxAcceleration = rawData.readFloat();
			
			minWidth = rawData.readFloat();
			maxWidth = rawData.readFloat();
			sizeFactor = rawData.readFloat();
			maxHeight = rawData.readFloat();
			
			minLife = rawData.readInt()
			maxLife = rawData.readInt();
			
			blendType = rawData.readUnsignedByte();
			
			dirType = rawData.readUnsignedByte();
			this.faceType = rawData.readUnsignedByte();
			this.readVector(this.faceNormal, rawData);
			this.readVector(this.selfRotateAxis, rawData);
			this.selfOriginRotateMin = rawData.readFloat();
			this.selfOriginRotateMax = rawData.readFloat();
			this.completelyFollow = rawData.readBoolean();
			
			if (this.dirType == UnitBaseProp.DIR_TYPE_FOLLOW) {
				this.completelyFollow = true;
				this.dirType = UnitBaseProp.DIR_TYPE_PARENT;				
			}
			
			this.followLinkNodeScale = rawData.readBoolean();
			
			
			this.rotateVMin = rawData.readFloat();
			this.rotateVMax = rawData.readFloat();
			
			if (version <= EffectProp.VERSION_UPDATE_Z_PARITCLE) {
				this.updateZ = 0;
			}
			
			if (version > EffectProp.VERSION_PARTICLE_RAND_SEED) {
				this.randomSeed = rawData.readUnsignedInt();
			} else {
				this.randomSeed = 0;
			}
			
			if (version > EffectProp.VERSION_NEW_FACE_NORMAL) {
				this.useNewFaceNormal = rawData.readByte();
			} else {
				this.useNewFaceNormal = 0;
			}
			this.calcNormal();
		}
		
		override public function writeRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			super.writeRawData(effectProp, rawData, version);
			var bytesLength:int;
			var i:int = 0;
			bytesLength = name.length;
			rawData.writeByte(bytesLength);
			rawData.writeUTFBytes(this.name);
			rawData.writeByte(this.emitType);
			
			rawData.writeFloat(this.emitTypeValueX);
			rawData.writeFloat(this.emitTypeValueY);
			rawData.writeFloat(this.emitTypeValueZ);
			this.writeVector(this.emitPlane, rawData);
			
			rawData.writeByte(this.emitDirType);
			rawData.writeFloat(this.emitDirTypeValue);
			this.writeVector(this.minEmitDir, rawData);
			this.writeVector(this.maxEmitDir, rawData);
			
			rawData.writeShort(this.emitFrequency);
			rawData.writeShort(this.emitMinIntensity);
			rawData.writeShort(this.emitMaxIntensity);
			
			rawData.writeByte(this.accelerationType);
			this.writeVector(this.accelerationDir, rawData);
			rawData.writeFloat(this.minAcceleration);
			rawData.writeFloat(this.maxAcceleration);
			
			rawData.writeFloat(this.minWidth);
			rawData.writeFloat(this.maxWidth);
			rawData.writeFloat(this.sizeFactor);
			rawData.writeFloat(this.maxHeight);
			
			rawData.writeInt(this.minLife);
			rawData.writeInt(this.maxLife);
			
			rawData.writeByte(this.blendType);
			rawData.writeByte(this.dirType);
			
			rawData.writeByte(this.faceType);
			this.writeVector(this.faceNormal, rawData);
			this.writeVector(this.selfRotateAxis, rawData);
			rawData.writeFloat(this.selfOriginRotateMin);
			rawData.writeFloat(this.selfOriginRotateMax);
			rawData.writeBoolean(this.completelyFollow);
			rawData.writeBoolean(this.followLinkNodeScale);
			rawData.writeFloat(this.rotateVMin);
			rawData.writeFloat(this.rotateVMax);
			rawData.writeUnsignedInt(this.randomSeed);
			rawData.writeByte(this.useNewFaceNormal);
		}		

		public function calcNormal():void
		{
			if (this.useNewFaceNormal == 1) {
				this.normalTrans.compose(0,0,0, this.faceNormal.x, this.faceNormal.y, this.faceNormal.z, 1, 1,1);
			} else {
				var dirA:Vector3D = new Vector3D(0,0,1);
				var dirB:Vector3D = this.faceNormal.clone();
				dirB.normalize();
				if (dirB.x ==0 && dirB.y==0 && dirB.z==1) {
					this.normalTrans.fromAxisAngle(0, -1, 0, 0);
					return;
				}
				
				var dirN:Vector3D = dirA.crossProduct(dirB);
				dirN.normalize();
				var dot:Number = dirA.dotProduct(dirB);
				this.normalTrans.fromAxisAngle(dirN.x, dirN.y, dirN.z, Math.acos(dot));
			}
		}
		
		override public function clone():UnitBaseProp
		{
			var unit:ParticleUnitProp = new ParticleUnitProp;
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override public function clonePropertiesFrom(source:UnitBaseProp):void
		{
			super.clonePropertiesFrom(source);
			var srcUnit:ParticleUnitProp = source as ParticleUnitProp;
			
			this.emitType = srcUnit.emitType;
			this.emitTypeValueX = srcUnit.emitTypeValueX;
			this.emitTypeValueY = srcUnit.emitTypeValueY;
			this.emitTypeValueZ = srcUnit.emitTypeValueZ;
			this.emitPlane = srcUnit.emitPlane.clone();
			
			this.emitDirType = srcUnit.emitDirType;
			this.emitDirTypeValue = srcUnit.emitDirTypeValue;
			this.minEmitDir = srcUnit.minEmitDir.clone();
			this.maxEmitDir = srcUnit.maxEmitDir.clone();
			
			this.emitFrequency = srcUnit.emitFrequency;
			this.emitMinIntensity = srcUnit.emitMinIntensity;
			this.emitMaxIntensity = srcUnit.emitMaxIntensity;
			
			this.accelerationType = srcUnit.accelerationType;
			this.accelerationDir = srcUnit.accelerationDir.clone();
			this.minAcceleration = srcUnit.minAcceleration;
			this.maxAcceleration = srcUnit.maxAcceleration;			
			
			this.minWidth = srcUnit.minWidth;
			this.maxWidth = srcUnit.maxWidth;
			this.sizeFactor = srcUnit.sizeFactor;
			this.maxHeight = srcUnit.maxHeight;
			
			this.minLife = srcUnit.minLife;
			this.maxLife = srcUnit.maxLife;
			
			this.srcBlend = srcUnit.srcBlend;
			this.dstBlend = srcUnit.dstBlend;
			
			this.blendType = srcUnit.blendType;
			this.dirType = srcUnit.dirType;
			
			this.faceType = srcUnit.faceType;
			this.faceNormal = srcUnit.faceNormal.clone();
			
			this.selfRotateAxis = srcUnit.selfRotateAxis.clone();
			this.selfOriginRotateMin = srcUnit.selfOriginRotateMin;
			this.selfOriginRotateMax = srcUnit.selfOriginRotateMax;
			
			this.completelyFollow = srcUnit.completelyFollow;
			this.followLinkNodeScale = srcUnit.followLinkNodeScale;
			
			this.rotateVMin = srcUnit.rotateVMin;
			this.rotateVMax = srcUnit.rotateVMax;
			this.useNewFaceNormal = srcUnit.useNewFaceNormal;
			this.calcNormal();
			this.randomSeed = srcUnit.randomSeed;
		}
	}
}