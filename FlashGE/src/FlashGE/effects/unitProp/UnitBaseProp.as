/*@author lye 123*/
package FlashGE.effects.unitProp
{
	import flash.geom.Vector3D;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.effects.EffectGroup;
	import FlashGE.effects.KeyPair;
	import FlashGE.resources.TextureFile;
	
	public class UnitBaseProp
	{
		public static const DIR_TYPE_PARENT:uint = 0;
		public static const DIR_TYPE_WORLD:uint = 1;
		public static const DIR_TYPE_CAMERA:uint = 2;
		public static const DIR_TYPE_FOLLOW:uint = 3;		
		public static const DIR_TYPE_H_CAMERA:uint = 3;//sprite only
		public static const DIR_TYPE_EMIT_VELOCITY:uint = 4;
		public static const DIR_TYPE_FLY_VELOCITY:uint = 6;
		public static const DIR_TYPE_FLY_VELOCITY_CAMERA:uint = 7;
		
		public var name:String;
		public var textureNames:Vector.<String>;
		
		public var lifeFramesNum:Number;
		public var unitBeginFrame:Number;
		public var offsetKeys:Vector.<KeyPair>;
		public var scaleKeys:Vector.<KeyPair>;
		public var colorKeys:Vector.<KeyPair>;		
		public var rotateKeys:Vector.<KeyPair>;
		
		public var uvV0:Number;
		public var uvV1:Number;
		
		
		public var uvAngle:Number;
		
		public var blendSrcType:int;
		public var blendDstType:int;
		public var replaceAlpha:int;
		
		public var renderPriority:int;
		public var alphaTest:Number;
		
		public var texOpIndex:int;
		public var repeatIndex:int;
		public var mipIndex:int;
		public var filterIndex:int;
		
		public var uvUOffset:Number;
		public var uvVOffset:Number;
		public var uv2UOffset:Number;
		public var uv2VOffset:Number;
		public var uvUScale:Number;
		public var uvVScale:Number;
		
		//
		public var uvType:int;
		public var uvColumnsCount:Number;
		public var uvRowsCount:Number;
		public var uvRangeBegin:Number;
		public var uvRangeLength:Number;
		public var uvFPS:Number;
		public var uvLoop:Boolean;
		public var uvAutoCenter:Boolean;
		
		public var followUnitName:String;
		
		public var alphaIntensity:Number;
		public var alphaSharpness:Number;
				
		public var aliasFollowUnitName:String;
		public var origEffectName:String;
		public var followHolder:Boolean;
		public var updateZ:uint;
		
		FlashGE var materialKey:String;	
		FlashGE var materialKeyBase:String;
		FlashGE var programCachedID:String;
		FlashGE var programKey:String;
		
		use namespace FlashGE;
		
		public function TYPE():uint
		{
			return EffectProp.TYPE_NULL;
		}
		
		public function contructor():void
		{
			this.name = "";
			this.textureNames = new Vector.<String>;
			this.lifeFramesNum = 24;
			this.unitBeginFrame = 0;
			this.offsetKeys = new Vector.<KeyPair>;
			this.scaleKeys = new Vector.<KeyPair>;
			this.colorKeys = new Vector.<KeyPair>;		
			this.rotateKeys = new Vector.<KeyPair>;
			this.uvV0 = 0;
			this.uvV1 = 0;
			this.uvAngle = 0;
			this.blendSrcType = Defines.BLEND_ONE;
			this.blendDstType = Defines.BLEND_ONE;
			this.replaceAlpha = 0;
			this.renderPriority = Defines.PRIORITY_EFFECT;
			this.alphaTest = 0;
			this.texOpIndex = 0;
			this.repeatIndex = 0;
			this.mipIndex = 0;
			this.filterIndex = 0;
			this.uvUOffset = 0.5;
			this.uvVOffset = 0.5;
			this.uv2UOffset = 0.5;
			this.uv2VOffset = 0.5;
			this.uvUScale = 1;
			this.uvVScale = 1;
			this.uvType = 0;
			this.uvColumnsCount = 8;
			this.uvRowsCount = 8;
			this.uvRangeBegin = 16;
			this.uvRangeLength = 16;
			this.uvFPS = 24;
			this.uvLoop = true;
			this.uvAutoCenter = true;
			this.followUnitName = "";
			this.alphaIntensity = 0;
			this.alphaSharpness = 0;
			this.aliasFollowUnitName = "";
			this.origEffectName = "";
			this.followHolder = false;
			this.updateZ = 0;
		}
		
		public function enumAllTextures(outRes:Dictionary):void
		{
			for (var i:int=0, n:int = this.textureNames.length; i < n; i++) {
				outRes[this.textureNames[i]] = true;
			}
		}
		
		public function readKeys(keys:Vector.<KeyPair>, rawData:ByteArray):void
		{
			keys.length = rawData.readUnsignedByte();
			var n:uint = keys.length;
			for (var i:uint=0; i < n; i++) {
				var keyPair:KeyPair = new KeyPair;
				keyPair.read(rawData);
				keys[i] = keyPair;
			}
		}
		
		public function writeKeys(keys:Vector.<KeyPair>, rawData:ByteArray):void
		{
			rawData.writeByte(keys.length);
			for (var i:uint=0; i < keys.length; i++) {
				var keyPair:KeyPair = keys[i];
				keyPair.write(rawData);
			}
		}
		
		public function readVector(v:Vector3D, rawData:ByteArray):void 
		{
			v.x = rawData.readFloat();
			v.y = rawData.readFloat();
			v.z = rawData.readFloat();
		}
		
		public function writeVector(v:Vector3D, rawData:ByteArray):void
		{
			rawData.writeFloat(v.x);
			rawData.writeFloat(v.y);
			rawData.writeFloat(v.z);
		}
		
		public function readRawDataPreVersion(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			var bytesLength:int = rawData.readUnsignedByte();
			this.name = rawData.readUTFBytes(bytesLength);
			
			bytesLength = rawData.readUnsignedByte();
			var linkName:String = rawData.readUTFBytes(bytesLength);
			
			var i:int;
			lifeFramesNum = rawData.readFloat()*24;
			unitBeginFrame = rawData.readFloat()*24;
			this.readKeys(offsetKeys, rawData);
			this.readKeys(scaleKeys, rawData);
			this.readKeys(colorKeys, rawData);
			if (this.TYPE() == EffectProp.TYPE_PARTICLE) {
				if (version <= 12) {
					this.readKeys(rotateKeys, rawData);
					for (i=0; i < rotateKeys.length; i++){
						var key:Vector.<Number> = rotateKeys[i].items;
						key.length = 2;
						key[1] = key[0];
					}
					
				} else {
					this.readKeys(rotateKeys, rawData);
				}
			} else {
				this.readKeys(rotateKeys, rawData);
			}
			
			this.uvType = rawData.readUnsignedByte();
			this.uvAngle = rawData.readFloat();
			this.uvUOffset = rawData.readFloat();
			this.uvVOffset = rawData.readFloat();			
			this.uvV0 = rawData.readFloat();
			this.uvV1 = rawData.readFloat();
			this.uvUScale = rawData.readFloat();
			this.uvVScale = rawData.readFloat();
			
			this.uvColumnsCount = rawData.readFloat();
			this.uvRowsCount = rawData.readFloat();
			this.uvRangeBegin = rawData.readFloat();			
			this.uvRangeLength = rawData.readFloat();
			this.uvFPS = rawData.readFloat();
			this.uvLoop = rawData.readBoolean();
			
			this.texOpIndex = rawData.readUnsignedByte();
			if (this.texOpIndex > 2) {
				this.texOpIndex = 0;
			}
			this.blendSrcType = rawData.readUnsignedByte();
			this.blendDstType = rawData.readUnsignedByte();
			
			var texNum:int = rawData.readUnsignedByte();
			
			var et:TextureFile;
			var url:String;
			this.textureNames.length = texNum;
			for (i = 0; i < texNum; i++) {
				bytesLength = rawData.readUnsignedShort();
				url = rawData.readUTFBytes(bytesLength);
				
				if (EffectGroup.filterFunction!=null) {
					url = EffectGroup.filterFunction(url);
				}
				this.textureNames[i] = url;
			}
			
			if (version >= 1) {
				bytesLength = rawData.readUnsignedShort();
				this.followUnitName = rawData.readUTFBytes(bytesLength);
			}
			
			if (version >= 2) {
				updateZ = rawData.readBoolean() ? 1 : 0;
				var zTestFunction:int = rawData.readUnsignedByte();
				this.renderPriority = rawData.readUnsignedShort();
				this.texOpIndex = rawData.readUnsignedByte();
				if (this.texOpIndex > 2) {
					this.texOpIndex = 0;
				}
				this.repeatIndex = rawData.readUnsignedByte();
				this.mipIndex = rawData.readUnsignedByte();
				this.filterIndex = rawData.readUnsignedByte();
				this.alphaTest = rawData.readFloat();
				// must be 0
				recalRenderPriority(0);
			} else {
				updateZ = 0;
			}
			
			if (version > 6){
				this.alphaIntensity = rawData.readFloat();
				this.alphaSharpness = rawData.readFloat();
			}
			
			if (version > 9){
				var bindType:int = rawData.readUnsignedByte();
			}
			
			if (version > 10) {
				this.uvAutoCenter = rawData.readBoolean();
			}
			
			if (version > 14) {
				this.uv2UOffset = rawData.readFloat();
				this.uv2VOffset = rawData.readFloat();
			} else {
				this.uv2UOffset = this.uvUOffset;
				this.uv2VOffset = this.uvVOffset;
			}
			
			if (version > 16)
			{
				bytesLength = rawData.readUnsignedByte();
				var aliasName:String = rawData.readUTFBytes(bytesLength);
			}	
		}
		
		public function recalRenderPriority(version:int):void
		{
			if (version <= EffectProp.VERSION_MODIFY_PRIORITY) {
				this.renderPriority = Defines.PRIORITY_EFFECT;
			}
		}
		
		public function readRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			var bytesLength:int = rawData.readUnsignedByte();
			this.name = rawData.readUTFBytes(bytesLength);
			
			var i:int;
			lifeFramesNum = rawData.readFloat()
			unitBeginFrame = rawData.readFloat();
			this.readKeys(offsetKeys, rawData);
			this.readKeys(scaleKeys, rawData);
			this.readKeys(colorKeys, rawData);
			this.readKeys(rotateKeys, rawData);
			
			this.uvType = rawData.readUnsignedByte();
			this.uvAngle = rawData.readFloat();
			this.uvUOffset = rawData.readFloat();
			this.uvVOffset = rawData.readFloat();			
			this.uvV0 = rawData.readFloat();
			this.uvV1 = rawData.readFloat();
			this.uvUScale = rawData.readFloat();
			this.uvVScale = rawData.readFloat();
			
			this.uvColumnsCount = rawData.readFloat();
			this.uvRowsCount = rawData.readFloat();
			this.uvRangeBegin = rawData.readFloat();			
			this.uvRangeLength = rawData.readFloat();
			this.uvFPS = rawData.readFloat();
			this.uvLoop = rawData.readBoolean();
			
			this.texOpIndex = rawData.readUnsignedByte();
			if (this.texOpIndex > 2) {
				this.texOpIndex = 0;
			}
			this.blendSrcType = rawData.readUnsignedByte();
			this.blendDstType = rawData.readUnsignedByte();
			if (version > EffectProp.VERSION_ADD_REPLACE_ALPHA) {
				this.replaceAlpha = rawData.readUnsignedByte();
			}
			
			var texNum:int = rawData.readUnsignedByte();
			
			var et:TextureFile;
			var url:String;
			this.textureNames.length = texNum;
			for (i = 0; i < texNum; i++) {
				bytesLength = rawData.readUnsignedShort();
				url = rawData.readUTFBytes(bytesLength);				
				if (EffectGroup.filterFunction!=null) {
					url = EffectGroup.filterFunction(url);
				}
				if (TextureFile.forceATF) {
					url = url.replace(".tga", ".atf");
				}
				this.textureNames[i] = url;
			}
			
			bytesLength = rawData.readUnsignedShort();
			this.followUnitName = rawData.readUTFBytes(bytesLength);
			this.renderPriority = rawData.readUnsignedShort();
			recalRenderPriority(version);
			
			this.texOpIndex = rawData.readUnsignedByte();
			if (this.texOpIndex > 2) {
				this.texOpIndex = 0;
			}
			this.repeatIndex = rawData.readUnsignedByte();
			this.mipIndex = rawData.readUnsignedByte();
			this.filterIndex = rawData.readUnsignedByte();
			this.alphaTest = rawData.readFloat();
			this.alphaIntensity = rawData.readFloat();
			this.alphaSharpness = rawData.readFloat();
			this.uvAutoCenter = rawData.readBoolean();
			this.uv2UOffset = rawData.readFloat();
			this.uv2VOffset = rawData.readFloat();
			if (version > EffectProp.VERSION_ADD_FOLLOW_HOLDER) {
				this.followHolder = rawData.readBoolean();
			}
			
			if (version > EffectProp.VERSION_UPDATE_Z) {
				this.updateZ = rawData.readByte();
			} else {
				this.updateZ = 0;
			}
		}
		
		public function writeRawData(effectProp:EffectProp, rawData:ByteArray, version:uint):void
		{
			var bytesLength:int;
			bytesLength = name!=null?name.length : 0;
			rawData.writeByte(bytesLength);
			if (name)
				rawData.writeUTFBytes(name);
			
			rawData.writeFloat(this.lifeFramesNum);
			rawData.writeFloat(this.unitBeginFrame);
			
			this.writeKeys(offsetKeys, rawData);
			this.writeKeys(scaleKeys, rawData);
			this.writeKeys(colorKeys, rawData);
			this.writeKeys(rotateKeys, rawData);
			rawData.writeByte(this.uvType);
			rawData.writeFloat(this.uvAngle);
			rawData.writeFloat(this.uvUOffset);
			rawData.writeFloat(this.uvVOffset);			
			rawData.writeFloat(this.uvV0);
			rawData.writeFloat(this.uvV1);
			rawData.writeFloat(this.uvUScale);
			rawData.writeFloat(this.uvVScale);
			
			rawData.writeFloat(this.uvColumnsCount);
			rawData.writeFloat(this.uvRowsCount);
			rawData.writeFloat(this.uvRangeBegin);			
			rawData.writeFloat(this.uvRangeLength);
			rawData.writeFloat(this.uvFPS);
			rawData.writeBoolean(this.uvLoop);
			
			rawData.writeByte(this.texOpIndex);
			rawData.writeByte(this.blendSrcType);
			rawData.writeByte(this.blendDstType);
			rawData.writeByte(this.replaceAlpha);
			
			var i:int;
			var texNum:int = this.textureNames.length;
			rawData.writeByte(texNum);
			if (texNum > 0) {
				for (i = 0; i < texNum; i++) {
					var url:String = this.textureNames[i];
					
					if (EffectGroup.filterFunction!=null) {
						url = EffectGroup.filterFunction(url);
					}
					var prePos:uint = rawData.position;
					rawData.writeShort(url.length);
					var preValuePos:uint = rawData.position;
					rawData.writeUTFBytes(url);
					var curValuePos:uint = rawData.position;
					var num:uint = curValuePos - preValuePos;
					rawData.position = prePos;
					rawData.writeShort(num);
					rawData.position = curValuePos;
				}
			}
			
			bytesLength = this.followUnitName.length;
			rawData.writeShort(bytesLength);
			if (bytesLength > 0)
				rawData.writeUTFBytes(followUnitName);
			
			rawData.writeShort(this.renderPriority);
			rawData.writeByte(this.texOpIndex);
			rawData.writeByte(this.repeatIndex);
			rawData.writeByte(this.mipIndex);
			rawData.writeByte(this.filterIndex);
			rawData.writeFloat(this.alphaTest);
			rawData.writeFloat(this.alphaIntensity);
			rawData.writeFloat(this.alphaSharpness);
			rawData.writeBoolean(this.uvAutoCenter);
			rawData.writeFloat(this.uv2UOffset);
			rawData.writeFloat(this.uv2VOffset);
			rawData.writeBoolean(this.followHolder);
			rawData.writeByte(this.updateZ);
		}
		
		public function clone():UnitBaseProp
		{
			throw ("subclass must override the function:UnitBaseProp::clone");
		}
		
		static public function getRandomName(unit:*, filter:String):String 
		{
			if (unit == null)
				return "nullObject";
			
			var className:String = flash.utils.getQualifiedClassName(unit);
			var shortName:String = className.split("::").pop();
			shortName = shortName.replace(filter, "-");
			var id:Number = Math.random()*0xffffffff;
			var date:Date = new Date;
			var cloneID:String = (date.milliseconds + id).toString(16);
			
			var ni:uint = 0;			
			return ni + "-" + shortName + cloneID;
		}
		
		public function clonePropertiesFrom(srcUnit:UnitBaseProp):void
		{
			function cloneKeys(keys:Vector.<KeyPair>):Vector.<KeyPair>
			{
				var ret:Vector.<KeyPair> = new Vector.<KeyPair>;
				for (var i:int = 0; i < keys.length; i++)
					ret[i] = keys[i].clone();
				return ret;
			}
			
			this.name = UnitBaseProp.getRandomName(srcUnit, "UnitProp");
			this.textureNames.length = srcUnit.textureNames.length;
			for (var i:int =0; i <this.textureNames.length; i++) {
				this.textureNames[i] = srcUnit.textureNames[i];
			}
			this.lifeFramesNum = srcUnit.lifeFramesNum;			
			this.offsetKeys = cloneKeys(srcUnit.offsetKeys);
			this.scaleKeys = cloneKeys(srcUnit.scaleKeys);
			this.colorKeys = cloneKeys(srcUnit.colorKeys);
			this.rotateKeys = cloneKeys(srcUnit.rotateKeys);
			this.uvType = srcUnit.uvType;
			this.uvAngle = srcUnit.uvAngle;
			this.uvUOffset = srcUnit.uvUOffset;
			this.uvVOffset = srcUnit.uvVOffset;
			this.uvUScale = srcUnit.uvUScale;
			this.uvVScale = srcUnit.uvVScale;
			this.uvV0 = srcUnit.uvV0;
			this.uvV1 = srcUnit.uvV1;
			this.uvColumnsCount = srcUnit.uvColumnsCount;
			this.uvRowsCount = srcUnit.uvRowsCount;
			this.uvRangeBegin = srcUnit.uvRangeBegin;
			this.uvRangeLength = srcUnit.uvRangeLength;
			this.uvFPS = srcUnit.uvFPS;
			this.uvLoop = srcUnit.uvLoop;
			this.texOpIndex = srcUnit.texOpIndex;
			this.blendSrcType = srcUnit.blendSrcType;
			this.blendDstType = srcUnit.blendDstType;
			this.replaceAlpha = srcUnit.replaceAlpha;
			
			this.renderPriority = srcUnit.renderPriority;
			
			this.texOpIndex = srcUnit.texOpIndex;
			this.repeatIndex = srcUnit.repeatIndex;
			this.mipIndex = srcUnit.mipIndex;
			this.filterIndex = srcUnit.filterIndex;
			this.alphaTest = srcUnit.alphaTest;
			
			this.alphaIntensity = srcUnit.alphaIntensity;
			this.alphaSharpness = srcUnit.alphaSharpness;
			this.unitBeginFrame = srcUnit.unitBeginFrame;
			this.uvAutoCenter = srcUnit.uvAutoCenter;
			
			//just for editor(#ifdef maybe better)
			this.origEffectName = srcUnit.name;
			this.aliasFollowUnitName = srcUnit.followUnitName;
			this.uv2UOffset = srcUnit.uv2UOffset;
			this.uv2VOffset = srcUnit.uv2VOffset;
			this.followHolder = srcUnit.followHolder;
			this.updateZ = srcUnit.updateZ;
		}
	}
}