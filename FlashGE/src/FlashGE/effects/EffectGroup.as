package FlashGE.effects
{
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.utils.ByteArray;
	import flash.utils.CompressionAlgorithm;
	import flash.utils.Dictionary;
	import flash.utils.Endian;
	import flash.utils.getTimer;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	import FlashGE.common.Global;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.loaders.RawDataReader;
	import FlashGE.resources.LoaderMgr3D;
	import FlashGE.resources.TextureFile;
	
	use namespace FlashGE;
	public class EffectGroup
	{
		static FlashGE var effectGroups:Dictionary = new Dictionary;
		static FlashGE var filterFunction:Function = null;
		
		FlashGE var curVersion:int = -1;
		FlashGE var fileName:String;
		FlashGE var effectProps:Vector.<EffectProp>;
		FlashGE var nameIndexs:Vector.<String>;
		
		FlashGE var loadState:int  = Defines.LOAD_NULL;
		FlashGE var listeners:Vector.<Function>;
		FlashGE var dataContext:RawDataContext;
		FlashGE var getCurTimer:Function = getTimer;
		
		public function EffectGroup()
		{
		}
		
		static public function print():void
		{
			trace(Global.frameCount, "--------------------------------EffectGroup::print begin")
			var grp:EffectGroup = null;
			for each(grp in effectGroups) {
				trace(grp);
			}
			trace(Global.frameCount, "--------------------------------EffectGroup::print end")
		}
		
		public function toString():String
		{
			return "efg:" + this.fileName;
		}
		
		public function getFileName():String
		{
			return this.fileName;	
		}
		
		public function getNameIndexs():Vector.<String>
		{
			return this.nameIndexs;
		}
		
		static public function createEmpty(fxName:String):EffectGroup
		{
			var ret:EffectGroup = new EffectGroup;
			ret.effectProps = new Vector.<EffectProp>;
			ret.nameIndexs = new Vector.<String>;
			if (fxName) {
				ret.effectProps.push(new EffectProp);
				ret.nameIndexs.push(fxName);
			}
			return ret;
		}
		
		static public function create(fileName:String, loadListener:Function = null, 
									  insertFirst:Boolean = false, 
									  idleLoader:Boolean = false,
									  moveToHighPriority:Boolean = false):EffectGroup
		{
			var ret:EffectGroup;
			var useCache:Boolean = Global.useCacheData;
			
			ret = useCache ? effectGroups[fileName] : null;
			if (ret == null) {
				ret = new EffectGroup;
				if (useCache) effectGroups[fileName] = ret;
				ret.loadFromFile(fileName, false, insertFirst, idleLoader);
			} else {
				if (idleLoader==false) {
					LoaderMgr3D.getInstance().checkIdle(fileName, insertFirst, false);
				}
			}
			
			if (loadListener!=null) {
				ret.addListener(loadListener);
			}
			
			return ret;
		}
		
		static public function createFromPreVersion(fileName:String, listener:Function = null, insertFirst:Boolean = false):EffectGroup
		{
			var ret:EffectGroup = new EffectGroup;
			ret.loadFromFile(fileName, true, insertFirst);
			
			if (listener!=null) {
				ret.addListener(listener);
			}
			
			return ret;
		}
		
		public function getEffectProp(effectName:String):EffectProp
		{
			if (this.effectProps==null) return null;
			
			for (var i:int=0, n:int = this.effectProps.length; i < n; i++) {
				if (this.nameIndexs[i] == effectName) {
					return this.effectProps[i];
				}
			}
			return null;
		}
		
		public function addEffectByName(effectName:String):int
		{
			var n:int = this.nameIndexs.length;
			for (var i:int = 0; i < n; i++) {
				if (this.nameIndexs[i] == effectName) {
					return -1;
				}
			}
			this.effectProps.push(new EffectProp);
			this.nameIndexs.push(effectName);
			return 0;
		}
		
		public function delEffectByName(effectName:String):Boolean
		{
			var n:int = this.nameIndexs.length;
			for (var i:int = 0; i < n; i++) {
				if (this.nameIndexs[i] == effectName) {
					this.nameIndexs.splice(i, 1);
					this.effectProps.splice(i, 1);
					return true;
				}
			}
			
			return false;
		}
		
		public function modifyEffectName(newName:String, index:uint):Boolean
		{
			var n:int = this.nameIndexs.length;
			if (index < n) {
				this.nameIndexs[index] = newName;
				return true;
			}
			
			return false;
		}
		
		private function loadFromFile(fileName:String, preVersion:Boolean, 
									  insertFirst:Boolean, idleLoader:Boolean = false):void
		{
			this.fileName = fileName;
			var listener:Function = preVersion ? onPreVersionComplete : onComplete;
			
			if (idleLoader) {
				LoaderMgr3D.getInstance().pushIdleLoader(fileName, listener, true, false, insertFirst);
			} else {
				LoaderMgr3D.getInstance().pushLoader(fileName, listener, true, false, insertFirst);
			}
		}	
		
		private function onPreVersionComplete(e:Event):void
		{
			if (e is ErrorEvent) {
				loadState = Defines.LOAD_FAILD;
			} else {
				
				var effectProp:EffectProp = new EffectProp;
				this.effectProps = new Vector.<EffectProp>;
				this.effectProps.length = 1;
				this.effectProps[0] = effectProp
				this.nameIndexs = new Vector.<String>;
				this.nameIndexs.length = 1;
				this.nameIndexs[0] = "start";
				
				var rawData:ByteArray = e.target.data;
				rawData.endian = Endian.LITTLE_ENDIAN;
				rawData.position = 0;
				effectProp.readRawDataPreVersion(this, rawData);
				this.loadState = Defines.LOAD_OK;
			}
			
			var i:int = 0;
			var len:int = 0;
			if (this.listeners!=null) {
				for (i=0, len=this.listeners.length; i < len; i++) {
					var f:Function = this.listeners[i];
					f(this);
				}
				this.listeners.length = 0;
			}
		}
		
		private function onComplete(rawData:ByteArray, isHighPriority:Boolean = false):void
		{
			this.dataContext = new RawDataContext;		
			if (rawData == null) {
				loadState = Defines.LOAD_FAILD;
			} else {
				dataContext.raw = rawData;
				dataContext.raw.endian = Endian.LITTLE_ENDIAN;
				dataContext.raw.position = 0;	
			}
			RawDataReader.getInstance().addListener(parseRawData);
		}
		
		private function parseRawData(timer:int):int
		{
			var curTimer:int = getCurTimer();
			var i:int = 0;
			var len:int = 0;
			var f:Function;
			try {
				
				if (this.loadState == Defines.LOAD_FAILD) {
					if (this.listeners != null) {
						len = this.listeners.length;
						while (dataContext.listenerIndex < len) {
							f = this.listeners[dataContext.listenerIndex];
							if (f != null) {
								f(this);
								this.listeners[dataContext.listenerIndex] = null;
								dataContext.listenerIndex++;
								
								if (getCurTimer() - curTimer >= timer) {
									return 0;
								}
							} else {
								dataContext.listenerIndex++;
							}
						}
						this.listeners.length = 0;
					}
				} else {
					
					if (curVersion == -1) {
						
						var tag:String = dataContext.raw.readUTFBytes(7);
						if (tag == "deflate") {
							var temp:ByteArray = new ByteArray;
							temp.endian = Endian.LITTLE_ENDIAN;
							temp.position = 0;
							dataContext.raw.readBytes(temp, 0, dataContext.raw.length - 7);
							temp.uncompress(CompressionAlgorithm.ZLIB);
							dataContext.raw = temp;
						}
						dataContext.raw.position = 0;
						
						var signature:String = dataContext.raw.readUTFBytes(6);
						if (signature != "EFFECT") {
							dataContext.raw.position = 0;
						}
						if (getCurTimer() - curTimer >= timer) {
							return 0;
						}
						
					}
					
					if (this.curVersion == -1) {
						this.curVersion = dataContext.raw.readUnsignedShort();
						dataContext.effectNum = dataContext.raw.readUnsignedShort();
						this.effectProps = new Vector.<EffectProp>;
						this.effectProps.length = dataContext.effectNum;
						this.nameIndexs = new Vector.<String>;
						this.nameIndexs.length = dataContext.effectNum;
						if (getCurTimer() - curTimer >= timer) {
							return 0;
						}
					}
					
					while (dataContext.propIndex < dataContext.effectNum) {
						if (dataContext.propNameReaded == false) {
							len = dataContext.raw.readUnsignedShort();
							this.nameIndexs[dataContext.propIndex] = dataContext.raw.readUTFBytes(len);
							dataContext.propNameReaded = true;
						}
						
						var effectProp:EffectProp = this.effectProps[dataContext.propIndex]
						if (effectProp == null) {
							effectProp = this.effectProps[dataContext.propIndex] = new EffectProp;
						}
						
						if (effectProp.readRawData(this, dataContext.raw, timer - (getCurTimer() - curTimer))) {
							dataContext.propIndex++;
							dataContext.propNameReaded = false;
						}
						
						if (getCurTimer() - curTimer >= timer) {
							return 0;
						}
					}
					
					this.loadState = Defines.LOAD_OK;
					if (this.listeners != null) {
						len = this.listeners.length;
						while (dataContext.listenerIndex < len) {
							f = this.listeners[dataContext.listenerIndex];
							if (f != null) {
								f(this);
								this.listeners[dataContext.listenerIndex] = null;
								dataContext.listenerIndex++;
								
								if (getCurTimer() - curTimer >= timer) {
									return 0;
								}
							} else {
								dataContext.listenerIndex++;
							}
						}
						this.listeners.length = 0;
					}
					
				}
			} catch (e:Event) {
				
				this.loadState = Defines.LOAD_FAILD;
				if (this.listeners != null) {
					len = this.listeners.length;
					for (i = 0; i < len; i++) {
						var callf:Function = this.listeners[i];
						if (callf != null) {
							callf(this);
							this.listeners[i] = null;
						}
					}
				}
			}
			dataContext = null;
			RawDataReader.getInstance().removeListener(parseRawData);
			return timer - (getCurTimer() - curTimer);
		}
		
		public function addListener(listener:Function):void
		{
			if (this.loadState != Defines.LOAD_NULL) {
				listener(this);
			} else {
				if (listeners == null) {
					listeners = new Vector.<Function>;
				}
				listeners.push(listener);
			}
		}
		
		public function removeListener(listener:Function):void
		{
			if (this.listeners != null) {
				for (var i:int = 0, n:int = this.listeners.length; i <n; i++) {
					if (this.listeners[i] == listener) {
						this.listeners[i] = null;
						return;
					}
				}
			}
		}
		
		public function saveTo(rawData:ByteArray):void
		{
			var i:int;
			var len:int;
			rawData.writeUTFBytes("EFFECT");
			rawData.writeShort(this.curVersion);
			var effectNum:int = this.effectProps.length;
			rawData.writeShort(effectNum);
			for (i = 0; i < effectNum; i++) {
				len = this.nameIndexs[i].length;
				rawData.writeShort(len);
				rawData.writeUTFBytes(this.nameIndexs[i]);
				var effectProp:EffectProp = this.effectProps[i];
				effectProp.writeRawData(rawData);
			}
		}
		
		public function importFrom(src:EffectGroup):void
		{
			if (this.effectProps == null || this.nameIndexs == null || 
				src.effectProps == null || src.nameIndexs == null) return;
			
			for (var i:int = 0; i < src.effectProps.length; i++) {
				var srcName:String = src.nameIndexs[i];
				do {
					var repeatName:Boolean = false;
					for (var j:int = 0; j < this.nameIndexs.length; j++) {
						if (srcName == this.nameIndexs[j]) {
							srcName = srcName + "clone";
							repeatName = true;
							break;
						}
					}
					
				} while(repeatName);
				
				this.nameIndexs.push(srcName);
				this.effectProps.push(src.effectProps[i]);
			}
		}
		
		public function cloneBySets(effectName:String, sets:Dictionary):EffectGroup
		{
			function cloneKeys(keys:Vector.<KeyPair>):Vector.<KeyPair>
			{
				var ret:Vector.<KeyPair> = new Vector.<KeyPair>;
				for (var i:int = 0; i < keys.length; i++)
					ret[i] = keys[i].clone();
				return ret;
			}
			
			var newGroup:EffectGroup = new EffectGroup;
			newGroup.curVersion = this.curVersion;
			newGroup.fileName = this.fileName + "clone";
			newGroup.effectProps = new Vector.<EffectProp>;
			var newEffectProp:EffectProp = new EffectProp;
			newGroup.effectProps.push(newEffectProp);
			newGroup.nameIndexs = new Vector.<String>;
			newGroup.nameIndexs.push(effectName);
			newGroup.loadState = Defines.LOAD_OK;
			newGroup.listeners = null;
			newGroup.dataContext = null;
			
			var index:int = 0;
			var effectProp:EffectProp = this.getEffectProp(effectName);
			newEffectProp.linkName = effectProp.linkName;
			newEffectProp.lifeFramesNum = effectProp.lifeFramesNum;
			newEffectProp.followType = effectProp.followType;
			newEffectProp.offsetKeys = cloneKeys(effectProp.offsetKeys);
			newEffectProp.scaleKeys = cloneKeys(effectProp.scaleKeys);
			for (index in sets) {
				var prop:UnitBaseProp = effectProp.unitPropList[index].clone();
				newEffectProp.unitPropList.push(prop);
			}
			return newGroup;
		} // end cloneBySets
		
		public function enumAllTextures(outTextures:Dictionary):void
		{
			var i:int, n:int, item:EffectProp;
			for (i = 0, n = this.effectProps.length; i < n; i++) {
				item = this.effectProps[i];
				item.enumAllTextures(outTextures);
			}
		}
	}
}

import flash.utils.ByteArray;

internal class RawDataContext
{
	public var raw:ByteArray;
	public var effectNum:int = -1;
	public var propIndex:int = 0;
	public var propNameReaded:Boolean = false;
	public var listenerIndex:int = 0;
}