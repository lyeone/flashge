/*@author lye 123*/

package FlashGE.effects
{
	import FlashGE.FlashGE;
	import FlashGE.effects.unitProp.EffectProp;
	import FlashGE.effects.unitProp.SceneCameraUnitProp;
	import FlashGE.effects.unitProp.UnitBaseProp;
	import FlashGE.objects.Entity;
	import FlashGE.render.Camera3D;

	use namespace FlashGE;
	
	public class SceneCamera extends EffectUnitBase	
	{
		public var cacheCamera:Camera3D;
		
		override public function contructor(unitProp:UnitBaseProp = null):void
		{
			super.contructor(unitProp);
			
			if (unitProp == null) {
				this.setProp(new SceneCameraUnitProp);
			} else {
				this.setProp(unitProp);
			}
		}
		
		override public function TYPE():uint
		{
			return EffectProp.TYPE_SCENE_CAMERA;
		}
		
		override FlashGE function updateOneFrame(camera:Camera3D):void			
		{	
			if (!isShow())
				return;
			
			var percent:Number = getPercent();
			if (percent < 0 || percent > 1) {
				resetSceneCamera();
				return;
			}

			cacheCamera = camera;
			var curPlayTime:Number = percent * this.getLifeFramesNum();
			super.updateOneFrame(camera);
			UpdateCamera(cacheCamera, curPlayTime);
		}
		
		public function UpdateCamera(camera:Camera3D, curPlayTime:Number):void 
		{
			var percent:Number = curPlayTime / this.unitProp.lifeFramesNum;
			
			var offsetKey:KeyPair = new KeyPair,
				focusOffsetKey:KeyPair = new KeyPair,
				rotKey:KeyPair = new KeyPair;
			offsetKey.init(0);
			focusOffsetKey.init(0);
			rotKey.init(0);
			
			this.getInterpolationValue(percent, this.unitProp.offsetKeys, offsetKey);
			this.getInterpolationValue(percent, this.unitProp.scaleKeys, focusOffsetKey);
			var rotX:Number, rotY:Number, rotZ:Number;
			rotX = rotY = rotZ = 0;
			if (this.unitProp.rotateKeys.length > 0) {
				this.getInterpolationValue(percent, this.unitProp.rotateKeys, rotKey);
				rotX = rotKey.items[0];
				rotY = rotKey.items[1];
				rotZ = rotKey.items[2];
			}
			
			camera.SetCameraOffset(offsetKey.items[0], offsetKey.items[1], offsetKey.items[2]);
			camera.SetCameraLookAtOffset(focusOffsetKey.items[0], focusOffsetKey.items[1], focusOffsetKey.items[2]);
			camera.SetCameraRot(rotX, rotY, rotZ);
			camera.SetCamType(1);
		}
		
		public function setEditorCam(cam:Camera3D, curPlayTime:Number):void
		{
			if (!cam || !cam.getParent) return;
			
			var percent:Number = curPlayTime / this.unitProp.lifeFramesNum;
			
			var offsetKey:KeyPair = new KeyPair,
				focusOffsetKey:KeyPair = new KeyPair,
				rotKey:KeyPair = new KeyPair;
			offsetKey.init(0);
			focusOffsetKey.init(0);
			rotKey.init(0);
			
			this.getInterpolationValue(percent, this.unitProp.offsetKeys, offsetKey);
			this.getInterpolationValue(percent, this.unitProp.scaleKeys, focusOffsetKey);
			var rotX:Number, rotY:Number, rotZ:Number;
			rotX = rotY = rotZ = 0;
			if (this.unitProp.rotateKeys.length > 0) {
				this.getInterpolationValue(percent, this.unitProp.rotateKeys, rotKey);
				rotX = rotKey.items[0];
				rotY = rotKey.items[1];
				rotZ = rotKey.items[2];
			}
			
			var parent:Entity = cam.getParent();
			parent.setRotation(rotX, rotY, rotZ);
			
			cam.setPosition(offsetKey.items[0], offsetKey.items[1], offsetKey.items[2]);
			
			cam.lookAt(focusOffsetKey.items[0], focusOffsetKey.items[1], focusOffsetKey.items[2]);
		}
		
		public function resetSceneCamera():void
		{
			if (cacheCamera!=null) {
				cacheCamera.SetCameraOffset(0, 0, 0);
				cacheCamera.SetCameraLookAtOffset(0, 0, 0);
				cacheCamera.SetCameraRot(0,0,0);
				cacheCamera.UpdateSceneCamera(false);
				cacheCamera = null
			}
		}
		
		override public function clone():Entity
		{
			var unit:SceneCamera = new SceneCamera();
			unit.contructor();
			unit.clonePropertiesFrom(this);
			return unit;
		}
		
		override protected function clonePropertiesFrom(source:Entity):void
		{
			super.clonePropertiesFrom(source);	
		}
				
		override public function dispose():void
		{
			resetSceneCamera();
			cacheCamera = null;
			super.dispose();
		}
	}
}