package GETool
{
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	import flash.utils.CompressionAlgorithm;
	import flash.utils.Dictionary;
	import flash.utils.Endian;
	
	import FlashGE.FlashGE;
	import FlashGE.common.Defines;
	
	import GETool.common.KeyValue;

	use namespace FlashGE;
	public class AniKeysInfo
	{
		static FlashGE var keysInfoList:Dictionary = new Dictionary;
		FlashGE var fileName:String;
		FlashGE var loadState:int = Defines.LOAD_NULL;
		FlashGE var aniNum:int = 0;
		FlashGE var rawData:Dictionary;
		
		FlashGE var loadListeners:Vector.<Function> = new Vector.<Function>;
		
		public function AniKeysInfo() {}
		
		static public function load(fileName:String, listener:Function):AniKeysInfo
		{
			var ret:AniKeysInfo = AniKeysInfo.keysInfoList[fileName];
			if (ret == null) {
				ret = new AniKeysInfo;
				ret.addListener(listener);
				AniKeysInfo.keysInfoList[fileName] = ret;
				ret.createFromFile(fileName);
			} else {
				ret.addListener(listener);
			}
			return ret;
		}
		
		private function addListener(listener:Function):void
		{
			if (listener==null) return;
			
			if (this.loadState != Defines.LOAD_NULL) {
				listener(this, this.loadState == Defines.LOAD_OK);
			} else {
				this.loadListeners.push(listener);
			}
		}
		
		private function postListeners(isOK:Boolean):void
		{
			for (var i:int = 0, n:int = this.loadListeners.length; i < n; i++) {
				this.loadListeners[i](this, isOK);
			}
			this.loadListeners.length = 0;
		}
		
		public function getFileName():String
		{
			return this.fileName;	
		}
		
		public function createFromFile(fileName:String):void
		{
			this.fileName = fileName;
			var loader:URLLoader = new URLLoader;
			loader.dataFormat = URLLoaderDataFormat.BINARY;
			loader.addEventListener(Event.COMPLETE, onLoadComplete);
			loader.addEventListener(IOErrorEvent.DISK_ERROR, onLoadComplete);
			loader.addEventListener(IOErrorEvent.IO_ERROR, onLoadComplete);
			loader.addEventListener(IOErrorEvent.NETWORK_ERROR, onLoadComplete);
			loader.addEventListener(IOErrorEvent.VERIFY_ERROR, onLoadComplete);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onLoadComplete);
			
			loader.load(Defines.createRequest(fileName));			
		}
		
		private function onLoadComplete(e:Event):void
		{
			if (e is ErrorEvent) {
				this.loadState = Defines.LOAD_FAILD;
				postListeners(false);
				return;
			}
			
			var data:ByteArray = e.target.data;
			data.endian = Endian.LITTLE_ENDIAN;
			data.position = 0;
			var tag:String = data.readUTFBytes(7);
			if (tag == "deflate") {
				var temp:ByteArray = new ByteArray;
				temp.endian = Endian.LITTLE_ENDIAN;
				temp.position = 0;
				data.readBytes(temp, 0, data.length - 7);
				temp.uncompress(CompressionAlgorithm.ZLIB);
				data = temp;
			}
			data.position = 0;
			
			var j:int = 0;
			var aniNum:int = data.readUnsignedByte();
			var curKey:KeyValue;
			for (var i:int = 0; i < aniNum; i++) {
				var len:int = data.readUnsignedByte();
				var aniName:String = data.readUTFBytes(len);
				var keysList:Vector.<Vector.<KeyValue>> = new Vector.<Vector.<KeyValue>>;
				keysList.length = 2;
				
				var atkKeysNum:int = data.readUnsignedShort();
				if (atkKeysNum > 0 ) {
					var atkKeysList:Vector.<KeyValue> = new Vector.<KeyValue>;
					atkKeysList.length = atkKeysNum;
					for (j = 0; j < atkKeysNum; j++) {
						curKey = new KeyValue;
						curKey.keyIndex = data.readUnsignedShort();
						curKey.keyValue = data.readUnsignedShort();
						atkKeysList[j] = curKey;
					}
					
					keysList[0] = atkKeysList;
				}
				
				var fxKeysNum:int = data.readUnsignedShort();
				if (fxKeysNum > 0 ) {
					var fxKeysList:Vector.<KeyValue> = new Vector.<KeyValue>;
					fxKeysList.length = fxKeysNum;
					for (j = 0; j < fxKeysNum; j++) {
						curKey = new KeyValue;
						curKey.keyIndex = data.readUnsignedShort();
						curKey.keyValue = data.readUnsignedShort();
						fxKeysList[j] = curKey;
					}
					keysList[1] = fxKeysList;
				}
				rawData = rawData || new Dictionary;
				rawData[aniName] = keysList;
			}
			
			this.loadState = Defines.LOAD_OK;
			postListeners(true);
		}
		
		public function saveToByteArray():ByteArray
		{
			if (rawData == null) return null;

			var data:ByteArray = new ByteArray;
			data.endian = Endian.LITTLE_ENDIAN;
			data.position = 0;
			var aniNum:int = 0;
			data.writeByte(aniNum);
			
			var j:int = 0;
			for (var aniName:* in this.rawData) {
				aniNum++;
				var len:int = aniName.length;
				data.writeByte(len);
				data.writeUTFBytes(aniName);
				
				var keysList:Vector.<Vector.<KeyValue>> = rawData[aniName];
				// atk
				var atkKeysList:Vector.<KeyValue> = keysList[0];
				var atkKeysNum:int = atkKeysList ? atkKeysList.length : 0;
				data.writeShort(atkKeysNum);
				for (j=0; j < atkKeysNum; j++) {
					data.writeShort(atkKeysList[j].keyIndex);
					data.writeShort(atkKeysList[j].keyValue);
				}
				
				// fx
				var fxKeysList:Vector.<KeyValue> = keysList[1];
				var fxKeysNum:int = fxKeysList ? fxKeysList.length : 0;
				data.writeShort(fxKeysNum);
				for (j=0; j < fxKeysNum; j++) {
					data.writeShort(fxKeysList[j].keyIndex);
					data.writeShort(fxKeysList[j].keyValue);
				}				
			}
			data.position = 0;
			data.writeByte(aniNum);
			var orginData:ByteArray = new ByteArray;
			orginData.endian = Endian.LITTLE_ENDIAN;
			orginData.position = 0;
			data.position = 0;
			data.readBytes(orginData, 0, data.length);
			
			data.position = 0;
			data.compress(CompressionAlgorithm.ZLIB);
			
			if (data.length + 7 < orginData.length) {
				var ret:ByteArray = new ByteArray;
				ret.endian = Endian.LITTLE_ENDIAN;
				ret.position = 0;
				ret.writeUTFBytes("deflate");
				ret.writeBytes(data);
				return ret;
			}
			return orginData;
		}
		
		private function delKeys(aniName:String, keyIndex:int, index:int):void
		{
			if (rawData == null) return;
			
			var keysList:Vector.<Vector.<KeyValue>> = rawData[aniName];
			if (keysList == null) return;
			
			var itemKeysList:Vector.<KeyValue> = keysList[index];
			if (itemKeysList == null) return;
			
			for (var i:int = 0; i < itemKeysList.length; i++) {
				if (itemKeysList[i].keyIndex == keyIndex) {
					itemKeysList.splice(i, 1);
					
					for(var j:int =0; j <keysList.length; j++) {
						if (keysList[j] && keysList[j].length > 0)
							return;
					}
					delete rawData[aniName];
					
					return;
				}
			}
		}
		
		private function addKeys(aniName:String, keyIndex:int, keyValue:int, typeIndex:int):void
		{
			rawData = rawData || new Dictionary;
			
			var keysList:Vector.<Vector.<KeyValue>> = rawData[aniName];
			if (keysList == null) {
				keysList = rawData[aniName] = new Vector.<Vector.<KeyValue>>;
				keysList.length = 2;
			}
			
			var itemKeysList:Vector.<KeyValue> = keysList[typeIndex];
			if (itemKeysList == null) {
				itemKeysList = keysList[typeIndex] = new Vector.<KeyValue>; 
			}
			
			for (var i:int = 0; i < itemKeysList.length; i++) {
				if (itemKeysList[i].keyIndex == keyIndex) {
					itemKeysList[i].keyValue = keyValue
					return;
				}
			}
			
			var key:KeyValue = new KeyValue;
			key.keyIndex = keyIndex;
			key.keyValue = keyValue;
			itemKeysList.push(key);
		}
		
		public function addAtkKeys(aniName:String, keyIndex:int, keyValue:int):void
		{
			addKeys(aniName, keyIndex, keyValue, 0);
		}
		
		public function addFxKeys(aniName:String, keyIndex:int, keyValue:int):void
		{
			addKeys(aniName, keyIndex, keyValue, 1);
		}
		
		public function delAtkKeys(aniName:String, keyIndex:int):void
		{
			delKeys(aniName, keyIndex, 0);
		}
		
		public function delFxKeys(aniName:String, keyIndex:int):void
		{
			delKeys(aniName, keyIndex, 1);
		}
		
		public function getKeysList(aniName:String):Vector.<Vector.<KeyValue>>
		{
			if (rawData == null) return null;
			var keysList:Vector.<Vector.<KeyValue>> = rawData[aniName];
			return keysList;
		}
		
		public function getKeyInfo(aniName:String, keyIndex:int):Object
		{			
			if (rawData == null) return null;
			
			var keysList:Vector.<Vector.<KeyValue>> = rawData[aniName];
			if (keysList == null) return null;
			
			var ret:Object;
			
			for (var j:int = 0; j < 2; j++) {
				var itemKeysList:Vector.<KeyValue> = keysList[j];
				if (itemKeysList != null) {
					ret = ret || new Object;
					ret[j] = false;
					for (var i:int = 0; i < itemKeysList.length; i++) {
						if (itemKeysList[i].keyIndex == keyIndex) {
							ret[j] = true;
							break;
						}
					}
				}
			}			
			return ret;
		}
	}
}
